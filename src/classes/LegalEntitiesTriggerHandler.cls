public with sharing class LegalEntitiesTriggerHandler {

	public static void handleAfterInsert(Map<Id, LLC_BI__Legal_Entities__c> newMap){
		
		//get a list of entities that are part of the treasury service, create authorized signers for them
		List<LLC_BI__Legal_Entities__c> tsList = new List<LLC_BI__Legal_Entities__c>();
		for(LLC_BI__Legal_Entities__c le : newMap.values()){
			if(le.LLC_BI__Treasury_Service__c != null){
				tsList.add(le);
			}
		}
		System.debug('tsList size ' + tsList.size());
		if(tsList.size() > 0){
			createAuthorizedSigners(tsList);
		}
	}

	public static void handleAfterUpdate(Map<Id, LLC_BI__Legal_Entities__c> oldMap, Map<Id, LLC_BI__Legal_Entities__c> newMap){
		List<LLC_BI__Legal_Entities__c> tsList = new List<LLC_BI__Legal_Entities__c>();
		for(LLC_BI__Legal_Entities__c le : newMap.values()){
			if(le.LLC_BI__Treasury_Service__c != oldMap.get(le.id).LLC_BI__Treasury_Service__c && oldMap.get(le.Id).LLC_BI__Treasury_Service__c == null){
				tsList.add(le);
			}
		}
		System.debug('tsList size ' + tsList.size());
		if(tsList.size() > 0){
			createAuthorizedSigners(tsList);
		}
	}

	//add the authorized signers from an Account to the legal entities as the entities are created
	//each contact on an account should only have one authorized signer, regardless of the number of legal entities
	//this is consistent with how it works when a contact is created as an authorized signer
    public static void createAuthorizedSigners(List<LLC_BI__Legal_Entities__c> entitiesList) {
    	System.debug('entities ' + entitiesList);
    	//get accounts and contacts for the entities
        Set<Id> acctSet = new Set<Id>();

        //loop through the entities and get their accounts
        for(LLC_BI__Legal_Entities__c le : entitiesList) {
            acctSet.add(le.LLC_BI__Account__c);
        }

        //get contacts for these accounts 
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        List<Contact> contactList = [Select id, Authorized_Signer__c, accountId from Contact where accountid in :acctSet and Authorized_Signer__c = true];
   	 	//populate contact set
   	 	for(Contact c : contactList){
   	 		contactMap.put(c.id, c);
   	 	}

   	 	System.debug('Contact Map ' + contactMap);

        //check to see if the contact already has at least one authorized signer
        List<LLC_BI__Contingent_Liabilty__c> signerList = [select id, LLC_BI__Contact__c from LLC_BI__Contingent_Liabilty__c where LLC_BI__Contact__c in :contactList];
        System.debug('signerList size' + signerList.size());

        //if there are signers in the list then they need to be removed from contactList
        if(signerList.size() > 0){

       	 	//remove contacts with signers from the contact list
       	 	for(LLC_BI__Contingent_Liabilty__c signer : signerList){
        		if(contactMap.containsKey(signer.LLC_BI__Contact__c)){
        			contactMap.remove(signer.LLC_BI__Contact__c);
        		}
        	}
        }

        //create a map where id is the account Id and it has a list of entitys
        Map<Id, List<LLC_BI__Legal_Entities__c>>accountToEntityMap = new Map<Id, List<LLC_BI__Legal_Entities__c>>();
        for(LLC_BI__Legal_Entities__c entity : entitiesList) {
            List<LLC_BI__Legal_Entities__c> tempList = accountToEntityMap.get(entity.LLC_BI__Account__c);
            if(tempList == null){
                tempList = new List<LLC_BI__Legal_Entities__c>();
                accountToEntityMap.put(entity.LLC_BI__Account__c, tempList);
            }
            tempList.add(entity);
        }
        System.debug('::Map ' + accountToEntityMap);

        List<LLC_BI__Contingent_Liabilty__c> authorizedSignerList = new List<LLC_BI__Contingent_Liabilty__c>();

        //loop through contacts, for each contact, get the list of entities it needs an LLC_BI__Contingent_Liabilty__c record
        //for and create that record
        for(Contact c : contactMap.values()) {
            System.debug('::Contact ' + c);
            List<LLC_BI__Legal_Entities__c> entities = accountToEntityMap.get(c.AccountId);
            if(entities != null){
                LLC_BI__Contingent_Liabilty__c signer = new LLC_BI__Contingent_Liabilty__c();
                signer.LLC_BI__Authority__c = 'Authorized';
                signer.LLC_BI__Role__c = 'Official';

                signer.LLC_BI__Entity__c = entities[0].id;
                signer.LLC_BI__Contact__c = c.id;
                authorizedSignerList.add(signer);
            }
        }

        if(authorizedSignerList.size() > 0){
            insert authorizedSignerList;
        }
    }	
}