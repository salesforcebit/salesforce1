@isTest
private class ScenarioItemTriggerHandler_UT {

	@testSetup static void testSetup() {
		Opportunity o =	UnitTestFactory.buildTestOpportunity();
		insert o;
	    LLC_BI__Scenario__c  s = UnitTestFactory.buildTestScenario(o);
	    insert s;
	    Scenario_Product__c sp = new Scenario_Product__c(Name='X',Scenario__c=s.Id);
	    insert sp;
		/*
		LLC_BI__Scenario_Item__c
		Scenario_Product__c






		*/


	}

	@isTest static void updateScenItemsWExceptionPricingTEST() {
		//query for the scenario product
		Scenario_Product__c sp = [SELECT id, Name, Scenario__c from Scenario_Product__c WHERE Name='X'];

		LLC_BI__Scenario_Item__c si1 = new LLC_BI__Scenario_Item__c(LLC_BI__Scenario__c = sp.Scenario__c, Scenario_Product__c = sp.id, LLC_BI__Standard_Price__c = .08, LLC_BI__Exception_Price__c = .09);
		LLC_BI__Scenario_Item__c si2 = new LLC_BI__Scenario_Item__c(LLC_BI__Scenario__c = sp.Scenario__c, Scenario_Product__c = sp.id, LLC_BI__Standard_Price__c = .08, LLC_BI__Exception_Price__c = .08);
		LLC_BI__Scenario_Item__c si3 = new LLC_BI__Scenario_Item__c(LLC_BI__Scenario__c = sp.Scenario__c, Scenario_Product__c = sp.id, LLC_BI__Standard_Price__c = .08, LLC_BI__Exception_Price__c = .08);
		insert si3;
		si3.LLC_BI__Exception_Price__c = .07;

		Test.startTest();
		insert new LLC_BI__Scenario_Item__c[] {si1, si2};
		update si3;
		Test.stopTest();

		//requery
		Scenario_Product__c sp2 = [SELECT id, Name, Scenario__c, Scenario_Items_with_NonStandard_Price__c from Scenario_Product__c WHERE Name='X'];

		//assertions
		System.assertEquals(2, sp2.Scenario_Items_with_NonStandard_Price__c, 'Failed to calculate the number of scenario items with exception priced to equal true');
	}

}