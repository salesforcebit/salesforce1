@isTest
public with sharing class TestECCP_ProdPackApprovalController {
	@isTest
    public static void start() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
    }
    
    @isTest
    public static void cancel() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.cancel();
            
        Test.stopTest();
    }
    
    @isTest
    public static void delegateAuthority() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getdelegatedAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority1() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority2() {
        Account testAccount = createAccount('10');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority3() {
        Account testAccount = createAccount('17');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void reviewer1() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getreviewer1();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer2() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getreviewer2();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer3() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getreviewer3();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer4() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getreviewer4();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer5() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getreviewer5();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer6() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.getreviewer6();
        
        Test.stopTest();
    }
    
    @isTest
    static void save() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        
        System.ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ProductPackageApprovalController ppac = new ECCP_ProductPackageApprovalController(controller);
        
        Test.startTest();
        
        ppac.productPackageSave();
        
        Test.stopTest();
    }
    
    private static Account createAccount(String grade) {
        Account newA = new Account(
        	Name = 'Test',
        	Proposed_Risk_Rating__c = grade);
        
        insert newA;
        return newA;
    }
    
    private static LLC_BI__Product_Package__c createProductPackage(Id acctId) {
        LLC_BI__Product_Package__c newPP = new LLC_BI__Product_Package__c(
        	Name = 'Test',
        	LLC_BI__Account__c = acctId);
        
        insert newPP;
        return newPP;
    }
}