global with sharing class DataInstaller implements InstallHandler, UninstallHandler {
	global void onInstall(InstallContext context){
		createNewConfig();
		createConfigLayout();
	}

	global void onUninstall(UninstallContext context){
		Map<Id, nFORCE__Group__c> groupMap =
			new Map<Id, nFORCE__Group__c> (
				[
					SELECT
						Id,
						Name
					FROM
						nFORCE__Group__c
					WHERE
						Name =: TM_CONFIG_GROUP_NAME
					AND
						nFORCE__App__c =: TM_CONFIG_APP_NAME
				]
			);

		Map<Id, nFORCE__Route_Group__c> routeGroupMap =
			new Map<Id, nFORCE__Route_Group__c> (
				[
					SELECT
						Id,
						Name,
						nFORCE__Route__c,
						nFORCE__Route__r.Id,
						nFORCE__Parent_Route__c,
						nFORCE__Parent_Route__r.Id
					FROM
						nFORCE__Route_Group__c
					WHERE
						nFORCE__Group__c IN  :groupMap.keySet()
				]
			);
		
		Set<Id> routeIdSet = new Set<Id>();

		for (nFORCE__Route_Group__c rg : routeGroupMap.values()) {
			if (rg.nFORCE__Route__c != null) {
				routeIdSet.add(rg.nFORCE__Route__c);
			}
			if (rg.nFORCE__Parent_Route__c != null) {
				routeIdSet.add(rg.nFORCE__Parent_Route__c);
			}
		}
		
		Map<Id, nFORCE__Route__c> routeMap = 
			new Map<Id, nFORCE__Route__c> (
				[
					SELECT
						Id,
						Name
					FROM
						nFORCE__Route__c
					WHERE
						Id IN  :routeIdSet
				]
			);		
		
		Database.Delete(routeGroupMap.values());
		Database.Delete(routeMap.values());
		Database.Delete(groupMap.values());
		
		Utility.deleteSystemProperty(
    		TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    		TreasuryConfigurationConstants.TM_CONFIG_IS_PRODUCTION_KEY
		);
		
	}
	
	private static void createNewConfig(){
		sObject property = 
			Utility.createSystemProperty(
				null,
				TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
				TreasuryConfigurationConstants.TM_CONFIG_IS_PRODUCTION_KEY,
				'true',
				true,
				true
			);
	}

	private static void createConfigLayout(){
		nFORCE__Group__c g1 = createGroup();
		List<nFORCE__Route__c> routes = createRoutes();
		nFORCE__Route__c maintenanceRoute = createMaintenanceRoute();
		routes.add(maintenanceRoute);
		createRouteGroups(routes, g1.Id, null);
		List<nFORCE__Route__c> maintRoutes = createMaintenanceRoutes();
		createRouteGroups(maintRoutes, g1.Id, maintenanceRoute.Id);
	}

	private static nFORCE__Group__c createGroup(){
		nFORCE__Group__c group1 = new nFORCE__Group__c(
			Name = TM_CONFIG_GROUP_NAME,
			nFORCE__App__c = TM_CONFIG_APP_NAME
		);
		insert group1;
		return group1;
	}

	private static List<nFORCE__Route__c> createRoutes(){
		List<nFORCE__Route__c> newRoutes = new List<nFORCE__Route__c>();
		nFORCE__Route__c route = new nFORCE__Route__c(
			Name = 'Overview',
			nFORCE__App__c = 'tm-config-system',
			nFORCE__Is_Hidden__c = false,
			nFORCE__Topbar__c = 'nforce__topbar',
			nFORCE__Body__c = 'TreasuryConfiguration',
			nFORCE__Navigation__c = 'nforce__navigation',
			nFORCE__Sub_Navigation__c = 'nforce__sub_navigation',
			nFORCE__Icon_Class__c = 'n-icon-treasury_configuration');
		newRoutes.add(route);
		route = new nFORCE__Route__c(
			Name = 'Product Catalog',
			nFORCE__App__c = 'tm-config-cat',
			nFORCE__Is_Hidden__c = false,
			nFORCE__Topbar__c = 'nforce__topbar',
			nFORCE__Body__c = 'ProductCatalog',
			nFORCE__Navigation__c = 'nforce__navigation',
			nFORCE__Sub_Navigation__c = 'nforce__sub_navigation',
			nFORCE__Icon_Class__c = 'n-icon-treasury_configuration');
		newRoutes.add(route);
		/* Future functionality
		route = new nFORCE__Route__c(
			Name = 'Default Workflows',
			nFORCE__App__c = 'tm-config-wf',
			nFORCE__Is_Hidden__c = false,
			nFORCE__Topbar__c = 'nforce__topbar',
			nFORCE__Body__c = 'WorkflowCreation',
			nFORCE__Navigation__c = 'nforce__navigation',
			nFORCE__Sub_Navigation__c = 'nforce__sub_navigation',
			nFORCE__Icon_Class__c = 'n-icon-treasury_configuration');
		newRoutes.add(route);**/
		insert newRoutes;
		return newRoutes;
	}

	private static nFORCE__Route__c createMaintenanceRoute(){
		nFORCE__Route__c route = new nFORCE__Route__c(
			Name = 'Maintenance Configuration',
			nFORCE__App__c = 'tm-config-mt',
			nFORCE__Is_Hidden__c = false,
			nFORCE__Topbar__c = 'nforce__topbar',
			nFORCE__Body__c = 'MaintenanceConfiguration',
			nFORCE__Navigation__c = 'nforce__navigation',
			nFORCE__Sub_Navigation__c = 'nforce__sub_navigation',
			nFORCE__Icon_Class__c = 'n-icon-treasury_configuration');
		insert route;
		return route;
	}

	private static void createRouteGroups(List<nFORCE__Route__c> routes, Id groupId, Id parentId){
		Integer i = 1;
		List<nFORCE__Route_Group__c> routeGroups = new List<nFORCE__Route_Group__c>();
		for(nFORCE__Route__c route : routes){
			routeGroups.add(new nFORCE__Route_Group__c(
				nFORCE__Group__c = groupId,
				nFORCE__Route__c = route.Id,
				nFORCE__Parent_Route__c = parentId,
				nFORCE__Order__c = i
				));
			i++;
		}
		insert routeGroups;
	}

	private static List<nFORCE__Route__c> createMaintenanceRoutes(){
		List<nFORCE__Route__c> newRoutes = new List<nFORCE__Route__c>();
		nFORCE__Route__c route = new nFORCE__Route__c(
			Name = 'Maintenance Types',
			nFORCE__App__c = 'tm-config-maint-type',
			nFORCE__Is_Hidden__c = false,
			nFORCE__Topbar__c = 'nforce__topbar',
			nFORCE__Body__c = 'MaintenanceType',
			nFORCE__Navigation__c = 'nforce__navigation',
			nFORCE__Sub_Navigation__c = 'nforce__sub_navigation',
			nFORCE__Icon_Class__c = 'n-icon-treasury_configuration');
		newRoutes.add(route);
		route = new nFORCE__Route__c(
			Name = 'Maintenance Checklist Items',
			nFORCE__App__c = 'tm-config-cl-actions',
			nFORCE__Is_Hidden__c = false,
			nFORCE__Topbar__c = 'nforce__topbar',
			nFORCE__Body__c = 'MaintenanceChecklist',
			nFORCE__Navigation__c = 'nforce__navigation',
			nFORCE__Sub_Navigation__c = 'nforce__sub_navigation',
			nFORCE__Icon_Class__c = 'n-icon-treasury_configuration');
		newRoutes.add(route);
		route = new nFORCE__Route__c(
			Name = 'Checklist Map',
			nFORCE__App__c = 'tm-config-map-types',
			nFORCE__Is_Hidden__c = false,
			nFORCE__Topbar__c = 'nforce__topbar',
			nFORCE__Body__c = 'MaintenanceMapping',
			nFORCE__Navigation__c = 'nforce__navigation',
			nFORCE__Sub_Navigation__c = 'nforce__sub_navigation',
			nFORCE__Icon_Class__c = 'n-icon-treasury_configuration');
		newRoutes.add(route);
		insert newRoutes;
		return newRoutes;
	}

	/**
	* Routing Configuration Constants
	*/
	private static final String TM_CONFIG_GROUP_NAME = 'Treasury Configuration';
	private static final String TM_CONFIG_APP_NAME = 'treasury-config';
	
}