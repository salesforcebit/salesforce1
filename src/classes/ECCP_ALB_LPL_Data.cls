public class ECCP_ALB_LPL_Data{
        public ECCP_ALB_LPL_Data(){
        }
        public Decimal ProposedAmount;
        public Decimal InitialLoanInterestRate ;
        public string ApplicationNumber;
        public boolean AutoPay;
        public string ApproverType;
        public integer NumberofPayments ;
        public string InitialLonNoteRate;
        public Date FirstPaymentDate;
        public string PaymentType;
        public Decimal InitialLoanNoteRate;
        public Decimal PrincipalInterest;
        public string InterestPaymentFrequency;
        public Decimal Spread;
        public Decimal AmortizedMonth;
        public Decimal RateCeiling;
        //public Decimal RateFloor;
        public Decimal Margin ;
        public String PaymentSchedule;
        public string PricingBasis;
        public string LoanPurposeCode;
        public string CRASalesCode;
        public Date MaturityDate;
        public Decimal PeriodstoAmortize;
        //public Decimal RateCeiling;
        public Decimal RateFloor;
        public String RateType;
        //public String LoanPurposeCode;
        public string RequireAnnualStatements;
        public string PersonPreparingStatement;
        public string RequireInterimStatements;
        public string RequireCopiesofTaxReturns;
        public string DueDaysforTaxReturns;
        public Decimal TotalAmountBorrowerOwestoCreditor;
        public string PeriodicPaymentDetails ;
        public Decimal PeriodicPaymentAmount ;
        public string PrincipalPaymentSchedule;
        public string DisbursementDescription;
        public String DisbursementPaymentNumber;
        public String DisbursementAccountType;
        public decimal TotalDisbursed;
        public Decimal OriginalAmount;
        public String RecordID;
        public Decimal InterestRate ;
        public String BorrowerState; 
        public String Branch;
        public String RiskType;
        public String AssignmentUnit;
        public decimal ProposedObligorRiskRating;        
        public String AutomaticLoanPayment;
        public List<FeeDataClass> feeData;
        public list<collateralDataClass> loancollateralData;
        // Entities Involved object's Account 
        public list<AccountDataClass> loanAccountData;
        
        // from branch object
        public string costCenter;
     
        //Null fields and Error Messages
        public string ChargeType;
        public string LenderSecurityInterestChargesDescrip;
        public string LenderSecurityInterestCharge;
        public string InterestType;
        public string HostSystem;
        public Date LoanAgreementEndDate; 
        public string StateNull;
        public string FirstLienIndicator;

        public Decimal CertificateDeliveryGracePeriod;
        public String AnnualStatementPreparedby ;
        public Decimal MaximumLoanAmount ;
        public String InterestOnlyPayments;
        //public String PeriodicPaymentDetails;
        public Decimal CreditorPaymentAmount;
        public String PrincipalPaymentFrequency;  
        public string sendNullValues; 
        //Error Handling
        public string ErrorCode; 
        public string ErrorMessages; 
        //public list<CollateralGuarantorInformation> CollateralGuarantorList;    // CR-0002 Change moved this section under Collateral   
        //Added below 3 fields as part of CR-0002
        public String ProductKey;
        public String CRACode;
        public String RateDescription;
        // Release 1.1 Changes
        public String MaximumAvailability;
        public String MaximumAgeofEligibleAccounts;
        public String AdditionalLimitEligibleAccounts;
        public String InventoryEligibleExclusions;
        public String EligibleInventoryAdditionalLimitations;
        public Date ClosingDate;
        public String BankNumber;
        public Date PaymentDueDate;
        public String ComplianceCertificateDeliveryGracePeriod;
        //Relese 2.0 Changes
        public list<LandlordsConsentType> LandlordsConsent;
        public list<CovenantType> Covenants;
        public Date FirstInterestPaymentDate;
        public String InterestPaymentSchedule;
        public String RMOfficerCode;
        public String InsiderLoanYN;
        public String RestrictAccessYN;
        public String RegOLoan;
        public String PrePmtPenaltyYN;
        public String DDAAccountNumber;
        public String LoanAgreementType;
        public String ObligationRiskRating;
        public String NewMoney;
        public String CreditRatingDate;
        public String ProbabilityofDefault;
        public String DateRated;
        public String LossGivenDefault;
        public String PrepaymentPenaltyCode;  
        public String CreditScore;    
        public String AddReqAnnualStmtsYN;  
        public String AddReqAnnualStmtPrepBy;
        public String AddReqInterimStmtsYN;
        public String AddReqCopiesOfTaxReturnsYN;                   
        
        /*
        public list<Date> DateofLease ;
        public list<string> StreetAddress;
        public list<string> city;
        public list<string> State;
        public list<string> Zipcode;
        public list<string> Covenants;
        */
        
    //Release 2.0    
    public Class LandlordsConsentType{
        public Date DateofLease;
        public string StreetAddress;
        public string city;
        public string State;
        public string Zipcode;    
    }
    
    public class CovenantType {
        public String CovenantId;
        public String ComplianceCertificateDeliveryFrequency;
        public String DueXDaysAfterFYEAnnual;
        public String IntermStatementPreparedby;
        public String DueXDaysAfterInterm;
        public String IntermStatementDelivered;
        public String WhoPreparesTaxReturns;
        public String CovenantType;
        public String CovenantCode;
        public String AddReqAnnualStmtDueWithin;
        public String AddReqInterimStmtPrepBy;
        public String AddReqInterimStmtDueWithin;
        public String AddReqTaxReturnPrepBy;
        public String AddReqTaxReturnDueWithin;        
    }

    public class AccountdataClass{
        public String AccountId;
        public string EntityRelationShipType;    
        public string OrganizationType;
        public string EntityRoles;
        public string FirstNameandMiddleNname;
        public string Lastname;
        public string Title;  
        public string HomePhone;              
        public string BusinessName;
        public string TaxIdentificationNumber;
        public string FullBillingAddress;
        public string BillingCity;
        public string BillingCounty;
        public string BillingState;
        public string BillingPostalCode;
        public string PrimaryPhoneNumber;

        public string Authority;
        public string NAICSCode;
        public string IncorporationState;
        public string OrganizationID;
        public string CustomerNumber;
        public string RiskRating;
        public string NonProfitCorporation;
        public string Type;
        public list<AuthorizedSignerDataClass> conAuthorizedSignerData;  
        public string GuarantorEntityRoles; 
        //Release 2.0
        public String DBA; 
        public String GuarSecuredByCollateralYN;
        public String EntAccId;
        
      
             
    }

    public class collateralDataClass{
        public string UCCState;
        public string GeneralCollateralType;
        public string CollateralSubType;
        public string AgriculturalUse;
        public string CollateralDescription;
        public string BodyStyle;
        public string CollateralCity;
        public string CollateralCounty;
        public string CollateralState;
        public string Make;
        public string Model;
        public string PossessoryAccountNumber;
        public Decimal PossessoryCurrentBalance;
        public Decimal NumberofUnits;
        public Decimal CollateralID;
        public string StreetAddress;
        public string PropertyTaxID;
        public string VehicleIdentificationNumber;
        public string Year;
        public string Zipcode;
        public string FirstLienIndicator;
        public string FloodSectionDetail;
        public string BookEntry;
        public string LifeInsurancePolicyNumber;
        public Integer LifeInsuranceBenefitAmount;
        public string Nameoftheinsured;
        public string LifeInsuranceCompany;
        public string LifeInsuranceCompanyStreetAdress;
        public string LifeInsuranceCompanyCity;
        public string LifeInsuranceCompanyState;
        public string LifeInsuranceCompanyZipcode;
        public string CollateralType;
        public string Manufacturer;
        public string FAARegistrationNumber;
        public string UCCTypes;
        public string VesselName;
        public string VesselNumber;
        public string StateRegistered; 
        public CollateralGrantorInformationType CollateralGuarantorList; // CR Change -  moved CollateralGrantorInformationType section to Collateral  
        //Release 1.1 changes
        public String City;
        public String State;
        public id CollateralKey;
        //Release 2.0
        public String CollateralInsuranceAmount;    

                  
    }
    public class CollateralGrantorInformationType{
        public String CGOrganizationType;
        public String CGFirstNameandMiddleNname;
        public String CGLastname;
        public String CGBusinessName;
        public String CGTaxIdentificationNumber;
        public String CGFullAddress;
        public String CGCity;
        public String CGCounty;
        public String CGState;
        public String CGPostalCode;
        public String CGPrimaryPhone;
        public Decimal CollateralID;
        public id CollateralKey;
        public String CollateralGrantorKey;
        //Release 2.0
        public string GrantorDBA;

    } /*   
    public class ContactDataClass{
        public string FirstNameMiddleName;
        public string LastName;
        public string HomePhone;
        public string Title;
        public list<AuthorizedSignerDataClass> conAuthorizedSignerData;    
    }*/
    public class AuthorizedSignerDataClass{
        public string AuthorizedSigner;
    }

    public class FeeDataClass{
        public String Feetype;
        public Decimal PrepaidFinanceChargeCash;
        public String PrepaidFinanceChargeDescription;
        public Decimal PrepaidFinanceChargeFinanced;
    }

}