@isTest
private class TROAddAccountRequest_UT {

    static testMethod void addAccountRequestTest() {
    	TROAddAccountRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_addAccountRequest_Port testAddAccount = new TROAddAccountRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_addAccountRequest_Port();

    	TROAddAccountRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTestExportFields();

    	Test.setMock(WebServiceMock.class, new TRO_AddAccountRequest_Mock());

		Test.startTest();
    		TROAddAccountRequest_v2.KTTResponse response = testAddAccount.processAddAccountAPI(exportFields);
		Test.stopTest();

			System.assertEquals(response.CSOAutomation.KTTResponse.status, 'Good to go');

    }

    static testMethod void aSyncAddAccountRequestTest() {
        AsyncTROAddAccountRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_addAccountRequest_Port aSyncAddAccount = new AsyncTROAddAccountRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_addAccountRequest_Port();

        TROAddAccountRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTestExportFields();
        Continuation con = new Continuation(120);
        con.continuationMethod = 'processResponse';

        Test.setMock(WebServiceMock.class, new TRO_AsyncAddAccountRequest_Mock());

        AsyncTROAddAccountRequest_v2.processAddAccountAPIResponseFuture response = aSyncAddAccount.beginProcessAddAccountAPI(con, exportFields);

    }

}