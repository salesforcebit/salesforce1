/*******************************************************************************************************************************************
* @name         :   ECC_CommentsFeedTrackingTest
* @description  :   This class will be responsible to create data for test classes.
* @author       :   Lakshmi
* @createddate  :   23/August/2016
*******************************************************************************************************************************************/
@isTest
public class ECC_CommentsFeedTrackingTest{
    public static testMethod void ECC_CommentsFeedTrackingTest(){
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
       User u = new User(Alias = 'stdKey', Email='standarduser@testorg.comKey', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comKey',PLOB__c='Support');
       insert u;
       System.runAs(u){
       /************Create Account**************/
        account acc = new account(Name = 'Test Account',Type ='Business Prospect',tin__c='12345');
        insert acc;
         
       
       /************Create Contact**************/
         contact con = new contact(firstname = 'Test',lastname = 'Test',AccountId = acc.Id, SSN__c = 'SSN',phone='1234567890');
         insert con;
          
       /**************Create Product Package*******************/ 
          LLC_BI__Product_Package__c pp = new LLC_BI__Product_Package__c();
          pp.Name = acc.name;
          PP.Bank_Division__c ='Business Banking';
          pp.LLC_BI__Account__c = acc.id;
          pp.LLC_BI__Stage__c = 'Application Processing';
          pp.LLC_BI__Primary_Officer__c = u.Id;
          pp.Credit_Authority_Name__c =u.Id;
          pp.Delegated_Authority_Name__c  = u.Id;
          insert pp;
          pp.Hold_Comments__c ='Test';
          update pp;  
        
        /*****************Create Loan*****************/

         LLC_BI__Loan__c newLoan = new LLC_BI__Loan__c(); 
         newLoan.name = 'LoanABCTest';
         newLoan.LLC_BI__Stage__c = 'Application Processing' ;
         newLoan.LLC_BI__Account__c = acc.Id;
         newLoan.LLC_BI__Amount__c = 56789000;
         newLoan.ECCP_Application_Number__c = 'ABC0977';
         newLoan.LLC_BI__Product_Package__c = pp.Id;
         newLoan.ECC_Rate_Description__c = '00170 Akron Stmt Sav';
         insert newLoan;  
     }
   }  
}