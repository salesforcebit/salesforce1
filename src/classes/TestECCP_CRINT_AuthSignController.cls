@isTest
public class TestECCP_CRINT_AuthSignController {
    @isTest
    static void start() {
        Account testAccount = createAccount();
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id);
        LLC_BI__Legal_Entities__c testEntity = createLegalEntity(testLoan.Id, testAccount.Id);
        
        ApexPages.currentPage().getParameters().put('id', testLoan.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CRINT_AuthorizedSignersController ecasc = new ECCP_CRINT_AuthorizedSignersController(controller);
    }
    
    @isTest
    static void viewAuthSigner() {
        Account testAccount = createAccount();
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id);
        LLC_BI__Legal_Entities__c testEntity = createLegalEntity(testLoan.Id, testAccount.Id);
        Contact testContact = createContact(testAccount.Id);
        LLC_BI__Contingent_Liabilty__c testAuthSigner = createAuthorizedSigner(testContact.Id, testEntity.Id);
        
        ApexPages.currentPage().getParameters().put('id', testLoan.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CRINT_AuthorizedSignersController ecasc = new ECCP_CRINT_AuthorizedSignersController(controller);
        
        Test.startTest();
        
        ecasc.authorizedSignerId = testAuthSigner.Id;
        ecasc.viewAuthorizedSigner();
        
        Test.stopTest();
    }
    
    @isTest
    static void back() {
        Account testAccount = createAccount();
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id);
        LLC_BI__Legal_Entities__c testEntity = createLegalEntity(testLoan.Id, testAccount.Id);
        Contact testContact = createContact(testAccount.Id);
        LLC_BI__Contingent_Liabilty__c testAuthSigner = createAuthorizedSigner(testContact.Id, testEntity.Id);
        
        ApexPages.currentPage().getParameters().put('id', testLoan.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CRINT_AuthorizedSignersController ecasc = new ECCP_CRINT_AuthorizedSignersController(controller);
        
        Test.startTest();
        
        ecasc.authorizedSignerId = testAuthSigner.Id;
        ecasc.viewAuthorizedSigner();
        ecasc.back();
        
        Test.stopTest();
    }
    
    private static LLC_BI__Loan__c createLoan(Id acctId) {
        LLC_BI__Loan__c newL = new LLC_BI__Loan__c(
            Name = 'Test',
            LLC_BI__Account__c = acctId);
        
        insert newL;
        return newL;
    }
    
    private static Account createAccount() {
        Account newA = new Account(
            Name = 'Test');
        
        insert newA;
        return newA;
    }
    
    private static LLC_BI__Legal_Entities__c createLegalEntity(Id loanId, Id acctId) {
        LLC_BI__Legal_Entities__c newLE = new LLC_BI__Legal_Entities__c(
            LLC_BI__Loan__c = loanId,
            LLC_BI__Account__c = acctId);
        
        insert newLE;
        return newLE;
    }
    
    private static Contact createContact(Id acctId) {
        Contact newC = new Contact(
            FirstName = 'First',
            LastName = 'Last',
            Phone='1234567890',
            Contact_Email__c ='test@test.com',
            AccountId = acctId);
        
        insert newC;
        return newC;
    }
    
    private static LLC_BI__Contingent_Liabilty__c createAuthorizedSigner(id contactId, Id legalEntityId) {
        LLC_BI__Contingent_Liabilty__c newCL = new LLC_BI__Contingent_Liabilty__c(
            LLC_BI__Entity__c = legalEntityId,
            LLC_BI__Contact__c = contactId,
            LLC_BI__Authority__c = 'Authorized',
            LLC_BI__Role__c = 'Officer',
            LLC_BI__Contingent_Percentage__c = 10.00           
            );
        
        insert newCL;
        return newCL;
    }
}