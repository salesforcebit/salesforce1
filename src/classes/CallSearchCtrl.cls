public class CallSearchCtrl{

    public List<Participant__c> partList{get;set;}
    public List<Call__c> callList{get;set;}
    public Id contactId;
    public Id newContactId{get;set;}
    public Id selectUserId{get;set;}
    public String newContactName{get;set;}
    public String selectUserName{get;set;}
    public Id accountId;
    public Participant__c partRec {get;set;}
    public string privateVar{get;set;}
    public ApexPages.StandardSetController partSet{get;set;}
    public List<partWrap> partWrapList{get;set;}
    public Temp_Search_Object__c tempObj{get;set;}
    public Set<ID> callIDSet;
    public Id selectCnt{get;set;}
    public Boolean selectAllBool{get;set;}
    public Boolean selectBool{get;set;}
    public Boolean showButton{get;set;}
    public Boolean showOneButton{get;set;}
    Id contIDvar;
    Id userIdvar;
    public integer total_size{get;set;} 
    private Final Integer PAGE_SIZE =20;
    String firstNm,lastNm;
    public List<RecordType> recTypeList;
    Map<Id,String> recordTypeMap;
    public Boolean ultimateParCheck{get;set;}
    Id contactUserId;
    Boolean userExistCheck = false;
    Id conRTId;
    Public String racfIdStr;
    public Map<Id,partWrap> partWrapMap;
    
    public CallSearchCtrl(ApexPages.StandardController controller){
        selectAllBool = true;
        showButton = true;
        selectBool = true;
        showOneButton = true;
        callList = new List<call__c>();
        partWrapList = new List<partWrap>();
        partWrapMap = new Map<Id,partWrap>();
        partRec = new Participant__c();
        partList =  new List<Participant__c>();
        partSet = new ApexPages.StandardSetController(partList);
        system.debug('part:'+partRec);
        privateVar = '<b>Y</b>';
        tempObj = new Temp_Search_Object__c();
        //Recordtype for Contacts 
        Map<String, Schema.SObjectType> consObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType cons = consObjectMap.get('Contact') ;
        Schema.DescribeSObjectResult resSchemaCon = cons.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> conRecordTypeInfo = resSchemaCon.getRecordTypeInfosByName();
        conRTId = conRecordTypeInfo.get('Key Employee').getRecordTypeId();
        System.Debug('conRTId:'+conRTId);
    }
    
    public void ultimateParentBox(){
        system.debug('ultimateParCheck:'+ultimateParCheck);
        system.Debug('tempAcc:'+tempObj.Account_Id__c);
        if(tempObj.Account_Id__c == null && ultimateParCheck == true){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Select Account.'); 
            ApexPages.addMessage(errMsg);
        }
    }
    public void contactSelected(){
        String  temp = ApexPages.currentPage().getParameters().get('allSelect');
        List<partWrap> tempPartWrapperList = new List<partWrap>();
        System.Debug('temp:'+temp);
        if(selectAllBool == true){
            for(partWrap tempRec: partWrapList){
                tempRec.chkBox = true;
                partWrapMap.put(tempRec.callId,tempRec);
            }
            System.Debug('selected Call Added:'+partWrapMap.size());
            System.Debug('partWrapListcheck:'+partWrapList);
            selectAllBool = false;
            showOneButton = true;
        }
        
        else if(selectAllBool == false){
            System.debug('Remove code for call*****'+partWrapMap.size());
            for(partWrap tempRec: partWrapList){
                tempRec.chkBox = false;
                if(partWrapMap.containsKey(tempRec.callId)){
                    partWrapMap.remove(tempRec.callId);
                }
            }
            System.Debug('Call Record Removed from Map:'+partWrapMap.size());
                    
           // partWrapList = tempWrapList;
            System.Debug('partWrapListcheck:'+partWrapList.size());
            selectAllBool = true;
            showOneButton = false;
        }
        
    }
    
    public void singleContactSelected(){
        Id  temp = ApexPages.currentPage().getParameters().get('singleSelect');
        System.Debug('temp:'+temp);
        //if(selectBool == true){
            for(partWrap tempRec1: partWrapList){
                if(temp == tempRec1.callId && tempRec1.chkBox == true){
                    system.debug('tempRec1.callId:'+tempRec1.callId);
                    tempRec1.chkBox = false;
                    System.debug('Remove code for call*****'+partWrapMap.size());
                    if(partWrapMap.containsKey(tempRec1.callId)){
                        partWrapMap.remove(tempRec1.callId);
                        System.Debug('Call Record Removed from list:'+partWrapMap.size());
                    }
                }
                else if(temp == tempRec1.callId && tempRec1.chkBox == false){
                    system.debug('tempRec1.callId:'+tempRec1.callId);
                    tempRec1.chkBox = true;
                    partWrapMap.put(tempRec1.callId,tempRec1);
                    System.Debug('added to map');
                }
            }
        //}
    }
  
    public void accountSelected(){
        System.debug('account Selected:'+tempObj.Account_Id__c);
    } 
    
    public void searchcalls(){
        //contactId = partRec.Contact_Id__c;
        System.Debug('***selectUserId***:'+selectUserId);
        partWrapMap.clear();
        System.Debug('racfIdStr:'+racfIdStr);
        if(racfIdStr == null || racfIdStr ==''){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The selected Officer to do not have correct RacfId.Kindly provide one and then proceed.'); 
            ApexPages.addMessage(errMsg);
            if(!Test.isRunningTest()){ 
                return ;
            }
        }
        if(selectUserId != null ){
            accountId = tempObj.Account_Id__c;
            System.Debug('Selected Account:'+AccountId);  
            System.Debug('Ultimate parent Check Box:');
            String inQueryStr = '';
            List<Account> ultimatePrntChildList = new List<Account>();
            List<Account> ultimateChildList = new List<Account>();
            
            //String query = 'SELECT id,Name,ownerId,Call_Type__c,recordTypeId,Call_Date__c,Associated_Account__r.name,Associated_Account__c,Is_Private__c from Call__c';
            
            String query = 'Select id,CallId__r.Call_Date__c,CallId__r.Name,Contact_Id__c,CallId__c,Call_Type__c,Call_Date__c,CallId__r.Associated_Account__c,CallId__r.Associated_Account__r.name,CallId__r.Is_Private__c,Contact_Id__r.RACFID__c,CallId__r.ownerId,CallId__r.recordTypeId from Participant__c';
            system.debug('initial query : '+query);
            try{
                List<String> whereStrList = new List<String>();
                /*
                if(selectUserId != null){
                    whereStrList.add('ownerId =\''+selectUserId+'\'');   
                    System.debug('*Owner Value*');              
                } */
                if(racfIdStr != null){
                    whereStrList.add('Contact_Id__r.RACFID__c =\''+racfIdStr+'\'');    
                } 
                if(AccountId != null){
                    
                }
                if(ultimateParCheck == true){
                    ultimatePrntChildList = [SELECT Id, Name, Ultimate_Parent_AccountId__c FROM Account WHERE Ultimate_Parent_AccountId__c =: AccountId   LIMIT 50000];
                    System.Debug('ultimatePrntChildList:'+ultimatePrntChildList.size());
                    ultimateChildList = [SELECT id, Name, Ultimate_Parent_AccountId__c FROM Account WHERE id =: AccountId]; 
                    System.Debug('Ulitmate Child List:'+ultimateChildList[0].Ultimate_Parent_AccountId__c);
                    Set<Id> childAccIdSet = new Set<Id>();
                    if(ultimatePrntChildList != null && ultimatePrntChildList.size() > 0){
                        for(Account accRecord : ultimatePrntChildList){
                           childAccIdSet.add(accRecord.id);
                        }
                    }
                    Set<Id> tempIdSet = new Set<Id>();
                    List<Account> completeUltimateAcc = new List<Account>();
                    if(ultimateChildList != null && ultimateChildList.size()>0){
                        System.Debug('inside Loop ultimateChildList');
                        for(Account accRecord : ultimateChildList){
                            if(accRecord.Ultimate_Parent_AccountId__c != null){
                                System.Debug('accRecord.Ultimate_Parent_AccountId__c:'+accRecord.Ultimate_Parent_AccountId__c);
                                tempIdSet.add(accRecord.Ultimate_Parent_AccountId__c);
                            }
                        }
                        completeUltimateAcc = [SELECT Id, Name, Ultimate_Parent_AccountId__c FROM Account WHERE Ultimate_Parent_AccountId__c IN:tempIdSet LIMIT 50000];
                        System.debug('completeUltimateAcc:'+completeUltimateAcc);
                        System.debug('completeUltimateAccSize():'+completeUltimateAcc.size());
                    }
                    for(Account accRec : completeUltimateAcc){
                        childAccIdSet.add(accRec.Id);
                        childAccIdSet.add(accRec.Ultimate_Parent_AccountId__c);                        
                    }
                    for(Id tempId : childAccIdSet){
                        childAccIdSet.add(tempId);
                    }
                    childAccIdSet.add(AccountId);
                    for(Id recId : childAccIdSet){
                        inQueryStr = inQueryStr + ((inQueryStr=='' || inQueryStr==null)?'':'\',\'') +recId;
                    }   
                    System.debug('inQueryStr:'+inQueryStr);
                    whereStrList.add('CallId__r.Associated_Account__c IN (\''+inQueryStr+'\')');
                }
                else if(AccountId != null){
                    whereStrList.add('CallId__r.Associated_Account__c =\''+accountId+'\'');
                }
                if(privateVar != null){
                    whereStrList.add('CallId__r.Is_Private__c ='+true); // (privateVar == 'Y'?true:false)
                } 
                system.debug('whereStrList size :'+whereStrList.size());
                system.debug('whereStrList :'+whereStrList);
                String whereQuery = '';
                if(whereStrList!=null && whereStrList.size()>0){
                    for(String tempStr : whereStrList){
                        whereQuery = whereQuery + ((whereQuery=='' || whereQuery==null)?' ':' AND ') + '(' +tempStr + ')'; 
                    }
                    query = query + ' WHERE ' + whereQuery +' LIMIT 10000';
                }
                system.debug('inside if query :'+query);
                partSet = new ApexPages.StandardSetController(Database.getQueryLocator(query));
                System.debug('PartSet:'+partSet.getResultSize() );
                partSet.setPageSize(PAGE_SIZE);
                if(partSet == null) total_size=0;
                else{
                    if(partSet.getresultsize() == 10000){
                        ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'More than 10000 call records found, please provide more filter criteria.'); 
                        ApexPages.addMessage(errMsg);
                    }
                    else {
                        getPartListMethod();
                        total_size=partSet.getresultsize();
                    }
                }
                system.debug('partWrapList Size: After Method:'+partWrapList.size());
                selectAllBool = true;
                showButton = false;
                selectBool = true;
                showOneButton = false;
                
            } 
            catch(exception exp){
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select values to search calls.'); 
                ApexPages.addMessage(errMsg);
                system.debug('exception :'+exp.getMessage());
                system.debug('exception stack trace :'+exp.getStackTraceString() );
            } 
        }  
        else {
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Select Officer.'); 
            ApexPages.addMessage(errMsg);
        }
    }
    
    public List<partWrap> getPartListMethod(){
        try{
            recTypeList = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Call__c'];
            System.debug('recTypeList:'+recTypeList.size());
            recordTypeMap = new Map<Id,String>();
            System.Debug('recordTypeMap:'+recordTypeMap);
            

            for(RecordType recordtypeRec : recTypeList){
                recordTypeMap.put(recordtypeRec.Id,recordtypeRec.Name);
            }

            System.Debug('recordTypeMap:'+recordTypeMap);
            

            partWrapList = new List<partWrap>();
            if(partSet != null){
                for(Participant__c tempPartRec : (List<Participant__c>)partSet.getRecords()){
                    partWrap wrapRec = new partWrap(tempPartRec.CallId__c,tempPartRec.CallId__r.Name,recordTypeMap.get(tempPartRec.CallId__r.recordtypeId),
                        getFormatDate(tempPartRec.CallId__r.Call_Date__c),tempPartRec.CallId__r.Associated_Account__r.Name,false,firstNm,lastNm,tempPartRec.CallId__r.Name);
                    partWrapList.add(wrapRec);
                }
                system.debug('partWrapList Size:'+partWrapList.size());
                system.debug('partWrapMap Size:'+partWrapMap.size());
                
                for(partWrap tempWrapRec : partWrapList){
                    System.Debug('One more inside partWrapMap:'+partWrapMap.size());
                    System.Debug('tempWrapRec.callId:'+tempWrapRec.callId);
                    if(partWrapMap != null && partWrapMap.size() > 0){
                        System.Debug('One more inside partWrapMap:'+partWrapMap.size());
                        //System.Debug('call name+'+partWrapMap.get(tempWrapRec.callId).callName);
                        //system.debug('call name 2:'+tempWrapRec.callId);
                        if(partWrapMap.get(tempWrapRec.callId) != null && partWrapMap.get(tempWrapRec.callId).callId == tempWrapRec.callId)
                            tempWrapRec.chkBox = partWrapMap.get(tempWrapRec.callId).chkBox;
                        System.debug('End of loop:'+tempWrapRec.chkBox);
                    }
                } 
            }
            if(partWrapList == null || partWrapList.size()==0){
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.Error,'No Search Results'); 
                ApexPages.addMessage(errMsg);
            }   
            selectAllBool = true;
              
            return partWrapList;  
            
        }
        catch(Exception exp){
            System.Debug('exception occured:'+exp.getMessage());
            System.debug('Stack Trace:'+exp.getStacktraceString());
            return null;
        }  







    }
    
    public void selectCheckBox(){
        String obj = Apexpages.currentpage().getparameters().get('obj');
    }
    
    public void transferRecords(){
        try{
            callIDSet = new Set<Id>();
            if(partWrapMap != null && partWrapMap.size() > 0)
                callIDSet = partWrapMap.keySet();
            
           /* for(partWrap tempRec : partWrapList){
                if(tempRec.chkBox == true){
                    callIDSet.add(tempRec.callId);
                }
            } */

            system.debug('callIDSet:'+callIDSet);
            System.debug('newContactId:'+newContactId);
            String newContactIdStr = String.ValueOf(newContactId);
            System.debug('newContactIdStr:'+newContactIdStr);
            Boolean setcheck = true;
            Boolean newUserCheck = true;
            Boolean sameUserCheck = false;
            if(callIDSet == null || callIDSet.size()==0){
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select any call record to transfer.'); 
                ApexPages.addMessage(errMsg);
                System.Debug('errmsg:'+errmsg);
                setcheck = false;
            }
            if(newContactIdStr == null || newContactIdStr == ''){ // || String.isEmpty(newContactIdStr)
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select an Officer.'); 
                ApexPages.addMessage(errMsg);
                System.Debug('errmsg:'+errmsg);
                newUserCheck = false;
            }
            if(callIDSet != null){
                List<Call__c> tempCallList = new List<Call__c>();
                tempCallList = [SELECT Id, Name FROM Call__c WHERE Id IN :callIDSet];
                System.Debug('tempCallList:'+tempCallList+' Size:'+tempCallList.size());
            }
            System.Debug(contactUserId +' = '+selectUserId);
            if(newUserCheck == true && selectUserId != null && selectUserId != contactUserId ){
                if(setcheck == true && newUserCheck == true){
                    System.debug('Data insertion starts:'+callIDSet.size());
                    System.debug('partWrapList');
                    List<Participant__c> insertPartList = new List<Participant__c>();
                    Participant__c partInsertRec = new Participant__c();
                    
                    if(partWrapMap != null && partWrapMap.size() > 0){
                        for(Id idVar : partWrapMap.keySet()){
                            partInsertRec = new Participant__c();
                            partInsertRec.Contact_Id__c = newContactId;
                            partInsertRec.CallId__c = idVar;
                            partInsertRec.Did_Not_Participate__c = true;
                            System.debug('idVar'+idVar);
                            insertPartList.add(partInsertRec);
                        
                        }
                    }
                    
                  /*  for(partWrap tempRec : partWrapList){
                        if(tempRec.chkBox == true){
                            partInsertRec = new Participant__c();
                            partInsertRec.Contact_Id__c = newContactId;
                            partInsertRec.CallId__c = tempRec.callId;
                            partInsertRec.Did_Not_Participate__c = true;
                            System.debug('tempRec.callId:'+tempRec.callId);
                            insertPartList.add(partInsertRec);
                       }
                    }*/
                    

                    System.debug('insertPartList:'+insertPartList);
                    insert insertPartList;
                    ApexPages.Message infoMsg = new ApexPages.Message(ApexPages.Severity.Info,'Call records are updated'); 
                    ApexPages.addMessage(infoMsg);
                    partWrapMap.clear();
                    for(partWrap tempRec : partWrapList){
                        tempRec.chkBox = false;
                    }
                    selectAllBool = true;
                }
            }
            else{
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Call cannot be shared to the same Owner.'); 
                ApexPages.addMessage(errMsg);
                System.Debug('errmsg:'+errmsg);
            } 
        } 
        catch(exception exp){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage()); 
            ApexPages.addMessage(errMsg);
        }   
    }
    
    public String getFormatDate(Date tempDate){
        String tempStringDate ;
        if(tempDate != null){
            tempStringDate = ((String.valueOf((tempDate).Month()).length()<2)? '0'+String.valueOf((tempDate).Month()) : String.valueOf((tempDate).Month())) + '/' 
                         + ((String.valueOf((tempDate).Day()).length()<2)? '0'+String.valueOf((tempDate).Day()) : String.valueOf((tempDate).Day())) + '/' 
                         + tempDate.Year();
        }
        return tempStringDate;
    }
    
    public void fetchContactDetails(){
    System.debug('newContactId:'+newContactId);
        contIDvar = Apexpages.currentpage().getparameters().get('contID');
        newContactId = Apexpages.currentpage().getparameters().get('contID');
        System.debug('fetchContactDetails:'+contIDvar);
        System.debug(':newContactId:'+newContactId);
        Contact conRec = [SELECT id,User_Id__c from Contact Where id =: newContactId LIMIT 1];
        System.Debug('conRec User Id:'+conRec.User_Id__c );
        contactUserId = conRec.User_Id__c ;
        System.Debug(contactUserId +' = '+selectUserId);
        if(selectUserId != null && selectUserId == contactUserId){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Call cannot be shared to the same Owner.'); 
            ApexPages.addMessage(errMsg);
            System.Debug('errmsg:'+errmsg);
        }
    }
    
    public void fetchUserDetails(){
    System.debug('selectUserId:'+selectUserId);
        userIdvar = Apexpages.currentpage().getparameters().get('uId');
        selectUserId = Apexpages.currentpage().getparameters().get('uId');
        System.debug('fetchUserDetails:'+userIdvar);
        try{
            User selectUserValue = [SELECT ID,lastName,firstName,RACF_Number__c from USER where id =:selectUserId AND RACF_Number__c <> null  LIMIT 1];
            firstNm = selectUserValue.firstName;
            lastNm = selectUserValue.lastName;
            racfIdStr = selectUserValue.RACF_Number__c;
            System.Debug('User name:'+firstNm+' '+lastNm);
            System.debug('Racf Id found:'+racfIdStr);
        }
        catch(exception exp){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The selected Officer to do not have correct RacfId.Kindly provide one and then proceed.'); 
            ApexPages.addMessage(errMsg);
            System.Debug('Exception Query User:'+exp.getMessage());
            return ;
        }
    }
    
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            if(partSet == null) return false;
            return partSet.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious{
        get {
            if(partSet==null) return false;
            return partSet.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            if(partSet==null) return 0;
            return partSet.getPageNumber();
        }
        set;
    }

    public Integer getPageSize(){
        return PAGE_SIZE;
    }
    
    public Integer getTotalPages(){
        if(total_size == null || total_size==0) return 0;
        return Integer.valueOf(Math.ceil(Decimal.valueOf(total_size)/getPageSize()));
    }
    
    // returns the first page of records
    public void first() {
        System.Debug('partSet:'+partSet);
        partSet.first();
        getPartListMethod();
    }


    // returns the last page of records
    public void last() {
        partSet.last();
        getPartListMethod();
    }


    // returns the previous page of records
    public void previous() {
        partSet.previous();
        getPartListMethod();
    }


    // returns the next page of records
    public void next() {
        partSet.next();
        getPartListMethod();
    }
    

    public class partWrap{
        //public Id contactId{get;set;}
        public Id callId{get;set;}
        public String callName{get;set;}
        public String callType{get;set;}
        public String callDt{get;set;}
        public String accountName{get;set;}
        public Boolean chkBox{get;set;}
        public String partFirstName{get;set;}
        public String partLastName{get;set;}
        public String partName{get;set;}
        
        public partWrap(Id callId,String callName,String callType,String callDt,String accountName,Boolean chkBox,String partFirstName,String partLastName,String partName){
            //this.contactId = contactId;
            this.callId = callId;
            this.callName = callName;
            this.callType = callType;
            this.callDt = callDt;
            this.accountName = accountName;
            this.chkBox = chkBox;
            this.partFirstName = partFirstName;
            this.partLastName = partLastName;
            this.partName = partName;
        }
    }            
    
}