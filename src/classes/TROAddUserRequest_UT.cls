@isTest
private class TROAddUserRequest_UT {

  static testMethod void addUserRequestTest() {
  	TROAddUserRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_addUserRequest_Port testAddUser = new TROAddUserRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_addUserRequest_Port();

  	TROAddUserRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_AddUser_ExportFields();

  	Test.setMock(WebServiceMock.class, new TRO_AddUserRequest_Mock());

		Test.startTest();
  	TROAddUserRequest_v2.KTTResponse response = testAddUser.processAddUserAPI(exportFields);
		Test.stopTest();

		System.assertEquals(response.CSOAutomation.KTTResponse.status, 'Good to go');

  }

    static testMethod void aSyncAddUserRequestTest() {
        AsyncTROAddUserRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_addUserRequest_Port aSyncAddAccount = new AsyncTROAddUserRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_addUserRequest_Port();

        TROAddUserRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_AddUser_ExportFields();
        Continuation con = new Continuation(120);
        con.continuationMethod = 'processResponse';

        Test.setMock(WebServiceMock.class, new TRO_AsyncAddUserRequest_Mock());

        AsyncTROAddUserRequest_v2.processAddUserAPIResponseFuture response = aSyncAddAccount.beginProcessAddUserAPI(con, exportFields);

    }
}