@isTest
private class Test_FundingGridTrigger 
{
    static testMethod void defaultPaymentMethodTest() 
    {
        List<Account> accList = TestUtilityClass.CreateTestAccount(1);
        insert accList;
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.BillingStreet = 'BillingStreet';
        acc.BillingCity = 'BillingCity';
        acc.BillingState = 'BillingState';
        acc.BillingPostalCode = 'BillingPostalCode';
        acc.BillingCountry = 'BillingCountry';
        acc.Phone = '7030357222';acc.KEF_Risk_Role__c = System.Label.Vendor_Supplier;
        acc.Vendor_Status__c = System.Label.Pending_Credit;
        insert acc;
        
        Test.StartTest();
        
        List<Funding_Grid__c> fgList = new List<Funding_Grid__c>();
        //inserting multiple fundng grid records under an account.
        for(Integer tmp = 0; tmp < 5; tmp ++)
        {
            fgList.add(TestUtilityClass.createFundingGrid(acc.id));
        }
        insert fgList;
        
        //unchecking the Default Payment method checkbox funding grid record under an account. THis should throw an error.  
        try
        {
            Funding_Grid__c fg = new Funding_Grid__c();
            fg = fgList[0];        
        
            fg.Default_payment_method__c = false;
            update fg;
            Map<id, String> errorMap = new Map<id, String>();
            errorMap.put(fg.id,'Insertion Failed');
            Map<Id, Funding_Grid__c> fgMap = new Map<Id, Funding_Grid__c>();
            fgMap.put(fg.id,fg);
            FundingGridHelper.showErrorOnScreen(errorMap,fgMap);
        
        }
        catch(DMLException dmlEx)
        {
            system.debug('dmlEx.getDMLMessage>>>>      '+dmlEx);
            System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION,dmlEx.getDmlType(0));
        }
        
        //inserting a new Funding Grid record which will be marked as default payment method 
        insert TestUtilityClass.createFundingGrid(acc.id);        
        
        Test.StopTest();
    }
}