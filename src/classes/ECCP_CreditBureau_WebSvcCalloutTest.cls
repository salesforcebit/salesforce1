@isTest
public class ECCP_CreditBureau_WebSvcCalloutTest {
    public static testmethod ECCP_CreditBureauResponse CBTest(){
    
        Test.setMock(WebServiceMock.class, new ECCP_CreditBureau_WebServiceMockImpl());
        
        ECCP_CreditBureauRequest TestReq = new ECCP_CreditBureauRequest();
        TestReq.SSN = '123456' ;
        TestReq.Orchestration_Code = '00011';
        TestReq.AccountName = 'Test Account';
        
        //ECCP_CreditbureauWS.ECCP_Credit_Bureau_Type CBres = ECCP_CreditBureau_WebSvcCallout.CreditBureauCallout(TestReq);
        
       ECCP_CreditBureauResponse CBres = ECCP_CreditBureau_WebSvcCallout.CreditBureauCallout(TestReq);
       return CBres;
   }
   /*
    public static testmethod ECCP_CreditBureauResponse CBTestExc(){
    
        Test.setMock(WebServiceMock.class, new ECCP_CreditBureau_WebServiceMockImplExc());        
        ECCP_CreditBureauRequest TestReq = new ECCP_CreditBureauRequest();
        TestReq.SSN = '123456' ;
        TestReq.Orchestration_Code = '00011';
        TestReq.AccountName = 'Test Account';        
        ECCP_CreditBureauResponse CBres = ECCP_CreditBureau_WebSvcCallout.CreditBureauCallout(TestReq);                  
        return CBres;
       }     
       */
          
}