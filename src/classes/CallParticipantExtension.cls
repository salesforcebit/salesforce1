//========================================================================
//  KeyBank
//  Object: Call & Participant Extension
//  Author: Offshore
//  Detail: Controller (Edit page) for Visualforce page - CallCustom 
//========================================================================

public with sharing class CallParticipantExtension 
{
    
    public Contact[] AvailableParticipants {get;set;}
    public Participant__c[] shoppingCart{get; set;}
    public Participant__c[] total{get; set;}
    public Call__Share[] callshare{get;set;}
    public Call__c theCall {get; set;}
    public User theUser {get; set;}
    public contact[] cRACF {get; set;}
    public AccountTeamMember[] atRACF {get; set;}
    public String toSelect{get; set;}
    public String tocSelect{get; set;}
    public String toatmSelect{get; set;}
    public String toUnselect{get; set;}
    //public String totUnselect{get;set;}
    public string warningRACF{get;set;} //Warning Racf message variable SS(6/10/15)
    public String searchString{get; set;}
    //public String value{get; set;}
    //added to test edit functionality
    public Boolean bEdit{get;set;}
    public string uid{get;set;}
    public Id profileId{get;set;}
   
    set<string> myString = new Set<String>{'%Sales%'};
    set<string> myStringA = new Set<String>{'%Admin%'};
    
    public Map<Id,user> usermap =new Map<Id,user>([Select Id,RACF_Number__c from user where IsActive = True]);
    Set<String> userNames = new Set<String>();
    private Participant__c[] forDeletion = new Participant__c[]{};
    public List<Call__Share> uslist =new list<Call__Share>();
    
     
    private ApexPages.StandardController controller;
    
    //Constructor
    
    public CallParticipantExtension (ApexPages.StandardController controller)
    {
            
        this.controller= controller;
        bEdit=true;
        if(controller.getrecord() != null)
        
        {
        String value = ApexPages.currentPage().getParameters().get('Id');
          
            theCall =[Select id, name, ownerid, AccountId__c from call__c where id=:controller.getRecord().id limit 1];
            cRACF = [select id, Name,RACFID__c,Contact_Type__c, Title, Contact.MailingCity,Contact.MailingState,Contact.Account.Name from Contact where AccountId =: theCall.AccountId__c and id not in (select Contact_Id__c from Participant__c where CallId__c =: theCall.id )order by Name];
            system.debug(+theCall.AccountId__c);
            system.debug(+toSelect);
            system.debug(+tocSelect);
            atRACF = [select UserId,User.Isactive, id, AccountId from AccountTeamMember where AccountId =: theCall.AccountId__c and User.Isactive=true order by UserId];
            total =[Select id,name,Contact_Id__c,CallId__c,Call_Date__c,Participant__c.Contact_Id__r.Name,Participant__c.Contact_Id__r.User_Id__c from participant__c where CallId__c =: theCall.Id];
            shoppingCart =[Select id,name,Contact_Id__c,CallId__c, Call_Date__c,Participant__c.Contact_Id__r.Name from participant__c where id =: toSelect]; 
            system.debug(theCall.Id);
            callshare =[select id,AccessLevel,ParentId,UserOrGroupId from Call__Share where ParentId =: theCall.Id];
            
            system.debug(callshare);
            system.debug(shoppingCart);
           
        }
         //updateAvailableList();
         
         //Populate Call Variable Custom Setting & get warning message SS(6/12/15)
        
        CallVF__c cVFcs = CallVF__c.getInstance(keyGV.vfCallNew);
        warningRACF = cVFcs.Warning_RACFID__c;
         
     }
          
    public void updateAvailableList() 
    {
        
        String qString =  'select id,Name,RACFID__c,Contact_Type__c, Title,Contact.MailingCity,Contact.MailingState,Contact.Account.Name from Contact where id not in (select Contact_Id__c from Participant__c where CallId__c = \'' + theCall.id + '\')' ;
        if(searchString!=null)  
        
        {          
            qString+= ' and( Contact.Name like \'%' + searchString + '%\' or Contact.RACFID__c like \'%' + searchString + '%\' or Contact.Officer_Code__c like \'%' + searchString + '%\')' ;                       
        }
      Set<Id> selectedEntries = new Set<Id>();
        for(Participant__c d:shoppingCart){
            selectedEntries.add(d.Contact_Id__c);
        }
        
        if(selectedEntries.size()>0){
            String tempFilter = ' and id not in (';
            for(id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
           qString+= extraFilter;
        }
        
        qString+= ' order by Name';
        qString+= ' limit 12';
        system.debug('qString:' +qString );
        if(searchString != null)
        {
            AvailableParticipants = database.query(qString);
        }
        
    } 
    
    
    public PageReference actionBeforeload()
    {
       profileId=userinfo.getProfileId();
       String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
       theUser =[Select Id, ProfileId from user where Id =: theCall.ownerid];
       String profileNameU=[Select Id,Name from Profile where Id=:theUser.profileId].Name;
       Map<String, List<Profile>> mapProfile= new Map<String, List<Profile>>();
       Map<Id, List<Participant__c>> mapParti= new Map<Id, List<Participant__c>>();
       system.debug(profileName);
       system.debug(theCall.ownerid);
       system.debug(theUser);
       system.debug(profileNameU);
       system.debug(total);
       
       if ((theCall.ownerid != UserInfo.getUserId() && profileName == 'System Administrator') || (theCall.ownerid != UserInfo.getUserId() && profileName == 'REC Admin')
        || (theCall.ownerid != UserInfo.getUserId() && profileName == 'MM Admin') || (theCall.ownerid != UserInfo.getUserId() && profileName == 'KBCM Admin')
        || (theCall.ownerid == UserInfo.getUserId() && profileName == 'Bank Admin') || (theCall.ownerid != UserInfo.getUserId() && profileName == 'ECP Admin'))

       { 
           bEdit=true;
           system.debug(bEdit);
       }
       if(theCall.ownerid != UserInfo.getUserId())
       {
        for(Call__Share cs : callshare)
        {
            if(cs.UserOrGroupId == UserInfo.getUserId())
            {
                uslist.add(cs);
                uid = cs.AccessLevel;
                system.debug(uslist);
                system.debug(uid);
                //set<string> myStringA = new Set<String>{'%Admin%'};
                for(Profile profA: [SELECT Id,Name FROM Profile where id =: profileId and (not name LIKE :myStringA)])
                if((!mapProfile.containsKey(profA.Name)) && (uid == 'Read'))
                {
                   system.debug('test loop6');
                   bEdit=false;
                   ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Users with read only access cannot edit the call'); 
                   ApexPages.addMessage(errMsg);
                }    
            }
        }
            
        //set<string> myString = new Set<String>{'%Sales%'};
        for(Profile prof: [SELECT Id,Name FROM Profile where id =: profileId and name LIKE :myString])
        {   
            system.debug(prof);
            system.debug(uid);
            system.debug(UserInfo.getUserId());
            if((!mapProfile.containsKey(prof.Name)) && (uid == 'Read'))
            {
                   system.debug('test loop7');
                   bEdit=false;
                   ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Users with read only access cannot edit the call'); 
                   ApexPages.addMessage(errMsg);
            }       
                   
        }
        }
    //return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    return null;
    } 
    
    
    // This function runs when a user hits "select" button next to a Participant
    
    public void addToShoppingCart()
    {
    
        for(Contact par : AvailableParticipants)
        {
        if(((String)par.id==toSelect && par.RACFID__c != null) || ((String)par.id==toSelect && par.Contact_Type__c == 'Business Contact'))
            {
                shoppingCart.add(new participant__c(Contact_Id__c=par.id, CallId__c = theCall.Id));
                system.debug(shoppingCart.size());
                break;
            } 
        
        if((String)par.id==toSelect && par.RACFID__c == null && par.Contact_Type__c == 'Key Employee')
            {
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF); 
                ApexPages.addMessage(errMsg);
            }
        }
        
       updateAvailableList();  
           
    }  
    
    
    public void addToShoppingCartAcontact()
    {
    
        for(Contact parts : cRACF )
        {
        if(((String)parts.id==tocSelect && parts.RACFID__c != null) || ((String)parts.id==tocSelect && parts.Contact_Type__c == 'Business Contact'))
            {
                shoppingCart.add(new participant__c(Contact_Id__c=parts.id, CallId__c = theCall.Id));
                system.debug(shoppingCart.size());
                break;
            } 
        
        if((String)parts.id==tocSelect && parts.RACFID__c == null && parts.Contact_Type__c == 'Key Employee')
            {
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF); 
                ApexPages.addMessage(errMsg);
            }
            
        }
        
       updateAvailableList();  
           
    }
    
    
   public void addToShoppingCartAtm()
   {
    
        for(AccountTeamMember partsatm : atRACF )
         {
            system.debug(partsatm.UserId);
            if(partsatm.UserId != null)
            {
                User us = usermap.get(partsatm.UserId);
                userNames.add(us.RACF_Number__c);
                system.debug(userNames);
                
            }
         }
         
          if(userNames.size() > 0)
        {
            
            //List<Contact> userNameContacts = [Select Id, RACFID__c from Contact where RACFID__c IN : userNames and Contact_Type__c='Key Employee' and Salesforce_User__c=true];
            List<Contact> userNameContacts = [Select Id, RACFID__c from Contact where RACFID__c IN : userNames and Contact_Type__c='Key Employee'];
            {
                for(AccountTeamMember partsatm : atRACF )
         
          {
             
             User us = usermap.get(partsatm.UserId);
             system.debug(partsatm.UserId);
              Contact foundContact;
                 for(Contact contact : userNameContacts)
                 {
                      if(contact.RACFID__c == us.RACF_Number__c)
                      {
                            foundContact = contact;
                            system.debug(foundContact);
                            break;
                      }
                 }
                 system.debug(partsatm.id);
                 system.debug(toatmSelect);
                 //system.debug(foundContact.RACFID__c);
                 
                 if(foundContact != null && partsatm.id==toatmSelect && partsatm.User.Isactive == true && foundContact.RACFID__c != null)
                 {
                      Participant__c  partatm = new Participant__c();
                      partatm.CallId__c = theCall.Id;
                      partatm.Contact_Id__c = foundContact.Id;
                      shoppingCart.add(partatm);
                      system.debug(partatm);
                 }
                 else if(foundContact != null && partsatm.id==toatmSelect && partsatm.User.Isactive == true && foundContact.RACFID__c == null)
                 {
                     system.debug('Else1 found-'  +foundContact );
                     ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF); 
                     ApexPages.addMessage(errMsg);
                 }
                 else if(foundContact != null && partsatm.id==toatmSelect && partsatm.User.Isactive == false && foundContact.RACFID__c != null)
                 {
                      system.debug('Else2 found-'  +foundContact );
                      ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The Selected User is Inactive'); 
                      ApexPages.addMessage(errMsg);   
                 }
                 else if(partsatm.id==toatmSelect && foundContact == null)
                 {
                     system.debug('Else3 found-'  +foundContact );
                     ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No KeyEmployee Contact For Selected User'); 
                     ApexPages.addMessage(errMsg);
                 } 
                 else
                 {
                 }
         
                
        }
        
       updateAvailableList();  
           
    }
    
   }
 }
    
      public PageReference removeFromShoppingCart()
      {
    
        // This function runs when a user hits "remove" on "Selected Participant" section
    
        Integer count = 0;
    
        for(Participant__c del : shoppingCart){
            if((String)del.Contact_Id__c==toUnselect){
            
                if(del.Id!=null)
                    forDeletion.add(del);
            
                shoppingCart.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }
    
     // This function runs when user hits save button
     
     public PageReference onSave(){
             
        try{
            PageReference pageRef = controller.save();
                        
            if(shoppingCart.size()>0)
                {
                Set<Participant__c> nonDuplicates = new Set<Participant__c>(shoppingCart);  
                system.debug(nonDuplicates);
                List<Participant__c> listStrings = new List<Participant__c>(nonDuplicates);
                system.debug(listStrings);
                //npshoppingCart.add(nonDuplicates);
                insert(listStrings); 
                }  
                   
           }
        
            catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
           
        // After save return the user to the CallPage
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
        
    }     
}