/*******************************************************************************************************************************************
* @name         :   ECCP_IntegrationConstants
* @description  :   This class is used to declare the constant.
* @author       :   Nagaarjuna Lakkoju
* @createddate  :   17/03/2016
*******************************************************************************************************************************************/

public class ECCP_IntegrationConstants{
    public static final string AuwService = 'ECCP_AuwCreditProcess';
    public static final string AuwProcessRequest = 'AUW Credit Card Processing'; 
    // AUW Credit Card Limit Increase Constants Start
    public static final string AuwCreditLimitIncrease = 'ECCP_AUWCreditCardLimitIncrease';
    public static final string MethodName = 'populateResponseObject';
    public static final string MethodName2 = 'ECCP_AuwCreditLinitIncreaseProcess';
    // AUW Credit Card Limit Increase Constants End
    // MCA inboud service constants start
    public static final string MCAInService = 'ECCP_MCA_WSIN_Service'; 
    public static final string MCAProcessRequest = 'MCA Compliance Check'; 
    public static final string updateMethod ='updateLoan';
    public static final string updateLoanReq = 'updateLoanReq';
    public static final string MCAClassName = 'ECCP_McaLoanProcessUpdatelogic';
    // MCA inboud service constants end
    //AUW Inbound start
    public static final string AUWInService = 'ECCP_AUW_WSIN_Service'; 
    public static final string AUWInProcessRequest = 'AUW Update Credit Request';    
    public static final string NullValue = NULL;  
    public static final string InboundNullInput = 'Input is Empty';  
    public static final string NoRecordFound = 'Empty Query Result'; 
    public static final string pending= 'Can not Update Compliance Record if Existing MCA status equals Pass/Fail'; 
    public static final string AUWupdateMethod ='updateLoanReq';
    public static final string AUWupdateLoanReq = 'updateLoanReq';
    public static final string AUWClassName = 'ECCP_AUWLoanProcessUpdatelogic';   
    public static final string NullResponseMessage = 'Response from the service is null. Check the input provided';
    //AUW Inbound end
    public static final string InBound= 'InBound'; 
    // Error Code Constants start
    public static final string nullResponse = 'ECCP_E009';   
    public static final String responseHandler = 'ECCP_E010';
    public static final String updateFail = 'ECCP_E005';
    public static final String nullInput ='ECCP_E002';
    public static final String dmlException = 'ECCP_E007';
    public static final string Success= 'Success';      
    public static final string Error= 'Error'; 
    //Error code constants end
    // MCA check out bound service Constants Start
    public static final string MCAObServiceStub = 'ECCP_MCAObServiceStub';
    public static final string MCACheckMCIWS = 'ECCP_MCACheckMCIWS';  
    public static final string Facing = 'Facing';  
    public static final string NonFacing = 'NonFacing';  
    public static final string LOB = 'BUSBANKCR';  
    // MCA check out bound service Constants End
    
    // Credit Bureau constants
    public static final string serviceMatadaEquifex = 'Equifex';
    public static final string serviceCreditBureau = 'CreditBureau Outbound';
    public static final String CreditBureauMataData ='ECCP_CreditBureau';
    public static final string CreditBureauProcessRequest = 'Credit Bureau Request';
    public static final string SoleProprietorship ='Sole Proprietorship';
    public static final string CreditScore = '995';

    public static final string Err01 = 'ECCP_INT_0001';
    public static final string Err02 = 'ECCP_INT_0002';
    public static final string Err03 = 'ECCP_INT_0003';
    public static final string Err04 = 'ECCP_EXT_0004';
    public static final string Err05 = 'ECCP_INT_0005';
    public static final string Err06 = 'ECCP_INT_0006';
    public static final string Err07 = 'ECCP_INT_0007';
    

    
    //MCA call out trigger
    public static final string MCAStatus= 'Appeal Approved';
    public static final string BatchorRealTime ='P';
    public static final string channel ='ECC';
    public static final string accBusinessRecordType = 'Business';
    public static final string accIndividualRecordType = 'Individual';
    public static final string custTypeBusiness ='ORG';
    public static final string custTypeIndividual ='IND';
    public static final string ProdCd ='BBCEXPRESS'; 
    public static final string serviceMCA = 'MCA Check OutBound';
   
    //DSC Customer
    public static final String DSCCustomerMataData ='ECCP_DSCCustomer';
    public static final String productLine = 'CMT';
    public static final String operationInsert = 'Insert';
    public static final String operationUpdate = 'Update';
    public static final String depositSmall = 'deposit';
    public static final String depositCamel = 'Deposit';
    public static final String AccName = 'Name';
    
    /* LPL Service Variables */
    public static final String LPLService ='ECCP_LPLWS';
    public static final String Guarantor='Guarantor';
    public static final String Borrower='Borrower';
    //Syed:adding Signor's grantor logic ver2.2
    public static final String Grantor='Grantor';
    public static final String Signer='Signer';   
    public static final String Auth='Auth';  
    public static final String DirectAccount ='DirectAccount';
    public static final String ServiceNa='Service Not Available';  
    public static final String LPLStatus='status';  
    public static final String LPLpkgID='LPL Package ID';    
    
    
     
}