@istest
Public class Test_CallParticipantExtensionNew
{
     public static testMethod void theTests()
    {
        
        
        Profile p = [select id from profile where name='Bank Admin'];
        Record_Type_Name__c Accrt = Record_Type_Name__c.getValues('AccRecordTypeID');
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan1@keybank.com',
           emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',RACF_Number__c='HINDF',
           timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan123@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
        
        Account acc = new Account(name ='Acc Test');
        insert acc;
        Accountteammember at = new Accountteammember(AccountId=acc.id,TeamMemberRole='Banker',UserId=testUser.id);
        insert at;
        contact con = new contact(LastName='Mr. Dhanesh Subramanian',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc.id,Salesforce_User__c=true,User_Id__c=testUser.Id,Phone='9876543210' );
        insert con;  
        recordtype rt1=[select id  from recordtype where name='Account Call'];  
        Call__c c = new Call__c(Subject__c='test',Call_Date__c=system.today(),Call_Type__c='COI Contact',RecordTypeId=rt1.id,AccountId__c=acc.id,Status__c='done');
        insert c;
        Participant__c part =new Participant__c(CallId__c=c.id,Contact_Id__c=con.id);
        insert part;
         //custom setting insertion
          CallVF__c cVF = new CallVF__c(name='CallCustomNew',Default_Label__c='Please select RecordType',Warning_Prompt__c='NOTE: Please enter mandatory fields before adding Participants from the below search list.',Warning_RACFID__c='RACF-ID is Empty for last selected participant.');
          insert cVF;
        Record_Type_Name__c rtn = new Record_Type_Name__c(name='AccRecordTypeID',Record_Type__c=rt1.id);
        insert rtn;
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        CallParticipantExtensionNew e = new  CallParticipantExtensionNew(sc);        
        string crecType = keyGV.callRTAccount;     
        CallVF__c cVFcs = CallVF__c.getInstance(keyGV.vfCallNew);
        List<SelectOption> rt = e.callRTs;  
        e.setRecordType();
        Integer startCount = e.ShoppingCart.size();
                  
        e.searchString = 'Dhanesh';
        e.updateAvailableList();
              
        e.tonSelect= e.AvailableParticipants[0].Id;
        e.addToShoppingCart();
        e.updateAvailableList();
        
        e.toUnselect = e.ShoppingCart[0].id;
        //e.forDeletion[0].id=part.id;
        e.removeFromShoppingCart();
        e.updateAvailableList();
        
        e.toatmSelect=at.id;
        e.AtmRef = [select id, UserId, AccountId,Account.Contact_Name__c,user.Isactive from AccountTeamMember where AccountId =: acc.id order by UserId]; 
        System.assert(e.AtmRef.size()>0);
        List<Contact> userNameContacts = [Select Id, Name from Contact where Name =: testUser.NAME and Contact_Type__c='Key Employee' and Salesforce_User__c=true];
        e.AccountTeamMemberList();
        e.ConRef = [select Id,Name, AccountId,Title,RACFID__c, Contact.MailingCity,Contact.MailingState,Contact.Account.Name from contact where AccountId =:acc.id order by Name];
        e.tocSelect=con.id; 
        System.assert(e.ConRef.size()>0);       
        e.ContactList();
        e.InVFPage();
        if(crecType == keyGV.callRTAccount) 
        { 
        e.newCall.id=c.id;
        }

        //System.assert(Accrt.Record_Type__c =='012o0000000OBtE');   
        e.onSave();   
    }
    public static testMethod void theTests1()
    {
        Profile p = [select id from profile where name='Bank Admin'];
        Record_Type_Name__c Accrt = Record_Type_Name__c.getValues('OppRecordTypeID');
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan13@keybank.com',RACF_Number__c='HINDF',
           emailencodingkey='UTF-8', lastname='Testing2016', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan2016@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
        
        opportunity opty = new opportunity(name ='optyTest',RecWeb_Number__c = '12589', StageName='Pursue', CloseDate=date.today());
        insert opty;
        Account acc1 = new Account(name ='test12');
        insert acc1;
        contact con = new contact(LastName='Mr. Dhanesh Subramanian',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc1.id,Salesforce_User__c=true,User_Id__c=testUser.Id,Phone='9876543210' );
        insert con;  
        recordtype rt1=[select id  from recordtype where name='Opportunity Call'];  
        Call__c c = new Call__c(Subject__c='test',Call_Date__c=system.today(),Call_Type__c='COI Contact',RecordTypeId=rt1.id,OpportunityId__c=opty.id,Status__c='done');
        insert c;
        Participant__c part =new Participant__c(CallId__c=c.id,Contact_Id__c=con.id);
        insert part;
         //custom setting insertion
          CallVF__c cVF = new CallVF__c(name='CallCustomNew',Default_Label__c='Please select RecordType',Warning_Prompt__c='NOTE: Please enter mandatory fields before adding Participants from the below search list.',Warning_RACFID__c='RACF-ID is Empty for last selected participant.');
          insert cVF;
          Record_Type_Name__c rtn = new Record_Type_Name__c(name='OppRecordTypeID',Record_Type__c=rt1.id);
        insert rtn;
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        CallParticipantExtensionNew e = new  CallParticipantExtensionNew(sc);        
        string crecType = keyGV.callRTOpportunity;     
        CallVF__c cVFcs = CallVF__c.getInstance(keyGV.vfCallNew);
        List<SelectOption> rt = e.callRTs;  
        e.setRecordType();
        Integer startCount = e.ShoppingCart.size();
                  
        e.searchString = 'Dhanesh';
        e.updateAvailableList();
              
        e.tonSelect= e.AvailableParticipants[0].Id;
        e.addToShoppingCart();
        e.updateAvailableList();
        
        e.toUnselect = e.ShoppingCart[0].id;
        e.removeFromShoppingCart();
        e.updateAvailableList();
        
        
        if(crecType == keyGV.callRTOpportunity) 
        { 
        e.newCall.id=c.id;
        }
        e.onSave();   
    }
    public static testMethod void theTests2()
    {
        Profile p = [select id from profile where name='Bank Admin'];
        Record_Type_Name__c Accrt = Record_Type_Name__c.getValues('LeaRecordTypeID');
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan14@keybank.com',RACF_Number__c='HINDF',
           emailencodingkey='UTF-8', lastname='Testing2019', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan1243@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
        
        Lead leadr= new Lead(Lastname ='lead test', company = 'sunil');
        insert leadr;
        Account acc2 = new Account(name ='test123');
        insert acc2;
        contact con = new contact(LastName='Mr. Dhanesh Subramanian',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc2.id,Salesforce_User__c=true,User_Id__c=testUser.Id,Phone='9876543210' );
        insert con; 
        recordtype rt1=[select id  from recordtype where name='Lead Call'];  
        Call__c c = new Call__c(Subject__c='test',Call_Date__c=system.today(),Call_Type__c='COI Contact',RecordTypeId=rt1.id,LeadId__c=leadr.id,Status__c='done');
        insert c;
        Participant__c part =new Participant__c(CallId__c=c.id,Contact_Id__c=con.id);
        insert part;
              //custom setting insertion
          CallVF__c cVF = new CallVF__c(name='CallCustomNew',Default_Label__c='Please select RecordType',Warning_Prompt__c='NOTE: Please enter mandatory fields before adding Participants from the below search list.',Warning_RACFID__c='RACF-ID is Empty for last selected participant.');
          insert cVF;
          Record_Type_Name__c rtn = new Record_Type_Name__c(name='LeaRecordTypeID',Record_Type__c=rt1.id);
        insert rtn;
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        CallParticipantExtensionNew e = new  CallParticipantExtensionNew(sc);        
        string crecType = keyGV.callRTLead;     
        CallVF__c cVFcs = CallVF__c.getInstance(keyGV.vfCallNew);
        List<SelectOption> rt = e.callRTs;  
        e.setRecordType();
        Integer startCount = e.ShoppingCart.size();
                  
        e.searchString = 'Dhanesh';
        e.updateAvailableList();
              
        e.tonSelect= e.AvailableParticipants[0].Id;
        e.addToShoppingCart();
        e.updateAvailableList();
        
        e.toUnselect = e.ShoppingCart[0].id;
        //e.forDeletion[0].id=part.id;
        e.removeFromShoppingCart();
        e.updateAvailableList();
        
        
        if(crecType == keyGV.callRTLead) 
        { 
        e.newCall.id=c.id;
        }
        e.onSave();   
    }
    public static testMethod void theTests3()
    {
        Profile p = [select id from profile where name='Bank Admin'];
        Record_Type_Name__c Accrt = Record_Type_Name__c.getValues('ConRecordTypeID');
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan16@keybank.com',RACF_Number__c='HINDF',
           emailencodingkey='UTF-8', lastname='Testing342', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan1365@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
        
        
        Account acc3 = new Account(name ='test1234');
        insert acc3;
        contact con = new contact(LastName='Mr. Dhanesh Subramanian',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc3.id,Salesforce_User__c=true,User_Id__c=testUser.Id,Phone='9876543210' );
        insert con; 
        recordtype rt1=[select id  from recordtype where name='Contact Call'];  
        Call__c c = new Call__c(Subject__c='test',Call_Date__c=system.today(),Call_Type__c='COI Contact',RecordTypeId=rt1.id,ContactId__c=con.id,Status__c='done');
        insert c;
        Participant__c part =new Participant__c(CallId__c=c.id,Contact_Id__c=con.id);
        insert part;
              //custom setting insertion
          CallVF__c cVF = new CallVF__c(name='CallCustomNew',Default_Label__c='Please select RecordType',Warning_Prompt__c='NOTE: Please enter mandatory fields before adding Participants from the below search list.',Warning_RACFID__c='RACF-ID is Empty for last selected participant.');
          insert cVF;
          Record_Type_Name__c rtn = new Record_Type_Name__c(name='ConRecordTypeID',Record_Type__c=rt1.id);
        insert rtn;
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        CallParticipantExtensionNew e = new  CallParticipantExtensionNew(sc);        
        string crecType = keyGV.callRTContact;     
        CallVF__c cVFcs = CallVF__c.getInstance(keyGV.vfCallNew);
        List<SelectOption> rt = e.callRTs;  
        e.setRecordType();
        Integer startCount = e.ShoppingCart.size();
                  
        e.searchString = 'Dhanesh';
        e.updateAvailableList();
              
        e.tonSelect= e.AvailableParticipants[0].Id;
        e.addToShoppingCart();
        e.updateAvailableList();
        
        e.toUnselect = e.ShoppingCart[0].id;
        //e.forDeletion[0].id=part.id;
        e.removeFromShoppingCart();
        e.updateAvailableList();
        
        
        if(crecType == keyGV.callRTContact) 
        { 
        e.newCall.id=c.id;
        }
        e.onSave();   
    }
    
    public static testMethod void theTests4()
    {
        Profile pc = [select id from profile where name='System Administrator'];
        Record_Type_Name__c Accrtc = Record_Type_Name__c.getValues('ConRecordTypeID');
        User testUserc = new User(alias = 'u1', email='vidhyasagaran_muralidharan126@keybank.com',RACF_Number__c='HINDFC',
           emailencodingkey='UTF-8', lastname='Testing3424', languagelocalekey='en_US',
           localesidkey='en_US', profileid = pc.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan13657@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUserc;
        
        
        Account acc4 = new Account(name ='test12345');
        insert acc4;
        contact conc = new contact(LastName='Mr. Dhanesh Subramanianc',RACFID__c='HINDFC', Officer_Code__c='HINDFC',Contact_Type__c='Key Employee',AccountId=acc4.id,Salesforce_User__c=true,User_Id__c=testUserc.Id,Phone='9876543210' );
        insert conc; 
              //custom setting insertion
          CallVF__c cVF = new CallVF__c(name='CallCustomNew',Default_Label__c='Please select RecordType',Warning_Prompt__c='NOTE: Please enter mandatory fields before adding Participants from the below search list.',Warning_RACFID__c='RACF-ID is Empty for last selected participant.');
          insert cVF;
        recordtype rt4=[select id  from recordtype where name='Contact Call'];  
        Call__c cc = new Call__c(Subject__c='testc',Call_Date__c=system.today(),Call_Type__c='COI Contact',RecordTypeId=rt4.id,ContactId__c=conc.id,Status__c='done');
        insert cc;
        
        if(conc.RACFID__c == testUserc.RACF_Number__c && conc.RACFID__c != null && testUserc.RACF_Number__c != null)
        {
            Contact foundContact;
            foundContact = conc;
        
        if(foundContact != null)
        {
            Participant__c  partc = new Participant__c();
            partc.CallId__c = cc.Id;
            partc.Contact_Id__c = conc.Id;
            insert partc;
        }
        } 
    }
}