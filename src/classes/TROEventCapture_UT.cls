@isTest
private class TROEventCapture_UT {
	static testmethod void TROEventCapture_UT_test1()
    {
        TROEventCapture inst_TROEventCapture = new TROEventCapture();
        //reciepient type
        list<TROEventCapture.RecipientType> list_recpType = new list<TROEventCapture.RecipientType>();
        TROEventCapture.RecipientType recpType = new TROEventCapture.RecipientType();
        recpType.recipientTypeIdentifier = '';
        recpType.recipientValue = '';
        list_recpType.add(recpType);
        
        //eventEV00017BodyType
        TROEventCapture.eventEV00017BodyType event0017 = new TROEventCapture.eventEV00017BodyType();
        event0017.channelName = '';
        event0017.organizationId = '';
        event0017.recipients = list_recpType;
        event0017.phoneNumber = '';
        event0017.status = '';
        
        //Transaction elements
        TROEventCapture.transactionDetails_element transElem = new TROEventCapture.transactionDetails_element();
        transElem.accountNumber = '';
        transElem.bankNumber = '';
        transElem.productCode = '';
        transElem.exposureAmount = '';
        transElem.exposureDate = System.now();
        
        //eventEV00004BodyType
        TROEventCapture.eventEV00004BodyType event004Type = new TROEventCapture.eventEV00004BodyType();
        event004Type.originatorWire = '';
        event004Type.referenceNumber = ''; 
        event004Type.amount = 0.00;
        event004Type.currencyCode ='';
        event004Type.effectiveDate = System.now();
        event004Type.accountNumber = '';
        event004Type.bankNumber = '';
        event004Type.productCode = '';
        event004Type.beneficiaryCompany = '';
        event004Type.creditDebitIndicator = '';
        event004Type.wireType = '';
        event004Type.originatorToBeneficiaryInformation = '';
        event004Type.bankToBankInformation = '';
        event004Type.referenceForTheBeneficiary = '';
        
        //eventEV00021BodyType
        TROEventCapture.eventEV00021BodyType event0021 = new TROEventCapture.eventEV00021BodyType();
        event0021.userID  = '';
        event0021.emailIDTo  = '';
        event0021.emailIDBcc = '';
        event0021.originationSystem = '';
        
        //eventEV00023BodyType
        TROEventCapture.eventEV00023BodyType event0023 = new TROEventCapture.eventEV00023BodyType();
        event0023.userID = '';
        event0023.password = '';
		event0023.userID  = '';
        event0023.emailIDTo  = '';
        event0023.emailIDBcc = '';
        
        //eventEV00010BodyType
        TROEventCapture.eventEV00010BodyType event0010 = new TROEventCapture.eventEV00010BodyType();
		event0010.bankNumber = '';
        event0010.productCode= '';
        event0010.accountNumber= '';
        event0010.effectiveDate = System.now();
        event0010.actualDate= System.now();
        event0010.time_x= System.now();
        event0010.sourceCode= '';
        event0010.amount= '';
        event0010.amountSign= '';
        
        //eventEV00019_20BodyType
        TROEventCapture.eventEV00019_20BodyType event0019 = new TROEventCapture.eventEV00019_20BodyType();
        event0019.phoneNumber = '';
        
        //recipientsDocument
        TROEventCapture.recipientsDocument recpDocs = new TROEventCapture.recipientsDocument();
        //recpDocs.recipients = list_recpType;
        
        //eventEV00009BodyType
        TROEventCapture.eventEV00009BodyType event009 = new TROEventCapture.eventEV00009BodyType ();
        
        //eventEV00011_12BodyType
        TROEventCapture.eventEV00011_12BodyType event011 = new TROEventCapture.eventEV00011_12BodyType();
        
        //receiveEV00011_12Response_element
        TROEventCapture.receiveEV00011_12Response_element respevent11 = new TROEventCapture.receiveEV00011_12Response_element();
        
        //receiveEV00015Response_element
        TROEventCapture.receiveEV00015Response_element respevent15 = new TROEventCapture.receiveEV00015Response_element();
        
        //receiveEV00014_element
        TROEventCapture.receiveEV00014_element  respevent14 = new TROEventCapture.receiveEV00014_element();
        
        //receiveEV00006_07Response_element
        TROEventCapture.receiveEV00006_07Response_element  respevent6_7 = new TROEventCapture.receiveEV00006_07Response_element();
        
        //receiveEV00022Response_element
        TROEventCapture.receiveEV00022Response_element  respevent022 = new TROEventCapture.receiveEV00022Response_element();
        
        //receiveEV00010_element
        TROEventCapture.receiveEV00010_element  respevent010 = new TROEventCapture.receiveEV00010_element();
        
        //receiveEV00016_18_element
        TROEventCapture.receiveEV00016_18_element  respevent016_18 = new TROEventCapture.receiveEV00016_18_element();
        
        //receiveEV00001_03Response_element
		TROEventCapture.receiveEV00001_03Response_element  respevent01_03 = new TROEventCapture.receiveEV00001_03Response_element();
        
        //receiveEV00009_element
        TROEventCapture.receiveEV00009_element  respevent009 = new TROEventCapture.receiveEV00009_element();
        
        //receiveEV00019_20_element
        TROEventCapture.receiveEV00019_20_element  respevent019_20 = new TROEventCapture.receiveEV00019_20_element();
        
        //receiveEV00004Response_element
        TROEventCapture.receiveEV00004Response_element  respevent004 = new TROEventCapture.receiveEV00004Response_element();
        
        //receiveEV00006_07_element
		TROEventCapture.receiveEV00006_07_element  respevent006_7 = new TROEventCapture.receiveEV00006_07_element();
        
        //receiveEV00009Response_element
        TROEventCapture.receiveEV00009Response_element  respevent09 = new TROEventCapture.receiveEV00009Response_element();
        
        //receiveEV00022_element
        TROEventCapture.receiveEV00022_element  respevent0022 = new TROEventCapture.receiveEV00022_element();
        
        //receiveEV00005_element
		TROEventCapture.receiveEV00005_element  respevent005 = new TROEventCapture.receiveEV00005_element();
        
        //eventEV00016_18BodyType
        TROEventCapture.eventEV00016_18BodyType  respevent0016_18 = new TROEventCapture.eventEV00016_18BodyType();
        
        //recipients
        TROEventCapture.recipients  recepts = new TROEventCapture.recipients();
        
        //EventHeaderType
        TROEventCapture.EventHeaderType  EventHeaderTypeinst = new TROEventCapture.EventHeaderType();
        
        //receiveEV00001_03_element
        TROEventCapture.receiveEV00001_03_element  recv001_003 = new TROEventCapture.receiveEV00001_03_element();
        
        //eventEV00014BodyType
        TROEventCapture.eventEV00014BodyType  event004 = new TROEventCapture.eventEV00014BodyType();
        
        //receiveEV00017_element
		TROEventCapture.receiveEV00017_element  event00017 = new TROEventCapture.receiveEV00017_element();
        
        //EmailAddressDataType
        TROEventCapture.EmailAddressDataType  emailAdd = new TROEventCapture.EmailAddressDataType();
        
        //EventBodyType
        TROEventCapture.EventBodyType  eventBdy = new TROEventCapture.EventBodyType();
        
        //receiveEV00016_18Response_element
        TROEventCapture.receiveEV00016_18Response_element  rec168118 = new TROEventCapture.receiveEV00016_18Response_element();
        
        //receiveEV00017Response_element
        TROEventCapture.receiveEV00017Response_element  rec17 = new TROEventCapture.receiveEV00017Response_element();
        
        //receiveEV00021Response_element
        TROEventCapture.receiveEV00021Response_element  rec21 = new TROEventCapture.receiveEV00021Response_element();
        
        //eventEV00001_03BodyType
        TROEventCapture.eventEV00001_03BodyType  event1_3 = new TROEventCapture.eventEV00001_03BodyType();
        
        //receiveEV00015_element
        TROEventCapture.receiveEV00015_element  even15 = new TROEventCapture.receiveEV00015_element();
        
        //TransactionSummaryType
        TROEventCapture.TransactionSummaryType  summType = new TROEventCapture.TransactionSummaryType();
        
        //eventEV00005BodyType
        TROEventCapture.eventEV00005BodyType  eve5 = new TROEventCapture.eventEV00005BodyType();
        
        //receiveEV00013_element
		TROEventCapture.receiveEV00013_element  rec13 = new TROEventCapture.receiveEV00013_element();
        
        //receiveEV00019_20Response_element
        TROEventCapture.receiveEV00019_20Response_element  rec1920 = new TROEventCapture.receiveEV00019_20Response_element();
        
        //receiveEV00008_element
        TROEventCapture.receiveEV00008_element  rec08 = new TROEventCapture.receiveEV00008_element();
        
        //receiveEV00010Response_element
        TROEventCapture.receiveEV00010Response_element  rec10 = new TROEventCapture.receiveEV00010Response_element();
        
        //receiveEV00013Response_element
        TROEventCapture.receiveEV00013Response_element  r13 = new TROEventCapture.receiveEV00013Response_element();
        
        //receiveEV00011_12_element
        TROEventCapture.receiveEV00011_12_element  rec1112 = new TROEventCapture.receiveEV00011_12_element();
        
        //receiveEV00023Response_element
        TROEventCapture.receiveEV00023Response_element  rec23 = new TROEventCapture.receiveEV00023Response_element();
        
        //eventEV00008BodyType
        TROEventCapture.eventEV00008BodyType  eve08 = new TROEventCapture.eventEV00008BodyType();
        
        //eventEV00006_07BodyType
        TROEventCapture.eventEV00006_07BodyType  ev607 = new TROEventCapture.eventEV00006_07BodyType();
        
        //receiveEV00023_element
        TROEventCapture.receiveEV00023_element  rec0023 = new TROEventCapture.receiveEV00023_element();
        
        //receiveEV00004_element
        TROEventCapture.receiveEV00004_element  rec4 = new TROEventCapture.receiveEV00004_element();
        
        //eventEV00013BodyType
        TROEventCapture.eventEV00013BodyType  ee13 = new TROEventCapture.eventEV00013BodyType();
        
        //receiveEV00021_element
        TROEventCapture.receiveEV00021_element  r21 = new TROEventCapture.receiveEV00021_element();
        
        //receiveEV00014Response_element
        TROEventCapture.receiveEV00014Response_element  rec14 = new TROEventCapture.receiveEV00014Response_element();
        
        //eventEV00015BodyType
        TROEventCapture.eventEV00015BodyType  e15 = new TROEventCapture.eventEV00015BodyType();
        
        //receiveEV00005Response_element
        TROEventCapture.receiveEV00005Response_element  re05 = new TROEventCapture.receiveEV00005Response_element();
        
        //receiveEV00008Response_element
        TROEventCapture.receiveEV00008Response_element  rec8 = new TROEventCapture.receiveEV00008Response_element();
        
        //eventEV00022BodyType
        TROEventCapture.eventEV00022BodyType  e22 = new TROEventCapture.eventEV00022BodyType();
                
        
        
        
        
        
        
        
        
        
        
                
        
        
        
    }
}