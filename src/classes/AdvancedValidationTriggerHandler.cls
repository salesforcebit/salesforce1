public class AdvancedValidationTriggerHandler {
    public Boolean integrationCheck(Boolean vrExclude) {
        ECC_Int_Profile_Permissions__c cs = ECC_Int_Profile_Permissions__c.getInstance();
        
        if ((cs.Exemption_Flag__c || cs.Integration_Override__c) && vrExclude) {
            return true;
        } else {
            return false;
        }
    }
    
    public String validateRecords(Id parentId, SObject newData, Advanced_Validation_Rule__c rule) {
        
        //Base Variable Set
        Schema.DescribeSObjectResult obj = Schema.getGlobalDescribe().get(rule.Object_To_Validate__c).getDescribe();
        Schema.FieldSet objFieldSet = obj.FieldSets.getMap().get(rule.Field_Set__c);
        String[] rules = String.valueOf(rule.Rule__c).split(SEMICOLON);
        String header = BLANK;
        String errorMessage = BLANK;
        
        //Query Definition
        String query = SELECTSTART;
        for(Schema.FieldSetMember f : objFieldSet.getFields()) {
            query += f.getFieldPath() + COMMASPACE;
        }
        query += FROMSTART + String.escapeSingleQuotes(rule.Object_To_Validate__c);
        if (rule.Junction_Object__c != BLANK && rule.Junction_Object__c != null) {
            query += WHERESTART + rule.Junction_Object__c + INCLAUSE + LEFTPARENTHESE + SELECTSTART + IDVALUE + FROMCLAUSE + rule.Junction_Object__c;
        }
        
        if (rule.Filter_Criteria__c != BLANK || rule.Filter_Criteria__c != null) {
            query += WHERESTART + rule.Parent_Object__c + EQUALANDQUOTE + parentId + ESCAPEDQUOTE;
            if (rule.Junction_Object__c != BLANK && rule.Junction_Object__c != null) {
                query += RIGHTPARENTHESE;
            }
        }
        
        query += ANDCLAUSE + LEFTPARENTHESE + rule.Filter_Criteria__c + RIGHTPARENTHESE;
        
        
        query += ORDERBYSTART;
        SObject[] records = Database.query(query);
        
        //Do we build the Error Message?
        if (isPassed(rules, newData, rule.Evaluation_Type__c)) {
            for (SObject item: records) {
                header = BREAKLINE + obj.getLabel() + HYPERLINKSTART + item.get(IDVALUE) + HYPERLINKMIDDLE + item.get(NAMEVALUE) + HYPERLINKEND + System.Label.ECC_Error_Msg_Missing_Data + UNORDEREDLISTSTART;
                String body = BLANK;
                for (Schema.FieldSetMember field : objFieldSet.getFields()) {
                    if(((item.get(field.getFieldPath()) == null || String.valueOf(item.get(field.getFieldPath())) == BLANK) && String.valueOf(field.getType()) != BOOLEANVALUE && String.valueOf(field.getType()) != REFERENCEVALUE) ||
                       (String.valueOf(item.get(field.getFieldPath())) == FALSEVALUE && String.valueOf(field.getType()) == BOOLEANVALUE && String.valueOf(field.getType()) != REFERENCEVALUE) ||
                       (item.get(field.getFieldPath()) == null && String.valueOf(field.getType()) == REFERENCEVALUE && String.valueOf(field.getType()) != BOOLEANVALUE)) {
                           
                           body += LISTSTART + field.getLabel() + LISTEND ;
                       }
                }
                
                if (body != BLANK) {
                    errorMessage += header + body + UNORDEREDLISTEND ;
                }
            }
        }
        
        return errorMessage;
    }

	private Boolean isPassed(String[] rules, SObject newData, String typeValue) {
        Boolean status = false;
        Integer pass = 0;
        Integer fail = 0;

        for(String r: rules) {
            String[] pair = String.valueOf(r).split(EQUALSIGN);
            String key = String.valueOf(pair[0]);
            String value = String.valueOf(pair[1]);

            try {
                if(String.valueOf(newData.get(key)).equals(value) && !String.isBlank(String.valueOf(newData.get(key)))) {
					pass += 1;
				} else {
					fail += 1;
				}
            } catch (Exception e) {
				fail += 1;
			}
        }

        if(typeValue == TYPEVALUEAND) {
            if(rules.size() == pass) {
                status = true;
            }
        } else if(typeValue == TYPEVALUEOR) {
            if(pass > 0) {
                status = true;
            }
        }

    	return status;
    }
    
    private static final String BLANK = '';
    private static final String EQUALSIGN = '=';
    private static final String SEMICOLON = ';';
    private static final String FALSEVALUE = 'false';
    private static final String BOOLEANVALUE = 'Boolean';
    private static final String REFERENCEVALUE = 'Reference';
    private static final String IDVALUE = 'Id';
    private static final String NAMEVALUE = 'Name';
    private static final String UNORDEREDLISTSTART = '<ul>';
    private static final String UNORDEREDLISTEND = '</ul>';
    private static final String LISTSTART = '<li style="list-style-type:none;">- ';
    private static final String LISTEND = '</li>';
    private static final String BREAKLINE = '<br />';
    private static final String HYPERLINKSTART = ': <a href="/';
    private static final String HYPERLINKMIDDLE = '/e" target="_blank">';
    private static final String HYPERLINKEND = '</a> - ';
    private static final String SELECTSTART = 'SELECT ';
    private static final String COMMASPACE = ', ';
    private static final String FROMCLAUSE = ' FROM ';
    private static final String FROMSTART = 'Id, Name FROM ';
    private static final String WHERESTART = ' WHERE ';
	private static final String EQUALANDQUOTE = ' = \'';
    private static final String ESCAPEDQUOTE = '\'';
    private static final String ANDCLAUSE = ' AND ';
    private static final String INCLAUSE = ' IN ';
    private static final String ORDERBYSTART = ' ORDER BY Id';
	private static final String TYPEVALUEAND = 'AND';
	private static final String TYPEVALUEOR = 'OR';
	private static final String LEFTPARENTHESE = '(';
	private static final String RIGHTPARENTHESE = ')';
}