@isTest
public with sharing class EmailCredentials_UT {
	static testMethod void EmailCredentials_UT() {
		LLC_BI__Deposit__c da = new LLC_BI__Deposit__c();
    da.Name = 'TestDA2';
    da.Bank_Number__c = '1234';
    da.LLC_BI__Account_Number__c = '999999';
    insert da;
		TSO_Cost_Center__c tcc = new TSO_Cost_Center__c();
		tcc.Charge_Class__c = 'CN';
		insert tcc;
		Account acc = new Account();
		acc.Name = 'TestAccount';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		acc.Cost_Center__c = tcc.Id;
    acc.Billing_Account_Number__c = da.Id;
		acc.Bank_Number__c = da.Id;
		acc.Delivery_Method__c = 'I';
		insert acc;
		Opportunity oppty = new Opportunity();
		oppty.Name = 'TestOppty';
		oppty.AccountId = acc.Id;
		oppty.CloseDate = System.Today();
		oppty.StageName = 'Pursue';
		oppty.Opportunity_Type__c = 'Addl Product';
		insert oppty;
		Product2 SFprod = UnitTestFactory.buildTestProduct();
		SFprod.Name = 'Scenario Test';
		SFprod.Family = 'Deposits & ECP';
		insert SFprod;
		LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
		insert pl;
		LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
		insert pt;
		LLC_BI__Product__c prod = new LLC_BI__Product__c();
		prod.Name = 'ACH: Origination & Receipt';
		prod.Related_SFDC_Product__c = SFprod.Id;
		prod.LLC_BI__Product_Type__c = pt.Id;
		insert prod;
		LLC_BI__Opportunity_Product__c op = new LLC_BI__Opportunity_Product__c();
		op.LLC_BI__Opportunity__c = oppty.Id;
		op.LLC_BI__Product__c = prod.Id;
		insert op;
		LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c();
		ts.Name = 'TestTS';
		ts.Request_Type__c = 'New';
		ts.LLC_BI__Stage__c = 'Fulfillment';
		ts.LLC_BI__Product_Reference__c = prod.Id;
		ts.LLC_BI__Relationship__c = acc.Id;
		ts.GTM_Request_Type__c = 'NEW Setup';
		insert ts;
		Contact con = new Contact();
		con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Contact').getRecordTypeId();
		con.LastName = 'Test';
		con.FirstName = 'Test';
		con.AccountId = acc.Id;
		con.Contact_Email__c = 'test@test.com';
		con.Phone = '1111111111';
		con.Phone_Extension__c = 'x123';
		con.Title = 'Blah';
		con.user_name__c = 'test';
		insert con;
		LLC_BI__Authorized_User__c AU = new LLC_BI__Authorized_User__c();
		AU.LLC_BI__Contact_Reference__c = con.Id;
		AU.LLC_BI__Treasury_Service__c = ts.Id;
		AU.User_has_unlimited_transfer_limit__c = true;
		Blob key = Crypto.generateAesKey(128);

		//AU.User_Password__c = EncodingUtil.base64Encode(Blob.valueOf('test te232st'));
		PasswordGenerator pg =new PasswordGenerator();
		AU.User_Password__c = pg.encryptPassword(pg.createPassword());
		insert AU;
		String auID = String.valueOf(AU.Id);
		EmailCredentials controller = new EmailCredentials();
		EmailCredentials.localTime();
		EmailCredentials.serviceName(auID);
	}
	static testMethod void EmailCredentials_UT2() {
		LLC_BI__Deposit__c da = new LLC_BI__Deposit__c();
		da.Name = 'TestDA2';
		da.Bank_Number__c = '1234';
		da.LLC_BI__Account_Number__c = '999999';
		insert da;
		TSO_Cost_Center__c tcc = new TSO_Cost_Center__c();
		tcc.Charge_Class__c = 'CN';
		insert tcc;
		Account acc = new Account();
		acc.Name = 'TestAccount';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		acc.Cost_Center__c = tcc.Id;
		acc.Billing_Account_Number__c = da.Id;
		acc.Bank_Number__c = da.Id;
		acc.Delivery_Method__c = 'I';
		insert acc;
		Opportunity oppty = new Opportunity();
		oppty.Name = 'TestOppty';
		oppty.AccountId = acc.Id;
		oppty.CloseDate = System.Today();
		oppty.StageName = 'Pursue';
		oppty.Opportunity_Type__c = 'Addl Product';
		insert oppty;
		Product2 SFprod = UnitTestFactory.buildTestProduct();
		SFprod.Name = 'Scenario Test';
		SFprod.Family = 'Deposits & ECP';
		insert SFprod;
		LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
		insert pl;
		LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
		insert pt;
		LLC_BI__Product__c prod = new LLC_BI__Product__c();
		prod.Name = 'ACH Fraud Services';
		prod.Related_SFDC_Product__c = SFprod.Id;
		prod.LLC_BI__Product_Type__c = pt.Id;
		insert prod;
		LLC_BI__Opportunity_Product__c op = new LLC_BI__Opportunity_Product__c();
		op.LLC_BI__Opportunity__c = oppty.Id;
		op.LLC_BI__Product__c = prod.Id;
		insert op;
		LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c();
		ts.Name = 'TestTS';
		ts.Request_Type__c = 'New';
		ts.LLC_BI__Stage__c = 'Fulfillment';
		ts.LLC_BI__Product_Reference__c = prod.Id;
		ts.LLC_BI__Relationship__c = acc.Id;
		ts.GTM_Request_Type__c = 'NEW Setup';
		insert ts;
		Contact con = new Contact();
		con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Contact').getRecordTypeId();
		con.LastName = 'Test';
		con.FirstName = 'Test';
		con.AccountId = acc.Id;
		con.Contact_Email__c = 'test@test.com';
		con.Phone = '1111111111';
		con.Phone_Extension__c = 'x123';
		con.Title = 'Blah';
		con.user_name__c = 'test';
		insert con;
		LLC_BI__Authorized_User__c AU = new LLC_BI__Authorized_User__c();
		AU.LLC_BI__Contact_Reference__c = con.Id;
		AU.LLC_BI__Treasury_Service__c = ts.Id;
		AU.User_has_unlimited_transfer_limit__c = true;
		insert AU;
		String auID = String.valueOf(AU.Id);
		EmailCredentials controller = new EmailCredentials();
		EmailCredentials.localTime();
		EmailCredentials.serviceName(auID);
	}
	static testMethod void testEmailCredentialsNonAch() {
		LLC_BI__Deposit__c da = new LLC_BI__Deposit__c();
    da.Name = 'TestDA2';
    da.Bank_Number__c = '1234';
    da.LLC_BI__Account_Number__c = '999999';
    insert da;
		TSO_Cost_Center__c tcc = new TSO_Cost_Center__c();
		tcc.Charge_Class__c = 'CN';
		insert tcc;
		Account acc = new Account();
		acc.Name = 'TestAccount';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		acc.Cost_Center__c = tcc.Id;
    acc.Billing_Account_Number__c = da.Id;
		acc.Bank_Number__c = da.Id;
		acc.Delivery_Method__c = 'I';
		insert acc;
		Opportunity oppty = new Opportunity();
		oppty.Name = 'TestOppty';
		oppty.AccountId = acc.Id;
		oppty.CloseDate = System.Today();
		oppty.StageName = 'Pursue';
		oppty.Opportunity_Type__c = 'Addl Product';
		insert oppty;
		Product2 SFprod = UnitTestFactory.buildTestProduct();
		SFprod.Name = 'Scenario Test';
		SFprod.Family = 'Deposits & ECP';
		insert SFprod;
		LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
		insert pl;
		LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
		insert pt;
		LLC_BI__Product__c prod = new LLC_BI__Product__c();
		prod.Name = 'NonACH: Origination & Receipt';
		prod.Related_SFDC_Product__c = SFprod.Id;
		prod.LLC_BI__Product_Type__c = pt.Id;
		insert prod;
		LLC_BI__Opportunity_Product__c op = new LLC_BI__Opportunity_Product__c();
		op.LLC_BI__Opportunity__c = oppty.Id;
		op.LLC_BI__Product__c = prod.Id;
		insert op;
		LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c();
		ts.Name = 'TestTS';
		ts.Request_Type__c = 'New';
		ts.LLC_BI__Stage__c = 'Fulfillment';
		ts.LLC_BI__Product_Reference__c = prod.Id;
		ts.LLC_BI__Relationship__c = acc.Id;
		ts.GTM_Request_Type__c = 'NEW Setup';
		insert ts;
		Contact con = new Contact();
		con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Contact').getRecordTypeId();
		con.LastName = 'Test';
		con.FirstName = 'Test';
		con.AccountId = acc.Id;
		con.Contact_Email__c = 'test@test.com';
		con.Phone = '1111111111';
		con.Phone_Extension__c = 'x123';
		con.Title = 'Blah';
		con.user_name__c = 'test';
		insert con;
		LLC_BI__Authorized_User__c AU = new LLC_BI__Authorized_User__c();
		AU.LLC_BI__Contact_Reference__c = con.Id;
		AU.LLC_BI__Treasury_Service__c = ts.Id;
		AU.User_has_unlimited_transfer_limit__c = true;
		Blob key = Crypto.generateAesKey(128);

		//AU.User_Password__c = EncodingUtil.base64Encode(Blob.valueOf('test te232st'));
		PasswordGenerator pg =new PasswordGenerator();
		AU.User_Password__c = pg.encryptPassword(pg.createPassword());
		insert AU;
		String auID = String.valueOf(AU.Id);
		EmailCredentials controller = new EmailCredentials();
		EmailCredentials.localTime();
		EmailCredentials.serviceName(auID);
	}
}