//========================================================================
//  KeyBank
//  Object: Opportunity
//  Author: Offshore
//  Detail: Trigger to  Insert/Update Opportunity owner in OpportunityTeamMember
//========================================================================
Public class OpportunityTeamMemberUpdate
{
    Public void InsertOpportunityeammember(List<Opportunity> newlist)
    {
    
        //It is insert of Opportunity Owner to OpportunityTeamMember
        List<OpportunityTeamMember> Opportunityteaminsertlist = new List<OpportunityTeamMember>();
        //assigning Opportunity owner name to Opportunity team member while creating Opportunity record.
        
        try
        {
            For(Opportunity acct :newlist)
            { 
                Opportunityteammember Opportunityteaminsert = new Opportunityteammember();
                Opportunityteaminsert.Userid = acct.ownerid;
                Opportunityteaminsert.OpportunityId=acct.id;
                Opportunityteaminsert.TeamMemberRole = 'Opportunity Owner';
                Opportunityteaminsertlist.add(Opportunityteaminsert);
            }
            
            if(Opportunityteaminsertlist.size()>0)
            {
                database.insert(Opportunityteaminsertlist);
            }
        }
        
        catch(DMLException e)
        {
            system.debug(e.getMessage());
        }    
                   
    }
    Public void UpdateOpportunityteammember(List<Opportunity> newupdatelist,Map<ID,Opportunity> Opportunitymap)
    {
    
        //It is update of Opportunity Owner to OpportunityTeamMember
        List<OpportunityTeamMember> Opportunityteamupdatelist = new List<OpportunityTeamMember>();
        //assigning Opportunity owner name to Opportunity team member while updating Opportunity owner.
        
        try
        {
            For(Opportunity actupd :newupdatelist)
            { 
                Opportunity oldOpportunity = Opportunitymap.get(actupd.id); 
                Boolean Ischanged = (actupd.OwnerId!=oldOpportunity.OwnerId);
                
                If(Ischanged)
                {
                    Opportunityteammember Opportunityteamupdate = new Opportunityteammember();
                    Opportunityteamupdate.Userid = actupd.ownerid;
                    Opportunityteamupdate.OpportunityId=actupd.id;
                    Opportunityteamupdate.TeamMemberRole = 'Opportunity Owner';
                    Opportunityteamupdatelist.add(Opportunityteamupdate);
                }
            }
            
            if(Opportunityteamupdatelist.size()>0)
            {
                database.insert(Opportunityteamupdatelist);
            }
        }            
        catch(DMLException e)
        {
            system.debug(e.getMessage());
        }              
    }
}