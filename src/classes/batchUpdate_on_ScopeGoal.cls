global class batchUpdate_on_ScopeGoal implements Database.Batchable<sObject>, Schedulable
{
      List <Sales_Goal__c> sg;
       
       global batchUpdate_on_ScopeGoal()
       {
         // Batch Constructor
       }
       
        // Start Method
       global Database.QueryLocator start(Database.BatchableContext BC){
         string query = 'SELECT ID, Name,User__r.Name, User__c,YTD_Calls__c,Current_Month_Call__c FROM Sales_Goal__c'; 
         
         return Database.getQueryLocator(query);
       }
       global void execute(SchedulableContext SC)
       {
        
       }
        // Execute Logic
       global void execute(Database.BatchableContext BC, List<Sales_Goal__c>scope)
       {
             date today = system.today();
             
             //counter to determine the number of Participants for the current month
             Integer countParticipant=0; 
             List<participant__c> listParticipant = new List<Participant__C>();           
             Map<String, List<Participant__c>> mapParticipant= new Map<String, List<Participant__C>>();

             //Fetching the list of participants for the current year
             for(Participant__c part: [SELECT Contact_Id__c,UserId__c ,CreatedDate ,Id,Name FROM Participant__c where UserId__c!=null  and CALENDAR_YEAR(CreatedDate) =: today.year() order by Contact_Id__c]){
                 if(!mapParticipant.containsKey(part.userid__C))
                      mapParticipant.put(part.userid__c, new List<Participant__c>{part});
                 else{
                      listparticipant=mapParticipant.get(part.userId__C);
                      listParticipant.add(part);
                      mapParticipant.put(part.UserID__c,listParticipant);    
                      }
             }
             
             for(Sales_Goal__c sg : scope) {                               
                if(mapParticipant.get(sg.user__r.Name)!=null){
                    for(Participant__c p : mapParticipant.get(sg.user__r.Name)) {
                       system.debug('*** User Name*** ' + p.UserId__C);
                       if(p.createdDate.month() == today.month() || p.createdDate.year() == today.year()){
                          sg.YTD_Calls__c= mapParticipant.get(sg.user__r.Name).size();
                          if(p.CreatedDate.month()==today.month()){
                             countParticipant++;
                          }  
                       }
                       //need to add logic to remove old year values
                 } 
             sg.Current_Month_Call__c =countParticipant;
             countParticipant  =0; 

                 
            } 
           
         } 
         update scope;   
       }
     
       global void finish(Database.BatchableContext BC){
            // Logic to be Executed at finish
            
            
       }
}