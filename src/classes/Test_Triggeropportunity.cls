@istest
Public class Test_Triggeropportunity 
{
    Static Profile p; 
    Static User testUser;
    Static Account v1;
    Static Account v2;
    Static Program__c pgm1;
    Static Program__c pgm;
    Static Agreement__c Agr;
    Static Agreement__c Agr1;
    Static Vendor_Agreement__c va;
    Static Vendor_Agreement__c va1;
    Static Picklist_Transformation__c adesc;
    Static Picklist_Transformation__c acode;
    Static Picklist_Transformation__c kog;
    Static Picklist_Transformation__c PF;
    Static Picklist_Transformation__c PT;
    Static opportunity optys;
    Static opportunity optys1;
    Static PricebookEntry pre12;
    Static PricebookEntry pbe;
    Static Id pricebookId;
    
    public static testMethod void PLUpdate()
    {
        p = [select id from profile where name='System Administrator'];
     
        testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharanty@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharanty@keybank.com',PLOB__c='support',PLOB_Alias__c='KBBC');             
        
        insert testUser;
        
        
        List<Opportunity> insertOpp = new List<Opportunity>();
        List<Vendor_Agreement__c> vendAgg = new List<Vendor_Agreement__c>();
        //recordtype rt = [Select Id, name from recordtype where name ='KEF Opportunity'];
        //recordtype rt1 = [Select Id, name from recordtype where name ='Lease'];
        
        Map<String, Schema.SObjectType> accsObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs = accsObjectMap.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaAcc = accs.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo = resSchemaAcc.getRecordTypeInfosByName();
        Id RT = accRecordTypeInfo.get('KEF Opportunity').getRecordTypeId();
        System.Debug('RT:'+RT);
        
        Map<String, Schema.SObjectType> accsObjectMap4 = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs4 = accsObjectMap4.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaAcc4 = accs4.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo4 = resSchemaAcc4.getRecordTypeInfosByName();
        Id RT4 = accRecordTypeInfo4.get('Lease').getRecordTypeId();
        System.Debug('RT4:'+RT4);
        
        List<Account> alist=new List<Account>();
        Account acc = new account (name='KEF Opp test');
        alist.add(acc);
        v1 = new account(name ='Vendor', KEF_Risk_Role__c='Vendor/Supplier',  vendor_status__c='Pending Credit', phone='9962556525',
        BillingCity ='Chennai',BillingCountry='US',BillingPostalCode='44136',BillingState='ohio',BillingStreet='strongsville');
        alist.add(v1);
        
        v2 = new account(name ='Vendor1', KEF_Risk_Role__c='Vendor/Supplier',  vendor_status__c='Pending Credit', phone='9876543210',
        BillingCity ='Chennai1',BillingCountry='US',BillingPostalCode='44133',BillingState='Brooklyn',BillingStreet='strong');
        alist.add(v2);
        
        insert alist;
        
        List<Program__c> plist=new List<Program__c>();        
        pgm = new Program__c(Name_of_the_Vendor_Program__c ='KEF Test Pgm', Business_Development_Stage__c='Initial Contact Made', Program_Status__c='Prospect');       
        plist.add(pgm);
        pgm1 = new Program__c(Name_of_the_Vendor_Program__c ='KEF Pgm', Business_Development_Stage__c='Initial Contact Made', Program_Status__c='Prospect');       
        plist.add(pgm1);
        
        
        insert plist;
        
        
       
        List<Agreement__c> aglist=new List<Agreement__c>();
        Agr = new Agreement__c(Agreement_Name__c ='KEF Test Agree', Default_Lessor__c='KEF', Agreement_Type__c='Internal', Agreement_Legal_Status__c ='Contractual', Program__c= pgm.id);
        aglist.add(Agr);
        Agr1 = new Agreement__c(Agreement_Name__c ='KEF Test ', Default_Lessor__c='KEF', Agreement_Type__c='Internal', Agreement_Legal_Status__c ='Contractual', Program__c= pgm1.id);
        aglist.add(Agr1);
        
        insert aglist;
        
        va = new Vendor_Agreement__c(Account_Vendor__c=v1.id, Agreement__c = Agr.id);
        va1 = new Vendor_Agreement__c(Account_Vendor__c=v2.id, Agreement__c = Agr1.id);        
        Vendagg.add(va);
        Vendagg.add(va1);
         
        insert vendagg;
        
        test.starttest();       
        List<Picklist_Transformation__c> insertPicklist =new List<Picklist_Transformation__c>();
        adesc = new Picklist_Transformation__c(Code__c='3',Picklist_Field__c='Asset Description',DefaultDescription__c='Aircraft');
        insertPicklist.add(adesc);        
        acode = new Picklist_Transformation__c(Code__c='4',Picklist_Field__c='Asset Code',DefaultDescription__c='A200 - Commercial Jet');
        insertPicklist.add(acode);        
        kog = new Picklist_Transformation__c(Code__c='1393',Picklist_Field__c='KEF Owning Group',DefaultDescription__c='MM - Bank');
        insertPicklist.add(kog);        
        PF= new Picklist_Transformation__c(Code__c='4',Picklist_Field__c='PaymentFrequency',DefaultDescription__c='Monthly');
        insertPicklist.add(PF);        
        PT= new Picklist_Transformation__c(Code__c='1393',Picklist_Field__c='Payment Type',DefaultDescription__c='Advance');
        insertPicklist.add(PT);        
        
        insert insertPicklist;
        
        
        optys = new opportunity(name ='KEF Test Opty',volume__c = 12589, AccountId = acc.id, closedate=system.today(), StageName='Pursue', 
        recordtypeId=RT, ownerid=testUser.id,  Asset_Description__c='Aircraft',Vendor_agreement__c=Agr.id, Asset_Code__c='A200 - Commercial Jet', KEF_Owning_Group__c='MM - Bank', AssetCategoryID__c=adesc.id, AssetSubCategoryID__c=acode.id, CustomDataListItemID__c=kog.id );        
        
        optys1 = new opportunity(name ='Test Opty',volume__c = 12589, AccountId = acc.id, closedate=system.today(), StageName='Pursue', 
        recordtypeId=RT4 , ownerid=testUser.id, Loan_Syndication__c=true,Private_Code_Name__c='Test',Distribution_Strategy__c = 'Hold for sale',primary_syndicator__c=testUser.id,Vendor_agreement__c=Agr1.id);        
        insertOpp.add(optys);
        insertOpp.add(optys1);
        
        insert insertOpp;
                
        optys.vendor_agreement__c=Agr1.id;
        update optys;
        
         OpportunityTriggerHandler.updatevendor(insertopp);
         test.stoptest();
       /*
        pricebookId = Test.getStandardPricebookId();        
        Product2 prd12 = new Product2(Name='Testm', Family='Opportunity KEF', Product_category__c='Receivables Products',IsActive=true);
        
        insert prd12;
        
         pre12 = new PricebookEntry(Pricebook2Id=pricebookId, IsActive=true, UnitPrice =76582, Product2Id=prd12.id);
        insert pre12;
        
        OpportunityLineItem  oliitm = new OpportunityLineItem(OpportunityId=optys1.id, Primary_Product__c=true,Payment_Frequency__c='Monthly', Payment_Type__c='Advance', PricebookEntryId=pre12.id, Product_Type__c='Buy Side', PaymentFrequencyID__c=PF.id, AdvanceTypeID__c=PT.id);
        insert oliitm;
        
        
        
         
       
        
       List<Id> i=new List<Id>();
       i.add(oliitm .Id);
       OpportunityTriggerHandler.updateSalesPrice(i);
        */
        
       
        /*
        
        OpportunitySplitType ost = [Select ID, Description from OpportunitySplitType where Description =: 'Volume'];
       
        OpportunityTeamMember otm=new OpportunityTeamMember(OpportunityId=optys.id,UserId=testUser.id);
        insert otm;
        OpportunityTeamMember otm1=new OpportunityTeamMember(OpportunityId=optys1.id,UserId=testUser.id);
        insert otm1;
        
        OpportunitySplit os=new OpportunitySplit(OpportunityId=optys.id,SplitOwnerId=testUser.id,SplitPercentage=50,SplitTypeId=ost.id, Primary__c = true);
        insert os;
        
        OpportunitySplit os1=new OpportunitySplit(OpportunityId=optys1.id,SplitOwnerId=testUser.id,SplitPercentage=50,SplitTypeId=ost.id, Primary__c = true);
        insert os;
        */
    }
    
    
    public static testMethod void updatemethod()
    {
      
       List<Opportunity> oppList = new List<Opportunity>();
        Profile pr=[select id from profile where name='KEF Business Developer'];
        User testUser1 = new User(alias = 'u2', email='indu_venkatakrishnan@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = pr.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='indu_venkatakrishnan12@keybank.com',PLOB__c='support',PLOB_Alias__c='KBBC');             
        insert testUser1;
        pricebookId = Test.getStandardPricebookId();
        Account acc = new account (name='Test');
        insert acc;
        //recordtype rt2=[select Id, name from recordtype where name='Credit'];
        //recordtype rt3=[select Id, name from recordtype where name='KEF Opportunity'];
        
        Map<String, Schema.SObjectType> accsObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs = accsObjectMap.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaAcc = accs.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo = resSchemaAcc.getRecordTypeInfosByName();
        Id RT = accRecordTypeInfo.get('KEF Opportunity').getRecordTypeId();
        System.Debug('RT:'+RT);
        
        Map<String, Schema.SObjectType> accsObjectMap4 = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs4 = accsObjectMap4.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaAcc4 = accs4.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo4 = resSchemaAcc4.getRecordTypeInfosByName();
        Id RT4 = accRecordTypeInfo4.get('Credit').getRecordTypeId();
        System.Debug('RT4:'+RT4);
        
        Program__c p1=new Program__c(name_of_the_vendor_program__c='test',primary_business_developer__c=testUser1.id,program_status__c='Prospect');
        insert p1; 
        
        Agreement__c kefa = new Agreement__c(Business_Developer_Name__c=testUser1.id,program__c=p1.id,Agreement_Name__c='Test');
        insert kefa; 
        
        
        Program_for_BD__c pbd=new Program_for_BD__c (Name='Test',Year__c='2016',user_name__c=testUser1.id);
        insert pbd;
        
       
        
        opportunity opty2 = new opportunity(name ='Test Opty',volume__c = 12589, AccountId = acc.id, closedate=system.today(), StageName='Pursue', 
        recordtypeId=RT4 , Obligation_Number__c='12', Obligor_Number__c='13',DDA_Account__c='0',auto_debit__c='Yes',Vendor_Agreement__c=kefa.Id);   
        oppList.add(opty2);
        
        opportunity opty3 = new opportunity(name ='Test Opty',volume__c = 12589, AccountId = acc.id, closedate=system.today(), StageName='Pursue', 
        recordtypeId=RT , Obligation_Number__c='12', Obligor_Number__c='13',DDA_Account__c='0',auto_debit__c='Yes',Vendor_Agreement__c=kefa.Id,Estimated_Fee__c=123);   
        oppList.add(opty3);
        
        opportunity opty4 = new opportunity(name ='KEF Test Child Opty',volume__c = 12589, AccountId = acc.id, closedate=system.today(), StageName='Pursue', 
        recordtypeId=RT4, ownerid=testUser1.id, parent_opportunity_id__c = opty2.Id);
        oppList.add(opty4);
        
        insert oppList;
        List<Id> oldlist = new List<Id>();
        oldlist.add(opty4.Id);
        List<Id> idlist = new List<Id>();
        idlist.add(opty4.Id);
        OpportunityTriggerHandler.updateOppty(idlist);
        OpportunityTriggerHandler.countChildOpty(idlist);
        OpportunityTriggerHandler.countChildOptyupdate(idlist, oldlist);
        
        test.startTest();
        
        opty3 =[Select id,name,KEF_Program_ID__c,Estimated_Fee__c from opportunity where id= :opty3.id];
        Program_Goals__c pg=new Program_Goals__c(Year__c='2016',Goal_Month__C = system.today(),Name='Test',npv__c=12,Volume__c=45,Program_Name__c=opty3.KEF_Program_ID__c);
        insert pg;
         Product2 prd12 = new Product2(Name='Testm', Family='Opportunity KEF', Product_category__c='Receivables Products',IsActive=true);
        insert prd12;
        OpportunityTriggerHandler.insertProgramGoalJunction(oppList);
        
        pbe = new PricebookEntry(Pricebook2Id=pricebookId, IsActive=true, UnitPrice =76582, Product2Id=prd12.id);
        insert pbe;
        system.debug(pbe+' Price book entry** test class');
        
        OpportunityLineItem  oliitm = new OpportunityLineItem(OpportunityId=opty3.id,UnitPrice=opty3.Estimated_Fee__c, Primary_Product__c=true,Payment_Frequency__c='Monthly', Payment_Type__c='Advance', PricebookEntryId=pbe.id, Product_Type__c='Buy Side');
        
        insert oliitm; 
        List<Id> i=new List<Id>();
        i.add(oliitm .Id);
        OpportunityTriggerHandler.updateSalesPrice(i);
       
        
        opty3.actual_close_date__c=system.today();
        opty3.StageName='Closed';
        opty3.reason__c='Full Solution Provided';
        opty3.primary_syndicator__C=testUser1.id;
        update opty3;
        List<Opportunity> opty=new List<Opportunity>();
        opty.add(opty3);
        //OpportunityTriggerHandler.insertProgramGoalJunction(opty);
        OpportunityTriggerHandler.insertProgramBDJunction(opty);
        
        
        OpportunitySplitType ost = [Select ID, Description from OpportunitySplitType where Description =: 'Volume'];
        OpportunityTeamMember otm=new OpportunityTeamMember(OpportunityId=opty3.id,UserId=testUser1.id);
        insert otm;
        OpportunitySplit os=new OpportunitySplit(OpportunityId=opty3.id,SplitOwnerId=testUser1.id,SplitPercentage=50,SplitTypeId=ost.id, Primary__c = true);
        insert os;
        
        Segment_Revenue_Goal_Object__c sr=new Segment_Revenue_Goal_Object__c(Name='Test',Goal_Month__c=system.today());
        insert sr;
        OpportunityTriggerHandler.opportunitysegmentgoals(opty);
        Production_Goals__c prodg=new Production_Goals__c(Name='Test',Goal_Month__c=system.today(),Username__c=os.SplitOwnerId);
        insert prodg;
        OpportunityTriggerHandler.opportunitysplitquery(opty);
        
        prodg.name='Test1';
        update prodg;
        test.stopTest();
    }
}