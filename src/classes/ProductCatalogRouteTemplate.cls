/**
 * Provides a strongly-typed intermediate object for converting the JSON representation
 * of a Product Catalog Route Template to its sObject equivalent.
 */
public class ProductCatalogRouteTemplate extends nFORCE.AForce {
	public String sid;
	public String name;
	public String screenSectionResource;
	public String routeBody;
	public Decimal routeOrder;
	public String routeTopbar;
	public String routeIconClass;
	public String routeNavigation;
	public String routeSubNavigation;
	public String productCatalogUITemplate;
	public List<ProductCatalogSectionConfiguration> sectionConfigurations;

	public ProductCatalogRouteTemplate() {
		this.sectionConfigurations = new List<ProductCatalogSectionConfiguration>();
	}

	public ProductCatalogRouteTemplate(String sid, String name, String screenSectionResource,
		String routeBody, Decimal routeOrder, String routeTopbar, String routeIconClass,
		String routeNavigation, String routeSubNavigation) {
		this.sid = sid;
		this.name = name;
		this.screenSectionResource = screenSectionResource;
		this.routeBody = routeBody;
		this.routeOrder = routeOrder;
		this.routeTopbar = routeTopbar;
		this.routeIconClass = routeIconClass;
		this.routeNavigation = routeNavigation;
		this.routeSubNavigation = routeSubNavigation;
		this.sectionConfigurations = new List<ProductCatalogSectionConfiguration>();
	}

	public Product_Catalog_Route_Template__c getSObject() {
		Product_Catalog_Route_Template__c dbRouteTemplate = new Product_Catalog_Route_Template__c();
		this.mapToDb(dbRouteTemplate);
		return dbRouteTemplate;
	}

	public static ProductCatalogRouteTemplate buildFromDB(sObject obj) {
		ProductCatalogRouteTemplate routeTemplate = new ProductCatalogRouteTemplate();
		routeTemplate.mapFromDB(obj);
		return routeTemplate;
	}

	public override Type getType() {
		return ProductCatalogRouteTemplate.class;
	}

	public override Schema.SObjectType getSObjectType() {
		return Product_Catalog_Route_Template__c.sObjectType;
	}

	public override void mapFromDb(sObject obj) {
		Product_Catalog_Route_Template__c routeTemplate = (Product_Catalog_Route_Template__c)obj;
		if (routeTemplate != null) {
			this.sid = routeTemplate.ID;
			this.name = routeTemplate.Name;
			this.screenSectionResource = routeTemplate.Screen_Section_Resource__c;
			this.routeBody = routeTemplate.Route_Body__c;
			this.routeOrder = routeTemplate.Route_Order__c;
			this.routeTopbar = routeTemplate.Route_Topbar__c;
			this.routeIconClass = routeTemplate.Route_Icon_Class__c;
			this.routeNavigation = routeTemplate.Route_Navigation__c;
			this.routeSubNavigation = routeTemplate.Route_Sub_Navigation__c;
			this.productCatalogUITemplate = routeTemplate.Product_Catalog_UI_Template__c;
		}
	}

	public override void mapToDb(sObject obj) {
		Product_Catalog_Route_Template__c routeTemplate = (Product_Catalog_Route_Template__c)obj;
		if (routeTemplate != null) {
			if (this.sid != null && this.sid.length() > 0) {
				routeTemplate.Id = Id.valueOf(this.sid);
			}
			routeTemplate.Name = this.name;
			routeTemplate.Screen_Section_Resource__c = this.screenSectionResource;
			routeTemplate.Route_Body__c = this.routeBody;
			routeTemplate.Route_Order__c = this.routeOrder;
			routeTemplate.Route_Topbar__c = this.routeTopbar;
			routeTemplate.Route_Icon_Class__c = this.routeIconClass;
			routeTemplate.Route_Navigation__c = this.routeNavigation;
			routeTemplate.Route_Sub_Navigation__c = this.routeSubNavigation;
		}
	}
}