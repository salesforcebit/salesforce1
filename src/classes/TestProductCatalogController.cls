@isTest
public class TestProductCatalogController {
	@isTest
	public static void testGetNamespacePrefix() {
		String namespace = ProductCatalogController.getNamespacePrefix();
		String expectedNamespace = TestProductCatalogController.class.getName()
			.substringBefore('TestProductCatalogController').substringBefore('.');
		System.assertEquals(expectedNamespace, namespace);
	}

	@isTest
	public static void testRetrieveFieldSetsBlank() {
		Test.startTest();
		String result = ProductCatalogController.retrieveFieldSets('Not_A_Real_Object__z');
		Test.stopTest();

		List<Object> fieldSets = (List<Object>)JSON.deserializeUntyped(result);
		System.assertNotEquals(null, fieldSets);
		System.assertEquals(0, fieldSets.size());
	}

	@isTest
	public static void testRetrieveFieldSets() {
		Test.startTest();
		String result = ProductCatalogController.retrieveFieldSets('Account');
		Test.stopTest();

		List<Object> fieldSets = (List<Object>)JSON.deserializeUntyped(result);
		System.assertNotEquals(null, fieldSets);
		System.assert(fieldSets.size() > 0);
	}

	@isTest
	public static void testRetrieveSectionResources() {
		Test.startTest();
		String result = ProductCatalogController.retrieveSectionResources();
		Test.stopTest();

		List<Object> sectionResources = (List<Object>)JSON.deserializeUntyped(result);
		System.assertNotEquals(null, sectionResources);
		System.assert(sectionResources.size() > 0);
	}

	@isTest
	public static void testRetrieveRouteBodyOptions() {
		Test.startTest();
		String result = ProductCatalogController.retrieveRouteBodyOptions();
		Test.stopTest();

		List<ApexPage> apexPages = [
			SELECT
				Id,
				Name
			FROM
				ApexPage
		];

		List<Object> pages = (List<Object>)JSON.deserializeUntyped(result);
		System.assertNotEquals(null, pages);
		System.assertEquals(apexPages.size(), pages.size());
	}

	@isTest
	public static void testRetrieveSObjectTypes() {
		Test.startTest();
		String result = ProductCatalogController.retrieveSObjectTypes();
		Test.stopTest();

		List<Object> types = (List<Object>)JSON.deserializeUntyped(result);
		System.assertNotEquals(null, types);

		Map<String, Schema.sObjectType> globalDescribeMap = Schema.getGlobalDescribe();
		List<String> objectNames = new List<String>(globalDescribeMap.keySet());
		System.assertEquals(objectNames.size(), types.size());
	}

	@isTest
	public static void testRetrieveUITemplatesBlank() {
		Test.startTest();
		String templates = ProductCatalogController.retrieveUITemplates();
		Test.stopTest();

		List<Object> uiTemplates = (List<Object>)JSON.deserializeUntyped(templates);

		System.assertNotEquals(null, uiTemplates);
		System.assertEquals(0, uiTemplates.size());
	}

	@isTest
	public static void testRetrieveUITemplates() {
		insertTestUITemplates();

		Test.startTest();
		String templates = ProductCatalogController.retrieveUITemplates();
		Test.stopTest();

		List<Object> uiTemplates = (List<Object>)JSON.deserializeUntyped(templates);

		System.assertNotEquals(null, uiTemplates);
		System.assertEquals(1, uiTemplates.size());

		Map<String, Object> uiTemplate =
			(Map<String, Object>)uiTemplates.get(0);
		System.assertEquals('UI Template One', uiTemplate.get('name'));
		System.assertEquals(false, uiTemplate.get('isDefault'));
	}

	@isTest
	public static void testRetrieveUITemplateByIdNonExistent() {
		Test.startTest();
		String template = ProductCatalogController.retrieveUITemplateById('00136000009hUrP');
		Test.stopTest();

		Object obj = (Object)JSON.deserializeUntyped(template);
		System.assertNotEquals(null, obj);
	}

	@isTest
	public static void testRetrieveUITemplateById() {
		Id templateId = insertTestUITemplates();

		Test.startTest();
		String template = ProductCatalogController.retrieveUITemplateById(templateId);
		Test.stopTest();

		Object obj = (Object)JSON.deserializeUntyped(template);
		System.assertNotEquals(null, obj);

		Map<String, Object> uiTemplate = (Map<String, Object>)obj;
		System.assertEquals('UI Template One', uiTemplate.get('name'));
		System.assertEquals(false, uiTemplate.get('isDefault'));

		List<Object> uiTemplateRoutes =
			(List<Object>)(uiTemplate.get('routeTemplates'));
		System.assertEquals(5, uiTemplateRoutes.size());

		Map<String, Object> orderEntryRoute =
			(Map<String, Object>)uiTemplateRoutes.get(0);
		System.assertEquals('Order Entry', orderEntryRoute.get('name'));
		System.assertEquals('MappedSObjectScreenResource',
			orderEntryRoute.get('screenSectionResource'));
		System.assertEquals('nFORCE__KoDataGrid',
			orderEntryRoute.get('routeBody'));
		System.assertEquals(1,
			orderEntryRoute.get('routeOrder'));

		List<Object> orderEntrySectionConfigs =
			(List<Object>)(orderEntryRoute.get('sectionConfigurations'));
		System.assertEquals(1, orderEntrySectionConfigs.size());

		Map<String, Object> orderEntrySectionConfig =
			(Map<String, Object>)orderEntrySectionConfigs.get(0);
		System.assertEquals('LLC_BI__Treasury_Service__c',
			orderEntrySectionConfig.get('sObjectType'));
		System.assertEquals('UI_Order_Entry_for_Online_Banking',
			orderEntrySectionConfig.get('fieldSet'));
		System.assertEquals(1.01,
			orderEntrySectionConfig.get('displayOrder'));

		Map<String, Object> authorizedUsersRoute =
			(Map<String, Object>)uiTemplateRoutes.get(1);
		System.assertEquals('Authorized Users', authorizedUsersRoute.get('name'));
		System.assertEquals('MappedSObjectHierarchyScreenResource',
			authorizedUsersRoute.get('screenSectionResource'));
		System.assertEquals('nFORCE__KoRelatedList',
			authorizedUsersRoute.get('routeBody'));
		System.assertEquals(2,
			authorizedUsersRoute.get('routeOrder'));

		List<Object> authorizedUsersSectionConfigs =
			(List<Object>)(authorizedUsersRoute.get('sectionConfigurations'));
		System.assertEquals(1, authorizedUsersSectionConfigs.size());

		Map<String, Object> authorizedUsersSectionConfig =
			(Map<String, Object>)authorizedUsersSectionConfigs.get(0);
		System.assertEquals('LLC_BI__Authorized_User__c',
			authorizedUsersSectionConfig.get('sObjectType'));
		System.assertEquals('UI_Users_for_Online_Banking',
			authorizedUsersSectionConfig.get('fieldSet'));
		System.assertEquals(1.01,
			authorizedUsersSectionConfig.get('displayOrder'));

		Map<String, Object> analyzedAccountsRoute =
			(Map<String, Object>)uiTemplateRoutes.get(2);
		System.assertEquals('Analyzed Accounts', analyzedAccountsRoute.get('name'));
		System.assertEquals('MappedSObjectHierarchyScreenResource',
			analyzedAccountsRoute.get('screenSectionResource'));
		System.assertEquals('nFORCE__KoRelatedList',
			analyzedAccountsRoute.get('routeBody'));
		System.assertEquals(3,
			analyzedAccountsRoute.get('routeOrder'));

		List<Object> analyzedAccountsSectionConfigs =
			(List<Object>)(analyzedAccountsRoute.get('sectionConfigurations'));
		System.assertEquals(1, analyzedAccountsSectionConfigs.size());

		Map<String, Object> analyzedAccountsSectionConfig =
			(Map<String, Object>)analyzedAccountsSectionConfigs.get(0);
		System.assertEquals('LLC_BI__Analyzed_Account__c',
			analyzedAccountsSectionConfig.get('sObjectType'));
		System.assertEquals('UI_Accounts_for_Online_Banking',
			analyzedAccountsSectionConfig.get('fieldSet'));
		System.assertEquals(1.01,
			analyzedAccountsSectionConfig.get('displayOrder'));

		Map<String, Object> maintenanceRoute =
			(Map<String, Object>)uiTemplateRoutes.get(3);
		System.assertEquals('Maintenance', maintenanceRoute.get('name'));
		System.assertEquals('MappedSObjectHierarchyScreenResource',
			maintenanceRoute.get('screenSectionResource'));
		System.assertEquals('LLC_BI__Maintenance',
			maintenanceRoute.get('routeBody'));
		System.assertEquals(4,
			maintenanceRoute.get('routeOrder'));

		List<Object> maintenanceSectionConfigs =
			(List<Object>)(maintenanceRoute.get('sectionConfigurations'));
		System.assertEquals(1, maintenanceSectionConfigs.size());

		Map<String, Object> maintenanceSectionConfig =
			(Map<String, Object>)maintenanceSectionConfigs.get(0);
		System.assertEquals('LLC_BI__Treasury_Service__c',
			maintenanceSectionConfig.get('sObjectType'));
		System.assertEquals('UI_Setup_for_Maintenance',
			maintenanceSectionConfig.get('fieldSet'));
		System.assertEquals(1.01,
			maintenanceSectionConfig.get('displayOrder'));

		Map<String, Object> productSummaryRoute =
			(Map<String, Object>)uiTemplateRoutes.get(4);
		System.assertEquals('Product Summary', productSummaryRoute.get('name'));
		System.assertEquals('MappedSObjectHierarchyScreenResource',
			productSummaryRoute.get('screenSectionResource'));
		System.assertEquals('LLC_BI__KoProductSummary',
			productSummaryRoute.get('routeBody'));
		System.assertEquals(5,
			productSummaryRoute.get('routeOrder'));

		List<Object> productSummarySectionConfigs =
			(List<Object>)(productSummaryRoute.get('sectionConfigurations'));
		System.assertEquals(3, productSummarySectionConfigs.size());

		Map<String, Object> productSummarySectionConfigOne =
			(Map<String, Object>)productSummarySectionConfigs.get(0);
		System.assertEquals('LLC_BI__Treasury_Service__c',
			productSummarySectionConfigOne.get('sObjectType'));
		System.assertEquals(null,
			productSummarySectionConfigOne.get('fieldSet'));
		System.assertEquals(1.01,
			productSummarySectionConfigOne.get('displayOrder'));

		Map<String, Object> productSummarySectionConfigTwo =
			(Map<String, Object>)productSummarySectionConfigs.get(1);
		System.assertEquals('LLC_BI__Product_Package__c',
			productSummarySectionConfigTwo.get('sObjectType'));
		System.assertEquals(null,
			productSummarySectionConfigTwo.get('fieldSet'));
		System.assertEquals(2.01,
			productSummarySectionConfigTwo.get('displayOrder'));

		Map<String, Object> productSummarySectionConfigThree =
			(Map<String, Object>)productSummarySectionConfigs.get(2);
		System.assertEquals('LLC_BI__Treasury_Service__c',
			productSummarySectionConfigThree.get('sObjectType'));
		System.assertEquals('UI_Product_Summary',
			productSummarySectionConfigThree.get('fieldSet'));
		System.assertEquals(3.01,
			productSummarySectionConfigThree.get('displayOrder'));
	}

	@isTest
	public static void testSaveUITemplate() {
		ProductCatalogUITemplate jTemplate =
			new ProductCatalogUITemplate(null,
				'New UI Template', false);

		ProductCatalogRouteTemplate jRouteOrderEntry =
			new ProductCatalogRouteTemplate(null, 'Order Entry', 'MappedSObjectScreenResource',
			'nFORCE__KoDataGrid', 1, null, null, null, null);
		ProductCatalogSectionConfiguration jConfigOrderEntry =
			new ProductCatalogSectionConfiguration(null, null,
				'LLC_BI__Treasury_Service__c', 'UI_Order_Entry_for_Online_Banking', null, 1.01);

		ProductCatalogRouteTemplate jRouteAuthorizedUsers =
			new ProductCatalogRouteTemplate(null, 'Authorized Users',
				'MappedSObjectHierarchyScreenResource', 'nFORCE__KoRelatedList', 2, null, null, null,
				null);
		ProductCatalogSectionConfiguration jConfigAuthorizedUsers =
			new ProductCatalogSectionConfiguration(null, null,
				'LLC_BI__Authorized_User__c', 'UI_Users_for_Online_Banking', null, 1.01);

		ProductCatalogRouteTemplate jRouteAnalyzedAccounts =
			new ProductCatalogRouteTemplate(null, 'Analyzed Accounts',
				'MappedSObjectHierarchyScreenResource', 'nFORCE__KoRelatedList', 3, null, null, null,
				null);
		ProductCatalogSectionConfiguration jConfigAnalyzedAccounts =
			new ProductCatalogSectionConfiguration(null, null,
				'LLC_BI__Analyzed_Account__c', 'UI_Accounts_for_Online_Banking', null, 1.01);

		ProductCatalogRouteTemplate jRouteMaintenance =
			new ProductCatalogRouteTemplate(null, 'Maintenance',
				'MappedSObjectHierarchyScreenResource', 'LLC_BI__Maintenance', 4, null, null, null,
				null);
		ProductCatalogSectionConfiguration jConfigMaintenance =
			new ProductCatalogSectionConfiguration(null, null,
				'LLC_BI__Treasury_Service__c', 'UI_Setup_for_Maintenance', null, 1.01);

		ProductCatalogRouteTemplate jRouteProductSummary =
			new ProductCatalogRouteTemplate(null, 'Product Summary',
				'MappedSObjectHierarchyScreenResource', 'LLC_BI__KoProductSummary', 5, null, null, null,
				null);
		ProductCatalogSectionConfiguration jConfigProductSummaryOne =
			new ProductCatalogSectionConfiguration(null, null,
				'LLC_BI__Treasury_Service__c', '', null, 1.01);
		ProductCatalogSectionConfiguration jConfigProductSummaryTwo =
			new ProductCatalogSectionConfiguration(null, null,
				'LLC_BI__Product_Package__c', '', null, 2.01);
		ProductCatalogSectionConfiguration jConfigProductSummaryThree =
			new ProductCatalogSectionConfiguration(null, null,
				'LLC_BI__Treasury_Service__c', 'UI_Product_Summary', null, 3.01);

		jRouteOrderEntry.sectionConfigurations.add(jConfigOrderEntry);
		jRouteAuthorizedUsers.sectionConfigurations.add(jConfigAuthorizedUsers);
		jRouteAnalyzedAccounts.sectionConfigurations.add(jConfigAnalyzedAccounts);
		jRouteMaintenance.sectionConfigurations.add(jConfigMaintenance);
		jRouteProductSummary.sectionConfigurations.add(jConfigProductSummaryOne);
		jRouteProductSummary.sectionConfigurations.add(jConfigProductSummaryTwo);
		jRouteProductSummary.sectionConfigurations.add(jConfigProductSummaryThree);

		jTemplate.routeTemplates.add(jRouteOrderEntry);
		jTemplate.routeTemplates.add(jRouteAuthorizedUsers);
		jTemplate.routeTemplates.add(jRouteAnalyzedAccounts);
		jTemplate.routeTemplates.add(jRouteMaintenance);
		jTemplate.routeTemplates.add(jRouteProductSummary);

		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(jTemplate);

		Test.startTest();
		ProductCatalogController.saveUITemplate(gen.getAsString());
		Test.stopTest();

		List<Product_Catalog_UI_Template__c> uiTemplates = [
			SELECT
				Id,
				Name,
				Is_Default__c
			FROM
				Product_Catalog_UI_Template__c
		];

		System.assertNotEquals(null, uiTemplates);
		System.assertEquals(1, uiTemplates.size());
		System.assertEquals('New UI Template', uiTemplates[0].Name);
		System.assertEquals(false, uiTemplates[0].Is_Default__c);

		List<Product_Catalog_Route_Template__c> uiRoutes = [
			SELECT
				Id,
				Name,
				Route_Body__c,
				Route_Order__c,
				Screen_Section_Resource__c,
				Product_Catalog_UI_Template__c
			FROM
				Product_Catalog_Route_Template__c
		];

		System.assertNotEquals(null, uiRoutes);
		System.assertEquals(5, uiRoutes.size());

		System.assertEquals('Order Entry', uiRoutes[0].Name);
		System.assertEquals('nFORCE__KoDataGrid', uiRoutes[0].Route_Body__c);
		System.assertEquals(1, uiRoutes[0].Route_Order__c);
		System.assertEquals('MappedSObjectScreenResource', uiRoutes[0].Screen_Section_Resource__c);
		System.assertEquals(uiTemplates[0].Id, uiRoutes[0].Product_Catalog_UI_Template__c);

		System.assertEquals('Authorized Users', uiRoutes[1].Name);
		System.assertEquals('nFORCE__KoRelatedList', uiRoutes[1].Route_Body__c);
		System.assertEquals(2, uiRoutes[1].Route_Order__c);
		System.assertEquals('MappedSObjectHierarchyScreenResource', uiRoutes[1].Screen_Section_Resource__c);
		System.assertEquals(uiTemplates[0].Id, uiRoutes[1].Product_Catalog_UI_Template__c);

		System.assertEquals('Analyzed Accounts', uiRoutes[2].Name);
		System.assertEquals('nFORCE__KoRelatedList', uiRoutes[2].Route_Body__c);
		System.assertEquals(3, uiRoutes[2].Route_Order__c);
		System.assertEquals('MappedSObjectHierarchyScreenResource', uiRoutes[2].Screen_Section_Resource__c);
		System.assertEquals(uiTemplates[0].Id, uiRoutes[2].Product_Catalog_UI_Template__c);

		System.assertEquals('Maintenance', uiRoutes[3].Name);
		System.assertEquals('LLC_BI__Maintenance', uiRoutes[3].Route_Body__c);
		System.assertEquals(4, uiRoutes[3].Route_Order__c);
		System.assertEquals('MappedSObjectHierarchyScreenResource', uiRoutes[3].Screen_Section_Resource__c);
		System.assertEquals(uiTemplates[0].Id, uiRoutes[3].Product_Catalog_UI_Template__c);

		System.assertEquals('Product Summary', uiRoutes[4].Name);
		System.assertEquals('LLC_BI__KoProductSummary', uiRoutes[4].Route_Body__c);
		System.assertEquals(5, uiRoutes[4].Route_Order__c);
		System.assertEquals('MappedSObjectHierarchyScreenResource', uiRoutes[4].Screen_Section_Resource__c);
		System.assertEquals(uiTemplates[0].Id, uiRoutes[4].Product_Catalog_UI_Template__c);

		List<Product_Catalog_Section_Configuration__c> uiConfigs = [
			SELECT
				Id,
				Name,
				SObject_Type__c,
				Field_Set__c,
				Display_Order__c,
				Product_Catalog_Route_Template__c
			FROM
				Product_Catalog_Section_Configuration__c
		];

		System.assertEquals('LLC_BI__Treasury_Service__c', uiConfigs[0].SObject_Type__c);
		System.assertEquals('UI_Order_Entry_for_Online_Banking', uiConfigs[0].Field_Set__c);
		System.assertEquals(1.01, uiConfigs[0].Display_Order__c);
		System.assertEquals(uiRoutes[0].Id, uiConfigs[0].Product_Catalog_Route_Template__c);

		System.assertEquals('LLC_BI__Authorized_User__c', uiConfigs[1].SObject_Type__c);
		System.assertEquals('UI_Users_for_Online_Banking', uiConfigs[1].Field_Set__c);
		System.assertEquals(1.01, uiConfigs[1].Display_Order__c);
		System.assertEquals(uiRoutes[1].Id, uiConfigs[1].Product_Catalog_Route_Template__c);

		System.assertEquals('LLC_BI__Analyzed_Account__c', uiConfigs[2].SObject_Type__c);
		System.assertEquals('UI_Accounts_for_Online_Banking', uiConfigs[2].Field_Set__c);
		System.assertEquals(1.01, uiConfigs[2].Display_Order__c);
		System.assertEquals(uiRoutes[2].Id, uiConfigs[2].Product_Catalog_Route_Template__c);

		System.assertEquals('LLC_BI__Treasury_Service__c', uiConfigs[3].SObject_Type__c);
		System.assertEquals('UI_Setup_for_Maintenance', uiConfigs[3].Field_Set__c);
		System.assertEquals(1.01, uiConfigs[3].Display_Order__c);
		System.assertEquals(uiRoutes[3].Id, uiConfigs[3].Product_Catalog_Route_Template__c);

		System.assertEquals('LLC_BI__Treasury_Service__c', uiConfigs[4].SObject_Type__c);
		System.assertEquals(null, uiConfigs[4].Field_Set__c);
		System.assertEquals(1.01, uiConfigs[4].Display_Order__c);
		System.assertEquals(uiRoutes[4].Id, uiConfigs[4].Product_Catalog_Route_Template__c);

		System.assertEquals('LLC_BI__Product_Package__c', uiConfigs[5].SObject_Type__c);
		System.assertEquals(null, uiConfigs[5].Field_Set__c);
		System.assertEquals(2.01, uiConfigs[5].Display_Order__c);
		System.assertEquals(uiRoutes[4].Id, uiConfigs[5].Product_Catalog_Route_Template__c);

		System.assertEquals('LLC_BI__Treasury_Service__c', uiConfigs[6].SObject_Type__c);
		System.assertEquals('UI_Product_Summary', uiConfigs[6].Field_Set__c);
		System.assertEquals(3.01, uiConfigs[6].Display_Order__c);
		System.assertEquals(uiRoutes[4].Id, uiConfigs[6].Product_Catalog_Route_Template__c);
	}

	@isTest
	public static void testRetrieveProductCatalogBlank() {
		Test.startTest();
		String catalog = ProductCatalogController.retrieveProductCatalog();
		Test.stopTest();

		List<Object> productLines = (List<Object>)JSON.deserializeUntyped(catalog);

		System.assertNotEquals(null, productLines);
		System.assertEquals(0, productLines.size());
	}

	@isTest
	public static void testRetrieveProductCatalog() {
		insertTestProductCatalog();

		Test.startTest();
		String catalog = ProductCatalogController.retrieveProductCatalog();
		Test.stopTest();

		List<Object> productLines = (List<Object>)JSON.deserializeUntyped(catalog);

		System.assertNotEquals(null, productLines);
		System.assertEquals(1, productLines.size());

		Map<String, Object> productLine =
			(Map<String, Object>)productLines.get(0);
		System.assertEquals('Test Product Line', productLine.get('name'));

		List<Object> productTypes = (List<Object>)(productLine.get('productTypes'));
		System.assertEquals(2, productTypes.size());

		Map<String, Object> productTypeOne = (Map<String, Object>)productTypes.get(0);
		System.assertEquals('Test Product Type 1', productTypeOne.get('name'));
		System.assertEquals('abc', productTypeOne.get('lookupKey'));
		Map<String, Object> productTypeTwo = (Map<String, Object>)productTypes.get(1);
		System.assertEquals('Test Product Type 2', productTypeTwo.get('name'));
		System.assertEquals('bcd', productTypeTwo.get('lookupKey'));

		List<Object> productTypeOneProducts =
			(List<Object>)(productTypeOne.get('products'));
		System.assertEquals(2, productTypeOneProducts.size());

		Map<String, Object> productOne =
			(Map<String, Object>)productTypeOneProducts.get(0);
		System.assertEquals('Test Product 1', productOne.get('name'));
		System.assertEquals('abcd', productOne.get('lookupKey'));

		Map<String, Object> productTwo =
			(Map<String, Object>)productTypeOneProducts.get(1);
		System.assertEquals('Test Product 2', productTwo.get('name'));
		System.assertEquals('bcde', productTwo.get('lookupKey'));

		List<Object> productTypeTwoProducts =
			(List<Object>)(productTypeTwo.get('products'));
		System.assertEquals(1, productTypeTwoProducts.size());

		Map<String, Object> productThree =
			(Map<String, Object>)productTypeTwoProducts.get(0);
		System.assertEquals('Test Product 3', productThree.get('name'));
		System.assertEquals('cdef', productThree.get('lookupKey'));

		List<Object> productOneBillPoints =
			(List<Object>)(productOne.get('billPoints'));
		System.assertEquals(2, productOneBillPoints.size());

		Map<String, Object> billPointOne =
			(Map<String, Object>)productOneBillPoints.get(0);
		System.assertEquals('Test Bill Point 1', billPointOne.get('name'));
		System.assertEquals('abcde', billPointOne.get('lookupKey'));
		System.assertEquals(2.5, billPointOne.get('price'));
		System.assertEquals(1.25, billPointOne.get('unitCost'));

		Map<String, Object> billPointTwo =
			(Map<String, Object>)productOneBillPoints.get(1);
		System.assertEquals('Test Bill Point 2', billPointTwo.get('name'));
		System.assertEquals('bcdef', billPointTwo.get('lookupKey'));
		System.assertEquals(122.55, billPointTwo.get('price'));
		System.assertEquals(10, billPointTwo.get('unitCost'));

		List<Object> productTwoBillPoints =
			(List<Object>)(productTwo.get('billPoints'));
		System.assertEquals(1, productTwoBillPoints.size());

		Map<String, Object> billPointThree =
			(Map<String, Object>)productTwoBillPoints.get(0);
		System.assertEquals('Test Bill Point 3', billPointThree.get('name'));
		System.assertEquals('cdefg', billPointThree.get('lookupKey'));
	}

	@isTest
	public static void testSaveProductCatalogInsertOnly() {
		List<ProductLine> jLines =
			new List<ProductLine>();
		ProductLine jLine =
			new ProductLine(null,
				'Test Product Line', null);
		ProductType jType =
			new ProductType(null,
				'Test Product Type', 'abc', null);
		Product jProduct =
			new Product(null,
				'Test Product', 'abcd', null);
		BillPoint jBillPoint =
			new BillPoint(null,
				'Test Bill Point', 'abcde', 2.50, 1.25, null);

		jProduct.billPoints.add(jBillPoint);
		jType.products.add(jProduct);
		jLine.productTypes.add(jType);
		jLines.add(jLine);

		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(jLines);

		Test.startTest();
		ProductCatalogController.saveProductCatalog(gen.getAsString());
		Test.stopTest();

		List<LLC_BI__Product_Line__c> productLines = [
			SELECT
				Id,
				Name
			FROM
				LLC_BI__Product_Line__c
			WHERE
				LLC_BI__Product_Object__c =: ProductCatalogController.TS_PRODUCT_OBJECT
		];

		System.assertNotEquals(null, productLines);
		System.assertEquals(1, productLines.size());
		System.assertEquals('Test Product Line', productLines[0].Name);

		List<LLC_BI__Product_Type__c> productTypes = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Product_Line__c
			FROM
				LLC_BI__Product_Type__c
			WHERE
				LLC_BI__Usage_Type__c =: ProductCatalogController.TS_USAGE_TYPE
		];

		System.assertNotEquals(null, productTypes);
		System.assertEquals(1, productTypes.size());
		System.assertEquals('Test Product Type', productTypes[0].Name);
		System.assertEquals('abc', productTypes[0].LLC_BI__lookupKey__c);
		System.assertEquals(productLines[0].Id,
			productTypes[0].LLC_BI__Product_Line__c);

		List<LLC_BI__Product__c> products = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Product_Type__c
			FROM
				LLC_BI__Product__c
		];

		System.assertNotEquals(null, products);
		System.assertEquals(1, products.size());
		System.assertEquals('Test Product', products[0].Name);
		System.assertEquals('abcd', products[0].LLC_BI__lookupKey__c);
		System.assertEquals(productTypes[0].Id, products[0].LLC_BI__Product_Type__c);

		List<LLC_BI__Bill_Point__c> billPoints = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Price__c,
				LLC_BI__Unit_Cost__c,
				LLC_BI__Product__c
			FROM
				LLC_BI__Bill_Point__c
		];

		System.assertNotEquals(null, billPoints);
		System.assertEquals(1, billPoints.size());
		System.assertEquals('Test Bill Point', billPoints[0].Name);
		System.assertEquals('Test Bill Point', billPoints[0].Name);
		System.assertEquals('Test Bill Point', billPoints[0].Name);
		System.assertEquals('abcde', billPoints[0].LLC_BI__lookupKey__c);
		System.assertEquals(2.50, billPoints[0].LLC_BI__Price__c);
		System.assertEquals(1.25, billPoints[0].LLC_BI__Unit_Cost__c);
		System.assertEquals(products[0].Id, billPoints[0].LLC_BI__Product__c);
	}

	@isTest
	public static void testSaveProductCatalogUpdateExisting() {
		insertTestProductCatalog();

		List<LLC_BI__Product_Line__c> productLines = [
			SELECT
				Id,
				Name
			FROM
				LLC_BI__Product_Line__c
			WHERE
				LLC_BI__Product_Object__c =: ProductCatalogController.TS_PRODUCT_OBJECT
		];

		List<LLC_BI__Product_Type__c> productTypes = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Product_Line__c
			FROM
				LLC_BI__Product_Type__c
			WHERE
				LLC_BI__Usage_Type__c =: ProductCatalogController.TS_USAGE_TYPE
		];

		List<LLC_BI__Product__c> products = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Product_Type__c
			FROM
				LLC_BI__Product__c
		];

		List<LLC_BI__Bill_Point__c> billPoints = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Price__c,
				LLC_BI__Unit_Cost__c,
				LLC_BI__Product__c
			FROM
				LLC_BI__Bill_Point__c
		];

		List<ProductLine> jLines =
			new List<ProductLine>();
		// Update name
		ProductLine jLineOne =
			new ProductLine(productLines[0].Id,
				'Test Product Line Updated', null);
		// Update lookup
		ProductType jTypeOne =
			new ProductType(productTypes[0].Id,
				'Test Product Type 1', 'abcUpdated', null);
		// Keep the same
		ProductType jTypeTwo =
			new ProductType(productTypes[1].Id,
				'Test Product Type 2', 'bcd', null);
		// Update lookup
		Product jProductOne =
			new Product(products[0].Id,
				'Test Product 1', 'abcdUpdated', null);
		// Keep the same
		Product jProductTwo =
			new Product(products[1].Id,
				'Test Product 2', 'bcde', null);
		// Keep the same
		Product jProductThree =
			new Product(products[2].Id,
				'Test Product 3', 'cdef', null);
		// Update price and unitCost
		BillPoint jBillPointOne =
			new BillPoint(billPoints[0].Id,
				'Test Bill Point 1', 'abcde', 5.0, 1.50, null);
		// Keep the same
		BillPoint jBillPointTwo =
			new BillPoint(billPoints[1].Id,
				'Test Bill Point 2', 'bcdef', 122.55, 10, null);
		// Keep the same
		BillPoint jBillPointThree =
			new BillPoint(billPoints[2].Id,
				'Test Bill Point 3', 'cdefg', 0, 0, null);
		// Insert a new one
		BillPoint jBillPointFour =
			new BillPoint(null,
				'Test Bill Point 4', 'defgh', 500, 100, null);

		jProductOne.billPoints.add(jBillPointOne);
		jProductOne.billPoints.add(jBillPointTwo);
		jProductTwo.billPoints.add(jBillPointThree);
		jProductOne.billPoints.add(jBillPointFour);

		jTypeOne.products.add(jProductOne);
		jTypeOne.products.add(jProductTwo);
		jTypeTwo.products.add(jProductThree);

		jLineOne.productTypes.add(jTypeOne);
		jLineOne.productTypes.add(jTypeTwo);

		jLines.add(jLineOne);

		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(jLines);

		Test.startTest();
		ProductCatalogController.saveProductCatalog(gen.getAsString());
		Test.stopTest();

		productLines = [
			SELECT
				Id,
				Name
			FROM
				LLC_BI__Product_Line__c
			WHERE
				LLC_BI__Product_Object__c =: ProductCatalogController.TS_PRODUCT_OBJECT
		];

		System.assertNotEquals(null, productLines);
		System.assertEquals(1, productLines.size());
		System.assertEquals('Test Product Line Updated', productLines[0].Name);

		productTypes = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Product_Line__c
			FROM
				LLC_BI__Product_Type__c
			WHERE
				LLC_BI__Usage_Type__c =: ProductCatalogController.TS_USAGE_TYPE
		];

		System.assertNotEquals(null, productTypes);
		System.assertEquals(2, productTypes.size());

		System.assertEquals('Test Product Type 1', productTypes[0].Name);
		System.assertEquals('abcUpdated', productTypes[0].LLC_BI__lookupKey__c);
		System.assertEquals(productLines[0].Id,
			productTypes[0].LLC_BI__Product_Line__c);

		System.assertEquals('Test Product Type 2', productTypes[1].Name);
		System.assertEquals('bcd', productTypes[1].LLC_BI__lookupKey__c);
		System.assertEquals(productLines[0].Id,
			productTypes[1].LLC_BI__Product_Line__c);

		products = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Product_Type__c
			FROM
				LLC_BI__Product__c
		];

		System.assertNotEquals(null, products);
		System.assertEquals(3, products.size());

		System.assertEquals('Test Product 1', products[0].Name);
		System.assertEquals('abcdUpdated', products[0].LLC_BI__lookupKey__c);
		System.assertEquals(productTypes[0].Id, products[0].LLC_BI__Product_Type__c);

		System.assertEquals('Test Product 2', products[1].Name);
		System.assertEquals('bcde', products[1].LLC_BI__lookupKey__c);
		System.assertEquals(productTypes[0].Id, products[1].LLC_BI__Product_Type__c);

		System.assertEquals('Test Product 3', products[2].Name);
		System.assertEquals('cdef', products[2].LLC_BI__lookupKey__c);
		System.assertEquals(productTypes[1].Id, products[2].LLC_BI__Product_Type__c);

		billPoints = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Price__c,
				LLC_BI__Unit_Cost__c,
				LLC_BI__Product__c
			FROM
				LLC_BI__Bill_Point__c
		];

		System.assertNotEquals(null, billPoints);
		System.assertEquals(4, billPoints.size());

		System.assertEquals('Test Bill Point 1', billPoints[0].Name);
		System.assertEquals('abcde', billPoints[0].LLC_BI__lookupKey__c);
		System.assertEquals(5, billPoints[0].LLC_BI__Price__c);
		System.assertEquals(1.5, billPoints[0].LLC_BI__Unit_Cost__c);
		System.assertEquals(products[0].Id, billPoints[0].LLC_BI__Product__c);

		System.assertEquals('Test Bill Point 2', billPoints[1].Name);
		System.assertEquals('bcdef', billPoints[1].LLC_BI__lookupKey__c);
		System.assertEquals(122.55, billPoints[1].LLC_BI__Price__c);
		System.assertEquals(10, billPoints[1].LLC_BI__Unit_Cost__c);
		System.assertEquals(products[0].Id, billPoints[1].LLC_BI__Product__c);

		System.assertEquals('Test Bill Point 3', billPoints[2].Name);
		System.assertEquals('cdefg', billPoints[2].LLC_BI__lookupKey__c);
		System.assertEquals(0, billPoints[2].LLC_BI__Price__c);
		System.assertEquals(0, billPoints[2].LLC_BI__Unit_Cost__c);
		System.assertEquals(products[1].Id, billPoints[2].LLC_BI__Product__c);

		System.assertEquals('Test Bill Point 4', billPoints[3].Name);
		System.assertEquals('defgh', billPoints[3].LLC_BI__lookupKey__c);
		System.assertEquals(500, billPoints[3].LLC_BI__Price__c);
		System.assertEquals(100, billPoints[3].LLC_BI__Unit_Cost__c);
		System.assertEquals(products[0].Id, billPoints[3].LLC_BI__Product__c);
	}

	private static void insertTestProductCatalog() {
		LLC_BI__Product_Line__c newProductLine = new LLC_BI__Product_Line__c(
			Name='Test Product Line',
			LLC_BI__Product_Object__c=ProductCatalogController.TS_PRODUCT_OBJECT
		);
		insert newProductLine;

		LLC_BI__Product_Type__c newProductTypeOne = new LLC_BI__Product_Type__c(
			Name='Test Product Type 1',
			LLC_BI__lookupKey__c='abc',
			LLC_BI__Product_Line__c=newProductLine.Id,
			LLC_BI__Usage_Type__c=ProductCatalogController.TS_USAGE_TYPE
		);
		insert newProductTypeOne;

		LLC_BI__Product_Type__c newProductTypeTwo = new LLC_BI__Product_Type__c(
			Name='Test Product Type 2',
			LLC_BI__lookupKey__c='bcd',
			LLC_BI__Product_Line__c=newProductLine.Id,
			LLC_BI__Usage_Type__c=ProductCatalogController.TS_USAGE_TYPE
		);
		insert newProductTypeTwo;

		LLC_BI__Product__c newProductOne = new LLC_BI__Product__c(
			Name='Test Product 1',
			LLC_BI__lookupKey__c='abcd',
			LLC_BI__Product_Type__c=newProductTypeOne.Id
		);
		insert newProductOne;

		LLC_BI__Product__c newProductTwo = new LLC_BI__Product__c(
			Name='Test Product 2',
			LLC_BI__lookupKey__c='bcde',
			LLC_BI__Product_Type__c=newProductTypeOne.Id
		);
		insert newProductTwo;

		LLC_BI__Product__c newProductThree = new LLC_BI__Product__c(
			Name='Test Product 3',
			LLC_BI__lookupKey__c='cdef',
			LLC_BI__Product_Type__c=newProductTypeTwo.Id
		);
		insert newProductThree;

		LLC_BI__Bill_Point__c newBillPointOne = new LLC_BI__Bill_Point__c(
			Name='Test Bill Point 1',
			LLC_BI__lookupKey__c='abcde',
			LLC_BI__Price__c=2.5,
			LLC_BI__Unit_Cost__c=1.25,
			LLC_BI__Product__c=newProductOne.Id
		);
		insert newBillPointOne;

		LLC_BI__Bill_Point__c newBillPointTwo = new LLC_BI__Bill_Point__c(
			Name='Test Bill Point 2',
			LLC_BI__lookupKey__c='bcdef',
			LLC_BI__Price__c=122.55,
			LLC_BI__Unit_Cost__c=10,
			LLC_BI__Product__c=newProductOne.Id
		);
		insert newBillPointTwo;

		LLC_BI__Bill_Point__c newBillPointThree = new LLC_BI__Bill_Point__c(
			Name='Test Bill Point 3',
			LLC_BI__lookupKey__c='cdefg',
			LLC_BI__Product__c=newProductTwo.Id
		);
		insert newBillPointThree;
	}

	private static Id insertTestUITemplates() {
		Product_Catalog_UI_Template__c newTemplateOne = new Product_Catalog_UI_Template__c(
			Name='UI Template One',
			Is_Default__c=false
		);
		insert newTemplateOne;

		Product_Catalog_Route_Template__c newRouteOrderEntry = new Product_Catalog_Route_Template__c(
			Name='Order Entry',
			Product_Catalog_UI_Template__c=newTemplateOne.Id,
			Route_Body__c='nFORCE__KoDataGrid',
			Route_Order__c=1,
			Screen_Section_Resource__c='MappedSObjectScreenResource'
		);
		insert newRouteOrderEntry;

		Product_Catalog_Section_Configuration__c newConfigOrderEntry
			= new Product_Catalog_Section_Configuration__c(
			Product_Catalog_Route_Template__c=newRouteOrderEntry.Id,
			SObject_Type__c='LLC_BI__Treasury_Service__c',
			Field_Set__c='UI_Order_Entry_for_Online_Banking',
			Display_Order__c=1.01
		);
		insert newConfigOrderEntry;

		Product_Catalog_Route_Template__c newRouteAuthUsers = new Product_Catalog_Route_Template__c(
			Name='Authorized Users',
			Product_Catalog_UI_Template__c=newTemplateOne.Id,
			Route_Body__c='nFORCE__KoRelatedList',
			Route_Order__c=2,
			Screen_Section_Resource__c='MappedSObjectHierarchyScreenResource'
		);
		insert newRouteAuthUsers;

		Product_Catalog_Section_Configuration__c newConfigAuthUsers
			= new Product_Catalog_Section_Configuration__c(
			Product_Catalog_Route_Template__c=newRouteAuthUsers.Id,
			SObject_Type__c='LLC_BI__Authorized_User__c',
			Field_Set__c='UI_Users_for_Online_Banking',
			Display_Order__c=1.01
		);
		insert newConfigAuthUsers;

		Product_Catalog_Route_Template__c newRouteAnalyzedAccts = new Product_Catalog_Route_Template__c(
			Name='Analyzed Accounts',
			Product_Catalog_UI_Template__c=newTemplateOne.Id,
			Route_Body__c='nFORCE__KoRelatedList',
			Route_Order__c=3,
			Screen_Section_Resource__c='MappedSObjectHierarchyScreenResource'
		);
		insert newRouteAnalyzedAccts;

		Product_Catalog_Section_Configuration__c newConfigAnalyzedAccts
			= new Product_Catalog_Section_Configuration__c(
			Product_Catalog_Route_Template__c=newRouteAnalyzedAccts.Id,
			SObject_Type__c='LLC_BI__Analyzed_Account__c',
			Field_Set__c='UI_Accounts_for_Online_Banking',
			Display_Order__c=1.01
		);
		insert newConfigAnalyzedAccts;

		Product_Catalog_Route_Template__c newRouteMaintenance = new Product_Catalog_Route_Template__c(
			Name='Maintenance',
			Product_Catalog_UI_Template__c=newTemplateOne.Id,
			Route_Body__c='LLC_BI__Maintenance',
			Route_Order__c=4,
			Screen_Section_Resource__c='MappedSObjectHierarchyScreenResource'
		);
		insert newRouteMaintenance;

		Product_Catalog_Section_Configuration__c newConfigMaintenance
			= new Product_Catalog_Section_Configuration__c(
			Product_Catalog_Route_Template__c=newRouteMaintenance.Id,
			SObject_Type__c='LLC_BI__Treasury_Service__c',
			Field_Set__c='UI_Setup_for_Maintenance',
			Display_Order__c=1.01
		);
		insert newConfigMaintenance;

		Product_Catalog_Route_Template__c newRouteProdSummary = new Product_Catalog_Route_Template__c(
			Name='Product Summary',
			Product_Catalog_UI_Template__c=newTemplateOne.Id,
			Route_Body__c='LLC_BI__KoProductSummary',
			Route_Order__c=5,
			Screen_Section_Resource__c='MappedSObjectHierarchyScreenResource'
		);
		insert newRouteProdSummary;

		Product_Catalog_Section_Configuration__c newConfigProdSummaryOne
			= new Product_Catalog_Section_Configuration__c(
			Product_Catalog_Route_Template__c=newRouteProdSummary.Id,
			SObject_Type__c='LLC_BI__Treasury_Service__c',
			Field_Set__c='',
			Display_Order__c=1.01
		);
		insert newConfigProdSummaryOne;

		Product_Catalog_Section_Configuration__c newConfigProdSummaryTwo
			= new Product_Catalog_Section_Configuration__c(
			Product_Catalog_Route_Template__c=newRouteProdSummary.Id,
			SObject_Type__c='LLC_BI__Product_Package__c',
			Field_Set__c='',
			Display_Order__c=2.01
		);
		insert newConfigProdSummaryTwo;

		Product_Catalog_Section_Configuration__c newConfigProdSummaryThree
			= new Product_Catalog_Section_Configuration__c(
			Product_Catalog_Route_Template__c=newRouteProdSummary.Id,
			SObject_Type__c='LLC_BI__Treasury_Service__c',
			Field_Set__c='UI_Product_Summary',
			Display_Order__c=3.01
		);
		insert newConfigProdSummaryThree;

		return newTemplateOne.Id;
	}
}