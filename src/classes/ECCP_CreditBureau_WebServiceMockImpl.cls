@isTest
global class ECCP_CreditBureau_WebServiceMockImpl implements WebServiceMock {
global void doInvoke(
       Object stub,
       Object request,
       Map<string,object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
       
       ECCP_CreditbureauWS.ECCPCreditBureauResponse responseElement = new ECCP_CreditbureauWS.ECCPCreditBureauResponse();
       ECCP_CreditbureauWS.ECCP_Credit_Bureau_Type resp1 = new ECCP_CreditbureauWS.ECCP_Credit_Bureau_Type();
       
       resp1.SSN = '12345667';
       resp1.Orchestration_Code = '0000075';
       resp1.Credit_Bureau_ID =  '000015';
       
       responseElement.ECCP_Credit_Bureau = resp1;
       
       response.put('response_x', responseElement);
       
 
       }
       
       
    }