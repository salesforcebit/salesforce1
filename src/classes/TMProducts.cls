global virtual class TMProducts {

	public String csvFeature {get;set;}
	public String csvProductLine {get;set;}
	public String csvProductType {get;set;}
	public String csvProduct {get;set;}
	public String csvBillPoints {get;set;}
	public String csvProductConnection {get;set;}
	public String usageType {get;set;}
	public String productLineID {get;set;}
	public String productTypeID {get;set;}
	public Boolean isProductDefaultAppAvailable = false;
	public Map<String,String> featureMap {get;set;}
	public Map<String,String> productMap {get;set;}
	public Map<String,String> productTypeMap {get;set;}

	public TMProducts(){

		String namespace = Utility.getNamespace();
		Schema.SobjectType oType = Schema.getGlobalDescribe().get(namespace + 'Product__c');
		if(oType.getDescribe().fields.getMap().keySet().contains(namespace.tolowercase()+'default_app__c'))
			isProductDefaultAppAvailable = true;
	}



	/*
		Object Creation Methods
	*/
	public void createProductFeatures() {

		String namespace = Utility.getNamespace();
		Schema.SobjectType oType = Schema.getGlobalDescribe().get(namespace + 'Product_Feature__c');
		List<String> featuresToCreate = getSplitList(csvFeature, '\n');
		List<sObject> insertList = new List<sObject>();

		for (String feature : featuresToCreate) {

			sObject newObj = oType.newSObject();
			List<String> details = getSplitList(feature, ',');
			Integer size = details.size();

			if(size > 0) newObj.put(namespace + 'Purpose_Code__c', details.get(0));
			if(size > 1) newObj.put(namespace + 'lookupKey__c', getLookupKey(details.get(1)));
			if(size > 2) newObj.put(namespace + details.get(2).trim(), true);
			if(size > 3) newObj.put(namespace + details.get(3).trim(), true);
			insertList.add(newObj);
		}
		insert(insertList);
	}

	public void createProductLine() {

		String namespace = Utility.getNamespace();
		Schema.SobjectType oType = Schema.getGlobalDescribe().get(namespace + 'Product_Line__c');
		List<String> linesToCreate = getSplitList(csvProductLine, '\n');
		List<sObject> insertList = new List<sObject>();

		for (String line : linesToCreate) {

			sObject newObj = oType.newSObject();
			List<String> details = getSplitList(line, ',');
			Integer size = details.size();

			if(size > 0) newObj.put('Name', details.get(0));
			if(size > 1) newObj.put(namespace + 'lookupKey__c', getLookupKey(details.get(1)));
			if(size > 2) newObj.put(namespace + 'Product_Object__c', details.get(2));
			insertList.add(newObj);
		}

		insert(insertList);
	}

	public void createProductType() {

		if(productLineID != null && usageType != null){

			String namespace = Utility.getNamespace();
			Schema.SobjectType oType = Schema.getGlobalDescribe().get(namespace + 'Product_Type__c');
			List<String> typesToCreate = getSplitList(csvProductType, '\n');
			List<sObject> insertList = new List<sObject>();

			for (String type : typesToCreate) {

				sObject newObj = oType.newSObject();
				List<String> details = getSplitList(type, ',');
				Integer size = details.size();

				if(size > 0) newObj.put('Name', details.get(0));
				if(size > 1) newObj.put(namespace + 'lookupKey__c', getLookupKey(details.get(1)));

				newObj.put(namespace + 'Product_Line__c', productLineID);
				newObj.put(namespace + 'Usage_Type__c', usageType);
				insertList.add(newObj);
			}
			insert(insertList);
		}
	}

	public void createProduct() {

		if(productTypeID != null){

			String namespace = Utility.getNamespace();
			Schema.SobjectType oType = Schema.getGlobalDescribe().get(namespace + 'Product__c');
			List<String> productsToCreate = getSplitList(csvProduct, '\n');
			List<sObject> insertList = new List<sObject>();

			for (String prod : productsToCreate) {

				sObject newObj = oType.newSObject();
				List<String> details = getSplitList(prod, ',');
				Integer size = details.size();

				if(size > 0) newObj.put('Name', details.get(0));
				if(size > 1) newObj.put(namespace + 'lookupKey__c', getLookupKey(details.get(1)));

				if(size > 2 && featureMap.get(details.get(2).trim()) != null)
					newObj.put(namespace + 'Product_Feature__c', featureMap.get(details.get(2).trim()));

				if(isProductDefaultAppAvailable && size > 3)
					newObj.put(namespace + 'Default_App__c', details.get(3).trim());

				newObj.put(namespace + 'Product_Type__c', productTypeID);
				insertList.add(newObj);
			}
			insert(insertList);
		}
	}

	public void createBillPoint() {

		String namespace = Utility.getNamespace();
		Schema.SobjectType oType = Schema.getGlobalDescribe().get(namespace + 'Bill_Point__c');
		List<String> billPointsToCreate  = getSplitList(csvBillPoints, '\n');
		List<sObject> insertList = new List<sObject>();

		for (String billPt : billPointsToCreate) {

			sObject newObj = oType.newSObject();
			List<String> details = getSplitList(billPt, ',');
			Integer size = details.size();


			if(size > 0) newObj.put('Name', details.get(0));
			if(size > 1) newObj.put(namespace + 'lookupKey__c', getLookupKey(details.get(1)));

			if(size > 2 && productMap.get(details.get(2).trim()) != null)
				newObj.put(namespace + 'Product__c', productMap.get(details.get(2).trim()));

			if(size > 3 && productTypeMap.get(details.get(3).trim()) != null)
				newObj.put(namespace + 'Product_Type__c', productTypeMap.get(details.get(3).trim()));

			if(size > 4) newObj.put(namespace + 'Price__c', Decimal.valueOf(details.get(4)));
			if(size > 5) newObj.put(namespace + 'Unit_Cost__c', Decimal.valueOf(details.get(5)));
			insertList.add(newObj);
		}
		insert(insertList);
	}

	public void createProductConnection() {

		String namespace = Utility.getNamespace();
		Schema.SobjectType oType = Schema.getGlobalDescribe().get(namespace + 'Product_Connection__c');
		List<String> productConnectionsToCreate  = getSplitList(csvProductConnection, '\n');
		List<sObject> insertList = new List<sObject>();

		for (String conn : productConnectionsToCreate) {

			sObject newObj = oType.newSObject();
			List<String> details = getSplitList(conn, ',');
			Integer size = details.size();

			if(size > 0 && productMap.get(details.get(0).trim()) != null)
				newObj.put(namespace + 'Parent_Product_Reference__c', productMap.get(details.get(0).trim()));

			if(size > 1 && productMap.get(details.get(1).trim()) != null)
				newObj.put(namespace + 'Child_Product_Reference__c', productMap.get(details.get(1).trim()));

			if(size > 2 && details.get(2).trim() == 'Y')
				newObj.put(namespace + 'Is_Required__c', true);

			if(size > 3)
				newObj.put(namespace + 'Type__c', details.get(3).trim());
			insertList.add(newObj);
		}
		insert(insertList);
	}

	/*
		Get Object Mappings (LookupKey => Id)
	*/
	public void getFeatureMap() {

		String namespace = Utility.getNamespace();
		List<sObject> sobjList = Database.query('SELECT Id, '+ namespace + 'lookupKey__c '+
															 'FROM '+ namespace + 'Product_Feature__c');

		featureMap = new Map<String,String>();
		if(sobjList != null){
			for (sObject feature : sobjList) {
				featureMap.put((String) feature.get(namespace +'lookupKey__c'), feature.Id);
			}
		}
	}

	public void getProductTypeMap() {

		String namespace = Utility.getNamespace();
		List<sObject> sobjList = Database.query('SELECT Id, '+ namespace + 'lookupKey__c '+
															 'FROM '+ namespace + 'Product_Type__c');

		productTypeMap = new Map<String,String>();
		if(sobjList != null){
			for (sObject feature : sobjList) {
				productTypeMap.put((String) feature.get(namespace +'lookupKey__c'), feature.Id);
			}
		}
	}

	public void getProductMap() {

		String namespace = Utility.getNamespace();
		List<sObject> sobjList = Database.query('SELECT Id, '+ namespace + 'lookupKey__c '+
															 'FROM '+ namespace + 'Product__c');

		productMap = new Map<String,String>();
		if(sobjList != null){
			for (sObject feature : sobjList) {
				productMap.put((String) feature.get(namespace +'lookupKey__c'), feature.Id);
			}
		}
		System.debug(productMap);
	}

	public String getProductLineIdFromLookupKey(String lookupKey) {

		String namespace = Utility.getNamespace();
		List<sObject> sobjList = Database.query('SELECT Id FROM '+ namespace + 'Product_Line__c '+
															 'WHERE '+ namespace + 'lookupKey__c =:lookupKey');

		if(sobjList != null && sobjList.size() > 0)
			return sobjList.get(0).Id;
		else
			return null;
	}


	/*
		Helper Methods
	*/
	public List<String> getSplitList(String splitMe, String splitBy) {

		List<String> splitList = new List<String>();
		if (splitMe.contains(splitBy))
			splitList = splitMe.split(splitBy);
		else
			splitList.add(splitMe);

		return splitList;
	}

	public String getLookupKey(String lookupKey) {

		lookupKey = Utility.alphanumericOnly(lookupKey);
		if (lookupKey.length() > 18)
			lookupKey = lookupKey.substring(0, 17);

		return lookupKey;
	}


}