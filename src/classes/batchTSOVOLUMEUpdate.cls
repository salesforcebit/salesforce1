global class batchTSOVOLUMEUpdate implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //query on Task
        String query = 'SELECT id,Short_Description__c FROM TSO_Volumes_and_Prices__c';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<TSO_Volumes_and_Prices__c> scope)
    {
         for(TSO_Volumes_and_Prices__c a : scope)
         {   
             //Updating Task status to completed
              a.Short_Description__c= 'Testing@CTS@APEXTIMEDOUTERROR';   
              a.Bill_Point__c = 'a1sQ0000003oh6bIAA';
              a.External_ID__c = 'DDAACKBKPCN-0-0016263872';
              a.Long_Description__c = 'TEST';  
              a.Previous_Month_Pricing__c = 1;
              a.Previous_Month_Volume__c = 23.5;
              a.Short_Description__c = 'TEsting';
              a.Treasury_Service__c = 'a3nQ0000000CTcqIAG';
              a.X12_Month_Blended_Pricing__c = 0.333;
              a.X12_Month_Blended_Volume__c  = 69.23;     
         }
         
         update scope;
    }   
    global void finish(Database.BatchableContext BC)
    {
    }
}