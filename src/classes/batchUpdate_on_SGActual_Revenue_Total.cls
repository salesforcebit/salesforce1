global class batchUpdate_on_SGActual_Revenue_Total implements Database.Batchable<sObject>, Schedulable
{
    
    global batchUpdate_on_SGActual_Revenue_Total()
       {
         // Batch Constructor
       }
       
        // Start Method
       global Database.QueryLocator start(Database.BatchableContext BC){
         string query = 'SELECT ID, Name,User__r.Name, User__c FROM Sales_Goal__c'; 
         
         return Database.getQueryLocator(query);
       }
       global void execute(SchedulableContext SC)
       {
        
       }
        // Execute Logic
       global void execute(Database.BatchableContext BC, List<Sales_Goal__c>scope)
       {
             List <opportunity> opportunity = [SELECT ID, Name,Actual_Fee_Amount__c, CreatedDate FROM opportunity];
             List <OpportunityTeamMember> opportunityTM = [SELECT ID, OpportunityId, UserId, Actual_Fee__c , CreatedById FROM OpportunityTeamMember];
             
             
             List<Sales_Goal__c> monthSGL = new List<Sales_Goal__c >();
             List<Sales_Goal__c> monthSGM = new List<Sales_Goal__c >();
             set<Sales_Goal__c> myset = new set<Sales_Goal__c >();
             
              
             date today = system.today();
             
             for(Sales_Goal__c sg : scope) 
             {
             List<AggregateResult> resultAF  = [select SUM(Actual_Fee__c) total FROM OpportunityTeamMember where UserId=:sg.User__c group by UserId]; 
             //List<AggregateResult> resultAF  = [select SUM(Actual_Fee_Amount__c) total FROM Opportunity where UserId=:sg.User__c group by UserId]; 
             for(OpportunityTeamMember OTM : opportunityTM )  
              {
              if(sg.User__c == OTM.UserId)                 
               {
                for(Opportunity oppy : opportunity)
                {
                 if(OTM.OpportunityId == oppy.Id && oppy.createdDate.year() == today.year())
                 
                      {
                          for (AggregateResult ary : resultAF )
                          {
                          string tesy = String.valueOf(ary.get('total'));
                          system.debug(resultAF);
                          if(ary.get('total') != null)
                          {
                          sg.Actual_Revenue_Total__c = integer.valueOf(tesy);
                          monthSGL.add(sg);
                          } 
                          }
                      }
                   
                 }
                  
               } 
                   
            } 
            
            
       }
        if(monthSGL.size()>0) 
              {
               //system.debug('sum:' +resultAF);  
               system.debug('size:' +monthSGL.size());
               for(Sales_Goal__c s : monthSGL)
                  {
                   if(myset.add(s))
                   {
                      monthSGM.add(s);
                    }
                   }
                   system.debug(monthSGM);
                }   
       update monthSGM;
     }
     
       global void finish(Database.BatchableContext BC){
            // Logic to be Executed at finish
            
            
       }
}