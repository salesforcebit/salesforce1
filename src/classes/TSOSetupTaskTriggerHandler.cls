public without sharing class TSOSetupTaskTriggerHandler {

    //Used to prevent trigger running multiple times due to field updates
    private static Map<Id, TSO_Setup_Task__c> previousNewMap {get {
        if(previousNewMap == null) {
            previousNewMap = new Map<Id, TSO_Setup_Task__c>();
        }
        return previousNewMap;
    }set;}

    private Map<Id, TSO_Setup_Task__c> oldMap {get;set;}
    private Map<Id, TSO_Setup_Task__c> newMap {get;set;}

    private Set<Id> treasuryServiceIds {get;set;}
    private Set<Id> treasuryServiceCheckCompleteIds {get;set;}
    private Set<Id> previouslyOnHoldTreasuryServiceIds {get;set;}
    private Set<Id> treasuryServiceOnHoldIds {get;set;}
    private Set<Id> setupTaskTemplateIds {get;set;}  
    private Map<Id, String> treasuryServiceIdToTaskStageName {get {if(treasuryServiceIdToTaskStageName == null) {treasuryServiceIdToTaskStageName = new Map<Id, String>();}return treasuryServiceIdToTaskStageName;}set;}
    private Map<Id, LLC_BI__Treasury_Service__c> updatedTreasuryServices {get {
        if(updatedTreasuryServices == null) {
            updatedTreasuryServices = new Map<Id, LLC_BI__Treasury_Service__c>();
        }
        return updatedTreasuryServices;
    }set;}

    private Map<String, Set<String>> workflowTemplateAvailableStages {get;set;}
    Map<Id,string> parentMapexceptions = new Map<Id,string>();
    
    public TSOSetupTaskTriggerHandler(Map<Id, TSO_Setup_Task__c> oMap, Map<Id, TSO_Setup_Task__c> nMap) {
        oldMap = oMap;
        newMap = nMap;
        parentMapexceptions = TSOStageChecker_CTS.FullfillmentcheckMap(newMap.values());
        system.debug('parentMapexceptions'+parentMapexceptions);
    }
        public void handleAfterUpdate() {
        processAfterUpdate();
        if(treasuryServiceOnHoldIds.size() > 0) {
            holdStages();
        }
        //Do this first to revert the status back and then any marked as complete can refer to the correct status to advance
        if(previouslyOnHoldTreasuryServiceIds.size() > 0) {
            revertStages();
        }
        if(treasuryServiceCheckCompleteIds.size() > 0) {
            determineAvailableStages();
            advanceStages();
        }
        commitStages();

        incrementParentTaskCount();
        handleafterprocessstages();
      
    }
    public id tsvalueid{set;get;}
    public set<id> TSOcomplete = new set<id>();
    public void handleafterprocessstages(){
    List<TSO_Setup_Task__c> setuptask =  [Select id,Treasury_Service__c FROM TSO_Setup_Task__c where id IN :newmap.values()];
         for(TSO_Setup_Task__c Taskcomp :newmap.values()){  
              system.debug('2');
            if(Taskcomp.Status__c != 'Complete'){
                  system.debug('3');
               TSOcomplete.add(Taskcomp.id);
            } 
              system.debug('4');
            tsvalueid = Taskcomp.Treasury_Service__c;
        } 
        system.debug(' TSO TASK SIZE  ' +TSOcomplete.size());
             if(TSOcomplete.size() == 0){  
                system.debug('5');
        TSOstageshandling trr = new TSOstageshandling(tsvalueid);
            trr.handleafterprocess(); }
    }

    //added by mdorus@westmonroepartners.com
    //4/25/2016 to handle update count on Parent Task
    public void handleAfterInsert() {
        incrementParentTaskCount();
    }

    public void handleAfterDelete() {
        System.debug('inside after delete handler...');
        decrementParentTaskCount();
    }

    public void decrementParentTaskCount() {

        //id is parent ID, list is deleted tasks for each parent
        Map<Id, List<TSO_Setup_Task__c>> parentMap = new Map<Id, List<TSO_Setup_Task__c>>();
        //iterate through tasks.   Add each child to the appropriate parent list in map
        for(TSO_Setup_Task__c task : oldMap.values()){
            if(task.Parent_Setup_Task__c != null){
                List<TSO_Setup_Task__c> childList = parentMap.get(task.Parent_Setup_Task__c);
                if(childList == null){
                    childList = new List<TSO_Setup_Task__c>();
                    parentMap.put(task.Parent_Setup_Task__c, childList);
                }
                childList.add(task);
            }
        }
        //query for the parent tasks of the deleted tasks
        List<TSO_Setup_Task__c> taskList = [Select id, Parent_Setup_Task__c, Number_of_Child_Tasks__c from TSO_Setup_Task__c where id in :parentMap.keyset()];

        //loop through the parents and update their count
        for(TSO_Setup_Task__c task : taskList) {
            System.debug('task ' + task);
            if(task.Number_of_Child_Tasks__c != null && task.Number_of_Child_Tasks__c - parentMap.get(task.id).size() >= 0){
                task.Number_of_Child_Tasks__c = task.Number_of_Child_Tasks__c - parentMap.get(task.id).size();
            }
        }
        System.debug('tasklist'+taskList);
        if(taskList.size() > 0)
            update taskList;
    }

    //used for incrementing the parent task the Number_of_Child_Tasks__c field
    public void incrementParentTaskCount() {
        Set<Id> parentIdSet = new Set<Id>();

        //get all the parent tasks for new inserted tasks
        system.debug('parentMapexceptions'+parentMapexceptions);
        for(TSO_Setup_Task__c newTask : newMap.values()) {
            //rajesh//
            if(newTask.Treasury_Service__c != null && parentMapexceptions.containsKey(newTask.Treasury_Service__c) && parentMapexceptions.size()>0){
                newTask.adderror(parentMapexceptions.get(newTask.Treasury_Service__c));
            }    
            //rajesh//
            if(newTask.Parent_Setup_Task__c != null){
                parentIdSet.add(newTask.Parent_Setup_Task__c);
            }
        }

        //for the list of parent tasks, select all the child tasks and parent tasks
        List<TSO_Setup_Task__c> taskList = [Select id, Parent_Setup_Task__c, Number_of_Child_Tasks__c from TSO_Setup_Task__c where Parent_Setup_Task__c in :parentIdSet or id in :parentIdSet];

        //seperate the parent and childtask;
        List<TSO_Setup_Task__c> parentTasks = new List<TSO_Setup_Task__c>();

        //id is parent ID, list is child tasks
        Map<Id, List<TSO_Setup_Task__c>> parentMap = new Map<Id, List<TSO_Setup_Task__c>>();
        //iterate through childTasks.   Add each child to the appropriate parent list in map
        for(TSO_Setup_Task__c task : taskList){
            if(task.Parent_Setup_Task__c != null){
                List<TSO_Setup_Task__c> childList = parentMap.get(task.Parent_Setup_Task__c);
                if(childList == null){
                    childList = new List<TSO_Setup_Task__c>();
                    parentMap.put(task.Parent_Setup_Task__c, childList);
                }

                childList.add(task);
            } else {  //if the parent task id is null, its a child task
                parentTasks.add(task);
            }
        }

        List<TSO_Setup_Task__c> updatedTasks = new List<TSO_Setup_Task__c>();
        //update the count on the parent task
        for(TSO_Setup_Task__c task : parentTasks){

            List<TSO_Setup_Task__c> childList = parentMap.get(task.id);
            if(childList != null){
                if(task.Number_of_Child_Tasks__c != childList.size()){
                    task.Number_of_Child_Tasks__c = childList.size();
                    updatedTasks.add(task);
                }
            }
        }

        if(updatedTasks.size() > 0){
            update updatedTasks;
        }
    }

@testvisible
    private void processAfterUpdate() {
        treasuryServiceIds = new Set<Id>();
        treasuryServiceCheckCompleteIds = new Set<Id>();
        previouslyOnHoldTreasuryServiceIds = new Set<Id>();
        treasuryServiceOnHoldIds = new Set<Id>();
        setupTaskTemplateIds = new Set<Id>();
      
        for(TSO_Setup_Task__c newTask : newMap.values()) {
            TSO_Setup_Task__c oldTask = previousNewMap.get(newTask.Id) == null ? oldMap.get(newTask.Id) : previousNewMap.get(newTask.Id);

               
            //Setup Task On Hold Criteria
            if(newTask.Status__c == Constants.TSOSetupTaskStatusOnHold && newTask.Status__c != oldTask.Status__c) {
                treasuryServiceOnHoldIds.add(newTask.Treasury_Service__c);
            }
            else {
                //Setup Task no longer on hold criteria
                if(newTask.Status__c != oldTask.Status__c && oldTask.Status__c == Constants.TSOSetupTaskStatusOnHold) {
                    previouslyOnHoldTreasuryServiceIds.add(newTask.Treasury_Service__c);
                    treasuryServiceIdToTaskStageName.put(newTask.Treasury_Service__c, newTask.Related_Stage__c);
                }
                //Setup Task Stage Update Complete Criteria
                if( (newTask.Status__c == Constants.TSOSetupTaskStatusComplete && newTask.Status__c != oldTask.Status__c)) {
                    treasuryServiceCheckCompleteIds.add(newTask.Treasury_Service__c);
                    setupTaskTemplateIds.add(newTask.Setup_Task_Template__c);
                }
            }
            previousNewMap.put(newTask.Id, newTask);
        }
    }
@testvisible
    private void determineAvailableStages() {
        workflowTemplateAvailableStages = new Map<String, Set<String>>();
        Set<Id> workflowTemplateIds = new Set<Id>();
        for(TSO_Setup_Task_Template__c taskTemplate : [Select Id, Parent_Stage__r.Workflow_Template__c, Parent_Stage__c From TSO_Setup_Task_Template__c Where Id in: setupTaskTemplateIds And Parent_Stage__c != null]) {
            workflowTemplateIds.add(taskTemplate.Parent_Stage__r.Workflow_Template__c);
        }

        for(TSO_Workflow_Template__c workflowTemplate : [Select Id, Product__c, Type__c, (Select Id, Related_Treasury_Stage__c From Stages__r) From TSO_Workflow_Template__c t]) {
            workflowTemplateAvailableStages.put(generateKey(workflowTemplate), new Set<String>());
            for(TSO_Stage__c stage : workflowTemplate.Stages__r) {
                workflowTemplateAvailableStages.get(generateKey(workflowTemplate)).add(stage.Related_Treasury_Stage__c);
            }
        }
    }
      public List<TSO_Setup_Task__c> setuptasklist1 = new List<TSO_Setup_Task__c>();
@testvisible
    private void advanceStages() {
        
      //Added for Defect 2188 By CTS offshore
    //Begin
        map<id,LLC_BI__Treasury_Service__c> tsparent = new  map<id,LLC_BI__Treasury_Service__c>([Select Id, LLC_BI__Stage__c,Is_this_a_KTT_client__c, LLC_BI__Product_Reference__c, Type__c from LLC_BI__Treasury_Service__c Where Id in: treasuryServiceCheckCompleteIds] );
       
        for(TSO_Setup_Task__c setuptask :[Select id,Related_Stage__c,Status__c,Treasury_Service__c  From TSO_Setup_Task__c where Treasury_Service__c in :tsparent.keySet()]){
            if( setuptask.Related_Stage__c == tsparent.get(setuptask.Treasury_Service__c).LLC_BI__Stage__c && setuptask.Status__c != Constants.TSOSetupTaskStatusComplete){
                System.debug('setuptask111'+setuptask);
                setuptasklist1.add(setuptask);
            }
        } 
        //End
        system.debug('setuptasklist......'+setuptasklist1.size());   
        for(LLC_BI__Treasury_Service__c treasuryService : [Select Id, LLC_BI__Stage__c,Is_this_a_KTT_client__c, LLC_BI__Product_Reference__c, Type__c, (Select Id from Setup_Tasks__r Where Status__c !=: Constants.TSOSetupTaskStatusComplete) from LLC_BI__Treasury_Service__c Where Id in: treasuryServiceCheckCompleteIds]) {
            //If there are only Complete Tasks, we want to move to the next stage
            System.debug('treasuryService::'+treasuryService);
            System.debug('setup task size ' + treasuryService.Setup_Tasks__r.size());
            System.debug('stage ' + treasuryService.LLC_BI__Stage__c);
     
            if((setuptasklist1.size() == 0 && treasuryService.LLC_BI__Stage__c != Constants.treasuryServiceStageOrderEntry) || (treasuryService.Setup_Tasks__r.size() == 0 && treasuryService.LLC_BI__Stage__c != Constants.treasuryServiceStageOrderEntry) ){
                //Check to see if the treasuryService is already in the update map, such as if it was previously on hold
                if(updatedTreasuryServices.containsKey(treasuryService.Id)) {
                    treasuryService = updatedTreasuryServices.get(treasuryService.Id);
                }
                //Boolean stagechecker = TSOStageChecker_CTS.Fullfillmentcheck(treasuryService,);
                
                //String nextStage;
                //if(stagechecker == true){
                    string nextStage = getNextAvailableStage(treasuryService);
                //}
                system.debug('nextStagee'+nextStage);
                //System.debug('treasuryService.LLC_BI__Stage__c::'+treasuryService.LLC_BI__Stage__c);
                //System.debug(treasuryService.LLC_BI__Stage__c + '::'+ 'Pending – Request Returned');
                //System.debug(treasuryService.LLC_BI__Stage__c == 'Pending – Request Returned');
                //System.debug(treasuryService.LLC_BI__Stage__c.escapeUnicode() );
                //System.debug(treasuryService.LLC_BI__Stage__c.escapeUnicode() == 'Pending - Request Returned');
                //System.debug('Boolean If Statement::'+treasuryService.LLC_BI__Stage__c.escapeUnicode() != ('Pending – Request Returned') );

                if(nextStage != null && treasuryService.LLC_BI__Stage__c.escapeUnicode() != ('Pending - Request Returned')  ) {
                    //Ensure that the treasury service is in the update map
                    if(!updatedTreasuryServices.containsKey(treasuryService.Id)) {
                        updatedTreasuryServices.put(treasuryService.Id, treasuryService);
                    }
                    System.debug('Updating stage to ' + nextStage);
                    //updatedTreasuryServices.get(treasuryService.Id).LLC_BI__Stage__c = nextStage;
                    //do not advance the stage if the stage is currently Order Entry
                    //if(updatedTreasuryServices.get(treasuryService.Id).LLC_BI__Stage__c != 'Order Entry')
                        updatedTreasuryServices.get(treasuryService.Id).LLC_BI__Stage__c = nextStage;
                }
            }
        }
    }
@testvisible
    private void holdStages() {
        for(Id treasuryServiceId : treasuryServiceOnHoldIds) {
            if(!updatedTreasuryServices.containsKey(treasuryServiceId)) {
                updatedTreasuryServices.put(treasuryServiceId, new LLC_BI__Treasury_Service__c(Id = treasuryServiceId));
            }
            updatedTreasuryServices.get(treasuryServiceId).LLC_BI__Stage__c = Constants.treasuryServiceStagePendingTaskOnHold;
        }
    }
@testvisible
    private void revertStages() {
        for(LLC_BI__Treasury_Service__c treasuryService : [Select Id, LLC_BI__Stage__c, LLC_BI__Product_Reference__c, Type__c, (Select Id from Setup_Tasks__r Where Status__c =: Constants.TSOSetupTaskStatusOnHold) from LLC_BI__Treasury_Service__c Where Id in: previouslyOnHoldTreasuryServiceIds]) {
            //If there are no remaining On Hold Tasks, we want to revert it to the active stage
            if(treasuryService.Setup_Tasks__r.size() == 0 ) {
                String activeStage = treasuryServiceIdToTaskStageName.get(treasuryService.Id);
                if(!updatedTreasuryServices.containsKey(treasuryService.Id)) {
                    updatedTreasuryServices.put(treasuryService.Id, treasuryService);
                }
                updatedTreasuryServices.get(treasuryService.Id).LLC_BI__Stage__c = activeStage;
            }
        }
    }
    @testvisible
    private void commitStages() {
        if(updatedTreasuryServices.size() > 0) {
            System.debug('updatedTreasuryServices.values()::'+updatedTreasuryServices.values());
            try{
                update updatedTreasuryServices.values();
            } catch(System.DmlException e){
                updatedTreasuryServices.values().get(0).addError(e.getDmlMessage(0));
             
            }
        }
    }

    public String getNextAvailableStage(LLC_BI__Treasury_Service__c treasuryService) {
        Set<String> availableStages = workflowTemplateAvailableStages.get(generateKey(treasuryService));
        //String nextStage;
        String nextStage = stageToNextStage.get(treasuryService.LLC_BI__Stage__c);
        System.debug(nextStage);
        System.debug(availableStages);
        System.debug(availableStages.contains(nextStage));
        while(nextStage != null && !availableStages.contains(nextStage)) {
            nextStage = stageToNextStage.get(nextStage);
        }
        return nextStage;
    }

    private Map<String, String> stageToNextStage {get {
        if(stageToNextStage == null) {
            stageToNextStage = new Map<String, String>();
            for(Treasury_Service_Stage_Order__mdt stageOrder : [Select Id, Current_Stage__c, Next_Stage__c from Treasury_Service_Stage_Order__mdt ]) {
                stageToNextStage.put(stageOrder.Current_Stage__c, stageOrder.Next_Stage__c);
            }
        }
        return stageToNextStage;
    }set;}

    public String generateKey(LLC_BI__Treasury_Service__c ts) {
        return ts.LLC_BI__Product_Reference__c + ts.Type__c;
    }

    public String generateKey(TSO_Workflow_Template__c workflowTemplate) {
        return workflowTemplate.Product__c + workflowTemplate.Type__c;
    }

    //before insert, before update
    //wil populate the product package lookup field on the setup task record if it is blank and the treasury sevrice lookup is populated or changed
    public static void updateProductPackage(List<TSO_Setup_Task__c> newList, Map<id, TSO_Setup_Task__c> oldMap){
        //intialize the variables
        Map<id, LLC_BI__Treasury_Service__c> id2TreasuryServiceMap = new Map<Id, LLC_BI__Treasury_Service__c>();
        Set<Integer> setupTaskLoc = new Set<Integer>();
        Map<Id, Id> setupTaskId2ProdPackId = new Map<Id, Id>();

        for(integer i= 0; i < newList.size() ; i++ ){
            System.debug('newList[i].Treasury_Service__c::'+newList[i].Treasury_Service__c);
            if(oldMap == null){//insert
                if(newList[i].Treasury_Service__c != null){//treasury service is populated and product package is null
                    setupTaskLoc.add(i);
                    id2TreasuryServiceMap.put(newList[i].Treasury_Service__c, null);
                }
            } else {//update - treasury service is not null and (treasury service changes or product package is null)
                if(newList[i].Treasury_Service__c != null && (newList[i].Treasury_Service__c != oldMap.get(newList[i].id).Treasury_Service__c || newList[i].Product_Package__c == null) ){
                    setupTaskLoc.add(i);
                    id2TreasuryServiceMap.put(newList[i].Treasury_Service__c, null);
                }
            }
        }

        //if at least one record meets the criteria set above; execute the below logic
        if(setupTaskLoc.size() > 0){
            //query for the treasury service record
            id2TreasuryServiceMap = new Map<Id, LLC_BI__Treasury_Service__c>( [SELECT id, LLC_BI__Product_Package__c FROM LLC_BI__Treasury_Service__c WHERE Id IN:id2TreasuryServiceMap.keyset()] );

            //loop through the ids for the setup task
            for(Integer i : setupTaskLoc){
                if( id2TreasuryServiceMap.get(newList[i].Treasury_Service__c).LLC_BI__Product_Package__c != null ){
                    if(oldMap != null){
                        newList[i].Product_Package__c = id2TreasuryServiceMap.get(newList[i].Treasury_Service__c).LLC_BI__Product_Package__c;
                    } else {
                        setupTaskId2ProdPackId.put(newList[i].id, id2TreasuryServiceMap.get(newList[i].Treasury_Service__c).LLC_BI__Product_Package__c);
                    }
                }
            }
                
            System.debug('old map is null? '+oldMap+' - '+setupTaskId2ProdPackId);
            if(oldMap == null){
                updateSetupTaskWProductPackage(setupTaskId2ProdPackId);
            }
        }
    }

    @future
    public static void updateSetupTaskWProductPackage(Map<Id, Id> setupTaskId2ProdPackId){
        //initialize the var
        List<TSO_Setup_Task__c> updateList = new List<TSO_Setup_Task__c>();

        for(Id i : setupTaskId2ProdPackId.keyset()){
            updateList.add(new TSO_Setup_Task__c(Id=i, Product_Package__c = setupTaskId2ProdPackId.get(i)) );
        }

        update updateList;
    }
}