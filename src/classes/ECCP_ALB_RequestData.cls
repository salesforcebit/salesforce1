global class ECCP_ALB_RequestData{

    //global class CreateLoanRequestType {
    public ECCP_ALB_RequestData(){}
    webservice string errormessage; //added by ganga to Fix 645 defect 12/6/2016
    webservice List<GeneralDataType> GeneralData;
    webservice CustomDataType CustomData;
    webservice List<SignerDataType> SignerData;
    webservice List<GuarantorDataType> GuarantorData;
    webservice List<CollateralDataType> CollateralData;
    webservice List<DisbursementDataType> DisbursementData;
    webservice List<FeeInfoType> FeeInfo;
    webservice List<BorrowerDataType> BorrowerData;
    //}

    global class CustomDataType {  
    
        webservice String HostSystem;
        webservice String CRASalesCode;
        webservice Date LoanAgreementEndDate;
        webservice Decimal ComplianceCertificateDeliveryGracePeriod; // Aditya : 3August 2016 Relese 1.1 change
        webservice String RequireAnnualStatements;
        webservice String AnnualStatementPreparedby;
        webservice String RequireInterimStatements;
        webservice String RequireCopiesofTaxReturns;
        webservice Decimal DueDaysforTaxReturns;
        // Aditya : 31August 2016 Relese 2.0 change Start
        /*
        webservice List<Date> DateofLease;
        webservice List<String> StreetAddress;
        webservice List<String> City;
        webservice List<String> State;
        webservice List<String> Zipcode;
        */
        webservice List<LandlordsConsentType> LandlordsConsent; // Aditya : 2.0 change 31 August 2016
        // Aditya : 3August 2016 Relese 1.1 change Start
        webservice String MaximumAvailability;
        webservice Decimal MaximumAgeofEligibleAccounts;
        webservice Decimal AdditionalLimitEligibleAccounts;
        // Aditya : 3August 2016 Relese 1.1 change End
        webservice Decimal MaximumLoanAmount;
        // Aditya : 3August 2016 Relese 1.1 change Start
        webservice String InventoryEligibleExclusions;
        webservice String EligibleInventoryAdditionalLimitations;
        // Aditya : 3August 2016 Relese 1.1 change End
        webservice Decimal TotalAmountBorrowerOwestoCreditor;
        webservice String InterestOnlyPayments;
        webservice String PeriodicPaymentDetails;
        webservice Decimal CreditorPaymentAmount;
        webservice String PrincipalPaymentFrequency;
        webservice List<CovenantType> Covenant;
    
    }
    global class CollateralGrantorInformationType {
        // Aditya : 3August 2016 Relese 1.1 change Start
        webservice String CollateralKey;
        webservice String CollateralGrantorKey;
        // Aditya : 3August 2016 Relese 1.1 change End
        webservice String GrantorDBA;  // Aditya : 2.0 change 31 August 2016
        webservice String CGOrganizationType;
        webservice String CGFirstNameandMiddleNname;
        webservice String CGLastname;
        webservice String CGBusinessName;
        webservice String CGTaxIdentificationNumber;
        webservice String CGFullAddress;
        webservice String CGCity;
        webservice String CGCounty;
        webservice String CGState;
        webservice String CGPostalCode;
        webservice String CGPrimaryPhone;
        webservice Decimal CollateralID;
        //Syed : added for defect number 548
        webservice String obligorNumber;
    }
    // Aditya : 2.0 change 31 August 2016
    global class LandlordsConsentType {
        webservice Date DateofLease;
        webservice String StreetAddress;
        webservice String City;
        webservice String State;
        webservice String Zipcode;
    }
    global class BorrowerDataType { 
        webservice String BorrowerKey;   // Aditya : 3August 2016 Relese 1.1 change
        webservice String BorrowerDBA;   // Aditya : 2.0 change 31 August 2016
        webservice String OrganizationType;
        webservice String Type_x;
        webservice String FirstnameMiddlename;
        webservice String LastName;
        webservice String BusinessName;
        webservice String TaxIdentificationNumber;
        webservice String Address;
        webservice String City;
        webservice String County;
        webservice String State;
        webservice String postalcode;
        webservice String primaryPhoneNumber;
        webservice String NAICSCode;
        webservice String IncorporationState;
        webservice String OrganizationId;
        webservice String CustomerNumber;
        webservice String RiskRating;
        webservice String CreditScore; // Aditya : 2.0 change 31 August 2016
    }
      
    global class DisbursementDataType { 
        webservice String DisbursementDescription;
        webservice String DisbursementPaymentNumber;
        webservice String DisbursementAccountType;
        webservice Decimal TotalDisbursed;
    }
    global class GeneralDataType {     
        webservice String ProductKey;
        webservice String CRACode;
        // Aditya : 3August 2016 Relese 1.1 change Start
        webservice Date ClosingDate;
       
        webservice String ObligationNumber;
        // Aditya : 3August 2016 Relese 1.1 change End
        webservice Decimal OriginalAmount;
        webservice String RecordID;
        webservice String AutomaticLoanPayment;
        webservice String BorrowerState;
        webservice String Branch;
        webservice String RiskType;
        webservice String AssignmentUnit;
        webservice Decimal ProposedObligorRiskRating;
        webservice Decimal InterestRate;
        webservice Date PaymentDueDate;  // Aditya : 3August 2016 Relese 1.1 change 
        webservice Date FirstInterestPaymentDate; // Aditya : 2.0 change 31 August 2016
        webservice Date FirstPaymentDate;
        webservice String InterestType;
        webservice String InterestPaymentSchedule; // Aditya : 2.0 change 31 August 2016
        webservice String PaymentType;   
        webservice Decimal InitialLoanInterestRate;
        webservice Integer NumberofPayments;
        webservice String PaymentSchedule;
        webservice Decimal Margin;
        webservice Decimal PeriodstoAmortize;
        webservice Decimal RateCeiling;
        webservice Decimal RateFloor;
        webservice String RateType;
        webservice String RateDescription;
        webservice String LoanPurposeCode;
        webservice String BankNumber; // Aditya : 3August 2016 Relese 1.1 change 
        // Aditya : 2.0 change 31 August 2016
        webservice String RMOfficerCode;
        webservice String InsiderLoanYN;
        webservice String RestrictAccessYN;
        webservice String RegOLoan;
        webservice String PrePmtPenaltyYN;
        webservice String DDAAccountNumber;
        webservice String LoanAgreementType;
        webservice String ObligationRiskRating;
        webservice String NewMoney;
        webservice String CreditRatingDate;
        webservice String ProbabilityofDefault;
        webservice String DateRated;
        webservice String LossGivenDefault;
        webservice String PrepaymentPenaltyCode;
        // 2.0 change End
    }
    global class CollateralDataType { 
        webservice String CollateralKey; // Aditya : 3August 2016 Relese 1.1 change
        webservice String UCCState;
        webservice String GeneralCollateralType;
        webservice String CollateralSubType;
        webservice String AgriculturalUse;
        webservice String CollateralDescription;
        webservice String BodyStyle;
        webservice String CollateralCity;
        webservice String CollateralCounty;
        webservice String CollateralState;
        webservice String Make;
        webservice String Model;
        webservice String PossessoryAccountNumber;
        webservice Decimal PossessoryCurrentBalance;
        webservice Decimal NumberofUnits;
        webservice Decimal CollateralID;
        webservice String StreetAddress;
        webservice String PropertyTaxID;
        webservice String VehicleIdentificationNumber;
        webservice String Year;
        webservice String Zipcode;
        webservice String FirstLienIndicator;
        webservice String FloodSectionDetail;
        webservice String BookEntry;
        webservice String LifeInsurancePolicyNumber;
        webservice Integer LifeInsuranceBenefitAmount;
        webservice String Nameoftheinsured;
        webservice String LifeInsuranceCompany;
        webservice String LifeInsuranceCompanyStreetAdress;
        webservice String LifeInsuranceCompanyCity;
        webservice String LifeInsuranceCompanyState;
        webservice String LifeInsuranceCompanyZipcode;
        webservice String CollateralType;
        webservice String Manufacturer;
        webservice String FAARegistrationNumber;
       // webservice String UCCTypes; // Aditya : 3August 2016 Relese 1.1 change
        webservice String VesselName;
        webservice String VesselNumber;
        webservice String StateRegistered;
        webservice String CollateralInsuranceAmount; // Aditya : 2.0 change 31 August 2016
        webservice CollateralGrantorInformationType CollateralGrantorInformation;
        
    }
    global class CovenantType {     
        webservice String CovenantId;       
        webservice String ComplianceCertificateDeliveryFrequency;       
        webservice String DueXDaysAfterFYEAnnual;       
        webservice String IntermStatementPreparedby;        
        webservice String DueXDaysAfterInterm;      
        webservice String IntermStatementDelivered;     
        webservice String WhoPreparesTaxReturns;        
        webservice List<String> Covenants;
    }
    // 2.0 Change End
    global class FeeInfoType {  
        webservice String Feetype;
        webservice Decimal PrepaidFinanceChargeCash;
        webservice String PrepaidFinanceChargeDescription;
        webservice Decimal PrepaidFinanceChargeFinanced;
    }
    global class GuarantorDataType {  
        // Aditya : 3August 2016 Relese 1.1 change Start
        webservice String GuarantorKey;
        webservice String GuarantorDBA;  // Aditya : 2.0 change 31 August 2016
        webservice String GuarantorOrganizationType;  // New in LPL Relase 1.1
        webservice String GuarantorEntityRoles;
        // Aditya : 3August 2016 Relese 1.1 change End
        webservice String GuarantorFirstNameandMiddleNname;
        webservice String GuarantorLastname;
        webservice String GuarantorBusinessName;
        webservice String GuarantorTaxIdentificationNumber;
        webservice String GuarantorAddress;
        webservice String GuarantorCity;
        webservice String GuarantorState;
        webservice String GuarantorPostalCode;
        webservice String NonIndividualGuarantorOrganizationType;
        webservice String GuarantorAuthority;
        webservice String NonProfitCorporation;
        webservice String GuarantorNAICSCode;
        // Aditya : 2.0 change 31 August 2016
        webservice String GuarantorCustomerNumber;
        webservice String GuarSecuredByCollateralYN;
        webservice String AddReqAnnualStmtsYN;
        webservice String AddReqAnnualStmtPrepBy;
        webservice String AddReqInterimStmtsYN;
        webservice String AddReqCopiesOfTaxReturnsYN;
        webservice Decimal ObligorNumber;
        webservice List<GuarantorCovenantType> GuarantorCovenant;
     
        // 2.0 change End
    }
    // Release 2.0 Change
    Global class GuarantorCovenantType {        
        webservice String CovenantId;       
        webservice String AddReqAnnualStmtDueWithin;        
        webservice String AddReqInterimStmtPrepBy;      
        webservice String AddReqInterimStmtDueWithin;       
        webservice String AddReqTaxReturnPrepBy;        
        webservice String AddReqTaxReturnDueWithin;
    }
     
    
    global class SignerDataType {
        // Aditya : 3August 2016 Relese 1.1 change Start
        webservice String SignorKey;
        webservice String BorrowerKey;
        webservice String GuarantorKey;
        //Syed : added for defect no:541
        webservice String GrantorKey;
        // Aditya : 3August 2016 Relese 1.1 change End
        webservice String SignorDBA; // Aditya : 2.0 change 31 August 2016
        webservice String OrganizationType;
        webservice String EntityRoles;
        webservice String FirstNameandMiddleNname;
        webservice String Lastname;
        webservice String BusinessName;
        webservice String TaxIdentificationNumber;
        webservice String FullBillingAddress;
        webservice String BillingCity;
        webservice String BillingCounty;
        webservice String BillingState;
        webservice String BillingPostalCode;
        webservice String PrimaryPhoneNumber;
        webservice String Title;
        webservice String Authority;
        webservice String NAICSCode;
        webservice String IncorporationState;
        webservice String OrganizationID;
    }
   

}