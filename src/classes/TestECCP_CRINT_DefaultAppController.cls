@isTest
private class TestECCP_CRINT_DefaultAppController {
    
    @isTest
    private static void testAppParameterNotSet_SetToLoanDefaultApp() {
        LLC_BI__Loan__c testLoan = new LLC_BI__Loan__c(Name = 'TestLoan', ECCP_CRINT_App__c = 'crint.product_package');
        insert testLoan;
        
        PageReference pageRef = Page.ECCP_CRINT_Loan;
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        ApexPages.StandardController ctrl = new ApexPages.StandardController(testLoan);
        ECCP_CRINT_DefaultAppController xCtrl = new ECCP_CRINT_DefaultAppController(ctrl);
        
        Test.stopTest();
        
        Map<String, String> params = ApexPages.currentPage().getParameters();
        
        System.assertEquals('crint.product_package', params.get(nFORCE.RouteController.PARAM_APP_REQUEST));
    }
    
    @isTest
    private static void testAppParameterSet_NotOverridden() {
        LLC_BI__Loan__c testLoan = new LLC_BI__Loan__c(Name = 'TestLoan', ECCP_CRINT_App__c = 'crint.product_package');
        insert testLoan;
        
        PageReference pageRef = Page.ECCP_CRINT_Loan;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(nFORCE.RouteController.PARAM_APP_REQUEST, 'product_package');
        
        Test.startTest();
        
        ApexPages.StandardController ctrl = new ApexPages.StandardController(testLoan);
        ECCP_CRINT_DefaultAppController xCtrl = new ECCP_CRINT_DefaultAppController(ctrl);
        
        Test.stopTest();
        
        Map<String, String> params = ApexPages.currentPage().getParameters();
        
        System.assertEquals('product_package', params.get(nFORCE.RouteController.PARAM_APP_REQUEST));
    }
    
    @isTest
    private static void testDefaultAppRemoved_AppSetToDefaultValue() {
        LLC_BI__Loan__c testLoan = new LLC_BI__Loan__c(Name = 'TestLoan');
        insert testLoan;
        
        System.assert(String.isBlank(testLoan.ECCP_CRINT_App__c));
        
        PageReference pageRef = Page.ECCP_CRINT_Loan;
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        ApexPages.StandardController ctrl = new ApexPages.StandardController(testLoan);
        ECCP_CRINT_DefaultAppController xCtrl = new ECCP_CRINT_DefaultAppController(ctrl);
        
        Test.stopTest();
        
        Map<String, String> params = ApexPages.currentPage().getParameters();
        
        System.assertEquals('crint.product_package', params.get(nFORCE.RouteController.PARAM_APP_REQUEST));
    }
}