/*
This class is written for KEF KRR Project by TCS Team

It is a handler class for Agreement related requirements
*/
public with sharing class AgreementHandler 
{
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    /* below maps are used for reports and dashboard requirement 148.   */
    
    private static Map<String, KEFReportFieldPopulator> statusValueMap;
    private static Map<String, KEFReportFieldPopulator> fieldAuditMap;
    
    public AgreementHandler(boolean isExecuting, integer size)
    {
        m_isExecuting = isExecuting;
        BatchSize = size; 
        
        /* below code block is written for Req. 148 - reports and dashboards   */
       
        if(statusValueMap == null)
        { 
            statusValueMap = new Map<String, KEFReportFieldPopulator>();
              //preparing a map with status value as key and related wrapper field mapping as value
            statusValueMap = AgreementHelper.populateMapForStatusChangeAudit();
        }
        
        if(fieldAuditMap == null)
        {
            fieldAuditMap = new Map<String, KEFReportFieldPopulator>();
            //preparing a map with field API name as key and related wrapper field mapping as value
            fieldAuditMap = AgreementHelper.populateMapForFieldChangeAudit();
        }
        /* Req. 148 block end.  */       
    } 
    //Calculation of Vendor Agreement End Date 
    public void OnBeforeInsert(Agreement__c[] triggerNewList)
    {
        if(triggerNewList != null && triggerNewList.size() > 0)
        {
            for(Agreement__c agr : triggerNewList)
            {
                AgreementHelper.calculateEnddate(agr); 
            
            
                /* below code block is written for Req. 148 - reports and dashboards   
                In this created date and creating user is populated whenever agreement status or any other field set on agreement.   */
             
                if(agr.Agreement_Status__c != null)
                {                
                    AgreementHelper.processAgreementforDRDataPopulation(agr, statusValueMap);                         
                } 
                
                /*  Defect 147 - Agreement name change date should not be populated while inserting agreement.
                for(String fieldAPI : fieldAuditMap.keyset())
                {
                    //if the field value is changed, populate DateTime and User id for those using a wrapper.
                    if(agr.get(fieldAPI) != null )
                    {
                        AgreementHelper.populateStatusDateAndUserId(agr, fieldAuditMap.get(fieldAPI));
                    } 
                } */
                /* Req. 148 block end.  */
            
                /*this code will check if the field value has valid email address separated by semi-colon */
                if(agr.Internal_Notification_Email_Address__c != null && 
                AgreementHelper.validateEmailAddress(agr.Internal_Notification_Email_Address__c))
                {
                    agr.Internal_Notification_Email_Address__c.addError(System.Label.EnterValidEmailId);
                }
            }
        }
    }  
    
    public void OnAfterInsert(Agreement__c[] triggerNewList, Map<ID, Agreement__c> triggerNewMap)
    {
        if(triggerNewList != null && triggerNewList.size() > 0)
        {            
          AgreementHelper.UpdateNotesAndAttachment(triggerNewList); 
        }
    }
    /*
    @future public static void OnAfterInsertAsync(Set<ID> newAgreementIDs)
    {
        
    }*/
    
    public void OnBeforeUpdate(Agreement__c[] triggerOldList, Agreement__c[] triggerNewList, Map<ID, Agreement__c> triggerNewMap)
    {
        if(triggerNewMap != null && !triggerNewMap.isEmpty() && triggerOldList != null && triggerOldList.size() > 0)
        {
            for(Agreement__c oldAgr : triggerOldList)
            {
                Agreement__c newAgr = triggerNewMap.get(oldAgr.id);
                
                //checking if any of the date/term  is changed, if changed then only call this method 
                if(oldAgr.Agreement_Commencement_Date__c != newAgr.Agreement_Commencement_Date__c ||
                oldAgr.Agreement_Renewal_Date__c != newAgr.Agreement_Renewal_Date__c ||
                oldAgr.Agreement_Term__c != newAgr.Agreement_Term__c)
                {
                  AgreementHelper.calculateEnddate(newAgr);  
                } 
                
                /* below code block is written for Req. 148 - reports and dashboards   
                In this modification date and modifying user is populated whenever agreement status or any other field changes.   */
                       
                if(newAgr.Agreement_Status__c != null && newAgr.Agreement_Status__c != oldAgr.Agreement_Status__c)
                {                
                    AgreementHelper.processAgreementforDRDataPopulation(newAgr, statusValueMap);                         
                } 
                
                //traversing the map created for checking fields that are changed.
                for(String fieldAPI : fieldAuditMap.keyset())
                {
                    system.debug('in_UPDATE-----fieldAPI      '+fieldAPI);
                    system.debug('in_UPDATE-----newAgr.get(fieldAPI)      '+newAgr.get(fieldAPI));
                    system.debug('in_UPDATE-----oldAgr.get(fieldAPI)      '+oldAgr.get(fieldAPI));
                    //if the field value is changed, populate DateTime and User id for those using a wrapper.
                    if(fieldAPI != null && newAgr.get(fieldAPI) != oldAgr.get(fieldAPI))
                    {
                        /*  Defect 147 - Agreement name change date should not be populated while inserting agreement. Hence bypassing 
                        if Agreement name is blank.*/
                        if(fieldAPI == System.Label.AgreementNameAPIName && oldAgr.get(fieldAPI) == null)
                        {
                            continue;
                        }
                        /*  Defect 147 - Agreement name change date should not be populated while inserting agreement.*/
                        AgreementHelper.populateStatusDateAndUserId(newAgr, fieldAuditMap.get(fieldAPI));
                    }
                }
                /*Req 148 code block end */    
                /*this code will check if the field value has valid email address separated by semi-colon */
                if(newAgr.Internal_Notification_Email_Address__c != null && 
                newAgr.Internal_Notification_Email_Address__c != oldAgr.Internal_Notification_Email_Address__c)
                {
                    if(AgreementHelper.validateEmailAddress(newAgr.Internal_Notification_Email_Address__c))
                    {
                        newAgr.Internal_Notification_Email_Address__c.addError(System.Label.EnterValidEmailId);
                    }
                }            
            }
        }        
    }
    
    public void OnAfterUpdate(Agreement__c[] triggerOldList, Agreement__c[] triggerNewList, Map<ID, Agreement__c> triggerNewMap)
    {
        
    }
    
    public void OnBeforeDelete(Agreement__c[] triggerOldList, Map<ID, Agreement__c> triggerOldMap)
    {
        for(Agreement__c agr : triggerOldList)
        {
            /* START - Code block added for Req. 174 - Release 3*/
            //Code added for restricting user from Deleting the agreement if it is integrated with external system.
            if(agr.Integrated__c)
            {
                agr.addError(System.Label.AgreementDeleteIntegratedError);
            }
            /* END - Code block added for Req. 174 - Release 3*/
        }    
    }
    
    /*
    @future public static void OnAfterUpdateAsync(Set<ID> updatedAgreementIDs)
    {
    
    }*/
    /*
    
    
    public void OnAfterDelete(Agreement__c[] triggerOldList, Map<ID, Agreement__c> triggerOldMap)
    {
        
    }
    
    @future public static void OnAfterDeleteAsync(Set<ID> deletedAgreementIDs)
    {
        
    }
    
    public void OnUndelete(Agreement__c[] triggerNewList)
    {
        
    }
    
    public boolean IsTriggerContext
    {
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext
    {
        get{ return !IsTriggerContext;}
    }*/
}