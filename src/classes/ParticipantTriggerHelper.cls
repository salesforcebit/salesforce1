//================================================================================
//  KeyBank  
//  Object: ParticipantTriggerHelper
//  Author: Offshore
//  Detail: Participant Trigger Helper Class
//================================================================================
//          Date            Purpose
// Created: 03/07/2014      Initial Version
// Changes: 06/29/2015      July release 
//================================================================================

public without sharing class ParticipantTriggerHelper
{
    @future public static void updateRelatedRecords(list<id> pIds){
        system.Debug('pIds:'+pIds);
        List<Dual_Coverage__c> dualCovList = new List<Dual_Coverage__c>();
        List<Program_Member__c> prgmList = new List<Program_Member__c>();
        List<Id> assocAccId = new List<Id>();
        List<Participant__c> parties = new List<Participant__c>();
        
        try{
            parties = [SELECT Id,CallId__c,CallId__r.Associated_Account__c,Contact_Id__c,Contact_Id__r.recordtypeid  FROM Participant__c WHERE Id IN : pIds];
            System.debug('parties:'+parties.size());
            System.debug('parties:'+parties[0].CallId__r.Associated_Account__c);
            System.debug('parties:'+parties[0].CallId__c);
            
            for(Participant__c partRec : parties){
                assocAccId.add(partRec.CallId__r.Associated_Account__c);
            }
            System.Debug('assocAccId:'+assocAccId);
            
            Map<String, Schema.SObjectType> consObjectMap = Schema.getGlobalDescribe() ;
            Schema.SObjectType cons = consObjectMap.get('Contact') ;
            Schema.DescribeSObjectResult resSchemaCon = cons.getDescribe() ;
            Map<String,Schema.RecordTypeInfo> conRecordTypeInfo = resSchemaCon.getRecordTypeInfosByName();
            Id conRTId = conRecordTypeInfo.get('Key Employee').getRecordTypeId();
            System.Debug('conRTId:'+conRTId);
            
            prgmList= [SELECT id,Name,AccountId__c,ownerId FROM Program_Member__c WHERE AccountId__c IN : assocAccId];
            system.debug('prgmList:'+prgmList.size());
            
            
            Dual_Coverage__c dualRec;
            for(Participant__c partRec : parties){
                for(Program_Member__c prgmRec : prgmList){
                    if(prgmRec.AccountId__c != null && partRec.CallId__r.Associated_Account__c!= null){
                        system.Debug('Associated and program account not null');
                        if(prgmRec.AccountId__c == partRec.CallId__r.Associated_Account__c && partRec.Contact_Id__r.recordtypeid == conRTId){
                            system.debug(+partRec.Contact_Id__r.recordtypeid+' AND '+conRTId);
                            dualRec = new Dual_Coverage__c();
                            dualRec.Participant__c = partRec.Id;
                            dualRec.Program_Member__c = prgmRec.Id;
                            dualRec.Account_Id__c = prgmRec.AccountId__c;
                            dualCovList.add(dualRec);
                            System.Debug('dualRec:'+dualRec);
                        }
                    }
                    System.Debug('dualCovList:'+dualCovList);
                }
            }
            System.Debug('dualCovList Size:'+dualCovList.size());
            Database.insert(dualCovList);
        } 
        catch(exception exp){
            System.Debug('Exception:'+exp.getMessage());
            System.Debug('StackTrace:'+exp.getStackTraceString());
        }
   }
}