/*******************************************************************************************************************************************
* @name         :   ECCP_DSCCustomerWS_Helper
* @description  :   This class will be responsible for calling  actual service and capturing response from the DSC Customer Service.
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/
public class ECCP_DSCCustomerWS_Helper{

        
        public static ECCP_CustomerResponse respData;

        
        public ECCP_CustomerResponse serviceCall(ECCP_CustomerRequest reqst){
            if(reqst != null){
                ECCP_DSCCustomer.ECCnCinoPreload_WebServices_Provider_getPreLoadDataWS_Port servCall = new ECCP_DSCCustomer.ECCnCinoPreload_WebServices_Provider_getPreLoadDataWS_Port();
                ECCP_DSCCustomer.ECCnCinoPreload_WebServices_Provider_getPreLoadDataWS_Port service = getService();
                //system.debug('----start call--');
               // system.debug('----test service---'+service.getPreLoadData(reqst.SSN,reqst.appId));
                // Aditya : 2.0 Change 6 Sep 2016 : added MDMId
                ECCP_DSCCustomer.PreLoadResponseDT servResp =  service.getPreLoadData(reqst.SSN,reqst.appId,reqst.MDMId);
               
                respData = handleResponse(servResp);
                //system.debug('---respData---'+respData);
                return respData;
                
            }
            else{
            return null;
            }
        }
/*******************************************************************************************************************************************
* @name         :   getService
* @description  :   This method will be responsible for setting url, certificate, and time out info.
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/
        public ECCP_DSCCustomer.ECCnCinoPreload_WebServices_Provider_getPreLoadDataWS_Port getService(){
            ECCP_DSCCustomer.ECCnCinoPreload_WebServices_Provider_getPreLoadDataWS_Port service = new ECCP_DSCCustomer.ECCnCinoPreload_WebServices_Provider_getPreLoadDataWS_Port();
            Map<String,ECCP_WebService_Data__mdt> mapWsSettings = ECCP_CustomMetadataUtil.queryMetadata(ECCP_IntegrationConstants.DSCCustomerMataData);
            service.timeOut_x   =   Integer.valueOf(mapWsSettings.get(ECCP_IntegrationConstants.DSCCustomerMataData).TimeOut__c);
            //system.debug('---service.timeOut_x --'+service.timeOut_x );
            service.endpoint_x  =   mapWsSettings.get(ECCP_IntegrationConstants.DSCCustomerMataData).EndPointUrl__c;
            service.clientCertName_x    =  mapWsSettings.get(ECCP_IntegrationConstants.DSCCustomerMataData).CertificateName__c;  
            return service;
        }
/*******************************************************************************************************************************************
* @name         :   getService
* @description  :   This method will be responsible for handling reponse and adding to reponse to response class.
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/        
        public ECCP_CustomerResponse  handleResponse(ECCP_DSCCustomer.PreLoadResponseDT resp){
            ECCP_CustomerResponse custResp = new ECCP_CustomerResponse();
            boolean nullcheck;
            //system.debug('----resp--');
            if(resp != null){
                    
                    List<ECCP_CustomerResponse.Acct> acctList = new List<ECCP_CustomerResponse.Acct>();

                    Map<String,List<ECCP_CustomerResponse.Acct>> accClsMap = new Map<String,List<ECCP_CustomerResponse.Acct>>();
                    
                    String key;
                    
                    ECCP_DSCCustomer.PreLoadResponseDT preLdT = new ECCP_DSCCustomer.PreLoadResponseDT();
                    ECCP_DSCCustomer.PreLoadResponse PreLoadResponseData = new ECCP_DSCCustomer.PreLoadResponse();
                    List<ECCP_DSCCustomer.PartyProfile> prtProf = new List<ECCP_DSCCustomer.PartyProfile>();
                    Map<String,List<ECCP_DSCCustomer.Cuac>> cuacWS = new Map<String,List<ECCP_DSCCustomer.Cuac>>();
                    Map<String,List<ECCP_DSCCustomer.Account>> accWS = new Map<String,List<ECCP_DSCCustomer.Account>>();
                    
                    preLdT = resp;
                    PreLoadResponseData = resp.PreLoadResponse;
                    //system.debug('----PreLoadResponseData---'+PreLoadResponseData);
                    if(PreLoadResponseData !=null){
                        PreLoadResponseData.Loan = resp.PreLoadResponse.Loan;
                        //system.debug('----PreLoadResponseData.Loan---'+PreLoadResponseData.Loan);
                        ECCP_DSCCustomer.Loan lon = PreLoadResponseData.Loan;
                        custResp.nAppID= lon.nAppID;
                        custResp.TotalConsumerDepositBalance= lon.TotalConsumerDepositBalance;
                        custResp.TotalCommercialDepositBalance= lon.TotalCommercialDepositBalance;
                        custResp.TotalConsumerCreditExposure= lon.TotalConsumerCreditExposure;
                        custResp.TotalCommercialCreditExposure= lon.TotalCommercialCreditExposure;
                        custResp.ExposureCalAmount= lon.ExposureCalAmount;
                        prtProf = lon.PartyProfile;
                        
                        //system.debug('---custResp.ExposureCalAmount---'+custResp.ExposureCalAmount);
                        
                        for(ECCP_DSCCustomer.PartyProfile prt : prtProf){
                            //system.debug('----prt for loop---'+prt);
                            if(prt.Cuac !=null){
                                cuacWS.put(prt.SocialSecurityNumber,prt.Cuac);
                                
                            }else{
                                nullcheck = true;
                                //system.debug('--in helper---'+cuacWS);
                            }
                        }
                        if(cuacWS !=null && cuacWS.size()>0){
                            for(string str : cuacWS.keySet()){
                                //system.debug('--aa 1--'+str);                    
                                //system.debug('--aa 1--'+cuacWS.get(str));

                                
                                for(ECCP_DSCCustomer.Cuac cu : cuacWS.get(str)){
                                   
                                    key= str+'-::-'+cu.cuacRelationshipCode+'-::-'+cu.accountIDREF;
                                    
                                    
                                    List<ECCP_DSCCustomer.Account> tempListAcc = new List<ECCP_DSCCustomer.Account>();
                                    if(accWS.containsKey(key)){
                                        tempListAcc = accWS.get(key);
                                        tempListAcc.addAll(cu.Account);
                                        accWS.put(key,tempListAcc);
                                    }
                                    else{
                                        tempListAcc.addAll(cu.Account);
                                        accWS.put(key,tempListAcc);
                                    }
                                    
                                }
                            }
                            
                            for(string str : accWS.keySet()){
                                List<String> keySplit = str.split('-::-');
                                List<ECCP_CustomerResponse.Acct> accList = new List<ECCP_CustomerResponse.Acct>();
                                for(ECCP_DSCCustomer.Account prt : accWS.get(str)){
                                    nullcheck = false;
                                    ECCP_CustomerResponse.Acct accInfo = new ECCP_CustomerResponse.Acct();
                                    accInfo.AcctID = prt.AcctID;
                                    accInfo.AccountType = prt.AccountType;
                                    accInfo.AccountNumber = prt.AccountNumber;
                                    accInfo.AccountName = prt.AccountName;
                                    accInfo.BankNumber= prt.BankNumber;
                                    accInfo.BalanceAmount=prt.BalanceAmount;
                                    accInfo.ChargeOffFlag=prt.ChargeOffFlag;
                                    accInfo.ExposureFlag=prt.ExposureFlag;
                                    accInfo.CommitmentAmount=prt.CommitmentAmount;
                                    accInfo.OpenDate=prt.OpenDate;
                                    accInfo.ProductCode=prt.ProductCode;
                                    accInfo.SSN=prt.SSN;
                                    accInfo.SubProductCode=prt.SubProductCode;
                                    accInfo.AccountStatus=prt.AccountStatus;
                                    accInfo.MaturityDate=prt.MaturityDate;
                                    accInfo.CL3Obligation=prt.CL3Obligation;
                                    accInfo.CL3Obligor=prt.CL3Obligor;
                                    accInfo.CRACode=prt.CRACode;
                                    accInfo.FacilityRR=prt.FacilityRR;
                                    accInfo.LostGivenDefault=prt.LostGivenDefault;
                                    accInfo.NSFCnt_12_M=prt.NSFCnt_12_M;
                                    accInfo.Obligor_RR=prt.Obligor_RR;
                                    accInfo.Obligor_POD=prt.Obligor_POD;
                                    accInfo.ODCnt_12_M=prt.ODCnt_12_M;
                                    accInfo.PmtAmt=prt.PmtAmt;
                                    accInfo.PmtDueDate=prt.PmtDueDate;
                                    accInfo.ReviewDate=prt.ReviewDate;
                                    accInfo.TSYSNumber=prt.TSYSNumber;
                                    accInfo.DDA_6M_Ave_bal = prt.DDA_6M_Ave_bal;
                                    accInfo.cuacRelationshipCode = keySplit[1];
                                    accInfo.accountIDREF = keySplit[2];
                                    accInfo.partySSN = keySplit[0];
                                    accList.add(accInfo);
                                
                                }
                                accClsMap.put(str,accList);
                            }
                        }
                        if(!nullcheck){
                            for(String str : accClsMap.keySet()){
                                for(ECCP_CustomerResponse.Acct acc : accClsMap.get(str)){
                                        
                                        acctList.add(acc);
                                        
                                }
                            }
                            
                            custResp.AcctData = acctList ;
                            
                            //system.debug('----custResp---'+custResp);
                            
                            return custResp;
                        }
                        else{
                            return null;
                        }
                       
                    }
                    else{
                        return null;
                    }
                }
            else{
                return null;
            }
        }

    }