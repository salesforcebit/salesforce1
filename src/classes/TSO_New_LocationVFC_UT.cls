@istest
private class TSO_New_LocationVFC_UT {

    static testmethod void testTSONewLocationVFC() {
        
        TSO_CAP_Data__c testTSO = new TSO_CAP_Data__c();
        insert testTSO;
        
        Account testAccount = UnitTestFactory.buildTestAccount();
        testAccount.Related_CAP__c = testTSO.Id;
		insert testAccount;
        
        LLC_BI__Treasury_Service__c testTreasuryService = UnitTestFactory.buildTestTreasuryService();
        
        Product2 testProduct2 = UnitTestFactory.buildTestProduct();
        testProduct2.Family = 'Deposits & ECP';
        insert testProduct2;
        
        LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
        testProductLine.Name = 'testProductLine';
        insert testProductLine;
        
        LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
        testProductLine.Name = 'testProductType';
        insert testProductType;
        
        LLC_BI__Product__c testProduct = UnitTestFactory.buildTestNCinoProduct(testProduct2, testProductType);
        testProduct.Name = 'testProduct';
        System.debug('>>>>>' + testProduct2.id);
        testProduct.Related_SFDC_Product__c = testProduct2.Id;
        insert testProduct;
        
        testTreasuryService.Name = 'testTreasuryService';
        testTreasuryService.LLC_BI__Product_Reference__c = testProduct.Id;
        System.debug('>>>>>' + testTreasuryService.LLC_BI__Product_Reference__c);
        testTreasuryService.LLC_BI__Relationship__c =  testAccount.id;      
 		insert testTreasuryService;
        
        ApexPages.StandardController std = new ApexPages.StandardController(testTreasuryService);
        TSO_New_LocationVFC controller = new TSO_New_LocationVFC(std);
        
        System.assertEquals('/', controller.getTreasuryServiceView().getUrl());
        
        /*
         * Cannot call a flow from apex that has user input and screens
        
		Map<String, Object> inputs = new Map<String, Object>();
        inputs.put('ParentTreasuryServiceId', testTreasuryService.Id);
        
        controller.flow = new Flow.Interview.TSO_New_Location(inputs);
        controller.flow.start();
        
        System.assertEquals('', controller.getTreasuryServiceView().getUrl());*/
    }
}