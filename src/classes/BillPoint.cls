/**
 * Provides a strongly-typed intermediate object for converting the JSON representation
 * of a Bill Point to its sObject equivalent.
 */
public class BillPoint extends nFORCE.AForce {
	public String sid;
	public String name;
	public String lookupKey;
	public String product;
	public Decimal price;
	public Decimal unitCost;
	public Boolean isDeleted;

	public BillPoint() {}

	public BillPoint(String sid, String name, String lookupKey, Decimal price, Decimal unitCost,
		Boolean isDeleted) {
		this.sid = sid;
		this.name = name;
		this.lookupKey = lookupKey;
		this.price = price;
		this.unitCost = unitCost;
		this.isDeleted = isDeleted;
	}

	public LLC_BI__Bill_Point__c getSObject() {
		LLC_BI__Bill_Point__c billPoint = new LLC_BI__Bill_Point__c();
		this.mapToDb(billPoint);
		return billPoint;
	}

	public override Schema.SObjectType getSObjectType() {
		return LLC_BI__Bill_Point__c.sObjectType;
	}

	public static BillPoint buildFromDB(sObject obj) {
		BillPoint billPoint = new BillPoint();
		billPoint.mapFromDB(obj);
		return billPoint;
	}

	public override Type getType() {
		return BillPoint.class;
	}

	public override void mapFromDb(sObject obj) {
		LLC_BI__Bill_Point__c billPoint = (LLC_BI__Bill_Point__c)obj;
		if (billPoint != null) {
			this.sid = billPoint.ID;
			this.name = billPoint.Name;
			this.lookupKey = billPoint.LLC_BI__lookupKey__c;
			this.product = billPoint.LLC_BI__Product__c;
			this.price = billPoint.LLC_BI__Price__c;
			this.unitCost = billPoint.LLC_BI__Unit_Cost__c;
		}
	}

	public override void mapToDb(sObject obj) {
		LLC_BI__Bill_Point__c billPoint = (LLC_BI__Bill_Point__c)obj;
		if (billPoint != null) {
			if (this.sid != null && this.sid.length() > 0) {
				billPoint.Id = Id.valueOf(this.sid);
			}
			billPoint.Name = this.name;
			billPoint.LLC_BI__lookupKey__c = this.lookupKey;
			billPoint.LLC_BI__Price__c = this.price;
			billPoint.LLC_BI__Unit_Cost__c = this.unitCost;
		}
	}
}