//================================================================================
//  KeyBank  
//  Object: Opportunity
//  Author: Offshore
//  Detail: OpportunityTriggerHandler
//  Description : To capture Actual Fee on Opportunity team member.

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================

public class OpportunityTriggerHandler
{
    
    private static boolean run = true;
    public static boolean flag=false;
    public static Boolean bUpdate=false;
    //Record type Id's
    public static Id creId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
    public static Id ecpId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Deposits & ECP').getRecordTypeId();
    public static Id ivbId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Invst Bnkg & Market').getRecordTypeId();
    public static Id leaseId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Lease').getRecordTypeId();
    public static Id recId= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('REC-On Balance Sheet').getRecordTypeId();
    public static Id KEFId= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('KEF Opportunity').getRecordTypeId();
    public static Id lsamId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('LSAM').getRecordTypeId();
    public static Id mortbId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Mortgage Banking').getRecordTypeId();
   // public opportunityLineItem[] ExistingOLI {get;set;}
    //@future public static void AFupdate(list<id> tid) 
    public static void AFupdate(List<Opportunity> opptyList) 
    {
    Set<Id> opportunityIds = new Set<Id>();
    List <OpportunityTeamMember> opportunityTM = [SELECT ID, OpportunityId, UserId, Actual_Fee__c FROM OpportunityTeamMember where OpportunityId =: opptyList];
    List <Opportunity> Oppfull = opptyList; // [SELECT ID, Opportunity.RecordTypeId, Actual_Fee_Amount__c, Actual_Syndication_Non_Amortizing_Fee__c, Actual_Fee_Rollup__c FROM Opportunity where Id =: tid];
    
    //system.debug(tid);
   
    List <opportunity> oppyL = new List <opportunity>();
    List <OpportunityTeamMember> otm = new List <OpportunityTeamMember>();
    List <OpportunityTeamMember> otmo = new List <OpportunityTeamMember>();
    List <OpportunityTeamMember> otmon = new List <OpportunityTeamMember>();

    for(opportunity oppt : Oppfull )
    {
         for(OpportunityTeamMember opportunityTML : opportunityTM)
         {
             if(oppt.RecordTypeId == ivbId)
             {
                 system.debug(oppt.RecordTypeId);
                 opportunityTML.Actual_Fee__c = oppt.Actual_Fee_Amount__c;
                 otm.add(opportunityTML);
                 system.debug(otm.size());
                 system.debug(otm);
             }           
             if(oppt.RecordTypeId == creId)
             {
                 opportunityTML.Actual_Fee__c = oppt.Actual_Syndication_Non_Amortizing_Fee__c;
                 otmo.add(opportunityTML);
                 system.debug(otmo.size());
                 system.debug(otmo);
             }
             if(oppt.RecordTypeId == ecpId)
             {
                 opportunityTML.Actual_Fee__c = oppt.Actual_Fee_Rollup__c;
                 otmon.add(opportunityTML);
                 system.debug(otmon.size());
                 system.debug(otmon);
             }
             
         }
    }

    if(otm.size()>0) 
 try{
        update otm;        
        system.debug(otm.size());
        system.debug(otm);        
    }
    catch (Exception e) {
                            System.debug('Exception is ' + e.getMessage());
                            //ApexPages.addMessages(e);
                        } 
    if(otmo.size()>0)
 try{
    update otmo;
    system.debug(otmo.size());
    system.debug(otmo);
    }
    catch (Exception f) {
                            System.debug('Exception is ' + f.getMessage());
                            //ApexPages.addMessages(f);
                        }
    if(otmon.size()>0)
 try{
    update otmon;
    system.debug(otmon.size());
    system.debug(otmon);
    }
    catch (Exception g) {
                            System.debug('Exception is ' + g.getMessage());
                            //ApexPages.addMessages(f);
                        } 
    }
    //method for adding Opportunity line item for single product opp on insert
    //@future public static void insertOppLineItem(List<Id> newOpp, List<String> productCode)
   public static void insertOppLineItem(List<Id> newOpp, List<String> productCode)
    {
      system.debug('Method entry ');
     
      String prodCode;
      String prodCodeMortgage;
      String prodCodeLSAM;
      //Pricebook Entry Population
      List<PriceBookEntry> listPriceBookEntry =[select Id, Name,Pricebook2Id,ProductCode, IsActive, Product2.Name, Product2.Family,Product2.Product_Category__c,Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where (PriceBookEntry.Product2.ProductCode like :productCode or PriceBookEntry.Product2.ProductCode like 'LSAM%' or PriceBookEntry.Product2.ProductCode like 'Mortgage%') and IsActive = true  order by Priorty__c asc NULLS last , Product2.Name asc  ];
      Map<String, id> priceBookMap = new Map<String,Id>();
      for(PriceBookEntry pbe: listPriceBookEntry ){
         pricebookMap.put(pbe.ProductCode,pbe.id);
      }
      system.debug(priceBookMap);
      //List<Opportunity> oppList=[select id,name,Recordtype.DeveloperName,Recordtype.Name,Product_Category__c,product__c, private__c from opportunity where id in :newOpp];
      List<OpportunityLineItem> OppLineItem= new List<OpportunityLineItem>();
      if(bUpdate){
          try{
              List<OpportunityLineItem> deleteOLIList=[Select id,opportunityId,priceBookentryId from OpportunityLineItem where opportunityId in :newOpp];
              if(deleteOLIlist.size()>0)
                  delete deleteOLIlist;
              }
          catch(Exception e){
              system.debug('Delete existing OLI '+e.getMessage());
          }
      }
     // OpportunityLineItem[] ExistingOLI=[select id, PriceBookEntryId, PriceBookEntry.Name,Primary_Product__c, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name,
                    //   PriceBookEntry.PriceBook2Id from opportunityLineItem where  Opportunityid =:newOpp];
      for(Opportunity opp: [select id,name,volume__c,amount,Recordtype.DeveloperName,Recordtype.Name,Product_Category__c,revenue__c,product__c, private__c,estimated_fee__c,estimated_revenue__c,product_LSAM__c,Investor_type_Product__c from opportunity where id in :newOpp]){
          
          system.debug('Record Type..'+opp.RecordTypeId);
          if(opp.RecordtypeId==leaseId ||opp.RecordtypeId==ivbId||opp.RecordtypeId==recId || opp.RecordtypeId==mortbId || opp.RecordtypeId==lsamId ){
              
              OpportunityLineItem oli = new OpportunityLineItem();
              oli.OpportunityId=opp.id;
              if(opp.RecordtypeId==ivbId||opp.RecordtypeId==recId){
              prodCode=opp.Recordtype.Name+'>'+opp.Product_Category__c+'>'+opp.Product__c;
              oli.PriceBookEntryid=pricebookMap.get(prodCode);
              }
              else if(opp.RecordtypeId==mortbId){
              prodCodeMortgage =opp.RecordType.name+'>'+opp.Investor_Type_Product__c+'>'+opp.Investor_Type_Product__c;              
              oli.PriceBookEntryid=pricebookMap.get(prodCodeMortgage);
              }
              else if(opp.RecordtypeId==lsamId){              
              prodCodeLSAM=opp.Recordtype.Name+'>'+opp.Product_LSAM__c+'>'+opp.Product_LSAM__c;
              oli.PriceBookEntryid=pricebookMap.get(prodCodeLSAM);
              }
              if(opp.RecordtypeId==ivbId ||opp.RecordtypeId==lsamId)
              oli.UnitPrice=opp.Estimated_Fee__c;
              else if(opp.RecordtypeId ==recId)
              oli.UnitPrice=opp.Estimated_revenue__c;
              else if(opp.recordtypeId==mortbId){
              system.debug('Check point 2');
              oli.UnitPrice=opp.Revenue__c;
              }
             
              system.debug('Code..'+prodcode+'...'+pricebookMap.get(prodCode));
              OppLineItem.add(oli);
              
          }
      }
      try{
        if(OppLineItem.size()>0){ 
          database.insert(OppLineItem);
          }
      }
      catch(Exception e){
           System.debug('Opportunity line item insert '+e.getMessage());  
       }        
    }
    
    //method for setting Private value as true for Investment banking,Credit, Lease and REc record types.
    //@future public static void updatePrivate(List<Opportunity> oppId)
   public static void updatePrivate(List<opportunity> OppId){
       try{      
        List<Id> ownerIdList= new List<id>();
        for(Opportunity opp:OppId){
            ownerIdList.add(opp.OwnerId);
        }
        List<User> userList=[Select id,name,plob_alias__c,slob__c from user where id in:ownerIdList ];
        for(Opportunity newOpp: oppId){
        
            if(newOpp.recordtypeId== ivbId)
            {
                newOpp.private__c = true;
                
            }
            if(newOpp.recordtypeId==creId||newOpp.recordtypeId==leaseId||newOpp.recordtypeId==recId){
               if(newOpp.Loan_Syndication__c==true){
               newOpp.private__c = true;
               newOpp.Name= newOpp.Private_Code_Name__c;
               }
               for(User us: userList){
                   if(newOpp.OwnerId==us.id){
                     if((us.PLOB_Alias__c=='KEF'||us.PLOB_Alias__c=='KBBC') && newOpp.recordtypeId!=recId ){
                       newOpp.private__c = true;  
                     }
                    //Commented the below functionality as part of CR 1554 -Nov 2016 Release
                    /* else if(newOpp.recordtypeId==recId && us.SLOB__c=='INSTITUTIONAL REAL ESTATE GRP'){
                      system.debug('Inside REC');
                      newOpp.private__c = true;  
                     }*/ 
                   }
                   
                   else
                   break;
               }
               
            }
           
        }
      }
      catch(Exception e){
          system.debug(e);
      }
      
    }
    public static void updateSalesPrice(List<Id> listUpdate){
        
        List<OpportunityLineItem> oppLineItem = new List<OpportunityLineItem>();
        
        for(OpportunityLineItem OLI:[Select id,name, OpportunityId,Primary_Product__c,UnitPrice,Opportunity.Estimated_Fee__c,Opportunity.Estimated_Revenue__c,Opportunity.revenue__c,Opportunity.RecordtypeId from OpportunityLineItem where Opportunityid in :listUpdate ]){
            if(oli.opportunity.RecordtypeId==ivbId|| oli.opportunity.RecordtypeId==lsamId)
            oli.UnitPrice= oli.Opportunity.Estimated_Fee__c;
            if(oli.opportunity.RecordtypeId==recId)
            oli.unitPrice = oli.Opportunity.Estimated_Revenue__c;
            if(oli.opportunity.RecordtypeId==mortbId)
            oli.UnitPrice= oli.Opportunity.Revenue__c;
           oppLineItem.add(OLI);
        }
       
        update oppLineItem;
        }
  //To automatically add the vendor to opportunity if there is one vendor for agreement.
  //Jan release
    public static void updatevendor(List<opportunity> opplist)
    {
    map<Id,Vendor_Agreement__c> venmap = new map<Id,Vendor_Agreement__c>();
    List<Vendor_Agreement__c> vl=[select id,name,Account_Vendor__c,Agreement__c from Vendor_Agreement__c];
    for(Vendor_Agreement__c v: vl)
    {
    venmap.put(v.Agreement__c,v);
    }
    For(Opportunity op: opplist)
    {
    if(op.vendor_count__c==1.00 && op.recordtypeid == KEFId)
    {
     Vendor_Agreement__c va = venmap.get(op.Vendor_Agreement__c);
     opportunity o = new opportunity();
     o.id=op.id;
     o.Vendor__c=va.Account_Vendor__c;
     o.Vendor_and_Agreement_Association__c=va.id;
     update o;
     }
    }
  } 
  //to Remove the product once the Vendor agreement is changed
  public static void removeProduct(List<Opportunity>opp ,List<id> oppId){
       String pName= [select id,name from profile where id=:UserInfo.getProfileId()].name;
        if(pName != 'Integration/Data Migration' ){
       system.debug(oppId+'...Delete');
        List<OpportunityLineItem> deleteOLIList= new List<OpportunityLineItem>();
        deleteOLIList=[Select id,opportunityId,priceBookentryId,Primary_Product__c from OpportunityLineItem where opportunityId in :oppId];
        List<OpportunityLineItem> updatePrimary= new List<OpportunityLineItem>();
                  system.debug(deleteOLIList+'...Delete List');
                  for(OpportunityLineItem oli:deleteOLIList){
                      if(oli.Primary_Product__c==true)
                         oli.Primary_Product__c=false;
                          updatePrimary.add(oli);
                  }
                  if(updatePrimary.size()>0){
                  update updatePrimary;
                  }
                  if(deleteOLIlist.size()>0)
                      delete deleteOLIlist;
        }
   }

  //To remove the Vendor Agreement related details when the Vendor Agreement is changed.
  //Jan release
  public Static void updateVendorDetails(List<Opportunity> Opp,Map<id,Opportunity> oldMapOpp,Map<id,Opportunity> newMapOpp){
      system.debug('Inside method..'+opp);
      for(Opportunity oppList: opp){
            if(oppList.recordtypeid == KEFId){
            System.debug(oldMapOpp.get(oppList.Id).Vendor_Agreement__c+' Old..New '+newMapOpp.get(oppList.Id).Vendor_Agreement__c);
            if(oldMapOpp.get(oppList.Id).Vendor_Agreement__c!=newMapOpp.get(oppList.Id).Vendor_Agreement__c){
            
            oppList.Custom_Reporting__c=null;
            oppList.Promotion_A__c=null;
            oppList.Vendor__c =null;}
            }
        }
  }
  
  //Method to populate Production Goal Junction record when opty is closed
  //added as part of march release 
  public static void opportunitysplitquery(List<Opportunity> oppsplId)
    {
       
       List<OpportunitySplit> opsplt = new List<OpportunitySplit>();
       List<Production_Goals_Junction__c> pgjlist = new List<Production_Goals_Junction__c>();
       //getting the Opportunity splitID
       OpportunitySplitType ost = [Select ID, Description from OpportunitySplittype where Description =: 'Volume'];
       opsplt= [Select ID,SplitOwnerId,opportunityId,SplitPercentage,SplitTypeId,SplitOwner.Name,SplitOwner.UserRole.Name from OpportunitySplit where OpportunityID  in :oppsplId and SplitTypeId =: ost.Id and SplitPercentage != 0.00];
       Map<Id,List<OpportunitySplit>> mapOptySplit = new Map<Id,List<opportunitySplit>>();
       List<Id> userId = new List<Id>();
       for(OpportunitySplit split: opsplt){
         userid.add(split.splitownerId);
         system.debug(userid);
          if(mapOptySplit.containsKey(split.opportunityId)){
          mapOptySplit.get(split.opportunityId).add(split);
          }
          else
           mapOptySplit.put(split.opportunityId, new List<OpportunitySplit >{split});
       }
       Map<Id,Integer> YearO = new Map<Id,Integer>();
       Map<Id,Integer> YearPG = new Map<Id,Integer>();
       Map<Id,Integer> YearSGOA = new Map<Id,Integer>();
       Map<Id,String> YearSGO = new Map<Id,String>();
       Map<Id,String> UsrRol = new Map<Id,String>(); 
       Map<Id,String> ssrole = new Map<Id,String>();
      
       
       for(Opportunity opp:[Select id,name,Actual_Close_Date__c from Opportunity where Id IN: oppsplId]){
            system.debug(oppsplId);
            YearSGO.put(opp.Id,String.valueof(opp.Actual_Close_Date__c.Year()));
            system.debug(YearSGO);
            YearO.put(opp.Id,Integer.valueof(opp.Actual_Close_Date__c.month()));
            system.debug(YearO.values());
            YearSGOA.put(opp.Id,Integer.valueof(opp.Actual_Close_Date__c.Year()));
            system.debug(YearSGOA);
       }
       List<Production_Goals__c> prodgoals = [Select Id,Year__c,Month__c, Name, Goal_Month__c, Username__c,Username__r.Name from Production_Goals__c where  CALENDAR_MONTH(Goal_Month__c) IN :YearO.values() and username__c in :userId and CALENDAR_YEAR(Goal_Month__c) IN :YearSGOA.Values()];
       List<user> usr = [Select Id, name, UserRole.Name from user where Id IN :userid];
       for(User rusr : usr){
           UsrRol.put(rusr.Id,String.valueof(rusr.UserRole.Name));
       }
       
       List<Segment_Revenue_Goal_Object__c> segGoal = [Select Id, Name, Segment__c, Sub_Segment__c, Year__c from Segment_Revenue_Goal_Object__c where Year__c IN  :YearSGO.Values() and Sub_Segment__c IN :UsrRol.values()];
       for(Segment_Revenue_Goal_Object__c roless : segGoal){
            ssrole.put(roless.Id,String.valueof(roless.Sub_Segment__c));
       }
       system.debug(prodgoals.size()+'Prod goals size');
       system.debug(prodgoals);
       system.debug(segGoal);
       system.debug(mapOptySplit.values()+' Map Opty Split');
       for(Opportunity ops : [Select id,name,Actual_Close_Date__c,RecordType.Name,StageName from Opportunity where Id IN: oppsplId]){
         system.debug(oppsplId); 
         List<OpportunitySplit> opspl = mapOptySplit.get(ops.Id);
         system.debug(opspl);
         for(OpportunitySplit opsplit : opspl) 
         {
             for(Production_Goals__c Pgoals: prodgoals)
             {
                system.debug(Pgoals.Id + 'ID CHECK');
                YearPG.put(Pgoals.Id, Integer.valueof(Pgoals.Goal_Month__c.Month()));
                system.debug(YearO.values());
                system.debug(YearPG.values());
                system.debug(ops.Actual_Close_Date__c.year());
                system.debug(Pgoals.Goal_Month__c.year());
                system.debug(ssrole.values());
                system.debug(UsrRol.values());
                system.debug(opsplit.SplitOwner.Name);
                system.debug(Pgoals.Username__r.Name);
                system.debug(Pgoals.Username__c);
                system.debug(opsplit.SplitOwner.Id);
                system.debug(opsplit.SplitOwner.UserRole.Name);
                system.debug(ops.RecordType.Name); 
                if(Pgoals.Goal_Month__c != null && ops.StageName == 'Closed')
                {
                    if(ops.RecordType.Name == 'KEF Opportunity' && ops.Actual_Close_Date__c.year()==Pgoals.Goal_Month__c.year() && ops.Actual_Close_Date__c.month()==Pgoals.Goal_Month__c.month() && Pgoals.Username__c==opsplit.SplitOwner.Id) // && ssrole.get()==opsplit.SplitOwner.UserRole.Name)
                        {
                            system.debug(opSplit.SplitOwnerId+'**Split Owner** prod goals '+Pgoals.Username__c);
                            system.debug('inside if '+ pGoals);
                            Production_Goals_Junction__c pgj = new Production_Goals_Junction__c();
                            pgj.Username__c = opsplit.SplitOwnerId;
                            pgj.Opportunity__c = opsplit.opportunityId;
                            pgj.Percent__c = opsplit.SplitPercentage;
                            pgj.Name = opsplit.SplitOwner.Name + '-' + Pgoals.Month__c ;
                            pgj.Production_Goals__c = Pgoals.Id;
                            pgjlist.add(pgj);
                        
                       } 
                }     
              
          }
 
        }  
        
        system.debug(pgjlist.size()+' List size');
        if(pgjlist.size()>0)
        {
            Upsert(pgjlist);
        }
        
    }
    } 


  //Method to populate Segment Goal Junction record when opty is closed
  //as part of march release  
  public static void opportunitysegmentgoals(List<Opportunity> oppsegId)
  {
      Map<Id,Integer> YearO = new Map<Id,Integer>();
      Map<Id,Integer> YearPG = new Map<Id,Integer>();
      Map<Id,Integer> YearSGOA = new Map<Id,Integer>();
      Map<Id,String> YearSGO = new Map<Id,String>();
      Map<Id,String> UsrRol = new Map<Id,String>(); 
      Map<Id,String> ssrole = new Map<Id,String>();

      
      List<OpportunitySplit> opsplt = new List<OpportunitySplit>();
      //getting the Opportunity splitID
      OpportunitySplitType ost = [Select ID, Description from OpportunitySplittype where Description =: 'Volume'];
      opsplt= [Select ID,SplitOwnerId,opportunityId,SplitPercentage,SplitTypeId,SplitOwner.Name,SplitOwner.UserRole.Name from OpportunitySplit where OpportunityID in :oppsegId and SplitTypeId =: ost.Id and SplitPercentage != 0.00];
      Map<Id,List<OpportunitySplit>> mapOptySplit = new Map<Id,List<opportunitySplit>>();
      List<Id> userId = new List<Id>();
      for(OpportunitySplit split: opsplt){
      userid.add(split.splitownerId);
      system.debug(userid);
      if(mapOptySplit.containsKey(split.opportunityId)){
      mapOptySplit.get(split.opportunityId).add(split);
      }
      else
           mapOptySplit.put(split.opportunityId, new List<OpportunitySplit >{split});
      }
       
      /* for(Opportunity opp:[Select id,name,Actual_Close_Date__c from Opportunity where Id IN: oppsegId]){
            system.debug(oppsegId);
            YearSGO.put(opp.Id,String.valueof(opp.Actual_Close_Date__c.Year()));
            system.debug(YearSGO);
            YearO.put(opp.Id,Integer.valueof(opp.Actual_Close_Date__c.month()));
            system.debug(YearO.values());
            YearSGOA.put(opp.Id,Integer.valueof(opp.Actual_Close_Date__c.Year()));
            system.debug(YearSGOA);
       } */
       //List<Production_Goals__c> prodgoals = [Select Id,Year__c,Month__c, Name, Goal_Month__c, Username__c,Username__r.Name from Production_Goals__c where  CALENDAR_MONTH(Goal_Month__c) IN :YearO.values() and username__c in :userId and CALENDAR_YEAR(Goal_Month__c) IN :YearSGOA.Values()];
       List<user> usr = [Select Id, name, UserRole.Name from user where Id IN :userid];
       for(User rusr : usr){
           UsrRol.put(rusr.Id,String.valueof(rusr.UserRole.Name));
       } 
      
      List<Segment_Revenue_Goals_Junction__c> listSegGoalJn = new List<Segment_Revenue_Goals_Junction__c>();
      Map<Id,Integer> mapYearsg = new Map<Id,Integer>();
      for(Opportunity optysg :oppsegId){
            if(optysg .StageName=='Closed'){
             mapYearsg.put(optysg.Id, Integer.valueof(optysg.Actual_Close_Date__c.year()));   
           }
         }
    List<Segment_Revenue_Goal_Object__c> SegGoals = [Select id,Segment__c,Goal_Month__c,Sub_Segment__c,Year__c from Segment_Revenue_Goal_Object__c where CALENDAR_YEAR(Goal_Month__c) in :mapYearsg.values()]; 
    system.debug(SegGoals);
    for(Opportunity optysg  : [Select id,name,Actual_Close_Date__c,RecordType.Name,StageName from Opportunity where Id IN: oppsegId]){
     for(Segment_Revenue_Goal_Object__c goal :SegGoals){
      for(OpportunitySplit opsplit : opsplt){
            //system.debug(rusr.UserRole.Name);
            //system.debug(rusr);
            system.debug(opsplit.SplitOwner.UserRole.Name);
            system.debug(goal.Sub_Segment__c);
            if(goal.Goal_Month__c != null)
            {
                if(optysg.RecordType.Name == 'KEF Opportunity' && opsplit.SplitOwner.UserRole.Name == goal.Sub_Segment__c && goal.Goal_Month__c.Month() == optysg.Actual_Close_Date__c.month()){
                system.debug(usr);
                system.debug(goal);
                system.debug(opsplit);
                Segment_Revenue_Goals_Junction__c jn= new Segment_Revenue_Goals_Junction__c();
                jn.Opportunity__c=optysg.id;
                jn.Segment_Revenue_Goal_Name__c=goal.id;
                jn.Percentage__c=opsplit.SplitPercentage;
                jn.User__c=opsplit.SplitOwner.Id;
                jn.Name=opsplit.SplitOwner.name;
                listSegGoalJn.add(jn);
            }
            else
            break;
            }
       }
     }
    }
    system.debug(listSegGoalJn); 
    if(listSegGoalJn.size()>0)
        {
            Upsert(listSegGoalJn);
        } 
  }
  //Method to populate Program Goal Junction record when opty is closed
  //added as part of march release 
    public static void insertProgramGoalJunction(List<Opportunity> opp){
       List<Program_Goal_Junction__c> listProgGoalJn = new List<Program_goal_junction__c>();
       Map<Id,String> mapYear = new Map<Id,String>();
       for(Opportunity opty :opp){
            if(opty.StageName=='Closed'){
             mapYear.put(opty.Id, string.valueof(opty.Actual_Close_Date__c.year()));       
            
            }
        }
     List<Program_goals__c> progGoals = [Select id,name,year__c,Program_Name__c,npv__c,Volume__c,Program_Display_Name__c,goal_month__c from program_goals__c where year__c in :mapYear.values()]; 
     Integer year;
     for(Opportunity opty : [Select id,name,Actual_Close_Date__c,RecordType.Name,StageName,KEF_Program_ID__c from Opportunity where Id IN: opp]){
        for(Program_goals__c goal :progGoals){
            year=integer.valueof(goal.year__c);
            if(opty.RecordType.Name == 'KEF Opportunity' && opty.KEF_Program_ID__c==goal.program_Name__c && goal.Goal_Month__c.Month() == opty.Actual_Close_Date__c.month() && year==opty.Actual_Close_Date__c.year()){
            Program_goal_junction__c jn= new Program_Goal_Junction__c();
            jn.Opportunity__c=opty.id;
            jn.Program_Goals__c=goal.id;
            listProgGoalJn.add(jn);
            }
            else
            break;
        }
     }  
     upsert listProgGoalJn; 
    }
    //Method to populate Program BD Junction record when opty is closed
    //added as part of march release
    public static void insertProgramBDJunction(List<Opportunity> opp){
       List<Program_BD_Goal_Junction__c> listProgGoalJn = new List<Program_BD_Goal_Junction__c>();
       Map<Id,String> mapYear = new Map<Id,String>();
     
       for(Opportunity opty :opp){
            if(opty.StageName=='Closed'){
             mapYear.put(opty.Id, string.valueof(opty.Actual_Close_Date__c.year()));       
     
            }
        }
       List<Program_for_BD__c> progGoals = [Select id,name,year__c,User_Name__c from Program_for_BD__c where year__c in :mapYear.values()]; 
     for(Opportunity opty : [Select id,name,Actual_Close_Date__c,RecordType.Name,StageName,primary_business_developer__c from Opportunity where Id IN: opp]){
        for(Program_for_BD__c goal :progGoals){
            if(opty.RecordType.Name == 'KEF Opportunity' && opty.primary_business_developer__c==goal.user_Name__c){
            Program_BD_Goal_Junction__c jn= new Program_BD_Goal_Junction__c();
            jn.Opportunity__c=opty.id;
            jn.Program_for_BD__c=goal.id;
            listProgGoalJn.add(jn);
            }
            else
            break;
        }
     }  
     upsert listProgGoalJn; 
    }
  public static boolean runOnce(){
    if(run){
     run=false;
     return true;
    }else{
        return run;
    }
    }    
     //Created for CR1244
   /* public static void updateAutoDebit(List<Opportunity> opId){
       List<OpportunitylineItem> listOLI= [Select id,productCode,opportunityId from opportunityLineItem where OpportunityId in :opId];
       for(Opportunity o:opId)
       {
           for(OpportunitylineItem Opty: listOLI)
           {
               if(Opty.productCode.contains ('SBA') && o.StageName=='Closed'&&o.Auto_Debit__c==null)
                {
                    flag=true;   
                }                    
           }
           if(flag) o.Auto_Debit__c.addError('Auto Debit is required when SBA product is chosen');
       }
   }*/
    
    //Method to insert Primary Syndicator team member when a oppty is saved with distribution strategy as hold for sale or syndicate//
    //added as part of April release 
    public static void addPrimarySyndicator(List<Opportunity> opptyList){
        Set<Id> userIdSet = new Set<Id>();
        for(Opportunity oppty : opptyList){
            System.debug('oppty.Distribution_Strategy__c:'+oppty.Distribution_Strategy__c);
            System.debug('oppty.Primary_Syndicator__c:'+oppty.Primary_Syndicator__c);
            if(oppty.Distribution_Strategy__c != null && oppty.Distribution_Strategy__c != '' && (oppty.Distribution_Strategy__c == 'Hold for sale' || oppty.Distribution_Strategy__c == 'Syndicate')){
                if(oppty.Primary_Syndicator__c != null){
                    userIdSet.add(oppty.Primary_Syndicator__c);
                }
            }
        }
        System.Debug('userIdSet:'+userIdSet.size());
        List<User> primaryUserList = new List<User>();
        List<OpportunityTeamMember> opptyTeamList = new List<OpportunityTeamMember>();
        if(userIdSet != null && userIdSet.size() > 0){
            primaryUserList = [SELECT id,name,Email from User Where id IN :userIdSet];      
        }
        System.Debug('primaryUserList:'+primaryUserList.size());
        if(primaryUserList != null && primaryUserList.size() > 0){
            OpportunityTeamMember opptyTeamRec;
            for(Opportunity opptyRec : opptyList){
                for(User userRec : primaryUserList){
                System.Debug('If check:'+(opptyRec.Primary_Syndicator__c == userRec.Id));
                    if(opptyRec.Primary_Syndicator__c != null && opptyRec.Primary_Syndicator__c == userRec.Id){
                        opptyTeamRec = new OpportunityTeamMember();
                        opptyTeamRec.OpportunityId = opptyRec.Id;
                        opptyTeamRec.TeamMemberRole = 'Primary Syndicator';
                        opptyTeamRec.UserId = userRec.Id;
                        opptyTeamList.add(opptyTeamRec);
                    }
                }
            }
            System.debug('opptyTeamList:'+opptyTeamList.size());
        }
        Database.Insert(opptyTeamList);
        System.Debug('Inserted'); 
        
        List<OpportunityShare> shares = [select Id,OpportunityAccessLevel,RowCause FROM OpportunityShare WHERE OpportunityId IN :opptyList  AND RowCause = 'Team'];
        for(OpportunityShare share : shares) {
            share.OpportunityAccessLevel = 'Edit';
            System.Debug('Shares:'+share.OpportunityAccessLevel);
        }
        Database.Update(shares);
    } 
    
    
    //CR-1444 (September Release)
    // Method to update RecordType based on Lead-Opportunity Record Type value.
    
     public static void updateOppty(List<ID> oppList)
     {
        List<Opportunity> oList = new List<Opportunity>();
        List<Opportunity> updateList = new List<Opportunity>();
       
       oList=[Select Id,Name,Lead_Opportunity_Record_Type__c,RecordTypeId, RecordType.Name from Opportunity where id IN: oppList];
       
        if(oList.size() >0 )
        {
           for(Opportunity o : oList)
           { 
              if(o.Lead_Opportunity_Record_Type__c=='Credit')
               {
                 o.RecordTypeID = creId;
                 updateList.add(o);
               }
              
              else if(o.Lead_Opportunity_Record_Type__c=='Deposits & ECP')
               {
                 o.RecordTypeID = ecpId;
                 updateList.add(o);
               }
               
              else if(o.Lead_Opportunity_Record_Type__c=='Invst Bnkg & Market')
               {
                 o.RecordTypeID = ivbId;
                 updateList.add(o);
               }
               
               else if(o.Lead_Opportunity_Record_Type__c=='KEF Opportunity')
               {
                 o.RecordTypeID = KEFId;
                 updateList.add(o);
               }
               
               else if(o.Lead_Opportunity_Record_Type__c=='LSAM')
               {
                 o.RecordTypeID = lsamId;
                 updateList.add(o);
               }
               
               else if(o.Lead_Opportunity_Record_Type__c=='Mortgage Banking')
               {
                 o.RecordTypeID = mortbId;
                 updateList.add(o);
               }
               
               else if(o.Lead_Opportunity_Record_Type__c=='REC-On Balance Sheet')
               {
                 o.RecordTypeID = recId;
                 updateList.add(o);
               }
           }
        }
        
        if(updateList.size()>0)
        {
            update updateList;
        }  
     
   }
   //Added as part of CR 1499 for September 2016 release. This method helps to populate the flag if the opty contains child opporuntities
    @future
    public static void countChildOpty(List<id> opty){
      List<Opportunity> toUpdate= new List<Opportunity>();
      system.debug('Inside method');
      try{
      for(Opportunity oppId: [Select id,has_child_opportunity__c from Opportunity where id in: opty ]){
        oppId.has_child_opportunity__c= true;
        toUpdate.add(oppId);
      }
     update toUpdate;
      }
      Catch(Exception e){
          system.debug('Inserting child opty '+e); 
      }
    }
    //Added as part of CR 1499 for September 2016 release. This method helps to populate the flag if the opty contains child opporuntities on update & delete
    public static void countChildOptyupdate(List<id> opty,List<Id> oldList){
      Map<Id,List<Opportunity>> mapCount = new Map<Id,List<Opportunity>>();
      List<Id> oldparent = new List<Id>();
      List<Opportunity> toUpdate= new List<Opportunity>();
      List<Opportunity> checkfalse= new List<Opportunity>();
      system.debug('Inside method');
      try{
      for(Opportunity oppId: [Select id,has_child_opportunity__c from Opportunity where id in: opty ]){
        oppId.has_child_opportunity__c= true;
        toUpdate.add(oppId);
      } 
      
      
      for(Opportunity opp :[Select id,parent_opportunity_Id__c,has_child_opportunity__c from opportunity where parent_opportunity_id__c!=null and parent_opportunity_id__c in :oldList]){
      system.debug('Test '+opp);
      if(mapCount.containsKey(opp.parent_opportunity_id__c))
          mapCount.get(opp.parent_opportunity_id__c).add(opp);      
      else
          mapCount.put(opp.parent_opportunity_id__c, new List<Opportunity >{opp});
      }
      system.debug(mapCount.size()+'**Map count'+ mapcount);
      for(Opportunity opp1 :[Select id,parent_opportunity_Id__c,has_child_opportunity__c from opportunity where id in :oldList]){
       if( !mapcount.containskey(opp1.id)){
           system.debug('Inside if**');
           opp1.has_child_opportunity__c=false;
           checkfalse.add(opp1);
       }
      }
       system.debug(checkfalse.size()+'***check size');
       update toUpdate;
       update checkfalse;
      }
      catch(Exception e){
          system.debug('Child count method '+e);
      }
    }  
    
  }