@isTest
public class TestECCP_Exposure_MassEditController {
	@isTest
    static void startMethod() {
        LLC_BI__Product_Package__c testProductPackage = createProductPackage();
        Exposure_Adjustment__c testExposureAdjustment = createExposure(testProductPackage.Id);
        
        ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_Exposure_MassEditController MEEC = new ECCP_Exposure_MassEditController(controller);
    }
    
    @isTest
    static void viewNewMethod() {
        LLC_BI__Product_Package__c testProductPackage = createProductPackage();
        Exposure_Adjustment__c testExposureAdjustment = createExposure(testProductPackage.Id);
        
        ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_Exposure_MassEditController MEEC = new ECCP_Exposure_MassEditController(controller);
        
        Test.startTest();
        
        	MEEC.addNew();
        
        Test.stopTest();
    }
    
    @isTest
    static void updateMethod() {
        LLC_BI__Product_Package__c testProductPackage = createProductPackage();
        Exposure_Adjustment__c testExposureAdjustment = createExposure(testProductPackage.Id);
        
        ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_Exposure_MassEditController MEEC = new ECCP_Exposure_MassEditController(controller);
        
        Test.startTest();
        
        	MEEC.addNew();
            MEEC.exposureAdjustmentSave();
        
        Test.stopTest();
    }
    
    @isTest
    static void cancel() {
        LLC_BI__Product_Package__c testProductPackage = createProductPackage();
        Exposure_Adjustment__c testExposureAdjustment = createExposure(testProductPackage.Id);
        
        ApexPages.currentPage().getParameters().put('id', testProductPackage.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_Exposure_MassEditController MEEC = new ECCP_Exposure_MassEditController(controller);
        
        Test.startTest();
        
        	MEEC.cancel();
        
        Test.stopTest();
    }
    
    private static LLC_BI__Product_Package__c createProductPackage() {
        LLC_BI__Product_Package__c newPP = new LLC_BI__Product_Package__c(
        	Name = 'Test');
        insert newPP;
        return newPP;
    }
    
    private static Exposure_Adjustment__c createExposure(Id productPackageId) {
        Exposure_Adjustment__c newEA = new Exposure_Adjustment__c(
        	ProductPackage__c = productPackageId,
        	Adjust_Committed_Exposure_By__c = 1200);
        
        insert newEA;
        return newEA;
    }
}