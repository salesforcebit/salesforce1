public with sharing class TROEventCapture_Builder {

	public static TROEventCapture.EventHeaderType createEventHeaderType(DateTime eventCreateDateAndTime, String sourceTLA, String EAPEventCode, String sourceReferenceNumber, String processEventDelayInMinutes){

		TROEventCapture.EventHeaderType header = new TROEventCapture.eventHeaderType();
		header.eventCreateDateAndTime = eventCreateDateAndTime;
    	header.sourceTLA = sourceTLA;
   	 	header.EAPEventCode = EAPEventCode;
   	 	header.eventVersion = '1';
   		header.sourceReferenceNumber = sourceReferenceNumber;
    //header.processEventDelayInMinutes = processEventDelayInMinutes;

		return header;
	}

	public static TROEventCapture.eventEV00021BodyType createEventEV00021BodyType(String userId, String emailIDTo){

		TROEventCapture.eventEV00021BodyType body = new TROEventCapture.eventEV00021BodyType();
		body.userID = userId;
		body.emailIDTo = emailIDTo;
		body.originationSystem = 'AL00027';

		return body;

	}

	public static TROEventCapture.eventEV00022BodyType createEventEV00022BodyType(String userId, String password, String emailIDTo) {

		TROEventCapture.eventEV00022BodyType body = new TROEventCapture.eventEV00022BodyType();
		body.password = password;
		body.userID = userId;
		body.emailIDTo = emailIDTo;
		body.originationSystem = 'AL00028';

		return body;

	}

	public static TROEventCapture.eventEV00023BodyType createEventEV00023BodyType(String userId, String password, String orgId, String emailIDTo) {
		TROEventCapture.eventEV00023BodyType body = new TROEventCapture.eventEV00023BodyType();
		body.password = password;
		body.userID = userId;
		body.emailIDTo = emailIDTo;
		body.userOrg = orgId;

		return body;
	}

}