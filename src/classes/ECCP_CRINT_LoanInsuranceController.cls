public class ECCP_CRINT_LoanInsuranceController {

    public LLC_BI__Loan__c loan {get;set;}    
    public List<ECC_Loan_Insurance_Mgmt__c> loanInsurance {get;set;}
    public List<Schema.FieldSetMember> loanInsuranceFieldset {get;set;}
    public List<Schema.FieldSetMember> selectedLoanInsuranceFieldset {get;set;}
    public ECC_Loan_Insurance_Mgmt__c  selectedLoanInsurance {get;set;}
    public String loanInsuranceId {get;set;}
    public Boolean showList {get;set;}
    public nForce.TemplateController template;
    
    public ECCP_CRINT_LoanInsuranceController(nForce.TemplateController controller) {
        this.template = controller;
        this.loan = getLoan(ApexPages.currentPage().getParameters().get(PARAMETER_NAME));
        this.loanInsuranceFieldset = this.getLoanInsuranceFieldListView(FIELDSETLIST);
        this.loanInsurance = this.getLoanInsuranceList(this.loan.Id);
        this.showList = True;
    }
    
    public void viewLoanInsurance() {
    	this.showList = False;
        this.selectedLoanInsuranceFieldset = this.getSelectedLoanInsuranceFieldList(FIELDSETINDIVIDUAL);
        this.selectedLoanInsurance = this.getSelectedLoanInsurance(this.loanInsuranceId);
    }
    
    public void back() {
        this.showList = True;
    }
    
    private List<Schema.FieldSetMember> getLoanInsuranceFieldListView(String fieldSet) {
        return SObjectType.ECC_Loan_Insurance_Mgmt__c.fieldSets.getMap().get(fieldSet).getFields();
    }
    
    private List<Schema.FieldSetMember> getSelectedLoanInsuranceFieldList(String fieldSet) {
        return SObjectType.ECC_Loan_Insurance_Mgmt__c.fieldSets.getMap().get(fieldSet).getFields();
    }
    
    
    private LLC_BI__Loan__c getLoan(Id recordId) {
        return [SELECT
               		Id,
               		Name
               	FROM
               		LLC_BI__Loan__c
               	WHERE
               		Id = :recordId LIMIT 1];
    }
    
    private List<ECC_Loan_Insurance_Mgmt__c> getLoanInsuranceList(Id recordId) {
        System.debug(recordId);
        String query = SELECTSTATEMENT;
        for(Schema.FieldSetMember f: loanInsuranceFieldset) {
            query += f.getFieldPath() + COMMASPACE;
        }
        
		query += 'Id ';
        query += 'FROM ECC_Loan_Insurance_Mgmt__c ';
        query += 'WHERE Id IN ';
        query += '(SELECT ECC_Loan_Insurance_Mgmt__c  FROM ECC_Loan_Insurance__c  WHERE ECC_Loan__c  = \'' + recordId + '\') LIMIT 1000';
        return Database.query(query);
    }
    
    private ECC_Loan_Insurance_Mgmt__c  getSelectedLoanInsurance(Id recordId) {
        String query = SELECTSTATEMENT;
        for(Schema.FieldSetMember f: selectedLoanInsuranceFieldset) {
            query += f.getFieldPath() + COMMASPACE;
        }
        System.debug(recordId);
        query += 'Id ';
        query += 'FROM ECC_Loan_Insurance_Mgmt__c  ';
        query += 'WHERE Id = \'' + recordId + '\' LIMIT 1';
        
        return Database.query(query);
    }
    
    private static final String PARAMETER_NAME = 'id';
    private static final String FIELDSETLIST = 'CRINT_Loan_Insurance_Loan_Information';
    private static final String FIELDSETINDIVIDUAL = 'CRINT_Loan_Insurance_Loan_Information_De';
    private static final String SELECTSTATEMENT = 'SELECT ';
    private static final String COMMASPACE = ', ';
}