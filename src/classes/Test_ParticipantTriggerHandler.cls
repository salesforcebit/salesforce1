@isTest
 private class Test_ParticipantTriggerHandler
 {
  static testMethod void testParticipantTriggerHandler()
  {

        List<Id>tIds=new List<Id>();
        List<Id>tIdp=new List<Id>();
        List<Id>tIdc=new List<Id>();
        
        Profile p = [select id from profile where name='System Administrator'];
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan@keybank.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = p.Id, country='United States',
                                 timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan6@keybank.com',PLOB__c='support',PLOB_Alias__c='support',UserPermissionsMarketingUser = true);             
        insert testUser;
        Account acc = new Account(name ='Acc Test');
        insert acc;
        
        Campaign cmp = new Campaign(Name = 'test program campaign',IsActive = true) ;
        insert cmp;
        
        Program_Member__c prgmMmbr = new Program_Member__c(AccountId__c = acc.id,CampaignId__c = cmp.id);
        insert prgmMmbr;
        System.debug('Account Id Log:'+prgmMmbr.AccountId__c);
        
        Map<String, Schema.SObjectType> consObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType cons = consObjectMap.get('Contact') ;
        Schema.DescribeSObjectResult resSchemaCon = cons.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> conRecordTypeInfo = resSchemaCon.getRecordTypeInfosByName();
        Id conRTId = conRecordTypeInfo.get('Key Employee').getRecordTypeId();
        System.Debug('conRTId:'+conRTId);
            
        contact con = new contact(LastName='Mr. Dhanesh Subramanian',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc.id,Salesforce_User__c=true,User_Id__c=testUser.Id,recordtypeId = conRTId,phone='9876543210' );
        insert con;    
        Call__c c = new Call__c(Subject__c='test',Call_Date__c=system.today(),Call_Type__c='COI Contact',AccountId__c=acc.id,Status__c='done');
        insert c;
        Participant__c p1 = new Participant__c(CallId__c=c.id,Contact_Id__c=con.id);
        insert p1;
        
        tIds.add(p1.CallId__c);
        tIdp.add(p1.id);
        tIdc.add(p1.Contact_Id__c);
        
        Test.startTest();
        ParticipantTriggerHandler.OnAfterInsertAsync(tIds,tIdp,tIdc);
        Test.stopTest();
      }
      static testMethod void testCallGoal()
      {
      Profile p = [select id from profile where name='System Administrator'];
        List<User> listuser = new List<user>();
        User testUser = new User(alias = 'u2', email='vidhyasagaran_muralidharan1@keybank.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = p.Id, country='United States',
                                 timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan12@keybank.com',PLOB__c='support',PLOB_Alias__c='support',UserPermissionsMarketingUser = true);             
        listuser.add(testUser);
        User testUser1 = new User(alias = 'u3', email='vidhyasagaran_muralidharan12@keybank.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = p.Id, country='United States',
                                 timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan22@keybank.com',PLOB__c='support',PLOB_Alias__c='support',UserPermissionsMarketingUser = true);             
        listuser.add(testUser1);
        insert listuser;
       Map<String, Schema.SObjectType> consObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType cons = consObjectMap.get('Contact') ;
        Schema.DescribeSObjectResult resSchemaCon = cons.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> conRecordTypeInfo = resSchemaCon.getRecordTypeInfosByName();
        Id conRTId = conRecordTypeInfo.get('Key Employee').getRecordTypeId();
        System.Debug('conRTId:'+conRTId);
       Account acc = new Account(name ='Acc Test');
        insert acc;
        contact con = new contact(LastName='Mr. Dhanesh Subramanian',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc.id,Salesforce_User__c=true,User_Id__c=testUser.Id,recordtypeId = conRTId,phone='9876543210' );
        insert con;    
        Call__c c = new Call__c(Subject__c='test',Call_Date__c=system.today(),Call_Type__c='COI Contact',AccountId__c=acc.id,Status__c='done');
        insert c;
        Participant__c p1 = new Participant__c(CallId__c=c.id,Contact_Id__c=con.id,Did_Not_Participate__c=false);
        insert p1;
      Call_goals__c callGoal=new Call_Goals__c(Username__c=testuser.id,Goal_Month__c=system.today());
      insert callGoal;
      callGoal.username__c=testuser1.id;
      update callGoal;
      }
}