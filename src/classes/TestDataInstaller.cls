@isTest
private class TestDataInstaller {
	
	@isTest
	static void testDataInstallerOnInstall() {
		Test.startTest();

		DataInstaller installer = new DataInstaller();
		installer.onInstall(null);

		List<nFORCE__Route__c> installedRoutes = getInstalledRoutes();

		System.assertEquals(6, installedRoutes.size());

		Test.stopTest();
	}

	@isTest
	static void testDataInstallerOnUninstall() {
		Test.startTest();

		DataInstaller installer = new DataInstaller();
		installer.onInstall(null);

		List<nFORCE__Route__c> installedRoutes = getInstalledRoutes();

		System.assertEquals(6, installedRoutes.size());
		
		installer.onUninstall(null);
		installedRoutes = getInstalledRoutes();
		
		System.assertEquals(null, installedRoutes);

		Test.stopTest();
	}

	private static List<nFORCE__Route__c> getInstalledRoutes(){
		List<nFORCE__Route__c> routes = [
			SELECT
				Id,
				Name,
				nFORCE__Body__c
			FROM
				nFORCE__Route__c
		];
		if(routes.size() > 0){
			return routes;
		}
		return null;
	}
	
}