@isTest 
public with sharing class testTriggerLegalEntities {

    static testMethod void testInsertTrigger(){
        Account a = UnitTestFactory.buildTestAccount();
        insert a;

        Contact c = UnitTestFactory.buildTestContact(a.id);
        c.Authorized_Signer__c = true;
        c.Contact_Email__c = 'testUser@key.com';
        insert c;

        LLC_BI__Treasury_Service__c ts = UnitTestFactory.buildTestTreasuryService();
        insert ts;

        LLC_BI__Legal_Entities__c le = UnitTestFactory.buildTestLegalEntities(a.id , ts.id);

        //check to make sure the contact doesnt have  a signer
        List<LLC_BI__Contingent_Liabilty__c> signer = [select id, LLC_BI__Contact__c from LLC_BI__Contingent_Liabilty__c where LLC_BI__Contact__c =: c.id];

        System.assertEquals(0, signer.size());

        test.startTest();
            insert le;
        test.stopTest();

        //check to make sure the contact has a signer now
        signer = [select id, LLC_BI__Contact__c from LLC_BI__Contingent_Liabilty__c where LLC_BI__Contact__c =: c.id];

        System.assertEquals(c.id, signer[0].LLC_BI__Contact__c);
    }

    static testMethod void testUpdateTrigger(){
        Account a = UnitTestFactory.buildTestAccount();
        insert a;

        Contact c = UnitTestFactory.buildTestContact(a.id);
        c.Authorized_Signer__c = true;
        c.Contact_Email__c = 'testUser@key.com';
        insert c;

        LLC_BI__Treasury_Service__c ts = UnitTestFactory.buildTestTreasuryService();
        insert ts;

        LLC_BI__Legal_Entities__c le = UnitTestFactory.buildTestLegalEntities(a.id , null);
        insert le;

        //check to make sure the contact doesnt have  a signer
        List<LLC_BI__Contingent_Liabilty__c> signer = [select id, LLC_BI__Contact__c from LLC_BI__Contingent_Liabilty__c where LLC_BI__Contact__c =: c.id];

        System.assertEquals(0, signer.size());
        le.LLC_BI__Treasury_Service__c  = ts.id;

        test.startTest();
            update le;
        test.stopTest();

        //check to make sure the contact has a signer now
        signer = [select id, LLC_BI__Contact__c from LLC_BI__Contingent_Liabilty__c where LLC_BI__Contact__c =: c.id];

        System.assertEquals(c.id, signer[0].LLC_BI__Contact__c);
    }


}