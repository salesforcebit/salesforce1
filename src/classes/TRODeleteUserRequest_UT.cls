@isTest
private class TRODeleteUserRequest_UT {

  static testMethod void deleteUserRequestTest() {
  	TRODeleteUserRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_deleteUserRequest_Port testDeleteUser = new TRODeleteUserRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_deleteUserRequest_Port();

  	TRODeleteUserRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_DeleteUser_ExportFields();

  	Test.setMock(WebServiceMock.class, new TRO_DeleteUserRequest_Mock());

		Test.startTest();
  		TRODeleteUserRequest_v2.KTTResponse response = testDeleteUser.processDeleteUserAPI(exportFields);
		Test.stopTest();

		System.assertEquals(response.CSOAutomation.KTTResponse.status, 'Good to go');

  }

  static testMethod void aSyncDeleteUserRequestTest() {
    AsyncTRODeleteUserRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_deleteUserRequest_Port aSyncAddService = new AsyncTRODeleteUserRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_deleteUserRequest_Port();

    TRODeleteUserRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_deleteUser_ExportFields();
    Continuation con = new Continuation(120);
    con.continuationMethod = 'processResponse';

    Test.setMock(WebServiceMock.class, new TRO_AsyncDeleteUserRequest_Mock());

    AsyncTRODeleteUserRequest_v2.processDeleteUserAPIResponseFuture response = aSyncAddService.beginProcessDeleteUserAPI(con, exportFields);

    }
}