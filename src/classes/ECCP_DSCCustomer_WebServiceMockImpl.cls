@isTest
global class ECCP_DSCCustomer_WebServiceMockImpl implements WebServiceMock {
global void doInvoke(
       Object stub,
       Object request,
       Map<string,object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
       
       ECCP_DSCCustomer.getPreLoadDataResponse responseElement = new ECCP_DSCCustomer.getPreLoadDataResponse();
       ECCP_DSCCustomer.PreLoadResponseDT PreLoadRespDT = new ECCP_DSCCustomer.PreLoadResponseDT();
       ECCP_DSCCustomer.PreLoadResponse  PreLoadResp = new ECCP_DSCCustomer.PreLoadResponse();
       ECCP_DSCCustomer.Loan lon = new ECCP_DSCCustomer.Loan();
      
       ECCP_DSCCustomer.PartyProfile prt = new ECCP_DSCCustomer.PartyProfile();
       List<ECCP_DSCCustomer.PartyProfile> prtList = new List<ECCP_DSCCustomer.PartyProfile>();
       ECCP_DSCCustomer.Cuac cuac = new ECCP_DSCCustomer.Cuac();
       List<ECCP_DSCCustomer.Cuac> cuacList = new List<ECCP_DSCCustomer.Cuac>();
       ECCP_DSCCustomer.Account acc = new ECCP_DSCCustomer.Account();
       ECCP_DSCCustomer.Account accDep = new ECCP_DSCCustomer.Account();
       ECCP_DSCCustomer.Account accExcep= new ECCP_DSCCustomer.Account();
       List<ECCP_DSCCustomer.Account> accList = new List<ECCP_DSCCustomer.Account>();
       List<ECCP_DSCCustomer.Account> accListExc = new List<ECCP_DSCCustomer.Account>();
       
       acc.AcctID = '12345667';
       acc.AccountType = 'Credit';
       acc.AccountNumber =  '1';
      
         acc.AccountName= 'name';
         acc.BankNumber= '123';
         acc.BalanceAmount= '123';
         acc.ChargeOffFlag= 'N';
         acc.ExposureFlag= 'Y';
         acc.CommitmentAmount= '123';
         acc.OpenDate= '2006-04-20';
         acc.ProductCode= '123';
         acc.SSN= '123456789';
         acc.SubProductCode= 'subP';
         acc.AccountStatus= 'Open';
         acc.MaturityDate= '2006-04-20';
         acc.CL3Obligation= '123';
         acc.CL3Obligor= '123';
         acc.CRACode= '234';
         acc.FacilityRR= '123';
         acc.NSFCnt_12_M= '123';
         acc.Obligor_RR= '445';
         acc.Obligor_POD= '1';
         acc.ODCnt_12_M= '432';
         acc.PmtAmt= '123';
         acc.PmtDueDate= '2006-04-20';
         acc.ReviewDate= '2006-04-20';
         acc.TSYSNumber= '123';
         acc.DDA_6M_Ave_bal= '1234';
         
                  accDep.AcctID = '12345667';
         accDep.AccountType = 'CreditDeposit';
         accDep.AccountNumber =  '2';
      
         accDep.AccountName= 'name';
         accDep.BankNumber= '123';
         accDep.BalanceAmount= '123';
         accDep.ChargeOffFlag= 'N';
         accDep.ExposureFlag= 'Y';
         accDep.CommitmentAmount= '123';
         accDep.OpenDate= '2006-04-20';
         accDep.ProductCode= '123';
         accDep.SSN= '123456789';
         accDep.SubProductCode= 'subP';
         accDep.AccountStatus= 'Open';
         accDep.MaturityDate= '2006-04-20';
         accDep.CL3Obligation= '123';
         accDep.CL3Obligor= '123';
         accDep.CRACode= '234';
         accDep.FacilityRR= '123';
         accDep.NSFCnt_12_M= '123';
         accDep.Obligor_RR= '445';
         accDep.Obligor_POD= '1';
         accDep.ODCnt_12_M= '432';
         accDep.PmtAmt= '123';
         accDep.PmtDueDate= '2006-04-20';
         accDep.ReviewDate= '2006-04-20';
         accDep.TSYSNumber= '123';
         accDep.DDA_6M_Ave_bal= '1234';
         
         accExcep.AcctID = '12345667';
         accExcep.AccountType = 'CreditDeposit';
         accExcep.AccountNumber = '3';
      
         accExcep.AccountName= 'name';
         accExcep.BankNumber= '123';
         accExcep.BalanceAmount= '123';
         accExcep.ChargeOffFlag= 'N';
         accExcep.ExposureFlag= 'Y';
         accExcep.CommitmentAmount= '123';
         accExcep.OpenDate= '2006-04-20';
         accExcep.ProductCode= '123';
         accExcep.SSN= '123456789';
         accExcep.SubProductCode= 'subP';
         accExcep.AccountStatus= 'Open';
         accExcep.MaturityDate= '2006-04-20';
         accExcep.CL3Obligation= '123';
         accExcep.CL3Obligor= '123';
         accExcep.CRACode= '234';
         accExcep.FacilityRR= '123';
         accExcep.NSFCnt_12_M= '123';
         accExcep.Obligor_RR= '445';
         accExcep.Obligor_POD= '789';
         accExcep.ODCnt_12_M= '432';
         accExcep.PmtAmt= '123';
         accExcep.PmtDueDate= '2006-04-20';
         accExcep.ReviewDate= '2006-04-20';
         accExcep.TSYSNumber= '123';
         accExcep.DDA_6M_Ave_bal= '1234fgj';   

       accList.add(acc);
       accList.add(accDep);
       accListExc.add(accExcep);
       cuac.Account = accList;
       cuac.accountIDREF = 'Borrower';
       
       cuacList.add(cuac);
       prt.SocialSecurityNumber ='567908999';
       prt.Cuac = cuacList;
       prtList.add(prt);
       lon.TotalConsumerDepositBalance ='123';
       lon.ExposureCalAmount ='123';
       lon.PartyProfile = prtList;
       PreLoadResp.Loan = lon;
       PreLoadRespDT.PreLoadResponse = PreLoadResp;
       responseElement.PreLoadResponseDT = PreLoadRespDT;
       response.put('response_x', responseElement);
       
       }
       
       
    }