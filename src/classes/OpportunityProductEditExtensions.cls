//================================================================================
//  KeyBank  
//  Object: Opportunity
//  Author: Offshore
//  Detail: OpportunityProductEditExtensions 
//  Description :To Implement the PrimaryProduct Check/uncheck Requirement For Opportunity Product.

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================


public with sharing class OpportunityProductEditExtensions {
    
 public Opportunitylineitem OLI{Get; set;}
 public Id oliId {get; set;}
 public Boolean primarycheck{get;set;}
 Public Boolean userpermission{get;set;}
 public opportunityLineItem[] CheckPrimaryProduct {get;set;}
 //Public Map<Id, OpportunityLineItem> mapOptyLineitem = new Map<Id, OpportunityLineItem>();
 public Set<Opportunitylineitem > setlineitem= New Set<Opportunitylineitem> ();
   ApexPages.StandardController controller;
 
 public OpportunityProductEditExtensions(ApexPages.StandardController controller){
    //this.controller=controller;
 userpermission=false;
  OLI = [Select Id,PricebookEntry.Product2Id,PricebookEntry.Product2.Name,name,Primary_Product__c,Cost_of_Funds__c,Product_Category__c,Spread__c,Pricing_Yield1__c,Product_Type__c,Feature_Type__c,Deal_Term_in_Months__c,
         Payment_Frequency__c,Payment_Type__c,Commitment_Non_Usage_Renewal_Fee__c ,Inc_Bal_Amount__c,TESTIncr_New_Commitment__c,Pricing_ROE__c,NPV__c,Doc_Origination_Fee__c,House_Deal__c,Date_Opened__c,Index_Used__c,Day_s_Opened__c,Interest_Variability_Code__c,
         Opportunity.id from Opportunitylineitem where id =:controller.getRecord().id limit 1 ];
         system.debug('value of cost--'+OLI);
           oliId = OLI.Opportunity.id;
           
  CheckPrimaryProduct=[select id, PriceBookEntryId, PriceBookEntry.Name,Primary_Product__c, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name,
                       PriceBookEntry.PriceBook2Id from opportunityLineItem where opportunityid=:oli.Opportunity.Id and Primary_Product__c=true];
   
   
    setlineitem.addall(CheckPrimaryProduct);
    system.debug('primarycheck:'+primarycheck);
    
    List<PermissionSetAssignment> lstcurrentUserPerSet = [  SELECT Id, PermissionSet.Name,AssigneeId
                                                                FROM PermissionSetAssignment
                                                                WHERE AssigneeId = :Userinfo.getUserId() ];
    
    for (PermissionSetAssignment psa: lstcurrentUserPerSet)
    {
    system.debug('##psa.PermissionSet.Name' + psa.PermissionSet.Name);
        if ( psa.PermissionSet.Name.equals('KEFOptysubmittedFieldPermission') ) {
        userpermission=true;
        }
                  
    }
    
 } 

  public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Opportunity   
      PageReference pageRef = new PageReference ('/'+oli.Id);
      return pageRef ;
      
    }
    
    public void primaryPrd(){
        //system.debug('primarycheck:'+primarycheck);
        system.debug('primarycheck:'+ApexPages.currentPage().getParameters().get('primarycheck'));
        system.debug('OpportunityLineItem.Primary_Product__c:'+OLI.Primary_Product__c);
        // system.debug('OpportunityLineItembvalue:'+OLI.NPV__c);
        
        
    }
    
     public PageReference onsave(){
    try{
            //check the existing product present and if the value is true..then make the existing primary value uncheck and keep checked the new Product record.
            system.debug('CheckPrimaryProduct.size():'+CheckPrimaryProduct.size());
            if(CheckPrimaryProduct != null && CheckPrimaryProduct.size()>0 && OLI.Primary_Product__c == true)
            {
                if(CheckPrimaryProduct[0].Id != OLI.id )
                {
                    CheckPrimaryProduct[0].Primary_Product__c=false;
                    system.debug('valueCARTELSEif1---'+CheckPrimaryProduct[0].Primary_Product__c);
                    CheckPrimaryProduct.add(OLI);
                    System.debug('CheckPrimaryProduct.size():'+CheckPrimaryProduct.size());
                    update(CheckPrimaryProduct);
                    PageReference pageRef = new PageReference ('/'+oliId );
                    return pageRef ;
                     
                   
                }
                
                else{
                       system.debug('Else part'+oli);
                       ApexPages.StandardController controller = new ApexPages.StandardController(OLI);
                       
                       if(controller.save() == null)
                       {
                           return null;
                       }
                       /* if(OLI.Deal_Term_in_Months__c >360){
                            system.debug('valueofterm---'+OLI.Deal_Term_in_Months__c);
                            return null;
                        }*/
                       
                        else{
                        
                        PageReference pageRef = new PageReference ('/'+oliId );
                        return pageRef ;
                        }
                         
                }
                
            }
            
           // If user manually uncheck and save it will save the product
            else if(CheckPrimaryProduct != null && CheckPrimaryProduct.size()>0 && OLI.Primary_Product__c == false)
            {
                 if(CheckPrimaryProduct[0].Id != OLI.id ){
                    ApexPages.StandardController controller = new ApexPages.StandardController(OLI);
                   if (null==controller.save())
                    {
                           return null;
                    }
                    /*if(OLI.Deal_Term_in_Months__c >360){
                        system.debug('valueofterm---'+OLI.Deal_Term_in_Months__c);
                        return null;
                    }*/
                    else{
                    PageReference pageRef = new PageReference ('/'+oliId );
                    return pageRef ;
                     
                   } 
                 }
                 else{
                 //If it is the only one primary product and user manually uncheck the checkbox then error will show after save
                    system.debug('Else part');
                      ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR,'One primary product is reqiured for each Opportunity!');
                      ApexPages.addMessage(msg);
                      return null;
                }
            }
           
             
       }
            catch(DMLException e)
            {  
                ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
                ApexPages.addMessage(msg);
                system.debug('Exception:'+e.getmessage());
                return null;
        
            }   
    
       return new PageReference('/'+ ApexPages.currentPage().getParameters().get('Id')); 
        
    }
}