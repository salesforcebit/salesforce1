@istest
private class TreasuryServicePendingPrereqsBatch_UT {

	static testmethod void testPendingPrereqsBatch() {
		Account testAccount = UnitTestFactory.buildTestAccount();
		insert testAccount;

		Product2 testProduct = UnitTestFactory.buildTestProduct();
		testProduct.Family = 'Deposits & ECP';
		insert testProduct;

		LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
		insert testProductLine;

		LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
		insert testProductType;

		LLC_BI__Product__c testNCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
		insert testNCinoProduct;

		LLC_BI__Product_Package__c testProductPackage = UnitTestFactory.buildTestProductPackage();
		insert testProductPackage;

		LLC_BI__Treasury_Service__c testPrerequisiteTestingTS = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testPrerequisiteTrainingTS = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testPrerequisiteFulfillmentTS = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		testPrerequisiteTestingTS.LLC_BI__Stage__c = Constants.treasuryServiceStageTesting;
		testPrerequisiteTrainingTS.LLC_BI__Stage__c = Constants.treasuryServiceStageTraining;
		testPrerequisiteFulfillmentTS.LLC_BI__Stage__c = Constants.treasuryServiceStageFulfillment;
		insert new List<LLC_BI__Treasury_Service__c> {testPrerequisiteTestingTS, testPrerequisiteTrainingTS, testPrerequisiteFulfillmentTS};

		List<LLC_BI__Treasury_Service__c> testTSes = new List<LLC_BI__Treasury_Service__c>();
		LLC_BI__Treasury_Service__c testTSPendingPrereqsTestingTodayImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsTestingPastImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsTestingNullImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsTestingFutureImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsTrainingTodayImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsTrainingPastImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsTrainingNullImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsTrainingFutureImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsFulfillmentTodayImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsFulfillmentPastImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsFulfillmentNullImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqsFulfillmentFutureImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSOrderEntryTestingTodayImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);

		testTSPendingPrereqsTestingTodayImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsTestingPastImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsTestingNullImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsTestingFutureImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsTrainingTodayImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsTrainingPastImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsTrainingNullImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsTrainingFutureImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsFulfillmentTodayImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsFulfillmentPastImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsFulfillmentNullImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSPendingPrereqsFulfillmentFutureImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSOrderEntryTestingTodayImp.LLC_BI__Stage__c = Constants.treasuryServiceStageOrderEntry;
		testTSPendingPrereqsTestingTodayImp.Prerequisite_Treasury_Service__c = testPrerequisiteTestingTS.Id;
		testTSPendingPrereqsTestingPastImp.Prerequisite_Treasury_Service__c = testPrerequisiteTestingTS.Id;
		testTSPendingPrereqsTestingNullImp.Prerequisite_Treasury_Service__c = testPrerequisiteTestingTS.Id;
		testTSPendingPrereqsTestingFutureImp.Prerequisite_Treasury_Service__c = testPrerequisiteTestingTS.Id;
		testTSPendingPrereqsTrainingTodayImp.Prerequisite_Treasury_Service__c = testPrerequisiteTrainingTS.Id;
		testTSPendingPrereqsTrainingPastImp.Prerequisite_Treasury_Service__c = testPrerequisiteTrainingTS.Id;
		testTSPendingPrereqsTrainingNullImp.Prerequisite_Treasury_Service__c = testPrerequisiteTrainingTS.Id;
		testTSPendingPrereqsTrainingFutureImp.Prerequisite_Treasury_Service__c = testPrerequisiteTrainingTS.Id;
		testTSPendingPrereqsFulfillmentTodayImp.Prerequisite_Treasury_Service__c = testPrerequisiteFulfillmentTS.Id;
		testTSPendingPrereqsFulfillmentPastImp.Prerequisite_Treasury_Service__c = testPrerequisiteFulfillmentTS.Id;
		testTSPendingPrereqsFulfillmentNullImp.Prerequisite_Treasury_Service__c = testPrerequisiteFulfillmentTS.Id;
		testTSPendingPrereqsFulfillmentFutureImp.Prerequisite_Treasury_Service__c = testPrerequisiteFulfillmentTS.Id;
		testTSOrderEntryTestingTodayImp.Prerequisite_Treasury_Service__c = testPrerequisiteTestingTS.Id;
		testTSPendingPrereqsTestingTodayImp.Planned_Implementation_Start_Date__c = date.today();
		testTSPendingPrereqsTestingPastImp.Planned_Implementation_Start_Date__c = date.today().addDays(-7);
		testTSPendingPrereqsTestingNullImp.Planned_Implementation_Start_Date__c = null;
		testTSPendingPrereqsTestingFutureImp.Planned_Implementation_Start_Date__c = date.today().addDays(7);
		testTSPendingPrereqsTrainingTodayImp.Planned_Implementation_Start_Date__c = date.today();
		testTSPendingPrereqsTrainingPastImp.Planned_Implementation_Start_Date__c = date.today().addDays(-7);
		testTSPendingPrereqsTrainingNullImp.Planned_Implementation_Start_Date__c = null;
		testTSPendingPrereqsTrainingFutureImp.Planned_Implementation_Start_Date__c = date.today().addDays(7);
		testTSPendingPrereqsFulfillmentTodayImp.Planned_Implementation_Start_Date__c = date.today();
		testTSPendingPrereqsFulfillmentPastImp.Planned_Implementation_Start_Date__c = date.today().addDays(-7);
		testTSPendingPrereqsFulfillmentNullImp.Planned_Implementation_Start_Date__c = null;
		testTSPendingPrereqsFulfillmentFutureImp.Planned_Implementation_Start_Date__c = date.today().addDays(7);
		testTSOrderEntryTestingTodayImp.Planned_Implementation_Start_Date__c = date.today();

		testTSes.add(testTSPendingPrereqsTestingTodayImp);
		testTSes.add(testTSPendingPrereqsTestingPastImp);
		testTSes.add(testTSPendingPrereqsTestingNullImp);
		testTSes.add(testTSPendingPrereqsTestingFutureImp);
		testTSes.add(testTSPendingPrereqsTrainingTodayImp);
		testTSes.add(testTSPendingPrereqsTrainingPastImp);
		testTSes.add(testTSPendingPrereqsTrainingNullImp);
		testTSes.add(testTSPendingPrereqsTrainingFutureImp);
		testTSes.add(testTSPendingPrereqsFulfillmentTodayImp);
		testTSes.add(testTSPendingPrereqsFulfillmentPastImp);
		testTSes.add(testTSPendingPrereqsFulfillmentNullImp);
		testTSes.add(testTSPendingPrereqsFulfillmentFutureImp);
		testTSes.add(testTSOrderEntryTestingTodayImp);
		for(LLC_BI__Treasury_Service__c service : testTSes) {
			service.Documents_Complete__c = true;
		}
		insert testTSes;

		TreasuryServicePendingPrereqsBatch batch = new TreasuryServicePendingPrereqsBatch();
		test.startTest();
			Database.executeBatch(batch);
		test.stopTest();

		Set<Id> expectedFulfillmentIds = new Set<Id>{testTSPendingPrereqsTestingTodayImp.Id, testTSPendingPrereqsTestingPastImp.Id, testTSPendingPrereqsTestingNullImp.Id, testTSPendingPrereqsTrainingTodayImp.Id, testTSPendingPrereqsTrainingPastImp.Id, testTSPendingPrereqsTrainingNullImp.Id};
		Set<Id> expectedPendingFutureIds = new Set<Id>{testTSPendingPrereqsTestingFutureImp.Id, testTSPendingPrereqsTrainingFutureImp.Id};
		Set<Id> expectedImpStartNextDayIds = new Set<Id>{testTSPendingPrereqsFulfillmentTodayImp.Id, testTSPendingPrereqsFulfillmentPastImp.Id};
		Set<Id> expectedNoChangesIds = new Set<Id>{testTSPendingPrereqsFulfillmentNullImp.Id, testTSPendingPrereqsFulfillmentFutureImp.Id, testTSOrderEntryTestingTodayImp.Id};

		for(LLC_BI__Treasury_Service__c resultTS : [Select Id, LLC_BI__Stage__c, Planned_Implementation_Start_Date__c from LLC_BI__Treasury_Service__c Where Id in: expectedFulfillmentIds]) {
			System.assertEquals(Constants.treasuryServiceStageFulfillment, resultTS.LLC_BI__Stage__c);
		}
		for(LLC_BI__Treasury_Service__c resultTS : [Select Id, LLC_BI__Stage__c, Planned_Implementation_Start_Date__c from LLC_BI__Treasury_Service__c Where Id in: expectedPendingFutureIds]) {
			System.assertEquals(Constants.treasuryServiceStagePendingFutureStartDate, resultTS.LLC_BI__Stage__c);
		}
		for(LLC_BI__Treasury_Service__c resultTS : [Select Id, LLC_BI__Stage__c, Planned_Implementation_Start_Date__c, Date_Implementation_Delayed__c from LLC_BI__Treasury_Service__c Where Id in: expectedImpStartNextDayIds]) {
			System.assertEquals(BusinessHours.nextStartDate(Constants.defaultBusinessHours.Id, date.today().addDays(1)).date(), resultTS.Planned_Implementation_Start_Date__c);
			System.assertEquals(date.today(), resultTS.Date_Implementation_Delayed__c);
		}
		for(LLC_BI__Treasury_Service__c resultTS : [Select Id, LLC_BI__Stage__c, Planned_Implementation_Start_Date__c from LLC_BI__Treasury_Service__c Where Id in: expectedNoChangesIds]) {
			System.assertNotEquals(Constants.treasuryServiceStageFulfillment, resultTS.LLC_BI__Stage__c);
		}
	}

}