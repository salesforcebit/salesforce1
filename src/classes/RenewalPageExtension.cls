/**
 * This controller extension is used by the RenewalPage to drive backend functionality
 */
public with sharing class RenewalPageExtension {

    public Boolean disableSave {get; set;}
    public Boolean showAlert {get; set;}
    public String renewalType {get; set;}

    public List<SelectOption> renewalTypes {
        get {
            if(renewalTypes == null) {

                Schema.DescribeFieldResult fieldResult = LLC_BI__Scenario__c.Renewal_Type__c.getDescribe();
                renewalTypes = new List<SelectOption>();
                for(Schema.PicklistEntry pck : fieldResult.getPicklistValues()){
                    if(pck.isActive())
                        renewalTypes.add(new SelectOption(pck.getValue(), pck.getLabel() ) );   
                }
                //renewalTypes.add(new SelectOption('Return to Standard Pricing','Return to Standard Pricing'));
                //renewalTypes.add(new SelectOption('Renew Existing Pricing','Renew Existing Pricing'));
                //renewalTypes.add(new SelectOption('Pricing Modification Renewal','Pricing Modification Renewal'));
            }
            return renewalTypes;
        } private set;
    }

    private Boolean success;
    private LLC_BI__Scenario__c scenario;
    private LLC_BI__Scenario__c renewedScenario;
    private List<LLC_BI__Scenario_Item__c> scenarioItems;
    private Opportunity childOpportunity;
    private List<OpportunityLineItem> lineItems;

    public RenewalPageExtension(ApexPages.StandardController stdController) {
        Id scenarioId = stdController.getId();
        disableSave = false;
        showAlert = false;
        success = true;
        loadScenario(scenarioId);
    }

    /**
     * This method processes the renewals and is initially invoked via the save and continue button
     */
    public PageReference processRenewals() {
        showAlert = false;
        Savepoint sp = Database.setSavepoint();
        if(renewalType == 'Return to Standard Pricing'
                || renewalType == 'Renew Existing Pricing') {
            renewedScenario = generateScenario();
            renewedScenario.LLC_BI__Opportunity__c = scenario.LLC_BI__Opportunity__c;
            performInsert(new List<SObject>{renewedScenario});
        } else if(renewalType == 'Pricing Modification Renewal') {
            createChildOpporutnity();
            loadOpportunityLineItems(scenario.LLC_BI__Opportunity__c);
            cloneOpportunityLineItems();
            renewedScenario = generateScenario();
            renewedScenario.LLC_BI__Opportunity__c = childOpportunity.Id;
            performInsert(new List<SObject>{renewedScenario});
        } else {
            return null;
        }
        cloneScenarioItems();
        if(!success) {
            Database.rollback(sp);
            return null;
        }
        if(renewedScenario.Id != null) {
            PageReference renewedScenarioPage = new ApexPages.StandardController(renewedScenario).view();
            renewedScenarioPage.setRedirect(true);
            return renewedScenarioPage;
        } else {
            return null;
        }
    }

    /* sets flag to show modal */
    public void showAlert() {
        showAlert = true;
    }

    /* sets flag to hide modal */
    public void closeAlert() {
        showAlert = false;
    }

    /**
     * This method creates a renewal scenario record
     * @return LLC_BI__Scenario__c renewal scenario
     */
    private LLC_BI__Scenario__c generateScenario() {
        LLC_BI__Scenario__c scen = new LLC_BI__Scenario__c();
        scen.Name = createRenewalName(scenario.Name);
        scen.LLC_BI__Status__c = 'Draft';
        scen.Renewal__c = true;
        scen.Renewal_Type__c = renewalType;
        scen.Exception_Pricing_Expiration_Date2__c = Date.today().addYears(1);
        return scen;
    }


    /**
     * This method creates the child opportunity for a pricing mod renewal
     */
    private void createChildOpporutnity() {
        childOpportunity = new Opportunity();
        childOpportunity.Parent_Opportunity_Id__c = scenario.LLC_BI__Opportunity__c;
        childOpportunity.AccountId = scenario.Account__c;
        childOpportunity.RecordTypeId = getEcpRecordTypeId();
        childOpportunity.Opportunity_Type__c = 'Pricing Maintenance';
        childOpportunity.CloseDate = Date.today();
        childOpportunity.Actual_Close_Date__c = Date.today();
        childOpportunity.StageName = 'Closed Lost';
        childOpportunity.Reason__c = 'ECP Pricing Maintenance';
        childOpportunity.Private__c = true;
        childOpportunity.Private_Code_Name__c = 'ECP Pricing Maintenance';
        childOpportunity.Name = createRenewalName(scenario.Name);
        performInsert(new List<SObject>{childOpportunity});
    }

    /**
     * This method uses regex replacements to create a new renewal name that truncates at 80 characters
     * @param  String original      original name
     * @return        new renewal name
     */
    private String createRenewalName(String original) {
        if(original == null) return original;
        Integer lastIndex = original.lastIndexOf('-');
        if(lastIndex != -1 && original.endsWith('Renewal')) original = original.subString(0,lastIndex);
        Date today = Date.today();
        String newName = original + ' - ' + String.valueOf(today.month()) +
                + '/' + String.valueOf(today.year() + 1) + ' Renewal';
        if(newName.length() > 80) newName = newName.subString(0,79);
        return newName;
    }

    //================================================================================
    // Query Methods
    //================================================================================

    /**
     * This method loads the scenario into the view state. In the event that no Id is present, an
     * error is thrown
     * @param  Id scenarioId    scenario Id in URL header
     */
    private void loadScenario(Id scenarioId) {
        if(scenarioId != null) {
            try{
                scenario = [SELECT Renewal_Type__c, LLC_BI__Opportunity__c, Name, Account__c
                        FROM LLC_BI__Scenario__c WHERE Id =: scenarioId];
                loadScenarioItems(scenarioId);
            } catch(QueryException e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'An existing Scenario '
                    + 'Id must be specified.'));
            disableSave = true;
        }
    }

    /**
     * This method loads all of the scenario items so that they are ready for cloning. This
     * is not done in the scenario as a child query in order to utilize dynamic field retrievals
     * and also to keep the view state size small in the event of a large number of scenario items
     * @param  Id scenarioId    Id of scenario
     */
    private void loadScenarioItems(Id scenarioId) {
        Set<String> itemFields = LLC_BI__Scenario_Item__c.getSObjectType().getDescribe().fields.getMap().keySet();
        List<LLC_BI__Scenario_Item__c> items = new List<LLC_BI__Scenario_Item__c>();
        String queryString = 'SELECT ';
        for(String field : itemFields) {
            queryString += field + ', ';
        }
        queryString = queryString.removeEnd(', ');
        queryString += ' FROM LLC_BI__Scenario_Item__c WHERE LLC_BI__Scenario__c = \'' + scenarioId
                + '\'';
        System.debug('### Scenario Item Query: ' + queryString);
        try{
            scenarioItems = Database.query(queryString);
        } catch(QueryException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
        }
    }

    /**
     * This method loads all of the opportunity line items from the existing scenario's opportuntiy
     * so that they are ready for cloning.
     * @param  Id opportunityId    Id of opportunity
     */
    private void loadOpportunityLineItems(Id opportunityId) {
        Set<String> itemFields = OpportunityLineItem.getSObjectType().getDescribe().fields.getMap().keySet();
        List<OpportunityLineItem> items = new List<OpportunityLineItem>();
        String queryString = 'SELECT ';
        for(String field : itemFields) {
            queryString += field + ', ';
        }
        queryString = queryString.removeEnd(', ');
        queryString += ' FROM OpportunityLineItem WHERE OpportunityId = \'' + opportunityId
                + '\'';
        System.debug('### OpportunityLineItem Query: ' + queryString);
        try{
            lineItems = Database.query(queryString);
        } catch(QueryException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
        }
    }

    /**
     * This method attempts to retrieve the ECP record type Id
     * @return Id ECP record type id
     */
    private Id getEcpRecordTypeId() {
        Map<String,Id> recordTypeIdsByName = new Map<String,Id>();
        for(Schema.RecordTypeInfo rt : Schema.SObjectType.Opportunity.getRecordTypeInfosById().values()) {
			recordTypeIdsByName.put(rt.getName(),rt.getRecordTypeId());
		}
        return recordTypeIdsByName.get('Deposits & ECP');
    }

    //================================================================================
    // Cloning Methods
    //================================================================================

    /**
     * This method clones all of the scenario items upon renewal
     */
    private void cloneScenarioItems() {
        List<LLC_BI__Scenario_Item__c> clonedScenarioItems = new List<LLC_BI__Scenario_Item__c>();
        for(LLC_BI__Scenario_Item__c scenarioItem : scenarioItems) {
            LLC_BI__Scenario_Item__c clonedItem = scenarioItem.clone(false,true,false,false);
            clonedItem.LLC_BI__Scenario__c = renewedScenario.Id;
            if(renewalType == 'Return to Standard Pricing') clonedItem.LLC_BI__Exception_Price__c = null;
            clonedScenarioItems.add(clonedItem);
        }
        performInsert(clonedScenarioItems);
    }

    /**
     * This method clones all of the opportunity line items from a parent to a child
     */
    private void cloneOpportunityLineItems() {
        List<OpportunityLineItem> clonedLineItems = new List<OpportunityLineItem>();
        for(OpportunityLineItem lineItem : lineItems) {
            OpportunityLineItem clonedItem = lineItem.clone(false,true,false,false);
            clonedItem.OpportunityId = childOpportunity.Id;
            clonedItem.UnitPrice = null;
            clonedLineItems.add(clonedItem);
        }
        performInsert(clonedLineItems);
    }

    //================================================================================
    // DML Utilities
    //================================================================================

    /**
     * This method performs dml operations and captures any erros so that they are sent to the
     * apex page
     * @param  List<SObject> records       the records to insert
     */
    private void performInsert(List<SObject> records) {
        if(records != null && !records.isEmpty()) {
            try{
                insert records;
            } catch(Exception e) {
                success = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()
                        + e.getStackTraceString()));
            }
        }
    }

}