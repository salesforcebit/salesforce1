/* 
*This class is created as part of KEF development process.
*It is the Testdata Factory for all objects. 
*/

public with sharing class TestUtilityClass 
{

    //Testdata for Account
    public static List<Account> CreateTestAccount(Integer numAccts)
    {
        List<Account> accts = new List<Account>();
            
        for(Integer i=0;i<numAccts;i++) 
        {
            Account acc = new Account(Name='An_Account' + i);
            acc.BillingStreet = 'BillingStreet';
            acc.BillingCity = 'BillingCity';
            acc.BillingState = 'BillingState';
            acc.BillingPostalCode = 'BillingPostalCode';
            acc.BillingCountry = 'BillingCountry';
            acc.Phone = '7030357222';        
            accts.add(acc);
        }  
        return accts;     
    }
    
    //method for creating a contact record
    public static List<Contact> createTestContacts(Integer numCons, Id accountId)
    {
        List<Contact> conList = new List<Contact>();
        
        for(Integer num = 0; num < numCons; num ++)
        {
            Contact con = new Contact(FirstName = 'FirstName', LastName = 'LastName', AccountId = accountId, MailingCity='City',MailingStreet='Street',MailingState='State',MailingCountry='Country',MailingPostalCode='123456', Phone='9876543210');
            con.Contact_Type__c = System.Label.Business_Contact;  
            conList.add(con);          
        }
        
        return conList;
    }
    
    //test data for Contact Agreement Associations
    public static Contact_Agreement__c  createCAAssociations(id condId, Id agreementId)
    {
        Contact_Agreement__c ca = new Contact_Agreement__c();
        ca.Contact__c = condId;
        ca.Agreement__c = agreementId;
        ca.Contact_Role__c = System.Label.Legal;
        return ca;
    }
    
    //test data for KEF product
    public static List<Product2> createProductsList()
    {
        List<Product2> prodList = Test.loadData(Product2.sObjectType, 'KEFProductsTestData');
        return prodList;
    }
    
    //test data for KEF product Associations
    public static KEF_Product__c  createProductAssociations(Id agreementId, Id prodId)
    {
        KEF_Product__c kefProd = new KEF_Product__c();
        kefProd.Product__c = prodId;
        kefProd.Agreement__c = agreementId;
        return kefProd;
    }
    
    //Testdata for Attachment
    public static Attachment CreateTestAttachment(String parentId) 
    {
        Attachment objatt  = new Attachment();
        objatt.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        objatt.body=bodyBlob;
        objatt.parentId=parentId;
        return objatt;
    }
    
    //Testdata for Agreement 
    public static Agreement__c  CreateTestAgreement()
    
    {
        List<Agreement__c> aggs = new List<Agreement__c>();
            
        Program__c objpgm=new Program__c
        (
            Program_Status__c=System.Label.Prospect, Name_of_the_Vendor_Program__c='Testprogram',
            Business_Development_Stage__c=System.label.BuisnessStage
        );
        insert objpgm;
        
        Agreement__c objagg=new Agreement__c
        (
            Agreement_Name_UDF__c='TestUDF',Agreement_Legal_Status__c=System.Label.informal,Agreement_Type__c=System.label.other,
            Agreement_Status__c=System.label.Prospect,Generate_invoices_to_the_servicer__c='Yes',Servicer_Name__c='Testnme', 
            Default_lessor__c=System.Label.KEF,Program__c=objpgm.id,Agreement_Term__c = 12, Agreement_Commencement_Date__c = System.Today()
        );
    
        return objagg;
    }
    
    //Testdata for Agreement 
    public static Agreement__c  CreateAgreementWithProgramId(Id programId)
    
    {
        Agreement__c objagg = new Agreement__c
        (
            Agreement_Name_UDF__c = 'TestUDF', Agreement_Legal_Status__c = System.Label.informal, 
            Agreement_Type__c = System.label.other, Agreement_Status__c = System.label.Prospect, 
            Generate_invoices_to_the_servicer__c = 'Yes', Servicer_Name__c = 'Testnme', 
            Default_lessor__c = System.Label.KEF, Program__c = programId, Agreement_Term__c = 12, 
            Agreement_Commencement_Date__c = System.Today()
        );
    
        return objagg;
    }
    
    //Testdata for program
    public static Program__c  CreateTestProgram()
    {
        Program__c objpgm1=new Program__c
        (
            Program_Status__c=System.Label.Prospect, Name_of_the_Vendor_Program__c='Testprogram', 
            Business_Development_Stage__c=System.label.BuisnessStage
        );
        return  objpgm1;
    }
  
    //test data for transaction recourse object
    public static Recourse__c createRecourses(Id agreementId)
    {
        Recourse__c tr = new Recourse__c ();
        tr.Agreement__c = agreementId;
        tr.Recourse_Code__c = System.Label.G002_Full_Recourse;
        //tr.Recourse_Amount__c = 100;
        tr.Recourse_Expiry_date__c = System.Today().addDays(100);
        return tr; 
    }
    
    //test data for PayOff Quote object
    public static Payoff_Quote__c createPayoffQuote(Id agreementId)
    {
        Payoff_Quote__c poq = new Payoff_Quote__c();
        poq.Agreement__c = agreementId;
        poq.Payquote_Status__c = System.Label.Active;
        poq.Payquote_Type__c = System.Label.Buyout_to_Keep;
        poq.Discount__c = 10;
        poq.Fee__c = 10;
        poq.Residual__c = 10;
        return poq;
    } 
    
    //test data for Custom Reporting object
    public static Custom_Reporting__c createCustomReporting(Id agreementId)
    {
        Custom_Reporting__c cr = new Custom_Reporting__c();
        cr.Agreement__c = agreementId;
        cr.Custom_Reporting_Values__c = 'Test Values';
        cr.Custom_Reporting_Name__c = 'test Name';
        return cr;        
    } 
    
    //test data for Custom Reporting object
    public static Promotion__c createPromotions(Id agreementId)
    {
        Promotion__c pr = new Promotion__c();
        pr.Agreement__c = agreementId;
        pr.Promotion_Description__c = 'Test Values';
        pr.Promotion_Name__c = 'test Name';
        return pr;        
    } 
    
    //test data for Vendor Agreement Associations
    public static Vendor_Agreement__c  createVAAssociations(id accountId, Id agreementId)
    {
        Vendor_Agreement__c va = new Vendor_Agreement__c();
        va.Account_Vendor__c = accountId;
        va.Agreement__c = agreementId;
        return va;
    }
    
    //test data for funding grid object
    public static Funding_Grid__c createFundingGrid(id accId)
    {
        Funding_Grid__c fg = new Funding_Grid__c();
        fg.Account__c = accId;
        fg.Funding_Method__c = System.Label.Check;
        fg.Default_Payment_method__c = true;
        return fg;
    }
    
    public static User createUser(String profileName)
    {
        Profile p = [SELECT Id FROM Profile WHERE Name=:profileName]; 
        UserRole ur = [SELECT Id FROM UserRole WHERE DeveloperName =: System.Label.KEF_BUSINESS_ADMINISTRATORS];
        
        User use = new User(Alias = 'HubO', Email='standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', UserRoleId = ur.Id,
                            LocaleSidKey='en_US', ProfileId = p.Id,PLOB__c='support',PLOB_Alias__c='support', 
                            TimeZoneSidKey='America/Los_Angeles', UserName='test@keybank.com'+ ''+DateTime.Now().format('MM/dd/yyyy') + dateTime.now().millisecond() + Math.random()*100);
        system.debug('UserName-->       '+use.UserName);
        system.debug('UserRoleId-->       '+use.UserRoleId);
        return use;
    }
    
    public static Opportunity createOpportunity(Id accId)
    {
        Opportunity opp = new Opportunity(
            Name ='test' + Math.Random(), AccountId = accId, CloseDate = system.today(), 
            StageName='Pursue');
          return opp;
    } 
    public static Origination_Document__c createOriginationDocument(Id agrId)
    {
        Origination_Document__c doc = new Origination_Document__c(
                                    KEF_Agreement__c = agrId, Status__c = System.Label.Active,
                                    Document_Selection_s__c = 'simple text' );
        return doc;
    } 
}