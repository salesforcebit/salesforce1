/*
This class is written for KEF KRR Project by TCS Team
It is a test class for Origination Document Object's Trigger
*/
@isTest
Private Class TestOriginationDocumentTrigger_KEF {
    static testMethod void testAgrIntStatusUpdate(){
        
        Agreement__c agr = TestUtilityClass.CreateTestAgreement();
        agr.Integrated__c = true;
       
        Origination_Document__c oriDocument;
        List<Origination_Document__c> oriDocumentList = new List<Origination_Document__c>();
        
        test.startTest();
        
        insert agr;        
        oriDocument = TestUtilityClass.createOriginationDocument(agr.id);
        oriDocumentList.add(oriDocument);
        oriDocument = TestUtilityClass.createOriginationDocument(agr.id);
        oriDocumentList.add(oriDocument);       
        insert oriDocumentList;
        delete oriDocumentList;
        
        test.stopTest();
        
        Agreement__c UpdatedAgr = [SELECT id, Integration_Status__c FROM Agreement__c WHERE id = :agr.id];
        system.debug('UpdatedAgr:'+UpdatedAgr.Integration_Status__c);
        System.assertEquals(System.Label.Not_Picked_Up, UpdatedAgr.Integration_Status__c);
        agr.Integrated__c = false;
        update agr;
    }
}