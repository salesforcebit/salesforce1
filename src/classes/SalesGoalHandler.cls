//================================================================================
//  KeyBank  
//  Object: Sales Goal
//  Author: Offshore
//  Detail: SalesGoal Trigger Handler Class
//================================================================================
//Created:  26/06/2015    -    Initial Version
//Changes: 
//================================================================================

public class SalesGoalHandler 
{
 private static boolean run = true;
 public void ownerchange(Sales_Goal__c[] sglists)
 
 {
  
  List<Sales_Goal__c> sglist = new List<Sales_Goal__c>();
  
  {
  for(Sales_Goal__c sg : sglists)
      {
        if(sg.User__c != null)
        {
            Sales_Goal__c sgl = new Sales_Goal__c();
            sgl.Id = sg.Id;
            sgl.OwnerId = sg.User__c; 
            sglist.add(sgl);
            system.debug(sgl);  
        }       
      }
       if(sglist.size()>0)
       try{
            update sglist;
            system.debug(sglist);        
          }
    catch (Exception e) {
                            System.debug('Exception is ' + e.getMessage());                            
                        } 
      
   }

 }

//To avoid Recursive trigger.
public static boolean runOnce()
 {
    if(run)
    {
     run=false;
     return true;
    }
    else {
        return run;
    }
 }  
}