public class ECCP_CRINT_CollateralController {
    public LLC_BI__Loan__c loan {get;set;}
    public List<LLC_BI__Loan_Collateral2__c> collateralList {get;set;}
    public List<Schema.FieldSetMember> collateralFieldListView {get;set;}
    public List<Schema.FieldSetMember> selectedCollateralFieldList {get;set;}
    public LLC_BI__Collateral__c selectedCollateral {get;set;}
    public String collateralId {get;set;}
    public String collateralTypeId {get;set;}
    public Boolean showList {get;set;}
    public Boolean showError {get;set;}
    public nForce.TemplateController template;
    
    public ECCP_CRINT_CollateralController(nForce.TemplateController controller) {
        this.template = controller;
        this.loan = getLoan(ApexPages.currentPage().getParameters().get(PARAMETER_NAME));
        this.showList = True;
        this.showError = False;
        this.collateralFieldListView = getCollateralFieldListView(FIELDSETLIST);
        this.collateralList = getCollateralList(loan.Id);
    }
    
    public void viewCollateral() {
    	this.showList = False;
        LLC_BI__Collateral_Type__c type = getType(this.collateralTypeId);
        
        if(type.CRINT_Field_Set__c != null) {
            this.selectedCollateralFieldList = getSelectedCollateralFieldList(type.CRINT_Field_Set__c);
            this.selectedCollateral = getSelectedCollateral(this.collateralId);
            this.showError = False;
        } else {
            this.showError = True;
        }
    }
    
    public void back() {
        this.showList = True;
        this.showError = False;
    }
    
    private List<Schema.FieldSetMember> getCollateralFieldListView(String fieldSet) {
        return SObjectType.LLC_BI__Loan_Collateral2__c.fieldSets.getMap().get(fieldSet).getFields();
    }
    
    private List<Schema.FieldSetMember> getSelectedCollateralFieldList(String fieldSet) {
        return SObjectType.LLC_BI__Collateral__c.fieldSets.getMap().get(fieldSet).getFields();
    }
    
    private LLC_BI__Loan__c getLoan(Id recordId) {
        return [SELECT
               		Id,
               		Name
               	FROM
               		LLC_BI__Loan__c
               	WHERE
               		Id = :recordId LIMIT 1];
    }
    
    private List<LLC_BI__Loan_Collateral2__c> getCollateralList(Id recordId) {
        String query = SELECTSTATEMENT;
        for(Schema.FieldSetMember f: this.collateralFieldListView) {
            query += f.getFieldPath() + COMMASPACE;
        }
        
        query += 'Id, LLC_BI__Collateral__c, LLC_BI__Collateral__r.LLC_BI__Collateral_Type__c ';
        query += 'FROM LLC_BI__Loan_Collateral2__c ';
        query += 'WHERE LLC_BI__Loan__c = \'' + recordId + '\' ';
        query += 'ORDER BY LLC_BI__Active__c DESC, LLC_BI__Collateral_Value__c DESC LIMIT 1000';
        
        return Database.query(query);
    }
    
    private LLC_BI__Collateral_Type__c getType(Id recordId) {
        return [SELECT 
                	CRINT_Field_Set__c 
                FROM 
                	LLC_BI__Collateral_Type__c 
                WHERE 
                	Id = :recordId LIMIT 1];
    }
    
    private LLC_BI__Collateral__c getSelectedCollateral(Id recordId) {
        String query = SELECTSTATEMENT;
            for(Schema.FieldSetMember f: this.selectedCollateralFieldList) {
                query += f.getFieldPath() + COMMASPACE;
            }
            
            query += 'Id FROM LLC_BI__Collateral__c WHERE Id = \'' + recordId + '\' LIMIT 1';
        
        return Database.query(query);
    }
    
    private static final String PARAMETER_NAME = 'id';
    private static final String FIELDSETLIST = 'CRINT_Collateral_List_View';
    private static final String SELECTSTATEMENT = 'SELECT ';
    private static final String COMMASPACE = ', ';
}