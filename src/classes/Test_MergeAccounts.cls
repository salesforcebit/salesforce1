@istest
Public class Test_MergeAccounts
{
    public static  testMethod void mergetest() 
    {
        Account a = new account(name='test');
        insert a; 
        Account a1 =new account(name='test1');
        insert a1;  
        
        merge a a1;
        
        if(a.masterrecordId!=null)
        {
            Account_Audit__c Aaudit = new Account_Audit__c(
            Losing_Account_Name__c = a.name,
            Losing_Owner__c = a.ownerId,
            Merge_date_time__c = system.now(),
            Merged_by_user__c = UserInfo.getName());
            insert Aaudit;
        }
      }
 }