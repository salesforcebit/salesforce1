public without sharing class OpportunityLineItemTriggerHandler {
	
	public static void handleAfterDelete(Map<Id, OpportunityLineItem> opportunityLineItems) {
		
		List<LLC_BI__Opportunity_Product__c> nCinoOpportunityProductsToDelete = new List<LLC_BI__Opportunity_Product__c>();
		
		for(OpportunityLineItem oli : opportunityLineItems.values()) {
			if(oli.Opportunity_Record_Type__c == 'Deposits & ECP' && oli.nCino_Opportunity_Product__c != null) {
				nCinoOpportunityProductsToDelete.add(new LLC_BI__Opportunity_Product__c(id = oli.nCino_Opportunity_Product__c));
			}
		}
		
		if(nCinoOpportunityProductsToDelete.size() > 0) {
			delete nCinoOpportunityProductsToDelete;
		}
	}
}