//=================================================================================================
//  KeyBank
//  Object: ext_maAccountCall
//  Author: Offshore
//  Detail: Controller (New Page) for Visualforce page - maAccountCall for mobile
//=================================================================================================
//          Date            Purpose
// Created: 04/15/2015      Initial Version
// Changes: 07/01/2015      July release - Exception handling & search filter based on RACF.
//        : 07/03/2015      Account Team member fix. 
//        : 08/28/2015      October Release - FX Requirement
//=================================================================================================

public with sharing class ext_maAccountCall {
    
    //Variables
    Account act;
    public String UserID {get; set;}
    public String searchString{get; set;}
    public Participant__c[] shoppingCart{get; set;}
    public Contact[] AvailableParticipants {get;set;}
    public String tonSelect{get; set;}
    public String tocSelect{get; set;}
    public String toatmSelect{get; set;}
    public String toUnselect{get; set;}
    private Participant__c[] forDeletion = new Participant__c[]{};
    
    //Containers
    public list<AggregateResult> prodFamilies;
    public list<AggregateResult> prodCats;    
    public set<String> pFams;
        
    //Getters and Setters
        //Information Block
        public Date callDate {get;set;}
        public String callSubject {get;set;}
        public Boolean cPrivate {get;set;}
        public String cStatus {get;set;}
        public String cType {get;set;}
        public String cComments {get;set;}
        public String cCallLocation {get;set;}
        
        //Additional Information Block
        public Boolean cJointCall {get;set;}
        public String cProductFamily {get;set;}
        public String cProductCategory {get;set;}
        public String cProduct {get;set;}
        public string warningRACF{get;set;} //Warning Racf message variable SS(6/10/15)
        
        
        
    //Account Team and Contact Containers
    public Contact[] accountContacts{get;set;}
    public AccountTeamMember[] accountTeams{get;set;}
    
    //Fix part of Account Team Member - by SS
    Set<String> userNames = new Set<String>();
    Map<Id,user> usermap =new Map<Id,user>([Select Id, RACF_Number__c from user]);
       
    //Constructor
    public ext_maAccountCall (ApexPages.StandardController stdController) {
        
        //Populate Account
        this.act = (Account)stdController.getRecord();
        
        //Distinct Product Category
        prodFamilies = [select Family from Product2 group by Family order by Family asc];

        //Populate AccountTeam and Contacts
        accountTeams = [select id, UserId, User.Isactive,AccountId, User.RACF_Number__c from AccountTeamMember where AccountId =: act.Id]; 
        accountContacts = [select id, Name,RACFID__c,Contact_Type__c,  Title, Contact.MailingCity,Contact.MailingState,Contact.Account.Name from Contact where AccountId =: act.Id]; 
        
        //Search and display in Contact section
         {shoppingCart = [Select id,name,CallId__c,Contact_Id__c,Call_Date__c,Participant__c.Contact_Id__r.Name from participant__c where id =: tonSelect ];}
 
        updateAvailableList();
        
        //Populate Call Variable Custom Setting & get warning message SS(6/12/15)
        
        CallVF__c cVFcs = CallVF__c.getInstance(keyGV.vfCallNew);
        warningRACF = cVFcs.Warning_RACFID__c;        
        
    }

    //Create Call
    public pagereference createCall(){
        return null;
    }
    
    //Populate Call Status
    public List<selectOption> cStatusList {
        get{
            List<selectOption> options = new List<selectOption>();
            options.add(new SelectOption('DONE', 'DONE' ));
            for(callSelect_Status__c c : [select status__c from callSelect_Status__c])
                options.add(new SelectOption(c.Status__c, c.Status__c));
            return options;
        }
    }
    
    //Populate Call Location - For FX Requirement Picklist 
    public List<selectOption> cCallLocationList {
        get{
            List<selectOption> options = new List<selectOption>();
            options.add(new SelectOption('', '-- Choose a Call Location --' ));
            for(callSelect_Call_Location__c cl : [select Call_Location__c from callSelect_Call_Location__c])
                options.add(new SelectOption(cl.Call_Location__c, cl.Call_Location__c));
            return options;
        }
    }
    
    //Populate Call Type
    public List<selectOption> cTypeList {
        get{
            List<selectOption> options = new List<selectOption>();
            options.add(new SelectOption('', '-- Choose a Call Type --' ));
            for(callSelect_Type__c cs : [select Type__c from callSelect_Type__c])
                options.add(new SelectOption(cs.Type__c, cs.Type__c));
            return options;
        }
    }
    
    //Populate Product Family 
    public List<selectOption> pFamilyList {
        
        //Get distinct Product Families
        get {
            List<selectOption> options = new List<selectOption>();

                options.add(new SelectOption('','--NONE--'));
                for (AggregateResult prod : prodFamilies)
                    options.add(new SelectOption(String.valueOf(prod.get('Family')),String.valueOf(prod.get('Family'))));
            return options;           
        }
        set;
    }
    
    //Populate Product Category
    public List<selectOption> pCategoryList {
        get {
            prodCats = [select Product_Category__c from Product2 where family =: cProductFamily and isactive=true group by Product_Category__c order by Product_Category__c asc];
            List<selectOption> options = new List<selectOption>();
                options.add(new SelectOption('','--NONE--'));
                for (AggregateResult pc : prodCats)
                    options.add(new SelectOption(String.valueOf(pc.get('Product_Category__c')),String.valueOf(pc.get('Product_Category__c'))));
            return options;           
        }
        set;
    }   
    
    //Populate Products
    public List<selectOption> ProductList {
        get {
            List<selectOption> options = new List<selectOption>();

                options.add(new SelectOption('','--NONE--'));
                for (Product2 p2 : [select name from Product2 where Product_Category__c =: cProductCategory and isactive=true])
                    options.add(new SelectOption(p2.name, p2.name));
            return options;           
        }
        set;
    } 
    
    
    //Offshore Shopping Cart
    public void updateAvailableList() {
      
        UserID = UserInfo.getUserId(); 
        String qString =  'select id, Name,RACFID__c,Contact_Type__c, Title, Contact.MailingCity,Contact.MailingState,Contact.Account.Name from Contact where User_Id__c not in (select ID from User where id =: UserID )' ;
        system.debug(qString);
        
        if(searchString!=null) {          
            qString+= ' and ( Contact.Name like \'%' + searchString + '%\' or Contact.RACFID__c like \'%' + searchString + '%\' or Contact.Officer_Code__c like \'%' + searchString + '%\') ';                       
        }
        
       Set<Id> selectedEntries = new Set<Id>();
       if(tonSelect!=null)
        for(Participant__c d : shoppingCart){
            selectedEntries.add(d.Contact_Id__c);
        }
        
        if(selectedEntries.size()>0){
            String tempFilter = ' and id not in (';
            for(id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        } 
        
        qString+= ' order by Name';
        qString+= ' limit 12';
        system.debug('qString:' +qString );
        if(searchString != null){               
            AvailableParticipants = database.query(qString);
            system.debug(AvailableParticipants);
        }
    } 
    
    // This function runs when a user hits "select" button next to a Participant
    public void addToShoppingCart() { 
        for(Contact part : AvailableParticipants){
            //if((String)part.id==tonSelect && part.RACFID__c == null && part.Contact_Type__c != 'Key Employee')
            if(((String)part.id==tonSelect && part.RACFID__c != null) || ((String)part.id==tonSelect && part.Contact_Type__c == 'Business Contact'))
            {
                shoppingCart.add(new Participant__c (Contact_Id__c =part.id));
                system.debug(shoppingCart);
                system.debug('Participant select size' + shoppingCart.size());
                break;
            }
            if((String)part.id==tonSelect && part.RACFID__c == null && part.Contact_Type__c == 'Key Employee')
            {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF)); 
                //ApexPages.addMessage(errMsg);
                system.debug('error loop');
            }
        }    
        updateAvailableList();  
    } 
    
    //Add AccountTeamMember as a Participant
    /*public void addParticipantAT() {
        
        //Build and Populate Contact Map
        Map<String,Contact> contactMap = new Map<String,Contact>();
            
            //Build list of RACFs
            list<String> userRACFIDs = new list<String>();
            for(AccountTeamMember cAT : accountTeams){
                userRACFIDs.add(cAT.User.RACF_Number__c);                
            }
            
            //Get Contacts based on RACFs
            //list<Contact> atContacts = [Select Id, RACFID__c from Contact where RACFID__c IN : userRACFIDs and Contact_Type__c='Key Employee' and Salesforce_User__c=true];
            list<Contact> atContacts = [Select Id, RACFID__c from Contact where RACFID__c IN : userRACFIDs and Contact_Type__c='Key Employee'];
            
            //Build Contact Map
            for(Contact atCs : atContacts){
                contactMap.put(atCs.RACFID__c, atCs);                
            }
        
        for(AccountTeamMember partsatm : accountTeams){
            
            //Get Contact from ContactMap
            Contact atC = contactMap.get(partsatm.User.RACF_Number__c);
            Participant__c atP = new Participant__c();
            system.debug(atC);
            system.debug(atC.id);
           
                atP.Contact_Id__c = atC.Id;
                
                
            //Populate Selected else throw exception based on selection criteria
            if(atC != null && partsatm.id==toatmSelect && partsatm.User.Isactive == true && atC.RACFID__c != null)
            //if(partsatm.id==toatmSelect && atC.RACFID__c != null)
            {
                shoppingCart.add(atP);
            }   
            else if(atC != null && partsatm.id==toatmSelect && partsatm.User.Isactive == true && atC.RACFID__c == null)
            //else if((String)partsatm.id==toatmSelect && atC.RACFID__c == null)
            {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF)); 
                //ApexPages.addMessage(errMsg);
                system.debug('error loop');
            }
            else if(atC != null && partsatm.id==toatmSelect && partsatm.User.Isactive == false && atC.RACFID__c != null)
            {
                system.debug('Else2 found-'  +atC );
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The Selected User is Inactive')); 
            }
            else if(partsatm.id==toatmSelect && atC == null)
            {
                system.debug('Else3 found-'  +atC );
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No KeyEmployee Contact For Selected User')); 
            } 
            else
            {
            }   
            
            updateAvailableList();
        }
    } */
    
 
  //Add AccountTeamMember as a Participant - by SS on 07/03/2015 as part of July release and fix
 public void addParticipantAT()  
    {      
    for(AccountTeamMember partsatm : accountTeams)
         {
            system.debug(partsatm.UserId);
            if(partsatm.UserId != null)
            {
                User us = usermap.get(partsatm.UserId);
                userNames.add(us.RACF_Number__c);
                
                system.debug(us.RACF_Number__c);
            }
         }
         
          if(userNames.size() > 0) {
            //List<Contact> userNameContacts = [Select Id, RACFID__c,Salesforce_User__c,User_Id__r.Isactive from Contact where RACFID__c IN : userNames and Contact_Type__c='Key Employee' and Salesforce_User__c=true];
            List<Contact> userNameContacts = [Select Id, RACFID__c,Salesforce_User__c,User_Id__r.Isactive from Contact where RACFID__c IN : userNames and Contact_Type__c='Key Employee'];
            {
                for(AccountTeamMember partsatm : accountTeams)         
            {
                         
             User us = usermap.get(partsatm.UserId);
             system.debug(partsatm.UserId);
             system.debug(partsatm.User.Isactive);
              Contact foundContact;
                 for(Contact contact : userNameContacts)
                 {
                      if(contact.RACFID__c == us.RACF_Number__c)
                      {
                            foundContact = contact;
                            system.debug('found-'  +foundContact );
                            system.debug('RACF-' +foundContact.RACFID__c);                      
                      }                      
                 }
                
                 if(foundContact != null && partsatm.id==toatmSelect && partsatm.User.Isactive == true && foundContact.RACFID__c != null)
                 {
                      system.debug('If found-'  +foundContact );
                      system.debug('RACF inside-' +foundContact.RACFID__c);
                      Participant__c  partatm = new Participant__c();
                      partatm.Contact_Id__c = foundContact.Id;
                      //partatm.CallId__c=newCall.id;
                      system.debug(partatm);
                      shoppingCart.add(partatm);                      
                 }
                 else if(foundContact != null && partsatm.id==toatmSelect && partsatm.User.Isactive == true && foundContact.RACFID__c == null)
                 {
                     system.debug('Else1 found-'  +foundContact );
                     ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF); 
                     ApexPages.addMessage(errMsg);
                 }
                 else if(foundContact != null && partsatm.id==toatmSelect && partsatm.User.Isactive == false && foundContact.RACFID__c != null)
                 {
                      system.debug('Else2 found-'  +foundContact );
                      ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The Selected User is Inactive'); 
                      ApexPages.addMessage(errMsg);   
                 }
                 else if(partsatm.id==toatmSelect && foundContact == null)
                 {
                     system.debug('Else3 found-'  +foundContact );
                     ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No KeyEmployee Contact For Selected User'); 
                     ApexPages.addMessage(errMsg);
                 } 
                 else
                 {
                 }       
        }
        
      updateAvailableList();  
           
    }
    
  }
  
 }
 
    //Add Contact to Participant List
    public void ContactList() {
        for(Contact parts : accountContacts ) {
            Participant__c cp = new Participant__c();
            cp.Contact_Id__c = parts.Id;
            
            if(((String)parts.id==tocSelect && parts.RACFID__c != null) || ((String)parts.id==tocSelect && parts.Contact_Type__c == 'Business Contact'))
            {
                shoppingCart.add(cp);
                break;
            }
            if((String)parts.id==tocSelect && parts.RACFID__c == null && parts.Contact_Type__c == 'Key Employee')
            
            {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF)); 
                //ApexPages.addMessage(errMsg);
                system.debug('error loop');
            }
                      
        }
       updateAvailableList();  
    }

    public PageReference removeFromShoppingCart(){
    
        // This function runs when a user hits "remove" on "Selected Participant" section
    
        Integer count = 0;
    
        for(Participant__c del : shoppingCart){
            if((String)del.Contact_Id__c==toUnselect){
            
                if(del.Id!=null)
                    forDeletion.add(del);
            
                shoppingCart.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }            

    public PageReference Save(){
        
        //Id Variable
        Id newCallId;
        //Get RecordType
        Id recordTypeId = keyGlobalUtilities.getRecordTypeId('Call__c', keyGV.callRTAccount);
        
        //Build Call Record
        Call__c newCall = New Call__c();
            newCall.AccountId__c = act.Id;
            newCall.RecordTypeId = recordTypeId;
            newCall.Subject__c = callSubject;
            newCall.Call_Date__c = callDate;
            newCall.Is_Private__c = cPrivate;
            newCall.Status__c = cStatus;
            newCall.Call_Type__c = cType;
            newCall.Joint_Call__c = cJointCall;
            newCall.Call_Location__c = cCallLocation;
            
            if(!String.isBlank(cComments)){
                newCall.Comments__c = cComments;
            }
            if(!String.isBlank(cProductFamily)){
                newCall.Product_Family__c = cProductFamily;
            }
            if(!String.isBlank(cProductCategory)){
                newCall.Product_Category__c = cProductCategory;
            }
            if(!String.isBlank(cProduct)){
                newCall.Product__c = cProduct;
            }       
                                        
        //Insert Call
        try{
            insert newCall;
            newCallId = newCall.Id;
        }   
        catch (Exception ex) {
            ApexPages.addMessages(ex);
            System.debug('Error inserting Call Record - '+ex.getMessage());
            return null;
        }

        
        //Build Participants Record
        list<Participant__c> newParticipants = new list<Participant__c>();
        if(shoppingCart.size()>0){
            for(Participant__c ppp : shoppingCart){
                Participant__c nPartic = new Participant__c();
                    nPartic.CallId__c = newCallId;
                    nPartic.Contact_Id__c = ppp.Contact_Id__c;
                newParticipants.add(nPartic);
            }
        }
                                
        //Save Participants
        if(newParticipants.size()>0){
            try{
                insert newParticipants;
            }
            catch(Exception ex) {
                ApexPages.addMessages(ex);
                System.debug('Error inserting Participant Record - '+ex.getMessage());
                return null;
            }
                
        }

        //Redirect to New Call
        return new PageReference('/' + newCallId);  
    } 
}