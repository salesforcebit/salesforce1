/** This class generates all the 6 types of GTM-KTT requests from the Treasury Services page in nCino.
 * West Monroe Partners, June 2015
 */
public with sharing class TROGTMFeedSetupError {

    /** Global variables */
    ApexPages.StandardController stdController;
    public String redirectUrl {public get; private set;}
    String tsStage;
    public Boolean check {get; set;}
    String response;



    public LLC_BI__Treasury_Service__c service {get; set;}
    public String errMsg {get; set;}
    public LLC_BI__Treasury_Service__c ts {get;set;}

    /** Constructor - initializes all the objects needed to construct the requests */
    public TROGTMFeedSetupError(ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()) {
            stdcontroller.addFields(new string[]{
               'LLC_BI__Stage__c'
            });
        }
        this.stdController = stdController;
        this.ts = (LLC_BI__Treasury_Service__c)stdController.getRecord();
        System.debug('treasury service record in '+ts);
        check = false;        
        service = [SELECT Id, LLC_BI__Stage__c,Owner.Name, Special_GTM_instructions__c, GTM_Request_Timeout__c, GTM_Incomplete_Error__c, GTM_Data_Error__c, Ship_to_Name__c, Ship_to_Phone_Number__c, Intraday_ACH_Options__c, GTM_Revision_Options__c, GTM_Request_Type__c, OwnerId, Document_Reference_Number__c, User_Name__c, LLC_BI__Relationship__c, Account_Name__c, Delivery_Method__c, Account_type__c, Bank_Number__c, Customer_Statement_Service_Options__c, View_Intraday_Customer_Statement__c, Information_Reporting_Service_Options__c, Previous_Day_Debit__c, Previous_Day_Summary_and_Detail_Options__c, Previous_Day_Credit__c, Previous_Day_Viewing_Options__c, Previous_Day_Via__c, First_Name__c, Last_Name__c, Email_Address__c, Phone__c, Company_Authentication_Type__c, User_ID__c, Billing_Account_Number__c, RACFID__c,   Billing_Account_Bank__c, Requested_By__c, Revision_Options__c, Company__c, Request_Type__c FROM LLC_BI__Treasury_Service__c WHERE Id =: ts.Id];
        System.debug('service.LLC_BI__Stage__c '+service.LLC_BI__Stage__c);
        String tsStage = service.LLC_BI__Stage__c;
        if(tsStage.equals('Fulfillment')){
            // do nothing
        }
        else {
            check = true;
            System.debug('check is true');
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Stage has to be Fulfillment to send a GTM Request.');
            //ApexPages.addMessage(myMsg);
            errMsg = 'Treasury Service stage has to be in Fulfillment to send a GTM Request';
        }
    }

}