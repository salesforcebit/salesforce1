@isTest
public class ECCP_MCATriggerHandlerTest{

public static TestMethod void createData(){
list<LLC_BI__Loan__c> lnList = new list<LLC_BI__Loan__c>();
list<LLC_BI__Loan__c> LoanList1 = new list<LLC_BI__Loan__c>();

Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
User u = new User(Alias = 'stdKey1', Email='standarduser1@testorg.comKey', 
            EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg.comKey',PLOB__c='Support');
   insert u;
   System.runAs(u){
   
        WebServiceStub__c stubObj= new WebServiceStub__c();
        stubObj.Response__c = '{"Proceed" : "Proceed", "Pass" : "Pass"}';
        stubObj.Name='ECCP_MCAObServiceStub';
        insert stubObj;
        ECCP_MCACheckRequestData req = new ECCP_MCACheckRequestData();
        ECCP_WebserviceStub.ECCP_MCACheck(req); 
     

lnlist = ECCP_CreateLoanUtil.CreateLoanData(3,4); 

         system.debug('----lnList---- '+lnlist );
         //added for performance log
         ECCP_LogSetting__c plog = new ECCP_LogSetting__c();
         plog.name = 'PerformanceExceptionLogEnabled';
         plog.isEnable__c = True;
         insert plog;
         
         ECCP_LogSetting__c plog1 = new ECCP_LogSetting__c();
         plog1.name = 'SaveRequest_Response';
         plog1.isEnable__c = True;
         insert plog1;
         
         /*ECCP_Compliance__c comp = new ECCP_Compliance__c();
         comp.ECCP_Entity_Involvement__c = lnlist[0].ECCP_Entity_Involvement__c;
         comp.ECCP_MCA_Service_Succes__c = True;
         comp.MCAStatus__c = 'Pass';
        
         
         insert comp;
         */
         
         Test.startTest();
        
         for(LLC_BI__Loan__c ln : lnlist){
         ln.ECCP_MCA_Check__c =True;
         ln.ECC_IntegrateToLPL__c = True;
         ln.ECCP_Credit_Bureau_ID__c ='123456';
         LoanList1.add(ln);
         
         }
         update LoanList1;
         Test.stopTest();
         }
     }}