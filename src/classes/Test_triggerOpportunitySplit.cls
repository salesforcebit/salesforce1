@istest
Public class Test_triggerOpportunitySplit
{
public static testMethod void privatecheck()
{
List<User> ulist=new List<User>();
Profile p=[select id from profile where name='KEF Business Developer'];
User testUser = new User(alias = 'u1', email='vidhya11sagarantest123@keybank.com',
                emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, country='United States',
                timezonesidkey='America/Los_Angeles', username='vidhya11sagaran1_muralidharanty@keybank.com',PLOB__c='support',PLOB_Alias__c='KBBC');             
ulist.add(testUser);        
User testUser1 = new User(alias = 'u2', email='indu123test123@keybank.com',
                 emailencodingkey='UTF-8', lastname='Testing2016', languagelocalekey='en_US',
                 localesidkey='en_US', profileid = p.Id, country='United States',
                 timezonesidkey='America/Los_Angeles', username='indu1_11venkatakrishnan@keybank.com',PLOB__c='support');             
ulist.add(testUser1);
insert ulist; 

recordtype rt3=[select Id, name from recordtype where name='KEF Opportunity'];

Account acc = new account (name='Test');
insert acc;

Program__c p1=new Program__c(name_of_the_vendor_program__c='test',primary_business_developer__c=testUser1.id,program_status__c='Prospect');
insert p1; 
        
Agreement__c kefa = new Agreement__c(Business_Developer_Name__c=testUser1.id,program__c=p1.id,Agreement_Name__c='Test');
insert kefa; 

Opportunity o=new Opportunity(Name='Test', volume__c = 12589, AccountId = acc.id, closedate=system.today(), StageName='Pursue', recordtypeid=rt3.id,Vendor_agreement__c=kefa.id);
insert o;

OpportunitySplitType ost = [Select ID, Description from OpportunitySplitType where Description =: 'Volume'];
OpportunityTeamMember otm=new OpportunityTeamMember(OpportunityId=o.id,UserId=testUser1.id);
insert otm;
OpportunityTeamMember otm1=new OpportunityTeamMember(OpportunityId=o.id,UserId=testUser.id);
insert otm1;
List<OpportunitySplit> oslist=new List<OpportunitySplit>();
OpportunitySplit os=new OpportunitySplit(OpportunityId=o.id,SplitOwnerId=testUser1.id,SplitPercentage=50,SplitTypeId=ost.id);
//oslist.add(os);
insert os;
/*OpportunitySplit os1=new OpportunitySplit(OpportunityId=o.id,SplitOwnerId=testUser.id,SplitPercentage=50,SplitTypeId=ost.id);
oslist.add(os1);
insert oslist;*/
os.primary__c=true;
update os;    
  
}
}