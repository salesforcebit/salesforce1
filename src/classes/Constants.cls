public class Constants {

	public static final String treasuryServiceTypeNew = 'New';
	public static final String treasuryServiceTypeRevise = 'Revise - Add/Change/Delete';
	public static final String treasuryServiceTypeCancel = 'Cancellation';

	public static final String treasuryServiceStageOrderEntry = 'Order Entry';
	public static final String treasuryServiceStagePendingPrerequisites = 'Pending - Prerequisites';
	public static final String treasuryServiceStagePendingFutureStartDate = 'Pending - Future Start Date';
	public static final String treasurtServiceStagePendingOperationsAcceptance = 'Pending – Operations Acceptance';
	public static final String treasuryServiceStagePendingTaskOnHold = 'Pending - Task on Hold';
	public static final String treasuryServiceStageFulfillment = 'Fulfillment';
	public static final String treasuryServiceStageTesting = 'Testing';
	public static final String treasuryServiceStageTraining = 'Training';
	public static final String treasuryServiceStageActiveService = 'Active Service';

	public static final String TSOSetupTaskStatusNew = 'New';
	public static final String TSOSetupTaskStatusOnHold = 'On Hold';
	public static final String TSOSetupTaskStatusComplete = 'Complete';

	public static final String tsoSetupTaskTemplateAssignmentClientOnboardingManager = 'Client Onboarding Manager';
	public static final String tsoSetupTaskTemplateAssignmentClientManager = 'Client Manager';
	public static final String tsoSetupTaskTemplateAssignmentQueue = 'Queue';

	public static final String ErrorGanttNoStages = 'One or more of your treasury services is missing required information. Please validate that the Request Type and Product fields are populated on the treasury services related to this Product Package. If you continue to encounter an error, please reach out to your administrator for further assistance.';

	public static BusinessHours defaultBusinessHours {get {
		if(defaultBusinessHours == null) {
			defaultBusinessHours = [Select Id from BusinessHours Where isDefault = true];
		}
		return defaultBusinessHours;
	}set;}

	/*public static EmailTemplate tsoPlannedImplementationStartDateDelayTemplate {get {
		if(tsoPlannedImplementationStartDateDelayTemplate == null) {
			tsoPlannedImplementationStartDateDelayTemplate = [Select Id from EmailTemplate Where DeveloperName = 'TSO_Planned_Implementation_Start_Date_Delay'];
		}
		return tsoPlannedImplementationStartDateDelayTemplate;
	}set;}*/

	public static List<Treasury_Service_Stage_Order__mdt> treasuryServiceStageOrders {get {
		if(treasuryServiceStageOrders == null) {
			treasuryServiceStageOrders = [Select Id, Current_Stage__c, Next_Stage__c, Stage_Order__c from Treasury_Service_Stage_Order__mdt];
		}
		return treasuryServiceStageOrders;
	}set;}

	/*public static Map<String, String> nextStageMap {get {
		if(nextStageMap == null) {
			nextStageMap = new Map<String, String>();
			for(Treasury_Service_Stage_Order__mdt stageOrder : treasuryServiceStageOrders) {
				nextStageMap.put(stageOrder.Current_Stage__c, stageOrder.Next_Stage__c);
			}
		}
		return nextStageMap;
	}set;}*/

	public static Map<String, Decimal> stageOrderMap {get {
		if(stageOrderMap == null) {
			stageOrdermap = new Map<String, Decimal>();
			for(Treasury_Service_Stage_Order__mdt stageOrder : treasuryServiceStageOrders) {
				stageOrderMap.put(stageOrder.Current_Stage__c, stageOrder.Stage_Order__c);
			}
		}
		return stageOrderMap;
	}set;}

	    /****** Encrpytion Key  ******************/
  public static final String ENCRYPTION_KEY = 'urwdrsZZ3/8G/nTXAPFd9PSn0Q02MSz3Zbht5gi5+Ig=';


}