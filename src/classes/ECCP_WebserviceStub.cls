public with sharing class ECCP_WebserviceStub {
/*******************************************************************************************************************************************
* @name         :   ECCP_WebserviceStub
* @description  :   This method is to generate the stub response .
* @author       :   Nagarjun
* @createddate  :   18/03/2016
*******************************************************************************************************************************************/

    
    //Added by ashish for MCA outbound on 23/03/2016
    public static ECCP_MCA_CheckMCI_Response ECCP_MCACheck(ECCP_MCACheckRequestData reqObj){
        String jSonString;
        //change this
        WebServiceStub__c stubObj=[select Response__c from WebServiceStub__c where Name='ECCP_MCAObServiceStub'];
        ECCP_MCA_CheckMCI_Response resObj = JSONtoObjectMCACheckMCIcgetResponse(stubObj.Response__c); 
        return resObj;        
     }
    public static ECCP_MCA_CheckMCI_Response JSONtoObjectMCACheckMCIcgetResponse(String jSonString){
        ECCP_MCA_CheckMCI_Response resObj =(ECCP_MCA_CheckMCI_Response ) JSON.deserialize(jSonString, ECCP_MCA_CheckMCI_Response.class);
        return resObj;
    }     
}