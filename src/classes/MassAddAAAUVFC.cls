public with sharing class MassAddAAAUVFC {

    /*
        General
    */
    Set<Id> Realationshipaccids = new Set<Id>();
    public Id treasuryServiceId {get;set;}
    public LLC_BI__Treasury_Service__c treasuryService {get {

        if(treasuryService == null) {

            treasuryService = [Select Id, LLC_BI__Relationship__c, LLC_BI__Product_Reference__c, LLC_BI__Product_Reference__r.Transfer_Limits_Required__c from LLC_BI__Treasury_Service__c Where Id =: treasuryServiceId];
            if(treasuryService.LLC_BI__Relationship__c!=null){
                Realationshipaccids.add(treasuryService.LLC_BI__Relationship__c);
            }
        }

        return treasuryService;
    }set;}

    public MassAddAAAUVFC(ApexPages.standardController std) {
        treasuryServiceId = std.getId();
        showContactAndDepositSelection = true;
    }

    public Boolean showContactAndDepositSelection {get;set;}
    public Boolean showContactCreation {get;set;}
    public Boolean showDepositCreation {get;set;}
    public Boolean showDefaultServiceOptions {get;set;}
    public Boolean showServiceOptions {get;set;}
    public Boolean showUsersAndAccounts {get;set;}
    /*
        Add Authorized Users
    */

    public Map<Id, Contact> contacts {get {

        if(contacts == null) {
            contacts = new Map<Id, Contact>([Select Id, Name from Contact Where AccountId =: treasuryService.LLC_BI__Relationship__c]);
        }

        return contacts;
    }set;}

    public set<String> selectedUserIds {get {if(selectedUserIds == null) selectedUserIds = new Set<String>(); return selectedUserIds;}set;}

    public List<String> availableUsersSelected {get;set;}
    public List<String> selectedUsersSelected {get;set;}

    public List<SelectOption> availableUserOptions {get {
        if(availableUserOptions == null) {
            availableUserOptions = new List<SelectOption>();
            for(Contact c : contacts.values()) {
                if(!selectedUserIds.contains(c.Id))
                    availableUserOptions.add(new SelectOption(c.Id, c.Name));
            }
        }

        if(availableUserOptions.size() == 0) {
            availableUserOptions.add(new SelectOption('', '--None--'));
        }
        return availableUserOptions;
    }set;}

    public List<SelectOption> selectedUserOptions {get {
        if(selectedUserOptions == null) {
            selectedUserOptions = new List<SelectOption>();
            for(Contact c : contacts.values()) {
                if(selectedUserIds.contains(c.Id))
                    selectedUserOptions.add(new SelectOption(c.Id, c.Name));
            }
        }

        if(selectedUserOptions.size() == 0) {
            selectedUserOptions.add(new SelectOption('', '--None--'));
        }
        return selectedUserOptions;
    }set;}

    public PageReference addSelectedAvailableUsers() {

        selectedUserIds.addAll(availableUsersSelected);
        refreshSelectedUsers();
        return null;
    }

    public PageReference removeSelectedSelectedUsers() {

        selectedUserIds.removeAll(selectedUsersSelected);
        refreshSelectedUsers();
        return null;
    }

    public PageReference addAllAvailableUsers() {

        for(SelectOption so : availableUserOptions) {
            selectedUserIds.add(so.getValue());
        }
        refreshSelectedUsers();
        return null;
    }

    public PageReference removeAllAvailableUsers() {

        selectedUserIds = null;
        refreshSelectedUsers();
        return null;
    }

    private void refreshSelectedUsers() {
        availableUserOptions = null;
        selectedUserOptions = null;
    }

    /*
        Add Analyzed Accounts
    */

    public Map<Id, LLC_BI__Deposit__c> deposits {get {

        if(deposits == null) {
            deposits = new Map<Id, LLC_BI__Deposit__c>([Select Id, Name, LLC_BI__Account_Number__c ,Bank_Number2__c from LLC_BI__Deposit__c Where LLC_BI__Account__c =: treasuryService.LLC_BI__Relationship__c]);
        }

        return deposits;
    }set;}

    public set<String> selectedDepositIds {get {if(selectedDepositIds == null) selectedDepositIds = new Set<String>(); return selectedDepositIds;}set;}

    public List<String> availableDepositsSelected {get;set;}
    public List<String> selectedDepositsSelected {get;set;}

    public List<SelectOption> availableDepositOptions {get {
        if(availableDepositOptions == null) {
            availableDepositOptions = new List<SelectOption>();
            for(LLC_BI__Deposit__c d : deposits.values()) {
                if(!selectedDepositIds.contains(d.Id))
                    availableDepositOptions.add(new SelectOption(d.Id, d.LLC_BI__Account_Number__c));
            }
        }

        if(availableDepositOptions.size() == 0) {
            availableDepositOptions.add(new SelectOption('', '--None--'));
        }
        return availableDepositOptions;
    }set;}

    public List<SelectOption> selectedDepositOptions {get {
        if(selectedDepositOptions == null) {
            selectedDepositOptions = new List<SelectOption>();
            for(LLC_BI__Deposit__c d : deposits.values()) {
                if(selectedDepositIds.contains(d.Id))
                    selectedDepositOptions.add(new SelectOption(d.Id, d.LLC_BI__Account_Number__c));
            }
        }

        if(selectedDepositOptions.size() == 0) {
            selectedDepositOptions.add(new SelectOption('', '--None--'));
        }
        return selectedDepositOptions;
    }set;}

    public PageReference addSelectedAvailableDeposits() {
        selectedDepositIds.addAll(availableDepositsSelected);
        refreshSelectedDeposits();
        return null;
    }

    public PageReference removeSelectedSelectedDeposits() {

        selectedDepositIds.removeAll(selectedDepositsSelected);
        refreshSelectedDeposits();
        return null;
    }

    public PageReference addAllAvailableDeposits() {

        for(SelectOption so : availableDepositOptions) {
            selectedDepositIds.add(so.getValue());
        }
        refreshSelectedDeposits();
        return null;
    }

    public PageReference removeAllAvailableDeposits() {

        selectedDepositIds = null;
        refreshSelectedDeposits();
        return null;
    }

    private void refreshSelectedDeposits() {
        availableDepositOptions = null;
        selectedDepositOptions = null;
    }

    /*
        Create New Contact
    */

    public Contact newContact {get {if(newContact == null) newContact = new Contact(AccountId = treasuryService.LLC_BI__Relationship__c); return newContact;}set;}

    public PageReference createNewContact() {

        showContactAndDepositSelection = false;
        showContactCreation = true;
        showDepositCreation = false;
        newContact = null;
        return null;
    }

    public PageReference cancelCreateNewContact() {
        showContactAndDepositSelection = true;
        showContactCreation = false;
        newContact = null;
        return null;
    }

    public PageReference saveNewContact() {

        try {
            insert newContact;

            contacts = null;
            selectedUserIds.add(newContact.Id);
            refreshSelectedUsers();
            newContact = null;
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
        }

        return cancelCreateNewContact();
    }

    /*
        Create New Deposit
    */

    public LLC_BI__Deposit__c newDeposit {get {if(newDeposit == null) newDeposit = new LLC_BI__Deposit__c(LLC_BI__Account__c = treasuryService.LLC_BI__Relationship__c); return newDeposit;}set;}

    public PageReference createNewDeposit() {
        showContactAndDepositSelection = false;
        showDepositCreation = true;
        showContactCreation = false;
        newDeposit = null;
        return null;
    }

    public PageReference cancelCreateNewDeposit() {
        showContactAndDepositSelection = true;
        showDepositCreation = false;
        newDeposit = null;
        return null;
    }
    public string v {get;set;} 
    
    public PageReference saveNewDeposit() {     
            
            try { 
            
                 v = newDeposit.LLC_BI__Account_Number__c;
                List<LLC_BI__Deposit__c> dpp = [SELECT id,Account_Number__c From LLC_BI__Deposit__c where LLC_BI__Deposit__c.LLC_BI__Account_Number__c= :v and LLC_BI__Account__c in:Realationshipaccids ];   
                system.debug('dp.size()'+dpp.size());
                if(dpp.size() > 0){
                     System.debug('value already availble '); 
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Account Number already available'));   
                }else{
                system.debug('dpp'+newDeposit);    
                insert newDeposit;  
                }
          
               
                deposits = null;
                selectedDepositIds.add(newDeposit.Id);
                refreshSelectedDeposits();
                newDeposit = null; 
               
            } catch(Exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Hi'+e.getMessage()));
            }
        
          
       
       return cancelCreateNewDeposit();
    }

    /*
        Select Default Service Options
    */

    public PageReference continueToDefaultServiceOptions() {
    
        showServiceOptions = false;
    showDefaultServiceOptions = true;
    showContactAndDepositSelection = false;
    showContactCreation = false;
    showDepositCreation = false;
    showUsersAndAccounts = false;
        return null;
    }

    public LLC_BI__Authorized_User__c authorizedUserDefaults {get {
        if(authorizedUserDefaults == null) {
            authorizedUserDefaults = new LLC_BI__Authorized_User__c();
        }
        return authorizedUserDefaults;
    }set;}

    public LLC_BI__Analyzed_Account__c analyzedAccountServiceOptions {get {
        if(analyzedAccountServiceOptions == null) {
            analyzedAccountServiceOptions = new LLC_BI__Analyzed_Account__c();
        }
        return analyzedAccountServiceOptions;
    }set;}

  public List<String> defaultAccountAPINames {get {
    if(defaultAccountAPINames == null) {
      if(analyzedAccountServiceOptions.Service_Options__c != null) {
        Set<String> selectedServiceOptions = new Set<String>();
        for(String serviceOption : analyzedAccountServiceOptions.Service_Options__c.split(';')) {
          selectedServiceOptions.add(serviceOption);
        }
        defaultAccountAPINames = serviceOptionFields(new List<String>(selectedServiceOptions));
      }
      else return new List<String>();
    }
    return defaultAccountAPINames;
  }set;}

  public void updateDefaultAccountServiceOptions() {
    Map<String, Object> fieldValues = new Map<String, Object>();
    //Save old values and clear all variables when the service options selected change
    for(String fieldName : defaultAccountAPINames) {
      fieldValues.put(fieldName, analyzedAccountServiceOptions.get(fieldName));
      analyzedAccountServiceOptions.put(fieldName, null);
    }
    //Recalculate field names based on new service options
    defaultAccountAPINames = null;
    //Replace field values from previously existing values
    for(String fieldName : defaultAccountAPINames) {
      analyzedAccountServiceOptions.put(fieldName, fieldValues.get(fieldName));
    }
  }

  public List<String> defaultUserAPINames {get {
    if(defaultUserAPINames == null) {
      if(authorizedUserDefaults.Service_Options__c != null) {
        Set<String> selectedServiceOptions = new Set<String>();
        for(String serviceOption : authorizedUserDefaults.Service_Options__c.split(';')) {
          selectedServiceOptions.add(serviceOption);
        }
        defaultUserAPINames = serviceOptionFields(new List<String>(selectedServiceOptions));
      }
      else return new List<String>();
    }
    return defaultUserAPINames;
  }set;}

  public void updateDefaultUserServiceOptions() {
    Map<String, Object> fieldValues = new Map<String, Object>();
    //Save old values and clear all variables when the service options selected change
    for(String fieldName : defaultUserAPINames) {
       System.debug('updateDefaultUserServiceOptions'+fieldName+authorizedUserDefaults.get(fieldName)); 
      fieldValues.put(fieldName, authorizedUserDefaults.get(fieldName));
      authorizedUserDefaults.put(fieldName, null);
    }
    //Recalculate field names based on new service options
    defaultUserAPINames = null;
    //Replace field values from previously existing values
    for(String fieldName : defaultUserAPINames) {
      authorizedUserDefaults.put(fieldName, fieldValues.get(fieldName));
    }
  }

  public boolean defaultOptionsValid() {
    Boolean valid = false;
    SavePoint sp = Database.setSavePoint();

    try {
      if(selectedUserIds.size() > 0) {
        authorizedUserDefaults.Id = null;
        List<String> uIds = new List<String> (selectedUserIds);
        authorizedUserDefaults.User_has_unlimited_transfer_limit__c = true; //Bypass this validation rule as the field will be populated at a later stage
        authorizedUserDefaults.LLC_BI__Contact_Reference__c = uIds[0]; //Required field
        insert authorizedUserDefaults;
      }
      if(selectedDepositIds.size() > 0) {
        analyzedAccountServiceOptions.Id = null;
        insert analyzedAccountServiceOptions;
      }
      valid = true;
    }
    catch(Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
    }

    Database.rollback(sp);
    return valid;
  }

    /*public List<ServiceOptionWrapper> serviceOptionValues {get {

        if(serviceOptionValues == null) {
            serviceOptionValues = new List<ServiceOptionWrapper>();

            for(Schema.PicklistEntry entry : LLC_BI__Analyzed_Account__c.Service.Service_Options__c.getDescribe().getPicklistValues()) {
                serviceOptionValues.add(new ServiceOptionWrapper(entry.getLabel(), false));
            }
        }
        return serviceOptionValues;
    }set;}*/

    /*
        Select Service Options By 
    */

    public PageReference continueToServiceOptions() {

      if(defaultOptionsValid() == true) {

            showServiceOptions = true;
            showDefaultServiceOptions = false;
            showContactAndDepositSelection = false;
            showContactCreation = false;
            showDepositCreation = false;
          showUsersAndAccounts = false;
      }
        return null;
    }

    public List<ServiceOptionWrapper> defaultAuthorizedUserServiceOptions {get {
        if(defaultAuthorizedUserServiceOptions == null) {
            defaultAuthorizedUserServiceOptions = new List<ServiceOptionWrapper>();

            if(authorizedUserDefaults.Service_Options__c != null) {
                for(String option : authorizedUserDefaults.Service_Options__c.split(';')) {
                    defaultAuthorizedUserServiceOptions.add(new ServiceOptionWrapper(option, true));
                }
            }
        }

        return defaultAuthorizedUserServiceOptions;
    }set;}

    public List<ServiceOptionWrapper> defaultAnalyzedAccountServiceOptions {get {
        if(defaultAnalyzedAccountServiceOptions == null) {
            defaultAnalyzedAccountServiceOptions = new List<ServiceOptionWrapper>();

            if(analyzedAccountServiceOptions.Service_Options__c != null) {
                for(String option : analyzedAccountServiceOptions.Service_Options__c.split(';')) {
                    defaultAnalyzedAccountServiceOptions.add(new ServiceOptionWrapper(option, true));
                }
            }
        }

        return defaultAnalyzedAccountServiceOptions;
    }set;}

    public class ServiceOptionWrapper {
        public Boolean isChecked {get;set;}
        public String optionName {get;set;}

        public ServiceOptionWrapper(String option, Boolean defaulted) {
            optionName = option;
            isChecked = defaulted;
        }   
    }

    public List<AuthorizedUserWrapper> authorizedUserWrappers {get {
        if(authorizedUserWrappers == null) {
            authorizedUserWrappers = new List<AuthorizedUserWrapper>();

            for(String contactId : selectedUserIds) {
                authorizedUserWrappers.add(new AuthorizedUserWrapper(contacts.get(contactId), treasuryServiceId, defaultAuthorizedUserServiceOptions, authorizedUserDefaults, defaultUserAPINames));
            }
        }

        return authorizedUserWrappers;
    }set;}

    public class AuthorizedUserWrapper {
        public LLC_BI__Authorized_User__c authorizedUser {get;set;}
        public String contactName {get;set;}
        public List<ServiceOptionWrapper> serviceOptions {get;set;}
        //Added as part of the defect.no 773
        Public Boolean showTransferLimitwrapper {get;set;}
      public List<String> apiFieldNames {get {
        if(apiFieldNames == null) {
          apiFieldNames = serviceOptionFields(serviceOptions);
        }
        return apiFieldNames;
      }set;}

        public AuthorizedUserWrapper(Contact c, Id treasuryServiceId, List<ServiceOptionWrapper> serviceOptionsAvailable, LLC_BI__Authorized_User__c defaultUser, List<String> defaultAPINames){
            authorizedUser = new LLC_BI__Authorized_User__c();
            authorizedUser.LLC_BI__Treasury_Service__c = treasuryServiceId;
            authorizedUser.LLC_BI__Contact_Reference__c = c.Id;
            contactName = c.Name;
            serviceOptions = new List<ServiceOptionWrapper>();
            //Added as part of the defect.no 773
            showTransferLimitwrapper  = false; 
            for(ServiceOptionWrapper wrapper : serviceOptionsAvailable) {
                serviceOptions.add(new ServiceOptionWrapper(wrapper.optionName, wrapper.isChecked));
                //Added as part of the defect.no 773
                if(wrapper.optionName != null && (wrapper.optionName.equals(System.label.Book_Transfer_From) || wrapper.optionName.equals(System.label.Book_Transfer_To))){
                    showTransferLimitwrapper = true;
                }
                //End
            }
        for(String defaultAPIName : defaultAPINames) {
          authorizedUser.put(defaultAPIName, defaultUser.get(defaultAPIName));
        }
        }

      public void updateServiceOptions() {
        Map<String, Object> fieldValues = new Map<String, Object>();
        //Save old values and clear all variables when the service options selected change
        for(String fieldName : apiFieldNames) {
          System.debug('updateServiceOptions'+fieldName+authorizedUser.get(fieldName));  
          fieldValues.put(fieldName, authorizedUser.get(fieldName));
          authorizedUser.put(fieldName, null);
        }
        //Recalculate field names based on new service options
        apiFieldNames = null;
        //Replace field values from previously existing values
        for(String fieldName : apiFieldNames) {
          authorizedUser.put(fieldName, fieldValues.get(fieldName));
        }
      }
    }

    public List<AnalyzedAccountWrapper> analyzedAccountWrappers {get {
        if(analyzedAccountWrappers == null) {
            analyzedAccountWrappers = new List<AnalyzedAccountWrapper>();

            for(String depositId : selectedDepositIds) {
                analyzedAccountWrappers.add(new AnalyzedAccountWrapper(deposits.get(depositId), treasuryServiceId, defaultAnalyzedAccountServiceOptions, analyzedAccountServiceOptions, defaultAccountAPINames));
            }
        }
        return analyzedAccountWrappers;
    }set;}

    public class AnalyzedAccountWrapper {
        public LLC_BI__Analyzed_Account__c analyzedAccount {get;set;}
        public String depositName {get;set;}
        public String depositAccountNumber {get;set;}
        public String depositBankNumber {get;set;}
        public List<ServiceOptionWrapper> serviceOptions {get;set;}
      public List<String> apiFieldNames {get {
        if(apiFieldNames == null) {
          apiFieldNames = serviceOptionFields(serviceOptions);
        }
        return apiFieldNames;
      }set;}

        public AnalyzedAccountWrapper(LLC_BI__Deposit__c d, Id treasuryServiceId, List<ServiceOptionWrapper> serviceOptionsAvailable, LLC_BI__Analyzed_Account__c defaultAccount, List<String> defaultAPINames) {
            analyzedAccount = new LLC_BI__Analyzed_Account__c();
            analyzedAccount.LLC_BI__Treasury_Service__c = treasuryServiceId;
            analyzedAccount.LLC_BI__Deposit_Account__c = d.Id;
            depositName = d.Name;
            depositAccountNumber = d.LLC_BI__Account_Number__c;
            depositBankNumber = d.Bank_Number2__c;
            serviceOptions = new List<ServiceOptionWrapper>();

            for(ServiceOptionWrapper wrapper : serviceOptionsAvailable) {
                serviceOptions.add(new ServiceOptionWrapper(wrapper.optionName, wrapper.isChecked));
            }

        for(String defaultAPIName : defaultAPINames) {
          analyzedAccount.put(defaultAPIName, defaultAccount.get(defaultAPIName));
        }
        }

      public void updateServiceOptions() {
        Map<String, Object> fieldValues = new Map<String, Object>();
        //Save old values and clear all variables when the service options selected change
        for(String fieldName : apiFieldNames) {
          fieldValues.put(fieldName, analyzedAccount.get(fieldName));
          analyzedAccount.put(fieldName, null);
        }
        //Recalculate field names based on new service options
        apiFieldNames = null;
        //Replace field values from previously existing values
        for(String fieldName : apiFieldNames) {
          analyzedAccount.put(fieldName, fieldValues.get(fieldName));
        }
      }
    }

    public void updateAccountServiceOptions() {
      for(AnalyzedAccountWrapper aaw : analyzedAccountWrappers) {
        aaw.updateServiceOptions();
      }
    }

    public void updateUserServiceOptions() {
      for(AuthorizedUserWrapper auw : authorizedUserWrappers) {
        auw.updateServiceOptions();
      }
    }

    public static List<String> serviceOptionFields(List<ServiceOptionWrapper> serviceOptions) {
      Set<String> apiFieldNames = new Set<String>();
      if(serviceOptions == null) return new List<String> (apiFieldNames);

      Set<String> selectedServiceOptions = new Set<String>();
      for(ServiceOptionWrapper serviceOption : serviceOptions) {
        if(serviceOption.isChecked == true) {
          selectedServiceOptions.add(serviceOption.optionName);
        }
      }

      for(Mass_Add_Accounts__mdt serviceOptionMeta : serviceOptionMetadata) {
        if(selectedServiceOptions.contains(serviceOptionMeta.Service_Option_Value__c)) {
          apiFieldNames.add(serviceOptionMeta.Field_to_Display__c);
        }
      }

      return new List<String>(apiFieldNames);
    }

    public static List<String> serviceOptionFields(List<String> serviceOptions) {
      Set<String> apiFieldNames = new Set<String>();
      if(serviceOptions == null) return new List<String> (apiFieldNames);

      Set<String> selectedServiceOptions = new Set<String>(serviceOptions);

      for(Mass_Add_Accounts__mdt serviceOptionMeta : serviceOptionMetadata) {
        if(selectedServiceOptions.contains(serviceOptionMeta.Service_Option_Value__c)) {
          apiFieldNames.add(serviceOptionMeta.Field_to_Display__c);
        }
      }

      return new List<String>(apiFieldNames);
    }

    public static List<Mass_Add_Accounts__mdt> serviceOptionMetadata {get {
      if(serviceOptionMetadata == null) {
        serviceOptionMetadata = [Select Service_Option_Value__c, Field_to_Display__c from Mass_Add_Accounts__mdt];
      }
      return serviceOptionMetadata;
    }set;}

    public boolean transferLimitsPopulated() {
      Boolean allPopulated = true;
     /* for(AuthorizedUserWrapper auw : authorizedUserWrappers) {
        if(auw.authorizedUser.User_has_unlimited_transfer_limit__c == false && auw.authorizedUser.Transfer_Limit__c == null) {
          auw.authorizedUser.Transfer_Limit__c.addError('If user does not have an Unlimited Transfer Limit, please provide Transfer Limit.');
          allPopulated = false;
        }
      }*/

      return allPopulated;
    }

    /*
        Users and Accounts Junction
    */

    public PageReference continueToUsersAndAccounts() {

      if(saveServiceOptions()) {
          if(transferLimitsPopulated() == true) {
          showContactAndDepositSelection = false;
            showContactCreation = false;
            showDepositCreation = false;
            showDefaultServiceOptions = false;
            showServiceOptions = false;
            showUsersAndAccounts = true;
        }
      }

        return null;
    }

    public List<UsersAndAccountsByAccountWrapper> usersAndAccountsByAccountWrappers {get {
        if(usersAndAccountsByAccountWrappers == null) {
            usersAndAccountsByAccountWrappers = new List<UsersAndAccountsByAccountWrapper>();

            for(AnalyzedAccountWrapper aaw : analyzedAccountWrappers) {
                usersAndAccountsByAccountWrappers.add(new usersAndAccountsByAccountWrapper(authorizedUserWrappers, aaw));
            }
        }

        return usersAndAccountsByAccountWrappers;
    }set;}

    public class UsersAndAccountsByAccountWrapper {
        public AnalyzedAccountWrapper acct {get;set;}
        public List<UsersAndAccountsWrapper> usersAndAccountsWrappers {get;set;}

        public UsersAndAccountsByAccountWrapper( List<AuthorizedUserWrapper> authorizedUsers, AnalyzedAccountWrapper analyzedAccount) {
            acct = analyzedAccount;
            usersAndAccountsWrappers = new List<UsersAndAccountsWrapper>();
            for(AuthorizedUserWrapper auw : authorizedUsers) {
                usersAndAccountsWrappers.add(new UsersAndAccountsWrapper(auw, acct));
            }
        }
    }

    public class UsersAndAccountsWrapper {

        public AuthorizedUserWrapper authorizedUser {get;set;}
        public AnalyzedAccountWrapper analyzedAccount {get;set;}
      public Boolean isChecked {get;set;}
        //public List<ServiceOptionWrapper> serviceOptions {get;set;}

        public UsersAndAccountsWrapper(AuthorizedUserWrapper authUser, AnalyzedAccountWrapper analyzedAcct) {
            authorizedUser = authUser;
            analyzedAccount = analyzedAcct;
        isChecked = false;
            /*serviceOptions = new List<ServiceOptionWrapper>();

            for(ServiceOptionWrapper sow : analyzedAcct.serviceOptions) {
                if(sow.isChecked) {
                    serviceOptions.add(new ServiceOptionWrapper(sow.optionName, sow.isChecked));
                }
            }*/
        }
    }

    public Boolean saveServiceOptions() {
      SavePoint sp = Database.setSavePoint();

      try {
        List<LLC_BI__Authorized_User__c> authorizedUsers = new List<LLC_BI__Authorized_User__c>();

        for(AuthorizedUserWrapper auw : authorizedUserWrappers) {
          auw.authorizedUser.Select_Service_Option_Type__c = authorizedUserDefaults.Select_Service_Option_Type__c;
          auw.authorizedUser.Service_Options__c = '';
          for(ServiceOptionWrapper sow : auw.serviceOptions) {
            if(sow.isChecked) {
                System.debug('sow.isChecked'+sow.optionName);
                if(sow.optionName.equalsIgnoreCase('Current Day ACH Via'))  {
                    System.debug('sow.isChecked'+sow.isChecked);
                    //sow.optionName = 'Summary & Detail';
                    System.debug('sow.isChecked'+sow.optionName);
                }
              auw.authorizedUser.Service_Options__c += sow.optionName + ';';
            }
          }
          auw.authorizedUser.Service_Options__c = auw.authorizedUser.Service_Options__c.removeEnd(';');
          // Added by Chandu to fix the defect no.773 to bypass the validation rule
          auw.authorizedUser.User_has_unlimited_transfer_limit__c = true;
          auw.authorizedUser.Organization_ID__c = authorizedUserDefaults.Organization_ID__c;
          authorizedUsers.add(auw.authorizedUser);
        }

        List<LLC_BI__Analyzed_Account__c> analyzedAccounts = new List<LLC_BI__Analyzed_Account__c>();

        for(AnalyzedAccountWrapper aaw : analyzedAccountWrappers) {
          aaw.analyzedAccount.Select_Service_Option_Type__c = analyzedAccountServiceOptions.Select_Service_Option_Type__c;
          aaw.analyzedAccount.Service_Options__c = '';
          for(ServiceOptionWrapper sow : aaw.serviceOptions) {
            if(sow.isChecked) {
              aaw.analyzedAccount.Service_Options__c += sow.optionName + ';';
            }
          }
          aaw.analyzedAccount.Service_Options__c = aaw.analyzedAccount.Service_Options__c.removeEnd(';');

          analyzedAccounts.add(aaw.analyzedAccount);
        }

        if(authorizedUsers.size() > 0) {
          upsert authorizedUsers;
        }

        if(analyzedAccounts.size() > 0) {
          upsert analyzedAccounts;
        }

        return true;
      } catch(Exception e) {
        Database.rollback(sp);
      }

      return false;
    }

    public PageReference saveRecords() {

        SavePoint sp = Database.setSavePoint();

        try {
            

            List<Users_and_Accounts__c> usersAndAccounts = new List<Users_and_Accounts__c>();
            for(UsersAndAccountsByAccountWrapper uaabaw : usersAndAccountsByAccountWrappers) {
                for(UsersAndAccountsWrapper uaaw : uaabaw.usersAndAccountsWrappers) {
                    /*String uaawServiceOptions = '';
                    for(ServiceOptionWrapper sow : uaaw.serviceOptions) {
                        if(sow.isChecked) {
                            uaawServiceOptions += sow.optionName + ';';
                        }
                    }
                    uaawServiceOptions.removeEnd(';');
                    if(uaawServiceOptions != '') {
                        usersAndAccounts.add(new Users_and_Accounts__c(Authorized_User__c = uaaw.authorizedUser.authorizedUser.Id, Analyzed_Account__c = uaaw.analyzedAccount.analyzedAccount.Id, Select_Service_Option_Type__c = uaaw.analyzedAccount.analyzedAccount.Select_Service_Option_Type__c, Service_Options__c = uaaw.analyzedAccount.analyzedAccount.Service_Options__c));
                    }*/
            if(uaaw.isChecked == true) {
                System.debug('!!!!!!uaaw.isChecked()'+uaaw.isChecked);
              Users_and_Accounts__c uaa = new Users_and_Accounts__c(Authorized_User__c = uaaw.authorizedUser.authorizedUser.Id, Analyzed_Account__c = uaaw.analyzedAccount.analyzedAccount.Id, Select_Service_Option_Type__c = uaaw.analyzedAccount.analyzedAccount.Select_Service_Option_Type__c, Service_Options__c = uaaw.analyzedAccount.analyzedAccount.Service_Options__c);
              for(String fieldName : uaaw.analyzedAccount.apiFieldNames) {
                  System.debug('uaaw.fieldName()'+fieldName+uaaw.analyzedAccount.analyzedAccount.get(fieldName)+uaaw.authorizedUser.authorizedUser.get(fieldName));
                  if(fieldName.equalsIgnoreCase('Current_Day_Options__c')) {
                
                             uaa.put(fieldName, uaaw.authorizedUser.authorizedUser.get(fieldName)); 
                  }
                  else 
                      uaa.put(fieldName, uaaw.analyzedAccount.analyzedAccount.get(fieldName));
              }
              usersAndAccounts.add(uaa);
            }
                }
            }

            if(usersAndAccounts.size() > 0) {
                insert usersAndAccounts;
            }

        PageReference authorizedUserServiceOptionsPage = Page.AuthorizedUserServiceOptions;
        authorizedUserServiceOptionsPage.getParameters().put('id', treasuryServiceId);
        return authorizedUserServiceOptionsPage;

            /*ApexPages.standardController treasuryServiceController = new ApexPages.standardController(treasuryService);
            return treasuryServiceController.view();*/

        } catch(Exception e) {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
        }

        return null;
    }
}