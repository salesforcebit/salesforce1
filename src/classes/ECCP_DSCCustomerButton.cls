/*******************************************************************************************************************************************
* @name         :   ECCP_DSCCustomerButton
* @description  :   This class will be responsible for all the helper methods to create request, call the service and update response
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/
public class ECCP_DSCCustomerButton{
    public static Id objId {get;set;}
    public ECCP_CustomerRequest custRequest;
    public static List<String> ssnTINList = new List<String>();
    public ECCP_CustomerResponse response;
    public static List<sObject> legalEnt;
    public static List<LLC_BI__Product_Package__c> packageUpdateList = new List<LLC_BI__Product_Package__c>();
    public static List<Account> accUpdateList = new List<Account>();
    public static Map<String,String> ssnAccIdMap = new Map<String,String>();
    public static Map<String,String> ssnAccIdNewMap = new Map<String,String>();
    public static Set<String> loanAccNum = new Set<String>();
    public static Set<String> depAccNum = new Set<String>();
    public static Map<String,String> skipLoanSSN = new Map<String,String>();
    public static Map<String,String> skipDepSSN = new Map<String,String>();
    public boolean showError{get;set;}
    public boolean showServiceError{get;set;}
    public boolean nullResponse{get;set;}
    public boolean showSuccess{get;set;}
    public boolean showNullInput{get;set;}
    public static String objectName;
    public static String relation;
    
    
 
    public ECCP_DSCCustomerButton(ApexPages.StandardController controller) {

    }
/*******************************************************************************************************************************************
* @name         :   servicCall()
* @description  :   This method will be responsible to create request, call the service and handle response
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/
    public PageReference servicCall(){
        System.Savepoint svPt;
        Map<String,ECCP_Error_Code__mdt>  errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.dmlException);
        objId = ApexPages.currentPage().getParameters().get('id');
        //system.debug('---id--'+objId);
        objectName = objId.getSObjectType().getDescribe().getName();
        List<sObject> entQueryResponse = new List<sObject>();
        try{
            if(objId !=null ){
                
                entQueryResponse = queryEntity(objId);
                
            }
            if(entQueryResponse !=null && entQueryResponse.size()>0 ){
                custRequest = createRequest(entQueryResponse);
                if(custRequest !=null){
                    // helper class method call
                   if(!Test.isRunningTest()){
                       ECCP_DSCCustomerWS_Helper service = new ECCP_DSCCustomerWS_Helper();
                       response = service.serviceCall(custRequest);
                       
                   }
                   else{
                       response = ECCP_DscCustomer_WebSvcCalloutTest.DCTest();
                   }
                    if(response !=null){
                        svPt = Database.setSavepoint();
                        handleResponse(response); 
                        showSuccess = true;
                      
                    }
                    
                    else{
                        
                        showSuccess = false;
                        showServiceError = false;
                        nullResponse = true;
                       
                        return null;
                    }
                    // To get execption to increase coverage
                    if(Test.isRunningTest()){
                        Test.setMock(WebServiceMock.class, new ECCP_DSCCustomer_WebServiceMockImplExc());
        
                        ECCP_CustomerRequest TestReq = new ECCP_CustomerRequest();
                        List<String> ssnList = new List<String>();
                        ssnList.add('123');
                        ssnList.add('321');
                        TestReq.SSN = ssnList;
                        TestReq.appId='123asd';
                        TestReq.MDMId = 'extId';
                        
                        ECCP_CustomerResponse CBres = ECCP_DSCCustomerTest_WebSvcCallout.DSCCustomerCallout(TestReq);
                        handleResponse(CBres); 
                    }
                    
                }
                else{
                    showSuccess = false;
                    nullResponse = false;
                    showError = true;
                    
                    return null;
                    
                }
            }
            else{
                showSuccess = false;
                showError = false;
                nullResponse = false;
                showNullInput = true;
               
                return null;
            }
        }
        
        catch(callOutException ex){
         
         nullResponse = false;
         showServiceError = true;
         showSuccess = false;
         
         
         if(ex.getMessage().contains('CANNOT_EXECUTE_FLOW_TRIGGER')){
            
            return null;
         }
         try{   
         LLC_BI__Product_Package__c proPack = new LLC_BI__Product_Package__c(id=objId,ECCP_CIX_Status__c='Failure');
         update proPack; 
         errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullResponse);
         ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.DSCCustomerMataData , 'ECCP_DSCCustomerButton', 'servicCall', String.valueOf(ex.getLineNumber()),ex.getMessage(), objId,ex.getStackTraceString());
         
         ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
         }
         catch(exception exc){
             
         }
         return null;
        }
        
        catch(Exception e){
        
            nullResponse = false;
            showServiceError = true;
            
            showSuccess = false;
            Database.rollback(svPt);
            if(e.getMessage().contains('CANNOT_EXECUTE_FLOW_TRIGGER')){
            system.debug('--process builder error---');
            return null;
            }
            try{
            LLC_BI__Product_Package__c proPack = new LLC_BI__Product_Package__c(id=objId,ECCP_CIX_Status__c='Failure');
            update proPack;
            errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullResponse);
            ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.DSCCustomerMataData , 'ECCP_DSCCustomerButton', 'servicCall', String.valueOf(e.getLineNumber()),e.getMessage(),objId,e.getStackTraceString());
            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
            }
            catch(Exception exc2){
            //system.debug('---exc2');
            }
            return null;
        } 
    
        return null;
    }
/*******************************************************************************************************************************************
* @name         :   queryEntity
* @description  :   This method will be responsible to query SSNs from Entity Involved Object
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/
    public List<sObject> queryEntity(Id obId){
        String queryString;
        // Aditya : 2.0 Change 6 Sep 2016 : added LLC_BI__Product_Package__r.LLC_BI__Account__r.LegacyPrimaryKey__c
        queryString = 'select id,LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c,LLC_BI__Product_Package__c,LLC_BI__Product_Package__r.LLC_BI__Account__c,LLC_BI__Account__c,LLC_BI__Product_Package__r.LLC_BI__Account__r.LegacyPrimaryKey__c  from ';
        if(objectName !='' && objectName =='LLC_BI__Product_Package__c'){
            queryString = queryString+'LLC_BI__Legal_Entities__c where LLC_BI__Product_Package__c = :objId';
        }
        
        
        legalEnt = Database.query(queryString);
        return legalEnt;
    }
/*******************************************************************************************************************************************
* @name         :   createRequest
* @description  :   This method will be responsible to create response for the service call. Also, it is checking for duplicate and null
                    SSNs.
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/
    public ECCP_CustomerRequest createRequest(List<LLC_BI__Legal_Entities__c> entityData){
        Set<String> SSNTINSet = new Set<String>();
        Set<String> LonIdSet = new Set<String>();
        for(LLC_BI__Legal_Entities__c ent : entityData){
            if(ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c !=null && ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c !=''){
                
                SSNTINSet.add(ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c);
                relation = ent.LLC_BI__Product_Package__r.LLC_BI__Account__c;
                ssnAccIdMap.put(ent.LLC_BI__Account__c,ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c);
                ssnAccIdNewMap.put(ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c,ent.LLC_BI__Account__c);
            }
            else{
                return null;
            }
            
        }
        
        custRequest = new ECCP_CustomerRequest();
        ssnTINList.addAll(SSNTINSet);
        custRequest.SSN = ssnTINList;
        custRequest.appId = objId;
        // Aditya : 2.0 Change 6 Sep 2016 : added MDMId
        custRequest.MDMId = entityData[0].LLC_BI__Product_Package__r.LLC_BI__Account__r.LegacyPrimaryKey__c;
        
        if(ssnTINList != null && ssnTINList.size()>0 && legalEnt !=null && ssnAccIdNewMap !=null && ssnAccIdNewMap!=null && ssnAccIdMap.size() == ssnAccIdNewMap.size()){
            system.debug('---custRequest---'+custRequest);
            return custRequest;
            
        }
        else{
            return null;
        }
    }
    
/*******************************************************************************************************************************************
* @name         :   handleResponse
* @description  :   This method will be responsible to read response from the service and insert data in various objects in salesforce.
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/    
    public void handleResponse(ECCP_CustomerResponse response){
        system.debug('----resp in code--'+response);
        Set<String> existingData = new Set<String>();
        Map<String,LLC_BI__Loan__c> loanMap = new Map<String,LLC_BI__Loan__c>();
        Map<String,LLC_BI__Deposit__c> depositMap = new Map<String,LLC_BI__Deposit__c>();
        List<LLC_BI__Loan__c> loanInsert = new List<LLC_BI__Loan__c>();
        List<LLC_BI__Deposit__c> depositInsert = new List<LLC_BI__Deposit__c>();
        List<Exposure_Adjustment__c> expAdj = new List<Exposure_Adjustment__c> ();
        List<Exposure_Adjustment__c> expAdjupdate = new List<Exposure_Adjustment__c> ();
        Map<String, Account> ssnAccountMap = new Map<String,Account>();
        Map<String,Account> entAccountUpdate = new Map<String,Account>();
        
        Map<String,ECCP_CustomerResponse.Acct> productAcntNum_prodResDetail_Map = new Map<String,ECCP_CustomerResponse.Acct> ();
        List<LLC_BI__Legal_Entities__c> entInvlToInsert = new List<LLC_BI__Legal_Entities__c> ();     
        String queryString;
        queryString ='Select id,ECCP_Total_Commercial_Credit_Exposure__c,ECCP_Total_Commercial_Deposit_Balance__c,ECCP_Total_Consumer_Credit_Exposure__c, ECCP_Total_consumer_Deposit_Balance__c,ECCP_Total_Exposure__c';
        if(objId !=null && objectName =='LLC_BI__Product_Package__c'){
            queryString = queryString +' from LLC_BI__Product_Package__c where id = : objId';
            for(LLC_BI__Product_Package__c pro : (List<LLC_BI__Product_Package__c>)database.query(queryString)){
                pro.ECCP_Total_Consumer_Credit_Exposure__c = response.TotalConsumerCreditExposure !=null && response.TotalConsumerCreditExposure !=''?  Decimal.valueOf(response.TotalConsumerCreditExposure): null;
                pro.ECCP_Total_Commercial_Credit_Exposure__c = response.TotalCommercialCreditExposure !=null && response.TotalCommercialCreditExposure !='' ? Decimal.valueOf(response.TotalCommercialCreditExposure): null;
                pro.ECCP_Total_Commercial_Deposit_Balance__c = response.TotalCommercialDepositBalance !=null && response.TotalCommercialDepositBalance !=''? Decimal.valueOf(response.TotalCommercialDepositBalance): null;
                pro.ECCP_Total_consumer_Deposit_Balance__c = response.TotalConsumerDepositBalance !=null && response.TotalConsumerDepositBalance !=''?  Decimal.valueOf(response.TotalConsumerDepositBalance): null;
                pro.ECCP_Total_Exposure__c =response.ExposureCalAmount !=null && response.ExposureCalAmount !='' ? Decimal.valueOf(response.ExposureCalAmount): null;
                pro.ECCP_CIX_Status__c = 'Success';
                packageUpdateList.add(pro);
            }
        }
        
        // creating ssn,account map of accounts involved in entity
        for(Account acc : [select id, Name, Current_Risk_Rating__c,AccountNumber from Account where id In : ssnAccIdMap.keySet()]){
            
            ssnAccountMap.put(ssnAccIdMap.get(acc.Id),acc);
           
            
        }
         system.debug('--ssnAccountMap--'+ssnAccountMap);
        // system.debug('--ssnAccIdMap--'+ssnAccIdMap);
        for(ECCP_CustomerResponse.Acct loanDepos : response.AcctData){
            LLC_BI__Deposit__c dep = new LLC_BI__Deposit__c();
            LLC_BI__Loan__c lon = new LLC_BI__Loan__c();
            String key;
            if(loanDepos.partySSN !=null && loanDepos.partySSN  !='' && ssnAccountMap !=null && ssnAccountMap.size()>0 ){
                system.debug('--209---'+loanDepos.partySSN);
                //system.debug('--ssnAccountMap.get(loanDepos.partySSN).Current_Risk_Rating__c--'+ssnAccountMap.get(loanDepos.partySSN).Current_Risk_Rating__c);
                ssnAccountMap.get(loanDepos.partySSN).Current_Risk_Rating__c = loanDepos.Obligor_RR !=null && loanDepos.Obligor_RR !='' ?string.valueof(loanDepos.Obligor_RR) : '' ;                
                
                ssnAccountMap.get(loanDepos.partySSN).AccountNumber = loanDepos.CL3Obligor;
              //  Aditya : Commented on 25 Aug to solve prod issue. No need to update name.
              
             //  ssnAccountMap.get(loanDepos.partySSN).Name = loanDepos.AccountName !=null && loanDepos.AccountName !='' ?  loanDepos.AccountName : ECCP_IntegrationConstants.AccName;
                
               entAccountUpdate.put(ssnAccountMap.get(loanDepos.partySSN).Id,ssnAccountMap.get(loanDepos.partySSN));
            }
            
           
            if(loanDepos.AccountType !=null && loanDepos.AccountType !=''){
               
                if(!loanDepos.AccountType.contains(ECCP_IntegrationConstants.depositCamel) && !loanDepos.AccountType.contains(ECCP_IntegrationConstants.depositSmall)){
                    
                    if(loanAccNum.contains(loanDepos.AccountNumber)){
                        
                        skipLoanSSN.put(loanDepos.partySSN+'-'+loanDepos.AccountNumber,loanDepos.partySSN);
                        
                    }
                    else{
                      
                        lon.ECCP_Bank_Code__c = loanDepos.BankNumber;
                        lon.ECCP_CMT_Obligation__c = loanDepos.CL3Obligation !=null && loanDepos.CL3Obligation !=''? Decimal.valueOf(loanDepos.CL3Obligation) : null;                       
                        lon.LLC_BI__Booked_Date__c = loanDepos.OpenDate !=null && loanDepos.OpenDate !='' ? Date.valueOf(loanDepos.OpenDate ) : null;
                        
                        lon.ECCP_Product_Status__c = loanDepos.AccountStatus;
                        lon.LLC_BI__Product_Line__c = ECCP_IntegrationConstants.productLine;
                        lon.LLC_BI__Product_Type__c = loanDepos.ProductCode;
                        lon.LLC_BI__Product__c = loanDepos.SubProductCode;
                        lon.LLC_BI__Account__c = relation;
                        
                        lon.ECCP_TSYS_Account_Number__c = loanDepos.TSYSNumber;
                        
                        lon.CRA_Code__c = loanDepos.CRACode;
                        lon.LLC_BI__Maturity_Date__c = loanDepos.MaturityDate !=null && loanDepos.MaturityDate !='' ? Date.valueOf(loanDepos.MaturityDate) : null;
                        lon.ECCP_Next_Review_Date__c = loanDepos.ReviewDate !=null && loanDepos.ReviewDate !=''? Date.valueOf(loanDepos.ReviewDate): null;
                        //Aditya As per 2.0 mapping changed to LLC_BI__Principal_Balance__c 
                        lon.LLC_BI__AmountOutstanding__c = loanDepos.CommitmentAmount !=null && loanDepos.CommitmentAmount !=''? Decimal.valueOf(loanDepos.CommitmentAmount) : null;
                        lon.LLC_BI__Principal_Balance__c = loanDepos.BalanceAmount !=null && loanDepos.BalanceAmount !=''? Decimal.valueOf(loanDepos.BalanceAmount) : null;
                        // 2.0 change end
                        lon.ECCP_Pmt_Due_Date__c = loanDepos.PmtDueDate !=null && loanDepos.PmtDueDate !=''? Date.valueOf(loanDepos.PmtDueDate) : null ;
                        lon.ECCP_Payment_Amount__c = loanDepos.PmtAmt !=null && loanDepos.PmtAmt !='' ? Decimal.valueOf(loanDepos.PmtAmt) : null;
                        lon.ECCP_Exposure_Indicator__c = loanDepos.ExposureFlag;
                        lon.Probability_of_Default__c = loanDepos.Obligor_POD !=null && loanDepos.Obligor_POD !='' ? Decimal.valueOf(loanDepos.Obligor_POD) : null;
                        lon.ECCP_CMT_Account_Number__c = loanDepos.AccountNumber;
                        lon.Current_Risk_Rating__c = loanDepos.FacilityRR ;
                        lon.Loss_Given_Default_LGD__c = loanDepos.LostGivenDefault !=null && loanDepos.LostGivenDefault !='' ? Decimal.valueOf(loanDepos.LostGivenDefault):null;
                        key = loanDepos.partySSN+'-'+loanDepos.AccountNumber;
                        lon.ECCP_LoanDSC__c = key;
                        loanMap.put(key,lon);
                        loanAccNum.add(loanDepos.AccountNumber); 
                        
                        productAcntNum_prodResDetail_Map.put(loanDepos.AccountNumber,loanDepos); 
                        
                    }
                }
                else{
                    if(depAccNum.contains(loanDepos.AccountNumber)){
                        
                        skipDepSSN.put(loanDepos.partySSN+'-'+loanDepos.AccountNumber,loanDepos.partySSN);
                    }
                    else{
                        
                        
                        dep.ECCP_Bank_Code__c = loanDepos.BankNumber;
                        // As per 2.0
                        dep.ECCP_CMT_DDA_Average_Balance__c = loanDepos.DDA_6M_Ave_bal != null && loanDepos.DDA_6M_Ave_bal !=''?Decimal.valueOf(loanDepos.DDA_6M_Ave_bal): null;
                        // 2.0 change
                        dep.LLC_BI__Amount__c  = loanDepos.BalanceAmount !=null && loanDepos.BalanceAmount !=''? Decimal.valueOf(loanDepos.BalanceAmount) : null;
                        // 2.0 change End
                        dep.ECCP_CMT_NSF_Count__c = loanDepos.NSFCnt_12_M != null && loanDepos.NSFCnt_12_M !=''?Decimal.valueOf(loanDepos.NSFCnt_12_M):null ;
                        key = loanDepos.partySSN+'-'+loanDepos.AccountNumber;
                        dep.LLC_BI__Account__c = relation;
                       
                        dep.LLC_BI__Open_Date__c = loanDepos.OpenDate !=null && loanDepos.OpenDate !=''  ? Date.valueOf(loanDepos.OpenDate) : null;
                        
                        dep.LLC_BI__Product__c =  loanDepos.SubProductCode;
                        dep.ECCP_CMT_Overdraft_Count__c = loanDepos.ODCnt_12_M !=null && loanDepos.ODCnt_12_M !='' ? Decimal.valueOf( loanDepos.ODCnt_12_M) :  null;
                        dep.ECCP_Product_Status__c = loanDepos.AccountStatus;
                        dep.LLC_BI__Product_Line__c = ECCP_IntegrationConstants.productLine;
                        dep.LLC_BI__Product_Type__c = loanDepos.ProductCode;
                        dep.ECCP_CMT_Account_Number__c = loanDepos.AccountNumber;
                        dep.LLC_BI__Account_Number__c = loanDepos.AccountNumber;
                        depositMap.put(key,dep);
                        depAccNum.add(loanDepos.AccountNumber); 
                        productAcntNum_prodResDetail_Map.put(loanDepos.AccountNumber,loanDepos); 
                    }
                }
            }
                
        }
       
       

        if(entAccountUpdate != null && entAccountUpdate.size()>0){
            accountUpdate(entAccountUpdate.Values());
        }       
        
        Map<String,LLC_BI__Loan__c> deDupLoanMap = new Map<String,LLC_BI__Loan__c>();
        Map<String,LLC_BI__Deposit__c> deDupDepositMap = new Map<String,LLC_BI__Deposit__c>();
        
        for(LLC_BI__Loan__c recLon : loanMap.values()){
            
            deDupLoanMap.put(recLon.ECCP_CMT_Account_Number__c,recLon);             
        }
        
        for(LLC_BI__Deposit__c recDep : depositMap.values()){
            deDupDepositMap.put(recDep.ECCP_CMT_Account_Number__c,recDep );              
        }
        if(deDupDepositMap !=null && deDupDepositMap.size()>0){
        upsert deDupDepositMap.values() ECCP_CMT_Account_Number__c;
       
        }
        if(deDupLoanMap !=null && deDupLoanMap.size()>0){
        upsert deDupLoanMap.values() ECCP_CMT_Account_Number__c;
        
        }
        
        List<String> depLonIdList = new List<String>();
        for(LLC_BI__Deposit__c dep :deDupDepositMap.values()){
            depLonIdList.add(dep.Id);
        }
        for(LLC_BI__Loan__c dep :deDupLoanMap.values()){
            depLonIdList.add(dep.Id);
        }
        Map<String,Exposure_Adjustment__c> existingAdjMap = new Map<String,Exposure_Adjustment__c>();
        // Aditya : Release 2.0 : added fields in queries in if block : 6 Sep 2016
        if(depLonIdList != null && depLonIdList.size()>0){
            for(Exposure_Adjustment__c ex : [select id,ProductPackage__c,ECCP_Deposit__c,ECCP_Loan__c,ECC_Dep_Loan_AcctProductNumber__c,ECC_Deposit_Loan_Balance__c from Exposure_Adjustment__c where ECCP_Deposit__c IN : depLonIdList 
                                            AND ProductPackage__c =: objId]){
                existingAdjMap.put(ex.ECCP_Deposit__c,ex);
            }
            for(Exposure_Adjustment__c ex : [select id,ProductPackage__c,ECCP_Deposit__c,ECCP_Loan__c,ECC_Dep_Loan_AcctProductNumber__c,ECC_Deposit_Loan_Balance__c from Exposure_Adjustment__c where ECCP_Loan__c IN : depLonIdList
                                            AND ProductPackage__c =: objId]){
                existingAdjMap.put(ex.ECCP_Loan__c,ex);
            }
        }
        for(LLC_BI__Deposit__c dep : deDupDepositMap.values()){
            Exposure_Adjustment__c eadj = new Exposure_Adjustment__c();
            if(!existingAdjMap.containsKey(dep.Id)){
                eadj.ProductPackage__c = objId;
                eadj.ECCP_Deposit__c = dep.Id;
                
                eadj.ECC_Dep_Loan_AcctProductNumber__c = dep.ECCP_CMT_Account_Number__c + '#' + dep.LLC_BI__Product_Type__c;
                eadj.ECC_Deposit_Loan_Balance__c  = dep.LLC_BI__Amount__c;
                
                expAdj.add(eadj);
            }else{
                 
                existingAdjMap.get(dep.Id).ECC_Dep_Loan_AcctProductNumber__c = dep.ECCP_CMT_Account_Number__c + '#' + dep.LLC_BI__Product_Type__c;
                existingAdjMap.get(dep.Id).ECC_Deposit_Loan_Balance__c = dep.LLC_BI__Amount__c;
                expAdjupdate.add(existingAdjMap.get(dep.Id));
            }
        }
        for(LLC_BI__Loan__c lon : deDupLoanMap.values()){
            Exposure_Adjustment__c eadjLon = new Exposure_Adjustment__c();
            if(!existingAdjMap.containsKey(lon.Id)){
                eadjLon.ProductPackage__c = objId;
                eadjLon.ECCP_Loan__c = lon.Id;
                eadjLon.Committed_Exposure__c = lon.LLC_BI__AmountOutstanding__c;
                // Aditya 2.0 Release : adding following fields on Exposure : 6 Sep 2016
                eadjLon.ECC_Dep_Loan_AcctProductNumber__c = lon.ECCP_CMT_Account_Number__c + '#' + lon.LLC_BI__Product_Type__c;
                eadjLon.ECC_Deposit_Loan_Balance__c = lon.LLC_BI__Principal_Balance__c;
               
                expAdj.add(eadjLon);
            }
            else{
                existingAdjMap.get(lon.Id).Committed_Exposure__c = lon.LLC_BI__AmountOutstanding__c;
                // Aditya 2.0 Release : adding following fields on Exposure : 6 Sep 2016
                existingAdjMap.get(lon.Id).ECC_Dep_Loan_AcctProductNumber__c = lon.ECCP_CMT_Account_Number__c + '#' + lon.LLC_BI__Product_Type__c;
                existingAdjMap.get(lon.Id).ECC_Deposit_Loan_Balance__c = lon.LLC_BI__Principal_Balance__c;
                expAdjupdate.add(existingAdjMap.get(lon.Id));
            }
        }
        if(expAdj !=null && expAdj.size()>0){
            insert expAdj;
        }
        if(expAdjupdate !=null && expAdjupdate.size()>0){
            update expAdjupdate;
        }
        // Create Entities Involvement Recs for Booked Loans and Deposits     
        
        Set<String> checkExtEntities = new set<string> ();        
        for(LLC_BI__Legal_Entities__c eIs : [select LLC_BI__Loan__c,LLC_BI__Deposit__c,LLC_BI__Account__c from LLC_BI__Legal_Entities__c where LLC_BI__Account__c in :ssnAccIdMap.keySet()]){                
               if(eIs.LLC_BI__Loan__c != null ){
                   checkExtEntities.add(eIs.LLC_BI__Loan__c+'-'+eIs.LLC_BI__Account__c);
               }else if(eIs.LLC_BI__Deposit__c != null){              
                   checkExtEntities.add(eIs.LLC_BI__Deposit__c+'-'+eIs.LLC_BI__Account__c);
               }             
        }
        
        Map<string,string> acntNum_loanIdMap = new Map<string,string> ();
        for(LLC_BI__Loan__c recLon : deDupLoanMap.values()){            
            acntNum_loanIdMap.put(recLon.ECCP_CMT_Account_Number__c,recLon.Id);            
        }
        
        Map<string,string> acntNum_DepIdMap = new Map<string,string> ();
        for(LLC_BI__Deposit__c recDep : deDupDepositMap.values()){            
            acntNum_DepIdMap.put(recDep.ECCP_CMT_Account_Number__c,recDep.Id);            
        }
        
        
        for(ECCP_CustomerResponse.Acct loanDepos : response.AcctData){
            if(loanDepos.partySSN !=null && loanDepos.partySSN  !='' && ssnAccountMap !=null && ssnAccountMap.size()>0 ){
                LLC_BI__Legal_Entities__c eI = new LLC_BI__Legal_Entities__c ();    
                ei.LLC_BI__Account__c = ssnAccIdNewMap.get(loanDepos.partySSN);
                ei.LLC_BI__Borrower_Type__c = loanDepos.accountIDREF;
                
                if(acntNum_loanIdMap.containsKey(loanDepos.AccountNumber)){
                    if(!checkExtEntities.contains(acntNum_loanIdMap.get(loanDepos.AccountNumber)+'-'+ei.LLC_BI__Account__c)){
                    //system.debug('-----in loan ent data acc number----'+loanDepos.AccountNumber+'---SSN--'+loanDepos.partySSN);
                    ei.LLC_BI__Loan__c = acntNum_loanIdMap.get(loanDepos.AccountNumber);
                    //ei.LLC_BI__Tax_ID__c = loanDepos.partySSN;
                    entInvlToInsert.add(ei);
                    }                   
                }   
                if(acntNum_DepIdMap.containsKey(loanDepos.AccountNumber)){
                    if(!checkExtEntities.contains(acntNum_DepIdMap.get(loanDepos.AccountNumber)+'-'+ei.LLC_BI__Account__c)){
                    //system.debug('-----in deposit ent data acc number----'+loanDepos.AccountNumber+'---SSN--'+loanDepos.partySSN);
                    ei.LLC_BI__Deposit__c  = acntNum_DepIdMap.get(loanDepos.AccountNumber);
                    //ei.LLC_BI__Tax_ID__c = loanDepos.partySSN;
                    entInvlToInsert.add(ei);
                    }                   
                }               
            }   
        }   
        
        // DeDup entInvlToInsert list
        
        Set<LLC_BI__Legal_Entities__c> setEnt = new Set<LLC_BI__Legal_Entities__c>();
        List<LLC_BI__Legal_Entities__c> entInvlToInsert2 = new List<LLC_BI__Legal_Entities__c>();
        setEnt.addAll(entInvlToInsert);
        entInvlToInsert2.addAll(setEnt);
        
        insert entInvlToInsert2;
        update packageUpdateList;
        
        
    }
    

/*******************************************************************************************************************************************
* @name         :   accountUpdate
* @description  :   This method will be responsible to update Account object in salesforce.
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/
    public void accountUpdate(List<Account> accUpdate){       
        update accUpdate;
        
    }
}