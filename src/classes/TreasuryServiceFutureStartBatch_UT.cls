@istest
private class TreasuryServiceFutureStartBatch_UT {

	static testmethod void testFutureStartBatch() {
		Account testAccount = UnitTestFactory.buildTestAccount();
		insert testAccount;

		Product2 testProduct = UnitTestFactory.buildTestProduct();
		testProduct.Family = 'Deposits & ECP';
		insert testProduct;

		LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
		insert testProductLine;

		LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
		insert testProductType;

		LLC_BI__Product__c testNCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
		insert testNCinoProduct;

		LLC_BI__Product_Package__c testProductPackage = UnitTestFactory.buildTestProductPackage();
		insert testProductPackage;

		List<LLC_BI__Treasury_Service__c> testTSes = new List<LLC_BI__Treasury_Service__c>();
		LLC_BI__Treasury_Service__c testTSFutureStartTodayImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSFutureStartPastImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSFutureStartFutureImp = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTSPendingPrereqs = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		testTSFutureStartTodayImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingFutureStartDate;
		testTSFutureStartPastImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingFutureStartDate;
		testTSFutureStartFutureImp.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingFutureStartDate;
		testTSPendingPrereqs.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
		testTSFutureStartTodayImp.Planned_Implementation_Start_Date__c = date.today();
		testTSFutureStartPastImp.Planned_Implementation_Start_Date__c = date.today().addDays(1);
		testTSFutureStartFutureImp.Planned_Implementation_Start_Date__c = date.today().addDays(-1);
		testTSPendingPrereqs.Planned_Implementation_Start_Date__c  = date.today();
		testTSes.add(testTSFutureStartTodayImp);
		testTSes.add(testTSFutureStartPastImp);
		testTSes.add(testTSFutureStartFutureImp);
		testTSes.add(testTSPendingPrereqs);
		for(LLC_BI__Treasury_Service__c service : testTSes) {
			service.Documents_Complete__c = true;
		}
		insert testTSes;

		TreasuryServiceFutureStartBatch batch = new TreasuryServiceFutureStartBatch();
		test.startTest();
			Database.executeBatch(batch);
		test.stopTest();

		LLC_BI__Treasury_Service__c resultFutureStartTodayImp = [Select Id, LLC_BI__Stage__c from LLC_BI__Treasury_Service__c Where Id =: testTSFutureStartTodayImp.Id];
		System.assertEquals(Constants.treasuryServiceStageFulfillment, resultFutureStartTodayImp.LLC_BI__Stage__c);

		LLC_BI__Treasury_Service__c resultFutureStartPastImp = [Select Id, LLC_BI__Stage__c from LLC_BI__Treasury_Service__c Where Id =: testTSFutureStartPastImp.Id];
		System.assertEquals(Constants.treasuryServiceStagePendingFutureStartDate, resultFutureStartPastImp.LLC_BI__Stage__c);

		LLC_BI__Treasury_Service__c resultFutureStartFutureImp = [Select Id, LLC_BI__Stage__c from LLC_BI__Treasury_Service__c Where Id =: testTSFutureStartFutureImp.Id];
		System.assertEquals(Constants.treasuryServiceStagePendingFutureStartDate, resultFutureStartFutureImp.LLC_BI__Stage__c);

		LLC_BI__Treasury_Service__c resultPendingPrereqs = [Select Id, LLC_BI__Stage__c from LLC_BI__Treasury_Service__c Where Id =: testTSPendingPrereqs.Id];
		System.assertEquals(Constants.treasuryServiceStagePendingPrerequisites, resultPendingPrereqs.LLC_BI__Stage__c);
	}
}