global class EmailCredentials {

	webservice String parameter { get; set; }

	public EmailCredentials() {
		this.parameter = 'value';
	}
    
    public static Datetime localTime() {
        
        Datetime current = Datetime.now(); // returns date time value in GMT time zone.
 
		Datetime local = current.Date();
        local = local.addHours(current.hour());
        local = local.addMinutes(current.minute());
        local = local.addSeconds(current.second()); 
        
        local.format('yyyy-MM-dd\'T\'hh:mm:ss');
        
        
        System.debug('Datetime '+local);
        return local;
        
    }

	webservice static String serviceName(String authorizedUserId){
		LLC_BI__Authorized_User__c user = [select User_Password__c, LLC_BI__Treasury_Service_Product__c	,KTT_User_Name__c, Email_Address__c, User_ID_Sent_to_EAP__c, User_Password_Sent_to_EAP__c, LLC_BI__Treasury_Service__c, Organization_ID__c, User_ID__c from LLC_BI__Authorized_User__c where id = :authorizedUserId];

		String lowerCaseUser = Utility.toLowerCase(user.KTT_User_Name__c);
		String docId = Utility.generateDocUnId();
        String email = user.Email_Address__c; 
        String tsProduct = user.LLC_BI__Treasury_Service_Product__c;
        String orgId = user.Organization_ID__c;
        
		String response;
        
		TROEventCapture.EAPEventCapturePortSoap1_1 port = new TROEventCapture.EAPEventCapturePortSoap1_1();
        
        String message; 
        String eapUID;
        String eventStatus;
         
        if(user.User_Password__c != null) {
       	 if(tsProduct.equalsIgnoreCase('ACH: Origination & Receipt') || tsProduct.equalsIgnoreCase('ACH Fraud Services')) {
			//send only Event 23
			TROEventCapture.EventHeaderType header23 = TROEventCapture_Builder.createEventHeaderType(localTime(), 'TRO', 'EV00023', docId, null);
			TROEventCapture.eventEV00023BodyType body23 = TROEventCapture_Builder.createEventEV00023BodyType(lowerCaseUser,Utility.decrypt(user.User_Password__c),orgId,email);
            TROEventCapture.TransactionSummaryType resultSummary23;
            if(!Test.isRunningTest())
			    resultSummary23 = port.receiveEV00023(header23, body23);
            else {
                resultSummary23 = new TROEventCapture.TransactionSummaryType();
                resultSummary23.eventStatus = 'SUCCESS';
			}
            
            eventStatus = resultSummary23.eventStatus;
            eapUID = resultSummary23.eapGUID;
            message = resultSummary23.message; 
        	    
            
            System.debug('email Address - ' +user.Email_Address__c); 
			System.debug('Response while sending User name '+message + ' '+ 'with source ref number: ' +  docId+' and EAPUID: '+ eapUID);
			if(eventStatus.equalsIgnoreCase('SUCCESS')) {
                user.User_ID_Sent_to_EAP__c = true;
                user.User_Password_Sent_to_EAP__c = true;
                response =  'Success while sending username/password for ACH - \n'+message + ' '+ 'with source ref number: ' +  docId+' and EAPUID: '+ eapUID;
   
            }
             else {
                 response =  'Error while sending UserName for ACH - \n'+message + ' '+ 'with source ref number: ' +  docId+' and EAPUID: '+ eapUID;
             } 
             
            //create audt log object
			Audit_Log__c log = new Audit_Log__c();
			log.Log_Time__c = System.now();
			log.Service__c = 'ACH Email Credentials Request';
			log.Treasury_Service__c = user.LLC_BI__Treasury_Service__c;
			log.User__c =  UserInfo.getUserId();
			log.response__c = response;

			insert log;
                
		}
        else { // else if(!tsProduct.equalsIgnoreCase('KTT Information Reporting')) {
            //first send 21.  then on success send 22
			TROEventCapture.EventHeaderType header21 = TROEventCapture_Builder.createEventHeaderType(localTime(), 'TRO', 'EV00021', docId, null);
			TROEventCapture.eventEV00021BodyType body21 = TROEventCapture_Builder.createEventEV00021BodyType(user.KTT_User_Name__c, email);
            TROEventCapture.TransactionSummaryType resultSummary21;
            if(!Test.isRunningTest())
			    resultSummary21 = port.receiveEV00021(header21, body21);
            else {
                resultSummary21 = new TROEventCapture.TransactionSummaryType();
                resultSummary21.eventStatus = 'SUCCESS';
			}
            
            eventStatus = resultSummary21.eventStatus;
            eapUID = resultSummary21.eapGUID;
            message = resultSummary21.message; 
        	    
            
            System.debug('email Address - ' +user.Email_Address__c); 
			System.debug('Response while sending User name '+message + ' '+ 'with source ref number: ' +  docId+' and EAPUID: '+ eapUID);
			//if EV00021 was a success do EV00022
			if(eventStatus.equalsIgnoreCase('SUCCESS')) {
                user.User_ID_Sent_to_EAP__c = true;
                System.debug('Sending user password now');
				TROEventCapture.EventHeaderType header22 = TROEventCapture_Builder.createEventHeaderType(localTime(), 'TRO', 'EV00022', docId, '240');
                TROEventCapture.eventEV00022BodyType body22;
                if(!Test.isRunningTest())
                     body22 = TROEventCapture_Builder.createEventEV00022BodyType(user.KTT_User_Name__c, Utility.decrypt(user.User_Password__c), email);
                else
                    body22 = new TROEventCapture.eventEV00022BodyType();

                TROEventCapture.TransactionSummaryType resultSummary22;
                if(!Test.isRunningTest())
				    resultSummary22 = port.receiveEV00022(header22, body22);
                else {
                    resultSummary22 = new TROEventCapture.TransactionSummaryType();
                    resultSummary22.eventStatus = 'SUCCESS';
                }
                
                eventStatus = resultSummary22.eventStatus;
                eapUID = resultSummary22.eapGUID;
                message = resultSummary22.message;
							
				System.debug('Response while sending User password '+message + ' '+ 'with source ref number: ' +  docId+' and EAPUID: '+ eapUID);
                
                    if(eventStatus.equalsIgnoreCase('SUCCESS')) {
                        user.User_Password_Sent_to_EAP__c = true;
                        response =  'Success while sending password - \n'+message + ' '+ 'with source ref number: ' +  docId+' and EAPUID: '+ eapUID;
                    }
                    else {
                         response = 'Error while sending password  - \n'+message + ' '+ 'with source ref number: ' +  docId+' and EAPUID: '+ eapUID;
                     } 
            }
             else {
                 response =  'Error while sending UserName  - \n'+message + ' '+ 'with source ref number: ' +  docId+' and EAPUID: '+ eapUID;
             } 
         }
            //create audt log object
			Audit_Log__c log = new Audit_Log__c();
			log.Log_Time__c = System.now();
			log.Service__c = 'Non KTT Email Credentials Request';
			log.Treasury_Service__c = user.LLC_BI__Treasury_Service__c;
			log.User__c =  UserInfo.getUserId();
			log.response__c = response;

			insert log;
        }
        else 
            response = 'User Password has to be generated before credentials can be sent';
		
        update user;
		return response;
	}
}