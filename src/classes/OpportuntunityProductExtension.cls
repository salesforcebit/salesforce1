//================================================================================
//  KeyBank  
//  Object: Opportunity
//  Author: Offshore
//  Detail: OpportuntunityProductExtension
//  Description : /** This Extension class is used in OpportunityProductCustomPage for Multi Product **/

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================



public class OpportuntunityProductExtension{
    Public Opportunity theOpp  {get; set;}
    public RecordType recordTypeID{get; set;}

    public opportunityLineItem[] shoppingCart {get;set;}

    public priceBookEntry[] AvailableProducts {get;set;}
    public String toSelect {get; set;}

    public String searchString {get; set;}
    public String SearchFilter{get; set;}
    public String recordtypename {get; set;}
    public List<SelectOption> statusOptions {get;set;}
    public List<SelectOption> statusOptions_ECP {get;set;}
    
     //Added for KEF Products Jan Relesae BY TCS Offshore
    public priceBookEntry[] KEFProducts {get;set;}
    public KEF_Product__c[] lstproduct {get;set;}
    public Boolean Selectedprimaryproduct {get; set;}
    public opportunityLineItem[] ExistProduct {get;set;}
    public List<SelectOption> Indexused {get;set;}
    public opportunityLineItem[] CheckPrimaryProduct {get;set;}
    public Id opptyId;
    Public boolean showerror{get;set;}
    
    // Initialising the properties in Constructor
  public OpportuntunityProductExtension(ApexPages.StandardController controller){
    Selectedprimaryproduct=true;
    theOpp= new Opportunity();
    showerror=false;
    opptyId = ApexPages.currentPage().getParameters().get('Id');
    // Getting the details of the opportunity in context*** Added Vendor Agreement for KEF Opportunity**
   theOpp=[select Id,RecordTypeid,Name,Vendor_Agreement__r.Name, Vendor_Agreement__r.Agreement_Name__c,Pricebook2Id, PriceBook2.Name from Opportunity WHERE id =:opptyId LIMIT 1];
    
    // Getting the Opportunity recordtype in the context
     recordTypeID =[Select Id,SobjectType,Name From RecordType where id =:theOpp.recordTypeid limit 1]; 
     recordtypename =recordTypeID.Name;
     system.debug('recordtypename'+recordtypename);
   
    //  Querying the list of products of family equals RecordTypeName
    if(recordtypename!='KCDC')
        AvailableProducts = [select Id, Name,Pricebook2Id, IsActive, Product2.Name, Product2.Family,Product2.Product_Category__c,Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where PriceBookEntry.Product2.Family = :recordtypename and IsActive = true  order by Priorty__c asc NULLS last , Product2.Name asc limit 100 ];
      else
       AvailableProducts = [select Id, Name,Pricebook2Id, IsActive, Product2.Name, Product2.Family,Product2.Product_Category__c,Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where PriceBookEntry.Product2.Family = :recordtypename and IsActive = true  order by Product2.Product_Category__c asc limit 100 ];
       
    //Added query as part of jan Release KEF Oppty by TCS Offshore
    lstproduct=[Select id, Product_Family__c, Product_Category__c, Product_Association__c, Agreement__c,product__c from KEF_Product__c where   Agreement__c=:theOpp.Vendor_Agreement__c];
    system.debug('lstproduct--'+lstproduct);
    list<id> lstprodid=new list<id>();
        for(KEF_Product__c l: lstproduct){
            if(l.product__c != null)
                lstprodid.add(l.product__c);
        }
    system.debug('size of list---'+lstprodid.size());
    if(lstproduct!=null && lstproduct.size() > 0 ){
        KEFProducts=[select id,Name,Pricebook2Id, IsActive, Product2.Name, Product2.Family,Product2.Product_Category__c,product2id from PricebookEntry where product2id IN :lstprodid];
    }
    else if(recordtypename=='KEF Opportunity'){
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,system.label.Products));
    }
    system.debug('productvalue1----'+KEFProducts);
    system.debug('productvalue2----'+KEF_Product__c.Agreement__c +' = ' +theOpp.Vendor_Agreement__c);
    //Query To fetch existing products
    ExistProduct=[select Id, PriceBookEntryId, PriceBookEntry.Name,Primary_Product__c, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.PriceBook2Id from opportunityLineItem where OpportunityId=:theOpp.Id];
    system.debug('shoppingCar1t----'+ExistProduct);
    
    CheckPrimaryProduct=[select id, PriceBookEntryId,PriceBookEntry.Product2.Product_Category__c, PriceBookEntry.Name,Primary_Product__c, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.PriceBook2Id from opportunityLineItem where OpportunityId=:theOpp.Id and Primary_Product__c=true];
    system.debug('checkproductt----'+CheckPrimaryProduct.size());
    //end of jan release code

    shoppingCart = [select Id, Quantity, TotalPrice, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name,PriceBookEntry.PriceBook2Id from opportunityLineItem where OpportunityId=:toSelect];
    system.debug('shoppingCart----'+shoppingCart);
    
    statusOptions = new List<SelectOption>();


     // setting default value for product status
     
    Schema.DescribeFieldResult statusFieldDescription =  opportunityLineItem.Status_credit__c.getDescribe();          
        for (Schema.Picklistentry  picklistEntry: statusFieldDescription .getPicklistValues()){
        statusOptions.add(new SelectOption( pickListEntry.getValue(),pickListEntry.getLabel()));        
       }
        statusOptions_ECP= new List<SelectOption>();

        Schema.DescribeFieldResult statusFieldDescription_ECP =  opportunityLineItem.Status__c.getDescribe();          
        for (Schema.Picklistentry  picklistEntry: statusFieldDescription_ECP .getPicklistValues()){
        statusOptions_ECP.add(new SelectOption( pickListEntry.getValue(),pickListEntry.getLabel()));        
       } 
       
       //Added for KEF PicklistEntry Value For JanuaryRelease for Indexused field
       Indexused= new List<SelectOption>();
       Schema.DescribeFieldResult ProductTypeFieldDescription =  opportunityLineItem.Index_Used__c.getDescribe();          
        for (Schema.Picklistentry  picklistEntry: ProductTypeFieldDescription .getPicklistValues()){
        Indexused.add(new SelectOption( pickListEntry.getValue(),pickListEntry.getLabel()));        
       } 
 }
 
 
 // Search for the user keyed in product name in the Search field
 
    public void updateAvailableList(){
 
    String qString = 'select Id, Name,Pricebook2Id, IsActive, Product2.Name, Product2.Family,Product2.Product_Category__c,Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where PriceBookEntry.Product2.Family = :recordtypename and IsActive = true ';
    system.debug('qString:---' +qString );  

        if(searchString!=null){

            qString+= ' and '+SearchFilter+' like \'%' + searchString + '%\' order by Priorty__c asc NULLS last , Product2.Name asc limit 100  ';

        }

        system.debug('qString:' +qString );        
        AvailableProducts = database.query(qString);
    }
   
    public void addToShoppingCart(){
       
        // This function runs when a user hits "select" button next to a product
    
        for(PricebookEntry d : AvailableProducts){
            if((String)d.Id==toSelect){
                shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice));
                break;
                
            }
        }
         
        // isdisabled= true;
    }
    
    
   //Added for jan Release for KEF opty by TCS OFFSHORE
   public void addToShoppingCartKEF(){
        system.debug('addToShoppingCartKEF');
       for(PricebookEntry  p : KEFProducts){
            system.debug('p---'+p);
            if((String)p.Id==toSelect){
                system.debug('shoppingCart.add----');
                shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id,PriceBookEntry=p, PriceBookEntryId=p.Id,Product_Type__c=P.name));
               
               break;
                
            }
        }
       
    }
    public PageReference onSave(){
    
    //***Added Code For SAVE THE value for PRIMARY PRODUCT req for jan Release By TCS OffSHORE***
        system.debug('value---'+ExistProduct.size());
        system.debug('shoppingCartELSEif1---'+shoppingCart[0].Primary_Product__c);
         system.debug('shoppingCartELSEif1---'+shoppingCart[0].name);
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
        //Check if the list of product is empty so the 1st product which is created bydefault make it as PRIMARY Product
            if((ExistProduct==null || ExistProduct.size()==0)  && (recordtypename=='KEF Opportunity' || recordtypename=='KEF Oppty - Submitted')){
                system.debug('value---'+ExistProduct.size());
                shoppingCart[0].Primary_Product__c=true;
                 system.debug('valueCARTIF---'+shoppingCart[0].Primary_Product__c);
                    
            }
            //Check if the product list having some esisting products and user not chked the primary checkbox then save as it is
            else if((ExistProduct != null || ExistProduct.size() >0 ) && shoppingCart[0].Primary_Product__c == false)
                system.debug('valueCARTELSEif---'+shoppingCart[0].Primary_Product__c);
             //check if the product list having values with primaryproduct true make it false and add newly created producta s primary   
            else if((ExistProduct != null || ExistProduct.size() >0 ) && shoppingCart[0].Primary_Product__c == true){
                CheckPrimaryProduct[0].Primary_Product__c=false;
                system.debug('valueCARTELSEif1---'+CheckPrimaryProduct[0].Primary_Product__c);
                system.debug('shoppingCartELSEif1---'+shoppingCart[0].Primary_Product__c);
                //shoppingCart.add(CheckPrimaryProduct[0]);
                update CheckPrimaryProduct[0];
            }
            System.Debug('shoppingCart:'+shoppingCart.size());
            upsert(shoppingCart);
            showerror=false;
            
        }
        catch(DMLException e){  
            system.debug('Exception:'+e.getStackTraceString());
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
            showerror=true;
            return null;
        } 
       
       // return new PageReference('/'+ ApexPages.currentPage().getParameters().get('Id'));        
       return null;
    }
    
    
    
   
    public PageReference saveAndNew(){
    system.debug('Method Entry--');
    system.debug('shoppingCartELSEif1---'+shoppingCart[0].Primary_Product__c);
    try{
           // upsert(shoppingCart);
           
            if((ExistProduct==null || ExistProduct.size()==0) && (recordtypename=='KEF Opportunity' || recordtypename=='KEF Oppty - Submitted')){
                system.debug('value---'+ExistProduct.size());
                shoppingCart[0].Primary_Product__c=true;
                 system.debug('valueCARTIF---'+shoppingCart[0].Primary_Product__c);
                    
            }
            else if((ExistProduct != null || ExistProduct.size() >0 ) && shoppingCart[0].Primary_Product__c == false)
                system.debug('valueCARTELSEif---'+shoppingCart[0].Primary_Product__c);
            else if((ExistProduct != null || ExistProduct.size() >0 ) && shoppingCart[0].Primary_Product__c == true){
                CheckPrimaryProduct[0].Primary_Product__c=false;
                system.debug('valueCARTELSEif1---'+CheckPrimaryProduct[0].Primary_Product__c);
                system.debug('shoppingCartELSEif1---'+shoppingCart[0].Primary_Product__c);
               // shoppingCart.add(CheckPrimaryProduct[0]);
               update CheckPrimaryProduct[0];
            }
            upsert(shoppingCart);
           
       }
         catch(DMLException e){  
                ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
                //ApexPages.addMessage(msg);
                system.debug('Exception:'+e.getmessage());
                return null;
         }  
             
   
        PageReference newpage = ApexPages.currentPage();
        system.debug('url value---'+ System.currentPageReference().getURL());
        system.debug('url value2---'+ ApexPages.currentPage().getURL());
        Id id = newpage .getParameters().get('Id');
        newpage .getParameters().clear();
        newpage .getParameters().put('Id', id);  
        newpage .setRedirect(true);
        return newpage ;  
    
    }
    
  
   public PageReference onCancel(){
    // If user hits cancel we commit no changes and return them to the Opportunity   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    } 
  
  public PageReference newProdCancel(){
   
       try{
           // upsert(shoppingCart);
           
            if((ExistProduct==null || ExistProduct.size()==0) && (recordtypename=='KEF Opportunity' || recordtypename=='KEF Oppty - Submitted')){
                system.debug('value---'+ExistProduct.size());
                shoppingCart[0].Primary_Product__c=true;
                 system.debug('valueCARTIF---'+shoppingCart[0].Primary_Product__c);
                    
            }
            else if((ExistProduct != null || ExistProduct.size() >0 ) && shoppingCart[0].Primary_Product__c == false)
                system.debug('valueCARTELSEif---'+shoppingCart[0].Primary_Product__c);
            else if((ExistProduct != null || ExistProduct.size() >0 ) && shoppingCart[0].Primary_Product__c == true){
                CheckPrimaryProduct[0].Primary_Product__c=false;
                system.debug('valueCARTELSEif1---'+CheckPrimaryProduct[0].Primary_Product__c);
                system.debug('shoppingCartELSEif1---'+shoppingCart[0].Primary_Product__c);
               // shoppingCart.add(CheckPrimaryProduct[0]);
               update CheckPrimaryProduct[0];
            }
            upsert(shoppingCart);
           
       }
         catch(DMLException e){  
                ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
                //ApexPages.addMessage(msg);
                system.debug('Exception:'+e.getmessage());
                return null;
         }  
         
          return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));   
      } 
   
  }