/******************************************************************************************************************************************
* @name         :   ECCP_MCA_WSIN_Service
* @description  :   This class is responsible for calling MCA inbound salesforce service.
* @author       :   Nagarjun
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
global class ECCP_MCA_WSIN_Service{
    webservice static ECCP_MCA_WSIB_Response loanRequest(ECCP_MCA_WSIB_Request loanReqInfo){
        ECCP_MCA_WSIB_Response McaResponse = new ECCP_MCA_WSIB_Response();  
        McaResponse= ECCP_McaLoanProcessUpdatelogic.updateLoanReq(loanReqInfo); 
        // Commented by Aditya
       /* ECCP_PerformanceLog__c plog;     
          if(loanReqInfo <> NULL){
            McaResponse= ECCP_McaLoanProcessUpdatelogic.updateLoanReq(loanReqInfo);  
            plog = ECCP_UTIL_PerformanceLog.createPerformanceLog(ECCP_IntegrationConstants.MCAInService, ECCP_IntegrationConstants.InBound,ECCP_IntegrationConstants.MCAProcessRequest ,JSON.Serialize(loanReqInfo));          
          }else{
            ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.MCAInService, ECCP_IntegrationConstants.MCAInService, ECCP_IntegrationConstants.MCAProcessRequest , ECCP_IntegrationConstants.NullValue ,ECCP_IntegrationConstants.InboundNullInput, ECCP_IntegrationConstants.NullValue,ECCP_IntegrationConstants.NullValue);
            McaResponse.sfdcResponse = ECCP_IntegrationConstants.InboundNullInput;
            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
        }
        ECCP_UTIL_PerformanceLog.updatePerformanceLog(plog, JSON.serialize(McaResponse));
        */
        return McaResponse;        
    }
}