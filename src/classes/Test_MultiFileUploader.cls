/*
	This class is created for KEF project by TCS Team.
	Test class created for Multiple File Upload Functionality
*/
@isTest
private class Test_MultiFileUploader 
{
	
    static testMethod void MultiUpload()
   {
	    Agreement__c agg=TestUtilityClass.CreateTestAgreement();
	    insert agg;
	    string parentId=agg.id;
	    Attachment atts=TestUtilityClass.CreateTestAttachment(parentId);	    
	    
	    PageReference myVfPage = Page.MultiUploadAttachment;
	    Test.setCurrentPageReference(myVfPage); // use setCurrentPageReference,
	  
	    ApexPages.currentPage().getParameters().put('id',agg.id);
	    
	    MultiFileUploader objfileuploader=new MultiFileUploader();
	    objfileuploader.allFileList.add(atts);
	    objfileuploader.fetchExistingAttachments();
	    objfileuploader.Add();
	    objfileuploader.SelectAll();
	    objfileuploader.SaveAttachments();
	    objfileuploader.Cancel();
	    objfileuploader.DeleteAttachments();
	    
	    system.assertEquals(true,[select id from Attachment where parentId=:agg.id].size() > 0);  
    
    }
     static testMethod void MultiUploadWrappercheck()
    {
	     Agreement__c agg=TestUtilityClass.CreateTestAgreement();
	     insert agg;
	     string parentId=agg.id;
	     Attachment atts=TestUtilityClass.CreateTestAttachment(parentId);
	     insert atts;
	    // system.assertEquals(atts.id, [select Id,name,body from Attachment where Id = :atts.id]);
	   	 
	   	 MultiFileUploader.WrapperClass objfileuploaderwrapper = new MultiFileUploader.WrapperClass(atts);
	    
	     list<MultiFileUploader.WrapperClass> wrapperlist = new list<MultiFileUploader.WrapperClass>();
	     wrapperlist.add(objfileuploaderwrapper);
 
	    PageReference myVfPage = Page.MultiUploadAttachment;
	    Test.setCurrentPageReference(myVfPage); // use setCurrentPageReference,
	  
	    ApexPages.currentPage().getParameters().put('id',agg.id);
	    String paramId = ApexPages.currentPage().getParameters().get('id');
	    system.assertEquals(true,paramId!=null);
	  
	    ApexPages.StandardController sc = new ApexPages.StandardController(agg);
	    MultiFileUploader objfileuploadercheck=new MultiFileUploader();
	
	   	for(MultiFileUploader.wrapperclass listwrapp : objfileuploadercheck.wrapperList)
	   	{
			listwrapp.checked=true;
			system.assertEquals(true,listwrapp.checked);
	    }
	    
	    objfileuploadercheck.listcount='5';
	    objfileuploadercheck.Add();
	    objfileuploadercheck.allBool=true;
	    objfileuploadercheck.selectAll();
	    objfileuploaderwrapper.checked=true;
	    objfileuploadercheck.selectAll();
	    objfileuploadercheck.DeleteAttachments();
	    objfileuploadercheck.SaveAttachments();
	     
	    MultiFileUploader objfileuploadercheck1=new MultiFileUploader(); 
	    MultiFileUploader.WrapperClass objfileuploaderwrapper1 = new MultiFileUploader.WrapperClass(atts);
	    objfileuploaderwrapper1.checked=false;
	    objfileuploadercheck1.allBool=false;
	    objfileuploadercheck1.selectAll();
	    objfileuploadercheck1.DeleteAttachments();
	    objfileuploadercheck1.SaveAttachments();
	 }
}