@isTest
public class TRO_AsyncAddServiceRequest_Mock implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        // Create response element from the autogenerated class.
        // Populate response element.
        // Add response element to the response parameter, as follows:
        AsyncTROAddServiceRequest_v2.processAddServiceAPIResponseFuture responseElement = TROUnitTestFactory.buildTestProcessAddServiceAPIResponseFuture();
        //responseElement.processCSULResponse.CSOAutomation.KTTResponse.status = 'Good to go';
        response.put('response_x', responseElement);
   }
}