/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest //(SeeAllData=true)
private class ScenarioTriggerHandler_UT {

 @testSetup
  static void setup()
  {
  //   Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
  //     User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
  //     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
  //     LocaleSidKey='en_US', ProfileId = p.Id,
  //     TimeZoneSidKey='America/Los_Angeles', UserName='sysadmin@keyorg.com', PLOB__c = 'Support');
  //
  // System.runAs(u) {
        Account acc = UnitTestFactory.buildTestAccount();
        acc.KBCM_Customer_Number__c = '98765';
        acc.Name = 'Test Account';
        acc.Average_Deposit_Balance__c = 50;
        acc.Service_Charges__c = 50;
        insert acc;
        System.debug('accRecTest::'+[SELECT id, recordTypeId FROM Account WHERE Id =:acc.id]);
        Id accId = acc.Id;
        System.debug(acc.Name);

        Opportunity o = UnitTestFactory.buildTestOpportunity();
        o.AccountId = acc.Id;
        o.Name = 'Scenario Test';

        Opportunity o2 = UnitTestFactory.buildTestOpportunity();
        o.AccountId = acc.Id;
        o.Name = 'Scenario Test2';

        insert new List<Opportunity>{o2, o};

        Id oppId = o.Id;
        Id oppId2 = o2.Id;

        List<Product2> prods = new List<Product2>();
        for (Integer i=0; i<3; i++)
        {
            Product2 prod = UnitTestFactory.buildTestProduct();
            prod.Name = 'Scenario Test' + i;
            prod.Family = 'Deposits & ECP';
            prods.add(prod);
        }
        insert prods;

        List<OpportunityLineItem> opprods = new List<OpportunityLineItem>();
        List<PricebookEntry> pbes = new List<PricebookEntry>();
        for (Product2 prod: prods)
        {
            PricebookEntry pbe = UnitTestFactory.buildTestPricebookEntry(prod);
            pbes.add(pbe);
        }
        insert pbes;

        List<Id> pbeIds = new List<Id>();
        for(PricebookEntry pbe: pbes)
        {
            pbeIds.add(pbe.Id);
        }

        for (Id pbeId: pbeIds)
        {
            OpportunityLineItem opprod = new OpportunityLineItem(OpportunityId=oppId, PricebookEntryId=pbeId);
            opprods.add(opprod);
        }
        for (Id pbeId: pbeIds)
        {
            OpportunityLineItem opprod = new OpportunityLineItem(OpportunityId=oppId2, PricebookEntryId=pbeId);
            opprods.add(opprod);
        }
        insert opprods;

        List<LLC_BI__Scenario__c> scenarios = new List<LLC_BI__Scenario__c>();
        Id scenarioRecordTypeId = Schema.SObjectType.LLC_BI__Scenario__c.getRecordTypeInfosByName().get('DDA Proforma').getRecordTypeId();
        LLC_BI__Scenario__c scenario = new LLC_BI__Scenario__c(recordTypeId=scenarioRecordTypeId, name='ScenarioOppProdStatus1', LLC_BI__Opportunity__c=o.id,
        Deposit_Balance__c=100.00, ServiceCharges_Eligible_4_EarningsCredit__c=100, EscapeFlow__c = true);
        LLC_BI__Scenario__c scenario2 = new LLC_BI__Scenario__c(recordTypeId=scenarioRecordTypeId, name='ScenarioOppProdStatus2', LLC_BI__Opportunity__c=o2.id,
        Deposit_Balance__c=100.00, ServiceCharges_Eligible_4_EarningsCredit__c=100, EscapeFlow__c = true);
        scenarios.add(scenario);
        scenarios.add(scenario2);
        insert scenarios;
        Id scenId = scenario.Id;
        Id scenId2 = scenario2.Id;

    LLC_BI__Bill_Point__c bpTemp = new LLC_BI__Bill_Point__c();
    bpTemp.Name = 'Test';
    bpTemp.AFP_Service_Code__c = '10';
    bpTemp.AFP_Service_Code_Name__c = 'Test';
    bpTemp.AFP_Service_Description__c = 'Test';
    insert bpTemp;

    LLC_BI__Scenario_Item__c scTemp = new LLC_BI__Scenario_Item__c();
    scTemp.Name = 'Test';
    scTemp.LLC_BI__Scenario__c = scenId;
    scTemp.LLC_BI__Product__c = 'Random';
    scTemp.LLC_BI__Standard_Price__c = 100;
    scTemp.LLC_BI__Bill_Point__c = bpTemp.Id;
    scTemp.EscapeFlow__c = true;
    insert scTemp;

        LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
        insert pl;
        LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
        insert pt;

        List<LLC_BI__Product__c> nps = new List<LLC_BI__Product__c>();
        for (Product2 prod: prods)
        {
            LLC_BI__Product__c np = UnitTestFactory.buildTestNCinoProduct(prod, pt);
            np.Core_Treasury_Product__c = true;
            nps.add(np);
        }
        insert nps;

        List<Scenario_Product__c> scenProds = new List<Scenario_Product__c>();
        for (LLC_BI__Product__c np: nps)
        {
            Scenario_Product__c scen = new Scenario_Product__c(Product__c = np.Id, Scenario__c=scenId, Total_Incremental_Bookable_Revenue__c=50.00);
            scenProds.add(scen);
        }
        for (LLC_BI__Product__c np: nps)
        {
            Scenario_Product__c scen = new Scenario_Product__c(Product__c = np.Id, Scenario__c=scenId2, Total_Incremental_Bookable_Revenue__c=0);
            scenProds.add(scen);
        }
        insert scenProds;
        System.debug(scenProds);

        
//  }
  }

  /* static testMethod void updateOpportunityRevenueTest()
    {
        //Scenario_Product__c scp1 = [select id, Scenario__r.Id, Product__c, Scenario__c, Total_Incremental_Bookable_Revenue__c, Total_Product_Revenue__c,
        //  Name, Fee_Equivalent_Revenue__c from Scenario_Product__c  LIMIT 1];
        LLC_BI__Scenario__c scn = [select id, LLC_BI__Opportunity__r.Id, LLC_BI__Status__c from LLC_BI__Scenario__c where LLC_BI__Status__c = 'Final' LIMIT 1];
        //scn.LLC_BI__Status__c
        ScenarioTriggerHandler th = new ScenarioTriggerHandler();
        List<LLC_BI__Scenario__c> scns = new List<LLC_BI__Scenario__c>();
        scns.add(scn);
        th.updateOpportunityRevenue(scns, null);

    }*/

/*  static testMethod void testScenarioProductBeforeInsertTrigger()
    {
        List<OpportunityLineItem> oppordList = [select OpportunityId from OpportunityLineItem];
        List<Id> oppProdIds = new List<Id>();
        for (OpportunityLineItem op: oppordList)
            oppProdIds.add(op.OpportunityId);


        LLC_BI__Scenario__c scn = [select id, LLC_BI__Opportunity__r.Id, LLC_BI__Status__c from LLC_BI__Scenario__c where LLC_BI__Status__c = 'Draft'
        and LLC_BI__Opportunity__r.Id <> NULL and LLC_BI__Opportunity__r.Id in :oppProdIds LIMIT 1];
        scn.LLC_BI__Status__c = 'Final' ;

        update scn;

    //  Scenario_Product__c scp = new Scenario_Product__c(Name=' AJ TEST 1', Product__c = prod.Id, Scenario__c = scenario.Id);
    //  insert scp;
    } */

    static testMethod void testOppProdStatusWon()
    {
        LLC_BI__Scenario__c scn = [select id, LLC_BI__Opportunity__r.Id, LLC_BI__Status__c from LLC_BI__Scenario__c where Name='ScenarioOppProdStatus1' ];
        scn.LLC_BI__Status__c = 'Final';
        Test.startTest();

        update scn;
        Test.stopTest();
       List<OpportunityLineItem> oppList = [select Product2.Id, Id, Total_Proforma_Revenue__c, Status__c from OpportunityLineItem where OpportunityId = :scn.LLC_BI__Opportunity__r.Id] ;
       for (OpportunityLineItem oppa: oppList)
       {
      //    System.assertEquals('Won', oppa.Status__c);
     //     System.assertEquals(50, oppa.Total_Proforma_Revenue__c);
       }
    }

    static testMethod void testOppProdStatusLost()
    {
        LLC_BI__Scenario__c scn2 = [select id, LLC_BI__Opportunity__r.Id, LLC_BI__Status__c from LLC_BI__Scenario__c where Name='ScenarioOppProdStatus2' ];
        scn2.LLC_BI__Status__c = 'Final';
        Test.startTest();
        update  scn2;
        Test.stopTest();

       List<OpportunityLineItem> opp2List = [select Product2.Id, Id, Total_Proforma_Revenue__c, Status__c from OpportunityLineItem where OpportunityId = :scn2.LLC_BI__Opportunity__r.Id] ;

        for (OpportunityLineItem oppa: opp2List)
        {
            System.assertEquals('Lost', oppa.Status__c);
      //        System.assertEquals(0, oppa.Total_Proforma_Revenue__c);
        }

    }

    static testMethod void testOppServiceChargesUpdate()
    {
        LLC_BI__Scenario__c scn = [select id, LLC_BI__Opportunity__r.Id, LLC_BI__Opportunity__r.Incremental_Deposit_Balance__c,
         LLC_BI__Opportunity__r.Incremental_Service_Charges__c, LLC_BI__Status__c from LLC_BI__Scenario__c where Name='ScenarioOppProdStatus1' ];
        scn.LLC_BI__Status__c = 'Final';
            Test.startTest();
        update scn;
            Test.stopTest();

        Opportunity opp = [select id, Incremental_Deposit_Balance__c, Incremental_Service_Charges__c from Opportunity
        where Id = :scn.LLC_BI__Opportunity__r.Id];
        System.debug('******Scenario: ' + opp.Incremental_Deposit_Balance__c);
        System.debug('******Scenario: ' + opp.Incremental_Service_Charges__c);
        System.assertEquals(50, opp.Incremental_Deposit_Balance__c);
        System.assertEquals(-50, opp.Incremental_Service_Charges__c);
    }

  static testMethod void testCreateAFP()
  {
    LLC_BI__Scenario__c scn = [select id, LLC_BI__Opportunity__r.Id, LLC_BI__Status__c from LLC_BI__Scenario__c where Name='ScenarioOppProdStatus1' ];
        scn.LLC_BI__Status__c = 'Approved';
        scn.Proforma_Approved__c = true;
    Test.startTest();
    update scn;
        Test.stopTest();
  }
}