public with sharing class SearchScenarioController {

    public String customerIPI {get; set;}
    public String requestId {get; set;}
    public String clientName {get; set;}
    public String ECPSalesAdvisor {get; set;}
    public String relationshipManager {get; set;}
    public String requestor {get; set;}
    public String boolOperator {
        get {
            return boolOperator;
        } set {
            boolOperator = String.escapeSingleQuotes(value);
        }
    }
    Boolean reload;
    static final Integer DISPLAY_COUNT = 10;

    public List<LLC_BI__Scenario__c> scenarioList {get; set;}

        public ApexPages.StandardSetController con {
        get {
            if(con == null  || reload)
            {
                if (scenarioList ==null)
                        scenarioList = new List<LLC_BI__Scenario__c>();

                con = new ApexPages.StandardSetController(scenarioList);
                // sets the number of records in each page set
                con.setPageSize(DISPLAY_COUNT);
                reload = false;
            }
            return con;
        }
        set;
    }

    public List<LLC_BI__Scenario__c> getRecords()
    {
        return (List<LLC_BI__Scenario__c>) con.getRecords();
    }

    public Pagereference  search()
    {
        String queryString;
        String fragment = '';
        String queryStringBase = 'Select OwnerId, Id, Name, Account_Name__c, Customer_IPI_Number__c, LLC_BI__Status__c, CreatedDate from LLC_BI__Scenario__c where ';
        if (String.isNotBlank(customerIPI))
        {
                customerIPI = String.escapeSingleQuotes(customerIPI);
                fragment = 'Customer_IPI_Number__c = \'' + customerIPI + '\'';
                System.debug(fragment);
        }

        if (String.isNotBlank(requestId))
        {
                if (String.isNotBlank(fragment))
                        fragment = fragment + ' ' + boolOperator + ' ';
                requestId = String.escapeSingleQuotes(requestId);
                fragment = fragment + 'Name = \'' + requestId + '\'';
                System.debug(fragment);
        }

        if (String.isNotBlank(clientName))
        {
                if (String.isNotBlank(fragment))
                        fragment = fragment + ' ' + boolOperator + ' ';
                clientName = String.escapeSingleQuotes(clientName);
                fragment = fragment + 'Account_Name__c = \'' + clientName + '\'';
                System.debug(fragment);
        }

        if (String.isNotBlank(ECPSalesAdvisor))
        {
                if (String.isNotBlank(fragment))
                        fragment = fragment + ' ' + boolOperator + ' ';
                ECPSalesAdvisor = String.escapeSingleQuotes(ECPSalesAdvisor);
                fragment = fragment + 'ECP_Payments_Manager__r.Name = \'' + ECPSalesAdvisor + '\'';
                System.debug(fragment);
        }

        if (String.isNotBlank(relationshipManager))
        {
                if (String.isNotBlank(fragment))
                        fragment = fragment + ' ' + boolOperator + ' ';
                relationshipManager = String.escapeSingleQuotes(relationshipManager);
                fragment = fragment + 'Relationship_Manager_Level_1__r.Name = \'' + relationshipManager + '\'';
                System.debug(fragment);
        }

        if (String.isNotBlank(requestor))
        {
                if (String.isNotBlank(fragment))
                        fragment = fragment + ' ' + boolOperator + ' ';
                requestor = String.escapeSingleQuotes(requestor);
                fragment = fragment + 'Owner.Name = \'' + requestor + '\'';
                System.debug(fragment);
        }

        if (String.isNotBlank(fragment))
        {
                queryString = queryStringBase + fragment;
                System.debug(queryString);
                if (scenarioList != null)
                        scenarioList.clear();
                scenarioList = Database.query(queryString);
                reload = true;

                return null;
        }
        else return null;
    }

    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }

    // returns the first page of records
     public void first() {
         con.first();
     }

     // returns the last page of records
     public void last() {
         con.last();
     }

     // returns the previous page of records
     public void previous() {
         con.previous();
     }

     // returns the next page of records
     public void next() {
         con.next();
     }

     // returns the PageReference of the original page, if known, or the home page.
     public void cancel() {
         con.cancel();
     }

     public Integer numPages
     {
        get{
                Integer pages = con.getResultSize() / DISPLAY_COUNT;
                if (Math.mod(con.getResultSize(), DISPLAY_COUNT) > 0)
                        pages++;
                return pages;
        }
        set;
     }



}