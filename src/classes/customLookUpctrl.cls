public class customLookUpctrl{

    public String contactName{get; set;}
    List<Contact> contactRecList;
    public ApexPages.StandardSetController contactSet{get;set;}
    public List<RecordType> recTypeList;
    
    public String selUserName{get; set;}
    private Final Integer PAGE_SIZE =15; //sets the page size or number of rows
    public integer total_size{get;set;} 
    List<User> userRecList = new  List<User>();
    public ApexPages.StandardSetController userSet{get;set;}
    
     public customLookUpctrl(ApexPages.StandardController controller){
        contactName = ApexPages.currentPage().getParameters().get('newContactName');
        system.debug('contactName: '+contactName);
        searchContact();
        total_size = 0;
        selUserName = ApexPages.currentPage().getParameters().get('userName');
        system.debug('selUserName: '+selUserName);
        SearchUserRecord();
        //userSet = new ApexPages.StandardSetController(userRecList); 
    }
    public void searchContact(){
        recTypeList = [SELECT Id, Name FROM RecordType WHERE Name = 'Key Employee'];
        System.debug('Key Employee:012o0000000OBtL:'+recTypeList[0].Id);
        Id recordTpId;
        if(recTypeList.size()>0)
            recordTpId = recTypeList[0].Id;
        String query = 'SELECT ID,Name, firstName, LastName, Active__c,Salesforce_User__c,recordTypeId  FROM Contact';
        system.debug('initial query : '+query);
        try{
            List<String> whereStrList = new List<String>();
            if(contactName != null && contactName != ''){
                whereStrList.add('Name LIKE \'%'+ String.escapeSingleQuotes(contactName)+'%\'');    
                //whereStrList.add('Contract_Number_New__c  LIKE \'%'+ String.escapeSingleQuotes(contractNumber)+'%\'');
            }
            if(recordTpId != null){
                whereStrList.add('recordTypeId =\''+recordTpId+'\'');  
            }
            whereStrList.add('Salesforce_User__c = true'); 
            whereStrList.add('Active__c = true'); 
            String whereQuery = '';
            if(whereStrList!=null && whereStrList.size()>0){
                for(String tempStr : whereStrList){
                    whereQuery = whereQuery + ((whereQuery=='' || whereQuery==null)?' ':' AND ') + '(' +tempStr + ')'; 
                }
                query = query + ' WHERE ' + whereQuery;
                system.debug('inside if query :'+query);
            }
            system.debug('query :'+query);
            contactSet = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        }
        catch(Exception exp){
            System.Debug('Exception Occurred:'+exp.getMessage());
        }
        
        /*
        contactRecList = new List<Contact>();
        contactRecList = [SELECT ID,Name, firstName, LastName, Active__c,Salesforce_User__c,recordTypeId  FROM Contact WHERE Active__c = true AND Salesforce_User__c = true];
        system.debug('contactRecList:'+contactRecList);*/
        //contractSet = new ApexPages.StandardSetController(Database.getQueryLocator(query));
    }
    
    public List<contact> getcontactList(){
        List<Contact> contList = new List<Contact>();
        if(contactSet != null){
            contList = (List<Contact>)contactSet.getRecords();
        }
        if(contList == null || contList.size()==0){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Records Found'); 
            ApexPages.addMessage(errMsg);
        }
        System.Debug('contList:'+contList);     
        return contList;    
    }
    public void SearchUserRecord(){
        String query = 'SELECT Id,IsActive,Name FROM User';
        system.debug('initial query : '+query);
         
        try{
            List<String> whereStrList = new List<String>();
            if(selUserName != null && selUserName != ''){
                whereStrList.add('Name LIKE \'%'+ String.escapeSingleQuotes(selUserName)+'%\'');    
            }
           // whereStrList.add('IsActive = true'); 
            String whereQuery = '';
            if(whereStrList!=null && whereStrList.size()>0){
                for(String tempStr : whereStrList){
                    whereQuery = whereQuery + ((whereQuery=='' || whereQuery==null)?' ':' AND ') + '(' +tempStr + ')'; 
                }
                query = query + ' WHERE ' + whereQuery;
                //system.debug('inside if query :'+query);
            }
            system.debug('query :'+query);
            query = query + ' LIMIT 1000';
            system.debug('query with limit:'+query);
            userSet = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            system.debug(userSet);
        }
        catch(Exception exp){
            System.Debug('Exception Occurred:'+exp.getMessage());
        }
    }
    
    public List<User> getUserList(){
        List<User> userList = new List<User>();
        //if(userSet != null && userSet.getResultSize() > 0){
        if(userSet != null){
        userList = (List<User>)userSet.getRecords();
            System.Debug('Size:'+userList.size());
        }
        if(userList == null || userList.size()==0){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Records Found'); 
            ApexPages.addMessage(errMsg);
        }
        System.Debug('userList:'+userList);     
        return userList;    
    }
    
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            if(userSet==null) return false;
            return userSet.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            if(userSet==null) return false;
            return userSet.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            if(userSet==null) return 0;
            return userSet.getPageNumber();
        }
        set;
    }

    public Integer getPageSize(){
        return PAGE_SIZE;
    }
    
    public Integer getTotalPages(){
        if(total_size == null || total_size==0) return 0;
        return Integer.valueOf(Math.ceil(Decimal.valueOf(total_size)/getPageSize()));
    }
    
    // returns the first page of records
     public void first() {
         userSet.first();
     }

     // returns the last page of records
     public void last() {
         userSet.last();
     }

     // returns the previous page of records
     public void previous() {
         userSet.previous();
     }

     // returns the next page of records
     public void next() {
         userSet.next();
     }
}