public class ECCP_Exposure_MassEditController {
    public LLC_BI__Product_Package__c productPackage {get;set;}
    public List<Exposure_Adjustment__c> exposureAdjustments {get;set;}
    public List<Schema.FieldSetMember> exposureAdjustmentListView {get;set;}
    public nForce.TemplateController template;
    
    public ECCP_Exposure_MassEditController(nForce.TemplateController controller) {
        this.template = controller;
        this.productPackage = this.getProductPackage(ApexPages.currentPage().getParameters().get(PARAMETER_NAME));
        this.exposureAdjustmentListView = this.getExposureAdjustmentListView(FIELDSETNAME);
        this.exposureAdjustments = this.getExposureAdjustments(productPackage.Id);
    }
    
    public PageReference exposureAdjustmentSave() {
        try{
            upsert exposureAdjustments;
        } catch(Exception ex) {
          	ApexPages.addMessages(ex);
        }
        
        return null;
    }
    
    public void addNew() {
        Exposure_Adjustment__c newEA = new Exposure_Adjustment__c(
            ProductPackage__c = this.productPackage.Id);
        this.exposureAdjustments.add(newEA);
    }
    
    public void cancel() {
        this.exposureAdjustments = this.getExposureAdjustments(productPackage.Id);
    }
    
    private List<Schema.FieldSetMember> getExposureAdjustmentListView(String fieldSet) {
        return SObjectType.Exposure_Adjustment__c.fieldSets.getMap().get(fieldSet).getFields();
    }
    
    private LLC_BI__Product_Package__c getProductPackage(Id recordId) {
        return [SELECT 
               		Id,
               		Name
               	FROM
               		LLC_BI__Product_Package__c
               	WHERE
               		Id = :recordId LIMIT 1];
    }
    
    private List<Exposure_Adjustment__c> getExposureAdjustments(Id recordId) {
        String query = SELECTSTATEMENT;
        for (Schema.FieldSetMember f: this.exposureAdjustmentListView) {
            query += f.fieldPath + COMMASPACE;
        }
        
        query += 'Id FROM Exposure_Adjustment__c WHERE ProductPackage__c = \'' + recordId + '\'';
        query += 'ORDER BY Name ASC';
        
        return Database.query(query);
    }
    
    private static final String PARAMETER_NAME = 'id';
    private static final String SELECTSTATEMENT = 'SELECT ';
    private static final String COMMASPACE = ', ';
    private static final String FIELDSETNAME = 'Exposure_List_View';
}