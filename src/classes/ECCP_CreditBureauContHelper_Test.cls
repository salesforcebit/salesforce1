@isTest
private class ECCP_CreditBureauContHelper_Test{
private static testmethod void CreateData(){

   Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
   User u = new User(Alias = 'stdKey', Email='standarduser@testorg.comKey', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comKey',PLOB__c='Support');
   insert u;

    list<LLC_BI__Legal_Entities__c> entList = new list<LLC_BI__Legal_Entities__c>();
   System.runAs(u){
    //string orchCode ='000111'
    /*************Create contact************/
    contact con = new contact();
    con.Lastname = 'Name';
    con.Firstname = 'Test';
    con.Birthdate = Date.valueOf('1980-01-01');
    con.SSN__c = 'TestSSN12';
    con.Phone ='1234567890';
    insert con;
    
   // RecordType rt = [SELECT id,Name FROM RecordType WHERE SobjectType='Account' AND Name='Indiviual' limit 1];
    
    /*************Account**********/
    Account a = new account();
    a.name = 'Test Account';
    a.LastName__c = 'Test Lastname';
    a.FirstName__c ='Test Firstname';
    a.BillingStreet ='test street';
    a.BillingCity ='test city';
    a.BillingState ='test state';
    a.BillingPostalCode ='302';
    a.Contact_Name__c = con.id;
  //  a.recordTypeId = rt.id;
    a.Organization_Type__c = 'Sole Proprietorship';
    a.type = 'Business Prospect';
    a.LLC_BI__Tax_Identification_Number__c ='34567890';
    a.TIN__c = '7890089';
    
    insert a;
    system.debug('---Account--'+a);
    
   /*************Product Package**********/ 
       
       LLC_BI__Product_Package__c pp = new LLC_BI__Product_Package__c();
          pp.Name = a.name;
          PP.Bank_Division__c ='Business Banking';
          pp.LLC_BI__Account__c = a.id;
          pp.LLC_BI__Stage__c = 'Application Processing';
          pp.LLC_BI__Primary_Officer__c = u.Id;
         // pp.Credit_Authority_Name__c =u.Id;
         //  pp.Delegated_Authority_Name__c  = u.Id;
      
     insert pp;
    
    /*****************Loan************/
    LLC_BI__Loan__c loan = new LLC_BI__Loan__c();
    loan.Name = 'Test Bureau';
    loan.LLC_BI__Stage__c = 'Application Processing';
    loan.LLC_BI__Account__c = a.id;
    loan.LLC_BI__Product_Package__c = pp.id;
    loan.LLC_BI__Amount__c = 5678000;
    loan.ECCP_Application_Number__c = '90067';
    loan.ECC_Rate_Description__c ='00170 Akron Stmt Sav';
    insert loan;
    
    
    /*********Legal Entity**************/
    for(integer i=0;i<5;i++){
    LLC_BI__Legal_Entities__c ent = new LLC_BI__Legal_Entities__c();
    ent.name ='Entity'+i;
    ent.LLC_BI__Loan__c = loan.id;
    ent.LLC_BI__Account__c = a.id;
    ent.LLC_BI__Tax_ID__c ='1234567890';
    ent.LLC_BI__Product_Package__c = pp.id;
    ent.LLC_BI__Borrower_Type__c = 'Borrower';
    entList.add(ent);
    system.debug('!!!!!!!!!!!'+entlist);
          }
          insert entList;
          
     /*********Credit Bureau**************/       
          ECCP_Credit_Bureau__c Cb = new ECCP_Credit_Bureau__c();
          cb.Loan__c = loan.id;
          cb.Service_Call_Flag__c =True;
          insert cb;
          
          test.startTest();
                   ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port service = new ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port();
service.endpoint_x  = 'http://api.salesforce.co/';
          ECCP_CreditBureauResponse res1 = new ECCP_CreditBureauResponse();
          res1 = ECCP_CreditBureau_WebSvcCalloutTest.CBTest();
         apexpages.currentpage().getparameters().put('LoanId' , Loan.Id);
         Apexpages.StandardController stdController = new Apexpages.StandardController(Loan);
         ECCP_CreditBureauController Controller = new ECCP_CreditBureauController(stdController);
         Controller.Search();
        
         Controller.ClosePage();
         
         ECCP_CreditBureauContHelper.responseWrapper reqW = new ECCP_CreditBureauContHelper.responseWrapper();
         List<ECCP_CreditBureauContHelper.responseWrapper> reqWList = new List<ECCP_CreditBureauContHelper.responseWrapper>();
         reqW.resObj = res1;
         reqW.loanId = loan.Id;
         reqW.entityId = entlist[0].Id;
         reqW.acctName = a.Name;
         reqWList.add(reqW);
         ECCP_CreditBureauContHelper.processResponse(reqWList);
         
          test.stopTest();
      
          }
       }   
         
  private static testmethod void CreateData1(){
  
  Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
   User u1 = new User(Alias = 'stdKey', Email='standarduser@testorg.comKey', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comKey',PLOB__c='Support');
   insert u1;
  
  list<LLC_BI__Legal_Entities__c> entList = new list<LLC_BI__Legal_Entities__c>();

   System.runAs(u1){ 
    
    string orchCode ='000111';
    
    contact con1 = new contact();
    con1.Lastname = 'name';
    con1.Firstname = 'Test';
    con1.Birthdate = Date.valueOf('1980-01-01');
    con1.SSN__c = 'TestSSN13';
    con1.Phone ='1234567890';
    insert con1;
    
  //  RecordType rt = [SELECT id,Name FROM RecordType WHERE SobjectType='Account' AND Name='Business' limit 1];
    
    
    Account a = new account();
    a.name = 'Test Account';
    a.BillingStreet ='test street';
    a.BillingCity ='test city';
    a.BillingState ='test state';
    a.BillingPostalCode ='302';
    a.Contact_Name__c = con1.id;
   // a.recordTypeId = rt.id;
    a.type = 'Business Prospect';
    a.Organization_Type__c ='Sole Proprietorship' ;
    a.LLC_BI__Tax_Identification_Number__c ='34567890';
    a.Tin__c = '5676869';
    insert a;
    system.debug('---Account--'+a);
    
    /****************ProductPackage************/
     LLC_BI__Product_Package__c pp = new LLC_BI__Product_Package__c();
          pp.Name = a.name;
          PP.Bank_Division__c ='Business Banking';
          pp.LLC_BI__Account__c = a.id;
          pp.LLC_BI__Stage__c = 'Application Processing';
          pp.LLC_BI__Primary_Officer__c = u1.Id;
         // pp.Credit_Authority_Name__c =u.Id;
         //  pp.Delegated_Authority_Name__c  = u.Id;
      
     insert pp;
    
    
    /*********Loan**************/
    LLC_BI__Loan__c loan = new LLC_BI__Loan__c();
    loan.Name = 'Test Bureau';
    loan.LLC_BI__Stage__c = 'Application Processing';
    loan.LLC_BI__Account__c = a.id;
    loan.LLC_BI__Product_Package__c = pp.id;
    loan.LLC_BI__Amount__c = 5678000;
    loan.ECCP_Application_Number__c = '90067';
    loan.ECC_Rate_Description__c ='00170 Akron Stmt Sav';    
    insert loan;
    
    
    
    for(integer i=0;i<5;i++){
    LLC_BI__Legal_Entities__c ent = new LLC_BI__Legal_Entities__c();
    ent.name ='Entity'+i;
    ent.LLC_BI__Loan__c = loan.id;
    ent.LLC_BI__Account__c = a.id;
    ent.LLC_BI__Tax_ID__c ='1234567890';
    ent.LLC_BI__Product_Package__c = pp.id;
    ent.LLC_BI__Borrower_Type__c = 'Borrower';
    entList.add(ent);
    system.debug('!!!!!!!!!!!'+entlist);
          }
          
          insert entList;
          
          
          ECCP_Credit_Bureau__c Cb = new ECCP_Credit_Bureau__c();
        
          cb.Loan__c = loan.id;
          cb.Service_Call_Flag__c =True;
          insert cb;
          
          test.startTest();
          ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port service = new ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port();
service.endpoint_x  = 'http://api.salesforce.co/';
          ECCP_CreditBureauResponse res1 = new ECCP_CreditBureauResponse();
           res1 = ECCP_CreditBureau_WebSvcCalloutTest.CBTest();
            system.debug('++++++res1' + res1);         
      
         apexpages.currentpage().getparameters().put('LoanId' , Loan.Id);
         Apexpages.StandardController stdController = new Apexpages.StandardController(Loan);
         ECCP_CreditBureauController Controller = new ECCP_CreditBureauController(stdController);
         Controller.Search();
        
         Controller.ClosePage();
        
          
       }
      } 
       
      /* private static testmethod void CreateData2(){
          list<LLC_BI__Legal_Entities__c> entList = new list<LLC_BI__Legal_Entities__c>();

    
    
    string orchCode ='000111';
    
    contact con = new contact();
    con.Lastname = 'Name';
    con.Firstname = 'Test';
    insert con;
    
 //   RecordType rt = [SELECT id,Name FROM RecordType WHERE SobjectType='Account' AND Name='Individual' limit 1];
    
    
    Account a = new account();
    a.name = 'Test Account';
    a.BillingStreet ='test street';
    a.BillingCity ='test city';
    a.BillingState ='test state';
    a.Contact_Name__c = con.id;
    a.Organization_Type__c = 'Partnership';
  //  a.recordTypeId = rt.id;
    a.type = 'Business Prospect';
    //a.LLC_BI__Tax_Identification_Number__c ='34567890';
    
    insert a;
    system.debug('---Account--'+a);
    
    
    LLC_BI__Loan__c loan = new LLC_BI__Loan__c();
    loan.Name = 'Test Bureau';
    //loan.LLC_BI__Stage__c = 'Application Processing';
    loan.LLC_BI__Account__c = a.id;
    insert loan;
    
    
    
    for(integer i=0;i<5;i++){
    LLC_BI__Legal_Entities__c ent = new LLC_BI__Legal_Entities__c();
    ent.name ='Entity'+i;
    ent.LLC_BI__Loan__c = loan.id;
    ent.LLC_BI__Account__c = a.id;
    entList.add(ent);
    system.debug('!!!!!!!!!!!'+entlist);
          }
          
          insert entList;
          ECCP_Credit_Bureau__c Cb = new ECCP_Credit_Bureau__c();
        
          cb.Loan__c = loan.id;
          cb.Service_Call_Flag__c =True;
          insert cb;
          
          test.startTest();
          ECCP_CreditBureauResponse res1 = new ECCP_CreditBureauResponse();
           res1 = ECCP_CreditBureau_WebSvcCalloutTest.CBTest();
                     
      
         apexpages.currentpage().getparameters().put('LoanId' , Loan.Id);
         Apexpages.StandardController stdController = new Apexpages.StandardController(Loan);
         ECCP_CreditBureauController Controller = new ECCP_CreditBureauController(stdController);
         Controller.Search();
        
         Controller.ClosePage();
       
     }*/
     
     }