public with sharing class ScenarioTriggerHandler {

    public void handleBeforeInsert(List<LLC_BI__Scenario__c> triggerNewList, Map<Id, LLC_BI__Scenario__c> triggerNewMap)
    {
        //setIncrementalValue(triggerNewList, triggerNewMap);
    }

    public void handleBeforeUpdate(List<LLC_BI__Scenario__c> triggerNewList, Map<Id, LLC_BI__Scenario__c> triggerNewMap, Map<Id, LLC_BI__Scenario__c> triggerOldMap )
    {
        updateOpportunityRevenue(triggerNewList, triggerNewMap, triggerOldMap);
    }

        public void handleAfterUpdate(List<LLC_BI__Scenario__c> triggerNewList)
    {
            System.debug('Update scenario');
            createAFP(triggerNewList);
    }

    @TestVisible
    private void updateOpportunityRevenue(List<LLC_BI__Scenario__c> triggerNewList, Map<Id, LLC_BI__Scenario__c> triggerNewMap, Map<Id, LLC_BI__Scenario__c> triggerOldMap)
    {
        List<Product2> prodIds;
        List<Scenario_Product__c> scenProdList;
        List<Id> scenIds = new List<Id>();
        List<LLC_BI__Product__c> nCinoprods = new List<LLC_BI__Product__c>();
        List<Id> oppIds =  new List<Id>();
        List<LLC_BI__Scenario__c> scenList = new List<LLC_BI__Scenario__c>();
        List<OpportunityLineItem> oppProducts;
        Map<Id, Scenario_Product__c > prodScp = new Map<Id, Scenario_Product__c >();
        Map<Id, Id > ncinoNativeProd = new Map<Id, Id >();
        List<OpportunityLineItem> updOppProd = new List<OpportunityLineItem>();
        Map<Id, LLC_BI__Scenario__c > oppToScenarioMap = new Map<Id, LLC_BI__Scenario__c >();

        for (LLC_BI__Scenario__c scp: triggerNewList)
        {
            if (scp.LLC_BI__Status__c == 'Final' && scp.LLC_BI__Status__c != triggerOldMap.get(scp.id).LLC_BI__Status__c)
            {
                scenIds.add(scp.Id);
                System.debug('Opp id is: ' + scp.LLC_BI__Opportunity__c);
                oppIds.add(scp.LLC_BI__Opportunity__c);
                oppToScenarioMap.put(scp.LLC_BI__Opportunity__c, scp);
            }
        }

        if (scenIds.size() > 0)
        {

            updateOpportunities(oppIds, scenIds, oppToScenarioMap);


            scenProdList = new List<Scenario_Product__c>([select Id, Scenario__r.Id, Product__r.Id, Product__r.Related_SFDC_Product__r.Id, Product__r.Core_Treasury_Product__c,
            Total_Incremental_Bookable_Revenue__c, fee_equivalent_revenue__c, Name from Scenario_Product__c
            where Scenario__r.Id in :scenIds]);

            for (Scenario_Product__c scp : scenProdList)
                System.debug('Scenario Products: '+ scp);

            for (Scenario_Product__c scenp: scenProdList)
            {
                ncinoNativeProd.put(scenp.Product__r.Related_SFDC_Product__r.Id, scenp.Product__r.Id);
                System.debug('Scenario Product Revenue: ' + scenp.Total_Incremental_Bookable_Revenue__c);
                prodScp.put(scenp.Product__r.Id, scenp);
            }

            oppProducts = new List<OpportunityLineItem>([select Product2.Id, Id, Status__c from
                OpportunityLineItem where  OpportunityId in :oppIds]);

            for (OpportunityLineItem opprod: oppProducts)
            {
                Id nCinoProdId = ncinoNativeProd.get(opprod.Product2.Id);
                Scenario_Product__c acp = prodScp.get(nCinoProdId);
                System.debug('Ncino Prod: ' + nCinoProdId );
                if(acp.Product__r.Core_Treasury_Product__c == false)continue;

                if (acp != null && acp.Total_Incremental_Bookable_Revenue__c > 0)
                {
                    if (acp != null)
                        opprod.Total_Proforma_Revenue__c = acp.Total_Incremental_Bookable_Revenue__c;

                    opprod.Total_Proforma_Revenue__c = acp.Total_Incremental_Bookable_Revenue__c;
                    opprod.Status__c = 'Won';
                    updOppProd.add(opprod);
                }
                else
                {
                    if (acp != null)
                        opprod.Total_Proforma_Revenue__c = acp.Total_Incremental_Bookable_Revenue__c;
                    opprod.Status__c = 'Lost';
                    updOppProd.add(opprod);
                }
            }

            if (updOppProd.size() > 0)
                update updOppProd;
        }

    }

    private void updateOpportunities(List<Id> oppIds, List<Id> scenIds, Map<Id, LLC_BI__Scenario__c> oppToScenarioMap)
    {
        List<Opportunity> oppList;
        List<Opportunity> updOpps = new List<Opportunity>();
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();

        oppList = new List<Opportunity>([select id, Incremental_Service_Charges__c, Incremental_Deposit_Balance__c, Account.Average_Deposit_Balance__c,
         Account.Service_Charges__c from Opportunity where id in :oppIds]);

         for (Opportunity oppt: oppList)
            oppMap.put(oppt.Id, oppt);

        for (Id aId : oppMap.keySet())
        {
            Opportunity opp = oppMap.get(aId);
            LLC_BI__Scenario__c scenario = oppToScenarioMap.get(aId);
            System.debug('Opp  '+ opp + '****Acc details : ' + opp.Account.Average_Deposit_Balance__c);
            if (scenario.Deposit_Balance__c != null && opp.Account.Average_Deposit_Balance__c != null)
                opp.Incremental_Deposit_Balance__c = scenario.Deposit_Balance__c - opp.Account.Average_Deposit_Balance__c;
            if(scenario.Service_Charges_Due__c != null && opp.Account.Service_Charges__c != null)
                opp.Incremental_Service_Charges__c = scenario.Service_Charges_Due__c - opp.Account.Service_Charges__c;
            updOpps.add(opp);
        }
        if (updOpps.size() > 0)
            update updOpps;
    }
    private void createAFP(List<LLC_BI__Scenario__c> triggerList){
        System.debug('triggerlist '+ triggerList);
        Map<String, List<LLC_BI__Scenario_Item__c>> scMap = new Map<String, List<LLC_BI__Scenario_Item__c>>();
        List<List<LLC_BI__Scenario_Item__c>> scList = new List<List<LLC_BI__Scenario_Item__c>>();
        List<LLC_BI__Scenario_Item__c> scitemList = new List<LLC_BI__Scenario_Item__c>();
        Set<String> afpName = new Set<String>();
        Set<Id> scids = new Set<Id>();
        Set<Id> initialscIds = new Set<Id>();
        for(LLC_BI__Scenario__c sc : triggerList){
            initialscIds.add(sc.Id);
        }
        List<LLC_BI__Scenario__c> sList = [SELECT Id, Name, LLC_BI__Status__c, (SELECT Id, Name, AFP_Service_Code__c, Standard_Charge__c, Balance_Needed_to_Support_Services__c, AFP_Service_Category__c, Parent_AFP_Service_Description__c, LLC_BI__Scenario__c, LLC_BI__Volumes__c, Actual_Price__c,AFP_Service_Description__c FROM LLC_BI__Scenario_Items__r) FROM LLC_BI__Scenario__c WHERE Id IN: initialscIds];
        System.debug('slist ' + sList);
        for(LLC_BI__Scenario__c sc : sList){
                if(sc.LLC_BI__Status__c == 'Approved'){
                    // System.debug(sc);
                    // System.debug(sc.LLC_BI__Scenario_Items__r);
                    scList.add(sc.LLC_BI__Scenario_Items__r);
                    scids.add(sc.Id);
                }
        }
        List<TSO_Scenario_AFP_Category_Summary__c> afpsummaryList = [SELECT Id, Name, AFP_Service_Code__c, Scenario__c, Balance_Needed_to_Support_Services__c, (SELECT Id, Name, Balance_Needed_to_Support_Services__c, Standard_Charge__c, Actual_Price__c, LLC_BI__Volumes__c FROM Scenario_Items__r), Total_Service_Charges__c FROM TSO_Scenario_AFP_Category_Summary__c WHERE Scenario__c IN: scids];
        System.debug('summary list ' + afpsummaryList);
        // System.debug(scList);
        if(afpsummaryList.size() == 0){
      // System.debug('afp list is null');
            if(scList.size() > 0){
        // System.debug('scenario items list is not null');
                for(List<LLC_BI__Scenario_Item__c> itemList : scList){
                    for(LLC_BI__Scenario_Item__c scitem : itemList){
                        scitemList.add(scitem);
                        afpName.add(scitem.AFP_Service_Category__c);
                    }
                }
        // System.debug(afpName);
                for(String afp : afpName){
                    List<LLC_BI__Scenario_Item__c> siList = new List<LLC_BI__Scenario_Item__c>();
                    for(LLC_BI__Scenario_Item__c scitem : scitemList){
                            if(scitem.AFP_Service_Category__c == afp){
                                    siList.add(scitem);
                            }
                    }
                    scMap.put(afp, siList);
                }
                List<TSO_Scenario_AFP_Category_Summary__c> afpList = new List<TSO_Scenario_AFP_Category_Summary__c>();
                for(String afp : afpName){
                    TSO_Scenario_AFP_Category_Summary__c afpTemp = new TSO_Scenario_AFP_Category_Summary__c();
                    // afpTemp.Name = scMap.get(afp)[0].AFP_Service_Description__c;
                    afpTemp.Name = 'AFP' + scMap.get(afp)[0].AFP_Service_Category__c;
                    afpTemp.AFP_Service_Description__c = scMap.get(afp)[0].Parent_AFP_Service_Description__c;
                    afpTemp.AFP_Service_Code__c = afp;
                    afpTemp.Scenario__c = scMap.get(afp)[0].LLC_BI__Scenario__c;
                    afpTemp.Balance_Needed_to_Support_Services__c = 0;
                    afpTemp.Total_Service_Charges__c = 0;
                    for(LLC_BI__Scenario_Item__c scitem : scMap.get(afp)){
                        afpTemp.Balance_Needed_to_Support_Services__c =+ scitem.Balance_Needed_to_Support_Services__c;
                        afpTemp.Total_Service_Charges__c =+ (scitem.Actual_Price__c * scitem.LLC_BI__Volumes__c);
                        afpTemp.Total_Volumes_from_Scenario_Items__c =+ scitem.LLC_BI__Volumes__c;
                    }
                    afpList.add(afpTemp);
                }
                // System.debug(afpList);
                insert afpList;
                // System.debug(afpList);
                List<LLC_BI__Scenario_Item__c> scUpdate = new List<LLC_BI__Scenario_Item__c>();
                for(TSO_Scenario_AFP_Category_Summary__c afpTemp : afpList){
                    // System.debug(afpTemp.AFP_Service_Code__c);
                    // System.debug(scMap.get(afpTemp.AFP_Service_Description__c));
                    for(LLC_BI__Scenario_Item__c scitem : scMap.get(afpTemp.AFP_Service_Code__c)){
                        scitem.TSO_Scenario_AFP_Category_Summary__c = afpTemp.Id;
                        scUpdate.add(scitem);
                    }
                }
                update scUpdate;
            }
        }
        else{
            System.debug('afp records exist.');
            List<TSO_Scenario_AFP_Category_Summary__c> afpsummaryList2 = [SELECT Id, Name, AFP_Service_Code__c, Scenario__c, Balance_Needed_to_Support_Services__c, (SELECT Id, Name, Balance_Needed_to_Support_Services__c, Standard_Charge__c, Actual_Price__c,LLC_BI__Volumes__c FROM Scenario_Items__r), Total_Service_Charges__c FROM TSO_Scenario_AFP_Category_Summary__c WHERE Scenario__c IN: scids];
            List<TSO_Scenario_AFP_Category_Summary__c> afpList = new List<TSO_Scenario_AFP_Category_Summary__c>();
            for(TSO_Scenario_AFP_Category_Summary__c afpTemp : afpsummaryList2){
                System.debug('afp temp ' + afpTemp);
                afpTemp.Balance_Needed_to_Support_Services__c = 0;
                afpTemp.Total_Service_Charges__c = 0;
                for(LLC_BI__Scenario_Item__c scitem : afpTemp.Scenario_Items__r){
                    // System.debug(scitem);
                    afpTemp.Balance_Needed_to_Support_Services__c =+ scitem.Balance_Needed_to_Support_Services__c;
                    afpTemp.Total_Service_Charges__c =+ (scitem.LLC_BI__Volumes__c * scitem.Actual_Price__c);
                    afpTemp.Total_Volumes_from_Scenario_Items__c =+ scitem.LLC_BI__Volumes__c;
                }
                afpList.add(afpTemp);
            }
             update afpList;
           
            // if(scList != NULL){
            //  for(List<LLC_BI__Scenario_Item__c> itemList : scList){
            //      for(LLC_BI__Scenario_Item__c scitem : itemList){
            //          scitemList.add(scitem);
            //          afpName.add(scitem.AFP_Service_Code__c);
            //      }
            //  }
            //  for(String afp : afpName){
            //      List<LLC_BI__Scenario_Item__c> siList = new List<LLC_BI__Scenario_Item__c>();
            //      for(LLC_BI__Scenario_Item__c scitem : scitemList){
            //              if(scitem.AFP_Service_Code__c == afp){
            //                      siList.add(scitem);
            //              }
            //      }
            //      scMap.put(afp, siList);
            //  }
                // List<TSO_Scenario_AFP_Category_Summary__c> afpList = new List<TSO_Scenario_AFP_Category_Summary__c>();
                // for(TSO_Scenario_AFP_Category_Summary__c afpTemp : afpsummaryList){
                //  afpTemp.Balance_Needed_to_Support_Services__c = 0;
                //  afpTemp.Total_Service_Charges__c = 0;
                //  for(LLC_BI__Scenario_Item__c scitem : scMap.get(afpTemp.AFP_Service_Code__c)){
                //      afpTemp.Balance_Needed_to_Support_Services__c =+ scitem.Balance_Needed_to_Support_Services__c;
                //      afpTemp.Total_Service_Charges__c =+ scitem.Standard_Charge__c;
                //  }
                //  afpList.add(afpTemp);
                // }
                // update afpList;
            // }
        }
    }
}