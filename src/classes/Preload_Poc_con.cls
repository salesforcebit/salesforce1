public with sharing class Preload_Poc_con {
 public list<Exposure_Line_Item_Demo__c> exposeItemDemo; 
 public Map<id,Exposure_Line_Item_Demo__c> exposureLineITems;
 public Map<id,list<Exposure_Line_Item_Demo__c>> ptochildsMap;
 public Map<id, list<Exposure_Line_Item_Demo__c>> ChildstoSubchilds;
 private string prodpackageId;
 public Loanclass obje{get;set;}
  public list<ParentPreloadObj> parentObj{get;set;}
  public Preload_Poc_con(ApexPages.StandardController controller){
     parentObj = new list<ParentPreloadObj>();
     response();
      Loanclass obje = new loanClass();
   ptochildsMap = new  Map<id,list<Exposure_Line_Item_Demo__c>>();
     prodpackageId = ApexPages.currentPage().getParameters().get('id')!= null ? ApexPages.currentPage().getParameters().get('id') : 'a32';
    exposureLineITems = new Map<id,Exposure_Line_Item_Demo__c>();
    ChildstoSubchilds = new  Map<id,list<Exposure_Line_Item_Demo__c>>();
     if(exposeItemDemo == null){
        exposeItemDemo = new list<Exposure_Line_Item_Demo__c>();
       exposeItemDemo  = [select id,Account_Product__c,Committed_Amount__c,Exposure_Line_Item_Demo__c,Exposure_Type__c,Loan_Number__c,MDMID__c,Outstanding_Amount__c,Relationship__c,Relationship_Type__c from Exposure_Line_Item_Demo__c ];
     } 
    
  }
  
  
  public void responsee(){
   if(!exposeItemDemo.isEmpty()){
      for(Exposure_Line_Item_Demo__c exp:exposeItemDemo){
         exposureLineITems.put(exp.id,exp);
         if(exp.Exposure_Line_Item_Demo__c!= null){
            ptochildsMap.put(exp.id, new list<Exposure_Line_Item_Demo__c >());
         }else{
            ptochildsMap.get(exp.id).add(exp);
         }
         if(ChildstoSubchilds.containsKey(exp.Exposure_Line_Item_Demo__c)){
           ChildstoSubchilds.get(exp.Exposure_Line_Item_Demo__c).add(exp);
         }else{
          ChildstoSubchilds.put(exp.Exposure_Line_Item_Demo__c, new list<Exposure_Line_Item_Demo__c>{exp});
         }
      }
   }
  wrapper = new MycustWrapper();
    for(Exposure_Line_Item_Demo__c exps:exposeItemDemo){
      if(ptochildsMap.containskey(exps.id) && exposureLineITems.containskey(exps.id)){
        wrapper.expLine = exposureLineITems.get(exps.id);
       // wrapper.firstWrap = ptochildsMap.get(exps.id);
      }
    }
  
  }
 // public class Loanclass;
  public MycustWrapper wrapper{get;set;}
  public class MycustWrapper{
  public Exposure_Line_Item_Demo__c expLine{get;set;}
  public list<FirstChildWrap> firstWrap{get;set;}
  public MycustWrapper(){

   firstWrap = new list<FirstChildWrap>();
  }
  
  }
  public class FirstChildWrap{
   public Exposure_Line_Item_Demo__c firstChild{get;set;}
   public list<SecondChildWrap> secondchild{get;set;}
   public FirstChildWrap(Exposure_Line_Item_Demo__c exp, list<SecondChildWrap> f2){
     firstChild = exp;
     secondchild = f2.clone();     
   }
  
  }
  public class SecondChildWrap{
   public Exposure_Line_Item_Demo__c  grand{get;set;}
   public SecondChildWrap(Exposure_Line_Item_Demo__c exp){
     grand = exp;
   }
  }
   
  
  
  public void response(){
  List<ParentPreloadObj> deserializedInvoices =  new list<ParentPreloadObj>();
  try {
     /* String jsonStr =  '[{  "loan": "EX-0041",'+
   '"Rm": "ABC co.",'+
  '"rmType": "Related Entity",'+
    '"prodtype": "Total",'+
    '"outStandingAmt": 400000,'+
    '"committedAmt": 450000,'+
    '"exposureType": "Committed Non Aggregate",'+
    '"childPreEx": [  {'+
        '"loan": "EX-0042",'+
        '"Rm": "ABC co.",'+
        '"rmType": "Related Entity",'+
        '"prodtype": "LOC-4456",'+
        '"outStandingAmt": 100000,'+
        '"committedAmt": 150000,'+
        '"exposureType": "Committed Non Aggregate",'+
        '"grandCHilds":[{'+
        '"loan": "EX-0043",'+
        '"Rm": " SUB 1 ABC co.",'+
        '"rmType": "Related Entity",'+
        '"prodtype": "LOC-4457",'+
        '"outStandingAmt": 200000,'+
        '"committedAmt": 150000,'+
        '"exposureType": "Committed Non Aggregate"'+
          '},{'+
          '"loan": "EX-0044",'+
        '"Rm": " SUB 2 ABC co.",'+
       '"rmType": "Related Entity",'+
        '"prodtype": "LOC-4457",'+
        '"outStandingAmt": 300000,'+
        '"committedAmt": 150000,'+
        '"exposureType": "Committed Non Aggregate"'+
      '}]}, {'+
        '"loan": "EX-0023",'+
        '"Rm": "ABC Sub 2",'+
        '"rmType": "Related Entity",'+
        '"prodtype": "LVL-6756",'+
        '"outStandingAmt": 300000,'+
        '"committedAmt": 300000,'+
        '"exposureType": "Committed Non Aggregate",'+
        '"grandCHilds":[ {'+
        '"loan": "EX-0024",'+
        '"Rm": " Child MCA 21",'+
        '"rmType": "Related Entity",'+
        '"prodtype": "LOC-4357",'+
        '"outStandingAmt": 100000,'+
        '"committedAmt": 100000,'+
        '"exposureType": "Committed Non Aggregate"},{'+
           '"loan": "EX-0025",'+
        '"Rm": " SUb MCA 21",'+
        '"rmType": "Related Entity",'+
        '"prodtype": "LOC-4337",'+
        '"outStandingAmt": 500000,'+
        '"committedAmt": 130000,'+
        '"exposureType": "Committed Non Aggregate"'+
        '}] }] }]';*/
        
 String jsonStr = '{'+
 '"loan": "EX-0041",'+
  '"Rm": "ABC co.",'+
  '"rmType": "Related Entity",'+
  '"prodtype": "Total",'+
  '"outStandingAmt": 400000,'+
  '"committedAmt": 450000,'+
  '"exposureType": "Committed Non Aggregate",'+
  '"childPreEx": ['+
    '{'+
      '"cloan": "EX-0042",'+
      '"cRm": " ABC Sub 1",'+
      '"crmType": "Borrower",'+
      '"cprodtype": "Total",'+
      '"coutStandingAmt": 350000,'+
      '"ccommittedAmt": 450000,'+
      '"cexposureType": "Proposed Direct Exposure",'+
      '"grandChilds": ['+
        '{'+
          '"gcloan": "EX-0044",'+
          '"gcRm": " John Smith",'+
          '"gcrmType": "Guarantor",'+
          '"gcprodtype": "ARC-3344",'+
          '"gcoutStandingAmt": 200000,'+
          '"gccommittedAmt": 250000,'+
          '"gcexposureType": "Proposed Indirect Exposure"'+
        '},'+
        '{'+
          '"gcloan": "EX-0045",'+
          '"gcRm": "John Smith",'+
          '"gcrmType": "Guarantor",'+
          '"gcprodtype": "ALS-1234",'+
          '"gcoutStandingAmt": 150000,'+
          '"gccommittedAmt": 200000,'+
          '"gcexposureType": "Proposed Indirect Exposure"'+
        '}'+
      ']'+
    '},'+
    '{'+
      '"cloan": "EX-0021",'+
      '"cRm": "ABC Sub 2",'+
      '"crmType": "Guarantor",'+
     '"cprodtype": "Total",'+
     '"coutStandingAmt": 500000,'+
      '"ccommittedAmt": 500000,'+
      '"cexposureType": "Committed Indirect Exposure",'+
      '"grandChilds": ['+
        '{'+
          '"gcloan": "EX-0031",'+
          '"gcRm": "ABC Sub 2",'+
          '"gcrmType": "Borrower",'+
          '"gcprodtype": "ARC-3355",'+
          '"gcoutStandingAmt": 300000,'+
          '"gccommittedAmt": 300000,'+
          '"gcexposureType": "Committed Indirect Exposure"'+
        '},'+
        '{'+
          '"gcloan": "EX-0032",'+
          '"gcRm": "ABC Sub 2",'+
          '"gcrmType": "Borrower",'+
          '"gcprodtype": "LOC-7898",'+
         '"gcoutStandingAmt": 200000,'+
          '"gccommittedAmt": 200000,'+
          '"gcexposureType": "Committed Indirect Exposure"'+
        '}'+
      ']'+
    '}'+
  ']'+
'}';
    

     
        if(jsonStr != null){
       obje = Loanclass.parse(jsonStr);
           system.debug('VEnkaty Partsed'+obje);
         /*  parentObj =  (List<ParentPreloadObj>)JSON.deserializestrict(jsonStr, List<ParentPreloadObj>.class);
             
           system.debug('response::deserializedInvoices '+parentObj );
           for(ParentPreloadObj obj:parentObj  ){
              system.debug('response::deserializedInvoices '+obj.childPreEx);
              for(ChildPreload cp:obj.childPreEx){
               system.debug('response::child parent records'+cp.loan);
              /* for(grandChildPreload gp:cp.grlist){
                  system.debug('glist::child parent records'+gp.loan);
               }
              }
           }*/
        }
        }catch (exception ex){
        system.debug('exception '+ex);
        }
  }
  
     public PageReference cancelAction() {
        return new pagereference('/'+prodpackageId );
    }
     public PageReference saveAction() {
        return new pagereference('/'+prodpackageId );
    }
    public PageReference backAction() {
        return new pagereference('/apex/MDMSearch?id='+prodpackageId);
    }



    
  public class ParentPreloadObj{
    public string loan             {get; set;}
    public string Rm               {get; set;}
    public string rmType           {get; set;}
    public string prodtype         {get;set;}
    public decimal outStandingAmt  {get;set;}
    public decimal committedAmt    {get;set;}
    public string exposureType     {get;set;}
    public list<ChildPreload> childPreEx{get;set;}
    public ParentPreloadObj(string loan, string rm, string rmtype,string prodtype, decimal outStandingAmt,decimal committedAmt,string exposureType ,list<ChildPreload> child ){
      this.loan = loan;
      this.rm = rm;
      this.rmtype = rmtype;
      this.prodtype = prodtype;
      this.outStandingAmt= outStandingAmt;
      this.committedAmt = committedAmt;
      this.exposureType = exposureType;      
      childPreEx = child.clone();
    }
    
  
  }
  public class ChildPreload{
    public string loan             {get; set;}
    public string Rm               {get; set;}
    public string rmType           {get; set;}
    public string prodtype         {get;set;}
    public decimal outStandingAmt  {get;set;}
    public decimal committedAmt    {get;set;}
    public string exposureType     {get;set;}
    public list<grandChildPreload> grlist{get;set;}
    public ChildPreload( ){
    /* this.loan = loan;
      this.rm = rm;
      this.rmtype = rmtype;
      this.prodtype = prodtype;
      this.outStandingAmt= outStandingAmt;
      this.committedAmt = committedAmt;
      this.exposureType = exposureType;
            grlist= gchild.clone();*/
    }
  
  }
 public class grandChildPreload{
    public string loan             {get; set;}
    public string Rm               {get; set;}
    public string rmType           {get; set;}
    public string prodtype         {get;set;}
    public decimal outStandingAmt  {get;set;}
    public decimal committedAmt    {get;set;}
    public string exposureType     {get;set;}
  
  }
 

  
}