global class TreasuryServicePendingPrereqsBatch implements Database.Batchable<SObject> {
	
	global String query = 'Select Id, LLC_BI__Stage__c, Planned_Implementation_Start_Date__c, Prerequisite_Treasury_Service__r.LLC_BI__Stage__c, Prerequisite_Treasury_Service__c From LLC_BI__Treasury_Service__c Where LLC_BI__Stage__c = \'' + Constants.treasuryServiceStagePendingPrerequisites + '\' And (Prerequisite_Treasury_Service__r.LLC_BI__Stage__c in (\'' + Constants.treasuryServiceStageTesting + '\', \'' + Constants.treasuryServiceStageTraining +'\') Or (Planned_Implementation_Start_Date__c != null And Planned_Implementation_Start_Date__c <= today))';
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<SObject> scope) {
		
		List<LLC_BI__Treasury_Service__c> updateTreasuryServices = new List<LLC_BI__Treasury_Service__c>();
		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
		
		for(LLC_BI__Treasury_Service__c treasuryService : (List<LLC_BI__Treasury_Service__c>)scope) {
			if(treasuryService.Prerequisite_Treasury_Service__r.LLC_BI__Stage__c == Constants.treasuryServiceStageTesting || treasuryService.Prerequisite_Treasury_Service__r.LLC_BI__Stage__c == Constants.treasuryServiceStageTraining) {
				
				if(treasuryService.Planned_Implementation_Start_Date__c == null || treasuryService.Planned_Implementation_Start_Date__c <= date.today()) {
					treasuryService.LLC_BI__Stage__c = Constants.treasuryServiceStageFulfillment;
				}
				else {
					treasuryService.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingFutureStartDate;
				}
			}
			else {
				treasuryService.Planned_Implementation_Start_Date__c = BusinessHours.nextStartDate(Constants.defaultBusinessHours.Id, date.today().addDays(1)).date();
				treasuryService.Date_Implementation_Delayed__c = date.today();
			}
			updateTreasuryServices.add(treasuryService);
		}
		
		if(updateTreasuryServices.size() > 0) {
			update updateTreasuryServices;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}