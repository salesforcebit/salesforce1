public with sharing class WorkflowController {

	public WorkflowController() {
		List<LLC_BI__Product_Type__c> productList = getProducts();
	}

	@testVisible private static List<LLC_BI__Product_Type__c> getProducts(){
		List<LLC_BI__Product_Type__c> prodTypes = [
			SELECT
				Id,
				Name
			FROM
				LLC_BI__Product_Type__c
			WHERE
				LLC_BI__Usage_Type__c = :TREASURY_MGT 
		];
		return prodTypes;
	}


	private static final List<String> availableWorkflowObjects = new List<String>();

	private static final String TREASURY_MGT = 'Treasury Management';
	
}