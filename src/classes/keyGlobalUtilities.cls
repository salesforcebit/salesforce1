//=================================================================================================
//  KeyBank
//  Object: keyGlobalUtilities
//  Author: Barney Young Jr
//  Detail: Global Utilities Class for KeyBank
//=================================================================================================

public with sharing class keyGlobalUtilities {
	
	//Get RecordType Id
	public static id getRecordTypeId(string sObjectName, string rtName){
		
         //Schema.GetGlobalDescribe()
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get(sObjectName);
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe();
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id recordTypeId = RecordTypeInfo.get(rtName).getRecordTypeId();
        
        //Return Id
        return recordTypeId;
    }
    
    
}