public with sharing class ScenarioRecordTypeCtrl {
	public LLC_BI__Scenario__c SC {get; set;}
	public Opportunity oppty {get; set;}
	public Boolean isDDA {get; set;}
	public Boolean isRelationship {get; set;}
	public ScenarioRecordTypeCtrl(ApexPages.StandardController controller) {
		if(!Test.isRunningTest()){
		controller.addFields(new String[]{'Name', 'AccountId'});
		}
		this.oppty = (Opportunity) controller.getRecord();
		SC = new LLC_BI__Scenario__c();
		isDDA = false;
		isRelationship = false;
	}
	public PageReference saveAndcontinue(){
		if(SC.DDA_Proforma__c == true && SC.Relationship_Proforma__c == true){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please only select one Proforma Type.'));
			return null;
		}
		if(SC.DDA_Proforma__c != true && SC.Relationship_Proforma__c != true){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Proforma Type.'));
			return null;
		}
		if(SC.DDA_Proforma__c == true && SC.Relationship_Proforma__c != true){
				isDDA = true;
				String scenarioName = createScenarioName();
				SC = new LLC_BI__Scenario__c(LLC_BI__Opportunity__c = oppty.Id, Name=scenarioName);
				SC.RecordTypeId =[SELECT Id,SobjectType,Name FROM RecordType WHERE Name =:'DDA Proforma' AND SobjectType =: 'LLC_BI__Scenario__c' limit 1].Id;
				SC.DDA_Proforma__c = true;
				// SC.IPI_Number_Lookup__c = oppty.AccountId;
				insert SC;
				PageReference pr = new PageReference('/apex/ScenarioPage?id='+SC.Id);
				return pr;
		}
		if(SC.DDA_Proforma__c != true && SC.Relationship_Proforma__c == true){
				isRelationship = true;
				String scenarioName = createScenarioName();
				SC = new LLC_BI__Scenario__c(LLC_BI__Opportunity__c = oppty.Id, Name=scenarioName);
				SC.RecordTypeId =[SELECT Id,SobjectType,Name FROM RecordType WHERE Name =:'Relationship Proforma' AND SobjectType =: 'LLC_BI__Scenario__c' limit 1].Id;
				SC.Relationship_Proforma__c = true;
        insert SC;
				PageReference pr = new PageReference('/apex/ScenarioPage?id='+SC.Id);
				return pr;
		}
		return null;
	}
	private String createScenarioName() {
        Integer scenarioSequence;
        List<LLC_BI__Scenario__c> existingScenarios = [SELECT Id, LLC_BI__Opportunity__c FROM LLC_BI__Scenario__c where LLC_BI__Opportunity__c =: oppty.Id];
        if(existingScenarios.size() > 0) {
            scenarioSequence = existingScenarios.size() + 1;
        }
        else{
            scenarioSequence = 1;
        }
        return oppty.Name + ' ' + Label.Default_Scenario_Name + ' ' + scenarioSequence;
    }
}