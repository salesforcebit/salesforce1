Global Class KYCWebserviceHelper{

   public static List<Account> Accountreclist = new List<Account>();
   public static List<Account> GuarntorAccountreclist = new List<Account>();
   public static List<Contact> Contactreclist = new List<Contact>();
   public static List<LLC_BI__Product__c> productslist = new List<LLC_BI__Product__c>();
   public static List<LLC_BI__Product_Package__c> ProductpackageList = new List<LLC_BI__Product_Package__c>();
   public static List<LLC_BI__Loan__c> Loanlist = new List<LLC_BI__Loan__c>();
   public static List<LLC_BI__Fee__c> feelist = new List<LLC_BI__Fee__c>();
   public static List<Opportunity> Opportunitylist = new List<Opportunity>();
   public static List<PriceBookEntry> priceBookList = new List<PriceBookEntry>();
   Public static Map<String,LLC_BI__Branch__c> branchIdsMap; 
   
   Public static void prepareentitywisedata(List<KYCEntitywisewebservicedata.BorrowerAccountdata> BorrowerServiceData){
        List<string> Products = new List<string>();
        branchIdsMap = new Map<String,LLC_BI__Branch__c>([select Name,id from LLC_BI__Branch__c limit 9999]);
        
        for(KYCEntitywisewebservicedata.BorrowerAccountdata sd:BorrowerServiceData){
            Account Accountrec = new Account();
            if(sd.Accountname != null){
                   Accountrec.name = sd.Accountname;
                   Accountrec.recordtypeid = '012o0000000OBtD';
                   Accountrec.AccountSource = sd.AccountSource;
                   if(branchIdsMap.containsKey(sd.AccountBranch))
                   {
                    Accountrec.LLC_BI__Branch__c = branchIdsMap.get(sd.AccountBranch).id;
                   }
                   Accountrec.ECCP_Legal_Business_Name__c=sd.AccountLegalBusinessName;
                   Accountrec.Description=sd.BusinessDescription;
                   Accountrec.NumberOfEmployees=sd.NumberOfEmployees;
                   Accountrec.Primary_Phone_Number__c=sd.PrimaryPhoneNumber;
                   Accountrec.Fax=sd.Fax;
                   Accountrec.Mobile_Phone__c=sd.MobilePhone;
                   Accountrec.LLC_BI__Tax_Identification_Number__c=sd.TaxIdentificationNumber;
                   Accountrec.NaicsCode=sd.NaicsCode;
                   Accountrec.ECC_Organization_ID__c=sd.OrganizationID;
                   Accountrec.ECC_Incorporation_State__c=sd.IncorporationState;
                   Accountrec.YearStarted=sd.YearStarted;
                   Accountrec.Organization_Type__c=sd.OrganizationType;
                   Accountrec.ECC_Other_Identification_Type__c=sd.OtherIdentificationType;
                   Accountrec.ECC_Other_Identification_Number__c=sd.OtherIdentificationNumber;
                   Accountrec.ECC_Other_Issue_Date__c=sd.OtherIssueDate;
                   Accountrec.ECC_Other_Identification_Expiry_Date__c=sd.OtherIdentificationExpiryDate;
                   Accountrec.ECC_Other_Identification_State__c=sd.OtherIdentificationState;
                   Accountrec.KBCM_Customer_Number__c=sd.IPI;
                   Accountrec.BillingStreet=sd.BillingStreet;
                   Accountrec.BillingCity=sd.BillingCity;
                   Accountrec.BillingState=sd.BillingState;
                   Accountrec.BillingPostalCode=sd.BillingPostalCode;
                   Accountrec.BillingCountry=sd.BillingCountry;
                   Accountrec.ShippingStreet=sd.ShippingStreet;
                   Accountrec.ShippingState=sd.ShippingState;
                   Accountrec.ShippingPostalCode=sd.ShippingPostalCode;
                   Accountrec.ShippingCountry=sd.ShippingCountry;
                   Accountreclist.add(Accountrec);
            }
            
            for(KYCEntitywisewebservicedata.Contactdata cd: sd.listcontactdata){
                Contact Contactrec = new Contact();
                if(cd.contactlastname!= null) Contactrec.lastname= cd.contactlastname;
                Contactrec.recordtypeid = '012o0000000OBtK';
                Contactreclist.add(Contactrec);
            }
            
            for(KYCEntitywisewebservicedata.Opportunitydata od: sd.listOpportunitydata){
                opportunity op = new opportunity();
                
                if(od.Oppname != null)op.name=od.oppname;
                op.CloseDate = date.today();
                if(od.stagevalue != null)op.stagename=od.stagevalue;
                op.recordtypeid = '012o0000000Odd2';
                Opportunitylist.add(op);                
                for(KYCEntitywisewebservicedata.LCPIProductdata lpd: od.listLCPIProductdata){
                    if(lpd.productname != null)
                    Products.add(lpd.productname);
                }
            }
            for(KYCEntitywisewebservicedata.Productpackagedata ppd: sd.listProductpackagedata){
                if(ppd.Productpackagename != null && ppd.ProductpackageLob != null){
                          LLC_BI__Product_Package__c pp= new LLC_BI__Product_Package__c();
                          pp.recordtypeid = '012o0000000yFtn';
                          pp.Name= ppd.Productpackagename;
                          pp.ECCP_Campus__c =  ppd.ProductpackageLob;
                          ProductpackageList.add(pp);
                 }
            }
            for(KYCEntitywisewebservicedata.Loandata ld: sd.listLoandata){
                if(ld.Loanoriginalamount != null && ld.LoanNewMoney != null && ld.LoanRequestType != null ){
                      LLC_BI__Loan__c loan = new LLC_BI__Loan__c();
                      loan.recordtypeid= '012o0000000yFtg';
                      loan.name = ld.loanname;
                      loan.LLC_BI__Amount__c = decimal.valueof(ld.Loanoriginalamount);
                      loan.LLC_BI__Amount_New_Money__c = decimal.valueof(ld.LoanNewMoney);
                      loan.Request_Type__c = ld.LoanRequestType;
                      loan.Test_Loan__c = 'Yes - This is a Test';
                      loan.LLC_BI__Loan_Officer__c = userinfo.getuserid();
                      Loanlist.add(loan);
                 }
                 for(KYCEntitywisewebservicedata.Feedata fd: ld.listFeedata){
                     if(fd.Feetype != null && fd.feecode !=null && fd.feeamount!=null){
                              LLC_BI__Fee__c fee = new LLC_BI__Fee__c();
                              fee.LLC_BI__Amount__c = Decimal.valueof(fd.feeamount);
                              fee.LLC_BI__Fee_Code__c = fd.feecode;
                              fee.LLC_BI__Fee_Type__c = fd.feetype;
                              feelist.add(fee);
                     }
                 }
            }
            
                   
        }
        if(Products.size()>0){
            for(LLC_BI__Product__c lp: [select id,name,Related_SFDC_Product__c from  LLC_BI__Product__c where Related_SFDC_Product__r.name in:Products]){
                productslist.add(lp);
            }
            for(PriceBookEntry pbe:[SELECT Id, Product2Id, Product2.Id, Product2.Name FROM PriceBookEntry WHERE Product2.Name in:Products AND PriceBook2.isStandard=true limit 1]){
                priceBookList.add(pbe);
            }
            system.debug('priceBookList '+priceBookList.size());
        }
        
        
   
   }
   
   public static void prepareGuarntordata(List<KYCEntitywisewebservicedata.guarantorAccountData> guarantorServiceData){
           for(KYCEntitywisewebservicedata.guarantorAccountData sd: guarantorServiceData){
                Account Accountrec = new Account();
                if(sd.AccountFirstName!= null){
                       Accountrec.name = sd.AccountFirstName;
                       Accountrec.recordtypeid = '012o0000000OBtD';
                       GuarntorAccountreclist.add(Accountrec);
                }
            }   
   }
          
}