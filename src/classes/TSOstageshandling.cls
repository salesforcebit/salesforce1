Public class TSOstageshandling{
 public id TRSID{set;get;}
 public TSOstageshandling( id tsid){
   TRSID = tsid; 
 }
  public void handleafterprocess(){  
     LLC_BI__Treasury_Service__c TRS =  [SELECT id,name,LLC_BI__Stage__c,Type__c,Is_this_a_KTT_client__c,LLC_BI__Product_Package__c,ownerid,LLC_BI__Product_Reference__c FROM LLC_BI__Treasury_Service__c where id = :TRSID];
       if(TRS.Name == 'ACH: Origination & Receipt'){  system.debug('1' );                    
            if( TRS.Type__c =='NEW'){ system.debug('2');                 
                  if(TRS.LLC_BI__Stage__c == 'Fulfillment'){  system.debug('3');                       
                      if(TRS.Is_this_a_KTT_client__c ) { system.debug('4');                            
         List<LLC_BI__Product__c> products = [Select id,name From LLC_BI__Product__c where id = :TRS.LLC_BI__Product_Reference__c ];
         LIST<TSO_Workflow_Template__c> workflowtemplate = [Select  id,name from TSO_Workflow_Template__c where  id = :products[0].Workflow_Templates__r ];
         LIST<TSO_Stage__c> stagee = [SELECT id,name,Related_Treasury_Stage__c,Workflow_Template__c FROM TSO_Stage__c where Workflow_Template__c = :workflowtemplate[0].id ];            
         LIST<TSO_Setup_Task_Template__c> setuptasktemplate = [Select id,Description__c,Queue_API_Name__c,Expected_Completion_Time__c,Responsible_Third_Party__c,Parent_Stage__c FROM TSO_Setup_Task_Template__c where Parent_Stage__c = :workflowtemplate[0].id LIMIT 2];
         system.debug('products '+products );  
         system.debug('workflowtemplate '+workflowtemplate );  
         system.debug('stagee '+stagee );  
         system.debug('setuptasktemplate '+setuptasktemplate );  
           if(setuptasktemplate.size()>0){
    TSO_Setup_Task__c tsoSetupTask = new TSO_Setup_Task__c();   
    tsoSetupTask.Name = setuptasktemplate[0].Description__c;
    tsoSetupTask.Description__c = setuptasktemplate[0].Description__c;
    tsoSetupTask.Related_Stage__c = stagee[0].Related_Treasury_Stage__c;
    tsoSetupTask.Setup_Task_Template__c = setuptasktemplate[0].id;
    tsoSetupTask.Treasury_Service__c = TRS.id;
    tsoSetupTask.Product_Package__c = TRS.LLC_BI__Product_Package__c;
    tsoSetupTask.Status__c = 'New';   
    tsoSetupTask.OwnerId = TRS.ownerid;
                           try{
                                 system.debug('10');
                               insert tsoSetupTask;
                           }catch (exception e){
                               system.debug('ERRORRRR STAGE'+e.getmessage());
                           }
                                                        } }
              }}
            } 
              
        }
    }