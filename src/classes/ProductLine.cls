/**
 * Provides a strongly-typed intermediate object for converting the JSON representation
 * of a Product Line to its sObject equivalent.
 * Uses the IForce pattern to map between sObject and JSON.
 */
public class ProductLine extends nFORCE.AForce {
	public String sid;
	public String name;
	public Boolean isDeleted;
	public List<ProductType> productTypes;

	public ProductLine() {
		this.productTypes = new List<ProductType>();
	}

	public ProductLine(String sid, String name, Boolean isDeleted) {
		this.sid = sid;
		this.name = name;
		this.isDeleted = isDeleted;
		this.productTypes = new List<ProductType>();
	}

	public LLC_BI__Product_Line__c getSObject() {
		LLC_BI__Product_Line__c productLine = new LLC_BI__Product_Line__c();
		this.mapToDb(productLine);
		return productLine;
	}

	public static ProductLine buildFromDB(sObject obj) {
		ProductLine productLine = new ProductLine();
		productLine.mapFromDB(obj);
		return productLine;
	}

	public override Type getType() {
		return ProductLine.class;
	}

	public override Schema.SObjectType getSObjectType() {
		return LLC_BI__Product_Line__c.sObjectType;
	}

	public override void mapFromDB(sObject obj) {
		LLC_BI__Product_Line__c productLine = (LLC_BI__Product_Line__c)obj;
		if (productLine != null) {
			this.sid = productLine.Id;
			this.name = productLine.Name;
		}
	}

	public override void mapToDb(sObject obj) {
		LLC_BI__Product_Line__c productLine = (LLC_BI__Product_Line__c)obj;
		if (productLine != null) {
			if (this.sid != null && this.sid.length() > 0) {
				productLine.Id = Id.valueOf(this.sid);
			}
			productLine.Name = this.name;
			productLine.LLC_BI__Product_Object__c = ProductCatalogController.TS_PRODUCT_OBJECT;
		}
	}
}