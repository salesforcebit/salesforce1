@isTest
private class AnalyzedAccountTriggerHandler_UT {
	
	@isTest static void test_handleBeforeUpdate() {
		LLC_BI__Deposit__c da = new LLC_BI__Deposit__c();
	    da.Name = 'TestDA2';
	    da.Bank_Number__c = '1234';
	    da.LLC_BI__Account_Number__c = '999999';
	    insert da;

		Account acc = UnitTestFactory.buildTestAccount();
		insert acc;

	    Product2 SFprod = UnitTestFactory.buildTestProduct();
		SFprod.Name = 'Scenario Test';
		SFprod.Family = 'Deposits & ECP';
		insert SFprod;

		LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
		insert pl;

		LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
		insert pt;

		LLC_BI__Product__c prod = new LLC_BI__Product__c();
		prod.Name = 'TestProd';
		prod.Related_SFDC_Product__c = SFprod.Id;
		prod.LLC_BI__Product_Type__c = pt.Id;
		insert prod;

	    LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c();
		ts.Name = 'TestTS';
		ts.Request_Type__c = 'New';
		ts.LLC_BI__Stage__c = 'Fulfillment';
		ts.LLC_BI__Product_Reference__c = prod.Id;
		ts.LLC_BI__Relationship__c = acc.Id;
		ts.GTM_Request_Type__c = 'NEW Setup';
		insert ts;

		LLC_BI__Analyzed_Account__c AA = new LLC_BI__Analyzed_Account__c();
		AA.LLC_BI__Deposit_Reference__c = da.Id;
        AA.LLC_BI__Deposit_Account__c = da.Id;
		AA.Name = 'Test';
		AA.LLC_BI__Treasury_Service__c = ts.Id;
		AA.Account_Number__c = '12345';
		AA.Select_Service_Option_Type__c = 'KTT Information Reporting';
		AA.GTM_Account__c = true;
        insert AA;

        Test.startTest();
        AA.Previous_Day_Credit__c  = 100.00;
        AA.Previous_Day_Via__c = 'KTT';
        AA.Previous_Day_Options__c = 'Summary';
        AA.Previous_Day_Summary_and_Detail_Options__c = 'Credits';
        AA.Service_Options__c = 'Previous Day Credit';
        AA.Current_Day_Options__c = 'Summary';
        AA.Intradays_Wires_Options__c = 'Summary';
        AA.CDA_Options__c = 'Summary';
        AA.CDA_Opt__c = 'HOP Summary';
        AA.HOP_Options__c = '2';
        AA.Check_Length__c = 10;
        AA.Olds_Options__c = 'Summary';
        AA.Lockbox_Options__c = 'Summary';
        AA.Lockbox_Number__c = '123';
        AA.DepItems_Options__c = 'Summary';
        AA.ESP_Options__c = 'EDI';
        AA.CS_Options__c = 'DDA';
		update AA;

		AA.Previous_Day_Debit__c  = 90.00;
        AA.Previous_Day_Via__c = 'KTT BAI2';
        AA.Previous_Day_Options__c = 'Summary & Detail';
        AA.Previous_Day_Summary_and_Detail_Options__c = 'Debits';
        AA.Service_Options__c = 'Previous Day Debit';
        AA.Current_Day_Options__c = 'Summary & Detail';
        AA.Intradays_Wires_Options__c = 'Summary & Detail';
        AA.CDA_Options__c = 'Summary & Detail';
        AA.CDA_Opt__c = 'ACH Addenda';
        AA.HOP_Options__c = '3';
        AA.Check_Length__c = 11;
        AA.Olds_Options__c = 'Summary & Detail';
        AA.Lockbox_Options__c = 'Summary';
        AA.Lockbox_Number__c = '1234';
        AA.DepItems_Options__c = 'Summary & Detail';
        AA.ESP_Options__c = 'ZBA';
        AA.CS_Options__c = 'RPM';
		update AA;
		Test.stopTest();

		List<LLC_BI__Analyzed_Account__c> aaList = [SELECT Id, Updated_Service_Options__c FROM LLC_BI__Analyzed_Account__c WHERE Id =: AA.Id];

		System.assertNotEquals(null, aaList[0].Updated_Service_Options__c);
	}
	
}