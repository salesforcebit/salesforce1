global class batchonetimeUpdate_on_Participants implements Database.Batchable<sObject>, Schedulable
{
    
    global batchonetimeUpdate_on_Participants()
       {
         // Batch Constructor
       }
       
        // Start Method
       global Database.QueryLocator start(Database.BatchableContext BC){
         //string query = 'SELECT ID, Name,Ownerid,Contact_Id__c,Contact_Id__r.User_Id__c,Contact_Id__r.Contact_Type__c FROM Participant__c limit 200'; 
         string query = 'SELECT Id, ownerid, Contact_Id__r.User_Id__r.id,Contact_Id__r.Contact_Type__c FROM Participant__c';
         system.debug(query);
         return Database.getQueryLocator(query);
       }
       global void execute(SchedulableContext SC)
       {
        
       }
        // Execute Logic
       global void execute(Database.BatchableContext BC, List<Participant__c>scope)
       {
            //List<Participant__c> parties = [SELECT Id, ownerid, Contact_Id__r.User_Id__r.id,Contact_Id__r.Contact_Type__c FROM Participant__c where Contact_Id__r.Contact_Type__c =: 'Key Employee' and Contact_Id__r.User_Id__r.id != null];
            List<Participant__c> pupdate = new List<Participant__c>();
            for(Participant__c p : scope)
            {
                if(p.Contact_Id__r.Contact_Type__c == 'Key Employee')
                system.debug(p.Contact_Id__r.Contact_Type__c);
                {
                if(p.Contact_Id__r.User_Id__r.id != null)
                {
                if(p.Contact_Id__r.User_Id__r.id != p.Ownerid)
                system.debug(p.Contact_Id__r.User_Id__r.id);
                system.debug(p.ownerid);
                
                    {
                        p.OwnerId = p.Contact_Id__r.User_Id__c;
                        pupdate.add(p); 
                    }    
                }
                }
                
            } 
            update pupdate;
            system.debug(pupdate);  
      }
     
       global void finish(Database.BatchableContext BC){
            // Logic to be Executed at finish
        }
}