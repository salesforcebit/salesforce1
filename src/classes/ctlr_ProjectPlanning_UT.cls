/**
 * Unit tests to validate logic in the controller ctlr_ProjectPlanning
 * Chintan Adhyapak, cadhyapak@westmonroepartners.com, February 18, 2016
 */
@isTest
private class ctlr_ProjectPlanning_UT {

    //Test method
    static testMethod void myUnitTest() {
    	//local vars
    	List<LLC_BI__Treasury_Service__c> tsList = new List<LLC_BI__Treasury_Service__c> ();
    	List<LLC_BI__Product__c> productList = new List<LLC_BI__Product__c> ();
    	List<Id> serviceIds = new List<Id> ();
    	
    	//Create account
    	Account myAccount = new Account();
    	myAccount.Name = 'Apex Test Account';
    	insert myAccount;
    	System.assertNotEquals(null, myAccount.Id);
    	
    	//Create an opportunity
    	Opportunity myOpp = new Opportunity();
    	myOpp.Name = 'Apex Test Opportunity';
    	myOpp.StageName = 'Pursue';
    	myOpp.CloseDate = Date.today() + 100;
    	insert myOpp;
    	System.assertNotEquals(null, myOpp.Id);
    	
    	//Create a Product Line
    	LLC_BI__Product_Line__c prodLine = new LLC_BI__Product_Line__c();
    	prodLine.Name = 'Apex Test Product Line';
    	insert prodLine;
    	System.assertNotEquals(null, prodLine.Id);
    	
    	//Create a Product Type
    	LLC_BI__Product_Type__c prodType = new LLC_BI__Product_Type__c();
    	prodType.Name='Apex Test Product Type';
    	prodType.LLC_BI__lookupKey__c='m9xx3';
    	prodType.LLC_BI__Product_Line__c = prodLine.Id;
    	insert prodType;
    	System.assertNotEquals(null, prodType.Id);
    	
    	
    	//Create 5 Products
    	for (integer i=0; i<5; i++) {
    		LLC_BI__Product__c product = new LLC_BI__Product__c();
    		product.Name = 'Apex Test Product ' + i;
    		product.LLC_BI__Product_Type__c = prodType.Id;
    		productList.add(product);
    	}
    	insert productList;
    	for (LLC_BI__Product__c product : productList)
    		System.assertNotEquals(null, product.Id);
    		
    	//Create a Product Package
    	LLC_BI__Product_Package__c prodPackage = new LLC_BI__Product_Package__c();
    	prodPackage.Name = 'Apex Test Product Package';
    	prodPackage.LLC_BI__Account__c = myAccount.Id;
    	insert prodPackage;
    	System.assertNotEquals(null, prodPackage.Id);
    		
        //Create 5 treasury services
        for (integer i=0; i<5; i++) {
        	LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c();
        	ts.Name = 'Apex Treasury Service ' + i;
        	ts.Opportunity__c = myOpp.Id;
        	ts.LLC_BI__Relationship__c = myAccount.Id;
        	ts.LLC_BI__Product_Reference__c = productList[i].Id;
        	ts.LLC_BI__Product_Package__c = prodPackage.Id;
        	tsList.add(ts);
        }
        insert tsList;
        for (LLC_BI__Treasury_Service__c ts : tsList) {
        	serviceIds.add(ts.Id);
        	System.assertNotEquals(null, ts.Id);
        }
        
        Test.startTest();
        
        	//Handle to the Apex page          
        	PageReference pageRef = Page.ProjectPlanning;
        	Test.setCurrentPage(pageRef);
        
        	//Get a handle to the controller
        	ApexPages.standardController stdController = new ApexPages.standardController(prodPackage);
        	ctlr_ProjectPlanning cusController = new ctlr_ProjectPlanning(stdController);
        	
        	//Retrieve Treasury Services
        	List<LLC_BI__Treasury_Service__c> serviceList = [Select Id, Planned_Implementation_Start_Date__c, Delay_Implementation__c, 
        			Prerequisite_Treasury_Service__c From LLC_BI__Treasury_Service__c Where Id in :serviceIds];
        	System.assertEquals(5, serviceList.size());
        	
        	//Initialize controller custom service records
        	for (integer i=0; i<5; i++)
        		cusController.myServicesList[i].prereqService = 'None';
        		
        	//Simulate form data modification
        	cusController.myServicesList[0].treasuryService.Planned_Implementation_Start_Date__c = Date.today() + 10;
        	cusController.myServicesList[1].treasuryService.Delay_Implementation__c = true;
        	cusController.myServicesList[2].prereqService = String.valueOf(cusController.myServicesList[2].treasuryService.Id);
        	cusController.myServicesList[3].prereqService = String.valueOf(cusController.myServicesList[4].treasuryService.Id);
        	cusController.myServicesList[4].prereqService = String.valueOf(cusController.myServicesList[3].treasuryService.Id);
        	
        	//save form data
        	cusController.saveData();
        	
        	//Validate
        	List<ApexPages.Message> messages = ApexPages.getMessages();
        	String errMsg = '';
        	for (ApexPages.Message msg : messages)
        		errMsg += msg.getDetail();
        	System.debug('errMsg: ' + errMsg);
        	System.assertEquals(true, errMsg.contains('You cannot have a <b>Planned Implementation Date</b> for treasury service'));
        	System.assertEquals(true, errMsg.contains('unless <b>Delay Implementation</b> is checked'));
        	System.assertEquals(true, errMsg.contains('You must have a <b>Planned Implementation Date</b> for treasury service'));
        	System.assertEquals(true, errMsg.contains('</span> if <b>Delay Implementation</b> is checked'));
        	System.assertEquals(true, errMsg.contains('Treasury service <span style=\"color:#659EC7; font-weight: bold;'));
        	System.assertEquals(true, errMsg.contains('</span> cannot be dependent on itself'));
        	System.assertEquals(true, errMsg.contains('Treasury service <span style=\"color:#659EC7; font-weight: bold;'));
        	System.assertEquals(true, errMsg.contains('</span> has a cyclic dependency on service <span style=\"color:#659EC7; font-weight: bold'));
        	System.assertEquals(true, errMsg.contains('They cannot be dependent on each other'));
        	
        	//Fix Errors
        	cusController.myServicesList[0].treasuryService.Planned_Implementation_Start_Date__c = Date.today() + 10;
        	cusController.myServicesList[0].treasuryService.Delay_Implementation__c = true;
        	cusController.myServicesList[1].treasuryService.Planned_Implementation_Start_Date__c = Date.today() + 30;
        	cusController.myServicesList[1].treasuryService.Delay_Implementation__c = true;
        	cusController.myServicesList[2].prereqService = 'None';
        	cusController.myServicesList[3].prereqService = String.valueOf(cusController.myServicesList[4].treasuryService.Id);
        	cusController.myServicesList[4].prereqService = 'None';
        	
        	//save form data
        	cusController.saveData();
        	
        	//Validate
        	System.debug('statusMsg is: ' + cusController.statusMsg);
        	System.assertEquals(true, cusController.statusMsg.equals('Your Changes Were Successfully Saved'));
        	System.assertEquals(true, cusController.displayGantt);
        	
        	//Return back
        	PageReference ref = cusController.returnBack();
        	System.assertEquals(true, ref.getUrl().contains('/' + prodPackage.Id));
        	
        Test.stopTest();
        
    } //myUnitTest
}