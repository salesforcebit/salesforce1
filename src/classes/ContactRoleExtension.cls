//================================================================================
//  KeyBank  
//  Object: Contact
//  Author: Offshore
//  Detail: ContactRoleExtension
//  Description : To View the Contacts associated to Account in Contact Page

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================

public class ContactRoleExtension
{
    public List<AccountContactRole> contactRoleAcc{get;set;}
    Public Contact contact;
    public ContactRoleExtension(ApexPages.StandardController controller)
    {
        contactRoleAcc= new List<AccountContactRole>();
        contact=(Contact) controller.getRecord();    
    }
    public List<AccountContactRole> getAccountRecords()
    {
        contactRoleAcc=[SELECT Account.Name,ContactId,IsPrimary,Role FROM AccountContactRole where contactId = :contact.id];
        return contactRoleAcc;
    }
}