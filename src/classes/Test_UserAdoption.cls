@istest
Public class Test_UserAdoption
{
    public static testMethod void LoginMethod()
    {
        Profile p = [select id from profile where name='System Administrator'];
        
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharanx@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharanx@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
        Map<String, Schema.SObjectType> consObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType cons = consObjectMap.get('Contact') ;
        Schema.DescribeSObjectResult resSchemaCon = cons.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> conRecordTypeInfo = resSchemaCon.getRecordTypeInfosByName();
        Id conRTId = conRecordTypeInfo.get('Key Employee').getRecordTypeId();
        System.Debug('conRTId:'+conRTId);
        Map<String, Schema.SObjectType> accsObjectMap4 = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs4 = accsObjectMap4.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaAcc4 = accs4.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo4 = resSchemaAcc4.getRecordTypeInfosByName();
        Id OptyRT4 = accRecordTypeInfo4.get('Credit').getRecordTypeId();
        
        LoginHistory testhist = new LoginHistory();
        
        System.runAs(testUser)
        {
            Account acc = new Account(name ='Acc Test');
            insert acc;
            Opportunity opp = new Opportunity(name = 'Test Opty', Recordtypeid = OptyRT4, accountid = acc.id, CloseDate = System.today(), StageName = 'Pursue');
            insert opp;
            contact con = new contact(LastName='Mr. ABCD',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc.id,Salesforce_User__c=true,User_Id__c=testUser.Id,recordtypeId = conRTId,Phone='9876543210' );
            insert con;
            Call__c c1 = new Call__c(Subject__c='test',Call_Date__c=date.ValueOf('2016-10-17'),Call_Type__c='COI Contact',AccountId__c=acc.id,Status__c='done');
            insert c1;
            Participant__c p1 = new Participant__c(CallId__c=c1.id,Contact_Id__c=con.id,Did_Not_Participate__c=false);
            insert p1;
            ParticipantDetails pdetails = new ParticipantDetails();
            pdetails.fetch(testUser.id);
            pdetails.detail();
            pdetails.back();
            OpportunityDetails odetails = new OpportunityDetails();
            odetails.fetch(testUser.id);
            odetails.detail();
            odetails.back();
            odetails.ondetails();
            User_Progress_Detail__c userdetail = new User_Progress_Detail__c();
            userdetail.Last_Month_Call_Count__c= 0;
            userdetail.Last_Month_Login_Count__c= 0;
            userdetail.Past_Due_Opportunities__c= 0;
            userdetail.This_Month_Call_Count__c = 0;
            userdetail.This_Month_Login_Count__c = 0;
            userdetail.Unmodified_Opportunities__c = 0;
            userdetail.User__c = testUser.id;
            insert userdetail;
            logindetailscontroller ldetails = new logindetailscontroller();
            ldetails.updateProgress();
            UserProgressDetails userprog = new UserProgressDetails();
            
        }
    } 
}