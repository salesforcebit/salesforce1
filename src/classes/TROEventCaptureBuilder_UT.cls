@isTest
private class TROEventCaptureBuilder_UT {
	static testMethod void TROEventCaptureBuilder_UT_test1() {
    	TROEventCapture.eventEV00021BodyType body_event0021 = new TROEventCapture.eventEV00021BodyType();
        body_event0021 = TROEventCapture_Builder.createEventEV00021BodyType('','');
        TROEventCapture.eventEV00022BodyType body_event0022 = new TROEventCapture.eventEV00022BodyType();
        body_event0022 = TROEventCapture_Builder.createEventEV00022BodyType('','','');
        TROEventCapture.eventEV00023BodyType body_event0023 = new TROEventCapture.eventEV00023BodyType();
        body_event0023 = TROEventCapture_Builder.createEventEV00023BodyType('','','','');

		//header
		TROEventCapture.EventHeaderType header_event = new TROEventCapture.EventHeaderType();
        Datetime tempDateTime = System.now();
		header_event  = TROEventCapture_Builder.createEventHeaderType(tempDateTime, '', '', '', '');        
    }
}