@isTest
public class TestECCP_ReviewApprovalController {
	
    @isTest
    public static void noProductPackage(){
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, null, 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'Approved');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
    }
    
    @isTest
    public static void loadLocked(){
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Completed', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'Approved');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
    }
    
    @isTest
    public static void loadUnlocked(){
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
    }
    
    @isTest
    static void save() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.reviewSave();
        
        Test.stopTest();
    }
    
    @isTest
    public static void cancel() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.cancel();
            
        Test.stopTest();
    }
    
    @isTest
    public static void delegateAuthority() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getdelegatedAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority1() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority2() {
        Account testAccount = createAccount('10');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority3() {
        Account testAccount = createAccount('17');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void reviewer1() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getreviewer1();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer2() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getreviewer2();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer3() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getreviewer3();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer4() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getreviewer4();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer5() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getreviewer5();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer6() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Review__c testReview = createReview(testAccount.Id, 'In Progress');
        
        ApexPages.currentPage().getParameters().put('id', testReview.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_ReviewApprovalController RAC = new ECCP_ReviewApprovalController(controller);
        
        Test.startTest();
        
        RAC.getreviewer6();
        
        Test.stopTest();
    }
    
    private static Account createAccount(String grade) {
        Account newA = new Account(
        	Name = 'Test',
        	Proposed_Risk_Rating__c = grade);
        
        insert newA;
        return newA;
    }
    
    private static LLC_BI__Product_Package__c createProductPackage(Id acctId, Date aDate, String stage, String status) {
        LLC_BI__Product_Package__c newPP = new LLC_BI__Product_Package__c(
        	Name = 'Test',
        	LLC_BI__Account__c = acctId,
        	LLC_BI__Primary_Officer__c = UserInfo.getUserId(),
        	ECC_Product_Package_Approval_Date__c = aDate,
        	LLC_BI__Stage__c = stage,
        	LLC_BI__Status__c = status);
        
        insert newPP;
        return newPP;
    }
    
    private static LLC_BI__Review__c createReview(Id recordId, String status) {
        LLC_BI__Review__c newR = new LLC_BI__Review__c(
        	LLC_BI__Account__c = recordId,
        	LLC_BI__Status__c = status,
        	LLC_BI__Review_Type__c = 'Annual');
        
        insert newR;
        return newR;
    }
}