/*******************************************************************************************************************************************
* @name         :   ECCP_MCATriggerHandler
* @description  :   This class will be responsible for all the helper methods to create request, call the service and update response
* @author       :   Aditya
* @createddate  :   25/04/2016
*******************************************************************************************************************************************/
public class ECCP_MCATriggerHandler{
   // public static integer j =0;
    public static Map<String,ECCP_MCACheckRequestData> mcaData;
    //public static Map<String,ECCP_MCA_CheckMCI_Response> responseDataMap = new Map<String,ECCP_MCA_CheckMCI_Response>();
    public static ECCP_MCA_CheckMCI_Response response;
    public static List<ECCP_Compliance__c> compDataList;
    //Entity Id, response map
    public static Map<String,ECCP_Compliance__c> ecpComp = new Map<String,ECCP_Compliance__c>();
    public static string exceptionRecord;
    public static List<String> entInvolvement = new List<String>();
    public static List<Id> loanId = new List<Id>();
    public static List<Id> entityId = new List<Id>();
    public static boolean triggerFire = true;
    public static boolean retry;
    public static Map<String,ECCP_MCA_CheckMCI_Response> responseDataMp = new Map<String,ECCP_MCA_CheckMCI_Response>();
    public static Integer serviceCalls;
    public map<String,ECCP_ServiceRetry__mdt> serviceRetry = new map<String,ECCP_ServiceRetry__mdt>();
    public static list<ECCP_PerformanceLog__c> plogList = new list<ECCP_PerformanceLog__c>();
    public ECCP_PerformanceLog__c plog;
    public static map<id,ECCP_Credit_Bureau__c> creditBureauMap = new map<id,ECCP_Credit_Bureau__c>();
    public static Map<String,ECCP_WebService_Data__mdt> mapWsSettings;
    public static map<String,ECCP_ServiceRetry__mdt> serviceRetry;
    public static string loanOwnerRacfId;
/*******************************************************************************************************************************************
* @name         :   preventTriggerRecurson
* @description  :   This Method will be responsible to prevent recursive trigger.
* @author       :   Aditya
* @createddate  :   25/04/2016
*******************************************************************************************************************************************/
    
    public static void preventTriggerRecurson(){
        triggerFire = false;
    }
/*******************************************************************************************************************************************
* @name         :   preventTriggerRecurson
* @description  :   Method to query Loan and other data. This method will also prevent to run service for which service ran successfully
                    previously.
* @author       :   Aditya
* @createddate  :   25/04/2016
*******************************************************************************************************************************************/    
    @Future(callout=true)
    public static void serviceCallOut(List<String> loandIds){

        try{
            //2.0 Changes
            List<LLC_BI__Legal_Entities__c> entData = [select id,LLC_BI__Loan__r.LLC_BI__Product_Package__r.Bank_Division__c,LLC_BI__Loan__r.OwnerId,LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c,
                                             LLC_BI__Loan__r.ECCP_Application_Number__c,LLC_BI__Account__c,LLC_BI__Account__r.BillingStreet,LLC_BI__Account__r.BillingCity,
                                             LLC_BI__Account__r.BillingState,LLC_BI__Account__r.BillingPostalCode,LLC_BI__Account__r.BillingCountry,LLC_BI__Account__r.Birthdate__c,
                                             LLC_BI__Loan__r.ECCP_Credit_Bureau_ID__c,LLC_BI__Loan__r.ECCP_Credit_Bureau_Request_Type__c,LLC_BI__Account__r.ECC_Driver_s_License_Issue_Date__c,
                                             LLC_BI__Account__r.ECC_Driver_s_License_State__c,LLC_BI__Account__r.ECC_Driver_s_License_Number__c,LLC_BI__Loan__r.ECC_APP_ID__c,
                                             LLC_BI__Account__r.Home_Phone__c,LLC_BI__Account__r.Mobile_Phone__c,LLC_BI__Account__r.ECC_US_Citizenship_Indicator__c,
                                             LLC_BI__Account__r.FirstName__c,LLC_BI__Account__r.recordType.Name, LLC_BI__Account__r.Organization_Type__c,
                                             LLC_BI__Account__r.LastName__c,LLC_BI__Account__r.ECCP_US_Citizenship_Indicator__c,LLC_BI__Loan__r.ECCP_Credit_Bureau_Instructions__c,
                                             LLC_BI__Account__r.Name,ECCP_Owner_RACFID__c,LLC_BI__Loan__r.LLC_BI__Loan_Officer__r.Email,LLC_BI__Loan__c,LLC_BI__Account__r.ECC_Contact_Middle_Initial1__c,
                                             LLC_BI__Account__r.ECC_Drivers_s_License_Expire_Date__c, LLC_BI__Account__r.ECC_Passport_Issue_Date__c,
                                             LLC_BI__Account__r.ECC_Passport_Number__c,LLC_BI__Account__r.ECC_Passport_Country__c ,LLC_BI__Account__r.ECC_Passport_Expiry_Date__c,
                                             LLC_BI__Account__r.ECC_Other_Issue_Date__c ,LLC_BI__Account__r.ECC_Other_Identification_State__c , LLC_BI__Account__r.ECC_Other_Identification_Number__c,
                                             LLC_BI__Account__r.ECC_Other_Identification_Expiry_Date__c ,LLC_BI__Account__r.ECC_Other_Identification_Type__c ,
                                             LLC_BI__Account__r.Phone,
                                             (select Credit_Bureau_ID__c, Credit_Bureau_FA_Request_Type__c,Bureau_Instructions_1__c, Bureau_Instructions_2__c,Credit_Bureau_Address_Discrepancy__c From ECCP_Credit_Bureaues__r ORDER BY CreatedDate DESC Limit 1) 
                                             From LLC_BI__Legal_Entities__c
                                             where LLC_BI__Loan__c In : loandIds];                                                                      
            for( LLC_BI__Legal_Entities__c entIns : entData ) {
                  for( ECCP_Credit_Bureau__c brIns : entIns.ECCP_Credit_Bureaues__r ) {
                        creditBureauMap.put(entIns.id,brIns);
                  }
            }
            if(!entData.isEmpty()){
                loanOwnerRacfId = [select FederationIdentifier from user where id = : userInfo.getUserId()].FederationIdentifier;
            }
            if(entData !=null && entData.size()>0){
                serviceRetry = ECCP_CustomMetadataUtil.retyMechanism(ECCP_IntegrationConstants.MCACheckMCIWS);
                mapWsSettings  = ECCP_CustomMetadataUtil.queryMetadata(ECCP_IntegrationConstants.MCACheckMCIWS);
                mcaData = createData(entData);
                system.debug('----MCAData'+MCAData);
                
                if(entInvolvement !=null && entInvolvement.size()>0){
                    compDataList = [select id,ECCP_Entity_Involvement__c,Loan_Name__c,ECCP_MCA_Service_Succes__c from ECCP_Compliance__c where Loan_Name__c In : loandIds
                                    and ECCP_Entity_Involvement__c In : entInvolvement and ECCP_MCA_Service_Succes__c = true ];
                }
                if(compDataList !=null && compDataList.size()>0 && mcaData != null && mcaData.size()>0){
                    for(ECCP_Compliance__c compData : compDataList){
                        string key = compData.ECCP_Entity_Involvement__c +'-'+compData.Loan_Name__c;
                        mcaData.remove(key);
                    }
                }
                system.debug('!!!!!'+mcaData);
                if(mcaData != null && mcaData.size()>0){
                    retry = false; 
                    serviceCalls = mcaData.size();
                    
                    newServiceCall(retry);
                }
            }
        }
        catch(Exception ex){
            Map<String,ECCP_Error_Code__mdt>  errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.dmlException);
            ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.serviceMCA , 'ECCP_MCATriggerHandler', 'serviceCallOut', String.valueOf(ex.getLineNumber()),ex.getMessage(), exceptionRecord,ex.getStackTraceString());
            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
        
        }
    }
/*******************************************************************************************************************************************
* @name         :   preventTriggerRecurson
* @description  :   Method to call the service and retry logic, if service fails. In case of any error code in try catch will run.
                    Code will run service again. After every successful run or unsuccessful run twice data will be removed from the map
                    and service will be called for next entity in the map. Hence, ensuring code will run for all the entities even if there
                    is exception for one of them.
* @author       :   Aditya
* @createddate  :   25/04/2016
*******************************************************************************************************************************************/
    public static void newServiceCall(boolean serviceCallCheck){
                // call the service helper class and pass mcaData
                try{
                    if(mcaData != null && mcaData.size()>0){
                      System.debug('---Service Call--');
                       Map<String,ECCP_MCA_CheckMCI_Response> responseDataMap = new Map<String,ECCP_MCA_CheckMCI_Response>();
                       system.debug('----serviceCalls'+serviceCalls);
                       responseDataMap = serviceRequest(mcaData,retry,exceptionRecord);
                       
                       ecpComp.putAll(handleReponse(responseDataMap));
                       if(ecpComp !=null && ecpComp.size()>0){
                            system.debug('---insert call---');
                            insertResponse(ecpComp);
                            
                        }
                    }
                }
                catch(Exception ex){
                    
                    system.debug('----in Catch1');  
                    // Retry for failed and Calling service for rest on the entities
                    //try{
                    //map<String,ECCP_ServiceRetry__mdt> serviceRetry = ECCP_CustomMetadataUtil.retyMechanism(ECCP_IntegrationConstants.MCACheckMCIWS);
                    if(retry == false && serviceRetry.get(ECCP_IntegrationConstants.MCACheckMCIWS).isRetrySet__c){
                        system.debug('----in retry catch');
                        retry = true;
                        newServiceCall(retry);
                    }
                    else{
                    // If retry mechanism is controlled by custom setting then remove the record caused exception to prevent retry for the record.
                        mcaData.remove(exceptionRecord);
                    }
                    
                    retry = false;
                    system.debug('----in Catch2 retry--'+retry);
                    // insert the exception response
                    ECCP_Compliance__c excp = new ECCP_Compliance__c();
                    
                    system.debug('----ex.getMessage()---'+ex.getMessage());
                    excp.MCA_Error_Description__c = ex.getMessage();
                    //Enter error code
                    //excp.MCA_Error_Code__c = ex.
                    excp.ECCP_Entity_Involvement__c = exceptionRecord.split('-')[0];
                    // Change the error type
                    excp.ECCP_MCA_Error_Type__c = 'MCA Service Not Available/ Not Responding';
                    if(ex.getMessage()!= NULL){
                        if(ex.getMessage().contains('DB2 Error')){
                            system.debug('ex.getMessage()' +ex.getMessage());
                        }
                    }
                    excp.Loan_Name__c = exceptionRecord.split('-')[1];
                    excp.Compliance_Id__c = exceptionRecord;
                    excp.ECCP_MCA_Service_Succes__c = false;
                    
                    ecpComp.put(exceptionRecord,excp);
                    system.debug('---exception---'+ecpComp.keySet());
                    if(responseDataMp !=null && responseDataMp.size()>0){
                        ecpComp.putAll(handleReponse(responseDataMp));
                    }
                    //responseDataMap.put(exceptionRecord,excp);
                    loanId.add(exceptionRecord.split('-')[1]);
                    entityId.add(exceptionRecord.split('-')[0]);
                    
                    
                    Map<String,ECCP_Error_Code__mdt>  errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullResponse);
                    ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.serviceMCA , 'ECCP_MCATriggerHandler', 'newServiceCall', String.valueOf(Ex.getLineNumber()),Ex.getMessage(), exceptionRecord,Ex.getStackTraceString());
                    newServiceCall(retry);
                    try{
                    system.debug('---exception ecpComp size---'+ecpComp.size());
                        if(ecpComp !=null && ecpComp.size()>0 && ecpComp.size() == serviceCalls){
                            system.debug('---insert call from try---');
                            insertResponse(ecpComp);
                            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
                        }
                        
                    }
                    catch(exception e){
                        system.debug('----catch3');
                        errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullResponse);
                        ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.serviceMCA , 'ECCP_MCATriggerHandler', 'newServiceCall', String.valueOf(e.getLineNumber()),e.getMessage(), exceptionRecord,e.getStackTraceString());
                        ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
                    }
                   
                   
                }
                
        
        
    }
/*******************************************************************************************************************************************
* @name         :   preventTriggerRecurson
* @description  :   This method will create input for service
* @author       :   Aditya
* @createddate  :   25/04/2016
*******************************************************************************************************************************************/
    
    public static  Map<String,ECCP_MCACheckRequestData> createData(List<LLC_BI__Legal_Entities__c> entData){
        MAP<String, ECCP_MCACheckRequestData > MCADataMap = new MAP<String, ECCP_MCACheckRequestData >();
        String key;
        for(LLC_BI__Legal_Entities__c ent : entData){
            ECCP_MCACheckRequestData requestData = new ECCP_MCACheckRequestData();
            boolean indOrBus = false;
            indOrBus = (ent.LLC_BI__Account__r.Organization_Type__c == ECCP_IntegrationConstants.accIndividualRecordType || ent.LLC_BI__Account__r.Organization_Type__c == ECCP_IntegrationConstants.SoleProprietorship) ? true : false;
            
            requestData.CustType= indOrBus == false ? ECCP_IntegrationConstants.custTypeBusiness  : ECCP_IntegrationConstants.custTypeIndividual ;
            requestData.ProdCd = ECCP_IntegrationConstants.ProdCd;          
            requestData.LOB = ECCP_IntegrationConstants.LOB;
            requestData.SSN = ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c;
            // Sub Product Code need to ask Rajeev
            //Hardcode to “Facing” or “NonFacing” based on SB2 logic (pending verification of logic from Key)
            requestData.SubProd = ECCP_IntegrationConstants.Facing;
            // Made requestData.AcctNbr ='' for requirement change in defect 382
           // requestData.AcctNbr= ent.LLC_BI__Account__c;
            requestData.AcctNbr= '';
            requestData.ApplNbr= ent.LLC_BI__Loan__r.ECC_APP_ID__c ;
            requestData.BtchOnln= ECCP_IntegrationConstants.BatchorRealTime;
            requestData.Channel= ECCP_IntegrationConstants.channel;
            // Defect 411 : updated to ECCP_US_Citizenship_Indicator__c 
            //requestData.Citizen= ent.LLC_BI__Account__r.ECC_US_Citizenship_Indicator__c;   
            requestData.Citizen= ent.LLC_BI__Account__r.ECCP_US_Citizenship_Indicator__c == true ? 'Y' : 'N'; 
            system.debug('--- requestData.Citizen--'+ requestData.Citizen);  
            //CR-0017
            //requestData.CntctPh = ent.LLC_BI__Account__r.Home_Phone__c != null ? ent.LLC_BI__Account__r.Home_Phone__c.remove('(').remove(')').remove('-').deleteWhitespace() : ( ent.LLC_BI__Account__r.Mobile_Phone__c != null ? ent.LLC_BI__Account__r.Mobile_Phone__c.remove('(').remove(')').remove('-').deleteWhitespace() : '');
            Boolean homePhoneCheck = false;
            Boolean phoneCheck = false;
            if(ent.LLC_BI__Account__r.Phone != null ){
            string formattedPhone = ent.LLC_BI__Account__r.Phone != null ? ent.LLC_BI__Account__r.Phone.remove('(').remove(')').remove('-').deleteWhitespace() : '';
            Integer phoneLength = Integer.valueOf(formattedPhone.length()); 
                if(phoneLength >= 10 ){
                    requestData.PAreaCd = formattedPhone.substring(0,3);
                    requestData.PExchgCd = formattedPhone.substring(3,6);
                    requestData.PRootNbr = formattedPhone.substring(6,phoneLength);
                    phoneCheck = true;
                }
            }
            if(ent.LLC_BI__Account__r.Home_Phone__c !=null){
                string formattedHomePhone = ent.LLC_BI__Account__r.Home_Phone__c != null ? ent.LLC_BI__Account__r.Home_Phone__c.remove('(').remove(')').remove('-').deleteWhitespace() : '' ;
                Integer homePhoneLength = Integer.valueOf(formattedHomePhone.length());
                if(homePhoneLength >= 10 && (ent.LLC_BI__Account__r.Phone == null )){
                    requestData.PAreaCd = formattedHomePhone.substring(0,3);
                    requestData.PExchgCd = formattedHomePhone.substring(3,6);
                    requestData.PRootNbr = formattedHomePhone.substring(6,homePhoneLength);
                    homePhoneCheck = true;
                }
                if(ent.LLC_BI__Account__r.Phone != null && homePhoneLength >=10){
                    requestData.SAreaCd = formattedHomePhone.substring(0,3);
                    requestData.SExchgCd = formattedHomePhone.substring(3,6);
                    requestData.SRootNbr = formattedHomePhone.substring(6,homePhoneLength);
                    phoneCheck = false;
                    
                }
            }
            if(ent.LLC_BI__Account__r.Mobile_Phone__c !=null){
                string formattedMobilePhone = ent.LLC_BI__Account__r.Mobile_Phone__c != null ? ent.LLC_BI__Account__r.Mobile_Phone__c.remove('(').remove(')').remove('-').deleteWhitespace() : '' ;
                Integer mobilePhoneLength = Integer.valueOf(formattedMobilePhone.length()); 
                if(mobilePhoneLength >= 10 && ent.LLC_BI__Account__r.Phone == null && ent.LLC_BI__Account__r.Home_Phone__c == null ){
                    requestData.PAreaCd = formattedMobilePhone.substring(0,3);
                    requestData.PExchgCd = formattedMobilePhone.substring(3,6);
                    requestData.PRootNbr = formattedMobilePhone.substring(6,mobilePhoneLength);
                }
                if((ent.LLC_BI__Account__r.Phone != null && phoneCheck && mobilePhoneLength >=10) || (ent.LLC_BI__Account__r.Home_Phone__c != null &&  homePhoneCheck && mobilePhoneLength >=10)){
                    requestData.SAreaCd = formattedMobilePhone.substring(0,3);
                    requestData.SExchgCd = formattedMobilePhone.substring(3,6);
                    requestData.SRootNbr = formattedMobilePhone.substring(6,mobilePhoneLength);
                }
            }
            //CR -0017 change end
            requestData.BusName= indOrBus == false  ?  ent.LLC_BI__Account__r.Name : '';
            // Commented if block as No Contact fields will be part of MCA
           //if(ent.LLC_BI__Account__r.Contact_Name__r != NULL){
            requestData.FrstName= indOrBus == true  ? ent.LLC_BI__Account__r.FirstName__c :'';
            requestData.LastName= indOrBus == true   ? ent.LLC_BI__Account__r.LastName__c :'';
            requestData.MdlName = indOrBus == true   ? ent.LLC_BI__Account__r.ECC_Contact_Middle_Initial1__c : '';  
            //}   
            requestData.AddrL1=ent.LLC_BI__Account__r.BillingStreet != NULL ? (ent.LLC_BI__Account__r.BillingStreet.trim().length() > 40 ? ent.LLC_BI__Account__r.BillingStreet.trim().substring(0,40) : ent.LLC_BI__Account__r.BillingStreet.trim()) : '' ;
            requestData.AddrL2=ent.LLC_BI__Account__r.BillingStreet != NULL ? (ent.LLC_BI__Account__r.BillingStreet.trim().length() > 80 ? ent.LLC_BI__Account__r.BillingStreet.trim().substring(40,80) : (ent.LLC_BI__Account__r.BillingStreet.trim().length() > 40 ? ent.LLC_BI__Account__r.BillingStreet.trim().substring(40,ent.LLC_BI__Account__r.BillingStreet.trim().length()) : '')) : '' ;
            requestData.AddrL3=ent.LLC_BI__Account__r.BillingStreet != NULL ? (ent.LLC_BI__Account__r.BillingStreet.trim().length() > 120 ? ent.LLC_BI__Account__r.BillingStreet.trim().substring(80,120) : (ent.LLC_BI__Account__r.BillingStreet.trim().length() > 80 ? ent.LLC_BI__Account__r.BillingStreet.trim().substring(80,ent.LLC_BI__Account__r.BillingStreet.trim().length()) : '')) : '' ;  
            requestData.AddrL4=ent.LLC_BI__Account__r.BillingStreet != NULL ? (ent.LLC_BI__Account__r.BillingStreet.trim().length() > 160 ? ent.LLC_BI__Account__r.BillingStreet.trim().substring(120,160) : (ent.LLC_BI__Account__r.BillingStreet.trim().length() > 120 ? ent.LLC_BI__Account__r.BillingStreet.trim().substring(120,ent.LLC_BI__Account__r.BillingStreet.trim().length()) : '')) : '' ;          
            requestData.City=ent.LLC_BI__Account__r.BillingCity != NULL ? (ent.LLC_BI__Account__r.BillingCity.trim().length() > 20 ? ent.LLC_BI__Account__r.BillingCity.trim().substring(0,20) : ent.LLC_BI__Account__r.BillingCity.trim()) : '' ;            
            //requestData.State=ent.LLC_BI__Account__r.BillingState != NULL ? (ent.LLC_BI__Account__r.BillingState.trim().length() > 2 ? ent.LLC_BI__Account__r.BillingState.trim().substring(0,2) : ent.LLC_BI__Account__r.BillingState.trim()) : '' ;
            // CR -0018
            requestData.State=ent.LLC_BI__Account__r.BillingState;
            requestData.Zip=ent.LLC_BI__Account__r.BillingPostalCode;
            requestData.Country=ent.LLC_BI__Account__r.BillingCountry != NULL ? (ent.LLC_BI__Account__r.BillingCountry.trim().length() > 3 ? ent.LLC_BI__Account__r.BillingCountry.trim().substring(0,3) : ent.LLC_BI__Account__r.BillingCountry.trim()) : '' ;
            requestData.DOB=ent.LLC_BI__Account__r.Birthdate__c != NULL ? String.valueOf(ent.LLC_BI__Account__r.Birthdate__c).remove('-') : '';
            // no field related to license number : may need to be changed
            requestData.DrvLcNbr=ent.LLC_BI__Account__r.ECC_Driver_s_License_Number__c;
            requestData.DrvLcSt=ent.LLC_BI__Account__r.ECC_Driver_s_License_State__c; 
            requestData.DrvIssDt=ent.LLC_BI__Account__r.ECC_Driver_s_License_Issue_Date__c != NULL ?  String.valueOf(ent.LLC_BI__Account__r.ECC_Driver_s_License_Issue_Date__c).remove('-') : '';
            // 2.0 Change
            requestData.DrvExpDt = ent.LLC_BI__Account__r.ECC_Drivers_s_License_Expire_Date__c != null ? String.valueOf(ent.LLC_BI__Account__r.ECC_Drivers_s_License_Expire_Date__c).remove('-') : '';
            requestData.PsprtIss = ent.LLC_BI__Account__r.ECC_Passport_Issue_Date__c != null ? String.valueOf(ent.LLC_BI__Account__r.ECC_Passport_Issue_Date__c).remove('-') : '';
            requestData.PsprtNbr = ent.LLC_BI__Account__r.ECC_Passport_Number__c;
            requestData.PsprtCntr = ent.LLC_BI__Account__r.ECC_Passport_Country__c;
            requestData.PsprtExp = ent.LLC_BI__Account__r.ECC_Passport_Expiry_Date__c !=null ? String.valueOf(ent.LLC_BI__Account__r.ECC_Passport_Expiry_Date__c).remove('-') : '';
            requestData.OthIdIssDt = ent.LLC_BI__Account__r.ECC_Other_Issue_Date__c !=null ?  String.valueOf(ent.LLC_BI__Account__r.ECC_Other_Issue_Date__c).remove('-') : '';
            requestData.OthIdNbr = ent.LLC_BI__Account__r.ECC_Other_Identification_Number__c ;
            requestData.OthIdIssSt = ent.LLC_BI__Account__r.ECC_Other_Identification_State__c;
            requestData.OthIdExpDt = ent.LLC_BI__Account__r.ECC_Other_Identification_Expiry_Date__c !=null ?  String.valueOf(ent.LLC_BI__Account__r.ECC_Other_Identification_Expiry_Date__c).remove('-') : '';
            requestData.OthIdTyp = ent.LLC_BI__Account__r.ECC_Other_Identification_Type__c;
            requestData.MdlName = ent.LLC_BI__Account__r.ECC_Contact_Middle_Initial1__c;
            
            requestData.UserId = (loanOwnerRacfId != NULL && loanOwnerRacfId != '') ? (loanOwnerRacfId.length() > 7? loanOwnerRacfId.substring(0,7) :loanOwnerRacfId) : NULL;
            
           // requestData.BureauID = string.ValueOf(creditBureauMap.get(ent.id).Credit_Bureau_ID__c) != NULL ?string.ValueOf(creditBureauMap.get(ent.id).Credit_Bureau_ID__c).trim().substring(0,6) : '' ;
            if(creditBureauMap.get(ent.id)!= NULL ){
                requestData.BureauID = string.ValueOf(creditBureauMap.get(ent.id).Credit_Bureau_ID__c) ;
                requestData.BureauFA=creditBureauMap.get(ent.id).Credit_Bureau_FA_Request_Type__c;
                requestData.AddDescInd=creditBureauMap.get(ent.id).Credit_Bureau_Address_Discrepancy__c;
                requestData.BureauInstruct1=creditBureauMap.get(ent.id).Bureau_Instructions_1__c; 
                requestData.BureauInstruct2=creditBureauMap.get(ent.id).Bureau_Instructions_2__c;
            }
            //requestData.BureauFA=ent.LLC_BI__Loan__r.ECCP_Credit_Bureau_Request_Type__c;
            requestData.ReteMailAddr= ent.LLC_BI__Loan__r.LLC_BI__Loan_Officer__r.Email != NULL ? (ent.LLC_BI__Loan__r.LLC_BI__Loan_Officer__r.Email.trim().length() > 80 ? ent.LLC_BI__Loan__r.LLC_BI__Loan_Officer__r.Email.trim().substring(0,80) : ent.LLC_BI__Loan__r.LLC_BI__Loan_Officer__r.Email.trim()): '';
            //requestData.BureauInstruct1=ent.LLC_BI__Loan__r.ECCP_Credit_Bureau_Instructions__c;
            // key is combination of EntityId-LoandId
            key = ent.Id+'-'+ent.LLC_BI__Loan__c;
            entInvolvement.add(ent.Id);
            MCADataMap.put(key,requestData);
            system.debug('----MCADataMap'+MCADataMap);
        }
        return MCADataMap;
    }
/*******************************************************************************************************************************************
* @name         :   preventTriggerRecurson
* @description  :   This method will call the service.
* @author       :   Aditya
* @createddate  :   25/04/2016
*******************************************************************************************************************************************/    

    public static Map<String,ECCP_MCA_CheckMCI_Response> serviceRequest(Map<String,ECCP_MCACheckRequestData> serviceInput,boolean retryCode,String excpRecord){
        
        Integer i;
        
        
        //Map<String,ECCP_MCA_CheckMCI_Response> responseDataMp = new Map<String,ECCP_MCA_CheckMCI_Response>();
        Boolean removeKey = true;
        for( String key : serviceInput.keySet() ){
            if(exceptionRecord !=null && exceptionRecord !=''){
                if(retryCode && exceptionRecord == excpRecord){
                    mcaData.remove(key);
                    //retry = false;
                    removeKey = false;
                }
            }
            exceptionRecord = key;
            system.debug('----exceptionRecord'+exceptionRecord);
            //Map<String,ECCP_WebService_Data__mdt> mapWsSettings  = ECCP_CustomMetadataUtil.queryMetadata(ECCP_IntegrationConstants.MCACheckMCIWS);  
            ECCP_PerformanceLog__c plog = new ECCP_PerformanceLog__c(); 
            if(mapWsSettings.get(ECCP_IntegrationConstants.MCACheckMCIWS).isPerfLogRequired__c){
                plog = ECCP_UTIL_PerformanceLogCheck.createPerformanceLog(ECCP_IntegrationConstants.MCACheckMCIWS, mapWsSettings.get(ECCP_IntegrationConstants.MCACheckMCIWS).EndPointUrl__c,ECCP_IntegrationConstants.MCAProcessRequest ,JSON.Serialize(serviceInput.get(key)));            
            }
            
            if(!Test.isRunningTest()){
                ECCP_MCA_WS_CheckHelper mca = new ECCP_MCA_WS_CheckHelper();
                system.debug('++++input+++' +JSON.serialize(serviceInput.get(key)));
                response = mca.ECCP_MCACheckMCIProcess(serviceInput.get(key));
                } 
            else{
            response = ECCP_MCAWebSvcCalloutTest.testMycalloutreturn();
            
                }
            
            if(mapWsSettings.get(ECCP_IntegrationConstants.MCACheckMCIWS).isPerfLogRequired__c){
               plogList.add(ECCP_UTIL_PerformanceLogCheck.updatePerformanceLog(plog, JSON.serialize(response )));
            }
           
            responseDataMp.put(key,response);
            system.debug('----responseDataMp---'+responseDataMp);
            // 29 June 2016: Added to cover exception in test class. 
            if(Test.isRunningTest()){
              //   response = ECCP_MCAWebSvcCalloutTest.testMycalloutreturnExceptionCase();
              //   system.debug('---exception test cover---'+response);
                   Test.setMock(WebServiceMock.class, new ECCP_MCA_WebServiceMockImplExceptionCase());
    
                    ECCP_MCACheckRequestData req = new ECCP_MCACheckRequestData();
                    req.CustNbr = '123456';
                    req.SupplCode ='MCA567';
                    req.SSN ='BNNAS8KMM8';
                    req.EmprNbr = '789';
                    
                    ECCP_MCA_CheckMCI_Response Myresp = ECCP_MCAWebSvcCallout.Mycalloutreturn(req);
                    responseDataMp.put(key,Myresp);
               
            }
            retry = false;
            // remove only those entities for which service ran
            if(removeKey){
                mcaData.remove(key);
            }
            
        }
        return responseDataMp;
        
    }
/*******************************************************************************************************************************************
* @name         :   preventTriggerRecurson
* @description  :   This method will create object to be stored from response.
* @author       :   Aditya
* @createddate  :   25/04/2016
*******************************************************************************************************************************************/
 
    public static Map<String,ECCP_Compliance__c> handleReponse(Map<String,ECCP_MCA_CheckMCI_Response> respMap){
        system.debug('---response---');
        for(String key : respMap.keySet()){
            ECCP_Compliance__c comp = new ECCP_Compliance__c();
            comp.MCA_Reference_ID__c = respMap.get(key).CIURefNb;
            comp.FraudActiveAlert__c = respMap.get(key).FraudActiveAlert;
            comp.FAAlert__c = respMap.get(key).FAAlertPass;
            comp.HotFile__c = respMap.get(key).HotFilePass;
            comp.OFAC__c = respMap.get(key).OFACPass;
            comp.PA326__c = respMap.get(key).PA326Pass;
            comp.RegO__c = respMap.get(key).RegOPass;
            comp.RegW__c = respMap.get(key).RegWPass;
            comp.AddressDiscrepancyAlert__c = respMap.get(key).AddressAlert;
            if((respMap.get(key).ErrorCd =='' || respMap.get(key).ErrorCd ==null )&& (respMap.get(key).ErrorDsc =='' || respMap.get(key).ErrorDsc ==null) && (respMap.get(key).Result !='' && respMap.get(key).Result != 'Pass' && respMap.get(key).Result != 'Y')){
            comp.ECCP_MCA_Service_Succes__c = true;
            }
            
            comp.MCA_Error_Code__c = respMap.get(key).ErrorCd;
            comp.MCA_Error_Description__c = respMap.get(key).ErrorDsc;
            comp.ECCP_MCA_Error_Type__c = respMap.get(key).ErrorTp;
            
            comp.MCA_Proceed_Code__c = respMap.get(key).Proceed;
            
            comp.MCAStatus__c = respMap.get(key).Pass == 'Y' ? 'PASS' : 'PEND';
            comp.OFACResult__c = respMap.get(key).OFACResult;
            comp.ECCP_Entity_Involvement__c = key.split('-')[0];
            
            comp.Loan_Name__c = key.split('-')[1];
            comp.Compliance_Id__c = key;
            ecpComp.put(key,comp);
            
            loanId.add(key.split('-')[1]);
            entityId.add(key.split('-')[0]);
            
        }
        system.debug('----ecpComp---'+ecpComp.keySet());
        return ecpComp;
        
    }
/*******************************************************************************************************************************************
* @name         :   preventTriggerRecurson
* @description  :   This method will upsert data.
* @author       :   Aditya
* @createddate  :   25/04/2016
*******************************************************************************************************************************************/

    public static void insertResponse(Map<String,ECCP_Compliance__c> dmlMap){
        system.debug('---dmlMap in InsertResponse---'+dmlMap.keySet());
        List<ECCP_Compliance__c> updateList = new List<ECCP_Compliance__c>();
        List<ECCP_Compliance__c> existingComplianceList;
        List<ECCP_Compliance__c> compData;
        string key;
        //Adding Performance log
        if(!plogList.isEmpty()){
            insert plogList;
        }
        try{
            if(dmlMap !=null && dmlMap.size()>0 && loanId != null && loanId.size()>0 && entityId!=null && entityId.size()>0){
                upsert dmlMap.values() Compliance_Id__c;
            }
            
           
            
            if(ecpComp !=null && ecpComp.size()>0){
                ecpComp.clear();
            }
        }
        catch(exception e){
            system.debug('----catch4');
            Map<String,ECCP_Error_Code__mdt>  errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullResponse);
            errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullResponse);
            ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.serviceMCA , 'ECCP_MCATriggerHandler', 'newServiceCall', String.valueOf(e.getLineNumber()),e.getMessage(), exceptionRecord,e.getStackTraceString());
            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
        }                
    }
}