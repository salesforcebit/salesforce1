//================================================================================
//  KeyBank  
//  Object: Opportunity
//  Author: Offshore
//  Detail: OpportunityProductTriggerHandler
//  Description : To RestrictUser delete the PrimaryProduct

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================



public with sharing class OpportunityProductTriggerHandler {
    

 public static Boolean flagcheck=false;
    
//Method  to Restrict user to delete opportunity product if primary product is checked    
    public static void DeleteOLI(OpportunityLineItem[] triggerOldList)
    {
        
        for(OpportunityLineItem OLI:triggerOldList)
        {
            system.debug('Value of OLI----'+OLI.Primary_product__c);
            system.debug('OLI.Opportunity_Record_Type__c:'+OLI.Opportunity_Record_Type__c);
           if( OLI.Primary_product__c==true && ( OLI.Opportunity_Record_Type__c== 'KEF Opportunity' || OLI.Opportunity_Record_Type__c=='KEF Oppty - Submitted')){
                OLI.Primary_product__c.adderror(system.label.Primaryvalue);
            }
        }
        
    }   

//Method to check/uncheck the value of primary product
   /* public static void UpdatePrimaryProduct(OpportunityLineItem[] triggernewList){
        
        List<OpportunityLineItem> OLInlist = [select id,Name,Primary_product__c from OpportunityLineItem
                                                 where Primary_product__c=true ];
        List<OpportunityLineItem> OLInlist1=new List<OpportunityLineItem>();
        system.debug('list size---'+OLInlist);
        for(OpportunityLineItem OLItem:triggernewList)
        {
            if(OLInlist.size()>0)
            {
                
                for(OpportunityLineItem OLItem1:OLInlist)
                {
                    if(OLItem.Primary_product__c==true)
                    {
                        OLItem1.Primary_product__c= false;
                        system.debug('lineitem value---'+OLItem1.Primary_product__c);
                        OLInlist1.add(OLItem1);
                    }
                }
                    update OLInlist1;
            }
          }
       }
   }*/
   }     
        
    /*  for(OpportunityLineItem OLItem:triggernewList)
        {
            if(OLItem.Primary_product__c){
                system.debug('valueoli---'+OLItem.Primary_product__c);
                flagcheck=true;
                break;
            }
        }
        
        if(!flagcheck)
        system.debug('flagcheckvalue---'+flagcheck);
        flagcheck=false;
        for(OpportunityLineItem OLItem:triggernewList){
            if(OLItem.Primary_product__c){
                if(flagcheck)
                system.debug('flagcheckvalue11---'+flagcheck);
                OLItem.Primary_product__c = false;
            }
            
            else
            {
                flagcheck = true;
            }
    
        }
        
        List<OpportunityLineItem> UpdatePrimaryProduct = new List<OpportunityLineItem>();
        for(OpportunityLineItem OLItem : [select id,Name,Primary_product__c from OpportunityLineItem WHERE Id NOT IN :triggernewList]){
        OLItem.Primary_product__c = false;
        system.debug('pp value--'+OLItem.Primary_product__c);
        UpdatePrimaryProduct.add(OLItem);
    } 

        update UpdatePrimaryProduct;*/