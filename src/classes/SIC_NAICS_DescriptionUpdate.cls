//================================================================================
//  KeyBank  
//  Object: SIC & NAICS
//  Author: Offshore
//  Detail: SIC_NAICS_DescriptionUpdate
//  Description : Automate the SIC and NAICS Desc value based on the code.

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================

Public class SIC_NAICS_DescriptionUpdate{

    public static void accupdate(List<Account> acclist,List<string> Lsic,List<String> LNaics)
    {
        //Create a map reference which will store custom setting values
        Map<String,String> SICMap = new Map<String, String>();
        Map<String,String> NAICMap = new Map<String, String>();    
    
        //fetch the code and description from the custom setting
        List<SIC_NAICS__c> sicDescList=[Select id,code__c,description__C from SIC_NAICS__c where code_type__c='SIC' and code__c IN: Lsic ];
        List<SIC_NAICS__c> NAICSDescList=[Select id,code__c,description__C from SIC_NAICS__c where code_type__c='NAICS' and code__c IN: LNaics];        
        
        for(SIC_NAICS__c sic :sicDescList)
        {
            SICMap.put(sic.code__c,sic.description__c);
        }
        for(SIC_NAICS__c NAIC :NAICSDescList)
        {
            NAICMap.put(NAIC.code__c,NAIC.Description__c);
        }    
     
        //for NAICS Code get the description value from the map & assign to SICDescription standards field
        for(Account a :acclist)
        {
            if(SICMap.containskey(a.sic) || (a.sic ==null))
            a.SicDesc =SICMap.get(a.sic);
            else
            a.addError('SIC Code does not exist');
        
            if((NAICMap.containskey(a.NaicsCode)) || (a.NaicsCode == null))
            a.NaicsDesc = NAICMap.get(a.NaicsCode);                   
            else
            a.addError('NAICS Code does not exist');
        
        }
     }
     
}