//================================================================================
//  KeyBank  
//  Object: Lead
//  Author: Offshore
//  Detail: LeadAccountsprmassociation_handler
//  Description : To associate a converted Account Id to the Program member. 
//                Automatic SIC and NAICS description based on Code.
//                Populate Initaiator Field automatically based on login user.

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================



Public class  LeadAccountsprmassociation_handler
{

    Public void LeadaccSNcodeupdate(Map<ID,lead> ld,List<string> Lsic)
    {
        List<Lead> leadWithacc = [select id, ConvertedAccountId, IsConverted, SIC_Code__c, SIC_Code_Description__c, NAICS_Code__c, NAICS_Description__c from lead where Id IN :ld.keySet()];
        Map<Id,Account> accmap = new Map<Id,Account>([Select Id, Name, sic, SicDesc, NaicsCode, NaicsDesc,type from Account  where Id IN :LSic  ]);
        try
        {
            for(Lead l: leadWithacc)
            {
                If(l.ConvertedAccountId!=null)
                {
                    account ac = accmap.get(l.ConvertedAccountId);
                     //account a = new account();                     
                    if(ac.sic == null)
                    {
                        ac.sic = l.SIC_Code__c;
                        ac.SicDesc = l.SIC_Code_Description__c;
                        update ac;
                    }
                    //Defect-545: when the Account ytpe is Business customer, the NAICS code need not to be brought to converted account.
                    if(ac.type != 'Business Customer' && ac.NaicsCode == null)
                    { 
                         ac.NaicsCode = l.NAICS_Code__c;
                         ac.NaicsDesc = l.NAICS_Description__c;
                         update ac;
                    }
                }
              }
           }              
        catch(NullPointerException e)
        {
            system.debug(e.getMessage());
        }
    }
//    ------------------------

    /* To associate a converted Account Id to the Program member. */
    Public void Leadaccountupdate(Map<ID,lead> ld)
    {
        List<Lead> leadWithprm = [select id,ConvertedAccountId,IsConverted,ConvertedOpportunityId,(select id,name,AccountId__c,LeadId__c,OpportunityName__c from Program_Member__r) from lead where Id IN :ld.keySet()];
        List<Program_Member__c> prmToUpdate = new List<Program_Member__c>();
        try
        {
            for(Lead l: leadWithprm)
            {
                If(l.ConvertedAccountId!=null)
                {
                    for(Program_Member__c pr: l.Program_Member__r)
                    {
                        pr.AccountId__c=l.ConvertedAccountId;
                        pr.OpportunityName__c=l.ConvertedOpportunityId;
                        prmToUpdate.add(pr);
                    }       
                }
                update prmToUpdate;
            }
        }
        catch(NullPointerException e)
        {
            system.debug(e.getMessage());
        }
        
    }
    
     Public void Leadcallupdate(List<Lead> Leadid)
    {
        List<Lead> leadWithcall = [select id,ConvertedAccountId,IsConverted from lead where Id in (select LeadId__c from Call__c ) and Id In : Leadid];
        List<Call__c> cUpdate = [select id,name,AccountId__c,LeadId__c,Associated_Account__c from Call__c where LeadId__c in : Leadid];
        try
        {
            for(Lead l: leadWithcall)
            {
                If(l.ConvertedAccountId!=null && l.IsConverted==true)
                {
                    for(Call__c c: cUpdate)
                    {
                       Call__c ca = new Call__c();
                        ca.AccountId__c=l.ConvertedAccountId;
                        ca.Associated_Account__c=l.ConvertedAccountId;
                        ca.id= c.id;
                        update ca;
                    } 
                       
                }
            }
        }
        catch(NullPointerException e)
        {
            system.debug(e.getMessage());
        }
    }
// ---------------------------    
    /* Automatic SIC and NAICS description based on Code. */    
    Public void Leadsicnaicupdate(List<lead> ldl)
    {
        Map<String,String> SICMap = new Map<String, String>();
        Map<String,String> NAICMap = new Map<String, String>();
        //fetch the code and description from the SIC & NAICS Custom Object
        List<SIC_NAICS__c> sicDescList=[Select id,code__c,description__C from SIC_NAICS__c where code_type__c='SIC'];
        List<SIC_NAICS__c> NAICSDescList=[Select id,code__c,description__C from SIC_NAICS__c where code_type__c='NAICS'];
        
        for(SIC_NAICS__c sic :sicDescList)
        {
            SICMap.put(sic.code__c,sic.description__c);
        }
        
        for(SIC_NAICS__c NAIC :NAICSDescList)
        {
            NAICMap.put(NAIC.code__c,NAIC.Description__c);
        }
        
        for(Lead l :ldl)
        {    
            if(SICMap.containskey(l.sic_code__c) || (l.sic_code__c ==null))
            l.SIC_Code_Description__c =SICMap.get(l.SIC_Code__c);
            
            else
            l.addError('SIC Code does not exist');
            
            if(NAICMap.containskey(l.NAICS_Code__c) || (l.NAICS_Code__c ==null))
            l.NAICS_Description__c = NAICMap.get(l.NAICS_Code__c);
            else
            l.addError('NAICS Code does not exist');
        
        
        }
    }
    
    /*Populate Initaiator Field automatically based on login user.*/
    Public void LeadInitiator(List<lead> LLead)
    {
        Map<Id,user> usermap =new Map<Id,user>([Select Id,Name,RACF_Number__c from user where isactive=true]);
        Set<String> userNames = new Set<String>();
        try
        {
          
            for(Lead Ld: LLead )
            {
                  system.debug(Ld.createdbyId);
                if(Ld.CreatedById != null && Ld.Initiator_Name__c==null && Ld.Leadsource == 'Internal Referral')
                {
                    User us = usermap.get(Ld.CreatedById);
                    userNames.add(us.RACF_Number__c);
                }
            }
        }
                    
        catch(NullPointerException e)
        {
            system.debug(e.getMessage());
        }
        
        if(userNames.size() > 0)
        {
            List<Contact> userNameContacts = [Select Id, Name,RACFID__c from Contact where RACFID__c IN : userNames and Contact_Type__c='Key Employee' and Salesforce_User__c=true ]; 
            //List<Lead> LLead1 = [select Id, OwnerId, Initiator_Name__c, Leadsource,CreatedById from Lead where id =: trigger.new];
            for(Lead Ld1: LLead)
            {
               if(Ld1.Initiator_Name__c==null)
               {
                User us = usermap.get(Ld1.CreatedById);
                Contact foundContact;
                for(Contact contact : userNameContacts)
                {
                    if(contact.RACFID__c == us.RACF_Number__c && contact.RACFID__c != null && us.RACF_Number__c != null)
                    {
                        foundContact = contact;
                        break;
                    }
                }
            
                if(foundContact != null  &&  Ld1.Leadsource == 'Internal Referral')
                {
                 Lead l = new lead();
                    l.Initiator_Name__c= foundContact.Id;
                    l.Id = Ld1.Id;
                 update l;
                }
            }
          }
        }
           
    }  
Public void Leadownerupdate(List<lead> LoLead)
{
List<User> Luser =[Select Id,Name,RACF_Number__c from user where isactive=true];
List<contact> Leadcontact=[Select Id,RACFID__c,User_Id__c from contact  where Contact_Type__c='Key Employee' and User_Id__c IN: Luser and Salesforce_User__c=true ];   
       Map<string,user> userLeadmap =new Map<string,user>();
       for(User u : Luser)
       {
        userLeadmap.put(u.RACF_Number__c,u);
       }
        for(Lead Ld2 : LoLead)
           {
            if(Ld2.Leadsource == 'Internal Referral' && Ld2.IsConverted==false)
            {
                for(contact Leadc : Leadcontact)
                {
                 if(Ld2.Lead_recipient__c==Leadc.Id)
                     {
                        Lead lou = new lead();
                        user lu = userLeadmap.get(Leadc.RACFID__c);
                        lou.id= Ld2.id;
                        lou.ownerId=lu.id;
                        update lou;
                     }
                }
              }
            }
        }
    private static boolean run = true;
    public static boolean runOnce(){
    if(run){
     run=false;
     return true;
    }else{
        return run;
    }
    }

}