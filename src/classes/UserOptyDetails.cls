/*
This class is used to count past due Opportunities and Unmodified Opportunities. 
*/
public with sharing class UserOptyDetails{
    
    public static Map<Id, Integer> UserPastDueOptyCounts(Set<id> userIds)
    {
        Map<Id, Integer> UserIdVsPastOptyCount = new Map<Id, Integer>();
        List<Opportunity> optylist= new List<Opportunity>();
         
        AggregateResult[] pastOptyCountList = [SELECT OwnerId, COUNT(Id) pastOppCount
                          FROM Opportunity 
                          WHERE OwnerId IN :userIds 
                          AND closedate < today 
                          AND (NOT stagename like 'Closed%') 
                          GROUP BY OwnerId]; 
        system.debug('pastOptyCountList ----'+pastOptyCountList );
     
        for(AggregateResult agr : pastOptyCountList ){
           UserIdVsPastOptyCount.put((Id)agr.get('OwnerId'), (Integer)agr.get('pastOppCount')); 
       }
       
        
        return UserIdVsPastOptyCount;  
   }  
   
    public static Map<Id, Integer> UserUnmodifiedOptyCounts(Set<id> userIds)
    {
        Map<Id, Integer> UserIdVsunmodifiedtOptyCount = new Map<Id, Integer>();
        List<Opportunity> optylist= new List<Opportunity>();
         
        AggregateResult[] unmodifiedOptyCountList = [select OwnerId, COUNT(Id) unmodifiedOppCount
                          from Opportunity 
                          where lastmodifieddate !=  LAST_N_DAYS:60 
                          AND OwnerId IN :userIds 
                          AND (NOT stagename like 'Closed%')
                          GROUP BY OwnerId]; 
        system.debug('unmodifiedOptyCountList ----'+unmodifiedOptyCountList );
     
        for(AggregateResult agr : unmodifiedOptyCountList ){
           UserIdVsunmodifiedtOptyCount .put((Id)agr.get('OwnerId'), (Integer)agr.get('unmodifiedOppCount')); 
       }
       
        
        return UserIdVsunmodifiedtOptyCount ;  
   }        
}