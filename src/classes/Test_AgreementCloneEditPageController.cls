@istest
Public class Test_AgreementCloneEditPageController
{
    public static testMethod void cloneTest()
    {
        Profile p = [select id from profile where name= 'System Administrator'];
        
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan1@keybank.com',
           emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='Test_User@Domain.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
        
        System.RunAs(testUser)
        {
            List<AgreementCloneHelper__c> achList = Test.loadData(AgreementCloneHelper__c.sObjectType, 'AgreementCloneHelperTestData');
            
            Agreement__c agr = new Agreement__c();
            agr = TestUtilityClass.CreateTestAgreement();
            agr.Agreement_Name_UDF__c = 'Test UDF Value';            
            insert agr;
            
            Account accRec= new Account();
            for(Account acc : TestUtilityClass.CreateTestAccount(1))
            {
                accRec = acc;
            }
            insert accRec;
            
            //inserting a new contact
            List<Contact> conList = new List<Contact>();
            for(Contact con : TestUtilityClass.createTestContacts(2, accRec.Id))
            {
                conList.add(con);
            }
            insert conList;
            
            //inserting contact agreement associations
            List<Contact_Agreement__c> caList = new List<Contact_Agreement__c>();
            for(Contact con : conList)
            {
                caList.add(TestUtilityClass.createCAAssociations(con.id, agr.Id));
            }
            insert caList;
            
            //inserting KEF Products
            List<KEF_Product__c> kefProds = new List<KEF_Product__c>();
            for(Product2 prod : TestUtilityClass.createProductsList())
            {
                kefProds.add(TestUtilityClass.createProductAssociations(agr.Id, prod.id));
            }
            insert kefProds;
            
            accRec.KEF_Risk_Role__c = System.Label.Vendor_Supplier;
            accRec.Vendor_status__c = System.Label.Pending_Credit;
            update accRec;
            
            Funding_Grid__c fg = TestUtilityClass.createFundingGrid(accRec.id);
            insert fg;
                        
            insert TestUtilityClass.createRecourses(agr.Id);        
            insert TestUtilityClass.createPayoffQuote(agr.Id);        
            insert TestUtilityClass.createPromotions(agr.Id);        
            insert TestUtilityClass.createVAAssociations(accRec.id, agr.Id);
            insert TestUtilityClass.createCustomReporting(agr.Id);    
            
            Test.StartTest();
            
            PageReference pageRef = Page.AgreementCloneEditPage;
            pageRef.getParameters().put('Id', agr.id);
            Test.setCurrentPageReference(pageRef);
            
            ApexPages.StandardController sc = new ApexPages.standardController(agr);
            AgreementCloneEditPageController ctrl = new  AgreementCloneEditPageController(sc);
            
            //covering the part where an error is thrown to user
            ctrl.clonedAgreementRec.Agreement_Name_UDF__c = 'Test UDF Value';
            ctrl.saveDetails();
            
            ctrl.clonedAgreementRec.Agreement_Name_UDF__c = 'UDF';
            ctrl.saveDetails();
            
            system.assertEquals(agr.Id, [select Cloned_From__c from Agreement__c where Id = :ctrl.clonedAgreementRec.id].Cloned_From__c);
            
            Test.StopTest();
        }
    }
}