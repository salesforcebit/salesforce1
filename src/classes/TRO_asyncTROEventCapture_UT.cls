@isTest
private class TRO_asyncTROEventCapture_UT {
    @isTest static void testAsyncTROEventCapture_mock1() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new TRO_AsyncTROEventCapture_Mock());
        
        AsyncTROEventCapture.receiveEV00023Response_elementFuture resp = new AsyncTROEventCapture.receiveEV00023Response_elementFuture();
        
        TROEventCapture.EventHeaderType eventHeader = new TROEventCapture.EventHeaderType();
        TROEventCapture.eventEV00023BodyType eventBody = new TROEventCapture.eventEV00023BodyType();
        Continuation con = new Continuation(120);
        con.continuationMethod = 'processResponse';

        AsyncTROEventCapture.AsyncEAPEventCapturePortSoap1_1 capSoap = new AsyncTROEventCapture.AsyncEAPEventCapturePortSoap1_1();
        capSoap.beginReceiveEV00023(con, eventHeader, eventBody);
        // Call the method that invokes a callout
        //String output = AsyncTROEventCapture.callEchoString('Hello World!');
    }
    @isTest static void testAsyncTROEventCapture_mock2() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new TRO_AsyncTROEventCapture_Mock1());
        
        AsyncTROEventCapture.receiveEV00022Response_elementFuture resp = new AsyncTROEventCapture.receiveEV00022Response_elementFuture();
        
        TROEventCapture.EventHeaderType eventHeader = new TROEventCapture.EventHeaderType();
        TROEventCapture.eventEV00022BodyType eventBody = new TROEventCapture.eventEV00022BodyType();
        Continuation con = new Continuation(120);
        con.continuationMethod = 'processResponse';

        AsyncTROEventCapture.AsyncEAPEventCapturePortSoap1_1 capSoap = new AsyncTROEventCapture.AsyncEAPEventCapturePortSoap1_1();
        capSoap.beginReceiveEV00022(con, eventHeader, eventBody);
        // Call the method that invokes a callout
        //String output = AsyncTROEventCapture.callEchoString('Hello World!');
    }
    @isTest static void testAsyncTROEventCapture_mock3() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new TRO_AsyncTROEventCapture_Mock2());
        
        AsyncTROEventCapture.receiveEV00021Response_elementFuture resp = new AsyncTROEventCapture.receiveEV00021Response_elementFuture();
        
        TROEventCapture.EventHeaderType eventHeader = new TROEventCapture.EventHeaderType();
        TROEventCapture.eventEV00021BodyType eventBody = new TROEventCapture.eventEV00021BodyType();
        Continuation con = new Continuation(120);
        con.continuationMethod = 'processResponse';

        AsyncTROEventCapture.AsyncEAPEventCapturePortSoap1_1 capSoap = new AsyncTROEventCapture.AsyncEAPEventCapturePortSoap1_1();
        capSoap.beginReceiveEV00021(con, eventHeader, eventBody);
        // Call the method that invokes a callout
        //String output = AsyncTROEventCapture.callEchoString('Hello World!');
    }
}