Global Class KYCWebservice {
   
   static string recordsuccess;
   static List<Account> Accountreclist = new List<Account>();
   static List<Account> GurantorAccountreclist = new List<Account>();
   static List<Contact> Contactreclist = new List<Contact>();
   static List<LLC_BI__Product_Package__c> ProductpackageList = new List<LLC_BI__Product_Package__c>();
   static List<LLC_BI__Loan__c> Loanlist = new List<LLC_BI__Loan__c>();
   static List<LLC_BI__Fee__c> feelist = new List<LLC_BI__Fee__c>();
   static List<LLC_BI__Product__c> productslist = new List<LLC_BI__Product__c>();
   static List<Opportunity> Opportunitylist = new List<Opportunity>();
   static List<LLC_BI__Legal_Entities__c> Entitieslist = new List<LLC_BI__Legal_Entities__c>();
   static List<PriceBookEntry> priceBookList = new List<PriceBookEntry>();
   static List<OpportunityLineItem> olitemList = new List<OpportunityLineItem>();
   static set<id> KBCMCustomernumberIds = new set<id>();
       
     webservice static list<String> CreateaDatancino(List<KYCEntitywisewebservicedata.BorrowerAccountdata> BorrowerServiceData,List<KYCEntitywisewebservicedata.guarantorAccountData> guarantorServiceData){
              Savepoint sp = Database.setSavepoint();
              list<String>  sendresponse= new list<String>();
              KBCMCustomernumberIds = new set<id>();
              List<Account> accexits = new list<Account>();
              Try { 
                  if(BorrowerServiceData.size()>0){
                      for(KYCEntitywisewebservicedata.BorrowerAccountdata sd: BorrowerServiceData){
                          system.debug(sd.IPI.length());
                          if(sd.IPI.length()>1){
                               system.debug(sd.IPI.length());
                               KBCMCustomernumberIds.add(sd.IPI);
                          }     
                      }
                  }
                  if(KBCMCustomernumberIds.size()>0 && ! KBCMCustomernumberIds.isempty()){
                         system.debug('recordsuccess '+recordsuccess);
                         accexits = [select id,Name from Account where KBCM_Customer_Number__c in: KBCMCustomernumberIds];
                         if(accexits.size()>0){
                             for(Account acc : accexits){
                                sendresponse.add( 'Customer Already Exists with '+acc.Name);
                             }
                         }
                  }
                 
                  if(BorrowerServiceData.size()>0 && accexits.isempty()){
                      createaccounts(BorrowerServiceData,guarantorServiceData);
                      if(recordsuccess =='Account') createcontacts();
                      if(recordsuccess =='contact') createOpportunities();
                      if(recordsuccess =='opportunity') createOpplineItems();
                      if(recordsuccess =='OpportunityLineItem') createProductpackage();
                      if(recordsuccess =='Productpackage') createloans();
                      if(recordsuccess =='Loan') createEntityInvolvements();
                      if(recordsuccess =='EntityInvolvement') createfeeunderloan();
                      if(recordsuccess =='fee') sendresponse.add('Success');
                  }
                  else if(!accexits.isempty() && BorrowerServiceData.size()>0){
                      createOpportunities();
                      if(recordsuccess =='opportunity') createOpplineItems();
                      if(recordsuccess =='OpportunityLineItem') createProductpackage();
                      if(recordsuccess =='Productpackage') createloans();
                      if(recordsuccess =='Loan') createEntityInvolvements();
                      if(recordsuccess =='EntityInvolvement') createfeeunderloan();
                      if(recordsuccess =='fee') sendresponse.add('Success');
                  }
              }
              catch(Exception ex){ 
                   system.debug('exception e '+ex.getmessage());
                   sendresponse.add('exception while creation of :'+recordsuccess+' '+ex.getmessage());
                   return sendresponse;
                   Database.rollback(sp);
                   ECCP_UTIL_ApplicationErrorLog.createExceptionLog('KYCWebservice','KYCWebservice','CreateaDatancino', String.valueOf(ex.getLineNumber()),ex.getMessage(), 'Record Id',ex.getStackTraceString());
                   ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();    
                   recordsuccess = '';
   
              } 
              if(sendresponse[0] == 'Success'){
                  string ApplicationNumber = [select ECC_APP_ID__c from LLC_BI__Loan__c where Id =: Loanlist[0].id].ECC_APP_ID__c;
                  
                  if(ApplicationNumber != null && accexits.isempty()){
                      sendresponse.add(ApplicationNumber);
                  }
                  else if(!accexits.isempty() && ApplicationNumber != null){
                   sendresponse.add('Customer Already Exists with'+accexits[0].name+' related Entities updated succesfully to that'+ApplicationNumber);
                  } 
                  return sendresponse; 
              }       
              
              else {
                  Database.rollback(sp);
                  sendresponse.add('exception while creation of : '+recordsuccess);
              } 
        return sendresponse;
     }
     
     
     public static void CreateAccounts(List<KYCEntitywisewebservicedata.BorrowerAccountdata> BorrowerServiceData,List<KYCEntitywisewebservicedata.guarantorAccountData> guarantorServiceData){
              KYCWebserviceHelper.prepareentitywisedata(BorrowerServiceData);
              Accountreclist = KYCWebserviceHelper.Accountreclist;
              if(guarantorServiceData.size()>0){
                 KYCWebserviceHelper.prepareGuarntordata(guarantorServiceData);
                 GurantorAccountreclist = KYCWebserviceHelper.GuarntorAccountreclist;
                 
                 if(GurantorAccountreclist.size()>0){
                      recordsuccess = 'Account';
                      Database.upsert(GurantorAccountreclist,true);
                 }
              }
              if(Accountreclist.size()>0){
                  recordsuccess = 'Account';
                  Database.upsert(Accountreclist,true);   
              }
     }
     
     public static void createcontacts(){
             Contactreclist = KYCWebserviceHelper.Contactreclist;
              if(Contactreclist.size()>0){
                  recordsuccess ='Contact';
                  for(Contact con: Contactreclist){
                      con.Accountid=Accountreclist[0].id;
                  }
                  Database.upsert(Contactreclist,true);
              }
     }
     
     public static void createOpportunities(){
             Opportunitylist = KYCWebserviceHelper.Opportunitylist;
              if(Opportunitylist.size()>0){
                  recordsuccess = 'Opportunity';
                  for(Opportunity opp: Opportunitylist){
                      opp.Accountid = Accountreclist[0].id;
                  }
                  Database.upsert(Opportunitylist,true);
              }
     }
     
     Public static void createOpplineItems(){
             priceBookList = KYCWebserviceHelper.priceBookList;
             recordsuccess ='OpportunityLineItem';
              if(priceBookList.size()>0){
                  OpportunityLineItem oli = new OpportunityLineItem();
                  oli.PricebookEntryId = priceBookList[0].id;
                  oli.Opportunityid=Opportunitylist[0].id;
                  olitemList.add(oli);
                  
                  if(olitemList.size()>0){
                      recordsuccess ='Opportunitylineitem';
                      database.upsert(olitemList,true);
                  }
              }
     }
     
     public static void createProductpackage(){
          ProductpackageList = KYCWebserviceHelper.ProductpackageList;
          if(ProductpackageList.size()>0){
              recordsuccess ='Productpackage';
              for(LLC_BI__Product_Package__c ppck : ProductpackageList ){
                  ppck.LLC_BI__Account__c = Accountreclist[0].id;
              }  
              Database.upsert(ProductpackageList ,true);
          }
     }
     
     public static void createloans(){
         Loanlist = KYCWebserviceHelper.Loanlist;
          if(Loanlist.size()>0){
             recordsuccess = 'Loan';
             for(LLC_BI__Loan__c loanrec: Loanlist){
                  loanrec.LLC_BI__Account__c = Accountreclist[0].id;
                  loanrec.LLC_BI__Product_Package__c = ProductpackageList[0].id;
             }
             Database.upsert(Loanlist,true);
          }  
     }
     
     Public static void createEntityInvolvements(){
            if(recordsuccess == 'Loan' && Accountreclist.size()>0){
                recordsuccess = 'EntityInvolvement';
                          LLC_BI__Legal_Entities__c le = new LLC_BI__Legal_Entities__c();
                          le.Name = Loanlist[0].name;
                          le.LLC_BI__Account__c = Accountreclist[0].id;
                          le.LLC_BI__Borrower_Type__c = 'Borrower';
                          le.LLC_BI__Product_Package__c = ProductpackageList[0].id;
                          le.LLC_BI__Loan__c = Loanlist[0].id;
                          Entitieslist.add(le);
             }
             if(recordsuccess == 'Loan' && GurantorAccountreclist.size()>0){
                recordsuccess = 'EntityInvolvement';
                          for(Account Acc: GurantorAccountreclist){
                              LLC_BI__Legal_Entities__c le = new LLC_BI__Legal_Entities__c();
                              le.Name = Loanlist[0].name;
                              le.LLC_BI__Account__c = Acc.id;
                              le.LLC_BI__Borrower_Type__c = 'Guarantor';
                              le.LLC_BI__Product_Package__c = ProductpackageList[0].id;
                              le.LLC_BI__Loan__c = Loanlist[0].id;
                              Entitieslist.add(le);
                          }
              }
              if(Entitieslist.size()>0){
                          recordsuccess = 'EntityInvolvement';
                          Database.upsert(Entitieslist,true);
              }
     }
     
     public static void createfeeunderloan(){
             if(recordsuccess == 'EntityInvolvement'){
                          feelist =  KYCWebserviceHelper.feelist;
                          recordsuccess = 'Fee';
                          for(LLC_BI__Fee__c feeitem :feelist){
                              feeitem.LLC_BI__Loan__c = Loanlist[0].id;
                          }
                          Database.upsert(feelist,true);
              }  
     }
}