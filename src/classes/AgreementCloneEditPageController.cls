/*
This class is written for KEF KRR Project by TCS Team. This apex class is written for Req. 160

It is a apex controller for Agreement Clone Page
*/
public with sharing class AgreementCloneEditPageController 
{
    public Agreement__c clonedAgreementRec {get;set;}
    
    Boolean isError;
       
    public AgreementCloneEditPageController(ApexPages.StandardController cntr)
    {
        String agId = ApexPages.CurrentPage().getParameters().get('Id');
        
        isError = false;
        
        //instance for cloned Agreement
        clonedAgreementRec = new Agreement__c();
        
        if(agId != null  && agId.trim() != '')
        {
            Agreement__c agreementRec = new Agreement__c();
            //querying for fields that needed to be cloned.
            String agQuery = prepareSOQLQueryforAgreement(fetchListofUpdatableFieldsAgreement(), agId);         
            if(agQuery != null && agQuery.trim() != '')
            {
                agreementRec = Database.Query(agQuery);
            }       
        
            //cloning the agreement record with clearing Id, and fully duplicating the record with no relation to original record
            clonedAgreementRec = agreementRec.clone(false,true,false,false);
            //populating parent Id.
            clonedAgreementRec.Cloned_From__c = agreementRec.id;
        }
    }
    
    //this method will fetch list of fields which are to be cloned for an agreement
    private static List<String> fetchListofUpdatableFieldsAgreement()
    {
        List<String> agreementFields = new List<String>();
        
        Map<String, Schema.SObjectField> fieldMap = new Map<String, Schema.SObjectField>();
        //using apex describe to fetch map of Agreement fields 
        fieldMap = Schema.sObjectType.Agreement__c.fields.getMap();
        
        Set<String> fieldsNotCloned = new Set<String>();
        //feching fields not be cloned from custom setting
        fieldsNotCloned = fetchFieldsNotToBeCloned();
        
        for(String fieldAPI : fieldMap.keyset())
        {
            Schema.SObjectField sobjField = fieldMap.get(fieldAPI);
            //getting field describe for the current field
            Schema.DescribeFieldResult sobjFieldDescribe = sobjField.getDescribe();
            
            //system.debug('fieldAPI--outside if>.    '+fieldAPI);
            
            //checking if the field is editable to the user
            if(!fieldsNotCloned.contains(fieldAPI))
            {
                agreementFields.add(fieldAPI);
                //system.debug('fieldAPI--inside if>.    '+fieldAPI);
            }
        }
        
        return agreementFields;
    }
    
    //this method will fetch list of fields which are not to be cloned, it will fetch data from custom setting
    private static Set<String> fetchFieldsNotToBeCloned()
    {
        Set<String> fieldAPIList = new Set<String>();
        
        for(AgreementCloneHelper__c achIns : AgreementCloneHelper__c.getall().values())
        {
            fieldAPIList.add(achIns.Field_API_Name__c.toLowerCase());
        }
            
        return fieldAPIList;
    }
    
    //method that will prepare a string having list of Agreement fields that can be cloned.
    private static String prepareSOQLQueryforAgreement(List<String> fieldsToQuery, String agId)
    {
        String soqlQuery = '';
        
        if(fieldsToQuery != null && fieldsToQuery.size() > 0 && agId != null && agId.trim() != '')
        {
            soqlQuery = ' select ';
            
            for(String fieldAPI : fieldsToQuery)
            {
                soqlQuery += fieldAPI + ',';
            }
            
            soqlQuery = soqlQuery.substring(0, soqlQuery.length() - 1);
            soqlQuery += ' from Agreement__c where Id = \'' + agId + '\'';
        }
        
        return soqlQuery;
    } 
    
    //method that will save the details to database
    public PageReference saveDetails()
    {
        Savepoint sp = Database.setSavepoint();
        
        List<Vendor_Agreement__c> vaListToInsert = new List<Vendor_Agreement__c>();
        List<Recourse__c> trListToInsert = new List<Recourse__c>();
        List<Payoff_Quote__c> poqListToInsert = new List<Payoff_Quote__c>();
        List<Custom_Reporting__c> crListToInsert = new List<Custom_Reporting__c>();
        List<Promotion__c> promListToInsert = new List<Promotion__c>();
        List<Contact_Agreement__c>  caListToInsert = new List<Contact_Agreement__c>();
        List<KEF_Product__c> kefProdListToInsert = new List<KEF_Product__c>();        
        
        try
        {
			system.debug('--isError->   '+isError);
			//if an exception has occurred, then only clear out the Id values
            if(isError)
            {
	            //clearing out populated invalid Ids
	            clonedAgreementRec = clonedAgreementRec.clone(false,true);
	            vaListToInsert = vaListToInsert.deepClone();
	            trListToInsert = trListToInsert.deepClone();
	            poqListToInsert = poqListToInsert.deepClone();
	            crListToInsert = crListToInsert.deepClone();
	            promListToInsert = promListToInsert.deepClone();
	            caListToInsert = caListToInsert.deepClone();
	            kefProdListToInsert = kefProdListToInsert.deepClone();
            }
            
            insert clonedAgreementRec;
                        
            //fetching list of Vendor Associations to be cloned.
            for(Vendor_Agreement__c va : [SELECT Id,Agreement__c, Account_Vendor__c 
                                            FROM Vendor_Agreement__c 
                                            WHERE Agreement__c = :clonedAgreementRec.Cloned_From__c ])
            {
                Vendor_Agreement__c vaCloned = va.clone(false,true);
                vaCloned.Agreement__c = clonedAgreementRec.id;
                vaListToInsert.add(vaCloned);                
            }
            
            insert vaListToInsert;
            
            //fetching list of Transaction-Recourse records to be cloned.
            for(Recourse__c tr : [SELECT id, Recourse_Amount__c, Recourse_Code__c, Recourse_Expiry_Date__c, Recourse_Percent__c
                                    FROM Recourse__c
                                    WHERE Agreement__c = :clonedAgreementRec.Cloned_From__c ])
            {
                Recourse__c clonedTr = tr.clone(false,true);
                clonedTr.Agreement__c = clonedAgreementRec.id;
                trListToInsert.add(clonedTr);   
            }
            
            insert trListToInsert;
            
            //fetching list pf Payoff Quotes to be cloned. Payoff Quote Status will not be cloned here.
            for(Payoff_Quote__c poq : [SELECT id, Discount__c, Fee__c, Payquote_Type__c, Residual__c
                                    FROM Payoff_Quote__c
                                    WHERE Agreement__c = :clonedAgreementRec.Cloned_From__c ])
            {
                Payoff_Quote__c clonedPOQ = poq.clone(false,true);
                clonedPOQ.Agreement__c = clonedAgreementRec.id;
                poqListToInsert.add(clonedPOQ); 
            }
            
            insert poqListToInsert;
            
            //fetching list of Custom Reporting records to be cloned.
            for(Custom_Reporting__c cr : [SELECT id, Custom_Reporting_Name__c, Custom_Reporting_Values__c
                                    FROM Custom_Reporting__c
                                    WHERE Agreement__c = :clonedAgreementRec.Cloned_From__c ])
            {
                Custom_Reporting__c clonedCR = cr.clone(false,true);
                clonedCR.Agreement__c = clonedAgreementRec.id;
                crListToInsert.add(clonedCR);   
            }
            
            insert crListToInsert;
            
            //fetching list of Custom Reporting records to be cloned.
            for(Promotion__c prom : [SELECT id, Promotion_Name__c, Promotion_Description__c
                                    FROM Promotion__c
                                    WHERE Agreement__c = :clonedAgreementRec.Cloned_From__c ])
            {
                Promotion__c clonedProm = prom.clone(false,true);
                clonedProm.Agreement__c = clonedAgreementRec.id;
                promListToInsert.add(clonedProm);   
            }
            
            insert promListToInsert;
            
            /* START - added this section for defect 122 */
            //fetching the list of Contact Associations to be cloned
            for(Contact_Agreement__c caRec : [SELECT Id, Contact__c, Contact_Role__c
            									FROM Contact_Agreement__c
            									WHERE Agreement__c = :clonedAgreementRec.Cloned_From__c ])
			{
				Contact_Agreement__c clonedCARec = caRec.clone(false,true);
				clonedCARec.Agreement__c = clonedAgreementRec.Id;
				caListToInsert.add(clonedCARec);	
			}
			
			insert caListToInsert;
			
			//fetching the list of KEF Products to be cloned
			for(KEF_Product__c kefProd : [SELECT Product__c
											FROM KEF_Product__c
											WHERE Agreement__c = :clonedAgreementRec.Cloned_From__c ])
			{
				KEF_Product__c clonedProd = kefProd.clone(false,true);
				clonedProd.Agreement__c = clonedAgreementRec.Id;
				kefProdListToInsert.add(clonedProd);
			}
			
			insert kefProdListToInsert;
			/* END - added this section for defect 122 */
			
			isError = false;
            
        }
        catch(DMLException de)
        {
			isError = true;
			
			system.debug('isError-in DMLEXception->   '+isError);
			
            for (Integer i = 0; i < de.getNumDml(); i++) 
            {
                // Process exception here
                System.debug(de.getDmlMessage(i));              
                List<String> errorFields = de.getDmlFieldNames(i);
                system.debug('errorFields--->  '+errorFields);
                if(de.getDmlMessage(i) != null && de.getDmlMessage(i).contains(System.Label.duplicate_value_found))
                { 
                	if(de.getDmlMessage(i).contains('Agreement_Name__c'))
                	{
                		ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,System.Label.AgreementNameAlreadyExists));
                	}
                	else
                	{
                		ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'A duplicate value found for children records, cannot proceed.'));
                	}
                }
                //checking if field level exception is there, if it is not then show the error messgae on scren.
                //field level errors are shown on screen by default, hence no need to add Message explicitely.                 
                else if(errorFields == null || (errorFields != null && errorFields.size() == 0))
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,de.getDmlMessage(i).unescapeXml(),''));
                }               
            }
            system.debug('de-->   '+de);
            Database.rollback(sp);
            
            return null;             
        }   
        catch(Exception E)
        {
		    isError = true;
		    
		    system.debug('isError-in Exception->   '+isError);
		    
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,E.getMessage()));
            system.debug('E-->   '+E);
            Database.rollback(sp);
            return null;            
        }
        
        
        
        PageReference pageRef = new PageReference('/'+clonedAgreementRec.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
}