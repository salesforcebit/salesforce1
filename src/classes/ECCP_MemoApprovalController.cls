public class ECCP_MemoApprovalController {
	public Boolean isCARequired {get;set;}
    public Boolean isLocked {get;set;}
    public Decimal totalExposure {get;set;}
    public Decimal newMoney {get;set;}
    public LLC_BI__Credit_Memo_Modifcation__c memo {get;set;}
    public LLC_BI__Product_Package__c productPackage {get;set;}
    public List<Schema.FieldSetMember> memoFieldSetMembers {get;set;}
    public nFORCE.TemplateController template;
    
    public ECCP_MemoApprovalController(nFORCE.TemplateController controller) {
        this.template = controller;
        this.memoFieldSetMembers = this.getMemoFieldSetMembers(FIELDSETNAME);
        this.memo = this.getMemo(ApexPages.currentPage().getParameters().get(PARAMETER_NAME)); 
        this.productPackage = this.getProductPackage(this.memo.LLC_BI__Loan__r.LLC_BI__Product_Package__c);
        Decimal memoNewMoney = 0;
        if(this.memo.ECC_New_Money__c != null){
            memoNewMoney = this.memo.ECC_New_Money__c;
        }
        this.totalExposure = this.getTotalExposure(this.productPackage.Total_exp__c, memoNewMoney);
        this.newMoney = this.getNewMoney(this.productPackage.LLC_BI__New_Money__c, memoNewMoney);
            
        if(this.memo.LLC_BI__Approval_Status__c == APPROVEDSTATUS) {
            this.isLocked = True;
        } else {
            this.isLocked = False;
            this.isCARequired = this.getCheckForCA(this.productPackage.Does_restricted_industry_re__c, 
                                                   this.productPackage.Lowest_Risk_Rating__c, 
                                                   this.totalExposure, 
                                                   this.newMoney, 
                                                   this.productPackage.NACIS_Restricted__c);
        }
    }
    
    public PageReference memoSave() {
        try {
            update this.memo;
        } catch(Exception ex) {
            ApexPages.addMessages(ex);
        }
        return null;
    }
    
    public PageReference cancel() {
        PageReference pageRef = new PageReference('/' + this.memo.Id);
        return pageRef;
    }
    
    public List<SelectOption> getdelegatedAuthorityUsers() {
        List<SelectOption> newSOList = new List<SelectOption>();
        
        List<User> users = [SELECT Id, FirstName, LastName, 
                            Delegated_Authority_RR1_RR12_Existing__c, 
                            Delegated_Authority_RR1_RR12_New_Incr__c
                            FROM User 
                            WHERE (Review_Role__c =: UNDERWRITERGROUPLEADER
                                   OR Review_Role__c =: UNDERWRITERTEAMLEADER 
                                   OR Review_Role__c =: UNDERWRITERASSOCIATE
                                  ) AND 
                                  (Delegated_Authority_RR1_RR12_Existing__c >=: this.totalExposure 
                                   AND Delegated_Authority_RR1_RR12_New_Incr__c >=: this.newMoney
                                  )];
        
        newSOList.add(new SelectOption(BLANK, NONE));
        for (User u: users) {
            newSOList.add(new SelectOption(u.Id, usernameFormat(u.FirstName, u.LastName)));
        }
        
        return newSOList;
    }
    
    public List<SelectOption> getcreditAuthorityUsers() {
        List<SelectOption> newSOList = new List<SelectOption>();
        List<User> users = new List<User>();
        
        if (this.productPackage.Lowest_Risk_Rating__c <= 7) {
            users = [SELECT Id, FirstName, LastName 
                     FROM User 
                     WHERE (Review_Role__c =: SENIORCREDITOFFICER
                            OR Review_Role__c =: CREDITOFFICER
                            OR Review_Role__c =: NATIONALCREDITOFFICER
                            OR Review_Role__c =: REGIONALCREDITOFFICER
                            OR Review_Role__c =: REGIONALCREDITEXECUTIVE
                            OR Review_Role__c =: GROUPCREDITEXECUTIVE
                           ) 
                            AND Credit_Approval_Authority_RR1_RR7__c >=: this.totalExposure];
        } else if (this.productPackage.Lowest_Risk_Rating__c > 7 && this.productPackage.Lowest_Risk_Rating__c <= 16) {
            users = [SELECT Id, FirstName, LastName 
                     FROM User 
                     WHERE (Review_Role__c =: SENIORCREDITOFFICER
                            OR Review_Role__c =: CREDITOFFICER
                            OR Review_Role__c =: NATIONALCREDITOFFICER
                            OR Review_Role__c =: REGIONALCREDITOFFICER
                            OR Review_Role__c =: REGIONALCREDITEXECUTIVE
                            OR Review_Role__c =: GROUPCREDITEXECUTIVE
                           ) 
                            AND Credit_Approval_Authority_RR8_RR16__c >=: this.totalExposure];
        } else if (this.productPackage.Lowest_Risk_Rating__c > 16 && this.productPackage.Lowest_Risk_Rating__c <= 18) {
            users = [SELECT Id, FirstName, LastName 
                     FROM User 
                     WHERE (Review_Role__c =: SENIORCREDITOFFICER
                            OR Review_Role__c =: CREDITOFFICER
                            OR Review_Role__c =: NATIONALCREDITOFFICER
                            OR Review_Role__c =: REGIONALCREDITOFFICER
                            OR Review_Role__c =: REGIONALCREDITEXECUTIVE
                            OR Review_Role__c =: GROUPCREDITEXECUTIVE
                           ) 
                            AND Credit_Approval_Authority_RR17_RR18__c >=: totalExposure];
        }
        
        newSOList.add(new SelectOption(BLANK, NONE));
        for (User u: users) {
            newSOList.add(new SelectOption(u.Id, usernameFormat(u.FirstName, u.LastName)));
        }
        
        return newSOList;
    }
    
    public List<SelectOption> getreviewer1() {
        List<SelectOption> newSOList = new List<SelectOption>();
        List<User> users = new List<User>();
        
        users = [SELECT Id, FirstName, LastName 
                     FROM User 
                     WHERE Review_Role__c =: UNDERWRITERTEAMLEADER
                        OR Review_Role__c =: UNDERWRITERGROUPLEADER];
        
        newSOList.add(new SelectOption(BLANK, NONE));
        for (User u: users) {
            newSOList.add(new SelectOption(u.Id, usernameFormat(u.FirstName, u.LastName)));
        }
        
        return newSOList;
    }
    
    public List<SelectOption> getreviewer2() {
        List<SelectOption> newSOList = new List<SelectOption>();
        List<User> users = new List<User>();
        
        users = [SELECT Id, FirstName, LastName 
                     FROM User 
                     WHERE Review_Role__c =: CREDITOFFICER];
        
        newSOList.add(new SelectOption(BLANK, NONE));
        for (User u: users) {
            newSOList.add(new SelectOption(u.Id, usernameFormat(u.FirstName, u.LastName)));
        }
        
        return newSOList;
    }
    
    public List<SelectOption> getreviewer3() {
        List<SelectOption> newSOList = new List<SelectOption>();
        List<User> users = new List<User>();
        
        users = [SELECT Id, FirstName, LastName 
                     FROM User 
                     WHERE Review_Role__c =: SENIORCREDITOFFICER];
        
        newSOList.add(new SelectOption(BLANK, NONE));
        for (User u: users) {
            newSOList.add(new SelectOption(u.Id, usernameFormat(u.FirstName, u.LastName)));
        }
        
        return newSOList;
    }
    
    public List<SelectOption> getreviewer4() {
        List<SelectOption> newSOList = new List<SelectOption>();
        List<User> users = new List<User>();
        
        users = [SELECT Id, FirstName, LastName 
                     FROM User 
                     WHERE Review_Role__c =: NATIONALCREDITOFFICER];
        
        newSOList.add(new SelectOption(BLANK, NONE));
        for (User u: users) {
            newSOList.add(new SelectOption(u.Id, usernameFormat(u.FirstName, u.LastName)));
        }
        
        return newSOList;
    }
    
    public List<SelectOption> getreviewer5() {
        List<SelectOption> newSOList = new List<SelectOption>();
        List<User> users = new List<User>();
        
        users = [SELECT Id, FirstName, LastName 
                     FROM User 
                     WHERE Review_Role__c =: REGIONALCREDITOFFICER];
        
        newSOList.add(new SelectOption(BLANK, NONE));
        for (User u: users) {
            newSOList.add(new SelectOption(u.Id, usernameFormat(u.FirstName, u.LastName)));
        }
        
        return newSOList;
    }
    
    public List<SelectOption> getreviewer6() {
        List<SelectOption> newSOList = new List<SelectOption>();
        List<User> users = new List<User>();
        
        users = [SELECT Id, FirstName, LastName 
                     FROM User 
                     WHERE Review_Role__c =: REGIONALCREDITEXECUTIVE];
        
        newSOList.add(new SelectOption(BLANK, NONE));
        for (User u: users) {
            newSOList.add(new SelectOption(u.Id, usernameFormat(u.FirstName, u.LastName)));
        }
        
        return newSOList;
    }
    
    private Boolean getCheckForCA(Boolean industryRestricted, 
                                  Decimal riskRating, 
                                  Decimal exposure, 
                                  Decimal newMoney, 
                                  Boolean naicsRestricted) {
        if(riskRating <= 12 && 
           exposure <= 2500000 && 
           newMoney <= 1000000 &&
           (naicsRestricted == False || 
            (naicsRestricted == True && 
             industryRestricted == False
            )
           )
          ) {
            return False;
        } else {
            return True;
        }
    }
    
    private List<Schema.FieldSetMember> getMemoFieldSetMembers(String fieldSet) {
        return SObjectType.LLC_BI__Credit_Memo_Modifcation__c.fieldSets.getMap().get(fieldSet).getFields();
    }
    
    private LLC_BI__Product_Package__c getProductPackage(Id recordId) {
        return [SELECT
                	Id, 
                	Name, 
                	Lowest_Risk_Rating__c, 
                	Total_exp__c, 
                	LLC_BI__New_Money__c, 
                	NACIS_Restricted__c, 
                	Bank_Division__c, 
                	Does_restricted_industry_re__c
                FROM 
                	LLC_BI__Product_Package__c 
               	WHERE Id = :recordId];
    }
    
    private LLC_BI__Credit_Memo_Modifcation__c getMemo(Id recordId) {
        return [SELECT
                	Id, 
                	Name, 
                	LLC_BI__Approval_Status__c,
                	ECC_Credit_Authority_Name__c,
                	ECC_Delegated_Authority_Name__c,
                	ECC_Reviewer_1_Name__c,
                    ECC_Reviewer_2_Name__c,
                    ECC_Reviewer_3_Name__c,
                    ECC_Reviewer_4_Name__c,
                    ECC_Reviewer_5_Name__c,
                    ECC_Reviewer_6_Name__c,
                	LLC_BI__Loan__r.LLC_BI__Product_Package__c,
                	ECC_New_Money__c 
                FROM 
                	LLC_BI__Credit_Memo_Modifcation__c 
               	WHERE Id = :recordId];
    }
    
    private Decimal getTotalExposure(Decimal productPackageExposure, Decimal memoNewMoney) {
        return productPackageExposure + memoNewMoney;
    }
    
    private Decimal getNewMoney(Decimal productPackageNewMoney, Decimal memoNewMoney) {
        return productPackageNewMoney + memoNewMoney;
    }
    
    private static String usernameFormat(String first, String last) {
        return (first + BLANKSPACE + last);
    }
    
    private static final String BLANK = '';
    private static final String BLANKSPACE = ' ';
    private static final String COMMASPACE = ', ';
    private static final String SELECTSTATEMENT = 'SELECT ';
    private static final String NONE = System.Label.ECCP_None_Picklist_Value;
    private static final String PARAMETER_NAME = 'id';
    
    private static final String FIELDSETNAME = 'ECC_Select_Approvers_Memo';
    
    private static final String UNDERWRITERGROUPLEADER = System.Label.ECCP_Reviewer_Role_Underwriting_Group_Leader;
    private static final String UNDERWRITERTEAMLEADER = System.Label.ECCP_Reviewer_Role_Underwriting_Team_Lead;
    private static final String UNDERWRITERASSOCIATE = System.Label.ECCP_Reviewer_Role_Underwriting_Associate;
    private static final String CREDITOFFICER = System.Label.ECCP_Reviewer_Role_Credit_Officer;
    private static final String SENIORCREDITOFFICER = System.Label.ECCP_Reviewer_Role_Senior_Credit_Officer;
    private static final String NATIONALCREDITOFFICER = System.Label.ECCP_Reviewer_Role_National_Credit_Officer;
    private static final String REGIONALCREDITOFFICER = System.Label.ECCP_Reviewer_Role_Regional_Credit_Officer;
    private static final String REGIONALCREDITEXECUTIVE = System.Label.ECCP_Reviewer_Role_Regional_Credit_Executive;
    private static final String GROUPCREDITEXECUTIVE = System.Label.ECCP_Reviewer_Role_Group_Credit_Executive;
    private static final String APPROVEDSTATUS = System.Label.ECCP_Approved_Stage;
}