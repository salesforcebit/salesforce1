/**
 * Provides a strongly-typed intermediate object for converting the JSON representation
 * of a Product to its sObject equivalent.
 */
public class Product extends nFORCE.AForce {
	public String sid;
	public String name;
	public String lookupKey;
	public String productType;
	public Boolean isDeleted;
	public List<BillPoint> billPoints;

	public Product() {
		this.billPoints = new List<BillPoint>();
	}

	public Product(String sid, String name, String lookupKey, Boolean isDeleted) {
		this.sid = sid;
		this.name = name;
		this.lookupKey = lookupKey;
		this.isDeleted = isDeleted;
		this.billPoints = new List<BillPoint>();
	}

	public LLC_BI__Product__c getSObject() {
		LLC_BI__Product__c product = new LLC_BI__Product__c();
		this.mapToDb(product);
		return product;
	}

	public override Schema.SObjectType getSObjectType() {
		return LLC_BI__Product__c.sObjectType;
	}

	public static Product buildFromDB(sObject obj) {
		Product product = new Product();
		product.mapFromDB(obj);
		return product;
	}

	public override Type getType() {
		return Product.class;
	}

	public override void mapFromDb(sObject obj) {
		LLC_BI__Product__c product = (LLC_BI__Product__c)obj;
		if (product != null) {
			this.sid = product.ID;
			this.name = product.Name;
			this.lookupKey = product.LLC_BI__lookupKey__c;
			this.productType = product.LLC_BI__Product_Type__c;
		}
	}

	public override void mapToDb(sObject obj) {
		LLC_BI__Product__c product = (LLC_BI__Product__c)obj;
		if (product != null) {
			if (this.sid != null && this.sid.length() > 0) {
				product.Id = Id.valueOf(this.sid);
			}
			product.Name = this.name;
			product.LLC_BI__lookupKey__c = this.lookupKey;
		}
	}
}