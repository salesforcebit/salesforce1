//================================================================================
//  Added for KRR Release 3 - 2 November 2015
//  Object: Funding Grid Handler
//  Description : Handler class for Funding Grid Object.
//================================================================================
public with sharing class FundingGridHandler_KEF 
{
    public FundingGridHandler_KEF()
    {
    	
    }
    
    public void OnBeforeInsert(List<Funding_Grid__c> triggerNewList, Map<ID, Funding_Grid__c> triggerNewMap)
    {
    	Map<Id, Funding_Grid__c> accIdFGRecMap = new Map<Id, Funding_Grid__c>();
    	
    	for(Funding_Grid__c fgRec : triggerNewList)
    	{
    		if(fgRec.Default_Payment_Method__c)
    		{
    			/* Start - Code added for Req. 252 - Release 3 */
    			//this covers scenario where mutliple records are added under the same account with Default payment Method checkbox checked. 
    			if(accIdFGRecMap.containsKey(fgRec.Account__c))
    			{
    				fgRec.Default_payment_Method__c = false;
    			}
    			else
    			{
    				accIdFGRecMap.put(fgRec.Account__c, fgRec);
    			}
    			/* End - Code added for Req. 252 - Release 3 */
    		}
    	}
    	
    	//Map's job is done, hence clearing values in it
    	accIdFGRecMap.clear();
    }
    
    public void OnAfterInsert(List<Funding_Grid__c> triggerNewList, Map<ID, Funding_Grid__c> triggerNewMap)
    {
    	//a map with Account Id as key and funding grid id as value
    	Map<Id,Id> accFGMap = new Map<id,Id>();
    	List<Funding_Grid__c> newDefaultFGList = new List<Funding_Grid__c>();
    	
    	for(Funding_Grid__c fgRec : triggerNewList)
    	{
    		/* Start - Code added for Req. 252 - Release 3 */
    		//checking the funding grid is marked as default
    		if(fgRec.Default_Payment_Method__c)
    		{
    			//preparing a map of Account id as key and Funding Grid id as value. 
    			accFGMap.put(fgRec.Account__c, fgRec.id);
    			//list of Funding Grid records who are marked as default
    			newDefaultFGList.add(fgRec);
    		}
    		/* End - Code added for Req. 252 - Release 3 */
    	}
    	
    	/* Start - Code added for Req. 252 - Release 3 */
    	Map<Id,String> errorMsgMap = FundingGridHelper.setUnsetDefaultfundingMethod(newDefaultFGList, new Map<Id, Funding_Grid__c>() , accFGMap);
    	FundingGridHelper.showErrorOnScreen(errorMsgMap, triggerNewMap);
    	/* End - Code added for Req. 252 - Release 3 */
    }
    
    public void OnBeforeUpdate(List<Funding_Grid__c> triggerOldList, List<Funding_Grid__c> triggerNewList, Map<ID, Funding_Grid__c> triggerNewMap)
    {
    	Map<Id, Funding_Grid__c> accIdFGRecMap = new Map<Id, Funding_Grid__c>();
    	
    	for(Funding_Grid__c oldFG : triggerOldList)
    	{ 
    		Funding_Grid__c newFG = triggerNewMap.get(oldFG.id);
    		
    		if(newFG.Default_Payment_Method__c)
    		{
    			/* Start - Code added for Req. 252 - Release 3 */
    			//this covers scenario where mutliple records are added under the same account with Default payment Method checkbox checked. 
    			if(accIdFGRecMap.containsKey(newFG.Account__c))
    			{
    				newFG.Default_payment_Method__c = false;
    			}
    			else
    			{
    				accIdFGRecMap.put(newFG.Account__c, newFG);
    			}
    			/* End - Code added for Req. 252 - Release 3 */
    		}
    	}
    	
    	//Map's job is done, hence clearing values in it
    	accIdFGRecMap.clear();
    }
    
    public void OnAfterUpdate(List<Funding_Grid__c> triggerOldList, List<Funding_Grid__c> triggerNewList, Map<ID, Funding_Grid__c> triggerNewMap)
    {
    	//a map with Account Id as key and funding grid id as value
    	Map<Id,Id> accFGMap = new Map<id,Id>(); 
    	List<Funding_Grid__c> newDefaultFGList = new List<Funding_Grid__c>();
    	Map<Id, Funding_Grid__c> oldDefaultFGMap = new Map<Id, Funding_Grid__c>();
    	
    	for(Funding_Grid__c oldFG : triggerOldList)
    	{
    		/* Start - Code added for Req. 252 - Release 3 */
    		Funding_Grid__c newFG = triggerNewMap.get(oldFG.Id);    		
    		//checking the funding grid is marked as default
    		if(newFG.Default_Payment_Method__c != oldFG.Default_Payment_Method__c)
    		{
    			//preparing a map of Account id as key and Funding Grid id as value.
    			accFGMap.put(newFG.Account__c, newFG.id);
    			//list of new Funding Grid records for which "Default Payment method' is changed
    			newDefaultFGList.add(newFG);
    			//map of old Funding Grid records for which "Default Payment method' is changed
    			oldDefaultFGMap.put(oldFG.id, oldFG);
    		}
    		/* End - Code added for Req. 252 - Release 3 */
    	}
    	/* Start - Code added for Req. 252 - Release 3 */
    	//upating related Account's lookup 'Defualt Payment method' with Id of new Default FG
    	Map<Id,String> errorMsgMap = FundingGridHelper.setUnsetDefaultfundingMethod(newDefaultFGList, oldDefaultFGMap, accFGMap);
    	//method to show error on screen, if any
    	FundingGridHelper.showErrorOnScreen(errorMsgMap, triggerNewMap);
    	/* End - Code added for Req. 252 - Release 3 */
    }
}