//================================================================================
//  KeyBank  
//  Object: Opportunity Team Member
//  Author: Offshore
//  Detail: opportunityteamMemberTracking_Handler
//  Description : To capture the Added and Deleted records in Opportunity team member and insert into Opportunity Audit table.

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================

public class opportunityteamMemberTracking_Handler
{

    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public opportunityteamMemberTracking_Handler(boolean isExecuting, integer size)
    {    
        m_isExecuting = isExecuting;
        BatchSize = size;  
    }
  
    @future
    public static void onAfterInsert(list<id> tId, list<id> tIdp)
    {
        List<opportunityteamMember> newOtmAdded = new List<opportunityteamMember>();
        newOtmAdded = [Select Id, TeamMemberRole, opportunityID, Userid from OpportunityTeamMember where Id =: tId];
        Map<id,Opportunity> oppmap = new Map<Id,opportunity>([select Id, OwnerId, Private__c, Private_code_name__c from Opportunity where id =: tIdp]);
        system.debug(newOtmAdded);
        system.debug(tId);
        system.debug(tIdp);
        
        // Creating a List to finally udsert the records        
        list<Opportunity_Audit_Table__c> otm_List = new list<Opportunity_Audit_Table__c> ();
        
    try{
        // Looping through each OTM record added or created         
        for(OpportunityTeamMember OTM_inserted : newOtmAdded)
        {
        
            // Creating a reference for the OMT_Tracking object and initialising it            
            Opportunity_Audit_Table__c otm_entry = new Opportunity_Audit_Table__c();
            
            // Assigning the values
            opportunity op = oppmap.get(OTM_inserted.opportunityID);
            
            otm_entry.User__c = OTM_inserted.Userid;
            otm_entry.Opportunity_Owner__c = op.OwnerId;
            otm_entry.role__c=OTM_inserted.TeamMemberRole;
            otm_entry.Opportunity_Name__c = OTM_inserted.opportunityID;
            otm_entry.Action__c = 'Added';
            otm_entry.Private_Code_Name__c = op.Private_code_name__c;
            otm_entry.Private_Opportunity__c = op.Private__c;
            
            // Added the new records to the list which is to be inserted
            
            otm_List.add(otm_entry);
        
        }
        upsert otm_List;
       } catch(DMlException e){
         System.debug(+e);
         } 
    }
  
    //@future  
    public void onAfterDelete(List<OpportunityTeamMember> newOtmdeleted )
    {
        //List<OpportunityTeamMember> newOtmdeleted = new List<OpportunityTeamMember>();
        //newOtmdeleted = [Select Id, TeamMemberRole, opportunityID, Userid from OpportunityTeamMember where Id =: OTId];
        Set<Id> OpptySet = new Set<Id>();
        for(OpportunityTeamMember opptyTM : newOtmdeleted){
            OpptySet.add(opptyTM.opportunityID);
        }
        // Createing a List to finally upsert the records        
        list<Opportunity_Audit_Table__c> otm_delList = new list<Opportunity_Audit_Table__c> ();  
        Map<id,Opportunity> oppmap = new Map<Id,opportunity>([select Id, OwnerId, Private__c, Private_code_name__c from Opportunity WHERE id IN :OpptySet]); 
        
    try{
        // Looping through each OTM record deleted        
        for(OpportunityTeamMember OTM_deleted : newOtmdeleted)
        {        
            // Creating a reference for the OMT_Tracking object and initialising it        
            Opportunity_Audit_Table__c otm_del = new Opportunity_Audit_Table__c();
            opportunity op = oppmap.get(OTM_deleted.opportunityID);
            
            // Assigning the values        
            otm_del.User__c = OTM_deleted.Userid;
            otm_del.Opportunity_Owner__c = op.OwnerId;
            otm_del.role__c=OTM_deleted.TeamMemberRole;
            otm_del.Opportunity_Name__c = OTM_deleted.opportunityID;
            otm_del.Private_Code_Name__c = op.Private_code_name__c;
            otm_del.Action__c  = 'Deleted or Removed';
            otm_del.Private_Opportunity__c = op.Private__c;
            
            // Added the new records to the list which is to be inserted
            
            otm_delList.add(otm_del);     
        
        }
        
        upsert otm_delList;
      } catch(DMlException e){
         System.debug(+e);
         }
    }
     /* @future
     public static void beforeInsertDisStrategy(Set<Id> oppIds){
        List<Opportunity> opptylist = new List<Opportunity>();
        opptyList = [SELECT id,Distribution_Strategy__c FROM Opportunity WHERE id IN :oppIds];
        Map<Id,String> oppMap = new Map<Id,String>();
        if(opptylist != null && opptylist.size() > 0){
            for(Opportunity tempObj : opptylist){
                oppMap.put(tempObj.Id,tempObj.Distribution_Strategy__c);
            }
            System.debug('oppMap:'+oppMap);
        }
        for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.new){
            if(oppMap.get(member.OpportunityId) != null && oppMap.get(member.OpportunityId) != '')
                member.Distribution_Strategy__c = oppMap.get(member.OpportunityId);
            System.debug('OpptyTeam:'+member.Distribution_Strategy__c);
        }
        
    }
    */
    public void onBeforeInsertOTM(Set<Id> oppIds){
        
        Map<String, Schema.SObjectType> consObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType cons = consObjectMap.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaCon = cons.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> conRecordTypeInfo = resSchemaCon.getRecordTypeInfosByName();
        id conRTId = conRecordTypeInfo.get('KEF Opportunity').getRecordTypeId();
        System.Debug('conRTId:'+conRTId);
        
        List<opportunityteamMember> teamMembers = [SELECT id,Name,TeamMemberRole, OpportunityId,Opportunity.Bank_Underwritten__c FROM opportunityteamMember WHERE OpportunityId IN : oppIds AND TeamMemberRole = 'Relationship Manager' AND Opportunity.RecordtypeId = :conRTId];
        system.debug(teamMembers.size());
        system.debug(teamMembers);
        Map<id, Set<String>> teamMembersMap  = new Map<id, Set<String>>();
        Map<id, String> opptyBankUnderMap  = new Map<id, String>();
        Map<String,String> insertTeamMemberMap = new Map<String,String>();
        
        //Organising Opportunity to team member roles in map to check for duplicate relationship manager.
        for(opportunityteamMember member : teamMembers){
            System.debug('member.Opportunity.Bank_Underwritten__c:'+member.Opportunity.Bank_Underwritten__c);
            opptyBankUnderMap.put(member.OpportunityId,member.Opportunity.Bank_Underwritten__c);
            system.debug('opptyBankUnderMap:'+opptyBankUnderMap);
            
            if(teamMembersMap.containsKey(member.OpportunityId))
                teamMembersMap.get(member.OpportunityId).add(member.TeamMemberRole);
            else
                teamMembersMap.put(member.OpportunityId, new Set<String> {member.TeamMemberRole} );
        } 
        
        for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.new){
            if(member.TeamMemberRole == 'Relationship Manager'){
                System.Debug('Add New record to Map');
                insertTeamMemberMap.put(member.Name, member.TeamMemberRole);
            }
            System.Debug('Map:'+insertTeamMemberMap);
        }
        
        for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.new){
            
            System.debug('opptyBankUnderMap:'+opptyBankUnderMap);
            //Checking for duplicate teamMemberRole
            //if(opptyBankUnderMap != null && opptyBankUnderMap.size() >0 && opptyBankUnderMap.get(member.OpportunityId).contains('Keybank Underwritten')){
                if((teamMembersMap != null && teamMembersMap.size() >0 && teamMembersMap.get(member.OpportunityId).contains(member.TeamMemberRole)) || (insertTeamMemberMap != null && (insertTeamMemberMap.size() != 1  && insertTeamMemberMap.size() > 0)))
                    member.addError('Only One relationship manager per Opportunity allowed');
          //  }
        }
    }
    public void opportunityshare(List<OpportunityTeamMember> oteamlist)
    {
    List<OpportunityShare> osharelist = new List<OpportunityShare>();
    List<User> ulist = new List<User>();
        
        ulist = [SELECT Department,Id,IsActive,Name,PLOB_Alias__c,PLOB__c,SLOB__c,UserRole.Name FROM User where isActive=true and SLOB__c = 'Technology'];
        List<OpportunityTeamMember> u = [Select userid, opportunityid from OpportunityTeamMember where userid in : ulist and id in : oteamlist ];
        
        Group  grp = [Select Id, Name from Group where Type ='Regular' and Name ='Tech IB Users' limit 1];
       
        for(OpportunityTeamMember otm : u)
        {
        OpportunityShare oshare = new Opportunityshare();
        oshare.OpportunityId = otm.OpportunityId;
        oshare.OpportunityAccessLevel = 'Edit';
        oshare.UserOrGroupId = grp.Id;
        osharelist.add(oshare);
        }
        
        insert osharelist;
      }
      
      public void opportunitysharedelete(List<OpportunityTeamMember> oteamlist)
      {
      try{
        List<Id> OpptyIdList = new List<Id>();
        System.debug('Trigger.Old:'+oteamlist);
        for(OpportunityTeamMember otmRec : oteamlist){
            OpptyIdList.add(otmRec.opportunityid);
        }
        //Map<Id,User> ulist = new Map<Id,User>([SELECT Department,Id,IsActive,Name,PLOB_Alias__c,PLOB__c,SLOB__c,UserRole.Name FROM User where isActive=true and SLOB__c = 'Technology']);
        List<OpportunityTeamMember> otmlist = new List<OpportunityTeamMember>();
        otmList  = [Select opportunityid,userid  from OpportunityTeamMember where opportunityid in :  OpptyIdList and  userid in : [SELECT Id,Department,IsActive from User where isActive=true and SLOB__c = 'Technology']];
        //System.debug('Count' + i);
        if(otmlist != null && otmlist.size() == 0)
        {
            List<OpportunityShare> odelete = new List<OpportunityShare>();
            List<OpportunityShare> olist = new List<OpportunityShare>();
            Group  gr = [Select Id, Name from Group where Type ='Regular' and Name ='Tech IB Users' limit 1];
            olist = [Select id,opportunityaccesslevel,opportunityid,UserOrGroupId from OpportunityShare where opportunityid in : OpptyIdList and UserOrGroupId =:gr.id];
            System.debug('olist:'+olist.size());
                /*for(OpportunityShare os : olist)
                {
                    
                    system.debug('*****');
                    system.debug(os.opportunityid);
                    
                    odelete .add(os);
                    
                }*/
                  
            
            delete olist ;
         }
            
       
       
    }
    catch(Exception e)
    {
    System.debug('e:'+e.getmessage());
    }
    }
    
    
}