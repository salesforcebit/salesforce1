@isTest
private class TestRiskRatingReviewSelectionController {
    @isTest 
    static void CreateNOTypeSet() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theLoan.Id);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.NoTypeSet();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateByLoan() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theLoan.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPELOAN);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateByLoanNoID() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPELOAN);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.NoIDSet();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateByLoanInvalidId() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPELOAN);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.InvalidIDSetLoan();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateByLoanGatherRelationships() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);
        LLC_BI__Legal_Entities__c theEnitytInvolvment = createEntityInvolvements(theAccount.Id, theLoan.Id, BORROWERTYPE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theLoan.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPELOAN);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        List<LLC_BI__Legal_Entities__c> relationshipList = [SELECT Id, LLC_BI__Account__r.Name,  LLC_BI__Account__c, LLC_BI__Borrower_Type__c FROM LLC_BI__Legal_Entities__c WHERE LLC_BI__Loan__c = :theLoan.Id];
        
        rrrC.currentRelationships = relationshipList;
        
        rrrC.getRelationships();

        Test.stopTest();
    }
    
    @isTest 
    static void GatherTemplateList() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);
        
        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theLoan.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPELOAN);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.getTemplateList();

        Test.stopTest();
    }
    
    @isTest 
    static void GatherTemplatePickList() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theLoan.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPELOAN);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.getTemplatePicklist();

        Test.stopTest();
    }
    
    @isTest 
    static void GatherAccountTemplateList() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);
        
        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.getAccountTemplateList();

        Test.stopTest();
    }
    
    @isTest 
    static void GatherAccountTemplatePickList() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.getAccountTemplatePicklist();

        Test.stopTest();
    }

    @isTest 
    static void ResetButton() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.Reset();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateByAccountPicklist() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.getCreateByAccountPicklist();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateRRRLoan() {
        init();
        
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, loan.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPELOAN);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.RiskGrade = VALUETEMPLATELABEL;
        rrrC.RelationshipID = account.Id;
        
        rrrC.Create();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateByRelationshipNoID() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.NoIDSet();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateByRelationship() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        

        Test.stopTest();
    }
    
    @isTest 
    static void AccountSelectionNone() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
               
        rrrC.AccountSelection();

        Test.stopTest();
    }
    
    @isTest 
    static void AccountSelectionAccountLoan() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.CreateBy = ACCOUNTLOAN;
        
        rrrC.AccountSelection();

        Test.stopTest();
    }
    
    @isTest 
    static void AccountSelectionAccountOnlyNoRRR() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.CreateBy = ACCOUNTONLY;
        
        rrrC.AccountSelection();

        Test.stopTest();
    }
    
    @isTest 
    static void AccountSelectionAccountOnlyRRR() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);
        theAccount.Risk_Rating_Review_Template__c = 'Account Template';
        update(theAccount);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.CreateBy = ACCOUNTONLY;
        
        rrrC.AccountSelection();

        Test.stopTest();
    }
    
    @isTest 
    static void LoanSelectionNoRisk() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, null);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.LoanID = theLoan.Id;
        
        rrrC.LoanSelection();

        Test.stopTest();
    }
    
    @isTest 
    static void LoanSelection() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUETEMPLATELABEL);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.LoanID = theLoan.Id;
        
        rrrC.LoanSelection();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateByRelationshipInvalidId() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theLoan.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.InvalidIDSetLoan();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateByRelationshipGatherRelationships() {
        LLC_BI__Product__c theProduct = createProduct(PRODUCTNAME);
        Account theAccount = CreateAccount(ACCOUNTNAME);
        LLC_BI__Loan__c theLoan = CreateLoan(LOANNAME, LOANAMOUNT, theAccount.Id, theProduct.Id, VALUENONE);
        LLC_BI__Legal_Entities__c theEnitytInvolvment = createEntityInvolvements(theAccount.Id, theLoan.Id, BORROWERTYPE);

        Test.startTest();

        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, theAccount.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        List<LLC_BI__Legal_Entities__c> loanList = [SELECT Id, LLC_BI__Loan__r.Name,  LLC_BI__Loan__c, LLC_BI__Loan__r.LLC_BI__lookupKey__c FROM LLC_BI__Legal_Entities__c WHERE LLC_BI__Account__c = :theAccount.Id];
        
        rrrC.currentLoans = loanList;
        
        rrrC.getLoans();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateRRRRelationship() {
        init();
        
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, account.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.RiskGrade = VALUETEMPLATELABEL;
        rrrC.LoanID = loan.Id;
        
        rrrC.CreateFromRelationship();

        Test.stopTest();
    }
    
    @isTest 
    static void CreateRRRRelationshipAccountOnly() {
        init();
        
        ApexPages.currentPage().getParameters().put(PARAMETERIDNAME, account.Id);
        ApexPages.currentPage().getParameters().put(PARAMETERTYPENAME, PARAMETERTYPEREL);
        RiskRatingReviewSelectionController rrrC = new RiskRatingReviewSelectionController();
        
        rrrC.RiskGrade = RISKGRADETEMPLATENAME2;        
        rrrC.CreateFromRelationshipOnly();

        Test.stopTest();
    }
    
    Private Static LLC_BI__Legal_Entities__c createEntityInvolvements(
        Id accountID,
        Id loanID,
        String borrowerType ) {
        
        LLC_BI__Legal_Entities__c  le = new LLC_BI__Legal_Entities__c(
            LLC_BI__Loan__c = loanID,
            LLC_BI__Account__c = accountID,
            LLC_BI__Borrower_Type__c = borrowerType
        );
        insert le;
        
        return le;
    }
    
    Private Static Account createAccount(
        String accountName){
        
        Account a = new Account(
            Name = accountName,
            Type = ACCOUNTTYPE
        );
        insert a;
        
        return a;
    }
    
    Private Static LLC_BI__Loan__c createLoan(
        String loanName,
        Decimal amount,
        Id accountID,
        Id productID,
        String templateName) {
        
        LLC_BI__Loan__c o = new LLC_BI__Loan__c(
            Name = loanName,
            LLC_BI__Product_Line__c = PRODUCTLINENAME,
            LLC_BI__Product_Type__c = PRODUCTTYPENAME,
            LLC_BI__Product__c = PRODUCTNAME,
            LLC_BI__Product_Reference__c = productID,
            LLC_BI__Account__c = accountID,
            LLC_BI__Stage__c = LOANSTAGE,
            LLC_BI__Amount__c = amount,
            LLC_BI__CloseDate__c = THEDATE,
            Risk_Rating_Review_Template__c = templateName
        );
        insert o;
        
        return o;
    }
    
    Private Static LLC_BI__Product__c createProduct(
        String productName
        ) {
        
        LLC_BI__Product_Line__c pl = new LLC_BI__Product_Line__c();
            pl.Name = 'Commerical';
        insert pl;
        
        LLC_BI__Product_Type__c pt = new LLC_BI__Product_Type__c();
            pt.Name = 'Real Estate';
            pt.LLC_BI__Product_Line__c = pl.Id;
        insert pt;
        
        LLC_BI__Product__c p = new LLC_BI__Product__c();
            p.Name = productName;
            p.LLC_BI__Product_Type__c = pt.Id;
        insert p;
           
        return p;
    }
        
    Public static void init()
    {
        Test.startTest();        
        
        LLC_BI__System_Properties__c utility = new LLC_BI__System_Properties__c(
                Name = UTILTIYNAME,
                LLC_BI__Category_Name__c = UTILTIYCATEGORYNAME,
                LLC_BI__Is_Active__c = UTILTIYISACTIVE,
                LLC_BI__Key__c = UTILTIYKEY,
                LLC_BI__Value__c = UTILTIYVALUE
            );
        
        insert utility;
            
        Account accountEntity = new Account();
        accountEntity.AccountNumber = ACCOUNTNUMBER;
        accountEntity.Name = ACCOUNTNAME;
        insert accountEntity;   
        account = [SELECT Id, Name FROM Account LIMIT 1];
        
        LLC_BI__Loan__c loanEntity = new LLC_BI__Loan__c();
        loanEntity.LLC_BI__Account__c = account.Id;
        loanEntity.LLC_BI__Billing_Address__c = LOANBILLINGADDRESS;
        loanEntity.LLC_BI__Billing_State__c = LOANSTATE;
        loanEntity.LLC_BI__Billing_Zipcode__c = LOANZIPCODE;
        loanEntity.Risk_Rating_Review_Template__c = VALUETEMPLATELABEL;
        
        insert loanEntity;
        loan = [SELECT Id, Name FROM LLC_BI__Loan__c LIMIT 1];
        
        LLC_BI__Risk_Grade_Template__c template = new LLC_BI__Risk_Grade_Template__c();
        template.Name = RISKGRADETEMPLATENAME2 ;
        template.LLC_BI__Template_Object_Field_Name__c = RISKRATINGREVIEWFIELDNAME;
        template.LLC_BI__Template_Object_Field_Value__c = VALUETEMPLATELABEL2;
        template.LLC_BI__Template_Object_Type__c = OBJECTTYPE2;
        insert template;
        accountTemplate = [SELECT Id, Name FROM LLC_BI__Risk_Grade_Template__c WHERE Name = :RISKGRADETEMPLATENAME2 LIMIT 1];
        insertRiskGradeFactorsAndCriteria(accountTemplate);
        
        template = new LLC_BI__Risk_Grade_Template__c();
        template.Name = RISKGRADETEMPLATENAME;
        template.LLC_BI__Template_Object_Field_Name__c = RISKRATINGREVIEWFIELDNAME;
        template.LLC_BI__Template_Object_Field_Value__c = VALUETEMPLATELABEL;
        template.LLC_BI__Template_Object_Type__c = OBJECTTYPE;
        insert template;
        loanTemplate = [SELECT Id, Name FROM LLC_BI__Risk_Grade_Template__c WHERE Name = :RISKGRADETEMPLATENAME LIMIT 1];
        insertRiskGradeFactorsAndCriteria(loanTemplate);
    }
    
    Private static void insertRiskGradeFactorsAndCriteria(LLC_BI__Risk_Grade_Template__c template){
        LLC_BI__Risk_Grade_Factor__c f = new LLC_BI__Risk_Grade_Factor__c();
        f.Name = 'risk grade factor name1';
        f.LLC_BI__Risk_Grade_Template__c = template.Id;
        f.LLC_BI__Type__c = 'Quantitative';
        f.LLC_BI__Weight__c = 0.3;
        f.LLC_BI__Units__c = '$';
        Database.SaveResult lsr = Database.insert(f);
        String riskGradeFactorId = lsr.getId();
        
        LLC_BI__Risk_Grade_Criteria__c criteria = new LLC_BI__Risk_Grade_Criteria__c();
        criteria.LLC_BI__High__c = 10.0;
        criteria.LLC_BI__Low__c = 6.0;
        criteria.LLC_BI__Risk_Grade__c = '1';
        criteria.LLC_BI__Risk_Grade_Factor__c = riskGradeFactorId;
        lsr = Database.insert(criteria);
        
        criteria = new LLC_BI__Risk_Grade_Criteria__c();
        criteria.LLC_BI__High__c = 5.0;
        criteria.LLC_BI__Low__c = 1.0;
        criteria.LLC_BI__Risk_Grade__c = '2';
        criteria.LLC_BI__Risk_Grade_Factor__c = riskGradeFactorId;
        lsr = Database.insert(criteria);
        
        f = new LLC_BI__Risk_Grade_Factor__c();
        f.Name = 'risk grade factor name2';
        f.LLC_BI__Risk_Grade_Template__c = template.Id;
        f.LLC_BI__Type__c = 'Qualitative';
        f.LLC_BI__Weight__c = 0.7;
        f.LLC_BI__Units__c = '';
        lsr = Database.insert(f);
        riskGradeFactorId = lsr.getId();
        
        criteria = new LLC_BI__Risk_Grade_Criteria__c();
        criteria.LLC_BI__Description__c = 'not bad';
        criteria.LLC_BI__Risk_Grade__c = '1';
        criteria.LLC_BI__Risk_Grade_Factor__c = riskGradeFactorId;
        lsr = Database.insert(criteria);
        
        criteria = new LLC_BI__Risk_Grade_Criteria__c();
        criteria.LLC_BI__Description__c = 'also not bad';
        criteria.LLC_BI__Risk_Grade__c = '2';
        criteria.LLC_BI__Risk_Grade_Factor__c = riskGradeFactorId;
        lsr = Database.insert(criteria);
    }
    
    Public static Account account;
    Public static LLC_BI__Loan__c loan;
    Public static LLC_BI__Risk_Grade_Template__c accountTemplate;
    Public static LLC_BI__Risk_Grade_Template__c loanTemplate;
    
    Private Static Final String ACCOUNTNAME = 'The Account Name';
    Private Static Final String ACCOUNTTYPE = 'Corporation';
    Private Static Final String ACCOUNTNUMBER = '1234567';
    Private Static Final String LOANNAME = 'The Loan Name';
    Private Static Final String LOANSTAGE = 'Closed Funded';
    Private Static Final String LOANBILLINGADDRESS = '123 OneTwoThree Road';
    Private Static Final String OBJECTTYPE = 'Loan__c';
    Private Static Final String OBJECTTYPE2 = 'Account';
    Private Static Final String LOANSTATE = 'FL';
    Private Static Final String LOANZIPCODE = '33401';
    Private Static Final String RISKRATINGREVIEWFIELDNAME = 'Risk_Rating_Review_Template__c';
    Private Static Final String RISKGRADETEMPLATENAME = 'Commercial';
    Private Static Final String RISKGRADETEMPLATENAME2 = 'Account Template';
    Private Static Final String PRODUCTNAME = 'LOC';
    Private Static Final String PRODUCTTYPENAME = 'Real Estate';
    Private Static Final String PRODUCTLINENAME = 'Commercial';
    Private Static Final String BORROWERTYPE = 'Borrower';
    Private Static Final String UTILTIYNAME = 'sdfhjksdfhklfsdfm_dajshfksdf_12342';
    Private Static Final String UTILTIYCATEGORYNAME = 'Utility';
    Private Static Final Boolean UTILTIYISACTIVE = true;
    Private Static Final String UTILTIYKEY = 'Composite_Field_Types';
    Private Static Final String UTILTIYVALUE = 'ADDRESS;LOCATION;';
    Private Static Final String PARAMETERIDNAME = 'id';
    Private Static Final String PARAMETERTYPENAME = 'type';
    Private Static Final String PARAMETERTYPELOAN = 'loan';
    Private Static Final String PARAMETERTYPEREL = 'rel';
    Private Static Final String VALUETEMPLATELABEL = 'Commercial Template';
    Private Static Final String VALUETEMPLATELABEL2 = 'Account Template';
    Private Static Final String VALUENONE = 'None';
    Private Static Final String ACCOUNTONLY = 'Account';
    Private Static Final String ACCOUNTLOAN = 'AccountLoan';
    Private Static Final Decimal LOANAMOUNT = 10000;
    Private Static Final Date THEDATE = System.today();
}