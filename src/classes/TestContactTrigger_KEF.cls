/*
This class is written for KEF KRR Project by TCS Team

It is a test class for KEF Contact object's Trigger
*/

@isTest
private class TestContactTrigger_KEF 
{

    static testMethod void TestAccIntStatusUpdate()
    {
        DmlException expectedException;
        List<Account> accList = TestUtilityClass.CreateTestAccount(1);
        Account acc = new Account();// = accList[0];       
        acc.Name ='TestName18';
        acc.BillingStreet = 'BillingStreet';
        acc.BillingCity = 'BillingCity';
        acc.BillingState = 'BillingState';
        acc.BillingPostalCode = 'BillingPostalCode';
        acc.BillingCountry = 'BillingCountry';
        acc.Phone = '7030357222';  
        //acc.Default_Payment_Method__c = 'True';
        acc.KEF_Risk_Role__c = System.Label.Supplier_Only;
        acc.Supplier_Status__c = System.Label.Pending_Credit;
        insert acc;
        LLC_BI__Treasury_Service__c treasuryObj = new LLC_BI__Treasury_Service__c();
        treasuryObj.name = 'Treasury Record';
        insert treasuryObj;
        LLC_BI__Legal_Entities__c lege= new LLC_BI__Legal_Entities__c();
        lege.name = 'Test legal';
        lege.LLC_BI__Account__c = acc.id;
        lege.LLC_BI__Treasury_Service__c = treasuryObj.id;
        insert lege;
        
        List<Contact> conList = TestUtilityClass.createTestContacts(2, acc.id);
        Contact conObj = TestUtilityClass.createTestContacts(1, acc.id)[0];
        conObj.Authorized_Signer__c = true;
        insert conObj;
        //insert conList;
        
        test.startTest();
        try{
            /*  acc.KEF_Risk_Role__c = System.Label.Vendor_Supplier;
            acc.Vendor_Status__c = System.Label.Pending_Credit;
            acc.Integrated__c = true;
            update acc;      
            */
            //for(Contact con : conList){
                conObj.Contact_Status__c = System.Label.Active;
                conObj.Authorized_Signer__c = false;
            //}
            Update conObj;  
            
            //Legal Entity record 
            
        }
        catch(DMLException dmlEx)
        {
            system.debug('dmlEx.getDMLMessage>>>>      '+dmlEx);
            System.assertEquals(StatusCode.CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY,dmlEx.getDmlType(0));
            expectedException = dmlEx;
        }    
        test.stopTest();
        
        Account insertedAccount = [SELECT id, Integration_Status__c FROM Account WHERE id = :acc.id];
        System.assertEquals(System.Label.Not_Picked_Up, insertedAccount.Integration_Status__c);
    }
    
    static testMethod void testAgrIntStatusUpdate(){
        
        Agreement__c agr = TestUtilityClass.CreateTestAgreement();
        agr.Integrated__c = true;
        
        List<Account> accList = TestUtilityClass.CreateTestAccount(1);
        //Account acc = accList[0];  
        Account acc = new Account(); 
        acc.Name ='TestName18';
        acc.BillingStreet = 'BillingStreet';
        acc.BillingCity = 'BillingCity';
        acc.BillingState = 'BillingState';
        acc.BillingPostalCode = 'BillingPostalCode';
        acc.BillingCountry = 'BillingCountry';
        acc.Phone = '7030357222';   
        acc.KEF_Risk_Role__c = System.Label.Supplier_Only;
        acc.Supplier_Status__c = System.Label.Pending_Credit;
        insert agr;        
        insert acc;
        LLC_BI__Treasury_Service__c treasuryObj = new LLC_BI__Treasury_Service__c();
        treasuryObj.name = 'Treasury Record';
        insert treasuryObj;
        
        LLC_BI__Legal_Entities__c lege= new LLC_BI__Legal_Entities__c();
        lege.name = 'Test legal';
        lege.LLC_BI__Account__c =acc.id;
        lege.LLC_BI__Treasury_Service__c =treasuryObj.id;
        insert lege;
        
        /*Contact conObj = TestUtilityClass.createTestContacts(1, acc.id)[0];
        conObj.Authorized_Signer__c = false;
        insert conObj;*/
        
        List<Contact_Agreement__c> conAgrAssoList = new List<Contact_Agreement__c>();            
        
        test.startTest();
        
        
        List<Contact> conList = TestUtilityClass.createTestContacts(2, acc.id);
        
        conlist[0].Authorized_Signer__c = false;
        insert conList;
        Contact_Agreement__c  conAgrAsso = TestUtilityClass.createCAAssociations(conList[0].id,agr.id);
        conAgrAssoList.add(conAgrAsso);
        conAgrAsso = TestUtilityClass.createCAAssociations(conList[1].id,agr.id);
        conAgrAssoList.add(conAgrAsso);   
        insert conAgrAssoList;
        
        for(Contact con : conList){
            con.Contact_Status__c = System.Label.Active;
        }
        Update conList;
        conlist[0].Authorized_Signer__c = true;
        update conlist[0];
        //Legal Entity record
        test.stopTest();
        
        Agreement__c UpdatedAgr = [SELECT id, Integration_Status__c FROM Agreement__c WHERE id = :agr.id];
        System.assertEquals(System.Label.Not_Picked_Up, UpdatedAgr.Integration_Status__c);
    }
}