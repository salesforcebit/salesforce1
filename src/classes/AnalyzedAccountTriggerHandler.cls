public without sharing class AnalyzedAccountTriggerHandler {

      public void handleBeforeUpdate(Map<ID, LLC_BI__Analyzed_Account__c> siNewMap, Map<ID, LLC_BI__Analyzed_Account__c> siOldMap){
      	System.debug('In the trigger');
        Set<Id> AnalyzedAccountIds = new Set<Id>();
          
        Set<String> values = new Set<String>();
        for(Id si : siNewMap.keyset()){
          System.debug('Id of the new map - AA '+si);  
          LLC_BI__Analyzed_Account__c newAcct = siOldMap.get(si);
          LLC_BI__Analyzed_Account__c oldAcct = siNewMap.get(si);
          
          System.debug('new acct '+newAcct);  
          System.debug('old acct '+oldAcct);  
          
          if(newAcct.GTM_Account__c) {
             System.debug('gtm account is true');
              //Previous_Day_Options__c,Previous_Day_Via__c,Previous_Day_Summary_and_Detail_Options__c,Previous_Day_Credit__c,Previous_Day_Debit__c
              if(newAcct.Previous_Day_Options__c != null) {
                  if(oldAcct.Previous_Day_Options__c != null) {
                      if(!newAcct.Previous_Day_Options__c.equals(oldAcct.Previous_Day_Options__c)) {
                          System.debug('Previous_Day_Options__c');
                  		  values.add('Previous Day');
                      }
                  }
                  else {
                      System.debug('Previous_Day_Options__c not null');
                  	  values.add('Previous Day');
                  }
              } 
              
              if(newAcct.Previous_Day_Via__c != null) {
                  if(oldAcct.Previous_Day_Via__c != null) {
                      if(!newAcct.Previous_Day_Via__c.equals(oldAcct.Previous_Day_Via__c)) {
                          System.debug('Previous_Day_Via__c');
                  		  values.add('Previous Day');
                      }
                  }
                  else {
                      System.debug('Previous_Day_Via__c not null');
                  	  values.add('Previous Day');
                  }
              } 
              
              if(newAcct.Previous_Day_Summary_and_Detail_Options__c != null) {
                  if(oldAcct.Previous_Day_Summary_and_Detail_Options__c != null) {
                      if(!newAcct.Previous_Day_Summary_and_Detail_Options__c.equals(oldAcct.Previous_Day_Summary_and_Detail_Options__c)) {
                          System.debug('Previous_Day_Summary_and_Detail_Options__c');
                  		  values.add('Previous Day');
                      }
                  }
                  else {
                      System.debug('Previous_Day_Summary_and_Detail_Options__c not null');
                  	  values.add('Previous Day');
                  }
              } 
              
              // Current Day ACH
              if(newAcct.Current_Day_Options__c != null) {
                  if(oldAcct.Current_Day_Options__c != null) {
                      if(!newAcct.Current_Day_Options__c.equals(oldAcct.Current_Day_Options__c)) {
                          System.debug('Current_Day_Options__c');
                  		  values.add('Current Day ACH');
                      }
                  }
                  else {
                      System.debug('Current_Day_Options__c not null');
                  	  values.add('Current Day ACH');
                  }
              } 
              
              // Current Day Wires
              if(newAcct.Intradays_Wires_Options__c != null) {
                  if(oldAcct.Intradays_Wires_Options__c != null) {
                      if(!newAcct.Intradays_Wires_Options__c.equals(oldAcct.Intradays_Wires_Options__c)) {
                          System.debug('Intradays_Wires_Options__c');
                  		  values.add('Current Day Wires');
                      }
                  }
                  else {
                      System.debug('Intradays_Wires_Options__c not null');
                  	  values.add('Current Day Wires');
                  }
              } 
              
              // Current Day CDA,CDA_Opt__c,HOP_Options__c,Check_Length__c
              if(newAcct.CDA_Options__c != null) {
                  if(oldAcct.CDA_Options__c != null) {
                      if(!newAcct.CDA_Options__c.equals(oldAcct.CDA_Options__c)) {
                          System.debug('CDA_Options__c');
                  		  values.add('Current Day CDA');
                      }
                  }
                  else {
                      System.debug('CDA_Options__c not null');
                  	  values.add('Current Day CDA');
                  }
              } 
              
              if(newAcct.CDA_Opt__c != null) {
                  if(oldAcct.CDA_Opt__c != null) {
                      if(!newAcct.CDA_Opt__c.equals(oldAcct.CDA_Opt__c)) {
                          System.debug('CDA_Opt__c');
                  		  values.add('Current Day CDA');
                      }
                  }
                  else {
                      System.debug('CDA_Opt__c not null');
                  	  values.add('Current Day CDA');
                  }
              } 
              
              if(newAcct.HOP_Options__c != null) {
                  if(oldAcct.HOP_Options__c != null) {
                      if(!newAcct.HOP_Options__c.equals(oldAcct.HOP_Options__c)) {
                          System.debug('HOP_Options__c');
                  		  values.add('Current Day CDA');
                      }
                  }
                  else {
                      System.debug('HOP_Options__c not null');
                  	  values.add('Current Day CDA');
                  }
              } 
              
              if(newAcct.Check_Length__c != null) {
                  if(oldAcct.Check_Length__c != null) {
                      if(newAcct.Check_Length__c == oldAcct.Check_Length__c) {
                          
                      }
                      else {
                          System.debug('Check_Length__c');
                  		  values.add('Current Day CDA');
                      }
                  }
                  else {
                      System.debug('Check_Length__c not null');
                  	  values.add('Current Day CDA');
                  }
              } 
              
              // Current day OLDS
              if(newAcct.Olds_Options__c != null) {
                  if(oldAcct.Olds_Options__c != null) {
                      if(!newAcct.Olds_Options__c.equals(oldAcct.Olds_Options__c)) {
                          System.debug('Olds_Options__c');
                  		  values.add('Current Day OLDS Memo Post');
                      }
                  }
                  else {
                      System.debug('Olds_Options__c not null');
                  	  values.add('Current Day OLDS Memo Post');
                  }
              } 
              
              //Current day Lockbox
              if(newAcct.Lockbox_Options__c != null) {
                  if(oldAcct.Lockbox_Options__c != null) {
                      if(!newAcct.Lockbox_Options__c.equals(oldAcct.Lockbox_Options__c)) {
                          System.debug('Lockbox_Options__c');
                  		  values.add('Current Day Lockbox');
                      }
                  }
                  else {
                      System.debug('Lockbox_Options__c not null');
                  	  values.add('Current Day Lockbox');
                  }
              } 
              
              if(newAcct.Lockbox_Number__c != null) {
                  if(oldAcct.Lockbox_Number__c != null) {
                      if(newAcct.Lockbox_Number__c == oldAcct.Lockbox_Number__c) {
                          
                      }
                      else {
                          System.debug('Lockbox_Number__c');
                  		  values.add('Current Day Lockbox');
                      }
                  }
                  else {
                      System.debug('Lockbox_Number__c not null');
                  	  values.add('Current Day Lockbox');
                  }
              } 
              
              // Current Day Deposit Items 
              if(newAcct.DepItems_Options__c != null) {
                  if(oldAcct.DepItems_Options__c != null) {
                      if(!newAcct.DepItems_Options__c.equals(oldAcct.DepItems_Options__c)) {
                          System.debug('DepItems_Options__c');
                  		  values.add('Current Day Deposit Items');
                      }
                  }
                  else {
                      System.debug('DepItems_Options__c not null');
                  	  values.add('Current Day Deposit Items');
                  }
              } 
              
              // ESP 
              if(newAcct.ESP_Options__c != null) {
                  if(oldAcct.ESP_Options__c != null) {
                      if(!newAcct.ESP_Options__c.equals(oldAcct.ESP_Options__c)) {
                          System.debug('ESP_Options__c');
                  		  values.add('ESP Reports');
                      }
                  }
                  else {
                      System.debug('ESP_Options__c not null');
                  	  values.add('ESP Reports');
                  }
              } 
              
              //Customer Reports
              if(newAcct.CS_Options__c != null) {
                  if(oldAcct.CS_Options__c != null) {
                      if(!newAcct.CS_Options__c.equals(oldAcct.CS_Options__c)) {
                          System.debug('CS_Options__c');
                  		  values.add('Customer Statements');
                      }
                  }
                  else {
                      System.debug('CS_Options__c not null');
                  	  values.add('Customer Statements');
                  }
              } 
              
              
              List<String> newList = new List<String>();
              newList.addAll(values);
              newAcct.Updated_Service_Options__c = String.join(newList,';');
              newAcct.Service_Options_Added__c = true;
              
              System.debug('newAcct.Updated_Service_Options__c'+newAcct.Updated_Service_Options__c);
              
               
          }
            	  
        
      }
   }
}