@isTest
public class TestECCP_CovenantCompApprovalController {
	
    @isTest
    public static void noProductPackage(){
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, null, 'Complete', 'In - Progress');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
    }
    
    @isTest
    public static void loadLocked(){
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'Approved');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
    }
    
    @isTest
    public static void loadUnlocked(){
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
    }
    
    @isTest
    static void save() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.covenantComplianceSave();
        
        Test.stopTest();
    }
    
    @isTest
    public static void cancel() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.cancel();
            
        Test.stopTest();
    }
    
    @isTest
    public static void delegateAuthority() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getdelegatedAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority1() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority2() {
        Account testAccount = createAccount('10');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority3() {
        Account testAccount = createAccount('17');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void reviewer1() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getreviewer1();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer2() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getreviewer2();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer3() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getreviewer3();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer4() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getreviewer4();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer5() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getreviewer5();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer6() {
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id, System.today(), 'Complete', 'Approved');
        LLC_BI__Covenant_Type__c testCovenantType = createCovenantType();
        LLC_BI__Covenant2__c testCovenant = createCovenant(testCovenantType.Id);
        LLC_BI__Account_Covenant__c testAccountCovenant = createAccountCovenant(testAccount.Id, testCovenant.Id);
        LLC_BI__Covenant_Compliance2__c testCovenantCompliance = createCovenantCompliance(testCovenant.Id, 'In-Progress');
        
        ApexPages.currentPage().getParameters().put('id', testCovenantCompliance.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CovenantCompApprovalController CCAC = new ECCP_CovenantCompApprovalController(controller);
        
        Test.startTest();
        
        CCAC.getreviewer6();
        
        Test.stopTest();
    }
    
    private static Account createAccount(String grade) {
        Account newA = new Account(
        	Name = 'Test',
        	Proposed_Risk_Rating__c = grade);
        
        insert newA;
        return newA;
    }
    
    private static LLC_BI__Product_Package__c createProductPackage(Id acctId, Date aDate, String stage, String status) {
        LLC_BI__Product_Package__c newPP = new LLC_BI__Product_Package__c(
        	Name = 'Test',
        	LLC_BI__Account__c = acctId,
        	LLC_BI__Primary_Officer__c = UserInfo.getUserId(),
        	ECC_Product_Package_Approval_Date__c = aDate,
        	LLC_BI__Stage__c = stage,
        	LLC_BI__Status__c = status);
        
        insert newPP;
        return newPP;
    }
    
    private static LLC_BI__Covenant_Type__c createCovenantType() {
        LLC_BI__Covenant_Type__c newCT = new LLC_BI__Covenant_Type__c(
        	Name = 'Type 1',
        	LLC_BI__Category__c = 'Category',
        	ECC_Covenant_Standard_Def__c = 'Definition Text');
       	
        insert newCT;
        return newCT;
    }
    
    private static LLC_BI__Account_Covenant__c createAccountCovenant(Id acctId, Id covenantId) {
        LLC_BI__Account_Covenant__c newAC = new LLC_BI__Account_Covenant__c(
        	LLC_BI__Covenant2__c = covenantId,
        	LLC_BI__Account__c = acctId);
        
        insert newAC;
        return newAC;
    }
    
    private static LLC_BI__Covenant2__c createCovenant(Id typeId) {
        LLC_BI__Covenant2__c newC = new LLC_BI__Covenant2__c(
        	LLC_BI__Next_Evaluation_Date__c = System.today(),
        	LLC_BI__Covenant_Type__c = typeId);
        
        insert newC;
        return newC;
    }
    
    private static LLC_BI__Covenant_Compliance2__c createCovenantCompliance(Id recordId, String status) {
        LLC_BI__Covenant_Compliance2__c newCC = new LLC_BI__Covenant_Compliance2__c(
        	LLC_BI__Covenant__c = recordId,
        	LLC_BI__Status__c = status);
        
        insert newCC;
        return newCC;
    }
}