public without sharing class TreasuryServiceTriggerHandler {

    private boolean beforeInsert {get;set;}
    private boolean afterInsert {get;set;}

    //Used to prevent trigger running multiple times due to field updates
    private static Map<Id, LLC_BI__Treasury_Service__c> previousNewMap {get {
        if(previousNewMap == null) {
            previousNewMap = new Map<Id, LLC_BI__Treasury_Service__c>();
        }
        return previousNewMap;
    }set;}

    private Map<Id, LLC_BI__Treasury_Service__c> oldMap {get;set;}
    private Map<Id, LLC_BI__Treasury_Service__c> newMap {get;set;}
    private List<LLC_BI__Treasury_Service__c> newList {get;set;}
    private Set<Id> ncinoProductIds {get {
        if(ncinoProductIds == null) {
            ncinoProductIds = new Set<Id>();
        }
        return ncinoProductIds;
    }set;}

    private Set<Id> packageIds {get {
        if(packageIds == null) {
            packageIds = new Set<Id>();
        }
        return packageIds;
    }set;}

    private Map<Id, LLC_BI__Product_Package__c> productPackageMap {get {
        if(productPackageMap == null) {
            productPackageMap = new Map<Id, LLC_BI__Product_Package__c>([Select Id, OwnerId from LLC_BI__Product_Package__c Where Id in: packageIds]);
        }
        return productPackageMap;
    }set;}

    private Set<Id> treasuryServiceStageChangedProductPackageIds {get {
        if(treasuryServiceStageChangedProductPackageIds == null) {
            treasuryServiceStageChangedProductPackageIds = new Set<Id>();
        }
        return treasuryServiceStageChangedProductPackageIds;
    }set;}

    Set<Id> stageIds {get {
        if(stageIds == null) {
            stageIds = new Set<Id>();
        }
        return stageIds;
    }set;}

    Set<String> queueAPINames {get {
        if(queueAPINames == null) {
            queueAPINames = new Set<String>();
        }
        return queueAPINames;
    }set;}

    Map<String, Id> queuesByAPIName {get;set;}

    private Map<String, TaskAndWorkflowAdminWrapper> taskAndWorkflowAdminWrappers {get {
        if(taskAndWorkflowAdminWrappers == null) {
            taskAndWorkflowAdminWrappers = new Map<String, TaskAndWorkflowAdminWrapper>();
        }

        return taskAndWorkflowAdminWrappers;
    }set;}

    public TreasuryServiceTriggerHandler(Map<Id, LLC_BI__Treasury_Service__c> oMap, Map<Id, LLC_BI__Treasury_Service__c> nMap) {
        oldMap = oMap;
        newMap = nMap;
    }

    public TreasuryServiceTriggerHandler(List<LLC_BI__Treasury_Service__c> nList) {
        newList = nList;
    }

    public void handleBeforeInsert() {
        beforeInsert = true;
        processBeforeInsert();

        if(taskAndWorkflowAdminWrappers.size() > 0) {
            processTaskAndWorkflowAdministration();
        }

        updateProductPackageFields();
    }



    public void processBeforeInsert() {
        for(LLC_BI__Treasury_Service__c newTS : newList) {
            if(meetsWorkflowAdministrationCriteria(null, newTS)) {
                ncinoProductIds.add(newTS.LLC_BI__Product_Reference__c);
                packageIds.add(newTS.LLC_BI__Product_Package__c);
                taskAndWorkflowAdminWrappers.put(generateWrapperKey(newTS), new TaskAndWorkflowAdminWrapper(newTS));
            }
        }
    }

    private id integrationUserId {get{
        if(integrationUserId == null){
            integrationUserId = [SELECT id FROM Profile WHERE Name='Integration/Data Migration'].id;
        }
        return integrationUserId;
    }set;}
    //Returns the record type id for Product package for treasury
    private Id prodPackageTreasuryRecId {get{
        if(prodPackageTreasuryRecId == null){
            prodPackageTreasuryRecId = Schema.SObjectType.LLC_BI__Product_Package__c.getRecordTypeInfosByName().get('Treasury').getRecordTypeId();
        }
        return prodPackageTreasuryRecId;
    }set;}
    
    /*
        Created by Todd Tompkins
        Before Insert. if Product Package is not null, profile is not Integration/Data Migration then update Record Type to Treasury and LLC_BI_Stage to Order Entry
    */
    public void updateProductPackageFields(){
        //intialize var
        Id cUserProfId = UserInfo.getProfileId();
        Map<Id, LLC_BI__Product_Package__c> updateMap = new Map<Id, LLC_BI__Product_Package__c>();

        for(LLC_BI__Treasury_Service__c ts : newList){
            if(ts.LLC_BI__Product_Package__c != null && cUserProfId != integrationUserId ){
                updateMap.put(ts.LLC_BI__Product_Package__c, new LLC_BI__Product_Package__c(Id=ts.LLC_BI__Product_Package__c, LLC_BI__Stage__c='Order Entry', RecordTypeId = prodPackageTreasuryRecId) );
            }
        }
        update updateMap.values();
    }

    public void handleAfterInsert() {
        afterInsert = true;
        processAfterInsert();

        if(taskAndWorkflowAdminWrappers.size() > 0) {
            processTaskAndWorkflowAdministration();
        }
    }

    public void processAfterInsert() {
        for(LLC_BI__Treasury_Service__c newTS : newMap.values()) {
            if(meetsWorkflowAdministrationCriteria(null, newTS)) {
                ncinoProductIds.add(newTS.LLC_BI__Product_Reference__c);
                packageIds.add(newTS.LLC_BI__Product_Package__c);
                taskAndWorkflowAdminWrappers.put(generateWrapperKey(newTS), new TaskAndWorkflowAdminWrapper(newTS));
            }
            previousNewMap.put(newTS.ID, newTS);
        }
    }

    public void handleBeforeUpdate() {
        system.debug('CarPow handle before update');
        processBeforeUpdate();

        if(taskAndWorkflowAdminWrappers.size() > 0) {
            processTaskAndWorkflowAdministration();
        }
    }

    //Determine which records meet criteria that requires further processing
    public void processBeforeUpdate() {
        for(LLC_BI__Treasury_Service__c newTS : newMap.values()) {
            LLC_BI__Treasury_Service__c oldTS = previousNewMap.get(newTS.Id) == null ? oldMap.get(newTS.Id) : previousNewMap.get(newTS.Id);

            if(meetsWorkflowAdministrationCriteria(oldTS, newTS)) {
                ncinoProductIds.add(newTS.LLC_BI__Product_Reference__c);
                packageIds.add(newTS.LLC_BI__Product_Package__c);
                taskAndWorkflowAdminWrappers.put(generateWrapperKey(newTS), new TaskAndWorkflowAdminWrapper(newTS));
            }

            previousNewMap.put(newTS.Id, newTS);
        }
    }

    public void processTaskAndWorkflowAdministration() {
        setWorkflowTemplateAndStageId();
        setStages();
        getQueues();
        if(afterInsert != true) {
            setTreasuryServiceFields();
        }
        if(beforeInsert != true) {
            System.debug('CarPow was here!');
            insertSetupTasks();
        }
    }

    public void setWorkflowTemplateAndStageId() {
        //Find all workflow templates for the products on the treasury service records
        system.debug('::I am here ');

        List<TSO_Workflow_Template__c> temps = [Select t.Type__c, t.SLA__c, t.Product__c, (Select Id, Related_Treasury_Stage__c From Stages__r) From TSO_Workflow_Template__c t Where Product__c in: ncinoProductIds];

        system.debug('::temps ' + temps);
        for(TSO_Workflow_Template__c workflowTemplate : [Select t.Type__c, t.SLA__c, t.Product__c, (Select Id, Related_Treasury_Stage__c From Stages__r) From TSO_Workflow_Template__c t Where Product__c in: ncinoProductIds]) {
            String key = generateWrapperKey(workflowTemplate);
            system.debug('::key ' + key);
            system.debug('::wrapper keys ' + taskAndWorkflowAdminWrappers.keyset());
            if(taskAndWorkflowAdminWrappers.containsKey(key)) {
                TaskAndWorkflowAdminWrapper wrapper = taskAndWorkflowAdminWrappers.get(key);
                wrapper.tsoWorkflowTemplate = workflowTemplate;
                System.debug('::all stage ids ' + workflowTemplate.Stages__r);
                for(TSO_Stage__c stage : workflowTemplate.Stages__r) {
                    if(stage.Related_Treasury_Stage__c == wrapper.treasuryService.LLC_BI__Stage__c) {
                        System.debug('::stageid ' + stage.id);
                        wrapper.tsoStage = new TSO_Stage__c(id = stage.Id);
                        stageIds.add(stage.Id);
                    }
                }
            }
        }
    }

    public void setStages() {
        Map<Id, TSO_Stage__c> stages = new Map<Id, TSO_Stage__c>([Select t.Id, t.Expected_Days_to_Complete__c, (Select Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, Parent_Stage__c, Stage__c, Expected_Completion_Time__c, Description__c, Third_Party_Task__c, Responsible_Third_Party__c, Queue_API_Name__c, Assignment__c, Setup_Task_Template_IDL_ID__c, Parent_Setup_Task_Unique_ID__c, Parent_Setup_Task__c From Setup_Task_Templates__r Where Dependent__c = false) From TSO_Stage__c t Where Id in: stageIds]);

        for(TaskAndWorkflowAdminWrapper wrapper : taskAndWorkflowAdminWrappers.values()) {
            if(wrapper.tsoStage != null && stages.containsKey(wrapper.tsoStage.Id)) {
                TSO_Stage__c stage = stages.get(wrapper.tsoStage.Id);
                wrapper.tsoStage = stage;
                wrapper.tsoSetupTaskTemplates = stage.Setup_Task_Templates__r;
                for(TSO_Setup_Task_Template__c setuptasktemplate : wrapper.tsoSetupTaskTemplates) {
                    queueAPINames.add(setuptasktemplate.Queue_API_Name__c);
                }
            }
        }
    }

    private void getQueues() {
        queueAPINames.remove(null);
        queuesByAPIName = new Map<String, Id>();
        for(Group g : [Select Id, DeveloperName from Group Where DeveloperName in: queueAPINames]) {
            queuesByAPIName.put(g.DeveloperName, g.Id);
        }
    }

    public void setTreasuryServiceFields() {
        for(TaskAndWorkflowAdminWrapper wrapper : taskAndWorkflowAdminWrappers.values()) {
            wrapper.setTreasuryServiceFields();
        }
    }

    public void insertSetupTasks() {
        List<TSO_Setup_Task__c> setupTasks = new List<TSO_Setup_Task__c>();
        Map<Id, LLC_BI__Treasury_Service__c> id2TreasMap = new Map<Id, LLC_BI__Treasury_Service__c>();
        //for(TaskAndWorkflowAdminWrapper wrapper : taskAndWorkflowAdminWrappers.values()) {
        //  if(wrapper.treasuryService.Parent_Treasury_Service__c != null)
        //      id2TreasMap.put(wrapper.treasuryService.Id, null);
        //}

        //if(id2TreasMap.keyset().size() > 0)
        //  id2TreasMap = new Map<id, LLC_BI__Treasury_Service__c>([SELECT id, Parent_Treasury_Service__c, Parent_Treasury_Service__r.LLC_BI__Product_Package__c FROM LLC_BI__Treasury_Service__c WHERE Id IN:id2TreasMap.keyset()]);
        for(TaskAndWorkflowAdminWrapper wrapper : taskAndWorkflowAdminWrappers.values()) {
            System.debug('::wrapper ' + wrapper);
            if(wrapper.tsoSetupTaskTemplates != null) {
                //setupTasks.addAll(createSetupTasksFromTemplates(wrapper.tsoSetupTaskTemplates, wrapper.treasuryService.Id, (wrapper.treasuryService.Parent_Treasury_Service__c == null ?  wrapper.treasuryService.LLC_BI__Product_Package__c : id2TreasMap.get(wrapper.treasuryService.Id).Parent_Treasury_Service__r.LLC_BI__Product_Package__c), wrapper.treasuryService.OwnerId));
                setupTasks.addAll(createSetupTasksFromTemplates(wrapper.tsoSetupTaskTemplates, wrapper.treasuryService.Id, wrapper.treasuryService.LLC_BI__Product_Package__c, wrapper.treasuryService.OwnerId));
            }

        }

        if(setupTasks.size() > 0) {
            System.debug(setupTasks);
            insert setupTasks;
        }

        //added by MDorus@WestMonroePartners.com, 4/21/2016
        addParentTask(setupTasks);

    }



    public Boolean meetsWorkflowAdministrationCriteria(LLC_BI__Treasury_Service__c oldTS, LLC_BI__Treasury_Service__c newTS) {
        if(oldTS != null) {
            return ((newTS.LLC_BI__Stage__c != oldTS.LLC_BI__Stage__c && oldTS.LLC_BI__Stage__c != Constants.treasuryServiceStagePendingTaskOnHold) || (newTS.Type__c != oldTS.Type__c)) && newTS.LLC_BI__Product_Reference__c != null && newTS.Type__c != null;
        }
        return (newTS.LLC_BI__Product_Reference__c!= null && newTS.Type__c != null);//For insert Treasury Services
    }

    public String generateWrapperKey(LLC_BI__Treasury_Service__c ts) {
        return ts.LLC_BI__Product_Reference__c + ts.Type__c;
    }

    public String generateWrapperKey(TSO_Workflow_Template__c workflowTemplate) {
        return workflowTemplate.Product__c + workflowTemplate.Type__c;
    }

    public List<TSO_Setup_Task__c> createSetupTasksFromTemplates(List<TSO_Setup_Task_Template__c> tsoSetupTaskTemplates, Id treasuryServiceId, Id productPackageId, Id treasuryServiceOwnerId) {
        System.debug('tsoSetupTaskTemplates ' + tsoSetupTaskTemplates);
        System.debug('treasuryServiceId ' + treasuryServiceId);
        System.debug('productPackageId ' + productPackageId);
        System.debug('treasuryServiceOwnerId ' + treasuryServiceOwnerId);
        List<TSO_Setup_Task__c> tsoSetupTasks = new List<TSO_Setup_Task__c>();

        for(TSO_Setup_Task_Template__c tsoSetupTaskTemplate : tsoSetupTaskTemplates) {
            TSO_Setup_Task__c tsoSetupTask = createSetupTaskFromTemplate(tsoSetupTaskTemplate, treasuryServiceId, productPackageId, treasuryServiceOwnerId);
            if(tsoSetupTask != null) {
                tsoSetupTasks.add(tsoSetupTask);
            }
        }

        return tsoSetupTasks;
    }

    public TSO_Setup_Task__c createSetupTaskFromTemplate(TSO_Setup_Task_Template__c
        tsoSetupTaskTemplate, Id treasuryServiceId, Id productPackageId, Id treasuryServiceOwnerId) {
        Id parentTaskRecordTypeId = Schema.SObjectType.TSO_Setup_Task__c.getRecordTypeInfosByName().get('Parent Task').getRecordTypeId();
        System.debug('productPackageId::'+productPackageId);

        TSO_Setup_Task__c tsoSetupTask = new TSO_Setup_Task__c();
        Integer nameMaxLength = TSO_Setup_Task__c.Name.getDescribe().getLength();
        tsoSetupTask.Name = tsoSetupTaskTemplate.Description__c != null && tsoSetupTaskTemplate.Description__c.length() > nameMaxLength ? tsoSetupTaskTemplate.Description__c.subString(0, nameMaxLength) : tsoSetupTaskTemplate.Description__c;
        tsoSetupTask.Description__c = tsoSetupTaskTemplate.Description__c;
        tsoSetupTask.Expected_Completion_Time_Days__c = tsoSetupTaskTemplate.Expected_Completion_Time__c;
        tsoSetupTask.Responsible_Third_Party__c = tsoSetupTaskTemplate.Responsible_Third_Party__c;
        tsoSetupTask.Related_Stage__c = tsoSetupTaskTemplate.Stage__c;
        tsoSetupTask.Third_Party_Task__c = tsoSetupTaskTemplate.Third_Party_Task__c;
        tsoSetupTask.Setup_Task_Template__c = tsoSetupTaskTemplate.Id;
        tsoSetupTask.Treasury_Service__c = treasuryServiceId;
        tsoSetupTask.Product_Package__c = productPackageId;
        tsoSetupTask.Treasury_Service_Owner__c = treasuryServiceOwnerId;
        tsoSetupTask.Status__c = Constants.TSOSetupTaskStatusNew;
        tsoSetupTask.parent_Task__c = tsoSetupTaskTemplate.Parent_Setup_Task__c;
        tsoSetupTask.OwnerId = treasuryServiceOwnerId;

        System.debug('tsoSetupTask ' + tsoSetupTask);

        //check if template parent checked
        if(!tsoSetupTaskTemplate.Parent_Setup_Task__c){
            //set Parent Task added by MDorus@westmonroepartners.com
            //set parent based on the parent Setup_Task_Template_IDL_ID__c field
            tsoSetupTask.Setup_Task_Template_IDL_ID__c = tsoSetupTaskTemplate.Parent_Setup_Task_Unique_ID__c;
            //TODO:
            //looks like task owner is set in the queue, need to find out if they
            //want me to put an owner on at all if its a child task
        }
        //if it is the parent, put its own template ID in
         else {
            tsoSetupTask.RecordTypeId = parentTaskRecordTypeId;
            tsoSetupTask.Setup_Task_Template_IDL_ID__c = tsoSetupTaskTemplate.Setup_Task_Template_IDL_ID__c;
        }

        assignTSOSetupTaskOwner(tsoSetupTask, tsoSetupTaskTemplate);
        return tsoSetupTask;
    }

    /**
        Added by MDorus@westmonroepartners.com April 21, 2016
        This method adds the parent task id to child tasks and
        changes the record type for child classes to Child Task

    */
    public void addParentTask(List<TSO_Setup_Task__c> setupTasks){
        //seperate parent and child tasks
        List<TSO_Setup_Task__c> parentTaskList = new List<TSO_Setup_Task__c>();
        List<TSO_Setup_Task__c> childTaskList = new List<TSO_Setup_Task__c>();

        Set<id> taskSet = new Set<id>();
        for(TSO_Setup_Task__c task : setupTasks){
            taskSet.add(task.id);
        }
        List<TSO_Setup_Task__c> tasksWithIds = [Select id, RecordTypeId, Treasury_Service__c, Setup_Task_Template_IDL_ID__c, Parent_Setup_Task__c, parent_Task__c from TSO_Setup_Task__c where id in :taskSet];

        Id childTaskRecordTypeId = Schema.SObjectType.TSO_Setup_Task__c.getRecordTypeInfosByName().get('Child Task').getRecordTypeId();

        for(TSO_Setup_Task__c task : tasksWithIds){
            //check if parent
            if(task.parent_Task__c){
                parentTaskList.add(task);
            } else {
                childTaskList.add(task);
            }
        }

        //create a map with the key of the treasury service(Treasury_Service__c), then inside that
        // is another map with the key of the unique TaskTemplate ID(Setup_Task_Template_IDL_ID__c)
        // and the value of that is the task Id
        Map<ID, Map<String, Id>> parentTaskIDMap = new Map<Id, Map<String, Id>>();

        if(parentTaskList.size() > 0){
            for(TSO_Setup_Task__c task : parentTaskList){


                Map<String, Id> templateMap = parentTaskIDMap.get(task.Treasury_Service__c);
                if(templateMap == null){
                    templateMap = new Map<String, Id>();
                    parentTaskIDMap.put(task.Treasury_Service__c, templateMap);
                }

                templateMap.put(task.Setup_Task_Template_IDL_ID__c, task.id);
            }
            //loop through task list and get the parent ID from the Map
            for(TSO_Setup_Task__c task : childTaskList){

                task.Parent_Setup_Task__c = parentTaskIDMap.get(task.Treasury_Service__c).get(task.Setup_Task_Template_IDL_ID__c);
                task.RecordTypeId = childTaskRecordTypeId;
            }

            if(childTaskList.size() > 0){
                update childTaskList;
            }

        }
    }

    public void assignTSOSetupTaskOwner(TSO_Setup_Task__c tsoSetupTask, TSO_Setup_Task_Template__c tsoSetupTaskTemplate) {
        try {
            
            if(tsoSetupTaskTemplate.Assignment__c == Constants.tsoSetupTaskTemplateAssignmentClientOnboardingManager) {
                System.debug('Onboarding Manager Owner: ' + tsoSetupTask.OwnerId);
            }
            else if(tsoSetupTaskTemplate.Assignment__c == Constants.tsoSetupTaskTemplateAssignmentClientManager) {
                //tsoSetupTask.OwnerId = 'TBD';
            }
            else if(tsoSetupTaskTemplate.Assignment__c == Constants.tsoSetupTaskTemplateAssignmentQueue) {
                if(queuesByAPIName.containsKey(tsoSetupTaskTemplate.Queue_API_Name__c)) {
                    tsoSetupTask.OwnerId = queuesByAPIName.get(tsoSetupTaskTemplate.Queue_API_Name__c);
                }
                else {
                    throw new OwnerException('The owner defined in the setup task template (' + tsoSetupTaskTemplate.Id + ') does not exist. Please contact your administrator to remediate this issue.');
                }
            }
        } catch(OwnerException e) {
            newMap.get(tsoSetupTask.Treasury_Service__c).addError(e.getMessage());
        }
    }

    public Id getPackageOwner(Id treasuryServiceId) {
        if(newMap.containsKey(treasuryServiceId)) {
            LLC_BI__Treasury_Service__c ts = newMap.get(treasuryServiceId);
            if(productPackageMap.containsKey(ts.LLC_BI__Product_Package__c)) {
                return productPackageMap.get(ts.LLC_BI__Product_Package__c).OwnerId;
            }
        }

        return null;
    }

    public void handleAfterUpdate() {
        processAfterUpdate();

        if(treasuryServiceStageChangedProductPackageIds.size() > 0) {
            processProductPackageStages();
        }
    }

    private void processAfterUpdate() {
        for(LLC_BI__Treasury_Service__c newTS : newMap.values()) {
            LLC_BI__Treasury_Service__c oldTS = oldMap.get(newTS.Id);

            if(newTS.LLC_BI__Stage__c != oldTS.LLC_BI__Stage__c) {
                treasuryServiceStageChangedProductPackageIds.add(newTS.LLC_BI__Product_Package__c);
            }
        }
    }

    private void processProductPackageStages() {
        List<LLC_BI__Product_Package__c> updateProductPackages = new List<LLC_BI__Product_Package__c>();
        for(LLC_BI__Product_Package__c productPackage : [Select l.LLC_BI__Stage__c, (Select LLC_BI__Stage__c From LLC_BI__Treasury_Services__r) From LLC_BI__Product_Package__c l Where Id in: treasuryServiceStageChangedProductPackageIds]) {
            String earliestStage = earliestStage(productPackage);
            if(productPackage.LLC_BI__Stage__c != earliestStage) {
                productPackage.LLC_BI__Stage__c = earliestStage;
                updateProductPackages.add(productPackage);
            }
        }
        if(updateProductPackages.size() > 0) {
            update updateProductPackages;
        }
    }

    private String earliestStage(LLC_BI__Product_Package__c productPackage) {
        String earliestStage;
        for(LLC_BI__Treasury_Service__c treasuryService : productPackage.LLC_BI__Treasury_Services__r) {
            earliestStage = earlierStage(treasuryService.LLC_BI__Stage__c, earliestStage);
        }
        return earliestStage;
    }

    private String earlierStage(String stage1, String stage2) {
        Decimal string1Value = stageValues.get(stage1);
        Decimal string2Value = stageValues.get(stage2);

        if(string2Value == null || string1Value < string2Value) {
            return stage1;
        }
        return stage2;
    }

    private Map<String, Decimal> stageValues {get {
        if(stageValues == null) {
            stageValues = new Map<String, Decimal>();
            for(Treasury_Service_Stage_Order__mdt stageOrder : [Select Id, Current_Stage__c, Stage_Order__c from Treasury_Service_Stage_Order__mdt ]) {
                stageValues.put(stageOrder.Current_Stage__c, stageOrder.Stage_Order__c);
            }
        }
        return stageValues;
    }set;}

    public class TaskAndWorkflowAdminWrapper {

        public LLC_BI__Treasury_Service__c treasuryService {get;set;}
        public TSO_Workflow_Template__c tsoWorkflowTemplate {get;set;}
        public TSO_Stage__c tsoStage {get;set;}
        public List<TSO_Setup_Task_Template__c> tsoSetupTaskTemplates {get;set;}

        public TaskAndWorkflowAdminWrapper(LLC_BI__Treasury_Service__c ts) {
            treasuryService = ts;
        }

        public void setTreasuryServiceFields() {
            if(tsoWorkflowTemplate != null) {
                treasuryService.SLA__c = tsoWorkflowTemplate.SLA__c;
            }
            treasuryService.Current_Stage_SLA__c = tsoStage != null ? tsoStage.Expected_Days_to_Complete__c : null;
        }
    }
    
    public class OwnerException extends Exception {
        
    }
}