public class ECCP_LPL_WebSvcCallout{
public static ECCP_LPL_Service.CreateLoanResponseType LPLCallout(ECCP_ALB_LPL_Data reqobj){
    /*
    ECCP_LPL_WS_Helper Helper = new ECCP_LPL_WS_Helper();
    Helper.ECCP_LPLProcess(reqobj);
    */
    ECCP_LPL_Service.loanTransactionService service = new ECCP_LPL_Service.loanTransactionService();
    service.endpoint_x  = 'http://api.salesforce.com/';

    ECCP_LPL_Service.CreateLoanRequestType  req = new ECCP_LPL_Service.CreateLoanRequestType();
    
    list<ECCP_LPL_Service.GeneralDataType> GenReqList = new list<ECCP_LPL_Service.GeneralDataType>();
    ECCP_LPL_Service.GeneralDataType GenReq = new ECCP_LPL_Service.GeneralDataType();
    GenReq.OriginalAmount = 34000000;
    GenReqList.add(GenReq);

    list<ECCP_LPL_Service.DisbursementDataType> DbsReqList = new list<ECCP_LPL_Service.DisbursementDataType>();
    ECCP_LPL_Service.DisbursementDataType DbsReq = new ECCP_LPL_Service.DisbursementDataType();
    DbsReq.DisbursementDescription = 'loan Disbursed';
    DbsReq.DisbursementPaymentNumber = 'LPL987000';
    DbsReqList .add(DbsReq );

    list<ECCP_LPL_Service.BorrowerDataType> BrwReqList = new list<ECCP_LPL_Service.BorrowerDataType>();
    ECCP_LPL_Service.BorrowerDataType BrwReq = new ECCP_LPL_Service.BorrowerDataType();
    BrwReq.OrganizationType = 'LLP';
    BrwReq.LastName = 'Test Lastname';
    BrwReqList.add(BrwReq);
    
    list<ECCP_LPL_Service.GuarantorDataType> GrnReqList = new list<ECCP_LPL_Service.GuarantorDataType>();
    ECCP_LPL_Service.GuarantorDataType GrnReq = new ECCP_LPL_Service.GuarantorDataType();
    GrnReq.GuarantorOrganizationType = 'Independence';
    GrnReq.GuarantorLastname = 'Guarantor Lastname';
    GrnReqList.add(GrnReq);
    
    list<ECCP_LPL_Service.SignerDataType> SignerReqList = new list<ECCP_LPL_Service.SignerDataType>();
    ECCP_LPL_Service.SignerDataType SignerReq  = new ECCP_LPL_Service.SignerDataType();
    SignerReq.OrganizationType = 'LLP';
    SignerReq.Lastname = 'Signer';
    SignerReqList.add(SignerReq);
    
    //list<ECCP_LPL_Service.CustomDataType> customReqList = new list<ECCP_LPL_Service.CustomDataType>();
    ECCP_LPL_Service.CustomDataType customReq = new ECCP_LPL_Service.CustomDataType();
    customReq.HostSystem = 'Mainframes';
    customReq.CRASalesCode ='CRACode';
    //customReqList.add(customReq);

    list<ECCP_LPL_Service.CollateralDataType>  ColReqList = new list<ECCP_LPL_Service.CollateralDataType>();
    ECCP_LPL_Service.CollateralDataType ColReq = new ECCP_LPL_Service.CollateralDataType();
    ColReq.UCCState ='CA';
    ColReq.GeneralCollateralType = 'Gen Collateral';
    ColReqList.add(ColReq);
    

    /*list<ECCP_LPL_Service.CollateralGuarantorInformationType> ColGReqList = new  list<ECCP_LPL_Service.CollateralGuarantorInformationType>();
    ECCP_LPL_Service.CollateralGuarantorInformationType ColGReq = new ECCP_LPL_Service.CollateralGuarantorInformationType();
    ColGReq.CGOrganizationType = 'LLP';
    ColGReq.CGLastname = 'ColGReq';
    ColGReqList.add(ColGReq);
    */
    

    list<ECCP_LPL_Service.FeeInfoType> FeeReqList = new list<ECCP_LPL_Service.FeeInfoType>();
    ECCP_LPL_Service.FeeInfoType FeeReq = new ECCP_LPL_Service.FeeInfoType();
    FeeReq.Feetype ='Annual';
    FeeReq.PrepaidFinanceChargeCash = 10000;
    FeeReqList.add(FeeReq);


    ECCP_LPL_Service.CreateLoanResponseType resp = new ECCP_LPL_Service.CreateLoanResponseType();
    resp = service.createLoan(GenReqList,customReq,SignerReqList,GrnReqList,ColReqList,DbsReqList,FeeReqList,BrwReqList);

    System.debug('LPL Response'+resp);
    AsyncECCP_LPL_Service.AsyncloanTransactionService async = new AsyncECCP_LPL_Service.AsyncloanTransactionService();
    Continuation con = new Continuation(60);
    async .beginCreateLoan(con,GenReqList,customReq,SignerReqList,GrnReqList,ColReqList,DbsReqList,FeeReqList,BrwReqList);      
    /*
    ECCP_LPL_ResponseData LPLResp = new ECCP_LPL_ResponseData();
    LPLResp.Status = resp.status;
    LPLResp.ErrorCode = resp.ErrorCode;
   
    return LPLResp;
    */
    return resp;
    
          }
         
    }