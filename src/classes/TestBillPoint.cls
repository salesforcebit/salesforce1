/**
 * Created By - ApExlarator
 *
 * This is the test class for BillPoint
 */
@isTest (seeAllData=false)
private class TestBillPoint {

	/**
	 * Test method for BillPoint#getSObject().
	 */
	private static testmethod void testGetSObject() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodgetSObject();

				*/
					Test.startTest();

					BillPoint obj = new BillPoint();
					obj.getSObject();

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}
	/**
	 * Test method for BillPoint#getSObjectType().
	 */
	private static testmethod void testGetSObjectType() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodgetSObjectType();

				*/
					Test.startTest();

					BillPoint obj = new BillPoint();
					obj.getSObjectType();

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}
	/**
	 * Test method for BillPoint#getType().
	 */
	private static testmethod void testGetType() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodgetType();

				*/
					Test.startTest();

					BillPoint obj = new BillPoint();
					obj.getType();

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}
	/**
	 * Test method for BillPoint#mapFromDb().
	 */
	private static testmethod void testMapFromDb() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodmapFromDb();

				*/
					Test.startTest();

					BillPoint obj = new BillPoint();
					obj.mapFromDb(null);

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}
	/**
	 * Test method for BillPoint#mapToDb().
	 */
	private static testmethod void testMapToDb() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodmapToDb();

				*/
					Test.startTest();

					BillPoint obj = new BillPoint();
					obj.mapToDb(null);

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}
	/**
	 * Test method for BillPoint#buildFromDB().
	 */
	private static testmethod void testBuildFromDB() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodbuildFromDB();

				*/
					Test.startTest();

					BillPoint.buildFromDB(null);

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}

}