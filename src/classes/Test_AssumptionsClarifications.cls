@isTest
public class Test_AssumptionsClarifications {
	
    public static testMethod void Assumptiontest() {
		Opportunity o =	UnitTestFactory.buildTestOpportunity();
		insert o;
	    LLC_BI__Scenario__c  s = UnitTestFactory.buildTestScenario(o);
	    insert s;
    	
    	//create a Assumptions and clarification record
    	Assumptions_and_Clarifications__c aac = UnitTestFactory.buildTestAssumptionsAndClarifications(s.id);

    	Test.startTest();
    		insert aac;	
    	Test.stopTest();

    	List<Scenario_Assumptions__c> saList = [Select id, name, scenario__c, Assumptions_and_Clarifications__c from Scenario_Assumptions__c where Assumptions_and_Clarifications__c = :aac.id];

    	//confirm that the assumption name matches the assumption and clarification name.
    	System.assertEquals(aac.name, saList[0].name);

    	//confirm they both have the same scenario
    	System.assertEquals(aac.Scenario__c, saList[0].Scenario__c);
    } 
}