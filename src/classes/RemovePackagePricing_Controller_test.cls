@isTest
Private with sharing class RemovePackagePricing_Controller_test {
    
    
    @isTest static void testmethod1(){
        Id exceptionPricingQ;
        exceptionPricingQ = [SELECT id FROM Group WHERE DeveloperName = 'Exception_Pricing' AND Type='Queue' ].id;
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'TestOppty';
        opp.AccountId = acc.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Pursue';
        opp.Opportunity_Type__c = 'Addl Product';
        insert opp;
        
        Id scenarioRecordTypeId = Schema.SObjectType.LLC_BI__Scenario__c.getRecordTypeInfosByName().get('Relationship Proforma').getRecordTypeId();
        
        LLC_BI__Scenario__c scenario = new LLC_BI__Scenario__c(recordTypeId=scenarioRecordTypeId, name='ScenarioOppProdStatus1', LLC_BI__Opportunity__c=opp.Id,
                                                               Deposit_Balance__c=100.00, ServiceCharges_Eligible_4_EarningsCredit__c=100);
        insert scenario;
        LLC_BI__Bill_Point__c lBBP = new LLC_BI__Bill_Point__c();
        lBBP.Name ='test';
        lBBP.Package_Billing_Element__c = true;
        insert lBBP;
        
        List<LLC_BI__Scenario_Item__c> scitlst=New List<LLC_BI__Scenario_Item__c>();
        LLC_BI__Scenario_Item__c sci1 = new LLC_BI__Scenario_Item__c(Name = 'test sci1',LLC_BI__Product__c = 'test pd type1',LLC_BI__Scenario__c = scenario.id,LLC_BI__Volumes__c =10,LLC_BI__Bill_Point__c = lBBP.id);
        scitlst.add(sci1);
        LLC_BI__Scenario_Item__c sci2 = new LLC_BI__Scenario_Item__c(Name = 'test sci2',LLC_BI__Product__c = 'test pd type2',LLC_BI__Scenario__c = scenario.id,LLC_BI__Volumes__c =10,LLC_BI__Bill_Point__c = lBBP.id);
        scitlst.add(sci2);
        LLC_BI__Scenario_Item__c sci3 = new LLC_BI__Scenario_Item__c(Name = 'test sci3',LLC_BI__Product__c = 'test pd type3',LLC_BI__Scenario__c = scenario.id,LLC_BI__Volumes__c =10,LLC_BI__Bill_Point__c = lBBP.id);
        scitlst.add(sci3);
        insert scitlst;
        LLC_BI__Scenario_Item__c lbs=[select id,name,Scenario_Item_Type__c,LLC_BI__Bill_Point__r.name,Package_Pricing__c,LLC_BI__Scenario__c
                                      from LLC_BI__Scenario_Item__c limit 1];
        System.assertEquals(true, scitlst.size()>0);
        List<TSO_Setup_Task__c> stlst=New List<TSO_Setup_Task__c>();
        Boolean Sucessfullydeleted=false;
        if(scitlst.size()>0){
            Sucessfullydeleted=True;
            for(LLC_BI__Scenario_Item__c sc:scitlst){
                stlst.add(New TSO_Setup_Task__c(Opportunity_Lookup__c=opp.Id,Name = 'Remove Package Pricing1',Status__c = 'New',OwnerId = exceptionPricingQ,RPM_Task__c = true,Description__c = 'A request has been submitted to remove package pricing'));
            }
            
        }
        
        System.assertEquals(true, stlst.size()>0);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        RemovePackagePricing_Controller scwrap= New RemovePackagePricing_Controller(sc);
        
        RemovePackagePricing_Controller.scenarioItemWrapper rs=New RemovePackagePricing_Controller.scenarioItemWrapper(lbs);
        rs.isCheck=true; 
        rs.scenitem=scitlst[0];
        List< RemovePackagePricing_Controller.scenarioItemWrapper> scenarioItemList = new List< RemovePackagePricing_Controller.scenarioItemWrapper>();
        scenarioItemList.add(rs);
        scwrap.opp = opp;
        scwrap.scenarioItemList.addAll(scenarioItemList);
        scwrap.removeItem();
     }
    
    @isTest static void testmethod2(){
        Id exceptionPricingQ;
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'TestOppty';
        opp.AccountId = acc.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Pursue';
        opp.Opportunity_Type__c = 'Addl Product';
        insert opp;
        
        Id scenarioRecordTypeId = Schema.SObjectType.LLC_BI__Scenario__c.getRecordTypeInfosByName().get('Relationship Proforma').getRecordTypeId();
        
        LLC_BI__Scenario__c scenario = new LLC_BI__Scenario__c(recordTypeId=scenarioRecordTypeId, name='ScenarioOppProdStatus1', LLC_BI__Opportunity__c=opp.Id,
                                                               Deposit_Balance__c=100.00, ServiceCharges_Eligible_4_EarningsCredit__c=100);
        insert scenario;
        LLC_BI__Bill_Point__c lBBP = new LLC_BI__Bill_Point__c();
        lBBP.Name ='test';
        lBBP.Package_Billing_Element__c = FALSE;
        insert lBBP;
        
        List<LLC_BI__Scenario_Item__c> scitlst=New List<LLC_BI__Scenario_Item__c>();
        LLC_BI__Scenario_Item__c sci1 = new LLC_BI__Scenario_Item__c(Name = 'test sci1',LLC_BI__Product__c = 'test pd type1',LLC_BI__Scenario__c = scenario.id,LLC_BI__Volumes__c =10,LLC_BI__Bill_Point__c = lBBP.id);
        scitlst.add(sci1);
        LLC_BI__Scenario_Item__c sci2 = new LLC_BI__Scenario_Item__c(Name = 'test sci2',LLC_BI__Product__c = 'test pd type2',LLC_BI__Scenario__c = scenario.id,LLC_BI__Volumes__c =10,LLC_BI__Bill_Point__c = lBBP.id);
        scitlst.add(sci2);
        LLC_BI__Scenario_Item__c sci3 = new LLC_BI__Scenario_Item__c(Name = 'test sci3',LLC_BI__Product__c = 'test pd type3',LLC_BI__Scenario__c = scenario.id,LLC_BI__Volumes__c =10,LLC_BI__Bill_Point__c = lBBP.id);
        scitlst.add(sci3);
        insert scitlst;
        LLC_BI__Scenario_Item__c lbs=[select id,name,Scenario_Item_Type__c,LLC_BI__Bill_Point__r.name,Package_Pricing__c,LLC_BI__Scenario__c
                                      from LLC_BI__Scenario_Item__c limit 1];
        System.assertEquals(true, scitlst.size()>0);
        List<TSO_Setup_Task__c> stlst=New List<TSO_Setup_Task__c>();
        Boolean Sucessfullydeleted=false;
        if(scitlst.size()>0){
            Sucessfullydeleted=True;
            for(LLC_BI__Scenario_Item__c sc:scitlst){
                stlst.add(New TSO_Setup_Task__c(Opportunity_Lookup__c=opp.Id,Name = 'Remove Package Pricing1',Status__c = 'New',OwnerId = exceptionPricingQ,RPM_Task__c = true,Description__c = 'A request has been submitted to remove package pricing'));
            }
            
        }
        
        System.assertEquals(true, stlst.size()>0);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        RemovePackagePricing_Controller scwrap= New RemovePackagePricing_Controller(sc);
    }
}