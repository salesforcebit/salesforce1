public with sharing class AuthorizedUser_VFC {

	public LLC_BI__Authorized_User__c user {get; set;}
	public ApexPages.StandardController controller {get;set;}
	public Id test {get;set;}
	public String password {get;set;}
	public String email {get;set;}

	public String username {get; set;}
	public PasswordGenerator pg;
	public Contact c {get;set;}


	public AuthorizedUser_VFC(ApexPages.StandardController stdController) {
	  pg = new PasswordGenerator();
		user = (LLC_BI__Authorized_User__c) stdController.getRecord();

		if(user.id != null){
			user = [select id, GTM_User__c,GTM_User1__c , User_Password__c, Select_Access_Day_s__c,Access_Start_Time_EDT__c ,Access_End_Time_EDT__c ,KTT_User_Name__c,LLC_BI__Contact_Reference__c, organization_Id__c, KTT_Company_Contact__c, Model_this_user_setup_after_User_Name__c, Model_this_user_setup_after_User_ID__c,Remove_User_from_GTM__c, User_ID_Sent_to_EAP__c, User_Password_Sent_to_EAP__c, Operations_Business_Unit_Contact__c, Accounts_Receivable_Contact__c, Business_Analyst__c, Card_Program_Contact__c, On_site_Contact__c, X1st_Authorized_User__c, X2nd_Authorized_User__c, Accounts_Payable_Contact__c, Returns_Daytime_Contact__c, Returns_Nighttime_Contact__c, Systems_Daytime_Contact__c, Systems_Nighttime_Contact__c, Site__c, Company_Contact__c, Contact_Name__c, Title__c, Email_Address__c, Mobile__c, Contact_Phone__c, Contact_Phone_Extension__c, Contact_Address__c, Contact_City__c, Contact_State__c, Contact_Zip_Code1__c, Product_Manager__c, Post_Production_Contact__c, Returns_Primary_Contact__c, Returns_Secondary_Contact__c, Systems_Primary_Contact__c, Systems_Secondary_Contact__c, LLC_BI__Treasury_Service__c ,nCino_EAP_Request__c ,nCino_EAP_Request_Status__c from LLC_BI__Authorized_User__c where id = :user.id];
			if(user.User_Password__c != null){
				password = pg.decryptPassword(user.User_Password__c);
			}
		}

		if(user.LLC_BI__Contact_Reference__c != null){
			c = [select id, user_name__c, Contact_Email__c from Contact where id = :user.LLC_BI__Contact_Reference__c];
			if(c.Contact_Email__c != null){
				email = c.Contact_Email__c;
			}
			if(c.user_name__c != null){
				username = c.user_name__c;
			}
		}
        if(c.user_name__c != null) {
            user.GTM_User1__c = true;
        }
        
		controller = stdController;
	}

	public PageReference getUserId() {
		c = [select id, user_name__c, Contact_Email__c from Contact where id = :user.LLC_BI__Contact_Reference__c];
		if(c.Contact_Email__c != null){
			email = c.Contact_Email__c;
		}
		if(c.user_name__c != null){
			username = c.user_name__c;
		}
		return null;
	}

	public PageReference saveNew() {

		try{
			upsert user;
  	} catch(System.DMLException e) {
    	ApexPages.addMessages(e);
    	return null;
  	}

    string s = '/' + ('' + user.get('Id')).subString(0, 3) + '/e?';
    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Info, s));
    return new Pagereference(s);

	}

    public PageReference saveRecord(){
        try{
			upsert user;
  	} catch(System.DMLException e) {
    	ApexPages.addMessages(e);
    	return null;
  	}
        return new Pagereference('/' + user.Id);
    }

	public PageReference generatePassword(){
		password = pg.createPassword();

		//save the password to the contact object as encrypted
		user.User_Password__c = pg.encryptPassword(password);
		return null;
	}
}