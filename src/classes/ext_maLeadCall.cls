//=================================================================================================
//  KeyBank
//  Object: ext_maLeadCall
//  Author: Offshore
//  Detail: Controller (New Page) for Visualforce page - maLeadCall
//=================================================================================================
//          Date            Purpose
//        04/15/2015      Initial Version
//        08/28/2015      October Release - FX Requirement
//=================================================================================================

public with sharing class ext_maLeadCall {
    
    //Variables
    Lead ld;
    public String UserID {get; set;}
    public String searchString{get; set;}
    public Participant__c[] shoppingCart{get; set;}
    public Contact[] AvailableParticipants {get;set;}
    public String tonSelect{get; set;}
    public String toUnselect{get; set;}
    private Participant__c[] forDeletion = new Participant__c[]{};
    
    //Containers
    public list<AggregateResult> prodFamilies;
    public list<AggregateResult> prodCats;    
    public set<String> pFams;
        
    //Getters and Setters
        //Information Block
        public Date callDate {get;set;}
        public String callSubject {get;set;}
        public Boolean cPrivate {get;set;}
        public String cStatus {get;set;}
        public String cType {get;set;}
        public String cComments {get;set;}
        public String cCallLocation {get;set;}
        
        //Additional Information Block
        public Boolean cJointCall {get;set;}
        public String cProductFamily {get;set;}
        public String cProductCategory {get;set;}
        public String cProduct {get;set;}
        public string warningRACF{get;set;} //Warning Racf message variable SS(6/10/15)
        
       
    //Constructor
    public ext_maLeadCall (ApexPages.StandardController stdController) {
        
        //Populate Account
        this.ld = (Lead)stdController.getRecord();
        
        //Distinct Product Category
        prodFamilies = [select Family from Product2 group by Family order by Family asc];
        
        //Search and display in Contact section
         {shoppingCart = [Select id,name,CallId__c,Contact_Id__c,Call_Date__c,Participant__c.Contact_Id__r.Name from participant__c where id =: tonSelect ];}
 
        updateAvailableList();   
        
        //Populate Call Variable Custom Setting & get warning message SS(6/12/15)
        
        CallVF__c cVFcs = CallVF__c.getInstance(keyGV.vfCallNew);
        warningRACF = cVFcs.Warning_RACFID__c;          
        
    }

    //Create Call
    public pagereference createCall(){
        return null;
    }
    
    //Populate Call Status
    public List<selectOption> cStatusList {
        get{
            List<selectOption> options = new List<selectOption>();
            options.add(new SelectOption('DONE', 'DONE' ));
            for(callSelect_Status__c c : [select status__c from callSelect_Status__c])
                options.add(new SelectOption(c.Status__c, c.Status__c));
            return options;
        }
    }
    
    //Populate Call Location - For FX Requirement Picklist 
    public List<selectOption> cCallLocationList {
        get{
            List<selectOption> options = new List<selectOption>();
            options.add(new SelectOption('', '-- Choose a Call Location --' ));
            for(callSelect_Call_Location__c cl : [select Call_Location__c from callSelect_Call_Location__c])
                options.add(new SelectOption(cl.Call_Location__c, cl.Call_Location__c));
            return options;
        }
    }
    
    //Populate Call Type
    public List<selectOption> cTypeList {
        get{
            List<selectOption> options = new List<selectOption>();
            options.add(new SelectOption('', '-- Choose a Call Type --' ));
            for(callSelect_Type__c cs : [select Type__c from callSelect_Type__c])
                options.add(new SelectOption(cs.Type__c, cs.Type__c));
            return options;
        }
    }
    
    //Populate Product Family 
    public List<selectOption> pFamilyList {
        
        //Get distinct Product Families
        get {
            List<selectOption> options = new List<selectOption>();

                options.add(new SelectOption('','--NONE--'));
                for (AggregateResult prod : prodFamilies)
                    options.add(new SelectOption(String.valueOf(prod.get('Family')),String.valueOf(prod.get('Family'))));
            return options;           
        }
        set;
    }
    
    //Populate Product Category
    public List<selectOption> pCategoryList {
        get {
            prodCats = [select Product_Category__c from Product2 where family =: cProductFamily and isactive=true group by Product_Category__c order by Product_Category__c asc];
            List<selectOption> options = new List<selectOption>();
                options.add(new SelectOption('','--NONE--'));
                for (AggregateResult pc : prodCats)
                    options.add(new SelectOption(String.valueOf(pc.get('Product_Category__c')),String.valueOf(pc.get('Product_Category__c'))));
            return options;           
        }
        set;
    }   
    
    //Populate Products
    public List<selectOption> ProductList {
        get {
            List<selectOption> options = new List<selectOption>();

                options.add(new SelectOption('','--NONE--'));
                for (Product2 p2 : [select name from Product2 where Product_Category__c =: cProductCategory and isactive=true])
                    options.add(new SelectOption(p2.name, p2.name));
            return options;           
        }
        set;
    } 
    
    
    //Offshore Shopping Cart
    public void updateAvailableList() {
      
        UserID = UserInfo.getUserId(); 
        String qString =  'select id, Name,RACFID__c,Contact_Type__c, Title, Contact.MailingCity,Contact.MailingState,Contact.Account.Name from Contact where User_Id__c not in (select ID from User where id =: UserID )' ;
        system.debug(qString);
        
        if(searchString!=null) {          
            qString+= ' and ( Contact.Name like \'%' + searchString + '%\' or Contact.RACFID__c like \'%' + searchString + '%\' or Contact.Officer_Code__c like \'%' + searchString + '%\') ';                       
        }
        
       Set<Id> selectedEntries = new Set<Id>();
       if(tonSelect!=null)
        for(Participant__c d : shoppingCart){
            selectedEntries.add(d.Contact_Id__c);
        }
        
        if(selectedEntries.size()>0){
            String tempFilter = ' and id not in (';
            for(id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        } 
        
        qString+= ' order by Name';
        qString+= ' limit 12';
        system.debug('qString:' +qString );
        if(searchString != null){               
            AvailableParticipants = database.query(qString);
            system.debug(AvailableParticipants);
        }
    } 
    
    // This function runs when a user hits "select" button next to a Participant
    public void addToShoppingCart() { 
        for(Contact part : AvailableParticipants){
            if(((String)part.id==tonSelect && part.RACFID__c != null) || ((String)part.id==tonSelect && part.Contact_Type__c == 'Business Contact'))
            {
                shoppingCart.add(new Participant__c (Contact_Id__c =part.id));
                system.debug(shoppingCart);
                system.debug('Participant select size' + shoppingCart.size());
                break;
            }
             if((String)part.id==tonSelect && part.RACFID__c == null && part.Contact_Type__c == 'Key Employee')
            {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF)); 
                //ApexPages.addMessage(errMsg);
                system.debug('error loop');
            }
        }    
        updateAvailableList();  
    } 
    
    public PageReference removeFromShoppingCart(){
    
        // This function runs when a user hits "remove" on "Selected Participant" section
    
        Integer count = 0;
    
        for(Participant__c del : shoppingCart){
            if((String)del.Contact_Id__c==toUnselect){
            
                if(del.Id!=null)
                    forDeletion.add(del);
            
                shoppingCart.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }            

    public PageReference Save(){
        
        //Id Variable
        Id newCallId;
        //Get RecordType
        Id recordTypeId = keyGlobalUtilities.getRecordTypeId('Call__c', keyGV.callRTLead);
        
        //Build Call Record
        Call__c newCall = New Call__c();
            newCall.LeadId__c = ld.Id;
            newCall.RecordTypeId = recordTypeId;
            newCall.Subject__c = callSubject;
            newCall.Call_Date__c = callDate;
            newCall.Is_Private__c = cPrivate;
            newCall.Status__c = cStatus;
            newCall.Call_Type__c = cType;
            newCall.Joint_Call__c = cJointCall;
            newCall.Call_Location__c = cCallLocation;
            
            if(!String.isBlank(cComments)){
                newCall.Comments__c = cComments;
            }
            if(!String.isBlank(cProductFamily)){
                newCall.Product_Family__c = cProductFamily;
            }
            if(!String.isBlank(cProductCategory)){
                newCall.Product_Category__c = cProductCategory;
            }
            if(!String.isBlank(cProduct)){
                newCall.Product__c = cProduct;
            }       
                                        
        //Insert Call
        try{
            insert newCall;
            newCallId = newCall.Id;
        }   
        catch (Exception ex) {
            ApexPages.addMessages(ex);
            System.debug('Error inserting Call Record - '+ex.getMessage());
            return null;
        }

        
        //Build Participants Record
        list<Participant__c> newParticipants = new list<Participant__c>();
        if(shoppingCart.size()>0){
            for(Participant__c ppp : shoppingCart){
                Participant__c nPartic = new Participant__c();
                    nPartic.CallId__c = newCallId;
                    nPartic.Contact_Id__c = ppp.Contact_Id__c;
                newParticipants.add(nPartic);
            }
        }
                                
        //Save Participants
        if(newParticipants.size()>0){
            try{
                insert newParticipants;
            }
            catch(Exception ex) {
                ApexPages.addMessages(ex);
                System.debug('Error inserting Participant Record - '+ex.getMessage());
                return null;
            }
                
        }

        //Redirect to New Call
        return new PageReference('/' + newCallId);  
    } 
}