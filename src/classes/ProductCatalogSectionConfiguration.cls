/**
 * Provides a strongly-typed intermediate object for converting the JSON representation
 * of a Product Catalog Section Configuration to its sObject equivalent.
 */
public class ProductCatalogSectionConfiguration extends nFORCE.AForce {
	public String sid;
	public String name;
	public String sObjectType;
	public String fieldSet;
	public String layout;
	public Decimal displayOrder;
	public String productCatalogRouteTemplate;

	public ProductCatalogSectionConfiguration() {}

	public ProductCatalogSectionConfiguration(String sid, String name, String sObjectType,
		String fieldSet, String layout, Decimal displayOrder) {
		this.sid = sid;
		this.name = name;
		this.sObjectType = sObjectType;
		this.fieldSet = fieldSet;
		this.layout = layout;
		this.displayOrder = displayOrder;
	}

	public Product_Catalog_Section_Configuration__c getSObject() {
		Product_Catalog_Section_Configuration__c dbSectionConfiguration
			= new Product_Catalog_Section_Configuration__c();
		this.mapToDb(dbSectionConfiguration);
		return dbSectionConfiguration;
	}

	public static ProductCatalogSectionConfiguration buildFromDB(sObject obj) {
		ProductCatalogSectionConfiguration sectionConfiguration
			= new ProductCatalogSectionConfiguration();
		sectionConfiguration.mapFromDB(obj);
		return sectionConfiguration;
	}

	public override Type getType() {
		return ProductCatalogSectionConfiguration.class;
	}

	public override Schema.SObjectType getSObjectType() {
		return Product_Catalog_Section_Configuration__c.sObjectType;
	}

	public override void mapFromDb(sObject obj) {
		Product_Catalog_Section_Configuration__c sectionConfiguration
			= (Product_Catalog_Section_Configuration__c)obj;
		if (sectionConfiguration != null) {
			this.sid = sectionConfiguration.ID;
			this.name = sectionConfiguration.Name;
			this.sObjectType = sectionConfiguration.SObject_Type__c;
			this.fieldSet = sectionConfiguration.Field_Set__c;
			this.layout = sectionConfiguration.Layout__c;
			this.displayOrder = sectionConfiguration.Display_Order__c;
			this.productCatalogRouteTemplate = sectionConfiguration.Product_Catalog_Route_Template__c;
		}
	}

	public override void mapToDb(sObject obj) {
		Product_Catalog_Section_Configuration__c sectionConfiguration =
			(Product_Catalog_Section_Configuration__c)obj;
		if (sectionConfiguration != null) {
			if (this.sid != null && this.sid.length() > 0) {
				sectionConfiguration.Id = Id.valueOf(this.sid);
			}
			sectionConfiguration.Name = this.name;
			sectionConfiguration.SObject_Type__c = this.sObjectType;
			sectionConfiguration.Field_Set__c = this.fieldSet;
			sectionConfiguration.Layout__c = this.layout;
			sectionConfiguration.Display_Order__c = this.displayOrder;
		}
	}
}