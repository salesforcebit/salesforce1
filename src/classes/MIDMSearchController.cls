public with sharing class MIDMSearchController {
    
    public list<PPRelatedrecords> Pprlist{get;set;}
    public Id productpackageid{get;set;}
    Public boolean checked {get;set;}
    public String jsonstring{get;set;}
    public string MatchingScore{get;set;}
    public List<ECC_Exposure_Entity__c> expEntityList = new List<ECC_Exposure_Entity__c>(); 
    public List<LLC_BI__Legal_Entities__c> legalEntityList = new List<LLC_BI__Legal_Entities__c>();
    public map<string,string> expEntityMap = new map<string,string>();
    public MIDMSearchController(ApexPages.StandardController controller) {
           checked= true;
        
           productpackageid = controller.getId();
           system.debug('productpackageid '+productpackageid);
           Pprlist = new list<PPRelatedrecords>();
           if(productpackageid!=null){
               for(LLC_BI__Legal_Entities__c ent:[select id,LLC_BI__Borrower_Type__c ,LLC_BI__Account__r.ECC_Incorporation_State__c, LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c,
                                                  LLC_BI__Account__r.birthdate__c,LLC_BI__Account__r.Name,LLC_BI__Account__r.FNFG_MDM_ID__c from LLC_BI__Legal_Entities__c
                                                  where LLC_BI__Product_Package__c =:productpackageid]){
                    legalEntityList.add(ent);                                  
                                                 
                  /* if(checked == true){
                       Pprlist.add(new PPRelatedrecords(true,ent));
                   }
                   else {
                       Pprlist.add(new PPRelatedrecords(false,ent));
                   }*/
               }
              for(ECC_Exposure_Entity__c expEnt:[select id,MDMID__c,Entity_Involvement_Key__c from ECC_Exposure_Entity__c
                                                 where Product_Package_Key__c =:productpackageid]) 
              	{
                     expEntityMap.put(expEnt.Entity_Involvement_Key__c,expEnt.id);                                
                }
              getWrapperList();  
           }
    }
    
    Public List<PPRelatedrecords> getWrapperList(){
        checked=true;
           Pprlist.clear();
           if(productpackageid!=null){
               /*for(LLC_BI__Legal_Entities__c ent:[select id,LLC_BI__Borrower_Type__c ,LLC_BI__Account__r.ECC_Incorporation_State__c, LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c,
                                                  LLC_BI__Account__r.birthdate__c,LLC_BI__Account__r.Name,LLC_BI__Account__r.FNFG_MDM_ID__c from LLC_BI__Legal_Entities__c where LLC_BI__Product_Package__c =:productpackageid])*/
                 for(ECC_Exposure_Entity__c expEnt:[select id,MDMID__c,Entity_Involvement_Key__c,Product_Package_Key__c,Entity_Involvement_Key__r.LLC_BI__Borrower_Type__c ,
                                                    Entity_Involvement_Key__r.LLC_BI__Account__r.ECC_Incorporation_State__c, Entity_Involvement_Key__r.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c,
                                                  Entity_Involvement_Key__r.LLC_BI__Account__r.birthdate__c,Entity_Involvement_Key__r.LLC_BI__Account__r.Name,Entity_Involvement_Key__r.LLC_BI__Account__r.FNFG_MDM_ID__c from ECC_Exposure_Entity__c
                                                  where Product_Package_Key__c =:productpackageid])                                 
               {
                                                  
                   Pprlist.add(new PPRelatedrecords(true,expEnt));
                  

               }
           }
           return Pprlist;
           
    }
    
   public pagereference processMDMRequest(){
         checked = true;
      
        /* jsonstring= '[{"Name":"Smoke_Test_2.0_ACC_RR","BorrowerType":"Guarantor","State":"OH","DOB":"2016-01-01","SSN":"12345","Gender":"Male","MDMID":"303030","Matchingscore":"90%"},{"Name":"Smoke_Test_2.0_ACC","BorrowerType":"Borrower","State":"OH","DOB":"2015-11-27","SSN":"975312467","Gender":"Male","MDMID":"202020","Matchingscore":"100%"},{"Name":"Smoke_Test_2.0_ACC_RR","State":"AP","BorrowerType":"Guarantor","DOB":"2015-01-01","SSN":"12345","Gender":"Male","MDMID":"101010","Matchingscore":"70%"},{"Name":"Test","State":"OH","DOB":"1988-02-02","SSN":"78954","Gender":"Male","MDMID":"50050","Matchingscore":"100%","BorrowerType":"Borrower"}]';
         List<PPRelatedrecords> deserializeddata =(List<PPRelatedrecords>)JSON.deserialize(jsonstring, List<PPRelatedrecords>.class);
         system.debug(deserializeddata.size());
         if(deserializeddata.size() > 0){   
             Pprlist = new list<PPRelatedrecords>();          
             Pprlist = deserializeddata;
             system.debug(Pprlist); 
         }*/
          Pprlist = new list<PPRelatedrecords>();
         for(ECC_Exposure_Demo__c demo : [select id,Name,Gender__c,Date_of_birth__c,Matching_Score__c,MDMID__c,Relationship_Type__c,State_Code__c,TID__c from ECC_Exposure_Demo__c ORDER BY Matching_Score__c DESC])
         {
            PPRelatedrecords obj = new PPRelatedrecords();
                  obj.State = demo.State_Code__c;
                  obj.MDMID = demo.MDMID__c;
                  obj.BorrowerType =  demo.Relationship_Type__c;
                  obj.SSN = demo.TID__c;
                  obj.DOB= string.valueof(demo.Date_of_birth__c);
                  obj.Name = demo.Name;
                  //entities = ent;
                  obj.gender = demo.Gender__c;
                  obj.MatchingScore = string.valueof(demo.Matching_Score__c); 
             Pprlist.add(obj);
         }   
         MatchingScore = 'Matching Score';
         return null;
    }
    public void selectallnone(){
            if(checked) checked = true;
            else  checked = false;   
            getWrapperList();
    }
    public Pagereference showExposure(){
           Pagereference pr = new Pagereference('/apex/preload_poc?id='+productpackageid );
           pr.setredirect(true);
           return pr;
    }
    public class PPRelatedrecords{
           
           public boolean selectedrec{get;set;}
           public string recId{get;set;}
           public string  Gender{get;set;}
           public string  MDMID{get;set;}
           public string  BorrowerType{get;set;}
           public string  Name{get;set;}
           public string  SSN{get;set;}
           public string  DOB{get;set;}
           public string  Matchingscore{get;set;}
           public string  State{get;set;}
           
          public LLC_BI__Legal_Entities__c entities {get;set;}
          // public ECC_Exposure_Entity__c expEntity {get;set;}
          public PPRelatedrecords(){}
           public PPRelatedrecords(boolean check,ECC_Exposure_Entity__c ent){
                  system.debug('check '+check);
                  selectedrec = true;                  
                  recId = ent.id; 
                  State = ent.Entity_Involvement_Key__r.LLC_BI__Account__r.ECC_Incorporation_State__c;
                  MDMID= ent.Entity_Involvement_Key__r.LLC_BI__Account__r.FNFG_MDM_ID__c;
                  BorrowerType =  ent.Entity_Involvement_Key__r.LLC_BI__Borrower_Type__c;
                  SSN = ent.Entity_Involvement_Key__r.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c;
                  DOB= string.valueof(ent.Entity_Involvement_Key__r.LLC_BI__Account__r.birthdate__c);
                  Name = ent.Entity_Involvement_Key__r.LLC_BI__Account__r.Name;
                  //entities = ent;
                  gender = 'Male';
           }
    }
     public void createExpEntityRecords()
     {
         if(legalEntityList.size()>0)
         {
            for(LLC_BI__Legal_Entities__c ent : legalEntityList)
            { 
                if(!expEntityMap.containsKey(ent.id))
                {   
                    ECC_Exposure_Entity__c obj = new ECC_Exposure_Entity__c();
                    obj.Entity_Involvement_Key__c = ent.id;
                    obj.Product_Package_Key__c = productpackageid; 
                    expEntityList.add(obj);
                }    
            } 
         }
         insert expEntityList;
         getWrapperList();
     }   
    //Last page data displayed
    public list<Exposure_Line_Item_Demo__c> exposLitemD;
    

}