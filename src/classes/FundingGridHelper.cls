//================================================================================
//  Added for KRR Release 3 - 2 November 2015
//  Object: Funding Grid Helper
//  Description : Helper class for Funding Grid Object.
//================================================================================
public with sharing class FundingGridHelper 
{   
    //this method updatse parent Account of fundig grid with populating Default ones Id in parent Account's lookup field.
    public static  Map<id, String> setUnsetDefaultfundingMethod(List<Funding_Grid__c> newFGList, Map<Id, Funding_Grid__c> oldFGMap,
    															Map<id,Id> accFGMap )
    {
    	Map<id, String> errorMessageMap = new Map<id, String>();
    	if(newFGList != null && newFGList.size() > 0 && oldFGMap != null && accFGMap != null)
    	{
		    Map<Id, Account> dpmAccountMap = new Map<Id, Account>();
		    //this map will store account id as KEY and set of Funding Grid ids as VALUE. 
		    //this map will store ids of those funding grids for which the checkbox is unchecked. 
		    Map<Id, Set<Id>> accFGListMap = new Map<Id, Set<id>>();
		    
		    for(Funding_Grid__c newFG : newFGList)
		    {
		    	Funding_Grid__c oldFG = oldFGMap.get(newFG.id);		    	
		    	
		    	Account accTemp = new Account();
		    	accTemp.id = newFG.Account__c;
		    	//if the funding grid is marked as default
				if(newFG.Default_payment_Method__c)
				{    				
					if(dpmAccountMap.containsKey(newFG.Account__c))
					{
						accTemp = dpmAccountMap.get(newFG.Account__c);    					 
					}
					//populate lookup field on Account with Default Fuding Grid's Id 
					accTemp.Default_Payment_Method__c = newFG.id;					
					dpmAccountMap.put(accTemp.Id, accTemp);    				
				}
				/* <<<<<<<<<< START - Below code block is commented for defect 264  - 14 December 2015>>>>>>>>>>>>>>>>>>>>>> */
				/*
				else if(!newFG.Default_payment_Method__c)
				{
					if(oldFG == null ||
					//if the old and new value of the field appears to be same, that means the 'Default Payment Method' is unchecked 
					//on record via end user or data user.
					//this field will be changed when updating record via code (backend)
					(oldFG != null && newFG.Funding_Grid_Default_Update_date__c == oldFG.Funding_Grid_Default_Update_date__c))
					{
						//the scenario is when user is manually unchecking the 'Default Payment Method' checkbox on Funding Grid record
						//as there should be one present for an Account, an error needs to be dispalyed.
						
						//check if the account id is already present for updating with new default one 
						if(dpmAccountMap.containsKey(newFG.Account__c) && accFGListMap.containsKey(newFG.Account__c))
						{
							//default payment present for this account, remove the from the error related map
							accFGListMap.remove(newFG.Account__c);							
						}
						//the funding grid's account is not present in map with Default Primary Funding Grid, add to map for showing error
						else if(!dpmAccountMap.containsKey(newFG.Account__c))
						{
							Set<id> fgIds = new set<Id>();
							if(accFGListMap != null && accFGListMap.containsKey(newFG.Account__c))
							{
								fgIds = accFGListMap.get(newFG.Account__c);
							}
							fgIds.add(newFG.id);
							accFGListMap.put(newFG.Account__c, fgIds);
						}
					}    	
				}*/
				/* <<<<<<<<<< END - Code block is commented for defect 264 - 14 December 2015>>>>>>>>>>>>>>>>>>>>>> */
		    }
		    
		    if(dpmAccountMap != null && !dpmAccountMap.isEmpty())
            {
                //dmlResults = Database.update(fgList, false);
                List<Account> accToUpdate = dpmAccountMap.values();
                List<Database.UpsertResult> dmlResults = Database.upsert(accToUpdate, false);            
            	Map<id, Account> accErrorMap = new Map<id, Account>();
            	accErrorMap = dpmAccountMap.deepClone();
	            for(Integer temp = 0; temp < dmlResults.size(); temp ++)
	            {                 
	                system.debug('---dmlResults[temp] '+dmlResults[temp]);
	                system.debug('---accToUpdate[temp] '+accToUpdate.size());
	                system.debug('---temp '+temp); 
	                if(!dmlResults[temp].isSuccess() && accToUpdate.size() >= temp)
	                {             
	                	Database.Error error = dmlResults.get(temp).getErrors( ).get(0);
		                String failedDML = error.getMessage();
		                String errorMsg = System.Label.Account_Update_Error_Message +  ' ' + (failedDML != null ? failedDML : '');
		                errorMessageMap.put(accFGMap.get(accToUpdate[temp].id),errorMsg);    
					}
	            }	            
            }	
            
            /* <<<<<<<<<< START - Below code block is commented for defect 264 - 14 December 2015>>>>>>>>>>>>>>>>>>>>>> */
            /*
            //the funding grid ids listed in this map are the ones which are unchecked as Default Payment method and the parent account
            //does not have any other default one, hence show the error message for such funding grids. 
            if(accFGListMap != null && !accFGListMap.isEmpty())
            {
            	for(Set<Id> fgIds : accFGListMap.values())
            	{
            		for(Id fgID : fgIds)
            		{
            			errorMessageMap.put(fgID, System.Label.OneDefaultPaymentMethod);
            		}
            	}
            }*/
            /* <<<<<<<<<< END - Code block is commented for defect 264 - 14 December 2015 >>>>>>>>>>>>>>>>>>>>>> */
    	}
    	return errorMessageMap;
    }
    
    public static void showErrorOnScreen(Map<id, String> errorMap, Map<Id, Funding_Grid__c> fgMap)
    {
    	if(fgMap != null && !fgMap.isEmpty() && errorMap != null && !errorMap.isEmpty())
    	{
    		for(Id fgId : errorMap.keyset())
    		{
    			if(fgMap.get(fgId) != null)
    			{
    				fgMap.get(fgId).addError(errorMap.get(fgId));    				
    			}
    		}
    	}
    }
}