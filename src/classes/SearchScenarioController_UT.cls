/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SearchScenarioController_UT {

	@testSetup
	static void createScenario()
	{
		Account acc = UnitTestFactory.buildTestAccount();
		acc.KBCM_Customer_Number__c = '98765';
		acc.Name = 'Test Account';
		insert acc;
		Id accId = acc.Id;
		System.debug(acc.Name);

		Opportunity o = UnitTestFactory.buildTestOpportunity();
		o.AccountId = acc.Id;
		insert o;

		Profile p = [select id from profile where name='System Administrator'];
		User testUser = new User(alias = 'u1', email='jayson_jonson@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2016', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharanx@keybank.com',PLOB__c='support',PLOB_Alias__c='support');
        insert testUser;

		Id oppId = o.Id;
		Id scenarioRecordTypeId = Schema.SObjectType.LLC_BI__Scenario__c.getRecordTypeInfosByName().get('DDA Proforma').getRecordTypeId();
		LLC_BI__Scenario__c scenario = new LLC_BI__Scenario__c(recordTypeId=scenarioRecordTypeId, name='ControllerUnitTest', LLC_BI__Opportunity__c=oppId,
			Account__c = accId, ECP_Payments_Manager__c = testUser.Id, Relationship_Manager_Level_1__c=testUser.Id, EscapeFlow__c = true, Prospect_Bank_Number__c='1256 - Idaho',
			Client_Banking_Region__c = 'Idaho');
		insert scenario;
		//LLC_BI__Scenario__c sc = [select id, Owner.Name from LLC_BI__Scenario__c where id = :scenario.Id];
		//System.debug('**************Scenario Owner in DB: ' + sc.Owner.Name);

	}
    static testMethod void CustomerNumberTest() {
    	Test.starttest();
		 SearchScenarioController srchcont = new SearchScenarioController();
		 srchcont.customerIPI = '98765';
		 srchcont.search();
		 List<LLC_BI__Scenario__c> scenarioList = srchcont.scenarioList;
		 test.stopTest();
		 System.assertEquals(scenarioList.size(), 1);
    }

       static testMethod void AccountNameTest() {
    	Test.starttest();
		 SearchScenarioController srchcont = new SearchScenarioController();
		// srchcont.customerIPI = '98765';
		 srchcont.clientName = 'Test Account';
	//	 srchcont.boolOperator = 'Or';
		 srchcont.search();
		 List<LLC_BI__Scenario__c> scenarioList = srchcont.scenarioList;
		 test.stopTest();
		 System.assertEquals(scenarioList.size(), 1);
    }

    static testMethod void ScenarioNameTest() {
    	Test.starttest();
		 SearchScenarioController srchcont = new SearchScenarioController();
		// srchcont.customerIPI = '98765';
		 srchcont.requestId = 'ControllerUnitTest';
	//	 srchcont.boolOperator = 'Or';
		 srchcont.search();
		 List<LLC_BI__Scenario__c> scenarioList = srchcont.scenarioList;
		 test.stopTest();
		 System.assertEquals(scenarioList.size(), 1);
    }

     static testMethod void ECPSalesManagerTest() {
     	User u = [select id, Name from User where email = 'jayson_jonson@keybank.com'];
    	Test.starttest();
		 SearchScenarioController srchcont = new SearchScenarioController();
		// srchcont.customerIPI = '98765';
		 srchcont.ECPSalesAdvisor = u.Name;
	//	 srchcont.boolOperator = 'Or';
		 srchcont.search();
		 List<LLC_BI__Scenario__c> scenarioList = srchcont.scenarioList;
		 //System.assertEquals(scenarioList.size(), 1);
		 test.stopTest();
    }

       static testMethod void RelationshipManagerTest() {
     	User u = [select id, Name from User where email = 'jayson_jonson@keybank.com'];
    	Test.starttest();
		 SearchScenarioController srchcont = new SearchScenarioController();
		// srchcont.customerIPI = '98765';
		 srchcont.relationshipManager = u.Name;
	//	 srchcont.boolOperator = 'Or';
		 srchcont.search();
		 List<LLC_BI__Scenario__c> scenarioList = srchcont.scenarioList;
		 //System.assertEquals(scenarioList.size(), 1);
		 test.stopTest();
    }

    /*    static testMethod void OwnerTest() {
     	User u = [select id, Name from User where email = 'jayson_jonson@keybank.com'];
		 SearchScenarioController srchcont = new SearchScenarioController();
	   	System.debug('***************Owner for search: '+ UserInfo.getName());
		 srchcont.requestor = UserInfo.getName();
	//	 srchcont.boolOperator = 'Or';
		 srchcont.search();
		 List<LLC_BI__Scenario__c> scenarioList = srchcont.scenarioList;
		 System.assertEquals(1, scenarioList.size());
    } */

     static testMethod void AccountNameOrCustNumTest() {
    	Test.starttest();
		 SearchScenarioController srchcont = new SearchScenarioController();
		 srchcont.customerIPI = '98765';
		 srchcont.clientName = 'Test Account';
		 srchcont.boolOperator = 'Or';
		 srchcont.search();
		 List<LLC_BI__Scenario__c> scenarioList = srchcont.scenarioList;
		 test.stopTest();
		 System.assertEquals(scenarioList.size(), 1);
    }

      static testMethod void AccountNameAndCustNumTest() {
    	Test.starttest();
		 SearchScenarioController srchcont = new SearchScenarioController();
		 srchcont.customerIPI = '98765';
		 srchcont.clientName = 'Test Account';
		 srchcont.boolOperator = 'And';
		 srchcont.search();
		 List<LLC_BI__Scenario__c> scenarioList = srchcont.scenarioList;
		 System.assertEquals(scenarioList.size(), 1);
		 srchcont.hasNext = true;
		 srchcont.hasPrevious = true;
		 srchcont.pageNumber = 1;
		 srchcont.first();
		 srchcont.last();
		 srchcont.previous();
		 srchcont.next();
		 srchcont.cancel();
		 srchcont.numPages = 20;
		 Integer numPage = srchcont.numPages;

		 test.stopTest();
    }

}