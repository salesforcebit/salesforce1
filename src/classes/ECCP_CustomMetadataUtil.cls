/*******************************************************************************************************************************************
* @name         :   ECCP_CustomMetadataUtil
* @description  :   This class will be responsible to query metadata
* @author       :   Aditya
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
public class ECCP_CustomMetadataUtil{


/*******************************************************************************************************************************************
* @name         :   queryMetadata
* @description  :   This class will be responsible to query Webservice ECCP_WebService_Data__mdt metadata
* @author       :   Aditya
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/

public static map<String,ECCP_WebService_Data__mdt> queryMetadata(String metaName){
        Map<String,ECCP_WebService_Data__mdt> mapWsSettings = new Map<String,ECCP_WebService_Data__mdt>();
        for(ECCP_WebService_Data__mdt mdt : [select DeveloperName,MasterLabel,CertificateName__c,EndPointUrl__c,TimeOut__c,isPerfLogRequired__c,OrchestrationCode__c from ECCP_WebService_Data__mdt
                                              where DeveloperName = :metaName]){
            mapWsSettings.put(mdt.DeveloperName,mdt);        
        }
        return mapWsSettings;
    }

/*******************************************************************************************************************************************
* @name         :   queryMetadata
* @description  :   This class will be responsible to query Webservice ECCP_Error_Code__mdt metadata
* @author       :   Aditya
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
    public static map<String,ECCP_Error_Code__mdt> queryErrorMetadata(){
        Map<String,ECCP_Error_Code__mdt> mapWsError = new Map<String,ECCP_Error_Code__mdt>();
        for(ECCP_Error_Code__mdt mdt : [select DeveloperName,ECCP_Error_Message__c,ECCP_Error_Description__c from ECCP_Error_Code__mdt]){
            mapWsError.put(mdt.DeveloperName,mdt);        
        }
        return mapWsError;
    }
/*******************************************************************************************************************************************
* @name         :   queryStubMetadata
* @description  :   This Method query webserviceStub ECCP_CheckTheService__mdt metadata
* @author       :   Nagarjun
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/    
    public static map<String,ECCP_CheckTheService__mdt> queryStubMetadata(String stubName){
        Map<String,ECCP_CheckTheService__mdt> mapWsSettings = new Map<String,ECCP_CheckTheService__mdt>();
        for(ECCP_CheckTheService__mdt mdt : [select DeveloperName,MasterLabel,isStubSet__c from ECCP_CheckTheService__mdt
                                              where DeveloperName = :stubName]){
            mapWsSettings.put(mdt.DeveloperName,mdt);        
        }
        return mapWsSettings;
    }
/*******************************************************************************************************************************************
* @name         :   queryMetadata
* @description  :   This method is responsible to query ECCP_Error_Code__mdt metadata
* @author       :   Aditya
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
    public static map<String,ECCP_Error_Code__mdt> errorMessage(String errorCode){
    
        Map<String,ECCP_Error_Code__mdt > mapException = new Map<String,ECCP_Error_Code__mdt >();
        
        for(ECCP_Error_Code__mdt mdt : [select DeveloperName,MasterLabel,ECCP_Error_Description__c,ECCP_Error_Message__c from ECCP_Error_Code__mdt
                                              where DeveloperName = :errorCode]){
            mapException.put(mdt.DeveloperName,mdt);        
        }
        return mapException;
    
    }   
/*******************************************************************************************************************************************
* @name         :   queryRetryMetadata
* @description  :   This method is responsible to query ECCP_ServiceRetry__mdt metadata
* @author       :   Naga
* @createddate  :   25/04/2016
*******************************************************************************************************************************************/
    public static map<String,ECCP_ServiceRetry__mdt> retyMechanism(String retyService){
    
        Map<String,ECCP_ServiceRetry__mdt > mapRetyService = new Map<String,ECCP_ServiceRetry__mdt >();
        
        for(ECCP_ServiceRetry__mdt mdt : [select DeveloperName,MasterLabel,isRetrySet__c from ECCP_ServiceRetry__mdt
                                              where DeveloperName = :retyService]){
            mapRetyService.put(mdt.DeveloperName,mdt);        
        }
        return mapRetyService;
    
    }   

}