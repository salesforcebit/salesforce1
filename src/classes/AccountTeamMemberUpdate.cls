//========================================================================
//  KeyBank
//  Object: Call & Participant Extension
//  Author: Offshore
//  Detail: Trigger to  Insert/Update Account owner in AccountTeamMember
//========================================================================
Public class AccountTeamMemberUpdate
{
    Public void Insertaccounteammember(List<Account> newlist)
    {
        //It is insert of account Owner to AccountTeamMember
        List<AccountTeamMember> Accountteaminsertlist = new List<AccountTeamMember>();
        //assigning account owner name to account team member while creating account record.
      try
        {
            //Map<Id,User> userMap = new Map<Id,User>([select Id,Name,IsActive from User where IsActive=true]);
    
        for(Account acct :newlist)
        {                  
            accountteammember accountteaminsert = new accountteammember();
            //if(userMap.get(acct.ownerid)!=null){
            accountteaminsert.Userid = acct.ownerid;
            //}
            accountteaminsert.AccountId=acct.id;
            accountteaminsert.TeamMemberRole = 'Account Owner';
            accountteaminsertlist.add(accountteaminsert);
        
        }
        if(Accountteaminsertlist.size()>0)
        {
            database.insert(Accountteaminsertlist);
        } 
        }
                    
        catch(DMLException e)
        {
            system.debug(e.getMessage());
        }    
    }
    
    Public void Updateaccountteammember(List<Account> newupdatelist,Map<ID,Account> Accountmap)
    {
        //It is update of account Owner to AccountTeamMember
        List<AccountTeamMember> Accountteamupdatelist = new List<AccountTeamMember>();
        //assigning account owner name to account team member while updating account owner.
     try
        {
        For(account actupd :newupdatelist)
        { 
            if ((actupd.type == 'Business Customer') || (actupd.type == 'Business Prospect'))
            {
                Account oldaccount = Accountmap.get(actupd.id); 
                //Boolean Ischanged = (actupd.OwnerId!=oldaccount.OwnerId);
                If(actupd.OwnerId!=oldaccount.OwnerId)
                {
                    accountteammember accountteamupdate = new accountteammember();
                    accountteamupdate.Userid = actupd.ownerid;
                    accountteamupdate.AccountId=actupd.id;
                    accountteamupdate.TeamMemberRole = 'Account Owner';
                    accountteamupdatelist.add(accountteamupdate);
                }
            }
        }
        if(Accountteamupdatelist.size()>0)
        {
            database.insert(Accountteamupdatelist);
        } 
        } 
         catch(DMLException e)
          {
            system.debug(e.getMessage());
          }   
    }
}