@istest
private class ProjectPlanningGanttVFC_UT {

	static testmethod void testProjectPlanningGantt() {

		Account testAccount = UnitTestFactory.buildTestAccount();
		insert testAccount;

		Product2 testProduct = UnitTestFactory.buildTestProduct();
		testProduct.Family = 'Deposits & ECP';
		insert testProduct;

		LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
		insert testProductLine;

		LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
		insert testProductType;

		LLC_BI__Product__c testNCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
		insert testNCinoProduct;

		List<TSO_Workflow_Template__c> testTSOWorkflowTemplates = new List<TSO_Workflow_Template__c>();
		TSO_Workflow_Template__c testTSOWorkflowTemplateNew = UnitTestFactory.buildTestTSOWorkflowTemplate(testNCinoProduct.Id, Constants.treasuryServiceTypeNew, 20);
		TSO_Workflow_Template__c testTSOWorkflowTemplateRevise = UnitTestFactory.buildTestTSOWorkflowTemplate(testNCinoProduct.Id, Constants.treasuryServiceTypeRevise);
		TSO_Workflow_Template__c testTSOWorkflowTemplateCancel = UnitTestFactory.buildTestTSOWorkflowTemplate(testNCinoProduct.Id, Constants.treasuryServiceTypeCancel);
		testTSOWorkflowTemplates.add(testTSOWorkflowTemplateNew);
		testTSOWorkflowTemplates.add(testTSOWorkflowTemplateRevise);
		testTSOWorkflowTemplates.add(testTSOWorkflowTemplateCancel);
		insert testTSOWorkflowTemplates;

		List<TSO_Stage__c> testTSOStages = new List<TSO_Stage__c>();
		TSO_Stage__c testTSOStageNewOrderEntry = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateNew.Id, Constants.treasuryServiceStageOrderEntry, 1);
		TSO_Stage__c testTSOStageNewFulfillment = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateNew.Id, Constants.treasuryServiceStageFulfillment, 2);
		TSO_Stage__c testTSOStageNewTesting = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateNew.Id, Constants.treasuryServiceStageTesting, 3);
		TSO_Stage__c testTSOStageReviseOrderEntry = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateRevise.Id, Constants.treasuryServiceStageOrderEntry, 1);
		TSO_Stage__c testTSOStageReviseFulfillment = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateRevise.Id, Constants.treasuryServiceStageFulfillment, 1);
		TSO_Stage__c testTSOStageCancelOrderEntry = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateCancel.Id, Constants.treasuryServiceStageOrderEntry, 1);
		TSO_Stage__c testTSOStageCancelFulfillment = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateCancel.Id, Constants.treasuryServiceStageFulfillment, 1);
		testTSOStages.add(testTSOStageNewOrderEntry);
		testTSOStages.add(testTSOStageNewFulfillment);
		testTSOStages.add(testTSOStageNewTesting);
		testTSOStages.add(testTSOStageReviseOrderEntry);
		testTSOStages.add(testTSOStageReviseFulfillment);
		testTSOStages.add(testTSOStageCancelOrderEntry);
		testTSOStages.add(testTSOStageCancelFulfillment);
		insert testTSOStages;

		LLC_BI__Product_Package__c testProductPackage = UnitTestFactory.buildTestProductPackage();
		insert testProductPackage;

		List<LLC_BI__Treasury_Service__c> treasuryServices = new List<LLC_BI__Treasury_Service__c>();
		LLC_BI__Treasury_Service__c testTS1 = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTS2 = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTS3 = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		testTS1.Type__c = Constants.treasuryServiceTypeNew;
		testTS2.Type__c = Constants.treasuryServiceTypeNew;
		testTS3.Type__c = Constants.treasuryServiceTypeNew;
		testTS1.Planned_Implementation_Start_Date__c = date.today().addDays(1);
		testTS1.Delay_Implementation__c = true;
		treasuryServices.add(testTS1);
		treasuryServices.add(testTS2);
		treasuryServices.add(testTS3);
		test.startTest();
			insert treasuryServices;
		test.stopTest();

		testTS3.Prerequisite_Treasury_Service__c = testTS1.Id;
		update testTS3;

		ProjectPlanningGanttVFC controller = new ProjectPlanningGanttVFC();
		controller.productPackageId = testProductPackage.Id;

		List<ProjectPlanningGanttVFC.TreasuryServiceGantt> ganttTreasuryServices = controller.ganttTreasuryServices;

		System.assertEquals(3, ganttTreasuryServices.size());

		System.assertEquals(testTS1.Id, ganttTreasuryServices[0].treasuryService.Id);
		System.assertEquals(testTS3.Id, ganttTreasuryServices[1].treasuryService.Id);
		System.assertEquals(testTS2.Id, ganttTreasuryServices[2].treasuryService.Id);

		System.assertEquals(date.today().addDays(1), ganttTreasuryServices[0].startDate);
		System.assertEquals(date.today().addDays(4), ganttTreasuryServices[1].startDate);
		System.assertEquals(date.today(), ganttTreasuryServices[2].startDate);

		System.assertEquals(true, ganttTreasuryServices[0].stages[1].isFulfillment);
		System.assertEquals(true, ganttTreasuryServices[1].stages[1].isFulfillment);
		System.assertEquals(true, ganttTreasuryServices[2].stages[1].isFulfillment);
		System.assertEquals(true, controller.stagesFound);
		System.assertEquals(date.today(), controller.startDate);
		System.assertEquals(date.today().addDays(10), controller.finalStageEndDate);
		System.assertEquals(10, controller.totalDaysToComplete);
	}

	static testmethod void testKickoffProject() {

		Account testAccount = UnitTestFactory.buildTestAccount();
		insert testAccount;

		Product2 testProduct = UnitTestFactory.buildTestProduct();
		testProduct.Family = 'Deposits & ECP';
		insert testProduct;

		LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
		insert testProductLine;

		LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
		insert testProductType;

		LLC_BI__Product__c testNCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
		insert testNCinoProduct;

		List<TSO_Workflow_Template__c> testTSOWorkflowTemplates = new List<TSO_Workflow_Template__c>();
		TSO_Workflow_Template__c testTSOWorkflowTemplateNew = UnitTestFactory.buildTestTSOWorkflowTemplate(testNCinoProduct.Id, Constants.treasuryServiceTypeNew, 20);
		TSO_Workflow_Template__c testTSOWorkflowTemplateRevise = UnitTestFactory.buildTestTSOWorkflowTemplate(testNCinoProduct.Id, Constants.treasuryServiceTypeRevise);
		TSO_Workflow_Template__c testTSOWorkflowTemplateCancel = UnitTestFactory.buildTestTSOWorkflowTemplate(testNCinoProduct.Id, Constants.treasuryServiceTypeCancel);
		testTSOWorkflowTemplates.add(testTSOWorkflowTemplateNew);
		testTSOWorkflowTemplates.add(testTSOWorkflowTemplateRevise);
		testTSOWorkflowTemplates.add(testTSOWorkflowTemplateCancel);
		insert testTSOWorkflowTemplates;

		List<TSO_Stage__c> testTSOStages = new List<TSO_Stage__c>();
		TSO_Stage__c testTSOStageNewOrderEntry = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateNew.Id, Constants.treasuryServiceStageOrderEntry, 1);
		TSO_Stage__c testTSOStageNewFulfillment = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateNew.Id, Constants.treasuryServiceStageFulfillment, 2);
		TSO_Stage__c testTSOStageNewTesting = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateNew.Id, Constants.treasuryServiceStageTesting, 3);
		TSO_Stage__c testTSOStageReviseOrderEntry = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateRevise.Id, Constants.treasuryServiceStageOrderEntry, 1);
		TSO_Stage__c testTSOStageReviseFulfillment = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateRevise.Id, Constants.treasuryServiceStageFulfillment, 1);
		TSO_Stage__c testTSOStageCancelOrderEntry = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateCancel.Id, Constants.treasuryServiceStageOrderEntry, 1);
		TSO_Stage__c testTSOStageCancelFulfillment = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateCancel.Id, Constants.treasuryServiceStageFulfillment, 1);
		testTSOStages.add(testTSOStageNewOrderEntry);
		testTSOStages.add(testTSOStageNewFulfillment);
		testTSOStages.add(testTSOStageNewTesting);
		testTSOStages.add(testTSOStageReviseOrderEntry);
		testTSOStages.add(testTSOStageReviseFulfillment);
		testTSOStages.add(testTSOStageCancelOrderEntry);
		testTSOStages.add(testTSOStageCancelFulfillment);
		insert testTSOStages;

		LLC_BI__Product_Package__c testProductPackage = UnitTestFactory.buildTestProductPackage();
		insert testProductPackage;

		List<LLC_BI__Treasury_Service__c> treasuryServices = new List<LLC_BI__Treasury_Service__c>();
		LLC_BI__Treasury_Service__c testTS1 = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTS2 = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTS3 = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		testTS1.Type__c = Constants.treasuryServiceTypeNew;
		testTS2.Type__c = Constants.treasuryServiceTypeNew;
		testTS3.Type__c = Constants.treasuryServiceTypeNew;
		testTS1.Documents_Complete__c = true;
		testTS2.Documents_Complete__c = true;
		testTS3.Documents_Complete__c = true;
		testTS1.Planned_Implementation_Start_Date__c = date.today().addDays(1);
		testTS1.Delay_Implementation__c = true;
		treasuryServices.add(testTS1);
		treasuryServices.add(testTS2);
		treasuryServices.add(testTS3);
		insert treasuryServices;

		testTS3.Prerequisite_Treasury_Service__c = testTS1.Id;
		update testTS3;

		ProjectPlanningGanttVFC controller = new ProjectPlanningGanttVFC();
		controller.productPackageId = testProductPackage.Id;

		List<ProjectPlanningGanttVFC.TreasuryServiceGantt> ganttTreasuryServices = controller.ganttTreasuryServices;

		System.assertEquals(3, ganttTreasuryServices.size());

		test.startTest();
			controller.kickoffProject();
		test.stopTest();

		Set<Id> treasuryServiceIds = new Set<Id> {testTS1.Id, testTS2.Id, testTS3.Id};
		Map<Id, LLC_BI__Treasury_Service__c> resultTreasuryServices = new Map<Id, LLC_BI__Treasury_Service__c>([Select Id, LLC_BI__Stage__c from LLC_BI__Treasury_Service__c Where Id in: treasuryServiceIds]);

		// System.assertEquals(resultTreasuryServices.get(testTS1.Id).LLC_BI__Stage__c, Constants.treasuryServiceStagePendingFutureStartDate);
		// System.assertEquals(resultTreasuryServices.get(testTS2.Id).LLC_BI__Stage__c, Constants.treasuryServiceStageFulfillment);
		// System.assertEquals(resultTreasuryServices.get(testTS3.Id).LLC_BI__Stage__c, Constants.treasuryServiceStagePendingPrerequisites);
	}

	static testmethod void testTSOStageGanttSort() {

		Account testAccount = UnitTestFactory.buildTestAccount();
		insert testAccount;

		Product2 testProduct = UnitTestFactory.buildTestProduct();
		testProduct.Family = 'Deposits & ECP';
		insert testProduct;

		LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
		insert testProductLine;

		LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
		insert testProductType;

		LLC_BI__Product__c testNCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
		insert testNCinoProduct;

		List<TSO_Workflow_Template__c> testTSOWorkflowTemplates = new List<TSO_Workflow_Template__c>();
		TSO_Workflow_Template__c testTSOWorkflowTemplateNew = UnitTestFactory.buildTestTSOWorkflowTemplate(testNCinoProduct.Id, Constants.treasuryServiceTypeNew, 20);
		testTSOWorkflowTemplates.add(testTSOWorkflowTemplateNew);
		insert testTSOWorkflowTemplates;

		List<TSO_Stage__c> tsoStagesReverseOrder = new List<TSO_Stage__c>();
		List<ProjectPlanningGanttVFC.TSOStageGantt> stageGantts = new List<ProjectPlanningGanttVFC.TSOStageGantt>();

		for(Treasury_Service_Stage_Order__mdt stageOrder : [Select Current_Stage__c, Stage_Order__c from Treasury_Service_Stage_Order__mdt Order By Stage_Order__c desc]) {
			if(stageOrder.Stage_Order__c != null) {
				TSO_Stage__c tsoStage = UnitTestFactory.buildTestTSOStage(testTSOWorkflowTemplateNew.Id, stageOrder.Current_Stage__c);
				tsoStagesReverseOrder.add(tsoStage);
				stageGantts.add(new ProjectPlanningGanttVFC.TSOStageGantt(tsoStage));
			}
		}

		//upsert tsoStagesReverseOrder;

		System.debug(tsoStagesReverseOrder);
		System.debug(stageGantts);
		stageGantts.sort();
		System.debug(stageGantts);

		for(Integer i = 0; i < stageGantts.size(); i++) {
			System.assertEquals(tsoStagesReverseOrder[stageGantts.size() - (i +1)].Id, stageGantts[i].stage.Id);
		}
	}

	static testmethod void testTreasuryServiceGanttSort() {

		Account testAccount = UnitTestFactory.buildTestAccount();
		insert testAccount;

		Product2 testProduct = UnitTestFactory.buildTestProduct();
		testProduct.Family = 'Deposits & ECP';
		insert testProduct;

		LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
		insert testProductLine;

		LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
		insert testProductType;

		LLC_BI__Product__c testNCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
		insert testNCinoProduct;

		List<TSO_Workflow_Template__c> testTSOWorkflowTemplates = new List<TSO_Workflow_Template__c>();
		TSO_Workflow_Template__c testTSOWorkflowTemplateNew = UnitTestFactory.buildTestTSOWorkflowTemplate(testNCinoProduct.Id, Constants.treasuryServiceTypeNew, 20);
		testTSOWorkflowTemplates.add(testTSOWorkflowTemplateNew);
		insert testTSOWorkflowTemplates;

		LLC_BI__Product_Package__c testProductPackage = UnitTestFactory.buildTestProductPackage();
		insert testProductPackage;

		List<LLC_BI__Treasury_Service__c> treasuryServices = new List<LLC_BI__Treasury_Service__c>();
		LLC_BI__Treasury_Service__c testTS1 = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTS2 = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		LLC_BI__Treasury_Service__c testTS3 = UnitTestFactory.buildTestTreasuryService(testNCinoProduct.Id, testProductPackage.Id, testAccount.Id);
		treasuryServices.add(testTS1);
		treasuryServices.add(testTS2);
		treasuryServices.add(testTS3);
		insert treasuryServices;

		testTS3.Prerequisite_Treasury_Service__c = testTS1.Id;
		update testTS3;

		List<ProjectPlanningGanttVFC.TreasuryServiceGantt> tsGantts = new List<ProjectPlanningGanttVFC.TreasuryServiceGantt>();
		tsGantts.add(new ProjectPlanningGanttVFC.TreasuryServiceGantt(testTS1, testTSOWorkflowTemplateNew));
		tsGantts.add(new ProjectPlanningGanttVFC.TreasuryServiceGantt(testTS2, testTSOWorkflowTemplateNew));
		tsGantts.add(new ProjectPlanningGanttVFC.TreasuryServiceGantt(testTS3, testTSOWorkflowTemplateNew));
		tsGantts[2].prerequisiteTreasuryService = tsGantts[0];
		tsGantts.sort();

		System.assertEquals(testTS1.Id, tsGantts[0].treasuryService.Id);
		System.assertEquals(testTS3.Id, tsGantts[1].treasuryService.Id);
		System.assertEquals(testTS2.Id, tsGantts[2].treasuryService.Id);
	}
}