/*******************************************************************************************************************************************
* @name         :   ECCP_McaLoanProcessUpdatelogic
* @description  :   This class is responsible for processing compliance request came from MCA.
* @author       :   Nagarjun
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
public class ECCP_McaLoanProcessUpdatelogic{
   
    public static ECCP_MCA_WSIB_Response updateLoanReq( ECCP_MCA_WSIB_Request complianceReqInfo){   
        ECCP_MCA_WSIB_Response updatedLoan = new ECCP_MCA_WSIB_Response();
        ECCP_Compliance__c complianceQuerried;
        ECCP_PerformanceLog__c plog; 
        try{
            plog = ECCP_UTIL_PerformanceLog.createPerformanceLog(ECCP_IntegrationConstants.MCAInService, ECCP_IntegrationConstants.InBound,ECCP_IntegrationConstants.MCAProcessRequest ,JSON.Serialize(complianceReqInfo));          
            if(complianceReqInfo.CIURefNbr !=null && complianceReqInfo.CIURefNbr !=''){//Check if the Mca referecnce no is null
                try{// query and fetch the loan id
                    complianceQuerried = ECCP_McaQuerryLoanDetails.fetchLoanRec(complianceReqInfo);     
                }
                catch(exception e){  //write exception if there are any problems with fetch                  
                    ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.MCAInService, ECCP_IntegrationConstants.MCAClassName, ECCP_IntegrationConstants.updateLoanReq , string.valueOf(e.getlineNumber()) ,e.getMessage() , complianceReqInfo.CIURefNbr,e.getStackTraceString());                      
                    updatedLoan.sfdcResponse = 'Error -'+ECCP_IntegrationConstants.dmlException +':'+ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.dmlException ).get(ECCP_IntegrationConstants.dmlException ).ECCP_Error_Message__c;
                    ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
                    return updatedLoan;
                } 
                if(complianceQuerried!=null){     //update the loan  
                    updatedLoan = ECCP_McaLoanRecUpdate.updateLoan(complianceQuerried ,complianceReqInfo);
                }
                else{
                    ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.MCAInService,ECCP_IntegrationConstants.MCAClassName, ECCP_IntegrationConstants.updateLoanReq, ECCP_IntegrationConstants.NullValue ,ECCP_IntegrationConstants.InboundNullInput, ECCP_IntegrationConstants.NullValue,ECCP_IntegrationConstants.NullValue);
                    updatedLoan.sfdcResponse = 'Error -'+ECCP_IntegrationConstants.dmlException +':'+ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.dmlException ).get(ECCP_IntegrationConstants.dmlException ).ECCP_Error_Message__c;
                    ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
                }               
            } 
            else{
                ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.MCAInService,ECCP_IntegrationConstants.MCAClassName, ECCP_IntegrationConstants.updateLoanReq, ECCP_IntegrationConstants.NullValue ,ECCP_IntegrationConstants.InboundNullInput, ECCP_IntegrationConstants.NullValue,ECCP_IntegrationConstants.NullValue);
                updatedLoan.sfdcResponse = 'Error -'+ECCP_IntegrationConstants.nullInput+':'+ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullInput).get(ECCP_IntegrationConstants.nullInput).ECCP_Error_Message__c;
                ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
            }                      
        }
        catch(Exception e){
            ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.MCAInService, ECCP_IntegrationConstants.MCAClassName, ECCP_IntegrationConstants.updateLoanReq , string.valueOf(e.getlineNumber()) ,e.getMessage() , complianceReqInfo.CIURefNbr,e.getStackTraceString());                      
            updatedLoan.sfdcResponse = 'Error -'+ECCP_IntegrationConstants.updateFail +':'+ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.updateFail).get(ECCP_IntegrationConstants.updateFail).ECCP_Error_Message__c;
            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
        }
        ECCP_UTIL_PerformanceLog.updatePerformanceLog(plog, JSON.serialize(updatedLoan));
        return updatedLoan ;
    }
}