@isTest
public class Test_batchUpdate_on_ScopeGoal 
{
  public static testmethod void batchtest() {
    Profile p = [select id from profile where name='Bank Admin'];
    List<User> userList = new List<User>();
    User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan77@keybank.com',
           emailencodingkey='UTF-8', lastname='TestKeyEmployee',RACF_Number__c='HINDF',languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan77@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
           userList.add(testUser);
     User testUser1 = new User(alias = 'u2', email='vidhyasagaran_muralidharan88@keybank.com',
           emailencodingkey='UTF-8', lastname='Testing21', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan88@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
           userList.add(testUser1);
           insert userlist;
    batchUpdate_on_ScopeGoal bc = new batchUpdate_on_ScopeGoal();
    string query = 'SELECT ID, Name,User__r.Name, User__c,YTD_Calls__c,Current_Month_Call__c FROM Sales_Goal__c';
    
    //Inserting test records
    Account acc = new Account(name ='Acc Test');
       insert acc;
    List<Contact> conList = new List<Contact>();
    List<Participant__c > partList = new List<Participant__c >();
    contact con = new contact(LastName='TestKeyEmployee',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc.id,Salesforce_User__c=true,User_Id__c=testUser.Id,Phone='9876543210' );
    contact con1 = new contact(LastName='TestKeyEmployee1',RACFID__c='HINDFT', Officer_Code__c='HINDFT',Contact_Type__c='Key Employee',AccountId=acc.id,Salesforce_User__c=true,User_Id__c=testUser1.Id,Phone='9876543210' ); 
    conList.add(con);
    conList.add(con1);
    insert conList; 
    
    RecordType RTc = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Call__c' and name='Account Call'];
        
    Call__c accountcallc = new Call__c(Subject__c='testc', RecordTypeId=RTc.id, Call_Date__c=system.today(),Call_Type__c='COI Contact',AccountId__c=acc.id,Status__c='done');
    insert accountcallc;
    
    
    date today = system.today();
    Participant__c participant = new Participant__c();
    participant.Contact_Id__c= con.id;
    participant.CallId__c = accountcallc.Id;
    participant.createdDate = today;
    partList.add(participant);
    Participant__c participant1 = new Participant__c();
    participant1.Contact_Id__c= con.id;
    participant1.CallId__c = accountcallc.Id;
    participant1.createdDate = today;
    partList.add(participant1);
    Participant__c participant2 = new Participant__c();
    participant2.Contact_Id__c= con1.id;
    participant2.CallId__c = accountcallc.Id;
    participant2.createdDate = today;
    partList.add(participant2);
    insert partList;
    List<Sales_Goal__c> sg = new List<Sales_Goal__c>();
        Sales_Goal__c sc = new Sales_Goal__c(User__c=testUser.Id);
        sg.add(sc);
        Sales_Goal__c sg1 = new Sales_Goal__c(User__c=testUser.Id);
        sg.add(sg1);       
    insert sg;
    
    
    Id batchID = Database.executeBatch(bc);
    
  }
  
  public static testmethod void batchtestemp() {
    Profile p = [select id from profile where name='System Administrator'];
    User testUser1 = new User(alias = 'u1', email='vidhyasagaran_muralidharan78@keybank.com',
           emailencodingkey='UTF-8', lastname='Testing20', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan78@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser1;
    batchUpdate_on_SGActual_Revenue_Total br = new batchUpdate_on_SGActual_Revenue_Total();
    string query = 'SELECT ID, Name,User__r.Name, User__c FROM Sales_Goal__c';
    List<Sales_Goal__c> sg = new List<Sales_Goal__c>();
    List <opportunity> opport = new List <opportunity>();
    Account acc = new account (name='test 13234');
    insert acc;
    List<OpportunityTeamMember> opportunityTM = new List<OpportunityTeamMember>();
    for (Integer i=0;i<10;i++) {
        Sales_Goal__c s = new Sales_Goal__c(User__c=testUser1.Id);
        sg.add(s);
    }

    for (Integer k=0;k<10;k++) {
        opportunity opty = new opportunity(name ='test'+k, AccountId = acc.id, closedate=system.today(), StageName='Pursue',ownerid=testUser1.id,RecWeb_Number__c='1234'+k); 
        opport.add(opty);
    }
    insert opport;
    for (Integer j=0;j<10;j++) {
      OpportunityTeamMember OTM = new OpportunityTeamMember(OpportunityId=opport[0].id, TeamMemberRole = 'Opportunity Owner', UserId = testuser1.id);
    // OpportunityTeamMember OTM = new OpportunityTeamMember(OpportunityId='00617000003x3bm', TeamMemberRole = 'Opportunity Owner', UserId = testuser1.id);
    
     opportunityTM.add(OTM);
    }
    insert sg;
    // insert opport;

    //insert opportunityTM;
    Test.startTest();
    id batchId1 = Database.executeBatch(br);
    // System.abortJob(batchId1);
    Test.stopTest();
    }
  
  // Test class for the Batch 'batchonetimeUpdate_on_Participants'
    public static testMethod void Test_batchonetimeUpdate_on_Participants(){
    
     // Creating Test Data
   
        string query = 'SELECT ID, Name,Ownerid,Contact_Id__c,Contact_Id__r.User_Id__c,Contact_Id__r.Contact_Type__c FROM Participant__c limit 10';
        
        Profile p = [select id from profile where name='System Administrator'];
        User testKeyUser = new User(alias = 'u1', email='TestKeyUser@keybank.com',emailencodingkey='UTF-8', lastname='Testing20', languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, country='United States',timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan78@keybank.com',PLOB__c='support',PLOB_Alias__c='support');
        insert testKeyUser;
        
        Account acc = new Account(name ='Acc Test');
        insert acc;
        
        contact con = new contact(LastName='TestKeyEmployee',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc.id,Salesforce_User__c=true,User_Id__c=testKeyUser.Id,Phone='9876543210' );
        insert con;
       Participant__c parti = new Participant__c (contact_id__c= con.id);
       insert parti;
        
        batchonetimeUpdate_on_Participants batch_Parti = new batchonetimeUpdate_on_Participants ();
     
      // Execute 
        
        Test.startTest();
         
       //   if(Test.IsRunningTest()){ 
          id batchID2 = Database.executeBatch(batch_Parti);
        //  System.abortJob(batchID2);
        //   }
       Test.stopTest();
    
    }
    
}