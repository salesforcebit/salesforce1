/*********************************************************************************************************************************
 * @author       : Nagarjun        
 * @date         : 29/04/2016       
 * @description  : This class consists of static methods that logs the performance metrics for the operation.                                        
 **********************************************************************************************************************************/
public with sharing class ECCP_UTIL_PerformanceLogCheck {

    /******************************************************************************************************************************
     * @description       This method builds the Peformance log object for the operation and returns it to the source
     * @param             srcName    Name of the Source of operation
     * @param             srcType    Type of the Source
     * @param             appName    Name of the Application
     * @param             startTime  Start Time of the operation 
     * @return            PerformanceLog__c
     * @throws            NA
     *******************************************************************************************************************************/

    public static boolean isEnabledFlag;

    // new changes according to the user story
    public static ECCP_PerformanceLog__c createPerformanceLog(String srcName, String destURL, String logMessage, String request) {
        Map < String, ECCP_LogSetting__C > logSettingMap = ECCP_LogSetting__C.getAll();
        System.debug('LOGS ::::map:: ' + logSettingMap);
        ECCP_PerformanceLog__c performanceLog;

        if (logSettingMap.get('PerformanceExceptionLogEnabled').isEnable__c == true) {
            performanceLog = new ECCP_PerformanceLog__c();
            performanceLog.DestinationURL__c = destURL;
            performanceLog.StartTime__c = System.now();
            performanceLog.Log_Message__c = logMessage;
            performanceLog.TargetName__c = srcName;
            system.debug('++++ logSettingMap.ge ' +logSettingMap.get('SaveRequest_Response').isEnable__c  );
            if (logSettingMap.get('SaveRequest_Response').isEnable__c == true) {
                performanceLog.Request__c = request;
            }
        }
        return performanceLog;
        
    }


    /**********************************************************************************************************************************
     * @description       This method builds the Peformance log object for the operation and returns it to the source
     * @param             pfLog      Peformance Log recod for the operation
     * @param             endTime    End Time of the operation 
     * @return            ECCP_PerformanceLog__c 
     * @throws            NA
     ***********************************************************************************************************************************/
    public static ECCP_PerformanceLog__c updatePerformanceLog(ECCP_PerformanceLog__c pfLog, String response) {
        ECCP_LogSetting__C reqReslogSetObj = ECCP_LogSetting__C.getInstance('SaveRequest_Response');
        ECCP_LogSetting__C logSettingObj = ECCP_LogSetting__C.getInstance('PerformanceExceptionLogEnabled');
        isEnabledFlag = false;

        if (pfLog != null) {
            pfLog.EndTime__c = System.now();
            system.debug('!!!!!!' +pfLog.EndTime__c);
            if (reqReslogSetObj.IsEnable__c) {
                pfLog.Response__c = response;
            }            
        }
        return pfLog;
    }
}