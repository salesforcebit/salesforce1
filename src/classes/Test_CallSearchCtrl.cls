@isTest
public class Test_CallSearchCtrl{
    
    static Call__c callObj;
    static Account accObj;
    static Call__c callObjChild;
    static Account accObjChild;
    static Contact conObj;
    static Contact conObjNew;
    static User userObj;
    static User userObjNew;
    static Participant__c partObj;
    
    public static void createDataforCls(){
        Map<String, Schema.SObjectType> accsObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs = accsObjectMap.get('Account') ;
        Schema.DescribeSObjectResult resSchemaAcc = accs.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo = resSchemaAcc.getRecordTypeInfosByName();
        Id accRTId = accRecordTypeInfo.get('Customer').getRecordTypeId();
        System.Debug('accRTId:'+accRTId);
        
        accObj = new Account();
        accObj.name = 'Test Account';
        accObj.recordtypeId = accRTId;
        insert accObj;

        accObjChild = new Account();
        accObjChild.name = 'Test Account';
        accObjChild.recordtypeId = accRTId;
        accObjChild.Ultimate_Parent_AccountId__c = accObj.Id;
        insert accObjChild;


        Map<String, Schema.SObjectType> callsObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType calls = callsObjectMap.get('Call__c') ; 
        Schema.DescribeSObjectResult resSchemaCall = calls.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> callRecordTypeInfo = resSchemaCall.getRecordTypeInfosByName(); 
        Id callRTId = callRecordTypeInfo.get('Account Call').getRecordTypeId();
        System.Debug('callRTId:'+callRTId);
        
        callObj = new Call__c();
        callObj.Call_Type__c = 'COI Contact';
        callObj.recordTypeId = callRTId;
        callObj.Call_Date__c = Date.Today();
        callObj.Is_Private__c = true;
        callObj.Subject__c = 'Test Call';
        callObj.Status__c = 'DONE';
        insert callObj;
        
        callObjChild = new Call__c();
        callObjChild.Call_Type__c = 'COI Contact Child';
        callObjChild.recordTypeId = callRTId;
        callObjChild.Call_Date__c = Date.Today();
        callObjChild.Is_Private__c = true;
        callObjChild.Subject__c = 'Test Call Child';
        callObjChild.Status__c = 'DONE';
        insert callObjChild;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        userObj = new User(alias = 'testcu',
            email='testclass.user@test.com',
            emailencodingkey='UTF-8',
            lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = p.ID,
            PLOB__c = 'Support',
            RACF_Number__c ='testCl',
            timezonesidkey='America/Los_Angeles',
            username='testclass.user@test.com');
        
        insert userObj;
        
        userObjNew = new User(alias = 'testcu',
            email='testclassnew.user@test.com',
            emailencodingkey='UTF-8',
            lastname='TestingNew', languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = p.ID,
            PLOB__c = 'Support',
            RACF_Number__c ='testCN',
            timezonesidkey='America/Los_Angeles',
            username='testclassnew.user@test.com');
        
        insert userObjNew;
        
        Map<String, Schema.SObjectType> consObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType cons = consObjectMap.get('Contact') ;
        Schema.DescribeSObjectResult resSchemaCon = cons.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> conRecordTypeInfo = resSchemaCon.getRecordTypeInfosByName();
        Id conRTId = conRecordTypeInfo.get('Key Employee').getRecordTypeId();
        System.Debug('conRTId:'+conRTId);
        
        conObj = new Contact(); 
        conObj.firstName = 'Test';
        conObj.LastName = 'User Contact';
        conObj.Active__c = true;
        conObj.Salesforce_User__c = true;
        conObj.User_Id__c = userObj.Id;
        conObj.recordtypeId = conRTId;
        conObj.RACFID__c = userObj.RACF_Number__c;
        conObj.Phone='9876543210';
        insert conObj;
        
        conObjNew = new Contact(); 
        conObjNew.firstName = 'TestNew';
        conObjNew.LastName = 'User Contact New';
        conObjNew.Active__c = true;
        conObjNew.Salesforce_User__c = true;
        conObjNew.User_Id__c = userObjNew.Id;
        conObjNew.recordtypeId = conRTId;
        conObjNew.RACFID__c = userObjNew.RACF_Number__c;
        conObjNew.Phone='9876543210';
        insert conObjNew;
        
        partObj = new Participant__c();
        partObj.CallId__c = callObj.id;
        partObj.Contact_Id__c = conObj.Id;
        insert partObj;
        
    }
    
    static TestMethod void searchCallTest(){
    
        createDataforCls();
        Test.startTest();
        PageReference pageRef = new PageReference('/callSearchPage');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdCtrl = new ApexPages.Standardcontroller(callObj);
        CallSearchCtrl ctrlObj = new CallSearchCtrl(stdCtrl);
        ctrlObj.contactSelected();
        ctrlObj.singleContactSelected();
        ctrlObj.accountSelected();
        ctrlObj.searchcalls();
        ctrlObj.selectCheckBox();
        ctrlObj.transferRecords();
        System.currentPageReference().getParameters().put('contID', conObj.Id);  
        System.currentPageReference().getParameters().put('uId', UserObj.Id);  
        ctrlObj.fetchContactDetails();
        ctrlObj.fetchUserDetails();
        ctrlObj.getPageSize();
        ctrlObj.getTotalPages();
        ctrlObj.getPartListMethod();
        ctrlObj.ultimateParentBox();
        ctrlObj.ultimateParCheck = true;
        ctrlObj.ultimateParentBox();
        ctrlObj.selectAllBool = false;
        ctrlObj.contactSelected();
        ctrlObj.getPageSize();
        ctrlObj.getTotalPages();
        System.assert(ctrlObj.hasNext != null);
        System.assert(ctrlObj.hasPrevious!= null);
        System.assert(ctrlObj.pageNumber != null);
        ctrlObj.total_size = 30;
        ctrlObj.getTotalPages();
        System.assert(ctrlObj.hasNext != null);
        System.assert(ctrlObj.hasPrevious!= null);
        System.assert(ctrlObj.pageNumber != null);
         
        Test.stopTest();
    }
    
    static TestMethod void testMethod2(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User userObjTemp = new User(alias = 'testct',
            email='testclass.user@test.com',
            emailencodingkey='UTF-8',
            lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = p.ID,
            PLOB__c = 'Support',
            timezonesidkey='America/Los_Angeles',
            username='testclasstemp.user@test.com');
        
        insert userObjTemp;
        System.runAs(userObjTemp){
            createDataforCls();
            Test.startTest();
            callObj = [SELECT id,Name,Is_Private__c,RecordTypeId FROM Call__c WHERE Id = :callObj.id];
            PageReference pageRef = new PageReference('/callSearchPage');
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdCtrl = new ApexPages.Standardcontroller(callObj);
            CallSearchCtrl ctrlObj = new CallSearchCtrl(stdCtrl);
            ctrlObj.selectUserId = userObj.id;
            ctrlObj.searchcalls();
            System.currentPageReference().getParameters().put('singleSelect',callObj.Id); 
            System.currentPageReference().getParameters().put('allSelect',''); 
            ctrlObj.selectCheckBox();
            ctrlObj.singleContactSelected();
            ctrlObj.fetchUserDetails();
            ctrlObj.newContactId = conObjNew.Id;
            System.currentPageReference().getParameters().put('uId',userObj.Id); 
            ctrlObj.fetchUserDetails();
            ctrlObj.searchcalls();
            ctrlObj.transferRecords();
            ctrlObj.ultimateParCheck = true;
            ctrlObj.selectUserId = userObj.id;
            ctrlObj.searchcalls();
            ctrlObj.first();
            ctrlObj.last();
            ctrlObj.previous();
            ctrlObj.next();
            ctrlObj.total_size = 10;
            ctrlObj.getTotalPages();
            System.currentPageReference().getParameters().put('contID',conObj.Id); 
            ctrlObj.selectUserId = userObjTemp.Id;
            ctrlObj.fetchContactDetails();
            System.currentPageReference().getParameters().put('singleSelect',callObj.Id); 
            ctrlObj.singleContactSelected();
            ctrlObj.transferRecords();
            System.currentPageReference().getParameters().put('contID',conObj.Id); 
            ctrlObj.selectUserId = userObj.Id;
            ctrlObj.fetchContactDetails();
            ctrlObj.ultimateParCheck = true;
            ctrlObj.searchcalls();
            Test.stopTest();
        }
    }
    static TestMethod void testMethod3(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User userObjTemp = new User(alias = 'testct',
            email='testclass.user@test.com',
            emailencodingkey='UTF-8',
            lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = p.ID,
            PLOB__c = 'Support',
            timezonesidkey='America/Los_Angeles',
            username='testclasstemp.user@test.com');
        
        insert userObjTemp;
        System.runAs(userObjTemp){
            createDataforCls();
            Test.startTest();
            callObj = [SELECT id,Name,Is_Private__c,RecordTypeId FROM Call__c WHERE Id = :callObj.id];
            PageReference pageRef = new PageReference('/callSearchPage');
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdCtrl = new ApexPages.Standardcontroller(callObj);
            CallSearchCtrl ctrlObj = new CallSearchCtrl(stdCtrl);
            ctrlObj.selectUserId = userObj.id;
            ctrlObj.searchcalls();
            System.currentPageReference().getParameters().put('singleSelect',callObj.Id); 
            System.currentPageReference().getParameters().put('allSelect',''); 
            ctrlObj.selectCheckBox();
            ctrlObj.singleContactSelected();
            ctrlObj.fetchUserDetails();
            ctrlObj.newContactId = conObjNew.Id;
            System.currentPageReference().getParameters().put('uId',userObj.Id); 
            ctrlObj.fetchUserDetails();
            ctrlObj.searchcalls();
            ctrlObj.transferRecords();
            Test.stopTest();
        }
    }
    
}