@isTest
public class Test_CallParticipantExtensionEdit
{
Public static testmethod void CallParticipant_test()
       {
         //Create Test Data
           Profile p = [select id from profile where name='Bank Admin'];

     User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan@keybank.com',
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id,RACF_Number__c='HINDF', country='United States',
      timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharan4@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
        
         //Account new record
         Account a11 = new account(name='Dup Test');
         insert a11;
         Accountteammember at = new Accountteammember(AccountId=a11.id,TeamMemberRole='Banker',UserId=testUser.id);
         insert at;
         
         //Contact new record
         Contact c1 = new contact(LastName='Mr. Dhanesh Subramanian',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=a11.id,Salesforce_User__c=true,User_Id__c=testUser.id,Phone='9876543210');
         insert c1;
         
         //Call New record 
          Call__c  call = new Call__c();
          call.Subject__c = 'TestCallParticipantExtension';
          call.Call_Type__c = 'BD Monthly Update';
          call.Call_Date__c = system.today();
          call.AccountId__c=a11.id;
          call.status__c='done';
          insert call; 
          
          //participant new record
          Participant__c  part = new Participant__c  ();
          part.Contact_Id__c = c1.id ;
          part.CallId__c = call.id; 
          insert part; 
          //custom setting insertion
          CallVF__c cVF = new CallVF__c(name='CallCustomNew',Default_Label__c='Please select RecordType',Warning_Prompt__c='NOTE: Please enter mandatory fields before adding Participants from the below search list.',Warning_RACFID__c='RACF-ID is Empty for last selected participant.');
          insert cVF;
          //redirecting to call page 
          PageReference pRef = Page.CallCustom ;
          pRef.getParameters().put('id',call.id);
          Test.setCurrentPage(pRef);
          
          //Passing parameter to call extension class
          ApexPages.StandardController sc = new ApexPages.StandardController(call);
          CallParticipantExtension cp = new CallParticipantExtension(sc);
          Integer startCount = cp.ShoppingCart.size();
          
          //testing update list method
          cp.searchString='Dhanesh';
          List<Contact> contactList = new List<Contact>();
          contactlist.add(c1);
          cp.updateAvailableList();
          cp.AvailableParticipants= contactList;
          if(!cp.AvailableParticipants.isempty())
          {
          cp.toSelect= cp.AvailableParticipants[0].Id;
          }
          
          cp.addToShoppingCart();
          cp.updateAvailableList();
          
          //testing addToShoppingCartAtm method
          cp.toatmSelect=at.Id;
          Accountteammember atRACF = [select UserId , id, AccountId from AccountTeamMember where Id =: at.id ];
          contact userNameContacts = [Select Id, Name, RACFID__c from Contact where User_Id__c=:c1.User_Id__c and Contact_Type__c='Key Employee' and Salesforce_User__c=true LIMIT 1];
          cp.addToShoppingCartAtm();
          
          
          //testing addToShoppingCartAcontact method
          Contact cRACF = [select id, Name,RACFID__c,Contact_Type__c, Title, Contact.MailingCity,Contact.MailingState,Contact.Account.Name from Contact where AccountId =: call.AccountId__c ];
          cp.tocSelect=c1.Id;
          cp.addToShoppingCartAcontact();
          
          //testing add to shopping cart method
          cp.toSelect = c1.Id;
          cp.actionBeforeload();
          cp.addToShoppingCart();
          
          
          //testing remove form shopping cart method
          cp.toUnselect= c1.Id;
          cp.removeFromShoppingCart();
          cp.onSave();
        }
        
    }