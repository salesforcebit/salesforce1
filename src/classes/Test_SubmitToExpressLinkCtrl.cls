@isTest
public class Test_SubmitToExpressLinkCtrl{
    
    static Account accObj;
    static Account accVendorObj;
    static Contact conObj;
    static Contact conObjNew;
    static User userObj;
    static User userKEFObj;
    static User userObjNew;
    static Opportunity opptyObj;
    static Account accpartnerObj;
    static Account accpartnerDupObj;
    static Partner partObj;
    static Partner partDupObj;
    static OpportunityContactRole opptyConRole;
    static Private_Note_Attachment__c privateAttObj;
    static OpportunityLineItem opptyLineItemObj;
    static Product2 productObj;
    static Id pricebookTestId;
    static PricebookEntry stdPriceBook;
    
    public static void createDataforCls(){
        Map<String, Schema.SObjectType> accsObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs = accsObjectMap.get('Account') ;
        Schema.DescribeSObjectResult resSchemaAcc = accs.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo = resSchemaAcc.getRecordTypeInfosByName();
        Id accRTId = accRecordTypeInfo.get('Customer').getRecordTypeId();
        System.Debug('accRTId:'+accRTId);
        
        accObj = new Account();
        accObj.name = 'Test Account';
        accObj.recordtypeId = accRTId;
        accObj.BillingStreet = 'dajgfrewg';
        accObj.BillingCity = 'dajgfrewg';
        accObj.BillingState = 'dajgfrewg';
        accObj.BillingPostalCode = 'dajgfrewg';
        accObj.BillingCountry = 'dajgfrewg';
        insert accObj;

        accVendorObj = new Account();
        accVendorObj.name = 'Test Vendor Account';
        accVendorObj.recordtypeId = accRTId;
        accVendorObj.BillingStreet = 'dajgfrewg';
        accVendorObj.BillingCity = 'dajgfrewg';
        accVendorObj.BillingState = 'dajgfrewg';
        accVendorObj.BillingPostalCode = '54646';
        accVendorObj.BillingCountry = 'dajgfrewg';
        insert accVendorObj;
        
        accpartnerObj = new Account();
        accpartnerObj.name = 'Test Partner Account';
        accpartnerObj.recordtypeId = accRTId;
        accpartnerObj.BillingStreet = 'dajtertertetertertgfrewg';
        accpartnerObj.BillingCity = 'dajtertertetertertgfrewg';
        accpartnerObj.BillingState = 'dajtertertetertertgfrewg';
        accpartnerObj.BillingPostalCode = '3434545';
        accpartnerObj.BillingCountry = 'dajtertertetertertgfrewg';
        insert accpartnerObj;
        
        accpartnerDupObj = new Account();
        accpartnerDupObj.name = 'Test Partner1 Account';
        accpartnerDupObj.recordtypeId = accRTId;
        accpartnerDupObj.BillingStreet = 'dajtertertetertertgfrewg';
        accpartnerDupObj.BillingCity = 'dajtertertetertertgfrewg';
        accpartnerDupObj.BillingState = '';
        accpartnerDupObj.BillingPostalCode = '4535';
        accpartnerDupObj.BillingCountry = 'dajtertertetertertgfrewg';
        insert accpartnerDupObj;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Profile pCredit = [SELECT Id FROM Profile WHERE Name='KEF Credit']; 
        userObj = new User(alias = 'testcu',
            email='testclass.user@test.com',
            emailencodingkey='UTF-8',
            lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = p.ID,
            PLOB__c = 'Support',
            RACF_Number__c ='testCl',
            timezonesidkey='America/Los_Angeles',
            username='testclass.user@test.com');
        
        insert userObj;
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='KEF Business Admin']; 
        userObjNew = new User(alias = 'testTcu',
            email='testclassnew.user@test.com',
            emailencodingkey='UTF-8',
            lastname='TestingNew', languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = p1.ID,
            PLOB__c = 'Support',
            RACF_Number__c ='testCN',
            timezonesidkey='America/Los_Angeles',
            username='testclassnew.user@test.com');
        insert userObjNew;
        
        userKEFObj = new User(alias = 'testKEF',
            email='testKEFclass.user@test.com',
            emailencodingkey='UTF-8',
            lastname='Testing1', languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = pCredit.ID,
            PLOB__c = 'Support',
            RACF_Number__c ='testKEF',
            timezonesidkey='America/Los_Angeles',
            username='testKEFclass.user@test.com');
        
        //insert userKEFObj;
        
        Map<String, Schema.SObjectType> consObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType cons = consObjectMap.get('Contact') ;
        Schema.DescribeSObjectResult resSchemaCon = cons.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> conRecordTypeInfo = resSchemaCon.getRecordTypeInfosByName();
        Id conRTId = conRecordTypeInfo.get('Key Employee').getRecordTypeId();
        System.Debug('conRTId:'+conRTId);
        
        conObj = new Contact(); 
        conObj.firstName = 'Test';
        conObj.LastName = 'User Contact';
        conObj.Active__c = true;
        conObj.Salesforce_User__c = true;
        conObj.User_Id__c = userObj.Id;
        conObj.recordtypeId = conRTId;
        conObj.RACFID__c = userObj.RACF_Number__c;
        conObj.MailingCity = 'ewrrwe';
        conObj.MailingStreet ='ewrwerwer';
        conObj.MailingState ='ewrwrwer';
        conObj.MailingCountry = 'deff';
        conObj.MailingPostalCode = '34345';
        conObj.Phone='9876543210';
        insert conObj;
        
        Map<String, Schema.SObjectType> oppsObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType opps = oppsObjectMap.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaOpp = opps.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> oppRecordTypeInfo = resSchemaOpp.getRecordTypeInfosByName();
        Id oppRTId = oppRecordTypeInfo.get('KEF Opportunity').getRecordTypeId();
        System.Debug('oppRTId:'+oppRTId);
        
        opptyObj = new Opportunity();
        opptyObj.name = 'Test Oppty';
        opptyObj.AccountId = accObj.Id;
        opptyObj.recordtypeId = oppRTId;
        opptyObj.Volume__c = 5;
        opptyObj.StageName = 'Pitch';
        opptyObj.CloseDate = System.Today()+100;
        opptyObj.Vendor__c = accVendorObj.Id;
        insert opptyObj;
        
        partObj = new Partner();
        partObj.Role = 'KEF – Asset Supplier';
        partObj.OpportunityId = opptyObj.Id;
        partObj.AccountToId = accpartnerObj.Id;
        insert partObj;
        
        partDupObj = new Partner();
        partDupObj.Role = 'KEF – Asset Supplier';
        partDupObj.OpportunityId = opptyObj.Id;
        partDupObj.AccountToId = accpartnerDupObj.Id;
        
        opptyConRole = new OpportunityContactRole ();
        opptyConRole.Role = 'Investor';
        opptyConRole.OpportunityId = opptyObj.Id;
        opptyConRole.ContactId = conObj.Id;
        insert opptyConRole;

        privateAttObj = new Private_Note_Attachment__c();
        privateAttObj.Opportunity_Name__c = opptyObj.Id;
        insert privateAttObj;
        
        productObj = new Product2();
        productObj.Family = 'KEF';
        productObj.Product_Category__c = 'Conditional Sales Lease';
        productObj.Name = 'Test-Synthetic-End of Term';
        productObj.IsActive = true;
        productObj.Description = 'test description';
        productObj.ProductCode = 'KEF>Conditional Sales Lease>Synthetic-End of Term';
        insert productObj;
        
        pricebookTestId = Test.getStandardPricebookId();
        System.debug('pricebookTestId:'+Test.getStandardPricebookId());
        stdPriceBook = new PricebookEntry(Pricebook2Id = pricebookTestId, Product2Id = productObj.Id,UnitPrice = 10, IsActive = true);
        insert stdPriceBook;
        
        opptyLineItemObj = new OpportunityLineItem();
        //opptyLineItemObj.Product2 = productObj;
        opptyLineItemObj.Opportunityid = opptyObj.Id;
        opptyLineItemObj.Primary_Product__c = true;
        opptyLineItemObj.Payment_Frequency__c = 'Quarterly';
        opptyLineItemObj.Payment_Type__c = 'Advance';
        opptyLineItemObj.PricebookEntryId = stdPriceBook.Id;
        insert opptyLineItemObj;
              
    }
    
    static TestMethod void submitOpptyTestMethod1(){
        
        createDataforCls();
        Test.startTest();
        PageReference pageRef = new PageReference('/SubmitToExpressLinkPage');
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('Id',opptyObj.Id); 
        ApexPages.StandardController stdCtrl = new ApexPages.Standardcontroller(opptyObj);
        SubmitToExpressLinkCtrl ctrlObj = new SubmitToExpressLinkCtrl(stdCtrl);
        System.debug('Test Class Oppty:'+opptyObj.Id);
        ctrlObj.getCustomerAccountCheck();
        ctrlObj.getVendorAccountCheck();
        ctrlObj.getCOIPartnerCheck();
        //ctrlObj.getCOIAssetStatusCheck();
        ctrlObj.getPartnerAddCheck();
        ctrlObj.getContactAddCheck();
        ctrlObj.getFileCheck();
        ctrlObj.getSubmitOpptyCheck();
        ctrlObj.updateoppty();
        ctrlObj.bckOppty();
        opptyObj.Send_to_Express_Link__c = true;
        update opptyObj;
        ctrlObj = new SubmitToExpressLinkCtrl(stdCtrl);
        opptyObj.Send_to_Express_Link__c = false;
        opptyObj.Distribution_Strategy__c = 'Hold for Sale';
        opptyObj.Primary_Syndicator__c = userObjNew .Id;
        update opptyObj;
        ctrlObj = new SubmitToExpressLinkCtrl(stdCtrl);
        accObj.BillingCountry = ' ';
        update accObj;
        ctrlObj.getCustomerAccountCheck();
        accVendorObj.BillingCountry = ' ';
        update accVendorObj;
        ctrlObj.getVendorAccountCheck();
        //partDupObj.role= 'KEF – Prime';
        insert partDupObj;
        ctrlObj.getCOIPartnerCheck();
        ctrlObj.getPartnerAddCheck();
        //ctrlObj.getContactRoleaddedBool();
        ctrlObj.getContactRoleCheck();
        ctrlObj.getAddPrimaryProd();
        ctrlObj.getCheckPrimaryProduct();
        conObj.MailingCountry = ' ';
        update conObj;
        ctrlObj.getContactAddCheck();
        opptyLineItemObj.Doc_Origination_Fee__c = 2;
        opptyLineItemObj.Cost_of_Funds__c = 5.00;
        opptyLineItemObj.Spread__c = 3.00;
        opptyLineItemObj.Deal_Term_in_Months__c = 3;
        opptyLineItemObj.NPV__c = 3.00;
        update opptyLineItemObj;
        ctrlObj.getAddPrimaryProd();
        ctrlObj.getCheckPrimaryProduct();
        /* System.RunAs(userObjNew){
            ctrlObj.getFileCheck();
        } */
        Test.stopTest();
    }
}