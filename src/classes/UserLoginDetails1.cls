/*
This class is written to get login counts of users.
It contains a method to which a List of User Ids and time period will be passed and it will return a map of user Ids 
with login count for perticular users during the given duration.
*/
public with sharing class UserLoginDetails1{
    
    public static Map<Id, Integer> UserLoginCounts(Set<id> userIds, String timePeriod)
    {
        Map<Id, Integer> userIdVsLoginCount = new Map<Id, Integer>();
        //List<LoginHistory> allHistoryList = new List<LoginHistory>();
        String soqlQuery;
         Map<Id, List<LoginHistory>> userIdVsHistoryList = new Map<Id, List<LoginHistory>>();
        if(!system.Test.isRunningTest()){
            soqlQuery = 'SELECT Id, LoginTime, UserId FROM LoginHistory WHERE UserId IN :userIds AND LoginTime = '+ timePeriod;   
        }
        else{
            soqlQuery='SELECT Id, LoginTime, UserId FROM LoginHistory WHERE UserId IN :userIds AND LoginTime = '+ timePeriod +' limit 10';
        }   
        for(List<LoginHistory> allHistoryList: database.query(soqlQuery)){
            for(LoginHistory aHistory : allHistoryList){
            
                 Id currentUserId = aHistory.UserId;
                 
                 if(userIdVsHistoryList.containsKey(currentUserId)){
                     List<LoginHistory> loginList = userIdVsHistoryList.get(currentUserId);
                     loginList.add(aHistory);
                     userIdVsHistoryList.put(currentUserId, loginList);
                 } 
                 else{
                     List<LoginHistory> loginList = new List<LoginHistory>();
                     loginList.add(aHistory);
                     userIdVsHistoryList.put(currentUserId, loginList);
                 }              
            }
        }               
        for(Id currentUserId : userIdVsHistoryList.keyset())
        {
            List<LoginHistory> loginList = userIdVsHistoryList.get(currentUserId);
            Integer loginCount = 0;
            Set<Date> loginDatesSet = new Set<Date>();
            
            if(loginList.size()>0 && loginList != Null){
             
                for(LoginHistory currentHistory : loginList){
                
                    DateTime loginTime = currentHistory.LoginTime ;
                    Date loginDate = date.newinstance(loginTime.year(), loginTime.month(), loginTime.day());
                
                    if(!loginDatesSet.contains(loginDate)){ 
                        loginDatesSet.add(loginDate);                       
                        loginCount = loginCount +1 ;        
                    }                                           
                }                    
            }
            userIdVsLoginCount.put(currentUserId, loginCount);
        }
        return userIdVsLoginCount;        
    }
}