//================================================================================
//  KeyBank  
//  Object: Opportunity
//  Author: Offshore
//  Detail: opportunity_product_detail_Extension
//  Description :To Implement the detailfunctionality For Opportunity Product.

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================
Public class opportunity_product_detail_Extension{
 
 public Opportunitylineitem OLI{Get; set;}
 public Id oliId {get; set;}
 Public Boolean userpermission{get; set;}
 
 public opportunity_product_detail_Extension(ApexPages.StandardController controller){
 
  OLI = [Select Id, Name, Opportunity.id from Opportunitylineitem where id =:controller.getRecord().id limit 1 ];
 
  oliId = OLI.Opportunity.id;
  
  List<PermissionSetAssignment> lstcurrentUserPerSet = [  SELECT Id, PermissionSet.Name,AssigneeId
                                                                FROM PermissionSetAssignment
                                                                WHERE AssigneeId = :Userinfo.getUserId() ];
    
    for (PermissionSetAssignment psa: lstcurrentUserPerSet)
    {
    system.debug('##psa.PermissionSet.Name' + psa.PermissionSet.Name);
        if ( psa.PermissionSet.Name.equals('KEFOptysubmittedFieldPermission') ) {
        userpermission=true;
        }
                  
    }
    
 } 

  public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Opportunity   
      PageReference pageRef = new PageReference ('/'+oliId );
      return pageRef ;
      
    }
    
    
     
}