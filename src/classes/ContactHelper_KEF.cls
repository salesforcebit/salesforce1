/*
This class is written for KEF KRR Project by TCS Team.
It is a handler class for contact related Requirements.
*/

public with sharing class ContactHelper_KEF
{
     /* Below method is added for Release 3 Integration Req. 207. 
     It updates the Integration status of Account on creation and updation of Contact.*/

    public static Map<id, string> updateAccountIntegrationStatus(set<id> contactIds)
    {
        List<Account> accList = new List<Account>();
        List<id> conIds;
        Map<id, List<id>> accIdVsConIds = new Map<id, List<id>>();
        Map<id, string> accErrorMap = new Map<id, string>();
        Map<id, string> conErrorMap = new Map<id, string>();
        
        for(Contact con:[SELECT id, AccountId, Account.Integrated__c, Account.Integration_Status__c, Account.phone, 
                                Account.KEF_Risk_Role__c, Account.Vendor_status__c  
                        FROM contact 
                        WHERE id IN: contactIds                     
                        //and Account.Integration_Status__c != :System.Label.Not_picked_up
                        and Account.phone != null
                        and Account.BillingCity != null
                        and Account.BillingStreet != null
                        and Account.BillingState != null
                        and Account.BillingCountry != null
                        and Account.BillingPostalCode != null
                        and Account.KEF_Risk_Role__c != null])
        {
            if(con.AccountId != null && 
                (con.Account.KEF_Risk_Role__c == System.Label.Supplier_Only || 
                    (con.Account.KEF_Risk_Role__c == System.Label.Vendor_Supplier && 
                        (con.Account.Vendor_status__c == System.Label.Active || con.Account.Integrated__c)
                    )
                )
            )
            {                
                ID accId = con.Account.id;
                if(accIdVsConIds.keyset().contains(accId))
                {
                    conIds = accIdVsConIds.get(accId);
                }
                else
                {
                    Account acc = new Account();
                    acc.id = accId;
                    acc.Integration_Status__c = System.Label.Not_Picked_Up;                    
                    acc.Bypass_Pending_Credit_Validation_Account__c = KRRUtilities.fetchRandomDateTimeValue(); 
                    accList.add(acc);
                       
                    conIds = new List<id>();
                } 
                conIds.add(con.id);
                accIdVsConIds.put(accId, conIds);                    
            }
        }
        
        if(accList!=null && accList.size()>0)
        {                
            Database.SaveResult[] srList = Database.update(accList, false); 
                   
            for (Integer temp =0; temp < srList.size(); temp ++) 
            {            
                if (!srList[temp].isSuccess()) 
                {
                    String errMsg = srList[temp].getErrors().get(0).getMessage();
                    for(Id conId : accIdVsConIds.get(accList[temp].id))
                    {
                        conErrorMap.put(conId, errMsg);   
                    } 
                }
            }
        } 
        return conErrorMap;
    }
    /* Below method is added for Release 3 Integration Req. 208. 
     It updates the Integration status of Agreement on updation of Contact.*/
    
    public static Map<id, string> updateAgreementIntegrationStatus(set<id> contactIds)
    {
        List<Agreement__c> agrList = new List<Agreement__c>();
        List<id> conIds;
        Map<id, List<id>> agrIdVsConIds = new Map<id, List<id>>();
        Map<id, string> agrErrorMap = new Map<id, string>();
        Map<id, string> conErrorMap = new Map<id, string>();
        
        for(Contact_Agreement__c conAgrAsso :[SELECT id, Contact__c, Agreement__c, Agreement__r.Integration_Status__c, 
                                                    Agreement__r.Agreement_Status__c, Agreement__r.Integrated__c 
                                             FROM Contact_Agreement__c 
                                             WHERE Contact_Agreement__c.Contact__c IN: contactIds
                                             //AND Agreement__r.Integration_Status__c != :System.Label.Not_picked_up
                                             AND ( Agreement__r.Agreement_Status__c = :System.Label.Agreement_Active
                                             OR Agreement__r.Integrated__c = true)])
        {                                            
                ID agrId = conAgrAsso.Agreement__c;
                if(agrIdVsConIds.keyset().contains(agrId))
                {
                    conIds = agrIdVsConIds.get(agrId);
                }
                else
                {
                    Agreement__c agr = new Agreement__c();
                    agr.id = agrId;
                    agr.Integration_Status__c = System.Label.Not_Picked_Up;
                    agr.Bypass_Pending_Credit_Validation_Agr__c = KRRUtilities.fetchRandomDateTimeValue();
                    agrList.add(agr);
                       
                    conIds = new List<id>();
                } 
                conIds.add(conAgrAsso.Contact__c);
                agrIdVsConIds.put(agrId, conIds);                    
  
        }
        if(agrList!=null && agrList.size()>0)
        {                
            Database.SaveResult[] srList = Database.update(agrList, false); 
                   
            for (Integer temp =0; temp < srList.size(); temp ++) 
            {            
                if (!srList[temp].isSuccess()) 
                {
                    String errMsg = srList[temp].getErrors().get(0).getMessage();
                    for(Id conId : agrIdVsConIds.get(agrList[temp].id))
                    {
                        conErrorMap.put(conId, errMsg);   
                    } 
                }
            }
        }
        return conErrorMap;    
    }
}