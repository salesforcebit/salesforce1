/*
This handler class will be invoked from Account Trigger. It is created for writing operations related to
KeyBank Equipment Financing Project. This class is written to separate the code to avoid confusion and 
complexity.
*/
public with sharing class AccountHelper_KEF 
{
    //method that will populate the next review date as per condition on Accounts.
    public static void populateNextReviewDate(Account acc)
    {
        if(acc != null && acc.KEF_Risk_Role__c != null )
        {
            //if the status is active then add 1 year to current date-time and set the value.
            if(((acc.KEF_Risk_Role__c == System.Label.Vendor_Supplier && //check if the account role is vendor
                acc.Vendor_Status__c != null && acc.Vendor_Status__c == System.Label.Active)
                ||
                (acc.KEF_Risk_Role__c == System.Label.Supplier_Only && //check if the account role is supplier
                acc.Supplier_Status__c != null && acc.Supplier_Status__c == System.Label.Active))
                && acc.Next_Review_Date__c == null)
            {
                acc.Next_Review_Date__c = System.Today().addYears(1);   
            }
            //if the status is inactive then nullify the value in the field.
            else if((acc.KEF_Risk_Role__c == System.Label.Vendor_Supplier && //check if the account role is vendor
                acc.Vendor_Status__c != null && acc.Vendor_Status__c == System.Label.Inactive)
                ||
                (acc.KEF_Risk_Role__c == System.Label.Supplier_Only && //check if the account role is supplier
                acc.Supplier_Status__c != null && acc.Supplier_Status__c == System.Label.Inactive))
            {
                acc.Next_Review_Date__c = null;
            }
        }
        system.debug('acc.Next_Review_Date__c-->    '+acc.Next_Review_Date__c);
    }
    
    //method to check/uncheck vendor/supplier related checkboxes on account
    public static void checkUncheckVendorSupplierCheckbox(Account acc)
    {
        if(acc != null && acc.KEF_Risk_Role__c != null)
        {
            if(acc.KEF_Risk_Role__c == System.Label.Vendor_Supplier)
            {
                acc.Is_Vendor__c = true;
                acc.Is_Asset_Supplier__c = false;
            }
            else if(acc.KEF_Risk_Role__c == System.Label.Supplier_Only)
            {
                acc.Is_Vendor__c = false;
                acc.Is_Asset_Supplier__c = true;
            }           
        }
        else if(acc != null)
        {
            acc.Is_Vendor__c = false;
            acc.Is_Asset_Supplier__c = false;
        }
    }
    //method is to assign created user of pending credit status to  lookup field vendor pending Credit 
    public static void CheckVendorstatusChange(Account acc)
    
    {
        if(acc !=null && acc.Vendor_Status__c == System.Label.Pending_Credit)
        {
           acc.Pending_credit_user__c = UserInfo.getUserId();           
           acc.Vendor_Pending_Credit_Date__c = System.Now();
        }
        
    }
    //method is to assign created user of pending credit status to  lookup field supplier  pending Credit
    public static void CheckSupplierstatusChange(Account acc)
    {
         if(acc!=null && acc.Supplier_Status__c==System.Label.Pending_Credit)
         {
            acc.Supplier_Pending_Credit_User__c =UserInfo.getUserId();
            acc.Supplier_Pending_Credit_Date__c = System.Now();
         }
            
    }
    
    //This method is added to create a Funding grid record under Account when Status is changed to Vendor or Supplier(Release 3 Req.)
    public static Map<id,string> createFundingGrids(ID[] accIdList)
    {
        Funding_Grid__c fundGrid;  
        List<Funding_Grid__c> fundGridList = new List<Funding_Grid__c>();
        Map<id,string> errorstr = new Map<id,string>();
        
        for(ID accId : accIdList)
        {
            fundGrid = new Funding_Grid__c(); 
            fundGrid.Account__c = accId;
            fundGrid.Funding_Method__c = System.Label.Check;
            fundGrid.Default_Payment_Method__c = true;            
            fundGridList.add(fundGrid);
        } 
        
        List<Database.SaveResult> insertResults = Database.insert(fundGridList, false); 
        
        errorstr = prepareErrorMapFrmSaveResult(insertResults, fundGridList, System.Label.FundingGridInsertError);       
        
        return errorstr;
    }
    
    //method updates 'Default Payment Method' checkbox on Funding Grid to false  
    public static Map<Id, String> uncheckDefaultPaymentMethods(Map<id, id> oldDPMFGAccList )
    {
        Map<id,string> errorMap = new Map<id,string>();
        
        if(oldDPMFGAccList != null && oldDPMFGAccList.size() > 0)
        {
            List<Funding_Grid__c> fgtoUpdate = new List<Funding_Grid__c>(); 
            
            for(Funding_Grid__c fgRec : [select Id, Account__c, Default_Payment_Method__c 
                                            from Funding_Grid__c
                                            where Id IN :oldDPMFGAccList.keyset()
                                            and Default_Payment_Method__c = true])
            {
                fgRec.Default_Payment_Method__c = false;
                fgRec.Funding_Grid_Default_Update_date__c = System.Now();
                fgtoUpdate.add(fgRec);
            }
            
            if(fgtoUpdate != null && fgtoUpdate.size() > 0)
            {
                List<Database.SaveResult> updateResults = Database.update(fgtoUpdate, false);
                errorMap = prepareErrorMapFrmSaveResult(updateResults, fgtoUpdate, null);               
            }
        }        
        return errorMap;
    }
    
    //this method prepare a map of Account Id as key and related error message as value. Error message is generated from SaveResult 
    //found while inserting/updating a Funding Grid record.
    public static Map<Id, String> prepareErrorMapFrmSaveResult(List<Database.SaveResult> saveResults, List<Funding_Grid__c> fgList,
                                                                String genericError)
    {
        Map<id,string> errorMap = new Map<id,string>();
        
        if(fgList != null && fgList.size() > 0 && saveResults != null)
        {
            for(Integer i=0;i<saveResults.size();i++)
            {
                 if (!saveResults.get(i).isSuccess())
                 {                
                    // DML operation failed
                    Database.Error error = saveResults.get(i).getErrors( ).get(0);
                    String failedDML = error.getMessage();
                    String errorMsg = (genericError != null? genericError + ' ' : '') + (failedDML != null ? failedDML : '');
                    errorMap.put(fgList.get(i).Account__c,errorMsg);
                }
            }
        }
        return errorMap;
    }
    
    //this generic method for putting add error with a custom message.
    public static void addErrorOverride(Map<Id, String> errorMap, Map<Id, Account> newAccountMap)
    {
        if((errorMap != null && !errorMap.isEmpty()) && newAccountMap != null && newAccountMap.size() > 0)
        {
            for(Id accId : errorMap.keyset())
            {
                if(newAccountMap.containskey(accId))
                {
                    Account accRec = newAccountMap.get(accId);
                    accRec.addError(errorMap.get(accId));
                }
            }                       
        } 
    }
    
    //method for restricting users from deleting Account record if children opportunities or vendors are present.
    public static Map<id,string> restrictDeletionOnCondition(set<ID> accIdList) //updated method name for defect 337 - 1 Feb 2016
    {    
        Map<id,string> errorMap = new Map<id,string>();
        List<Vendor_Agreement__c> venAgrAssoList;
        List<Opportunity> opptyList;
        //commenting the field in query below for a requirement in April Release for KEF - 24 Feb 2016
        for(Account acc : [Select Id, Name, Integrated__c, MasterRecordId, KEF_Risk_Role__c, //Entity_Id__c,
                                    (Select Id From Vendor_Agreement__r limit 2),
                                    (Select Id From Opportunities limit 2) 
                            from Account 
                            where id IN :accIdList ALL ROWS])
        {
            
            venAgrAssoList = acc.Vendor_Agreement__r;
            opptyList = acc.Opportunities;
            
            system.debug('acc.MasterRecordId>>>        '+acc.MasterRecordId);
            //if masterRecordId is null that means the record is deleted manually or by code
            //when this id is populated it means that the record is deleted in an account merge operation, in this scenario the validations 
            //need to be skipped
            if(acc.MasterRecordId == null)
            {
                //account record is integrated with external system, don't allow its deletion, hence putting into error Map
                if(acc.Integrated__c == true)
                {
                    errorMap.put(acc.id, System.Label.AccountDeleteErrorMsg);  
                }
                //there are Agreements and Opportunities associated with this account, don't allow deletion
                else if(venAgrAssoList.size()>0 && opptyList.size()>0)
                {
                    errorMap.put(acc.id,System.Label.AccountDeleteErrorMsg1);        
                }
                //there are agreements associated with this account, don't allow deletion 
                else if(venAgrAssoList.size()>0)
                {
                    errorMap.put(acc.id,System.Label.AccountDeleteErrorMsg2);
                }
                //there are opportunities assoiciated with this Account, don't allow deletion 
                else if(opptyList.size()>0)
                {
                    errorMap.put(acc.id,System.Label.AccountDeleteErrorMsg3);
                }
            }
            /*start - adding below else condition for defect 337 - 1 Feb 2016*/
            //if the field value is populated, it means the record is being delete because of Merge operation
            //start - commenting below block for a requirement in April Release for KEF  - 24 Feb 2016
            /*
            else if (acc.MasterRecordId != null)
            {
                if(acc.KEF_Risk_Role__c != null && 
                (acc.KEF_Risk_Role__c == System.label.Vendor_Supplier || acc.KEF_Risk_Role__c == System.label.Supplier_Only ))
                {
                    errorMap.put(acc.id, System.label.MergeRestrictError1 + acc.Name + System.Label.MergeRestrictError2 
                                    + acc.Entity_Id__c + System.Label.MergeRestrictError3);
                }
            }
            */
            //end - commenting below block for a requirement in April Release for KEF  - 24 Feb 2016
            /*end - adding below else condition for defect 337 - 1 Feb 2016*/
        }
        return errorMap;
    }
    
    //method that marks related agreements as Ready for picked up by external webmethods.
    public static Map<id, String> markAgreementAsNotPickedUp(Set<id> accountIds)
    {
        Map<Id, String> errorMap = new Map<id, String>(); 
        if(accountIds != null && accountIds.size() > 0)
        {
            Map<Id, Agreement__c> agreementMap = new Map<Id, Agreement__c>();
            Map<Id, Set<Id>> agreementToAccountMap = new Map<Id, Set<id>>();
            
            //query only those children junction records where their parent agreement's integration status is other than 'Not Picked Up'
            //their status is Active or sometime in the past it was Active
            for(Vendor_Agreement__c vaRec : [select Id, Agreement__r.Integration_Status__c, Agreement__r.Agreement_Status__c,
                                                    Agreement__r.Old_Status_Value__c, Agreement__c, Agreement__r.DateTime_Active__c,
                                                    Account_Vendor__c
                                                from Vendor_Agreement__c
                                                where Account_Vendor__c IN :accountIds                                              
                                                //and Agreement__r.Integration_Status__c != :System.Label.Not_picked_up 
                                                and (Agreement__r.Agreement_Status__c = :System.Label.Active
                                                or Agreement__r.Integrated__c = true)])
            {
                if(!agreementMap.containsKey(vaRec.Agreement__c))
                {
                    //preparing a map of Agreement for updating.
                    agreementMap.put(vaRec.Agreement__c, new Agreement__c(Id = vaRec.Agreement__c, 
                                                                        Integration_Status__c = System.Label.Not_Picked_Up,
                                                                        Bypass_Pending_Credit_Validation_Agr__c = KRRUtilities.fetchRandomDateTimeValue()));
                }
                //preparing a map with Agreement id as KEY and set of related Account Id as VALUE
                //this map will be used for showing an error the end user if update fails
                Set<id> accIds = new Set<Id>();
                if(agreementToAccountMap.containsKey(vaRec.Agreement__c))
                {
                    accIds = agreementToAccountMap.get(vaRec.Agreement__c);
                }
                accIds.add(vaRec.Account_Vendor__c);
                agreementToAccountMap.put(vaRec.Agreement__c, accIds);
            }           
            
            if(agreementMap != null && !agreementMap.isEmpty())
            {
                List<Agreement__c> agrToUpdate = new List<Agreement__c>();
                
                agrToUpdate = agreementMap.values();
                //update list of agreements with integration status as Not Picked Up
                List<Database.SaveResult> updateResults = Database.update(agrToUpdate, false);
                
                if(updateResults != null && updateResults.size() > 0)
                {
                    for(Integer i=0; i<updateResults.size(); i ++)
                    {
                         if (!updateResults.get(i).isSuccess() && agrToUpdate.size() >= i)
                         {                
                            // DML operation failed
                            Database.Error error = updateResults.get(i).getErrors( ).get(0);
                            String failedDML = error.getMessage();
                            String errorMsg = System.Label.Agreement_Update_Error_Message + ' ' + (failedDML != null ? failedDML : '');
                            //prepare a map with acount id as key and error message as value.
                            if(agreementToAccountMap.containsKey(agrToUpdate.get(i).Id))
                            {
                                for(Id accId : agreementToAccountMap.get(agrToUpdate.get(i).Id))
                                {
                                    errorMap.put(accId,errorMsg);
                                }
                            }
                        }
                    }                   
                }
            }
        }
        
        return errorMap;
    } 
}