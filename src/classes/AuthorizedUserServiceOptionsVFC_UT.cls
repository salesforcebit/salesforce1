@istest
private class AuthorizedUserServiceOptionsVFC_UT {

    static testmethod void testPageLoad() {
        Account testAccount = UnitTestFactory.buildTestAccount();
        insert testAccount;

        List<Contact> testContacts = new List<Contact>();
        for(Integer i = 0; i < 5; i++) {
            Contact c = UnitTestFactory.buildTestContact(testAccount.Id);
            c.LastName += i;
            c.Contact_Email__c = 'testUser@key.com';
            testContacts.add(c);
        }
        insert testContacts;

        List<LLC_BI__Deposit__c> testDeposits = new List<LLC_BI__Deposit__c>();
        for(Integer i =0; i < 5; i++) {
            LLC_BI__Deposit__c d = UnitTestFactory.buildtestDeposit(testAccount.Id);
            testDeposits.add(d);
        }
        insert testDeposits;

        Product2 testProduct = UnitTestFactory.buildTestProduct();
        testProduct.Name = 'Scenario Test';
        testProduct.Family = 'Deposits & ECP';
        insert testProduct;

        LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
        insert testProductLine;

        LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
        insert testProductType;

        LLC_BI__Product__c testnCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
        insert testnCinoProduct;

        LLC_BI__Treasury_Service__c testTreasuryService = UnitTestFactory.buildTestTreasuryService(testnCinoProduct.Id, null, testAccount.Id);
        insert testTreasuryService;

        List<LLC_BI__Authorized_User__c> testAuthorizedUsers = new List<LLC_BI__Authorized_User__c>();
        for(Integer i=0; i < 5; i++) {
            LLC_BI__Authorized_User__c au = UnitTestFactory.buildTestAuthorizedUser(testTreasuryService.Id, testContacts[i].Id);
            au.User_has_unlimited_transfer_limit__c = true;
            au.Organization_ID__c = '1';
            testAuthorizedUsers.add(au);
        }
        insert testAuthorizedUsers;

        List<LLC_BI__Analyzed_Account__c> testAnalyzedAccounts = new List<LLC_BI__Analyzed_Account__c>();
        for(Integer i=0; i<5; i++) {
            LLC_BI__Analyzed_Account__c testAnalyzedAccount = UnitTestFactory.buildTestAnalyzedAccount(testTreasuryService.Id, testDeposits[i].Id);
            testAnalyzedAccount.Select_Service_Option_Type__c = 'KTT Account Management';
            testAnalyzedAccount.Service_Options__c = 'Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155);Reports and Balances';
            testAnalyzedAccounts.add(testAnalyzedAccount);
        }
        insert testAnalyzedAccounts;

        List<Users_and_Accounts__c> testUsersAndAccounts = new List<Users_and_Accounts__c>();
        for(LLC_BI__Analyzed_Account__c aa : testAnalyzedAccounts) {
            Users_and_Accounts__c uaa = UnitTestFactory.buildTestUsersAndAccounts(testAuthorizedUsers[0], aa);
            uaa.Select_Service_Option_Type__c = 'KTT Account Management';
            uaa.Service_Options__c = 'Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155);Reports and Balances';
            testUsersAndAccounts.add(uaa);
        }
        Users_and_Accounts__c testUAA = UnitTestFactory.buildTestUsersAndAccounts(testAuthorizedUsers[1], testAnalyzedAccounts[0]);
        testUAA.Select_Service_Option_Type__c = 'KTT Account Management';
        testUAA.Service_Options__c = 'Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155);';
        testUsersAndAccounts.add(testUAA);
        insert testUsersAndAccounts;

        ApexPages.StandardController std = new ApexPages.StandardController(testTreasuryService);
        AuthorizedUserServiceOptionsVFC controller = new AuthorizedUserServiceOptionsVFC(std);

        System.assertEquals(6, controller.usersAndAccountWrappers.size());
        System.assertEquals(2, controller.userOptions.size());
        System.assertEquals(controller.userOptions[0].getValue(), controller.selectedUser);
        controller.selectedUser = testAuthorizedUsers[0].Id;
        System.assertEquals(5, controller.usersAndAccountsForUser.size());
    }

    static testmethod void testSelectUser() {
        Account testAccount = UnitTestFactory.buildTestAccount();
        insert testAccount;

        List<Contact> testContacts = new List<Contact>();
        for(Integer i = 0; i < 5; i++) {
            Contact c = UnitTestFactory.buildTestContact(testAccount.Id);
            c.LastName += i;
            c.Contact_Email__c = 'testUser@key.com';
            testContacts.add(c);
        }
        insert testContacts;

        List<LLC_BI__Deposit__c> testDeposits = new List<LLC_BI__Deposit__c>();
        for(Integer i =0; i < 5; i++) {
            LLC_BI__Deposit__c d = UnitTestFactory.buildtestDeposit(testAccount.Id);
            testDeposits.add(d);
        }
        insert testDeposits;

        Product2 testProduct = UnitTestFactory.buildTestProduct();
        testProduct.Name = 'Scenario Test';
        testProduct.Family = 'Deposits & ECP';
        insert testProduct;

        LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
        insert testProductLine;

        LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
        insert testProductType;

        LLC_BI__Product__c testnCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
        insert testnCinoProduct;

        LLC_BI__Treasury_Service__c testTreasuryService = UnitTestFactory.buildTestTreasuryService(testnCinoProduct.Id, null, testAccount.Id);
        insert testTreasuryService;

        List<LLC_BI__Authorized_User__c> testAuthorizedUsers = new List<LLC_BI__Authorized_User__c>();
        for(Integer i=0; i < 5; i++) {
            LLC_BI__Authorized_User__c au = UnitTestFactory.buildTestAuthorizedUser(testTreasuryService.Id, testContacts[i].Id);
            au.User_has_unlimited_transfer_limit__c = true;
            au.Organization_ID__c = '1';
            testAuthorizedUsers.add(au);
        }
        insert testAuthorizedUsers;

        List<LLC_BI__Analyzed_Account__c> testAnalyzedAccounts = new List<LLC_BI__Analyzed_Account__c>();
        for(Integer i=0; i<5; i++) {
            LLC_BI__Analyzed_Account__c testAnalyzedAccount = UnitTestFactory.buildTestAnalyzedAccount(testTreasuryService.Id, testDeposits[i].Id);
            testAnalyzedAccount.Select_Service_Option_Type__c = 'KTT Account Management';
            testAnalyzedAccount.Service_Options__c = 'Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155);Reports and Balances';
            testAnalyzedAccounts.add(testAnalyzedAccount);
        }
        insert testAnalyzedAccounts;

        List<Users_and_Accounts__c> testUsersAndAccounts = new List<Users_and_Accounts__c>();
        for(LLC_BI__Analyzed_Account__c aa : testAnalyzedAccounts) {
            Users_and_Accounts__c uaa = UnitTestFactory.buildTestUsersAndAccounts(testAuthorizedUsers[0], aa);
            uaa.Select_Service_Option_Type__c = 'KTT Account Management';
            uaa.Service_Options__c = 'Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155);Reports and Balances';
            testUsersAndAccounts.add(uaa);
        }
        Users_and_Accounts__c testUAA = UnitTestFactory.buildTestUsersAndAccounts(testAuthorizedUsers[1], testAnalyzedAccounts[0]);
        testUAA.Select_Service_Option_Type__c = 'KTT Account Management';
        testUAA.Service_Options__c = 'Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155);';
        testUsersAndAccounts.add(testUAA);
        insert testUsersAndAccounts;

        ApexPages.StandardController std = new ApexPages.StandardController(testTreasuryService);
        AuthorizedUserServiceOptionsVFC controller = new AuthorizedUserServiceOptionsVFC(std);

        controller.selectedUser = testAuthorizedUsers[0].Id;
        System.assertEquals(5, controller.usersAndAccountsForUser.size());

        controller.selectedUser = testAuthorizedUsers[1].Id;

        test.startTest();
            controller.selectUser();
        test.stopTest();

        System.assertEquals(1, controller.usersAndAccountsForUser.size());
    }

    static testmethod void testUpdateServiceOptions() {
        Account testAccount = UnitTestFactory.buildTestAccount();
        insert testAccount;

        List<Contact> testContacts = new List<Contact>();
        for(Integer i = 0; i < 5; i++) {
            Contact c = UnitTestFactory.buildTestContact(testAccount.Id);
            c.LastName += i;
            c.Contact_Email__c = 'testUser@key.com';
            testContacts.add(c);
        }
        insert testContacts;

        List<LLC_BI__Deposit__c> testDeposits = new List<LLC_BI__Deposit__c>();
        for(Integer i =0; i < 5; i++) {
            LLC_BI__Deposit__c d = UnitTestFactory.buildtestDeposit(testAccount.Id);
            testDeposits.add(d);
        }
        insert testDeposits;

        Product2 testProduct = UnitTestFactory.buildTestProduct();
        testProduct.Name = 'Scenario Test';
        testProduct.Family = 'Deposits & ECP';
        insert testProduct;

        LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
        insert testProductLine;

        LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
        insert testProductType;

        LLC_BI__Product__c testnCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
        insert testnCinoProduct;

        LLC_BI__Treasury_Service__c testTreasuryService = UnitTestFactory.buildTestTreasuryService(testnCinoProduct.Id, null, testAccount.Id);
        insert testTreasuryService;

        List<LLC_BI__Authorized_User__c> testAuthorizedUsers = new List<LLC_BI__Authorized_User__c>();
        for(Integer i=0; i < 5; i++) {
            LLC_BI__Authorized_User__c au = UnitTestFactory.buildTestAuthorizedUser(testTreasuryService.Id, testContacts[i].Id);
            au.User_has_unlimited_transfer_limit__c = true;
            au.Organization_ID__c = '1';
            testAuthorizedUsers.add(au);
        }
        insert testAuthorizedUsers;

        List<LLC_BI__Analyzed_Account__c> testAnalyzedAccounts = new List<LLC_BI__Analyzed_Account__c>();
        for(Integer i=0; i<5; i++) {
            LLC_BI__Analyzed_Account__c testAnalyzedAccount = UnitTestFactory.buildTestAnalyzedAccount(testTreasuryService.Id, testDeposits[i].Id);
            testAnalyzedAccount.Select_Service_Option_Type__c = 'KTT Account Management';
            testAnalyzedAccounts.add(testAnalyzedAccount);
        }
        insert testAnalyzedAccounts;

        List<Users_and_Accounts__c> testUsersAndAccounts = new List<Users_and_Accounts__c>();
        for(LLC_BI__Analyzed_Account__c aa : testAnalyzedAccounts) {
            Users_and_Accounts__c uaa = UnitTestFactory.buildTestUsersAndAccounts(testAuthorizedUsers[0], aa);
            uaa.Select_Service_Option_Type__c = 'KTT Account Management';
            testUsersAndAccounts.add(uaa);
        }
        Users_and_Accounts__c testUAA = UnitTestFactory.buildTestUsersAndAccounts(testAuthorizedUsers[1], testAnalyzedAccounts[0]);
        testUsersAndAccounts.add(testUAA);
        insert testUsersAndAccounts;

        ApexPages.StandardController std = new ApexPages.StandardController(testTreasuryService);
        AuthorizedUserServiceOptionsVFC controller = new AuthorizedUserServiceOptionsVFC(std);

        //Need to add service options here due to inability to predict dependant picklist values
        for(Users_and_Accounts__c uaa : controller.usersAndAccounts) {
            //All will have 2 possible service options from the analyzed account
            //uaa.Analyzed_Account__r.Service_Options__c = AuthorizedUserServiceOptionsVFC.serviceOptionMetadata[0].Service_Option_Value__c + ';' + AuthorizedUserServiceOptionsVFC.serviceOptionMetadata[1].Service_Option_Value__c;
            uaa.Analyzed_Account__r.Service_Options__c = 'ESP Reports; Previous Day';
            //One will only have 1 selected on the users and account record
            if(uaa.Id == testUAA.Id) {
                //uaa.Service_Options__c = AuthorizedUserServiceOptionsVFC.serviceOptionMetadata[0].Service_Option_Value__c;
                uaa.Service_Options__c = 'ESP Reports';
                uaa.put('ESP_Options__c', 'zero');
            }
            else {
                // uaa.Service_Options__c = AuthorizedUserServiceOptionsVFC.serviceOptionMetadata[0].Service_Option_Value__c + ';' + AuthorizedUserServiceOptionsVFC.serviceOptionMetadata[1].Service_Option_Value__c;
                uaa.Service_Options__c = 'ESP Reports; Previous Day';
                uaa.put('ESP_Options__c', 'zero');
                uaa.put('Previous_Day_Options__c', 'one');
            }
        }

        controller.selectedUser = testAuthorizedUsers[1].Id;

        //System.assertEquals(2, controller.usersAndAccountsForUser[0].serviceOptions.size());
        //System.assertEquals(1, controller.usersAndAccountsForUser[0].apiFieldNames.size());

        Integer selectedOptions = 0;
        for(AuthorizedUserServiceOptionsVFC.ServiceOptionWrapper sow : controller.usersAndAccountsForUser[0].serviceOptions) {
            if(sow.isChecked == true)
                selectedOptions += 1;
            sow.isChecked = false; //Uncheck for testing next step
        }

        System.assertEquals(1, selectedOptions);
        //System.assertEquals('zero', controller.usersAndAccountsForUser[0].record.get(AuthorizedUserServiceOptionsVFC.serviceOptionMetadata[0].Field_to_Display__c));

        test.startTest();
            controller.updateServiceOptions();
        test.stopTest();

        selectedOptions = 0;
        for(AuthorizedUserServiceOptionsVFC.ServiceOptionWrapper sow : controller.usersAndAccountsForUser[0].serviceOptions) {
            if(sow.isChecked == true)
                selectedOptions += 1;
        }
        System.assertEquals(0, selectedOptions);
        System.assertEquals(0, controller.usersAndAccountsForUser[0].apiFieldNames.size());
        //System.assertEquals(null, controller.usersAndAccountsForUser[0].record.get(AuthorizedUserServiceOptionsVFC.serviceOptionMetadata[0].Field_to_Display__c));
    }

    static testmethod void testSaveRecords() {
        Account testAccount = UnitTestFactory.buildTestAccount();
        insert testAccount;

        List<Contact> testContacts = new List<Contact>();
        for(Integer i = 0; i < 5; i++) {
            Contact c = UnitTestFactory.buildTestContact(testAccount.Id);
            c.LastName += i;
            c.Contact_Email__c = 'testUser@key.com';
            testContacts.add(c);
        }
        insert testContacts;

        List<LLC_BI__Deposit__c> testDeposits = new List<LLC_BI__Deposit__c>();
        for(Integer i =0; i < 5; i++) {
            LLC_BI__Deposit__c d = UnitTestFactory.buildtestDeposit(testAccount.Id);
            testDeposits.add(d);
        }
        insert testDeposits;

        Product2 testProduct = UnitTestFactory.buildTestProduct();
        testProduct.Name = 'Scenario Test';
        testProduct.Family = 'Deposits & ECP';
        insert testProduct;

        LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
        insert testProductLine;

        LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
        insert testProductType;

        LLC_BI__Product__c testnCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
        insert testnCinoProduct;

        LLC_BI__Treasury_Service__c testTreasuryService = UnitTestFactory.buildTestTreasuryService(testnCinoProduct.Id, null, testAccount.Id);
        insert testTreasuryService;

        List<LLC_BI__Authorized_User__c> testAuthorizedUsers = new List<LLC_BI__Authorized_User__c>();
        for(Integer i=0; i < 5; i++) {
            LLC_BI__Authorized_User__c au = UnitTestFactory.buildTestAuthorizedUser(testTreasuryService.Id, testContacts[i].Id);
            au.User_has_unlimited_transfer_limit__c = true;
            au.Organization_ID__c = '1';
            testAuthorizedUsers.add(au);
        }
        insert testAuthorizedUsers;

        List<LLC_BI__Analyzed_Account__c> testAnalyzedAccounts = new List<LLC_BI__Analyzed_Account__c>();
        for(Integer i=0; i<5; i++) {
            LLC_BI__Analyzed_Account__c testAnalyzedAccount = UnitTestFactory.buildTestAnalyzedAccount(testTreasuryService.Id, testDeposits[i].Id);
            testAnalyzedAccount.Select_Service_Option_Type__c = 'KTT Account Management';
            testAnalyzedAccount.Service_Options__c = 'Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155);Reports and Balances';
            testAnalyzedAccounts.add(testAnalyzedAccount);
        }
        insert testAnalyzedAccounts;

        List<Users_and_Accounts__c> testUsersAndAccounts = new List<Users_and_Accounts__c>();
        for(LLC_BI__Analyzed_Account__c aa : testAnalyzedAccounts) {
            Users_and_Accounts__c uaa = UnitTestFactory.buildTestUsersAndAccounts(testAuthorizedUsers[0], aa);
            uaa.Select_Service_Option_Type__c = 'KTT Account Management';
            uaa.Service_Options__c = 'Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155);Reports and Balances';
            testUsersAndAccounts.add(uaa);
        }
        Users_and_Accounts__c testUAA = UnitTestFactory.buildTestUsersAndAccounts(testAuthorizedUsers[1], testAnalyzedAccounts[0]);
        testUAA.Select_Service_Option_Type__c = 'KTT Account Management';
        testUAA.Service_Options__c = 'Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155)';
        testUsersAndAccounts.add(testUAA);
        insert testUsersAndAccounts;

        ApexPages.StandardController std = new ApexPages.StandardController(testTreasuryService);
        AuthorizedUserServiceOptionsVFC controller = new AuthorizedUserServiceOptionsVFC(std);

        controller.selectedUser = testAuthorizedUsers[0].Id;


        Id updatedRecordId = controller.usersAndAccountsForUser[0].record.Id;
        Id unchangedRecordId = controller.usersAndAccountsForUser[1].record.Id;
        for(AuthorizedUserServiceOptionsVFC.ServiceOptionWrapper sow : controller.usersAndAccountsForUser[0].serviceOptions) {
            sow.isChecked = false;
        }
        controller.updateServiceOptions(); //This is called whenever a user checks/unchecks a box on the page. Required for functionality to work.

        test.startTest();
            controller.saveOptions();
        test.stopTest();

        System.assertEquals(null, [Select Service_Options__c from Users_and_Accounts__c Where Id =: updatedRecordId].Service_Options__c);
        //System.assertEquals('Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155);Reports and Balances', [Select Service_Options__c from Users_and_Accounts__c Where Id =: unchangedRecordId].Service_Options__c);
        //System.assertEquals('Pay down on a Line of Credit (5155);Draw on a Line of Credit (5155)', [Select Service_Options__c from Users_and_Accounts__c Where Id =: testUAA.Id].Service_Options__c);
    }
}