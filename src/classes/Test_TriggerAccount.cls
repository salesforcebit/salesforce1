@istest
Public class Test_TriggerAccount 
{
    public static testMethod void mergetest() 
    {     
     Account mergeacc = new account(name='mergetest');
     insert mergeacc; 
     Account mergeacc1 =new account(name='mergetest1');
     insert mergeacc1;  
     merge mergeacc mergeacc1;
    }
    public static testMethod void atmtest()
    {
     Profile p = [select id from profile where name='Bank Admin'];
     User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharanx@keybank.com',
           emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharanx@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
    Account atmacc = new Account(name='ATMtest');
    insert atmacc;
    Account atmacc1 =[Select Id,name,ownerId from Account where Id=:atmacc.Id];
    atmacc1.ownerId=testUser.Id;
    update atmacc1;
    }
    public static testMethod void sicnaicstest()
    {
     SIC_NAICS__c sic= new SIC_NAICS__c(Code__c='11',Code_Type__c='SIC',Description__c='test');
     insert sic;
     SIC_NAICS__c naics=new SIC_NAICS__c(Code__c='12',Code_Type__c='NAICS',Description__c='test1');
     insert naics;
     Account acc = new account(name='acct',Sic=sic.Code__c,SicDesc=sic.Description__c,NaicsCode=naics.Code__c,NaicsDesc=naics.Description__c);
     insert acc;
    }
 }