public class ParticipantDetails
{
    public ParticipantDetails(ApexPages.StandardController controller) 
{
    Participant__c part1 = new Participant__c();
    this.part1= (Participant__c)controller.getRecord();
    
}
public ParticipantDetails() 
{
    listofparticipant= new List<Participant__c>(); 
    Id userid = Userinfo.getUserId();      
    listofparticipant = fetch(userid );  
    listofparticipant1= new List<Participant__c>(); 
    //Id userid = Userinfo.getUserId();      
    listofparticipant1 = fetch1(userid );      
}


public ParticipantDetails(logindetailscontroller controller) 
{

}    

Public List<Participant__c> listofparticipant{get;set;}
Public List<Participant__c> listofparticipant1{get;set;}
Public Participant__c part1{get;set;}


public List<Participant__C> fetch(Id i)
{
    return [select name, id,call_type__c, call_date__c, CallId__r.Owner.Name, Subject__c, CallId__c, Contact_Id__r.User_Id__c from Participant__c WHERE Contact_Id__r.User_Id__c =: i and call_date__c = last_month];
}
public List<Participant__C> fetch1(Id i)
{
    return [select name, id,call_type__c, call_date__c, CallId__r.Owner.Name, Subject__c, CallId__c, Contact_Id__r.User_Id__c from Participant__c WHERE Contact_Id__r.User_Id__c =: i and call_date__c = this_month];
}

public PageReference back()
{

    PageReference pageRef;
    pageRef = new PageReference('/home/home.jsp');
    pageRef.setRedirect(true);
    return pageRef;  
}

public PageReference detail()
{
    String i= ApexPages.currentPage().getParameters().get('pid');   
    pagereference detail = new pagereference('/'+i);
    return detail;
}
}