//=================================================================================================
//  KeyBank
//  Object: Call & Participant Extension
//  Author: Offshore
//  Detail: Controller (New Page) for Visualforce page - CallCustomNew
//=================================================================================================
//          Date            Purpose
// Changes: 02/23/2015      Initial Version
//          04/03/2015      BY: Added dynamic related to field for Mobile
//                              Added various enhancements to code
//          04/03/2015      SS: Rec.type HC for Account/Contact/Lead/Opportunity calls
//                              Added various enhancements to code
//          04/20/2015      SS: enchanced Save method to make save all fields
//                              Added various enhancements to code
//          05/17/2015      BY: Changed filters to indexed fields for Production Release
//                              Filter using RACF-ID for participant add.
//          06/12/2015      SS: July Release 
//                              Change from M/D relationship to Lookup relationship
//          07/02/2015      SS: July Release 
//                              Handling Exception on Account Team Member method
//          08/28/2015      SS: October Release 
//                              Adding FX related fields for Oct release
//=================================================================================================

global with sharing class CallParticipantExtensionNew {

    public Contact[] AvailableParticipants {get;set;}
    public Participant__c[] shoppingCart{get; set;}
    public Contact[] ConRef{get;set;}
    public AccountTeamMember[] AtmRef {get;set;}
    public String tonSelect{get; set;}
    public String tocSelect{get; set;}
    public String toatmSelect{get; set;}
    public String toUnselect{get; set;}
    public String searchString{get; set;}
    public Boolean AccountRT{get; set;}
    public Boolean ContactRT{get; set;}
    public Boolean LeadRT{get; set;}
    public Boolean OpportunityRT{get; set;}
    public String UserID {get; set;}
    public Id recordtypeId {get;set;}
    public Call__c calls{get;set;}
    public Call__c callsc{get;set;}  //record type hardcoded in constructor SS(4/16/15) 
    public string rectype {get;set;}
    public string crecType {get;set;}  //RecordType Selectlist Value   BY(4/3/15)
    public CallVF__c cVar {get;set;}  //Call Page Variable Custom Setting  BY(4/3/15)
    public string warningMessage{get;set;} //Warning Message Variable BY(4/3/15)
    public string defaultLabel{get;set;} //Default Message Variable BY(4/3/15)
    public string warningRACF{get;set;} //Warning Racf message variable SS(6/10/15)
    public Boolean defaultRT {get;set;}  //Setting Default RT field
    public Call__c newCall = New Call__c(); //setting for save method
    
    Static Map<Id,user> usermap =new Map<Id,user>([Select Id, RACF_Number__c from user]);
    Set<String> userNames = new Set<String>();
    private Participant__c[] forDeletion = new Participant__c[]{};
    private ApexPages.StandardController controller;
    
    
    //Constructor
    global CallParticipantExtensionNew (ApexPages.StandardController controller) {
        this.controller= controller;
        if(controller.getrecord() == null)
        system.debug(controller.getrecord());
       
        
        //Variable Defaults BY(4/3/15)
        AccountRT = false;
        ContactRT = false;
        LeadRT = false;
        OpportunityRT = false;
        defaultRT = true;
        
        this.calls = (Call__c ) controller.getRecord();
        if(calls.AccountId__c != null) {
            system.debug(calls.id);
            system.debug(calls.AccountId__c);
            AtmRef = [select id, UserId,User.Isactive, AccountId from AccountTeamMember where AccountId =: calls.AccountId__c ]; 
            ConRef = [select Id,Name,RACFID__c,Contact_Type__c, AccountId,Title, Contact.MailingCity,Contact.MailingState,Contact.Account.Name from contact where AccountId =:calls.AccountId__c order by Name]; 
            
        }
        
        
        //Rec.type HC for Account/Contact/Lead/Opportunity calls SS(4/16/15)
        this.callsc = (Call__c ) controller.getRecord(); 
        if(callsc.AccountId__c != null)
        {
            system.debug(callsc.AccountId__c);
            crecType = keyGV.callRTAccount;
            AccountRT = true;
            defaultRT = false;
        
        }
        if(callsc.ContactId__c != null)
        {
            system.debug(callsc.ContactId__c);
            crecType = keyGV.callRTContact;
            ContactRT = true;
            defaultRT = false;
        
        }
        if(callsc.LeadId__c != null)
        {
            system.debug(callsc.LeadId__c);
            crecType = keyGV.callRTLead;
            LeadRT = true;
            defaultRT = false;
        
        }
        if(callsc.OpportunityId__c != null)
        {
            system.debug(callsc.OpportunityId__c);
            crecType = keyGV.callRTOpportunity;
            OpportunityRT = true;
            defaultRT = false;
        
        }
      
        
        //Search and display in Contact section
        
        {shoppingCart = [Select id,name,CallId__c,Contact_Id__c,Call_Date__c,Participant__c.Contact_Id__r.Name from participant__c where id =: tonSelect ];}
        
        updateAvailableList();
        
        //Populate Call Variable Custom Setting & get warning message BY(4/3/15)
        
        CallVF__c cVFcs = CallVF__c.getInstance(keyGV.vfCallNew);
        warningMessage = cVFcs.Warning_Prompt__c;
        defaultLabel = cVFcs.Default_Label__c;
        warningRACF = cVFcs.Warning_RACFID__c;
     }
     
     
    // Redirecting the page..
    /* public Pagereference redirect(){
        Integer flag=0;
        List<Redirect_Profile__c> l=Redirect_Profile__c.getall().values();
        Pagereference pr;
        for(Redirect_Profile__c p:l){
            Profile p1=[select id from Profile where name= :p.name];
            system.debug(Userinfo.getProfileId());
            if(Userinfo.getProfileId()==p1.id){
                flag=1;
            }    
        }    
        if(flag==1){
            pr=Page.CallCustomNewFX;
        }else if(flag!=1){
            PageReference newp = new PageReference('/apex/CallCustomNew');
        }
            system.debug(pr);
        
        return pr;
    } */
    
    //Build Recortype Select List BY(4/3/15)
    
    public list<SelectOption> callRTs {
    
    
        get {
            list<SelectOption> callRecordTypes = new list<selectOption>();
            //List<Redirect_Profile__c> l=Redirect_Profile__c.getall().values();
             
            callRecordTypes.add(new SelectOption('Default','-- Choose a Recordtype --'));
            
            for (RecordType r : [select Id,Name from RecordType where sobjecttype = 'Call__c' ])
                callRecordTypes.add(new selectOption(r.Name, r.Name));
                
            /*for(Redirect_Profile__c p:l)
            {
            Profile p1=[select id from Profile where name= :p.name];
            system.debug(Userinfo.getProfileId());
            if(Userinfo.getProfileId()==p1.id)
              {
                callRecordTypes.add(new selectOption('Account Call'));                    
              }  
            }*/ 
            return callRecordTypes;           
                     
          }
          set;
    }
    
  
    
    //Set RecordType Variables
    public void setRecordType(){
        
        //Set RTBoolean based on crecType
                
            //Default
            if(crecType == 'Default'){
                AccountRT = false;
                ContactRT = false;
                LeadRT = false;
                OpportunityRT = false;
                defaultRT = true;               
            }
            //Account
            if(crecType == keyGV.callRTAccount){
                AccountRT = true;
                ContactRT = false;
                LeadRT = false;
                OpportunityRT = false;
                defaultRT = false;              
            }
            //Contact
            if(crecType == keyGV.callRTContact){
                AccountRT = false;
                ContactRT = true;
                LeadRT = false;
                OpportunityRT = false;
                defaultRT = false;  
            }
            //Lead
            if(crecType == keyGV.callRTLead){
                AccountRT = false;
                ContactRT = false;
                LeadRT = true;
                OpportunityRT = false;
                defaultRT = false;  
            }
            //Opportunity
            if(crecType == keyGV.callRTOpportunity){
                AccountRT = false;
                ContactRT = false;
                LeadRT = false;
                OpportunityRT = true;
                defaultRT = false;  
            }
    }
      
    public void updateAvailableList() 
    {
      
        UserID = UserInfo.getUserId(); 
        String qString =  'select id, Name,RACFID__c,Contact_Type__c, Title, Contact.MailingCity,Contact.MailingState,Contact.Account.Name from Contact where User_Id__c not in (select ID from User where id =: UserID )' ;
        system.debug(qString);
        
        if(searchString!=null)
        
        {          
            qString+= ' and ( Contact.Name like \'%' + searchString + '%\' or Contact.RACFID__c like \'%' + searchString + '%\' or Contact.Officer_Code__c like \'%' + searchString + '%\') ';                       
        }
       
        
       Set<Id> selectedEntries = new Set<Id>();
       if(tonSelect!=null)
        for(Participant__c d : shoppingCart){
            selectedEntries.add(d.Contact_Id__c);
        }
        
        if(selectedEntries.size()>0){
            String tempFilter = ' and id not in (';
            for(id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        } 
        
        qString+= ' order by Name';
        qString+= ' limit 12';
        system.debug('qString:' +qString );
          if(searchString != null)
       {               
        AvailableParticipants = database.query(qString);
        system.debug(AvailableParticipants);
        }
    } 
    
    
    // This function runs when a user hits "select" button next to a Participant    
    
    public void addToShoppingCart()    
    { 
      for(Contact part : AvailableParticipants)
       {
        if(((String)part.id==tonSelect && part.RACFID__c != null) || ((String)part.id==tonSelect && part.Contact_Type__c == 'Business Contact'))
        
            {                 
                shoppingCart.add(new Participant__c (Contact_Id__c =part.id));
                system.debug(shoppingCart);
                system.debug('Participant select size' + shoppingCart.size());               
            }
        
        if((String)part.id==tonSelect && part.RACFID__c == null && part.Contact_Type__c == 'Key Employee')
            {
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF); 
                ApexPages.addMessage(errMsg);
            }
        }    
        updateAvailableList();  
    } 
    
    // This function runs when a user hits "select" button next to Account Team member
 
    public void AccountTeamMemberList()  
    {      
    for(AccountTeamMember partsatm : AtmRef)
         {
            system.debug(partsatm.UserId);
            if(partsatm.UserId != null)
            {
                User us = usermap.get(partsatm.UserId);
                userNames.add(us.RACF_Number__c);
                
                system.debug(us.RACF_Number__c);
            }
         }
         
          if(userNames.size() > 0) {
            //List<Contact> userNameContacts = [Select Id, RACFID__c,Salesforce_User__c,User_Id__r.Isactive from Contact where RACFID__c IN : userNames and Contact_Type__c='Key Employee' and Salesforce_User__c=true];
            List<Contact> userNameContacts = [Select Id, RACFID__c,Salesforce_User__c,User_Id__r.Isactive from Contact where RACFID__c IN : userNames and Contact_Type__c='Key Employee'];
            {
                for(AccountTeamMember partsatm : AtmRef)         
            {
                         
             User us = usermap.get(partsatm.UserId);
             system.debug(partsatm.UserId);
             system.debug(partsatm.User.Isactive);
              Contact foundContact;
                 for(Contact contact : userNameContacts)
                 {
                      if(contact.RACFID__c == us.RACF_Number__c)
                      {
                            foundContact = contact;
                            system.debug('found-'  +foundContact );
                            system.debug('RACF-' +foundContact.RACFID__c);                      
                      }                      
                 }
                
                 if(foundContact != null && partsatm.id==toatmSelect && partsatm.User.Isactive == true && foundContact.RACFID__c != null)
                 {
                      system.debug('If found-'  +foundContact );
                      system.debug('RACF inside-' +foundContact.RACFID__c);
                      Participant__c  partatm = new Participant__c();
                      partatm.Contact_Id__c = foundContact.Id;
                      partatm.CallId__c=newCall.id;
                      system.debug(partatm);
                      shoppingCart.add(partatm);                      
                 }
                 else if(foundContact != null && partsatm.id==toatmSelect && partsatm.User.Isactive == true && foundContact.RACFID__c == null)
                 {
                     system.debug('Else1 found-'  +foundContact );
                     ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF); 
                     ApexPages.addMessage(errMsg);
                 }
                 else if(foundContact != null && partsatm.id==toatmSelect && partsatm.User.Isactive == false && foundContact.RACFID__c != null)
                 {
                      system.debug('Else2 found-'  +foundContact );
                      ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The Selected User is Inactive'); 
                      ApexPages.addMessage(errMsg);   
                 }
                 else if(partsatm.id==toatmSelect && foundContact == null)
                 {
                     system.debug('Else3 found-'  +foundContact );
                     ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No KeyEmployee Contact For Selected User'); 
                     ApexPages.addMessage(errMsg);
                 } 
                 else
                 {
                 }       
        }
        
      updateAvailableList();  
           
    }
    
  }
  
 }
   
 // This function runs when a user hits "select" button next to Contact
 public void ContactList()
    {
        for(Contact parts : ConRef )
        {  
         
         if(((String)parts.id==tocSelect && parts.RACFID__c != null) || ((String)parts.id==tocSelect && parts.Contact_Type__c == 'Business Contact'))
            {
                shoppingCart.add(new participant__c(Contact_Id__c=parts.id));
                system.debug(shoppingCart.size());
                break;
            }
         
         if((String)parts.id==tocSelect && parts.RACFID__c == null && parts.Contact_Type__c == 'Key Employee')
            {
                ApexPages.Message errMsgs = new ApexPages.Message(ApexPages.Severity.ERROR,warningRACF); 
                ApexPages.addMessage(errMsgs);
            }             
        }
        
       updateAvailableList();  
        
    } 
 
 // This function runs when a user selects Account lookup field   
 
 public void InVFPage()
    {
        this.calls = (Call__c ) controller.getRecord(); 
        AtmRef = [select id, UserId,User.Isactive, AccountId,Account.Contact_Name__c from AccountTeamMember where AccountId =: calls.AccountId__c order by UserId];
        ConRef = [select Id,Name,RACFID__c,Contact_Type__c,  AccountId,Title, Contact.MailingCity,Contact.MailingState,Contact.Account.Name from contact where AccountId =:calls.AccountId__c order by Name];
    }   
        
// This function runs when a user hits "remove" on "Selected Participant" section
   
 public PageReference removeFromShoppingCart()
     {
        Integer count = 0;
    
        for(Participant__c del : shoppingCart){
            if((String)del.Contact_Id__c==toUnselect){
            
                if(del.Id!=null)
                    forDeletion.add(del);
            
                shoppingCart.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }
    
    //Save Method 
    public PageReference onSave()
    {
             Record_Type_Name__c Accrt = Record_Type_Name__c.getValues('AccRecordTypeID');
             Record_Type_Name__c Conrt = Record_Type_Name__c.getValues('ConRecordTypeID');
             Record_Type_Name__c Leart = Record_Type_Name__c.getValues('LeaRecordTypeID');
             Record_Type_Name__c Opprt = Record_Type_Name__c.getValues('OppRecordTypeID');     
             //PageReference pageRef = controller.save();
             this.calls = (Call__c ) controller.getRecord(); 
             
             if(crecType == keyGV.callRTAccount) {                 
                  newCall.AccountId__c = calls.AccountId__c;
                  newCall.RecordTypeId = Accrt.Record_Type__c;
                  newCall.Subject__c = calls.Subject__c ;
                  newCall.Call_Date__c = calls.Call_Date__c ;
                  newCall.Is_Private__c = calls.Is_Private__c ;
                  newCall.Status__c = calls.Status__c ;
                  newCall.Call_Type__c = calls.Call_Type__c ;
                  newCall.Joint_Call__c = calls.Joint_Call__c ;
                  newCall.Product__c= calls.Product__c;
                  newCall.Product_Category__c = calls.Product_Category__c;
                  newCall.Product_Family__c = calls.Product_Family__c;
                  newCall.Comments__c = calls.Comments__c;
                  newCall.Call_Location__c = calls.Call_Location__c; //FX related field
                  system.debug(newCall); 
                  }
             
             if(crecType == keyGV.callRTContact) {                 
                  newCall.ContactId__c = calls.ContactId__c;
                  newCall.RecordTypeId = Conrt.Record_Type__c;
                  newCall.Subject__c = calls.Subject__c ;
                  newCall.Call_Date__c = calls.Call_Date__c ;
                  newCall.Is_Private__c = calls.Is_Private__c ;
                  newCall.Status__c = calls.Status__c ;
                  newCall.Call_Type__c = calls.Call_Type__c ;
                  newCall.Joint_Call__c = calls.Joint_Call__c ;
                  newCall.Product__c= calls.Product__c;
                  newCall.Product_Category__c = calls.Product_Category__c;
                  newCall.Product_Family__c = calls.Product_Family__c;
                  newCall.Comments__c = calls.Comments__c;
                  newCall.Call_Location__c = calls.Call_Location__c; //FX related field
                  system.debug(newCall); 
                  }
                  
               if(crecType == keyGV.callRTLead) {                 
                  newCall.LeadId__c= calls.LeadId__c;
                  newCall.RecordTypeId = Leart.Record_Type__c;
                  newCall.Subject__c = calls.Subject__c ;
                  newCall.Call_Date__c = calls.Call_Date__c ;
                  newCall.Is_Private__c = calls.Is_Private__c ;
                  newCall.Status__c = calls.Status__c ;
                  newCall.Call_Type__c = calls.Call_Type__c ;
                  newCall.Joint_Call__c = calls.Joint_Call__c ;
                  newCall.Product__c= calls.Product__c;
                  newCall.Product_Category__c = calls.Product_Category__c;
                  newCall.Product_Family__c = calls.Product_Family__c;
                  newCall.Comments__c = calls.Comments__c;
                  newCall.Call_Location__c = calls.Call_Location__c; //FX related field
                  system.debug(newCall); 
                  } 
                  
                if(crecType == keyGV.callRTOpportunity) {                 
                  newCall.OpportunityId__c= calls.OpportunityId__c;
                  newCall.RecordTypeId = Opprt.Record_Type__c;
                  newCall.Subject__c = calls.Subject__c ;
                  newCall.Call_Date__c = calls.Call_Date__c ;
                  newCall.Is_Private__c = calls.Is_Private__c ;
                  newCall.Status__c = calls.Status__c ;
                  newCall.Call_Type__c = calls.Call_Type__c ;
                  newCall.Joint_Call__c = calls.Joint_Call__c ;
                  newCall.Product__c= calls.Product__c;
                  newCall.Product_Category__c = calls.Product_Category__c;
                  newCall.Product_Family__c = calls.Product_Family__c;
                  newCall.Comments__c = calls.Comments__c;
                  newCall.Call_Location__c = calls.Call_Location__c; //FX related field
                  system.debug(newCall); 
                  }  
                 try{            
         
                 insert newCall;
                 system.debug(newCall);
             
        
            if(shoppingCart.size()>0) 
                  
                  for (Participant__c partmember : shoppingCart ){
                     partmember.CallId__c=newCall.id;
                     System.debug(partmember.CallId__c);
                  }
                  
                  System.debug('size' +shoppingCart.size());
                  Set<Participant__c> nonDuplicates = new Set<Participant__c>(shoppingCart);  
                  system.debug(nonDuplicates);
                  List<Participant__c> listStrings = new List<Participant__c>(nonDuplicates);
                  system.debug(listStrings);
                  
                  insert(listStrings);
                  
                }
           
        
            catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
           System.debug('completed');

        // After save return the user to the Call
       return new PageReference('/' + newCall.id);  
    }     
}