global virtual with sharing class ECCP_CRINT_DefaultAppController {
    global ECCP_CRINT_DefaultAppController(ApexPages.StandardController controller) {
        //If an app parameter is not set,
        //set it to the value of the default app field
        if(String.isBlank(ApexPages.currentPage().getParameters().get(nFORCE.RouteController.PARAM_APP_REQUEST))) {
            if(!Test.isRunningTest()){
                controller.addFields(new String[]{ this.getDefaultAppField() });
            }
            if(controller.getRecord().get(getDefaultAppField()) == null){
                ApexPages.currentPage().getParameters().put(
                    nFORCE.RouteController.PARAM_APP_REQUEST,
                    translateDefaultValue(getDefaultAppDefaultValue(controller)));
            } else {
                ApexPages.currentPage().getParameters().put(
                    nFORCE.RouteController.PARAM_APP_REQUEST,
                    (String) controller.getRecord().get(this.getDefaultAppField()));
            }
        }
    }
    protected virtual String getDefaultAppField() {
        return sObjectType.LLC_BI__Loan__c.fields.ECCP_CRINT_App__c.getName();
    }
    private String getDefaultAppDefaultValue(ApexPages.StandardController controller){
        return nFORCE.DescribesCache.getInstance().cacheAllSObjectFields(
            controller.getRecord().getSObjectType()).get(getDefaultAppField()).getDefaultValueFormula();
    }
    private String translateDefaultValue(Object value){
        return String.valueOf(value).replace(QUOTE, EMPTY);
    }
    
    private static final String EMPTY = '';
    private static final String QUOTE = '\'';
}