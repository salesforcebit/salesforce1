public class callcountprogress1
{
public callcountprogress1(logindetailscontroller controller) {
}

/*Public List<Participant__c> thismonthcall{get;set;}
Public List<Participant__c> lastmonthcall{get;set;}
Integer count1 = 0;
Integer count2 = 0;
String callCountColor1{get;set;}
String callCountColor2{get;set;} */

public static Map<Id, Integer> UserCallCounts(Set<id> userIds, String timePeriod)
{
    system.debug('Inside Call Count Method');
    Map<Id, Integer> userIdVsCallCount = new Map<Id, Integer>();
    
    //List<Integer> count = new List<Integer>();
    Integer count = 0;
    
    String soqlQuery = 'select id, call_date__c, Contact_Id__r.User_Id__c from Participant__c WHERE Contact_Id__r.User_Id__c IN :userIds AND call_date__c = '+ timePeriod;        
       
        //count = database.countquery(soqlQuery);
      //  system.debug('***thismonth***' + thismonthcall);
    Map<Id,Integer> userIdVsCallCountList = new Map<Id,Integer>();
    
    Map<Id, List<Participant__c>> ParticipantMap = new Map<Id, List<Participant__c>>();
    for(List<Participant__c> thismonthcall: database.query(soqlQuery))
    for(Participant__c p : thismonthcall)
    {
        
        Id i = p.Contact_Id__r.User_Id__c;
        if(ParticipantMap.containsKey(i))
        {      
                 //List<Participant__c> plist = new List<Participant__c>();
                 List<Participant__c> plist = ParticipantMap.get(i);
                 plist.add(p);
                 ParticipantMap.put(i,plist);
        } 
        else
        {
                 List<Participant__c> plist = new List<Participant__c>();
                 plist.add(p);
                 ParticipantMap.put(i,plist);
        } 
        
        system.debug('**participant map**' + ParticipantMap);              
        
    } 
     for(Id currentUserId : ParticipantMap.keyset())
        {
            List<Participant__c> plistnew = ParticipantMap.get(currentUserId);
            Integer Countnew = 0;
            system.debug('**participant list**'+plistnew);
            for(Participant__c par1 : plistnew)
            {                              
                 Countnew = Countnew +1 ;        
                 system.debug('**count**' + Countnew); 
                             
            }
             userIdVsCallCountList.put(currentUserId, Countnew);   
             system.debug('**userIdVsCallCountList**' + userIdVsCallCountList);
         } 
             
      return userIdVsCallCountList;

}

}