public class ECCP_CreditBureau_WebSvcCallout{

    public static ECCP_CreditBureauResponse CreditBureauCallout(ECCP_CreditBureauRequest reqobj){
        ECCP_CreditBureausHelper helper = new ECCP_CreditBureausHelper();
        helper.ECCP_CreditBureauProcess(reqobj);    
        //res1= ECCP_CreditBureausHelper.ECCP_CreditBureauProcess(reqobj);
        ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port service = new ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port();
        service.endpoint_x  = 'http://api.salesforce.com/';
        //Creating Request
        ECCP_CreditbureauWS.ECCPCreditBureauRequest CBreq = new ECCP_CreditbureauWS.ECCPCreditBureauRequest();
        ECCP_CreditbureauWS.Customer_Type CBreq1 = new  ECCP_CreditbureauWS.Customer_Type();
        CBreq1.SSN = 'BNNPM45678';
        CBreq1.Orchestration_Code = '00015';
        CBreq1.AccountName = 'Test Account';
        CBreq1.BillingStreet = 'Domlur' ;       
        CBreq.Customer= CBreq1;
        ECCP_CreditbureauWS.ECCP_Credit_Bureau_Type CBres = new ECCP_CreditbureauWS.ECCP_Credit_Bureau_Type();
        CBres = service.getRequestOverHttp(CBreq1);
        system.debug('!!!!!!!!!!!!!!!!!!'+CBres);   
        ECCP_CreditBureauResponse CBresponse = new ECCP_CreditBureauResponse();
        CBresponse.SSN = CBres.SSN; 
        AsyncECCP_CreditbureauWS.AsyncWebServices_CreditBureauWS_Port async = new   AsyncECCP_CreditbureauWS.AsyncWebServices_CreditBureauWS_Port();
        Continuation con = new Continuation(60);
        async .beginGetRequestOverHttp(con,CBreq1 );                
        return CBresponse;
       }
}