@isTest
public class ECCP_ALBTestClass{
    public static List<LLC_BI__Loan__c>  lonList;
    public static void createData(){
         lonList = ECCP_CreateLoanUtil.CreateLoanData(1,1);
         Profile p = [SELECT Id FROM Profile WHERE Name='Integration/Data Migration']; 
         User u = new User(Alias = 'stdInt', Email='standarduser@testorg.comInt', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comInt',PLOB__c='Support');
        insert u;
        System.runAs(u){
             List<LLC_BI__Fee__c> feeList = new List<LLC_BI__Fee__c>();
             LLC_BI__Fee__c fee = new LLC_BI__Fee__c();
             fee.LLC_BI__Loan__c = lonList[0].Id;
             fee.LLC_BI__Fee_Code__c = '55';
             feeList.add(fee);
             insert feeList;
             
             List<LLC_BI__Collateral_Type__c> colTypeList = new List<LLC_BI__Collateral_Type__c>();
             LLC_BI__Collateral_Type__c colType = new LLC_BI__Collateral_Type__c();
             colType.Name ='Real Estate-Jr mortgage/deed of trust-commercial-804';
             colTypeList.add(colType);
             insert colTypeList;
             
             List<LLC_BI__Collateral__c> colList = new List<LLC_BI__Collateral__c>();
             LLC_BI__Collateral__c col = new LLC_BI__Collateral__c();
             col.LLC_BI__Collateral_Type__c = colTypeList[0].id;
             colList.add(col); 
             insert colList;
             List<LLC_BI__Account_Collateral__c> collOwnerList = new List<LLC_BI__Account_Collateral__c>();
             LLC_BI__Account_Collateral__c collOwner = new LLC_BI__Account_Collateral__c();
             collOwner.LLC_BI__Collateral__c = colList[0].Id;
             collOwner.LLC_BI__Account__c = lonList[0].LLC_BI__Account__c;
             collOwner.LLC_BI__Primary_Owner__c = True;
             collOwnerList.add(collOwner);
             insert collOwnerList;
             List<LLC_BI__Loan_Collateral_Aggregate__c> lonCollList = new List<LLC_BI__Loan_Collateral_Aggregate__c>();
             LLC_BI__Loan_Collateral_Aggregate__c lonColl = new LLC_BI__Loan_Collateral_Aggregate__c();
             lonColl.LLC_BI__lookupKey__c ='test';
             lonCollList.add(lonColl); 
             insert lonCollList;
              
             List<LLC_BI__Loan_Collateral2__c> colteralPledgeList = new List<LLC_BI__Loan_Collateral2__c>();
             LLC_BI__Loan_Collateral2__c colteralPledge = new LLC_BI__Loan_Collateral2__c();
             colteralPledge.LLC_BI__Loan__c = lonList[0].Id;
             colteralPledge.LLC_BI__Collateral__c =  colList[0].Id;
             colteralPledge.LLC_BI__Loan_Collateral_Aggregate__c = lonCollList[0].Id;
             colteralPledge.LLC_BI__Amount_Pledged__c = 250000 ;
             colteralPledge.LLC_BI__Lien_Position__c = '2nd';
             colteralPledge.LLC_BI__Active__c = True;
             colteralPledgeList.add(colteralPledge);
             insert colteralPledgeList;
             
             ECC_Loan_Rate_Term__c RateTerm = new ECC_Loan_Rate_Term__c();
             RateTerm.ECC_Amortization_Type__c ='Mortgage';
             RateTerm.ECC_Amortized_Term_Months__c = 5;
             RateTerm.Loan__c = lonList[0].id;
             insert RateTerm;
             
             ECCP_Prepaymt_PenaltyCode_Xref__c XREF = new ECCP_Prepaymt_PenaltyCode_Xref__c();
             XREF.Prepayment_Code__c = '3456';
             XREF.Prepayment_Type__c = 'Manual';
             insert XREF;
             
             ECC_Disbursement_Information__c DI = new ECC_Disbursement_Information__c();
             DI.ECC_Loan__c = lonList[0].id;
             Insert DI;
             
             ECCP_Credit_Bureau__c Cb = new ECCP_Credit_Bureau__c();
             cb.Loan__c = lonList[0].id;
             insert cb;
             
             ECCP_Compliance__c Cm = new ECCP_Compliance__c();
             cm.Loan_Name__c = lonList[0].id;
             insert cm;
             
             LLC_BI__Covenant_Type__c covType = new LLC_BI__Covenant_Type__c();
             covType.Covenant_Code__c = 'code1';
             insert covType;
             
             LLC_BI__Covenant2__c cov = new LLC_BI__Covenant2__c();
             cov.LLC_BI__Last_Evaluation_Status__c = 'Compliant';
             cov.LLC_BI__Last_Evaluation_Date__c = System.Today() ; 
             cov.LLC_BI__Frequency__c ='Annually';
             cov.LLC_BI__Covenant_Type__c = covType.id;
             insert cov;
             
             LLC_BI__Loan_Covenant__c  LonCov = new LLC_BI__Loan_Covenant__c ();
             LonCov.LLC_BI__Loan__c =  lonList[0].id;
             LonCov.LLC_BI__Covenant2__c = cov.id;
             insert LonCov ;
             
        }
        
    }
    public static testMethod void ALBInboundCallTest(){
        
        ECCP_ALBTestClass.createData();
        String nullInput;
        system.debug('----lonList[0].id--'+lonList[0].Id);
        List<LLC_BI__Loan__c> lonDataQ = [select ECC_APP_ID__c from LLC_BI__Loan__c where id = : lonList[0].Id];
        String str = lonDataQ[0].ECC_APP_ID__c;
        system.debug('---str---'+str);
        test.startTest();
        
        ECCP_ALB_Inbound_Stubb.ALBInboundCall(str);
        // For exception case
        try{
        ECCP_ALB_Inbound_Stubb.ALBInboundCall(nullInput);
        }
        Catch(Exception ex){
        
        }
        test.stopTest();
    }
    
    public static testMethod void LPLCallTest(){
        
        ECCP_ALBTestClass.createData();
       
        List<String> lonId = new List<String>();
        lonId.add(lonList[0].Id);
        system.debug('----lonList[0].id--'+lonList[0].Id);
        
        
        test.startTest();
        
        ECCP_LPL_FutureHandler.LPLRequestQueryHandler(lonId);
        // For exception case
       // ECCP_LPL_FutureHandler.LPLRequestQueryHandler(nullInput);
        test.stopTest();
    } 
    
}