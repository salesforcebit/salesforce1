@isTest
private class ScenarioRecordTypeCtrl_UT {
	
	@isTest static void ddaTest() {
		// Implement test code
		Opportunity opp = [SELECT id, Name, AccountId FROM Opportunity WHERE Name='TestOppty'];

		ScenarioRecordTypeCtrl ctr = new ScenarioRecordTypeCtrl (new ApexPages.StandardController(opp) );
		ctr.SC.DDA_Proforma__c  = true;
		ctr.saveAndContinue();

		LLC_BI__Scenario__c scen = [SELECT id FROM LLC_BI__Scenario__c WHERE LLC_BI__Opportunity__c =:opp.id];

		System.assertNotEquals(null, scen);
	}

	@isTest static void relationshipTest() {
		// Implement test code
		Opportunity opp = [SELECT id, Name, AccountId FROM Opportunity WHERE Name='TestOppty'];

		ScenarioRecordTypeCtrl ctr = new ScenarioRecordTypeCtrl (new ApexPages.StandardController(opp) );
		ctr.SC.Relationship_Proforma__c = true;
		ctr.saveAndContinue();

		LLC_BI__Scenario__c scen = [SELECT id FROM LLC_BI__Scenario__c WHERE LLC_BI__Opportunity__c =:opp.id];

		System.assertNotEquals(null, scen);
	}

	@isTest static void errorTestBothTrue() {
		// Implement test code
		Opportunity opp = [SELECT id, Name, AccountId FROM Opportunity WHERE Name='TestOppty'];

		ScenarioRecordTypeCtrl ctr = new ScenarioRecordTypeCtrl (new ApexPages.StandardController(opp) );
		ctr.SC.Relationship_Proforma__c = true;
		ctr.SC.DDA_Proforma__c  = true;
		ctr.saveAndContinue();

		System.assertEquals(1, ApexPages.getMessages().size());
	}

	@isTest static void errorTestBothFalse() {
		// Implement test code
		Opportunity opp = [SELECT id, Name, AccountId FROM Opportunity WHERE Name='TestOppty'];

		ScenarioRecordTypeCtrl ctr = new ScenarioRecordTypeCtrl (new ApexPages.StandardController(opp) );
		//ctr.SC.Relationship_Proforma__c = true;
		//ctr.SC.DDA_Proforma__c  = true;
		ctr.saveAndContinue();

		System.assertEquals(1, ApexPages.getMessages().size());
	}
	
	@testSetup static void setupMethod() {
		// Implement test code
		LLC_BI__Deposit__c da = new LLC_BI__Deposit__c();
        da.Name = 'TestDA2';
        da.Bank_Number__c = '1234';
        da.LLC_BI__Account_Number__c = '999999';
        insert da;
		TSO_Cost_Center__c tcc = new TSO_Cost_Center__c();
		tcc.Charge_Class__c = 'CN';
		insert tcc;
		Account acc = new Account();
		acc.Name = 'TestAccount';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		acc.Cost_Center__c = tcc.Id;
        acc.Billing_Account_Number__c = da.Id;
		acc.Bank_Number__c = da.Id;
		acc.Delivery_Method__c = 'I';
		insert acc;
		LLC_BI__Deposit__c testDA  = [SELECT Id, Name FROM LLC_BI__Deposit__c limit 1];
		// testDA.LLC_BI__Account_Number__c = '5792874523';
		// insert testDA;
		Opportunity oppty = new Opportunity();
		oppty.Name = 'TestOppty';
		oppty.AccountId = acc.Id;
		oppty.CloseDate = System.Today();
		oppty.StageName = 'Pursue';
		oppty.Opportunity_Type__c = 'Addl Product';
		insert oppty;
		Product2 SFprod = UnitTestFactory.buildTestProduct();
		SFprod.Name = 'Scenario Test';
		SFprod.Family = 'Deposits & ECP';
		insert SFprod;
		LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
		insert pl;
		LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
		insert pt;
		LLC_BI__Product_Feature__c pf = new LLC_BI__Product_Feature__c();
		pf.LLC_BI__Is_Bundle__c = true;
		insert pf;
		LLC_BI__Product__c prod = new LLC_BI__Product__c();
		prod.Name = 'TestProd';
		prod.Related_SFDC_Product__c = SFprod.Id;
		prod.LLC_BI__Product_Feature__c = pf.Id;
		prod.LLC_BI__Product_Type__c = pt.Id;
		insert prod;
		LLC_BI__Opportunity_Product__c op = new LLC_BI__Opportunity_Product__c();
		op.LLC_BI__Opportunity__c = oppty.Id;
		op.LLC_BI__Product__c = prod.Id;
		insert op;
		LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c();
		ts.Name = 'TestTS';
		ts.Request_Type__c = 'New';
		ts.LLC_BI__Stage__c = 'Order Entry';
		ts.LLC_BI__Product_Reference__c = prod.Id;
		ts.LLC_BI__Relationship__c = acc.Id;
		insert ts;
		LLC_BI__Bill_Point__c bp = new LLC_BI__Bill_Point__c();
		bp.Name = 'TestBillPoint';
		bp.LLC_BI__Product__c = prod.Id;
        bp.LLC_BI__lookupKey__c = 'DDAPOSP1PCN-0';
		//insert bp;
		LLC_BI__Bill_Point__c bp2 = new LLC_BI__Bill_Point__c();
		bp2.Name = 'TestBillPoint';
		bp2.LLC_BI__Product__c = prod.Id;
        bp2.LLC_BI__lookupKey__c = 'DDAPOSP1PCN-0';
		insert new List<LLC_BI__Bill_Point__c>{bp, bp2};
		TSO_Volumes_and_Prices__c vp = new TSO_Volumes_and_Prices__c();
		vp.Name = 'TestVP';
		vp.Product__c = prod.Id;
		vp.Treasury_Service__c = ts.Id;
		vp.Bill_Point__c = bp2.Id;
		insert vp;
	}
	
}