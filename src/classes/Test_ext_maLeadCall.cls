@istest
Public class Test_ext_maLeadCall 
{
     public static testMethod void  ext_maLeadCallTest()
    {
        Profile p = [select id from profile where name='Bank Admin'];
       User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan@keybank.com',
       emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
       localesidkey='en_US', profileid = p.Id, country='United States',
       timezonesidkey='America/Los_Angeles',  username='vidhyasagaran_muralidharan8@keybank.com',PLOB__c='support',PLOB_Alias__c='support'); 
       insert testUser;
            
        Account acc = new Account(name ='Acc Test');
        insert acc;
       Lead lea = New Lead(FirstName='test',LastName='test',company='et');
        insert lea;
        contact con = new contact(LastName='Mr. Dhanesh Subramanian',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',AccountId=acc.id,Salesforce_User__c=true,User_Id__c=testUser.id,Phone='9876543210');
        insert con;

        Call__c  call = new Call__c();
        call.Subject__c = 'Test';
        call.Call_Type__c = 'BD Monthly Update';
        call.Call_Date__c = system.today();
        call.Is_Private__c = TRUE;
        call.Status__c = 'DONE';
        call.Comments__c = 'sample test for test class';
        call.Product_Family__c='test';
        call.Product_Category__c='test';
        call.Product__c='test';
        call.ContactId__c=con.id;
        insert call;
       //custom setting insertion
        CallVF__c cVF = new CallVF__c(name='CallCustomNew',Default_Label__c='Please select RecordType',Warning_Prompt__c='NOTE: Please enter mandatory fields before adding Participants from the below search list.',Warning_RACFID__c='RACF-ID is Empty for last selected participant.');
        insert cVF;
        callSelect_Status__c cs = new callSelect_Status__c(name='CANCELLED',Status__c='CANCELLED');
        insert cs;
         callSelect_Type__c cst= new callSelect_Type__c(name='BD Monthly Update',Type__c='BD Monthly Update');
         insert cst;
         
        ApexPages.StandardController sc = new ApexPages.StandardController(lea);
        ext_maLeadCall  e = new  ext_maLeadCall (sc);

        e.createCall();
        List<SelectOption> cStatus = e.cStatusList;
        system.assert(cStatus.size()>0);
        List<SelectOption> cType = e.cTypeList;
        List<SelectOption> pFamily = e.pFamilyList;
        List<SelectOption> pCategory = e.pCategoryList;
        List<SelectOption> Prod = e.ProductList;
        
        e.searchString='Dhanesh';
        Set<Id> selectedEntries = new Set<Id>();
        e.updateAvailableList();
        e.tonSelect = con.id;
        e.addToShoppingCart();
        e.toUnselect= con.id;
        e.removeFromShoppingCart();
        e.cComments=call.Comments__c;
        e.cProductFamily=call.Product_Family__c;
        e.cProductCategory=call.Product_Category__c;
        e.cProduct=call.Product__c;
        e.Save();
       Participant__c part =new Participant__c(CallId__c=call.id,Contact_Id__c=con.id);
        insert part;  

    }
}