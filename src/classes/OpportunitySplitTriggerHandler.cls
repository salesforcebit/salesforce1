public class OpportunitySplitTriggerHandler 
{
    private static boolean run = true;
    
    public static void OSupdate(list<OpportunitySplit> tid) {
        try{
            List<OpportunitySplit> osdml = new list<OpportunitySplit>();
            List<OpportunitySplit> OppSplit = new list<OpportunitySplit>();
            Map<Id,Id> osid = new Map<Id,Id>();
            // List <OpportunitySplit> OppSplit = [SELECT ID, OpportunityId,SplitPercentage, SplitownerID FROM OpportunitySplit where Id =: tid and Opportunity_Record_Type__c = true];
            for(OpportunitySplit os : tid){
                if(os.Id != null && os.Opportunity_Record_Type__c == true){
                    osid.put(os.Id, Id.valueof(os.OpportunityId));       
                }
            }
            
            List<OpportunitySplit> OppSplits = new List<OpportunitySplit>();
            //if(!Test.isRunningTest()){
                OppSplits = [SELECT ID,SplitPercentage, OpportunityId, SplitownerID, primary__c FROM OpportunitySplit where OpportunityId in :osid.values() and Id not in :tid and Opportunity_Record_Type__c = true];
            //}
           /* else {
                for(OpportunitySplit os : tid){
                    OppSplits.add(os);
                }
            }*/
            // List<Opportunity> Oppsall = [SELECT ID,OwnerId FROM Opportunity where Id in :osid.values()];
            
            Integer i= 0;
            //i = [Select count() from OpportunitySplit where OpportunityId in :osid.values() and primary__c=true and Opportunity_Record_Type__c = true];     
            for(OpportunitySplit os : tid){
                if(os.primary__c == true){
                   i++; 
                }
            }
            for(OpportunitySplit os : tid){
               for (OpportunitySplit o : OppSplits){
                  //  Integer j= [Select count() from OpportunitySplit where OpportunityId in :osid.values() and primary__c=true and Opportunity_Record_Type__c = true];
                    if(o.primary__c == true && i >= 1)
                    {
                        system.debug(os.Id);
                        system.debug(o.id);
                        o.primary__c=false; 
                        osdml.add(o);                  
                    }
                                            
                }
                
             }
             
            system.debug(osdml);
            update osdml;
        }
        catch(DMLException e)
        {
            system.debug(e.getMessage());
        }  
    }
    
  
    /*public static boolean runOnce()
    {
        if(run)
        {
         run=false;
         return true;
        }else
        {
         return run;
        }
    }*/    
    
    
}