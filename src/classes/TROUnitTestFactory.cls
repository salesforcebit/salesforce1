@isTest
public class TROUnitTestFactory {

	/////////////////////////Begin Add Acount ////////////////////////////////////

	public static TROAddAccountRequest_v2.ExportFields buildTestExportFields(){
		TROAddAccountRequest_v2.ExportFields exportFields = new TROAddAccountRequest_v2.ExportFields();
		exportfields.ExportFields = TROUnitTestFactory.buildTestExportFields2();

		return exportfields;
	}

	public static TROAddAccountRequest_v2.ExportFields2 buildTestExportFields2(){
		TROAddAccountRequest_v2.ExportFields2 exportFields = new TROAddAccountRequest_v2.ExportFields2();
		exportfields.Internet = TROUnitTestFactory.buildTestInternet();
		exportfields.InetAcct = new List<TROAddAccountRequest_v2.InetAcct>{TROUnitTestFactory.buildTestInetAcct()};
		exportfields.InetIR = new List<TROAddAccountRequest_v2.InetIR>{TROUnitTestFactory.buildTestInetIR()};
		exportfields.InetUserProfile = new List<TROAddAccountRequest_v2.InetUserProfile>{TROUnitTestFactory.buildTestInetUserProfile()};

		return exportfields;

	}

	public static TROAddAccountRequest_v2.Internet buildTestInternet(){
		TROAddAccountRequest_v2.Internet internet = new TROAddAccountRequest_v2.Internet();

		return internet;
	}
	public static TROAddAccountRequest_v2.InetUserProfile buildTestInetUserProfile(){
		TROAddAccountRequest_v2.InetUserProfile profile = new TROAddAccountRequest_v2.InetUserProfile();
		return profile;
	}
	public static TROAddAccountRequest_v2.InetIR buildTestInetIR(){
		TROAddAccountRequest_v2.InetIR inet = new TROAddAccountRequest_v2.InetIR();
		return inet;
	}
	public static TROAddAccountRequest_v2.InetAcct buildTestInetAcct(){
		TROAddAccountRequest_v2.InetAcct acct = new TROAddAccountRequest_v2.InetAcct();
		return acct;
	}

	public static TROAddAccountRequest_v2.processAddAccountAPIResponse buildTestAddAccountResponse(){
		TROAddAccountRequest_v2.processAddAccountAPIResponse response = new TROAddAccountRequest_v2.processAddAccountAPIResponse();
		response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();

		return response;
	}

	public static TROAddAccountRequest_v2.KTTResponse buildTestKTTResponse(){
		TROAddAccountRequest_v2.KTTResponse response = new TROAddAccountRequest_v2.KTTResponse();
		response.CSOAutomation = TROUnitTestFactory.buildTestCSOAutomation();

		return response;
	}

	public static TROAddAccountRequest_v2.CSOAutomation buildTestCSOAutomation(){
		TROAddAccountRequest_v2.CSOAutomation auto = new TROAddAccountRequest_v2.CSOAutomation();
		auto.KTTResponse = TROUnitTestFactory.buildTestKTTResponse2();

		return auto;
	}

	public static TROAddAccountRequest_v2.KTTResponse2 buildTestKTTResponse2(){
		TROAddAccountRequest_v2.KTTResponse2 response = new TROAddAccountRequest_v2.KTTResponse2();
        response.status = 'timeout error';
		response.successMsg = new List<TROAddAccountRequest_v2.successMsg>{TROUnitTestFactory.buildTestSuccessMessage()};
		response.errorMsg = new List<TROAddAccountRequest_v2.errorMsg>{TROUnitTestFactory.buildTestErrorMessage()};
		response.userDetails = new List<TROAddAccountRequest_v2.userDetails>{TROUnitTestFactory.buildTestUserDetails()};
		return response;
	}

	public static TROAddAccountRequest_v2.errorMsg buildTestErrorMessage(){
		TROAddAccountRequest_v2.errorMsg errorMsg = new TROAddAccountRequest_v2.errorMsg();
		errorMsg.errorDescription = 'Add Account error';
		return errorMsg;
	}

	public static TROAddAccountRequest_v2.successMsg buildTestSuccessMessage(){
		TROAddAccountRequest_v2.successMsg successMsg = new TROAddAccountRequest_v2.successMsg();
		successMsg.successDescription = 'Add Account Success';
		return successMsg;
	}

	public static TROAddAccountRequest_v2.userDetails buildTestUserDetails(){
		TROAddAccountRequest_v2.userDetails userDetails = new TROAddAccountRequest_v2.userDetails();
		//userDetails.userId = 'TEST100';
        //userDetails.pwdEventStatus = 'true';
        //userDetails.userIDEventStatus = 'true';
		return userDetails;
	}

/////////////////////////END Add Acount ////////////////////////////////////
/////////////////////////Begin Add Service ////////////////////////////////////

	public static TROAddServiceRequest_v2.ExportFields buildTest_AddService_ExportFields(){
		TROAddServiceRequest_v2.ExportFields exportFields = new TROAddServiceRequest_v2.ExportFields();
		exportfields.ExportFields = TROUnitTestFactory.buildTest_AddService_ExportFields2();

		return exportfields;
	}

	public static TROAddServiceRequest_v2.ExportFields2 buildTest_AddService_ExportFields2(){
		TROAddServiceRequest_v2.ExportFields2 exportFields = new TROAddServiceRequest_v2.ExportFields2();
		exportfields.Internet = TROUnitTestFactory.buildTest_AddService_Internet();
		exportfields.InetAcct = new List<TROAddServiceRequest_v2.InetAcct>{TROUnitTestFactory.buildTest_AddService_InetAcct()};
		exportfields.InetIR = new List<TROAddServiceRequest_v2.InetIR>{TROUnitTestFactory.buildTest_AddService_InetIR()};
		exportfields.InetUserProfile = new List<TROAddServiceRequest_v2.InetUserProfile>{TROUnitTestFactory.buildTest_AddService_InetUserProfile()};

		return exportfields;

	}

	public static TROAddServiceRequest_v2.Internet buildTest_AddService_Internet(){
		TROAddServiceRequest_v2.Internet internet = new TROAddServiceRequest_v2.Internet();

		return internet;
	}
	public static TROAddServiceRequest_v2.InetUserProfile buildTest_AddService_InetUserProfile(){
		TROAddServiceRequest_v2.InetUserProfile profile = new TROAddServiceRequest_v2.InetUserProfile();
		return profile;
	}
	public static TROAddServiceRequest_v2.InetIR buildTest_AddService_InetIR(){
		TROAddServiceRequest_v2.InetIR inet = new TROAddServiceRequest_v2.InetIR();
		return inet;
	}
	public static TROAddServiceRequest_v2.InetAcct buildTest_AddService_InetAcct(){
		TROAddServiceRequest_v2.InetAcct acct = new TROAddServiceRequest_v2.InetAcct();
		return acct;
	}

	public static TROAddServiceRequest_v2.processAddServiceAPIResponse buildTestAddServiceResponse(){
		TROAddServiceRequest_v2.processAddServiceAPIResponse response = new TROAddServiceRequest_v2.processAddServiceAPIResponse();
		response.addServiceResponse = TROUnitTestFactory.buildTest_AddService_KTTResponse();

		return response;
	}

	public static TROAddServiceRequest_v2.KTTResponse buildTest_AddService_KTTResponse(){
		TROAddServiceRequest_v2.KTTResponse response = new TROAddServiceRequest_v2.KTTResponse();
		response.CSOAutomation = TROUnitTestFactory.buildTest_AddService_CSOAutomation();

		return response;
	}

	public static TROAddServiceRequest_v2.CSOAutomation buildTest_AddService_CSOAutomation(){
		TROAddServiceRequest_v2.CSOAutomation auto = new TROAddServiceRequest_v2.CSOAutomation();
		auto.KTTResponse = TROUnitTestFactory.buildTest_AddService_KTTResponse2();

		return auto;
	}

	public static TROAddServiceRequest_v2.KTTResponse2 buildTest_AddService_KTTResponse2(){
		TROAddServiceRequest_v2.KTTResponse2 response = new TROAddServiceRequest_v2.KTTResponse2();
        response.status = 'success';
        response.errorCode = '100';
		response.successMsg = new List<TROAddServiceRequest_v2.successMsg>{TROUnitTestFactory.buildTest_AddService_SuccessMessage()};
		response.errorMsg = new List<TROAddServiceRequest_v2.errorMsg>{TROUnitTestFactory.buildTest_AddService_ErrorMessage()};
		response.userDetails = new List<TROAddServiceRequest_v2.userDetails>{TROUnitTestFactory.buildTest_AddService_UserDetails()};
		return response;
	}

	public static TROAddServiceRequest_v2.errorMsg buildTest_AddService_ErrorMessage(){
		TROAddServiceRequest_v2.errorMsg errorMsg = new TROAddServiceRequest_v2.errorMsg();
		errorMsg.errorDescription = 'Add Service Error';
		return errorMsg;
	}

	public static TROAddServiceRequest_v2.successMsg buildTest_AddService_SuccessMessage(){
		TROAddServiceRequest_v2.successMsg successMsg = new TROAddServiceRequest_v2.successMsg();

		return successMsg;
	}

	public static TROAddServiceRequest_v2.userDetails buildTest_AddService_UserDetails(){
		TROAddServiceRequest_v2.userDetails userDetails = new TROAddServiceRequest_v2.userDetails();

		return userDetails;
	}

/////////////////////////End Add Service ////////////////////////////////////
/////////////////////////Begin Add User ////////////////////////////////////

	public static TROAddUserRequest_v2.ExportFields buildTest_AddUser_ExportFields(){
		TROAddUserRequest_v2.ExportFields exportFields = new TROAddUserRequest_v2.ExportFields();
		exportfields.ExportFields = TROUnitTestFactory.buildTest_AddUser_ExportFields2();

		return exportfields;
	}

	public static TROAddUserRequest_v2.ExportFields2 buildTest_AddUser_ExportFields2(){
		TROAddUserRequest_v2.ExportFields2 exportFields = new TROAddUserRequest_v2.ExportFields2();
		exportfields.Internet = TROUnitTestFactory.buildTest_AddUser_Internet();
		exportfields.InetAcct = new List<TROAddUserRequest_v2.InetAcct>{TROUnitTestFactory.buildTest_AddUser_InetAcct()};
		exportfields.InetIR = new List<TROAddUserRequest_v2.InetIR>{TROUnitTestFactory.buildTest_AddUser_InetIR()};
		exportfields.InetUserProfile = new List<TROAddUserRequest_v2.InetUserProfile>{TROUnitTestFactory.buildTest_AddUser_InetUserProfile()};

		return exportfields;

	}

	public static TROAddUserRequest_v2.Internet buildTest_AddUser_Internet(){
		TROAddUserRequest_v2.Internet internet = new TROAddUserRequest_v2.Internet();

		return internet;
	}
	public static TROAddUserRequest_v2.InetUserProfile buildTest_AddUser_InetUserProfile(){
		TROAddUserRequest_v2.InetUserProfile profile = new TROAddUserRequest_v2.InetUserProfile();
		return profile;
	}
	public static TROAddUserRequest_v2.InetIR buildTest_AddUser_InetIR(){
		TROAddUserRequest_v2.InetIR inet = new TROAddUserRequest_v2.InetIR();
		return inet;
	}
	public static TROAddUserRequest_v2.InetAcct buildTest_AddUser_InetAcct(){
		TROAddUserRequest_v2.InetAcct acct = new TROAddUserRequest_v2.InetAcct();
		return acct;
	}

	public static TROAddUserRequest_v2.processAddUserAPIResponse buildTestAddUserResponse(){
		TROAddUserRequest_v2.processAddUserAPIResponse response = new TROAddUserRequest_v2.processAddUserAPIResponse();
		response.addUserResponse = TROUnitTestFactory.buildTest_AddUser_KTTResponse();

		return response;
	}

	public static TROAddUserRequest_v2.KTTResponse buildTest_AddUser_KTTResponse(){
		TROAddUserRequest_v2.KTTResponse response = new TROAddUserRequest_v2.KTTResponse();
		response.CSOAutomation = TROUnitTestFactory.buildTest_AddUser_CSOAutomation();

		return response;
	}

	public static TROAddUserRequest_v2.CSOAutomation buildTest_AddUser_CSOAutomation(){
		TROAddUserRequest_v2.CSOAutomation auto = new TROAddUserRequest_v2.CSOAutomation();
		auto.KTTResponse = TROUnitTestFactory.buildTest_AddUser_KTTResponse2();

		return auto;
	}

	public static TROAddUserRequest_v2.KTTResponse2 buildTest_AddUser_KTTResponse2(){
		TROAddUserRequest_v2.KTTResponse2 response = new TROAddUserRequest_v2.KTTResponse2();
        response.status = 'partial';
		response.successMsg = new List<TROAddUserRequest_v2.successMsg>{TROUnitTestFactory.buildTest_AddUser_SuccessMessage()};
		response.errorMsg = new List<TROAddUserRequest_v2.errorMsg>{TROUnitTestFactory.buildTest_AddUser_ErrorMessage()};
		response.userDetails = new List<TROAddUserRequest_v2.userDetails>{TROUnitTestFactory.buildTest_AddUser_UserDetails()};
		return response;
	}

	public static TROAddUserRequest_v2.errorMsg buildTest_AddUser_ErrorMessage(){
		TROAddUserRequest_v2.errorMsg errorMsg = new TROAddUserRequest_v2.errorMsg();
		errorMsg.errorDescription = 'Add Account error';
		return errorMsg;
	}

	public static TROAddUserRequest_v2.successMsg buildTest_AddUser_SuccessMessage(){
		TROAddUserRequest_v2.successMsg successMsg = new TROAddUserRequest_v2.successMsg();
		
		successMsg.successDescription = 'Add Account Success';
		return successMsg;
	}

	public static TROAddUserRequest_v2.userDetails buildTest_AddUser_UserDetails(){
		TROAddUserRequest_v2.userDetails userDetails = new TROAddUserRequest_v2.userDetails();

		return userDetails;
	}

/////////////////////////END Add User ////////////////////////////////////
/////////////////////////Begin Delete User ////////////////////////////////////

	public static TRODeleteUserRequest_v2.ExportFields buildTest_DeleteUser_ExportFields(){
		TRODeleteUserRequest_v2.ExportFields exportFields = new TRODeleteUserRequest_v2.ExportFields();
		exportfields.ExportFields = TROUnitTestFactory.buildTest_DeleteUser_ExportFields2();

		return exportfields;
	}

	public static TRODeleteUserRequest_v2.ExportFields2 buildTest_DeleteUser_ExportFields2(){
		TRODeleteUserRequest_v2.ExportFields2 exportFields = new TRODeleteUserRequest_v2.ExportFields2();
		exportfields.Internet = TROUnitTestFactory.buildTest_DeleteUser_Internet();
		exportfields.InetAcct = new List<TRODeleteUserRequest_v2.InetAcct>{TROUnitTestFactory.buildTest_DeleteUser_InetAcct()};
		exportfields.InetIR = new List<TRODeleteUserRequest_v2.InetIR>{TROUnitTestFactory.buildTest_DeleteUser_InetIR()};
		exportfields.InetUserProfile = new List<TRODeleteUserRequest_v2.InetUserProfile>{TROUnitTestFactory.buildTest_DeleteUser_InetUserProfile()};

		return exportfields;

	}

	public static TRODeleteUserRequest_v2.Internet buildTest_DeleteUser_Internet(){
		TRODeleteUserRequest_v2.Internet internet = new TRODeleteUserRequest_v2.Internet();

		return internet;
	}
	public static TRODeleteUserRequest_v2.InetUserProfile buildTest_DeleteUser_InetUserProfile(){
		TRODeleteUserRequest_v2.InetUserProfile profile = new TRODeleteUserRequest_v2.InetUserProfile();
		return profile;
	}
	public static TRODeleteUserRequest_v2.InetIR buildTest_DeleteUser_InetIR(){
		TRODeleteUserRequest_v2.InetIR inet = new TRODeleteUserRequest_v2.InetIR();
		return inet;
	}
	public static TRODeleteUserRequest_v2.InetAcct buildTest_DeleteUser_InetAcct(){
		TRODeleteUserRequest_v2.InetAcct acct = new TRODeleteUserRequest_v2.InetAcct();
		return acct;
	}

	public static TRODeleteUserRequest_v2.processDeleteUserAPIResponse buildTestDeleteUserResponse(){
		TRODeleteUserRequest_v2.processDeleteUserAPIResponse response = new TRODeleteUserRequest_v2.processDeleteUserAPIResponse();
		response.deleteUserResponse = TROUnitTestFactory.buildTest_DeleteUser_KTTResponse();

		return response;
	}

	public static TRODeleteUserRequest_v2.KTTResponse buildTest_DeleteUser_KTTResponse(){
		TRODeleteUserRequest_v2.KTTResponse response = new TRODeleteUserRequest_v2.KTTResponse();
		response.CSOAutomation = TROUnitTestFactory.buildTest_DeleteUser_CSOAutomation();

		return response;
	}

	public static TRODeleteUserRequest_v2.CSOAutomation buildTest_DeleteUser_CSOAutomation(){
		TRODeleteUserRequest_v2.CSOAutomation auto = new TRODeleteUserRequest_v2.CSOAutomation();
		auto.KTTResponse = TROUnitTestFactory.buildTest_DeleteUser_KTTResponse2();

		return auto;
	}

	public static TRODeleteUserRequest_v2.KTTResponse2 buildTest_DeleteUser_KTTResponse2(){
		TRODeleteUserRequest_v2.KTTResponse2 response = new TRODeleteUserRequest_v2.KTTResponse2();
        response.status = 'error';
        response.errorCode = '200';
		response.successMsg = new List<TRODeleteUserRequest_v2.successMsg>{TROUnitTestFactory.buildTest_DeleteUser_SuccessMessage()};
		response.errorMsg = new List<TRODeleteUserRequest_v2.errorMsg>{TROUnitTestFactory.buildTest_DeleteUser_ErrorMessage()};
		response.userDetails = new List<TRODeleteUserRequest_v2.userDetails>{TROUnitTestFactory.buildTest_DeleteUser_UserDetails()};
		return response;
	}

	public static TRODeleteUserRequest_v2.errorMsg buildTest_DeleteUser_ErrorMessage(){
		TRODeleteUserRequest_v2.errorMsg errorMsg = new TRODeleteUserRequest_v2.errorMsg();
		
		errorMsg.errorDescription = 'Add Account error';
		return errorMsg;
	}

	public static TRODeleteUserRequest_v2.successMsg buildTest_DeleteUser_SuccessMessage(){
		TRODeleteUserRequest_v2.successMsg successMsg = new TRODeleteUserRequest_v2.successMsg();
		successMsg.successDescription = 'Add Account Success';
		return successMsg;
	}

	public static TRODeleteUserRequest_v2.userDetails buildTest_DeleteUser_UserDetails(){
		TRODeleteUserRequest_v2.userDetails userDetails = new TRODeleteUserRequest_v2.userDetails();

		return userDetails;
	}

/////////////////////////END Delete User ////////////////////////////////////
/////////////////////////BEGIN New Setup ////////////////////////////////////
	public static TRONewSetupRequest_v2.ExportFields buildTest_NewSetup_ExportFields(){
		TRONewSetupRequest_v2.ExportFields exportFields = new TRONewSetupRequest_v2.ExportFields();
		exportfields.ExportFields = TROUnitTestFactory.buildTest_NewSetup_ExportFields2();

		return exportfields;
	}

	public static TRONewSetupRequest_v2.ExportFields2 buildTest_NewSetup_ExportFields2(){
		TRONewSetupRequest_v2.ExportFields2 exportFields = new TRONewSetupRequest_v2.ExportFields2();
		exportfields.Internet = TROUnitTestFactory.buildTest_NewSetup_Internet();
		exportfields.InetAcct = new List<TRONewSetupRequest_v2.InetAcct>{TROUnitTestFactory.buildTest_NewSetup_InetAcct()};
		exportfields.InetIR = new List<TRONewSetupRequest_v2.InetIR>{TROUnitTestFactory.buildTest_NewSetup_InetIR()};
		exportfields.InetUserProfile = new List<TRONewSetupRequest_v2.InetUserProfile>{TROUnitTestFactory.buildTest_NewSetup_InetUserProfile()};

		return exportfields;

	}

	public static TRONewSetupRequest_v2.Internet buildTest_NewSetup_Internet(){
		TRONewSetupRequest_v2.Internet internet = new TRONewSetupRequest_v2.Internet();

		return internet;
	}
	public static TRONewSetupRequest_v2.InetUserProfile buildTest_NewSetup_InetUserProfile(){
		TRONewSetupRequest_v2.InetUserProfile profile = new TRONewSetupRequest_v2.InetUserProfile();
		return profile;
	}
	public static TRONewSetupRequest_v2.InetIR buildTest_NewSetup_InetIR(){
		TRONewSetupRequest_v2.InetIR inet = new TRONewSetupRequest_v2.InetIR();
		return inet;
	}
	public static TRONewSetupRequest_v2.InetAcct buildTest_NewSetup_InetAcct(){
		TRONewSetupRequest_v2.InetAcct acct = new TRONewSetupRequest_v2.InetAcct();
		return acct;
	}

    public static TRONewSetupRequest_v2.processNewSetUpAPIResponse buildTestNewSetupResponse(){
		TRONewSetupRequest_v2.processNewSetUpAPIResponse response = new TRONewSetupRequest_v2.processNewSetUpAPIResponse();
        response.newSetupResponse = TROUnitTestFactory.buildTest_NewSetup_KTTResponse();

		return response;
	}

	public static TRONewSetupRequest_v2.KTTResponse buildTest_NewSetup_KTTResponse(){
		TRONewSetupRequest_v2.KTTResponse response = new TRONewSetupRequest_v2.KTTResponse();
        
		response.CSOAutomation = TROUnitTestFactory.buildTest_NewSetup_CSOAutomation();

		return response;
	}

	public static TRONewSetupRequest_v2.CSOAutomation buildTest_NewSetup_CSOAutomation(){
		TRONewSetupRequest_v2.CSOAutomation auto = new TRONewSetupRequest_v2.CSOAutomation();
		auto.KTTResponse = TROUnitTestFactory.buildTest_NewSetup_KTTResponse2();

		return auto;
	}

	public static TRONewSetupRequest_v2.KTTResponse2 buildTest_NewSetup_KTTResponse2(){
		TRONewSetupRequest_v2.KTTResponse2 response = new TRONewSetupRequest_v2.KTTResponse2();
        response.status = 'success';
        //response.errorCode = '200';
		response.successMsg = new List<TRONewSetupRequest_v2.successMsg>{TROUnitTestFactory.buildTest_NewSetup_SuccessMessage()};
		response.errorMsg = new List<TRONewSetupRequest_v2.errorMsg>{TROUnitTestFactory.buildTest_NewSetup_ErrorMessage()};
		response.userDetails = new List<TRONewSetupRequest_v2.userDetails>{TROUnitTestFactory.buildTest_NewSetup_UserDetails()};
		return response;
	}

	public static TRONewSetupRequest_v2.errorMsg buildTest_NewSetup_ErrorMessage(){
		TRONewSetupRequest_v2.errorMsg errorMsg = new TRONewSetupRequest_v2.errorMsg();
		
        errorMsg.errorDescription = 'Errorcode';
		return errorMsg;
	}

	public static TRONewSetupRequest_v2.successMsg buildTest_NewSetup_SuccessMessage(){
		TRONewSetupRequest_v2.successMsg successMsg = new TRONewSetupRequest_v2.successMsg();
		successMsg.successDescription = 'New Setup Success';
		return successMsg;
	}

	public static TRONewSetupRequest_v2.userDetails buildTest_NewSetup_UserDetails(){
		TRONewSetupRequest_v2.userDetails userDetails = new TRONewSetupRequest_v2.userDetails();
		/*userDetails.pwdEventStatus = 'SUCCESS';
        userDetails.userIDEventStatus = 'SUCCESS';
        userDetails.userId ='test100';*/
		return userDetails;
	}

/////////////////////////END New Setup /////////////////////////////////////
////////////////BEGIN Change Service User Level ////////////////////////////
	public static TROChangeServiceUserLevelRequest_v2.ExportFields buildTest_ChangeUser_ExportFields(){
		TROChangeServiceUserLevelRequest_v2.ExportFields exportFields = new TROChangeServiceUserLevelRequest_v2.ExportFields();
		exportfields.ExportFields = TROUnitTestFactory.buildTest_ChangeUser_ExportFields2();

		return exportfields;
	}

	public static TROChangeServiceUserLevelRequest_v2.ExportFields2 buildTest_ChangeUser_ExportFields2(){
		TROChangeServiceUserLevelRequest_v2.ExportFields2 exportFields = new TROChangeServiceUserLevelRequest_v2.ExportFields2();
		exportfields.Internet = TROUnitTestFactory.buildTest_ChangeUser_Internet();
		exportfields.InetAcct = new List<TROChangeServiceUserLevelRequest_v2.InetAcct>{TROUnitTestFactory.buildTest_ChangeUser_InetAcct()};
		exportfields.InetIR = new List<TROChangeServiceUserLevelRequest_v2.InetIR>{TROUnitTestFactory.buildTest_ChangeUser_InetIR()};
		exportfields.InetUserProfile = new List<TROChangeServiceUserLevelRequest_v2.InetUserProfile>{TROUnitTestFactory.buildTest_ChangeUser_InetUserProfile()};

		return exportfields;

	}

	public static TROChangeServiceUserLevelRequest_v2.Internet buildTest_ChangeUser_Internet(){
		TROChangeServiceUserLevelRequest_v2.Internet internet = new TROChangeServiceUserLevelRequest_v2.Internet();

		return internet;
	}
	public static TROChangeServiceUserLevelRequest_v2.InetUserProfile buildTest_ChangeUser_InetUserProfile(){
		TROChangeServiceUserLevelRequest_v2.InetUserProfile profile = new TROChangeServiceUserLevelRequest_v2.InetUserProfile();
		return profile;
	}
	public static TROChangeServiceUserLevelRequest_v2.InetIR buildTest_ChangeUser_InetIR(){
		TROChangeServiceUserLevelRequest_v2.InetIR inet = new TROChangeServiceUserLevelRequest_v2.InetIR();
		return inet;
	}
	public static TROChangeServiceUserLevelRequest_v2.InetAcct buildTest_ChangeUser_InetAcct(){
		TROChangeServiceUserLevelRequest_v2.InetAcct acct = new TROChangeServiceUserLevelRequest_v2.InetAcct();
		return acct;
	}

	public static TROChangeServiceUserLevelRequest_v2.processCSULAPIResponse buildTestChangeUserResponse(){
		TROChangeServiceUserLevelRequest_v2.processCSULAPIResponse response = new TROChangeServiceUserLevelRequest_v2.processCSULAPIResponse();
		response.processCSULResponse = TROUnitTestFactory.buildTest_ChangeUser_KTTResponse();

		return response;
	}

	public static TROChangeServiceUserLevelRequest_v2.KTTResponse buildTest_ChangeUser_KTTResponse(){
		TROChangeServiceUserLevelRequest_v2.KTTResponse response = new TROChangeServiceUserLevelRequest_v2.KTTResponse();
		response.CSOAutomation = TROUnitTestFactory.buildTest_ChangeUser_CSOAutomation();

		return response;
	}

	public static TROChangeServiceUserLevelRequest_v2.CSOAutomation buildTest_ChangeUser_CSOAutomation(){
		TROChangeServiceUserLevelRequest_v2.CSOAutomation auto = new TROChangeServiceUserLevelRequest_v2.CSOAutomation();
		auto.KTTResponse = TROUnitTestFactory.buildTest_ChangeUser_KTTResponse2();

		return auto;
	}

	public static TROChangeServiceUserLevelRequest_v2.KTTResponse2 buildTest_ChangeUser_KTTResponse2(){
		TROChangeServiceUserLevelRequest_v2.KTTResponse2 response = new TROChangeServiceUserLevelRequest_v2.KTTResponse2();
		response.status = 'partial';
        response.successMsg = new List<TROChangeServiceUserLevelRequest_v2.successMsg>{TROUnitTestFactory.buildTest_ChangeUser_SuccessMessage()};
		response.errorMsg = new List<TROChangeServiceUserLevelRequest_v2.errorMsg>{TROUnitTestFactory.buildTest_ChangeUser_ErrorMessage()};
		response.userDetails = new List<TROChangeServiceUserLevelRequest_v2.userDetails>{TROUnitTestFactory.buildTest_ChangeUser_UserDetails()};
		return response;
	}

	public static TROChangeServiceUserLevelRequest_v2.errorMsg buildTest_ChangeUser_ErrorMessage(){
		TROChangeServiceUserLevelRequest_v2.errorMsg errorMsg = new TROChangeServiceUserLevelRequest_v2.errorMsg();
		
errorMsg.errorDescription = 'Add Account error';
		return errorMsg;
	}

	public static TROChangeServiceUserLevelRequest_v2.successMsg buildTest_ChangeUser_SuccessMessage(){
		TROChangeServiceUserLevelRequest_v2.successMsg successMsg = new TROChangeServiceUserLevelRequest_v2.successMsg();
		
successMsg.successDescription = 'Add Account Success';
		return successMsg;
	}

	public static TROChangeServiceUserLevelRequest_v2.userDetails buildTest_ChangeUser_UserDetails(){
		TROChangeServiceUserLevelRequest_v2.userDetails userDetails = new TROChangeServiceUserLevelRequest_v2.userDetails();

		return userDetails;
	}

////////////////END Change Service User Level //////////////////////////////
/////////////////////////BEGIN Event Capture ////////////////////////////////////


public static AsyncTROEventCapture.receiveEV00023Response_elementFuture buildTestprocessEvent1ResponseFuture() {
	AsyncTROEventCapture.receiveEV00023Response_elementFuture response = new AsyncTROEventCapture.receiveEV00023Response_elementFuture();
	//response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();
	return response;
}

public static AsyncTROEventCapture.receiveEV00022Response_elementFuture buildTestprocessEvent2ResponseFuture() {
	AsyncTROEventCapture.receiveEV00022Response_elementFuture response = new AsyncTROEventCapture.receiveEV00022Response_elementFuture();
	//response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();
	return response;
}

public static AsyncTROEventCapture.receiveEV00021Response_elementFuture buildTestprocessEvent3ResponseFuture() {
	AsyncTROEventCapture.receiveEV00021Response_elementFuture response = new AsyncTROEventCapture.receiveEV00021Response_elementFuture();
	//response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();
	return response;
}

/////////////////////////END Event Capture ////////////////////////////////////
//////////////////////// BEGIN ASYNC ////////////////////////////////////

	public static AsyncTROAddAccountRequest_v2.processAddAccountAPIResponseFuture buildTestProcessAddAccountAPIResponseFuture() {
		AsyncTROAddAccountRequest_v2.processAddAccountAPIResponseFuture response = new AsyncTROAddAccountRequest_v2.processAddAccountAPIResponseFuture();
		//response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();
		return response;
	}

	public static AsyncTROAddServiceRequest_v2.processAddServiceAPIResponseFuture buildTestProcessAddServiceAPIResponseFuture() {
		AsyncTROAddServiceRequest_v2.processAddServiceAPIResponseFuture response = new AsyncTROAddServiceRequest_v2.processAddServiceAPIResponseFuture();
		//response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();
		return response;
	}

	public static AsyncTROAddUserRequest_v2.processAddUserAPIResponseFuture buildTestprocessAddUserAPIResponseFuture() {
		AsyncTROAddUserRequest_v2.processAddUserAPIResponseFuture response = new AsyncTROAddUserRequest_v2.processAddUserAPIResponseFuture();
		//response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();
		return response;
	}

	public static AsyncTRODeleteUserRequest_v2.processDeleteUserAPIResponseFuture buildTestprocessDeleteUserAPIResponseFuture() {
		AsyncTRODeleteUserRequest_v2.processDeleteUserAPIResponseFuture response = new AsyncTRODeleteUserRequest_v2.processDeleteUserAPIResponseFuture();
		//response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();
		return response;
	}

	public static AsyncTROChangeServiceUserLevelRequest_v2.processCSULAPIResponseFuture buildTestprocessChangeUserAPIResponseFuture() {
		AsyncTROChangeServiceUserLevelRequest_v2.processCSULAPIResponseFuture response = new AsyncTROChangeServiceUserLevelRequest_v2.processCSULAPIResponseFuture();
		//response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();
		return response;
	}

	public static AsyncTRONewSetupRequest_v2.processNewSetUpAPIResponseFuture buildTestprocessNewSetUpAPIResponseFuture() {
		AsyncTRONewSetupRequest_v2.processNewSetUpAPIResponseFuture response = new AsyncTRONewSetupRequest_v2.processNewSetUpAPIResponseFuture();
		//response.addAccountResponse = TROUnitTestFactory.buildTestKTTResponse();
		return response;
	}

//////////////////////// END ASYNC ////////////////////////////////////

}