@isTest
private class OpportunityLineItemTriggerHandler_UT {
	
	static testMethod void testDeleteOpportunityLineItemForDepositsOpportunity() {
		
		Opportunity testOpportunity = UnitTestFactory.buildTestDepositsECPOpportunity();
		insert testOpportunity;
		
		Product2 testProduct = UnitTestFactory.buildTestProduct();
		testProduct.Family = 'Deposits & ECP';
		insert testProduct;
		
		LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
		insert testProductLine;
		
		LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
		insert testProductType;
		
		LLC_BI__Product__c testNCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
		insert testNCinoProduct;
		
		PricebookEntry testPricebookEntry = UnitTestFactory.buildTestPricebookEntry(testProduct);
		insert testPricebookEntry;
		
		OpportunityLineItem testOpportunityLineItem = UnitTestFactory.buildTestOpportunityLineItem(testOpportunity, testPricebookEntry);
		insert testOpportunityLineItem;
		
		//System.assertEquals(1, [Select count() from LLC_BI__Opportunity_Product__c]);
		
		test.startTest();
			delete testOpportunityLineItem;
		test.stopTest();
		
		System.assertEquals(0, [Select count() from LLC_BI__Opportunity_Product__c]);
	}
	
	static testMethod void testDeleteOpportunityLineItemForDepositsOpportunityBulk() {
		
		Integer testSize = 2;
		
		List<Opportunity> testOpportunities = new List<Opportunity>();
		for(Integer i = 0; i < testSize; i++) {
			Opportunity testOpportunity = UnitTestFactory.buildTestDepositsECPOpportunity();
			testOpportunities.add(testOpportunity);
		}
		insert testOpportunities;
		
		Product2 testProduct = UnitTestFactory.buildTestProduct();
		testProduct.Family = 'Deposits & ECP';
		insert testProduct;
		
		LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
		insert testProductLine;
		
		LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
		insert testProductType;
		
		LLC_BI__Product__c testNCinoProduct = UnitTestFactory.buildTestNCinoProduct(testProduct, testProductType);
		insert testNCinoProduct;
		
		PricebookEntry testPricebookEntry = UnitTestFactory.buildTestPricebookEntry(testProduct);
		insert testPricebookEntry;
		
		List<OpportunityLineItem> testOpportunityLineItems = new List<OpportunityLineItem>();
		for(Integer i = 0; i < testSize; i++) {
			OpportunityLineItem testOpportunityLineItem = UnitTestFactory.buildTestOpportunityLineItem(testOpportunities[i], testPricebookEntry);
			testOpportunityLineItems.add(testOpportunityLineItem);
		}
		insert testOpportunityLineItems;
		
		//System.assertEquals(testSize, [Select count() from LLC_BI__Opportunity_Product__c]);
		
		test.startTest();
			delete testOpportunityLineItems;
		test.stopTest();
		
		System.assertEquals(0, [Select count() from LLC_BI__Opportunity_Product__c]);
	}
}