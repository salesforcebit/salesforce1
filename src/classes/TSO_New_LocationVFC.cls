public class TSO_New_LocationVFC {

    public Flow.Interview.TSO_New_Location flow {get;set;}
    
    public PageReference getTreasuryServiceView() {
        if(flow != null) {
            return new PageReference('/' + flow.getVariableValue('TSRecordID'));
        }
        else return new PageReference('/');
    }
    
    public TSO_New_LocationVFC(ApexPages.StandardController std) {
        
    }
}