@isTest 
private class testContactRoleExtension{
    static testMethod void testContactRole() {
       //Insert Account,contact & contactRole
       Account acc= new Account();
       acc.name='Test account';
       insert acc;
       Contact con= new Contact();
       con.firstname='test'; 
       con.lastname='Contact';
       con.accountID=acc.id;
       con.contact_type__C='Business Contact';
       con.Phone='9876543210';
       insert con;
       AccountContactRole obj = new AccountContactRole();
       obj.accountid=acc.id;
       obj.contactid=con.id;
       obj.IsPrimary=true;
       obj.Role='Board Member';
       insert obj;
       AccountContactRole obj1 = new AccountContactRole();
       obj1.accountid=acc.id;
       obj1.contactid=con.id;
       obj1.IsPrimary=true;
       obj1.Role='Attorney';
       insert obj1;
       
    
         // Retrieve contactRole
       List<AccountContactRole> contactRoleAcc = new List<AccountContactRole>();
       contactRoleAcc =[SELECT Account.Name,ContactId,IsPrimary,Role FROM AccountContactRole where contactId = :con.id];
       ApexPages.StandardController controller = new ApexPages.StandardController(con); 
       ContactRoleExtension   cre = new ContactRoleExtension(controller);
       
        cre.getAccountRecords();

       // Test that the trigger correctly updated the price
       
    }
}