/*
This class is written for KEF KRR Project by TCS Team

It is a handler class which is invoked from Program object's Trigger
*/

public with sharing class KEFProgramhandler 
{

    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    private static Map<String, KEFReportFieldPopulator> statusValueMap;
    
    public KEFProgramhandler(boolean isExecuting, integer size)
    {
        m_isExecuting = isExecuting;
        BatchSize = size;
        
        /*added below code block for Req 148 */
        //populating the Map for Status value changes functionality
        if(statusValueMap == null)
        { 
            statusValueMap = new Map<String, KEFReportFieldPopulator>();
            //preparing a map with status value as key and related wrapper field mapping as value
            statusValueMap = KEFProgramHelper.populateMapForStatusChangeAudit();
        }
        /*Req 148 code block end */
    } 
     
    public void OnBeforeInsert(Program__c[] triggerNewList)
    {
        if(triggerNewList != null && triggerNewList.size() > 0)
        {   
            for(Program__c prog : triggerNewList)
            {
                if(prog.Program_Status__c != null)
                {
                    //method for populating the Status date and user who added the Status
                    /* below code block is written for Req. 148 - reports and dashboards */
                    KEFProgramHelper.processProgramforDRDataPopulation(prog, statusValueMap);                   
                    /*Req 148 code block end */ 
                }
            }
        }
    }
    
    public void OnAfterInsert(Program__c[] triggerNewList, Map<ID, Program__c> triggerNewMap)
    {
        
         if(triggerNewList!= null && triggerNewList.size()>0)
         {
               KEFProgramHelper.UpdateNotesAndAttachment(triggerNewList);
         }    
    }
    /*
    @future public static void OnAfterInsertAsync(Set<ID> newAgreementIDs)
    {
        
    }*/
    
    public void OnBeforeUpdate(Program__c[] triggerOldList, Program__c[] triggerNewList, Map<ID, Program__c> triggerNewMap)
    {
        if(triggerNewMap != null && !triggerNewMap.isEmpty() && triggerOldList != null && triggerOldList.size() > 0)
        {   
            for(Program__c oldProg : triggerOldList)
            {
                Program__c newProg = triggerNewMap.get(oldProg.id);
                 
                if(newProg.Program_Status__c != null && newProg.Program_Status__c != oldProg.Program_Status__c)
                {
                    /* below code block is written for Req. 148 - reports and dashboards */
                    KEFProgramHelper.processProgramforDRDataPopulation(newProg, statusValueMap);                    
                    /*Req 148 code block end */
                }
            }
        }
    }
    //Method for changing ProgramName which will update the AgreementName   
    public void OnAfterUpdate(Program__c[] triggerOldList, Program__c[] triggerNewList, Map<ID, Program__c> triggerNewMap)
    {
    
     
    
       if(triggerNewMap != null && !triggerNewMap.isEmpty() && triggerOldList != null && triggerOldList.size() > 0)
       {
        //Map<Id,Set<Id>> NameUpdate = new Map<Id,Set<Id>>();
            Set<id> pgmsets= new Set<Id>();
            for(Program__c oldpgm : triggerOldList)
            {
                Program__c newpgm = triggerNewMap.get(oldpgm.id);
                
                if(oldpgm.Name_of_the_Vendor_Program__c != newpgm.Name_of_the_Vendor_Program__c)
                {
                    pgmsets.add(newpgm.Id);
                }             
            }
            
            //modifying the Agreement name for children agreements under a program, if errors occurs on any the same will be returned back 
            //to this map
            Map<Id, String> programErrorMap = KEFProgramHelper.UpdateAgreementNameBasedonProgramName(pgmsets);
            
            system.debug('programErrorMap-->     '+programErrorMap);
                        
            if((programErrorMap != null && !programErrorMap.isEmpty()) )
            {
                for(Id progId : programErrorMap.keyset())
                {
                    if(triggerNewMap.containskey(progId))
                    {
                        Program__c progRec = triggerNewMap.get(progId);
                        String errorMsg = programErrorMap.get(progId);
                        progRec.addError(errorMsg);
                    }
                }                       
            }
        }
    }
    /*
    @future public static void OnAfterUpdateAsync(Set<ID> updatedAgreementIDs)
    {
    
    }*/
    
    public void OnBeforeDelete(Program__c[] triggerOldList, Map<ID, Program__c> triggerOldMap)
    {
        
    }
    
    public void OnAfterDelete(Program__c[] triggerOldList, Map<ID, Program__c> triggerOldMap)
    {
        
    }
    /*
    @future public static void OnAfterDeleteAsync(Set<ID> deletedAgreementIDs)
    {
        
    }
    */
    public void OnUndelete(Program__c[] triggerNewList)
    {
        
    }
    

}