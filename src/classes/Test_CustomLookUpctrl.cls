@isTest
public class Test_CustomLookUpctrl{
 
    static Call__c callObj;
    static Account accObj;
    static Contact conObj;
    Static User userObj;
    
    static TestMethod void customLookUpctrlmethod(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        userObj = new User(alias = 'testcu',
            email='User_Test_class@test.com',
            emailencodingkey='UTF-8',
            lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = p.ID,
            PLOB__c = 'Support',
            timezonesidkey='America/Los_Angeles',
            username='User_Test_class@test.com');
        
        insert userObj;
        
        System.runAs(userObj){
            Map<String, Schema.SObjectType> accsObjectMap = Schema.getGlobalDescribe() ;
            Schema.SObjectType accs = accsObjectMap.get('Account') ;
            Schema.DescribeSObjectResult resSchemaAcc = accs.getDescribe() ;
            Map<String,Schema.RecordTypeInfo> accRecordTypeInfo = resSchemaAcc.getRecordTypeInfosByName();
            Id accRTId = accRecordTypeInfo.get('Customer').getRecordTypeId();
            System.Debug('accRTId:'+accRTId);
            
            accObj = new Account();
            accObj.name = 'Test Account';
            accObj.recordtypeId = accRTId;
            insert accObj;
            
            Map<String, Schema.SObjectType> callsObjectMap = Schema.getGlobalDescribe() ;
            Schema.SObjectType calls = callsObjectMap.get('Call__c') ; 
            Schema.DescribeSObjectResult resSchemaCall = calls.getDescribe() ;
            Map<String,Schema.RecordTypeInfo> callRecordTypeInfo = resSchemaCall.getRecordTypeInfosByName(); 
            Id callRTId = callRecordTypeInfo.get('Account Call').getRecordTypeId();
            System.Debug('callRTId:'+callRTId);
            
            callObj = new Call__c();
            callObj.Call_Type__c = 'COI Contact';
            callObj.recordTypeId = callRTId;
            callObj.Call_Date__c = Date.Today();
            callObj.Is_Private__c = true;
            callObj.Subject__c = 'Test Call';
            callObj.Status__c = 'DONE';
            insert callObj;
            
            Map<String, Schema.SObjectType> consObjectMap = Schema.getGlobalDescribe() ;
            Schema.SObjectType cons = consObjectMap.get('Contact') ;
            Schema.DescribeSObjectResult resSchemaCon = cons.getDescribe() ;
            Map<String,Schema.RecordTypeInfo> conRecordTypeInfo = resSchemaCon.getRecordTypeInfosByName();
            Id conRTId = conRecordTypeInfo.get('Key Employee').getRecordTypeId();
            System.Debug('conRTId:'+conRTId);
            
            conObj = new Contact(); 
            conObj.firstName = 'Test';
            conObj.LastName = 'User Contact';
            conObj.Active__c = true;
            conObj.Salesforce_User__c = true;
            conObj.User_Id__c = userObj.Id;
            conObj.recordtypeId = conRTId;
            conObj.Phone='9876543210';
            insert conObj;
            Test.startTest();
            
            PageReference pageRef = new PageReference('/contactCustomLkUpPage');
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('newContactName', '');  
            System.currentPageReference().getParameters().put('namefield', 'Component.mainPG:frmMain:newUserpb:pbsNewCnt:pbsiNewCnt:contNm');  
            System.currentPageReference().getParameters().put('idfield', 'Component.mainPG:frmMain:newUserpb:pbsNewCnt:pbsiNewCnt:contactID');
            ApexPages.StandardController stdCtrl = new ApexPages.Standardcontroller(callObj);
            customLookUpctrl ctrlObj = new customLookUpctrl(stdCtrl);
            ctrlObj.getcontactList();
            
            conObj = [SELECT id, Name, firstName,LastName, Active__c,Salesforce_User__c FROM Contact WHERE id = :conObj.Id limit 1];
            pageRef = new PageReference('/contactCustomLkUpPage');
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('newContactName', conObj.Name);  
            System.currentPageReference().getParameters().put('namefield', 'Component.mainPG:frmMain:newUserpb:pbsNewCnt:pbsiNewCnt:contNm');  
            System.currentPageReference().getParameters().put('idfield', 'Component.mainPG:frmMain:newUserpb:pbsNewCnt:pbsiNewCnt:contactID');
            stdCtrl = new ApexPages.Standardcontroller(callObj);
            ctrlObj = new customLookUpctrl(stdCtrl);
            ctrlObj.getcontactList();
            ctrlObj.getUserList();
            ctrlObj.getPageSize();
            ctrlObj.getTotalPages();
            ctrlObj.first();
            ctrlObj.last();
            ctrlObj.previous();
            ctrlObj.next();
            
            pageRef = new PageReference('/UserCustomLookUpPage');
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('userName', userObj.id);  
            System.currentPageReference().getParameters().put('namefield', 'Component.mainPG:frmMain:pbTop:pbsOldUser:pbsiOldCnt:userName');  
            System.currentPageReference().getParameters().put('idfield', 'Component.mainPG:frmMain:pbTop:pbsOldUser:pbsiOldCnt:UserSelID');
            stdCtrl = new ApexPages.Standardcontroller(callObj);
            ctrlObj = new customLookUpctrl(stdCtrl);
            ctrlObj.getUserList();
            ctrlObj.SearchUserRecord();
            System.assert(ctrlObj.hasNext != null);
            System.assert(ctrlObj.hasPrevious!= null);
            System.assert(ctrlObj.pageNumber != null);
            Test.stopTest();
        }
    } 
}