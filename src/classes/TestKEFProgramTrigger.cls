/*
This class is written for KEF KRR Project by TCS Team

It is a test class for KEF Program object's Trigger
*/

@isTest
private class TestKEFProgramTrigger 
{

    static testMethod void TestKEFPrgmtrigger()
    {
        Program__c pgm=TestUtilityClass.CreateTestProgram();
        insert pgm;
        
        Agreement__c agg=TestUtilityClass.CreateTestAgreement();
        agg.Agreement_Name_UDF__c='DellVendor';
        agg.Agreement_Type__c=System.label.other;
        agg.Default_lessor__c=System.Label.KEF;
        agg.program__c=pgm.id;
        insert agg;
        
        pgm.Name_of_the_Vendor_Program__c='DellVendor';
        pgm.Program_Name_Change_Reason__c='Adversk Risk';
        update pgm;
        
        Program__c pgm1=new Program__c();
        pgm1.Program_Status__c=System.Label.Prospect;
        pgm1.Name_of_the_Vendor_Program__c='Testprogram1';
        pgm1.Business_Development_Stage__c=System.label.BuisnessStage;
        insert pgm1;
        
        Agreement__c agg1=new Agreement__c();
        agg1.Agreement_Name_UDF__c=' ';
        agg1.Agreement_Type__c=System.label.other;
        agg1.Default_lessor__c=System.Label.KEF;
        agg1.program__c=pgm1.id;
        insert agg1;
        
        pgm1.Name_of_the_Vendor_Program__c='DellVendor Test';
        pgm1.Program_Name_Change_Reason__c='Adversk Risk';
        //addded below for REq. 148
        pgm1.Program_Status__c = System.Label.ActiveHuntingLicense;
        pgm1.Hunting_License_Next_Review__c = System.Today().addDays(10);
        update pgm1;
        
        system.assertEquals(UserInfo.getUserid(), [SELECT User_ActiveHuntingLicense__c FROM Program__c where Id =: pgm1.id].User_ActiveHuntingLicense__c);     
    }
    
   /* static testMethod void TestKEFPrgmtriggerExceptionCheck()
    {
        DateTime nowDT=System.now();
        String formatted=nowDT.format('MM/dd/yyyy');
        Profile p = [SELECT Id FROM Profile WHERE Name=:System.label.KEF_HUB_Owner]; 
        User use = new User(Alias = 'HubO', Email='standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', ProfileId = p.Id,PLOB__c='support',PLOB_Alias__c='support', 
                            TimeZoneSidKey='America/Los_Angeles', UserName='test@keybank.com'+ ''+formatted);
        insert use;
        
        User creditUser = TestUtilityClass.createUser(System.label.KEF_Credit);
        insert creditUser;
        
        User sysAdminUser = TestUtilityClass.createUser(System.label.System_Administrator);
        insert sysAdminUser;
        
        //use = [SELECT Id FROM User WHERE Alias = 'HubO' AND Email='standarduser@testorg.com'];
        Account accRec= new Account();
        System.runAs(use)
        {
            //List<Account> accList = TestUtilityClass.CreateTestAccount(1);
            //insert accList;
            
            for(Account acc : TestUtilityClass.CreateTestAccount(1))
            {
                accRec = acc;
            }
            insert accRec;
            
            Funding_Grid__c fg = TestUtilityClass.createFundingGrid(accRec.id);
            insert fg;
           
            //accRec = accList[0];
            accRec.BillingStreet = 'BillingStreet';
            accRec.BillingCity = 'BillingCity';
            accRec.BillingState = 'BillingState';
            accRec.BillingPostalCode = 'BillingPostalCode';
            accRec.BillingCountry = 'BillingCountry';
            accRec.Phone = '7030357222'; 
            accRec.Vendor_status__c=System.label.Pending_Credit;
            accRec.Business_Structure__c='TestBuisness';
            accRec.Credit_Analyst_Approving_Vendor__c=creditUser.id;//Userinfo.getuserid();
            accRec.KEF_Risk_Role__c=System.Label.Vendor_Supplier;
            update accRec;
        }
        
        system.runAs(sysAdminUser)
        {        
            accRec.Vendor_Status__c = System.Label.Active;
            update accRec;
        }

        System.runAs(use)
        {       
                    
            Program__c pgm=TestUtilityClass.CreateTestProgram();
            pgm.Name_of_the_Vendor_Program__c='TEST';
            insert pgm;
            
            
            Agreement__c agg=TestUtilityClass.CreateTestAgreement();
            agg.Agreement_Name_UDF__c='DellVendor';
            agg.Agreement_Type__c=System.label.other;
            agg.Default_lessor__c=System.Label.KEF;
            agg.Agreement_Status__c=System.label.Prospect;
            agg.program__c=pgm.id;
            insert agg;
            
            Payoff_Quote__c pqc= TestUtilityClass.createPayoffQuote(agg.id);
            pqc.Payquote_Status__c='Active';  
            insert pqc;
            
            Vendor_Agreement__c vagg= TestUtilityClass.createVAAssociations(accRec.id, agg.Id);
            insert vagg;
        
            agg.Agreement_Name_UDF__c='DellVendor1';
            agg.Agreement_Type__c=System.label.other;
            agg.Default_lessor__c=System.Label.KEF;
            agg.program__c=pgm.id;
            agg.Agreement_Status__c=System.Label.SubmittoVendorCommittee;
            agg.Who_performs_remarketing__c=System.Label.KEF;
            agg.Targeted_spread__c=3.0;
            agg.Targeted_avg_term__c=4.0; 
            agg.Targeted_Length_of_sales_cycle__c=123.0; 
            agg.Targeted_Financial_Product_mix__c='testmix'; 
            agg.Targeted_Average_transaction_size__c=1.0;
            agg.Expected_End_of_Term_behavior__c='testbehav';
            update agg;
            
            List<Product2> prodList = TestUtilityClass.createProductsList();
            KEF_Product__c prodAsso = TestUtilityClass.createProductAssociations(agg.id, prodList.get(0).Id);
            insert prodAsso;
            
            agg.Agreement_Name_UDF__c='TestDellVendor';
            agg.Agreement_Type__c=System.label.other;
            agg.Default_lessor__c=System.Label.KEF;
            agg.program__c=pgm.id;
            //agg.Agreement_Status__c=System.label.Prospect;
            agg.Agreement_Status__c=System.Label.SubmittoCredit;
            agg.Allow_Payoff_Quote_to_Lessee__c='True';
            agg.Allow_Syndication__c='True';
            agg.Servicer_address__c='TestService';
            agg.Servicer_Name__c='TestName';
            agg.Residual_matrix_used__c='Yes';
            agg.Remit_Code__c='TestGA';
            agg.Referral_fee_arrangements__c='56';
            agg.Private_Label_to_Lessee__c='YES';
            agg.Pricing_rate_cards_used__c='Yes';
            agg.Perfect_Pay__c='No'; 
            //agg.Notify_Vendor_of_payoff_quote__c='Yes'; 
            agg.KEF_Servicing__c='Yes';
            agg.KEF_Responsible_for_Upfront_Sales_Tax__c='Yes';
            agg.Insurance_Program__c='A-GOAG';
            agg.Installment_Sale_Contract_Purchase__c='Yes'; 
            agg.Generate_invoices_to_the_servicer__c='yes';
            agg.Formal_Remarketing_Rider_in_place__c='Yes';
            agg.Customer_service_allowed_to_view_quote__c='Yes';
            agg.Assignment_Agreement_comments__c='Testcome';
            agg.Assignment_Agreement__c='NA';
            agg.Allow_Syndication__c='yes';
            agg.Rate_Card_Type__c='Annual';
            agg.Residual_matrix_used__c='yes';
            agg.Residual_matrix_disclosed__c='yes';
            agg.Expiration_Date_of_Residual_Matrix__c=system.today();
            agg.Written_consent_required_Syndicate_sell__c='yes';
            agg.Who_is_Servicing_for_Sales_Tax__c = accRec.id;
            agg.Who_is_Servicing_for_Property_Tax__c = accRec.id;
            update agg;
            
            pgm.Name_of_the_Vendor_Program__c='DellVendor';
            pgm.Program_Name_Change_Reason__c='Adversk Risk';
            try
            {
                update pgm;
            }
            catch(Exception e)
            {
                string msg=e.getmessage();
                system.assertequals(true, msg.contains(System.Label.AgreementNameUpdateError)); 
            }
        }
    }*/
    
    static testMethod void ProgramStatusChangeAgreementFlaggedTest()
    {
        Program__c pgm=TestUtilityClass.CreateTestProgram();
        insert pgm;
        
        Agreement__c agrRec = new Agreement__c();
        agrRec = testUtilityClass.CreateAgreementWithProgramId(pgm.id);
        agrRec.Integrated__c = true;
        insert agrRec;
        
        Test.StartTest();
        
        pgm.Program_Status__c = System.Label.SubmittoCredit;
        update pgm;
        
        String integrationStatus = [select Integration_Status__c from Agreement__c where id = :agrRec.id].Integration_Status__c;
        
        System.assertEquals(System.Label.Not_Picked_Up, integrationStatus);
        
        Test.StopTest();  
    }
    
    static testMethod void ProgramDeleteTest()
    {
        Program__c pgm=TestUtilityClass.CreateTestProgram();
        insert pgm;
        
        Agreement__c agrRec = new Agreement__c();
        agrRec = testUtilityClass.CreateAgreementWithProgramId(pgm.id);
        insert agrRec;
        
        test.StartTest();
        try
        {
            delete pgm;
        }
        catch(DMLException dmlEx)
        {
            system.debug('dmlEx.getDMLMessage>>>>      '+dmlEx);
          //  System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION,dmlEx.getDmlType(0));
        }
        test.stopTest();  
    }
        
    static testMethod void ProgramUndeleteTest()
    {
        Program__c pgm=TestUtilityClass.CreateTestProgram();
        pgm.Name_of_the_Vendor_Program__c='DellVendor';
        insert pgm;
        List<Private_Note_Attachment__c> privlist=[Select Id from Private_Note_Attachment__c where KEF_Program__c=:pgm.id];
        delete privlist;
        delete pgm;
        undelete pgm;
    }
}