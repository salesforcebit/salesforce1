/*
This class is written for KEF KRR Project by TCS Team.
It is a handler class for contact related Requirements.
*/
public with sharing class ContactHandler_KEF {

    Map<id, string> errorMap = new Map<id, string>();

    /* Below method is added for Release 3 Integration Req. 207.
     It updates the Integration status of Account on creation of Contact.*/

    public void onAfterInsert(Map<id, Contact> newContactMap)
    {
        if(newContactMap != null && newContactMap.size()>0)
        {
            errorMap = ContactHelper_KEF.updateAccountIntegrationStatus(newContactMap.keyset());
            addErrors(newContactMap);

            List<Contact> AuthorizedSignerList = new List<Contact>();
            for(Contact c : newContactMap.values()) {
                if(c.Authorized_Signer__c){
                    AuthorizedSignerList.add(c);
                }
            }

            createAuthorizedSigners(AuthorizedSignerList);

        }
    }

    /* Below method is added for Release 3 Integration Req. 208.
     It updates the Integration status of Account and Agreement on updation of Contact.*/

    public void onAfterUpdate(Map<id, Contact> newContactMap)
    {
        if(newContactMap != null && newContactMap.size()>0)
        {
             errorMap = ContactHelper_KEF.updateAccountIntegrationStatus(newContactMap.keyset());
             addErrors(newContactMap);
             errorMap = ContactHelper_KEF.updateAgreementIntegrationStatus(newContactMap.keyset());
             addErrors(newContactMap);
        }
    }
    private void addErrors(Map<id, Contact> newContactMap){
        if((errorMap != null && !errorMap.isEmpty()) )
        {
                for(Id conId : errorMap.keyset()){

                    if(newContactMap.containskey(conId)){
                        newContactMap.get(conId).addError(errorMap.get(conId));
                    }
                }
        }
    }

    public void afterUpdate(Map<id, Contact> oldMap, Map<id, Contact> newMap){
        List<Contact> AuthorizedSignerList = new List<Contact>();
        for(Contact c : newMap.values()) {
            //check to see if the field changed
            if(oldMap.get(c.id).Authorized_Signer__c != c.Authorized_Signer__c){
                if(c.Authorized_Signer__c){
                    AuthorizedSignerList.add(c);
                }
            }
        }

        createAuthorizedSigners(AuthorizedSignerList);

    }

    //This method creates a LLC_BI__Contingent_Liabilty__c record for each LLC_BI__Legal_Entities__c
    //on an account when a contact of that account has Authorized_Signer__c set to true
    public void createAuthorizedSigners(List<Contact> signerList) {

        //grab the entities that for Tresury Services
        Set<Id> acctSet = new Set<Id>();

        //loop through the contacts and get their accounts
        for(Contact c : signerList) {
            acctSet.add(c.AccountId);
        }

        //get all entities for the accounts
        List<LLC_BI__Legal_Entities__c> entityList = [Select LLC_BI__Treasury_Service__c, LLC_BI__Account__c
                                                from LLC_BI__Legal_Entities__c
                                                where LLC_BI__Treasury_Service__c != null
                                                and LLC_BI__Account__c in :acctSet];
        System.debug('::EntityList ' + entityList);
        //create a map where id is the account Id and it has a list of entitys
        Map<Id, List<LLC_BI__Legal_Entities__c>>accountToEntityMap = new Map<Id, List<LLC_BI__Legal_Entities__c>>();
        for(LLC_BI__Legal_Entities__c entity : entityList) {
            List<LLC_BI__Legal_Entities__c> tempList = accountToEntityMap.get(entity.LLC_BI__Account__c);
            if(tempList == null){
                tempList = new List<LLC_BI__Legal_Entities__c>();
                accountToEntityMap.put(entity.LLC_BI__Account__c, tempList);
            }
            tempList.add(entity);
        }
        System.debug('::Map ' + accountToEntityMap);

        List<LLC_BI__Contingent_Liabilty__c> authorizedSignerList = new List<LLC_BI__Contingent_Liabilty__c>();

        //loop through contacts, for each contact, get the list of entities it needs an LLC_BI__Contingent_Liabilty__c record
        //for and create that record
        for(Contact c : signerList) {
            System.debug('::Contact ' + c);
            List<LLC_BI__Legal_Entities__c> entities = accountToEntityMap.get(c.AccountId);
            if(entities != null){
                //due to error in maintence page, only want to create one signer per account
                //uncomment out the below lines when there can be 1 signer per Entity
                //5/4/2016
                //for(LLC_BI__Legal_Entities__c entity : entities) {
                //    LLC_BI__Contingent_Liabilty__c signer = new LLC_BI__Contingent_Liabilty__c();
                //    signer.LLC_BI__Authority__c = 'Authorized';
                //    signer.LLC_BI__Role__c = 'Official';
                //    signer.LLC_BI__Entity__c = entity.id;
                //    signer.LLC_BI__Contact__c = c.id;
                //    authorizedSignerList.add(signer);
                //}

                //when error is fixed delete these lines
                LLC_BI__Contingent_Liabilty__c signer = new LLC_BI__Contingent_Liabilty__c();
                signer.LLC_BI__Authority__c = 'Authorized';
                signer.LLC_BI__Role__c = 'Official';

                System.debug('::entities '+ entities);

                signer.LLC_BI__Entity__c = entities[0].id;
                signer.LLC_BI__Contact__c = c.id;
                authorizedSignerList.add(signer);
            }

 

        }

        if(authorizedSignerList.size() > 0){
            insert authorizedSignerList;
        }
    }

}