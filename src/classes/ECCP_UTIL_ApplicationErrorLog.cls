/*******************************************************************************************************************************************
* @name         :   ECCP_CustomMetadatUtil
* @description  :   This class will be responsible to log exception and query metadata
* @author       :   Aditya
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
public class ECCP_UTIL_ApplicationErrorLog{

    public static list<ECCP_Excaption_Log__c> lstException = new list<ECCP_Excaption_Log__c>();

/*******************************************************************************************************************************************
* @name         :   createExceptionLog
* @description  :   This class will be responsible to create exception log
* @author       :   Aditya
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
    public static void createExceptionLog(String ServiceName,String ClassName, String MethodName, String LineNumber,String ErrorCodeAndDescription,String recordId,String StackTrace){
    
    // Record Id is unique identity of record which can be used to find record in other system.
    // ErrorCodeAndDescription format : 'Error Code' + errorCode + ':' + error message
    
        ECCP_Excaption_Log__c excp = new ECCP_Excaption_Log__c();
        excp.ECCP_Service_Name__c = ServiceName;
        excp.ECCP_Class_Name__c = ClassName;
        excp.ECCP_Method_Name__c = MethodName;
        excp.ECCP_Error_Message__c = ErrorCodeAndDescription;
        excp.ECCP_Error_Stack_Trace__c = StackTrace;
        excp.ECCP_Line_Number__c = String.valueOf(LineNumber);
        excp.ECCP_RecordID__c = recordId;
        lstException.add(excp);
        
    } 
/*******************************************************************************************************************************************
* @name         :   saveExceptionLog
* @description  :   This class will be responsible to insert exception log
* @author       :   Aditya
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
    public static void saveExceptionLog()
    {
    system.debug('---lstException'+lstException);
    if(!lstException.isEmpty())
    {
        Database.insert(lstException,false); 
    }
  }

}