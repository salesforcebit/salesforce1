public with sharing class ProjectPlanningGanttVFC {

	public Id productPackageId {get;set;}

	public List<LLC_BI__Treasury_Service__c> treasuryServices {get {
		if(treasuryServices == null) {
			treasuryServices = [Select Id, Name, l.Type__c, l.LLC_BI__Product_Reference__c, Prerequisite_Treasury_Service__c, Planned_Implementation_Start_Date__c, Delay_Implementation__c, LLC_BI__Product_Package__r.CreatedDate From LLC_BI__Treasury_Service__c l Where LLC_BI__Product_Package__c =: productPackageId];
		}
		return treasuryServices;
	}set;}

	public Set<Id> productIds {get {
		if(productIds == null) {
			productIds = new Set<Id>();
			for(LLC_BI__Treasury_Service__c ts : treasuryServices) {
				productIds.add(ts.LLC_BI__Product_Reference__c);
			}
			productIds.remove(null);
		}
		return productIds;
	}set;}

	public Map<String, TSO_Workflow_Template__c> workflowTemplateMap {get {
		if(workflowTemplateMap == null) {
			workflowTemplateMap = new Map<String, TSO_Workflow_Template__c>();
			for(TSO_Workflow_Template__c workflowTemplate : [Select Id, t.Type__c, t.Product__c, (Select Id, Name, Expected_Days_to_Complete__c From Stages__r) From TSO_Workflow_Template__c t Where Product__c in: productIds]) {
				workflowTemplateMap.put(generateWrapperKey(workflowTemplate), workflowTemplate);
			}
		}
		return workflowTemplateMap;
	}set;}

	public Map<Id, TreasuryServiceGantt> ganttTreasuryServicesMap {get {
		if(ganttTreasuryServicesMap == null) {
			ganttTreasuryServicesMap = new Map<Id, TreasuryServiceGantt>();
			for(LLC_BI__Treasury_Service__c treasuryService : treasuryServices) {
				ganttTreasuryServicesMap.put(treasuryService.Id, new TreasuryServiceGantt(treasuryService, workflowTemplateMap.get(generateWrapperKey(treasuryService))));
			}
			//Apply prereqs
			for(TreasuryServiceGantt ganttTreasuryService : ganttTreasuryServicesMap.values()) {
				if(ganttTreasuryService.treasuryService.Prerequisite_Treasury_Service__c != null) {
					ganttTreasuryService.prerequisiteTreasuryService = ganttTreasuryServicesMap.get(ganttTreasuryService.treasuryService.Prerequisite_Treasury_Service__c);
				}
			}
		}
		return ganttTreasuryServicesMap;
	}set;}

	public List<TreasuryServiceGantt> ganttTreasuryServices {get {
		if(ganttTreasuryServices == null) {
			ganttTreasuryServices = ganttTreasuryServicesMap.values();
            if(hasCycles(ganttTreasuryServices)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check for cyclical dependencies.'));
            }
            else {
                ganttTreasuryServices.sort();
                for(TreasuryServiceGantt ts : ganttTreasuryServices) {
                    ts.setStageDates();
                }
            }
		}
		return ganttTreasuryServices;
	}set;}

	public boolean stagesFound {get {
		for(TreasuryServiceGantt tsg : ganttTreasuryServices) {
			if(tsg.stages.size() > 0) {
				return true;
			}
		}
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Constants.ErrorGanttNoStages));
		return false;
	}set;}

	public Date startDate {get {
		if(startDate == null) {
			startDate = ganttTreasuryServices[0].startDate;
			for(TreasuryServiceGantt ts : ganttTreasuryServices) {
				if(ts.startDate < startDate) {
					startDate = ts.startDate;
				}
			}
		}
		return startDate;
	}set;}

	public Date finalStageEndDate {get {
		if(finalStageEndDate == null) {
			finalStageEndDate = date.today();
			for(TreasuryServiceGantt ts : ganttTreasuryServices) {
				if(ts.endDate > finalStageEndDate) {
					finalStageEndDate = ts.endDate;
				}
			}
		}
		return finalStageEndDate;
	}set;}

	public Integer totalDaysToComplete {get {
		if(totalDaysToComplete == null) {
			totalDaysToComplete = startDate.daysBetween(finalStageEndDate);
		}
		return totalDaysToComplete;
	}set;}

	public String generateWrapperKey(LLC_BI__Treasury_Service__c ts) {
		return ts.LLC_BI__Product_Reference__c + ts.Type__c;
	}

	public String generateWrapperKey(TSO_Workflow_Template__c workflowTemplate) {
		return workflowTemplate.Product__c + workflowTemplate.Type__c;
	}

	/*public ProjectPlanningGanttVFC(ApexPages.StandardController std) {
		productPackageId = std.getId();
	}*/
    
    public Boolean hasCycles(list<TreasuryServiceGantt> tsGantts) {
		for(TreasuryServiceGantt tsGantt : tsGantts) {
			if(hasCycle(tsGantt)) {
				return true;
			}
		}
		return false;
	}
	
	public Boolean hasCycle(TreasuryServiceGantt tsGantt) {
		TreasuryServiceGantt prereq = tsGantt.prerequisiteTreasuryService;
		while(prereq != null) {
			if(prereq.treasuryService.Id == tsGantt.treasuryService.Id) {
				return true;
			}
			prereq = prereq.prerequisiteTreasuryService;
		}
		
		return false;
	}

	public PageReference kickoffProject() {
		Savepoint sp = Database.setSavePoint();
		Boolean allPass;
		try {
			List< LLC_BI__Treasury_Service__c> updateServices = new List< LLC_BI__Treasury_Service__c>();
			Map<Id, LLC_BI__Treasury_Service__c> tresServMap = new Map<Id, LLC_BI__Treasury_Service__c>( treasuryServices );

			for(TreasuryServiceGantt tsGantt : ganttTreasuryServices) {
				LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c(id = tsGantt.treasuryService.Id);
				if(tsGantt.prerequisiteTreasuryService == null) {
					if(tsGantt.startDate <= date.today()) {
						ts.LLC_BI__Stage__c = Constants.treasuryServiceStageFulfillment;
					}
					else {
						ts.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingFutureStartDate;
					}
				}
				else {
					ts.LLC_BI__Stage__c = Constants.treasuryServiceStagePendingPrerequisites;
				}
				updateServices.add( ts);
			}
			if(updateServices.size() > 0) {
				System.debug('updateServices::'+updateServices);
				Database.SaveResult[] saveResult = Database.update( updateServices, false);
				allPass = true;
				//loop through save records and see if they succeeded
				for(integer i = 0; i < saveResult.size(); i++){
					Database.SaveResult sr = saveResult[i];
					if(!sr.isSuccess() ){//get sucess is false then continue
						LLC_BI__Treasury_Service__c tresServ = tresServMap.get( updateServices[i].id);
						System.debug('SaveResult::'+sr);
						System.debug('tresServ::'+tresServ);
						//set all pass to false
						allPass = false;
						for(Database.Error de : sr.getErrors() ){//add error message. Record Name : Field affected : error message
							ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, tresServ.Name + ' failed. '+ de.getMessage() + '::['+de.getFields()+']' ) );
						}
					}

				}
			}

			if(allPass){
				LLC_BI__Product_Package__c pack = new LLC_BI__Product_Package__c(id = productPackageId);
				ApexPages.standardController packSTD = new ApexPages.standardController(pack);
				return packSTD.view();
			} else {
				Database.rollback(sp);
			}
		} catch (Exception e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			Database.rollback(sp);
			System.debug('error::'+e.getMessage() );
		}

		return null;
	}

	public ProjectPlanningGanttVFC() {

	}

	public class TreasuryServiceGantt implements Comparable{
		public LLC_BI__Treasury_Service__c treasuryService {get;set;}
		public TreasuryServiceGantt prerequisiteTreasuryService {get;set;}
		public List<TSOStageGantt> stages {get;set;}
		//public Date productPackageStartDate {get;set;}
		public Date startDate {get {
			if(startDate == null) {
				if(prerequisiteTreasuryService == null) {
					Datetime packageStart = treasuryService.LLC_BI__Product_Package__r.CreatedDate;
					startDate = packageStart.date();
				}
				else {
					startDate = prerequisiteTreasuryService.testingStartDate;
				}

				//Check for delayed implementation start
				if(treasuryService.Delay_Implementation__c == true) {
					startDate = treasuryService.Planned_Implementation_Start_Date__c > startDate ? treasuryService.Planned_Implementation_Start_Date__c : startDate;
				}
			}
			return startDate;
		}set;}
		public Date endDate {get {
			if(endDate == null) {
				endDate = stages[stages.size() - 1].endDate;
			}
			return endDate;
		}set;}

		public Date testingStartDate {get {
			if(testingStartDate == null) {
				setStageDates();
				if(testingStartDate == null) {
					return date.today();
				}
			}
			return testingStartDate;
		}set;}

		public TreasuryServiceGantt(LLC_BI__Treasury_Service__c ts, TSO_Workflow_Template__c workflowTemplate) {
			treasuryService = ts;
			stages = new List<TSOStageGantt>();
			if(workflowTemplate != null && workflowTemplate.Stages__r != null && workflowTemplate.Stages__r.size() > 0) {
				for(TSO_Stage__c stage : workflowTemplate.Stages__r) {
					stages.add(new TSOStageGantt(stage));
				}
				stages.sort();
			}
		}

		private void setStageDates() {
			Date stageStart = startDate;
			for(TSOStageGantt stage : stages) {
				stage.startDate = stageStart;
				stage.endDate = stage.startDate.addDays(stage.stageLength);
				stageStart = stage.endDate;
				if(stage.stage.name == Constants.treasuryServiceStageTesting) {
					testingStartDate = stage.startDate;
				}
			}
		}

		public Integer compareTo(Object compareTo) {
			TreasuryServiceGantt compareToTS = (TreasuryServiceGantt)compareTo;
			if(prerequisiteTreasuryService == null) {
				if(compareToTS.prerequisiteTreasuryService == null) {
					if(treasuryService.Id > compareToTS.treasuryService.Id) return 1;
					else return -1;
				}
				else return compareTo(compareToTS.prerequisiteTreasuryService);
			}
			else {
				if(compareToTS.prerequisiteTreasuryService == null) return prerequisiteTreasuryService.compareTo(compareToTS);
				else return prerequisiteTreasuryService.compareTo(compareToTS.prerequisiteTreasuryService);
			}
		}
	}

	public class TSOStageGantt implements Comparable {
		public TSO_Stage__c stage {get;set;}
		public boolean isFulfillment {get {
			if(isFulfillment == null) {
				isFulfillment = stage.name.startsWith('Fulfillment');
			}
			return isFulfillment;
		}set;}
		public Date startDate {get;set;}
		public Date endDate {get;set;}
		public Integer stageLength {get {return (Integer)stage.Expected_Days_to_Complete__c;}set;}

		public TSOStageGantt(TSO_Stage__c s) {
			stage = s;
		}

		public Integer compareTo(Object compareTo) {
			TSOStageGantt compareToStage = (TSOStageGantt)compareTo;
			Decimal currentStageOrder = Constants.stageOrderMap.get(stage.Name);
			Decimal compareStageOrder = Constants.stageOrderMap.get(compareToStage.stage.Name);
			if(currentStageOrder == compareStageOrder) return 0;
			if(currentStageOrder > compareStageOrder) return 1;
			return -1;
		}
	}
}