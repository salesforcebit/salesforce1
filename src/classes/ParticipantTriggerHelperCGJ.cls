public without sharing class ParticipantTriggerHelperCGJ
{
    @future public static void callgoalsjunction(list<Id> parrecId)
    {
        system.debug('ParticipantName' + parrecId);
        List<Call_Goals_Junction__c> cglL = new List<Call_Goals_Junction__c>();
        List<Call_Goals__c> cgoalL = [Select Id,Name, Username__c,Username__r.Name, year__c, Goal_Month__c from Call_Goals__c];
        system.debug(cgoalL);
        List<Participant__c> ptlist = [Select Id, CallId__r.Id, Name, CallId__c,UserId__c, Call_Date__c, Did_Not_Participate__c from Participant__c where Id =: parrecId];
        system.debug(cgoalL.size());
        
            
            for(Call_Goals__c cgoal : cgoalL)                                     
            {        
                
                for(Participant__c partL : ptlist)
                    {                        
                        if(cgoal.Goal_Month__c != null)
                        {
                            List<Call__C> callsL = [Select Id, Name,Call_Date__c from Call__c where Id =: partL.CallId__c];system.debug(partL.UserId__c);
                            system.debug(cgoal.Username__r.name);
                            system.debug(cgoal.Goal_Month__c.Month());
                            system.debug(partL.Call_Date__c.Month());
                            if(partL.Did_Not_Participate__c == false && cgoal.Goal_Month__c.year() == partL.Call_Date__c.year() && cgoal.Goal_Month__c.month() == partL.Call_Date__c.month() && partL.UserId__c == cgoal.Username__r.Name)
                            {                            
                                Call_Goals_Junction__c cgj = new Call_Goals_Junction__c();
                                cgj.Participant__c = partL.Id;
                                cgj.Call_Goals__c = cgoal.Id;
                                cgj.Call__c = partL.CallId__r.Id;
                                cgj.Name = partL.UserId__c ;
                                cglL.add(cgj);
                                system.debug(cglL);                        
                            }
                            else
                            break;
                         }
                    }
                
            }
                system.debug(cglL);
                if(cglL.size()>0){
                   insert cglL;
                }
        
    }
}