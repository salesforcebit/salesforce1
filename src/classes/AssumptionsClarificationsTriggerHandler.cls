public with sharing class AssumptionsClarificationsTriggerHandler {

	//creates a Scenario_Assumptions__c record for each Assumptions_and_Clarifications__c created.
	public static void handleAfterInsert(List<Assumptions_and_Clarifications__c> newList) {
		List<Scenario_Assumptions__c>assumptions = new List<Scenario_Assumptions__c>();
		for(Assumptions_and_Clarifications__c aac : newList){
			Scenario_Assumptions__c tempSA = new Scenario_Assumptions__c();
			tempSA.name = aac.name;
			tempSA.Assumptions_and_Clarifications__c = aac.id;
			tempSA.Scenario__c = aac.Scenario__c;
			assumptions.add(tempSA);
		}

		if(assumptions.size() > 0) {
			insert assumptions;
		}


	}

}