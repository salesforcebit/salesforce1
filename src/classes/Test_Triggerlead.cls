@istest
Public class Test_Triggerlead
{
    Static Profile p; 
    Static SIC_NAICS__c sic;    
    Static SIC_NAICS__c naics;
    Static Account a;
    static contact con;
    static campaign camp ;
    static Lead leadc ;
    static Program_Member__c pc ;
    static User testUser;             
        
    public static testMethod void leadconverttest(){
             
        p = [select id from profile where name='System Administrator'];
        testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharanz@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharanx@keybank.com',PLOB__c='support',
        PLOB_Alias__c='support', RACF_Number__c ='HINDF');  
        insert testUser;
        
        sic= new SIC_NAICS__c(Code__c='11',Code_Type__c='SIC',Description__c='testl');
        insert sic;
        
        naics=new SIC_NAICS__c(Code__c='121',Code_Type__c='NAICS',Description__c='testlead1');
        insert naics;
        
        a = new account(name='acct',Sic=sic.Code__c,SicDesc=sic.Description__c,NaicsCode=naics.Code__c,NaicsDesc=naics.Description__c);
        insert a;
        
       // RecordType RT =[SELECT Id, Name, SobjectType FROM RecordType where SobjectType ='Contact' and Name='Key Employee'];
        Map<String, Schema.SObjectType> accsObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs = accsObjectMap.get('Contact') ;
        Schema.DescribeSObjectResult resSchemaAcc = accs.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo = resSchemaAcc.getRecordTypeInfosByName();
        Id RT = accRecordTypeInfo.get('Key Employee').getRecordTypeId();
        System.Debug('RT:'+RT);
        
        con = new contact(LastName='Mr. Testing2015', Recordtypeid=RT, RACFID__c='HINDG', Officer_Code__c='HINDP',Contact_Type__c='Key Employee',AccountId=a.id,Salesforce_User__c=true,User_Id__c=testUser.Id,Phone='9876543210' );
        insert con;
        
        // RecordType RT4 = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Campaign' and name='Programs'];
        
        Map<String, Schema.SObjectType> accsObjectMap4 = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs4 = accsObjectMap4.get('Campaign') ;
        Schema.DescribeSObjectResult resSchemaAcc4 = accs4.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo4 = resSchemaAcc4.getRecordTypeInfosByName();
        Id RT4 = accRecordTypeInfo4.get('Programs').getRecordTypeId();
        System.Debug('RT4:'+RT4);
        
        camp = new campaign(Name = 'TestCampaign', type='Top 5', status='Completed', IsActive=true, recordtypeId= RT4);
        insert camp;
        
        leadc=new Lead(LastName='Doe',FirstName='John',Company='Test',Status='New',Product_Category__c='test',
        Primary_Contact_Phone__c='9638527411',SIC_Code__c=sic.Code__c,SIC_Code_Description__c=sic.Description__c,
        NAICS_Code__c=naics.Code__c,NAICS_Description__c=naics.Description__c,Phone='9897608977', Street = 'Raju st', City='Erode',LeadSource='Client',
        state='Ohio', postalcode='44471', country='USA',Opportunity_Record_Type__c='Credit', AnnualRevenue =23,createdbyId=testUser.Id);
        insert leadc;
        
        // RecordType RT5 = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Program_Member__c' and name='Top 5 Program'];
        Map<String, Schema.SObjectType> accsObjectMap5 = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs5 = accsObjectMap5.get('Program_Member__c') ;
        Schema.DescribeSObjectResult resSchemaAcc5 = accs5.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo5 = resSchemaAcc5.getRecordTypeInfosByName();
        Id RT5 = accRecordTypeInfo5.get('Top 5 Program').getRecordTypeId();
        System.Debug('RT5:'+RT5);
        
        pc = new Program_Member__c(CampaignId__c=camp.id,recordtypeId=RT5, LeadId__c =leadc.Id );
        insert pc; 
                       
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(leadc.id);
        lc.setDoNotCreateOpportunity(false);
        lc.setConvertedStatus('Qualified');
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());
        
        //Lead ld = [select Id, ConvertedAccountId from Lead where Id =: leadc.id LIMIT 1];
           
        //Program_Member__c pc1 = [select Id, AccountId__c from Program_Member__c where Id =: pc.Id];
        
        pc.AccountId__c = leadc.ConvertedAccountId;
        Test.startTest();
        update pc;
        Test.stopTest();
        
    }
     
    public static testMethod void leadconverttest1(){
    
        p = [select id from profile where name='System Administrator'];
        sic= new SIC_NAICS__c(Code__c='11',Code_Type__c='SIC',Description__c='testl');
        insert sic;
        
        naics=new SIC_NAICS__c(Code__c='121',Code_Type__c='NAICS',Description__c='testlead1');
        insert naics;
         Account a1 = new account(name='acct',Sic=sic.Code__c,SicDesc=sic.Description__c,NaicsCode=naics.Code__c,NaicsDesc=naics.Description__c);
        insert a1;
        // RecordType RT1 =[SELECT Id, Name, SobjectType FROM RecordType where SobjectType ='Contact' and Name='Key Employee'];
         Map<String, Schema.SObjectType> accsObjectMap1 = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs1 = accsObjectMap1.get('Contact') ;
        Schema.DescribeSObjectResult resSchemaAcc1 = accs1.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo1 = resSchemaAcc1.getRecordTypeInfosByName();
        Id RT1 = accRecordTypeInfo1.get('Key Employee').getRecordTypeId();
        System.Debug('RT1:'+RT1);
        
        user testUser1 = new User(alias = 'u1', email='Dhanesh_subramanian@keybank.com',
        emailencodingkey='UTF-8', lastname='Dhanesh', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='Dhanesh_subramanianx2@keybank.com',PLOB__c='support',
        PLOB_Alias__c='support', RACF_Number__c ='SUBRAD3');             
        insert testUser1;
        
        contact con1 = new contact(LastName='Dhanesh', Recordtypeid=RT1, RACFID__c='SUBRAD2', Officer_Code__c='HINDF1',Contact_Type__c='Key Employee',AccountId=a1.id,Salesforce_User__c=true,User_Id__c=testUser1.Id,Phone='9876543210' );
        insert con1;
        
        Lead leadc1=new Lead(LastName='Doe',FirstName='John',Company='Test',Status='Inquiry',Product_Category__c='test',
        Primary_Contact_Phone__c='9638527411',SIC_Code__c=sic.Code__c,SIC_Code_Description__c=sic.Description__c,
        NAICS_Code__c=naics.Code__c,NAICS_Description__c=naics.Description__c,Phone='9897608977', Street = 'Raju st', City='Erode',
        state='Ohio', postalcode='44471', country='USA', AnnualRevenue =23, Initiator_Name__c=con1.id, 
        LeadSource='Internal Referral', Lead_recipient__c=con1.id, BusinessContact_Requested_Call_Key__c=true,createdbyId=testUser1.Id);
        insert leadc1; 
          
        Lead leadld=new Lead(LastName='Doe1',FirstName='John1',Company='Test12',Status='Inquiry',Product_Category__c='test1',
        Primary_Contact_Phone__c='963852751',SIC_Code__c=sic.Code__c,SIC_Code_Description__c=sic.Description__c,
        NAICS_Code__c=naics.Code__c,NAICS_Description__c=naics.Description__c,Phone='9897608967', Street = 'Raja st', City='Erode',
        state='Ohio', postalcode='44171', country='USA', AnnualRevenue =23,  
        LeadSource='Internal Referral', Lead_recipient__c=con1.id, BusinessContact_Requested_Call_Key__c=true,createdbyId=testUser1.Id);
        insert leadld; 
        if(leadld.Initiator_Name__c == null)
        {
            lead lt = new lead(); 
            lt.Id = leadld.Id;        
            lt.Initiator_Name__c=con1.id;
            Test.startTest();
            update lt;
            Test.stopTest();
        }
    }
}