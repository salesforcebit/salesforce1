/**
 * Provides a strongly-typed intermediate object for converting the JSON representation
 * of a Product Catalog UI Template to its sObject equivalent.
 * Uses the IForce pattern to map between sObject and JSON.
 */
public class ProductCatalogUITemplate extends nFORCE.AForce {
	public String sid;
	public String name;
	public Boolean isDefault;
	public List<ProductCatalogRouteTemplate> routeTemplates;

	public ProductCatalogUITemplate() {
		this.routeTemplates = new List<ProductCatalogRouteTemplate>();
	}

	public ProductCatalogUITemplate(String sid, String name, Boolean isDefault) {
		this.sid = sid;
		this.name = name;
		this.isDefault = isDefault;
		this.routeTemplates = new List<ProductCatalogRouteTemplate>();
	}

	public Product_Catalog_UI_Template__c getSObject() {
		Product_Catalog_UI_Template__c dbUiTemplate = new Product_Catalog_UI_Template__c();
		this.mapToDb(dbUiTemplate);
		return dbUiTemplate;
	}

	public static ProductCatalogUITemplate buildFromDB(sObject obj) {
		ProductCatalogUITemplate uiTemplate = new ProductCatalogUITemplate();
		uiTemplate.mapFromDB(obj);
		return uiTemplate;
	}

	public override Type getType() {
		return ProductCatalogUITemplate.class;
	}

	public override Schema.SObjectType getSObjectType() {
		return Product_Catalog_UI_Template__c.sObjectType;
	}

	public override void mapFromDB(sObject obj) {
		Product_Catalog_UI_Template__c uiTemplate = (Product_Catalog_UI_Template__c)obj;
		if (uiTemplate != null) {
			this.sid = uiTemplate.Id;
			this.name = uiTemplate.Name;
			this.isDefault = uiTemplate.Is_Default__c;
		}
	}

	public override void mapToDb(sObject obj) {
		Product_Catalog_UI_Template__c uiTemplate = (Product_Catalog_UI_Template__c)obj;
		if (uiTemplate != null) {
			if (this.sid != null && this.sid.length() > 0) {
				uiTemplate.Id = Id.valueOf(this.sid);
			}
			uiTemplate.Name = this.name;
			uiTemplate.Is_Default__c = this.isDefault;
		}
	}
}