//================================================================================
//  KeyBank  
//  Object: Program For BD,Production goals,Call goals
//  Author: Offshore
//  Detail: shareHandler
//  Description : To provide sharing access to users for Program for BD goals,Call goals, Production goals

//================================================================================
//          Date            Purpose
// Changes: 23/02/2016      Initial Version
//================================================================================
public class shareHandler{

//This method is used to share Program for BD goals the assigned user
public static void shareProgramForBD(List<Program_for_BD__c> pId){
  List<Program_for_BD__c> programBD =[Select id,user_name__c from Program_for_BD__c where id in :PId];
  List<Program_for_BD__Share> listShare = new List <Program_for_BD__Share> (); 
  for(Program_for_BD__c pBD: programBD ){
      listShare.add(new Program_for_BD__Share(AccessLevel = 'Read',ParentId = pBD.Id,UserOrGroupId = pBD.User_name__c ));
  }
  try{
      if(ListShare.size()>0)
      insert listShare;
  }
  Catch(Exception e){
      system.debug(e);
  }
}
//This method is used to share Production goals the assigned user
public static void shareProductionGoals(List<Production_Goals__c> prodId){
  List<Production_Goals__c> prod=[Select id,username__c from Production_Goals__c where id in :prodId];
  List<Production_Goals__Share> listProdShare = new List <Production_Goals__Share> (); 
  for(Production_Goals__c p: prod){
      listProdShare.add(new Production_Goals__Share(AccessLevel = 'Read',ParentId = p.Id,UserOrGroupId = p.Username__c ));
  }
  try{
      if(ListProdShare.size()>0)
      insert ListProdShare;
  }
  Catch(Exception e){
      system.debug(e);
  }
}
//This method is used to share Call goals the assigned user
public static void shareCallGoals(List<Call_Goals__c> callId){
  List<Call_Goals__c> call=[Select id,username__c from Call_Goals__c where id in :callId];
  List<Call_Goals__Share> listCallShare = new List <Call_Goals__Share> (); 
  for(Call_Goals__c c: call){
      listCallShare.add(new Call_Goals__Share(AccessLevel = 'Read',ParentId = c.Id,UserOrGroupId = c.Username__c ));
  }
  try{
      if(ListCallShare.size()>0)
      insert ListCallShare;
  }
  Catch(Exception e){
      system.debug(e);
  }
}

}