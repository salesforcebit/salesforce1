/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestOpporunityProductTrigger {

    Static PricebookEntry pre1;
    static testMethod void OLIPrimaryCheck() {
        // TO DO: implement unit test
        Account acc1 = new account (name='test acckef1');
        recordtype rt1 = [Select Id, name from recordtype where name ='KEF Opportunity'];
        
        
        List<Agreement__c> aggs = new List<Agreement__c>();
        
        Program__c objpgm=new Program__c
        (
        Program_Status__c=System.Label.Prospect, Name_of_the_Vendor_Program__c='Testprogram',
        Business_Development_Stage__c=System.label.BuisnessStage
        );
        insert objpgm;
        
        Agreement__c objagg=new Agreement__c
        (
        Agreement_Name_UDF__c='TestUDF',Agreement_Legal_Status__c=System.Label.informal,Agreement_Type__c=System.label.other,
        Agreement_Status__c=System.label.Prospect,Generate_invoices_to_the_servicer__c='Yes',Servicer_Name__c='Testnme',Third_Party_Name_for_Property_Tax_Servic__c='Test2',Third_Party_Name_for_Sales_Tax_Servicing__c='Test1' ,Default_lessor__c=System.Label.KEF,Program__c=objpgm.id,
        Agreement_Term__c = 12, Agreement_Commencement_Date__c = System.Today()
        );
        
        insert objagg;
        
        
        // Creating a Test Oppty of type KEF Opportunity
        opportunity KEFopty1 = new opportunity(
        name ='testKEF1',
        AccountId = acc1.id,
        closedate=system.today(),
        StageName='Pursue',
        Volume__c=23,
        Vendor_Agreement__c=objagg.id,
        recordtypeId=rt1.id);        
        insert KEFopty1;
        Product2 prd = new Product2(Name='Test', Family='KEF', Product_category__c='Conditional Lease',IsActive=true);
        insert prd;     
        Id pricebookId1 = Test.getStandardPricebookId();
        pre1 = new PricebookEntry(Pricebook2Id=pricebookId1,IsActive=true, UnitPrice =7658, Product2Id=prd.id);
        insert pre1;
        
        Opportunitylineitem oli = new Opportunitylineitem ();
        oli.Opportunityid=KEFopty1.id;
        oli.PriceBookEntryID=pre1.id;
        oli.Payment_Frequency__c='testfr1';
        oli.Payment_Type__c='testtype1';
        oli.Primary_Product__c=true;
        
        insert oli;
        
        
            
        Opportunitylineitem oli1=[select id from Opportunitylineitem where id=:oli.id];
        //delete oli1;
        
         try
        {
           delete oli1;
           
           
           // should throw exception, so fail test if get this far
           //System.assert(false, 'Exception expected');
        }
        catch (Exception e)
        {
           // expected exception
        }
                    
            }
            
        public static testMethod void olitest()
        {
        Account acc1= new Account(Name='Test');
        opportunity opty = new Opportunity(Name='Test ECP', Accountid = acc1.id, closedate = system.today(), stagename = 'pursue',
        Opportunity_Type__c='Audit Adjustment');
        insert opty;
        Product2 prd2 = new Product2(Name='Key Merchant Services', Family='Deposits & ECP', Product_category__c='Key Merchant Services',IsActive=true);
        insert prd2;     
        Id pricebookId2 = Test.getStandardPricebookId();
        PriceBookEntry pre2 = new PricebookEntry(Pricebook2Id=pricebookId2,IsActive=true, UnitPrice =7658, Product2Id=prd2.id);
        insert pre2;
        Opportunitylineitem oli2 = new Opportunitylineitem ();
        oli2.Opportunityid=opty.id;
        oli2.PriceBookEntryID=pre2.id;
        //oli.Payment_Frequency__c='testfr1';
        //oli.Payment_Type__c='testtype1';
        //oli.Primary_Product__c=true;
        
        insert oli2;
        Opportunitylineitem oli3=[select id from Opportunitylineitem where id=:oli2.id];
        
         try
        {
           delete oli3;
           
           
           // should throw exception, so fail test if get this far
           //System.assert(false, 'Exception expected');
        }
        catch (Exception e)
        {
           // expected exception
        }
                    
        }
}