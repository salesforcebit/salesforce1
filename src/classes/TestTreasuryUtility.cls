/**
* Utility test class
**/

@isTest
public class TestTreasuryUtility {

    @isTest
    public static void testCreateSystemProperty() {
    	Test.startTest();
    	sObject systemProperty =
    		Utility.createSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY,
    			'true',
    			true,
    			true
    		);

    	System.assert(null != systemProperty.get('Id'));
    	Test.stopTest();
    }

    @isTest
    public static void testUpdateSystemProperty() {

    	sObject systemProperty =
    		Utility.createSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY,
    			'true',
    			true,
    			true
    		);

    	Test.startTest();

    	sObject testSystemProperty =
    		Utility.updateSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY,
    			'false',
    			true
    		);

    	System.assert(
    		testSystemProperty.get(TreasuryConfigurationConstants.PROPERTY_FIELD_VALUE)
    		!=
     		systemProperty.get(TreasuryConfigurationConstants.PROPERTY_FIELD_VALUE)
    	);

    	testSystemProperty =
    		Utility.updateSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY,
    			'true',
    			true
    		);

    	System.assert(
    		testSystemProperty.get(TreasuryConfigurationConstants.PROPERTY_FIELD_VALUE)
    		==
     		systemProperty.get(TreasuryConfigurationConstants.PROPERTY_FIELD_VALUE)
    	);

    	Test.stopTest();
    }

    @isTest
    public static void testDeleteSystemProperty() {

    	sObject systemProperty =
    		Utility.createSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY,
    			'true',
    			true,
    			true
    		);

    	Test.startTest();

    	sObject testSystemProperty =
    		Utility.retrieveSystemPropertyObject(
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY
    		);

    	System.assert(
    		testSystemProperty.get(TreasuryConfigurationConstants.PROPERTY_FIELD_ID)
    		==
     		systemProperty.get(TreasuryConfigurationConstants.PROPERTY_FIELD_ID)
     	);

     	Utility.deleteSystemProperty(
			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY
     	);

		testSystemProperty =
    		Utility.retrieveSystemPropertyObject(
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY
    		);

    	System.assert(testSystemProperty == null);

    	Test.stopTest();
    }

    @isTest
    public static void testCreateOnUpdate() {
    	Test.startTest();

    	sObject testSystemProperty =
    		Utility.updateSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY,
    			'true',
    			true
    		);

    	System.assert(null != testSystemProperty.get('Id'));

    	Test.stopTest();
    }

    @isTest
    public static void testIsProduction() {
    	sObject systemProperty =
    		Utility.createSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIG_IS_PRODUCTION_KEY,
    			'true',
    			true,
    			true
    		);

    	Test.startTest();

    	Boolean isProduction = Utility.isProduction();
    	System.assertEquals(true, isProduction);

    	systemProperty =
    		Utility.updateSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIG_IS_PRODUCTION_KEY,
    			'false',
    			true
    		);

		TreasuryConfigurationConstants.SYSTEM_PROPERTIES.clearCache();

    	isProduction = Utility.isProduction();
    	System.assertEquals(false, isProduction);

     	Utility.deleteSystemProperty(
			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
			TreasuryConfigurationConstants.TM_CONFIG_IS_PRODUCTION_KEY
     	);

		TreasuryConfigurationConstants.SYSTEM_PROPERTIES.clearCache();

    	isProduction = Utility.isProduction();
    	System.assertEquals(false, isProduction);
    }

    @isTest
    public static void testIsToolDataInstalled() {
    	sObject systemProperty =
    		Utility.createSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_INITIAL_DATA_INSTALLED_KEY,
    			'true',
    			true,
    			true
    		);

    	Test.startTest();

    	Boolean isToolDataInstalled = Utility.isToolDataInstalled();
    	System.assertEquals(true, isToolDataInstalled);

    	systemProperty =
    		Utility.updateSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_INITIAL_DATA_INSTALLED_KEY,
    			'false',
    			true
    		);

		TreasuryConfigurationConstants.SYSTEM_PROPERTIES.clearCache();

    	isToolDataInstalled = Utility.isToolDataInstalled();
    	System.assertEquals(false, isToolDataInstalled);

     	Utility.deleteSystemProperty(
			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
			TreasuryConfigurationConstants.TM_INITIAL_DATA_INSTALLED_KEY
     	);

		TreasuryConfigurationConstants.SYSTEM_PROPERTIES.clearCache();

    	isToolDataInstalled = Utility.isToolDataInstalled();
    	System.assertEquals(false, isToolDataInstalled);
    }

    @isTest
    public static void testIsConfigurationToolInstalled() {
    	sObject systemProperty =
    		Utility.createSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY,
    			'true',
    			true,
    			true
    		);

    	Test.startTest();

    	Boolean isConfigurationToolInstalled = Utility.isConfigurationToolInstalled();
    	System.assertEquals(true, isConfigurationToolInstalled);

    	systemProperty =
    		Utility.updateSystemProperty(
    			'MyTestProperty',
    			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
    			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY,
    			'false',
    			true
    		);

		TreasuryConfigurationConstants.SYSTEM_PROPERTIES.clearCache();

    	isConfigurationToolInstalled = Utility.isConfigurationToolInstalled();
    	System.assertEquals(false, isConfigurationToolInstalled);

     	Utility.deleteSystemProperty(
			TreasuryConfigurationConstants.TM_CONFIG_CATEGORY,
			TreasuryConfigurationConstants.TM_CONFIGURATION_TOOL_INSTALLED_KEY
     	);

		TreasuryConfigurationConstants.SYSTEM_PROPERTIES.clearCache();

    	isConfigurationToolInstalled = Utility.isConfigurationToolInstalled();
    	System.assertEquals(false, isConfigurationToolInstalled);
    }

    @isTest
    public static void testNameSpace() {
    	String nameSpace = Utility.getNamespace();
    	String coreNameSpace = Utility.getCoreNamespace();
    	String initialString = '123%^%A B C';
    	String testString = Utility.alphanumericOnly(initialString);
    	System.assertEquals('123ABC', testString);
    }

    @isTest
    public static void testQueryStringGenerator() {
    	String testString = 'SELECT field1,field2,ID FROM queryObject WHERE name := nameString ORDER BY name';
    	Test.startTest();

    	String queryString =
    		Utility.getQueryString(
    			'queryObject',
    			new List<String>{'field1','field2'},
    			'name := nameString',
    			'name'
    		);

    	System.assertEquals(testString, queryString);

    	Test.stopTest();
    }

}