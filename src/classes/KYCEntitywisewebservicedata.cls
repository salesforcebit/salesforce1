global class KYCEntitywisewebservicedata{
      
      global class BorrowerAccountdata{
         webservice string AccountName;
         webservice string AccountBranch;
         webservice string AccountSource;
         webservice string AccountLegalBusinessName;
         webservice string BusinessDescription;
         webservice Integer NumberOfEmployees;
         webservice string PrimaryPhoneNumber;
         webservice string Fax;
         webservice string MobilePhone;
         webservice string TaxIdentificationNumber;
         webservice string NaicsCode;
         webservice string OrganizationID;
         webservice string IncorporationState;
         webservice string YearStarted;
         webservice string OrganizationType;
         webservice string OtherIdentificationType;
         webservice string OtherIdentificationNumber;
         webservice Date OtherIssueDate;
         webservice Date OtherIdentificationExpiryDate;
         webservice string OtherIdentificationState;
         webservice string IPI='';
         webservice string BillingStreet;
         webservice string BillingCity;
         webservice string BillingState;
         webservice string BillingPostalCode;
         webservice string BillingCountry;
         webservice string ShippingStreet;
         webservice string ShippingState;
         webservice string ShippingPostalCode;
         webservice string ShippingCountry;
         webservice list<Contactdata> listContactdata;
         webservice list<Opportunitydata> listOpportunitydata;
         webservice list<Productpackagedata> listProductpackagedata;
         webservice list<Loandata> listLoandata;         
      }
      global class guarantorAccountData{
            webservice string AccountFirstName;
            webservice string AccountECCContactMiddleInitial1;
            webservice string AccountLastName;
            webservice string AccountECCTitle;
            webservice string AccountGuarantorTIN;
            webservice string AccountBirthdate;
            webservice string AccountGuarantorAddress;
            webservice string AccountPrimaryEmail;
            webservice string AccountHomePhone;
            webservice string AccountOtherPhone;
            webservice string AccountMobilePhone;
            
            
      }
      global class Contactdata{ 
         webservice string contactlastname;
      }
      global class Productpackagedata{    
         webservice string Productpackagename;
         webservice string ProductpackageLob;
      }
      global class Feedata{
             webservice string FeeCode;
             webservice string Feetype;
             webservice string FeeAmount;
      }
      global class Opportunitydata{
             webservice string Oppname;
             webservice string estimateclosedate;
             webservice string stagevalue;
             webservice list<LCPIProductdata> listLCPIProductdata;
      }
      global class LCPIProductdata{
             webservice string productname;
      }
      global class EntityInvolvementdata{
              webservice string entityname;
              webservice string borrowertype;
      }
      global class Loandata{
         webservice string loanname;
         webservice string Loanoriginalamount;
         webservice string LoanNewMoney;
         webservice string LoanRequestType;
         webservice list<Feedata> listFeedata;
      }  
      
}