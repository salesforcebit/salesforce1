/*
This handler class will be invoked from Account Trigger. It is created for writing operations related to
KeyBank Equipment Financing Project. This class is written to separate the code to avoid confusion and 
complexity. 
*/
public with sharing class AccountHandler_KEF 
{
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public AccountHandler_KEF(boolean isExecuting, integer size)
    {
        m_isExecuting = isExecuting;
        BatchSize = size;
    } 
    
    //Calculation of Vendor Agreement End Date 
    public void OnBeforeInsert(Account[] triggerNewList)
    {
        if(triggerNewList != null && triggerNewList.size() > 0)
        {
            for(Account acc : triggerNewList)
            {
                //for populating Next Review Date
                AccountHelper_KEF.populateNextReviewDate(acc);
                
                //for populating Is Vendor/ Is Asset Supplier Checkboxes
                AccountHelper_KEF.checkUncheckVendorSupplierCheckbox(acc);
                //for populating vendor/supplier pending credit value
                AccountHelper_KEF.CheckVendorstatusChange(acc);
                AccountHelper_KEF.CheckSupplierstatusChange(acc);
            }   
        }
    }
   //This code is added for Release 3 Req. to create a Funding grid record under Account when Status is changed to Vendor or Supplier.
    public void OnAfterInsert(Account[] triggerNewList, Map<ID, Account> triggerNewMap)
    {
         if(triggerNewMap != null && !triggerNewMap.isEmpty() && triggerNewList != null && triggerNewList.size() > 0)
         {  
            /* START - code added for Req. 245 - Release 3 */
            List<ID> accIdList = new List<ID>();
            Map<id,string> errorMap;
            for(Account acc:triggerNewList)
            {
                //checking if KEF Risk role is equals Vendor or Supplier
                if((acc.KEF_Risk_Role__c == System.Label.Vendor_Supplier) || (acc.KEF_Risk_Role__c == System.Label.Supplier_Only))
                {  
                    accIdList.add(acc.id);
                }     
            }
            
            if(accIdList != null && accIdList.size()>0)
            {
                //insert default Funding Grid record for this Account
                errorMap = AccountHelper_KEF.createFundingGrids(accIdList);
            }
            
            if((errorMap != null && !errorMap.isEmpty()) )
            {
                for(Id accId : errorMap.keyset())
                {
                    if(triggerNewMap.containskey(accId))
                    {
                        Account accRec = triggerNewMap.get(accId);
                        String errorMsg = errorMap.get(accId);
                        accRec.addError(errorMsg);
                    }
                }                       
            }  
        }
          /* END - code added for Req. 245 - Release 3 */  
    }
    /*
    @future public static void OnAfterInsertAsync(Set<ID> newAcceementIDs)
    {
        
    }*/
    
    public void OnBeforeUpdate(Account[] triggerOldList, Account[] triggerNewList, Map<ID, Account> triggerNewMap)
    {
        if(triggerNewMap != null && !triggerNewMap.isEmpty() && triggerOldList != null && triggerOldList.size() > 0)
        {
            for(Account oldAcc : triggerOldList)
            {
                Account newAcc = triggerNewMap.get(oldAcc.id);
                //invoke the method if Account's Risk Role is changed OR Vendor/Supplier status is changed 
                if(newAcc != null && newAcc.KEF_Risk_Role__c != null &&  
                    ((newAcc.KEF_Risk_role__c != oldAcc.KEF_Risk_Role__c) || 
                     (newAcc.KEF_Risk_Role__c == System.Label.Vendor_Supplier && newAcc.Vendor_Status__c != oldAcc.Vendor_Status__c) ||
                     (newAcc.KEF_Risk_Role__c == System.Label.Supplier_Only && newAcc.Supplier_Status__c != oldAcc.Supplier_Status__c)))
                {
                    AccountHelper_KEF.populateNextReviewDate(newAcc);
                }
                
                if(newAcc != null && newAcc.KEF_Risk_role__c != oldAcc.KEF_Risk_Role__C)
                {
                    //for populating Is Vendor/ Is Asset Supplier Checkboxes
                    AccountHelper_KEF.checkUncheckVendorSupplierCheckbox(newAcc);
                }
                //for populating value of vendor pending credit 
                if(newAcc != null && newAcc.Vendor_Status__c != oldAcc.Vendor_Status__c )
                {
                    AccountHelper_KEF.CheckVendorstatusChange(newAcc);
                }
                //for populating value of  supplier pending credit
                if(newAcc != null && newacc.Supplier_Status__c!=oldAcc.Supplier_Status__c)
                {
                    AccountHelper_KEF.CheckSupplierstatusChange(newAcc);                    
                }
            }
        }        
    }
    
    //This code is added for Release 3 Req. to create a Funding grid record under Account when Status is changed to Vendor or Supplier.
    public void OnAfterUpdate(Account[] triggerOldList, Account[] triggerNewList, Map<ID, Account> triggerNewMap)
    {
        if(triggerNewMap != null && !triggerNewMap.isEmpty() && triggerOldList != null && triggerOldList.size() > 0)
        {
            //list of acccount Ids for which FUnding Grid needs to be added.
            List<ID> accIdList = new List<ID>();             
            
            //map with Funding Grid Id as KEY and its Parent Account Id as VALUE, the Funding Grids are no longer marked as Default
            Map<Id, Id> oldDPMFGAccList = new Map<Id, Id>();
            //set of ids which will be sent to method for updating related agreements.
            Set<Id> accUpdatedToIntegrateAgreement = new Set<Id>();
            
            for(Account oldAcc : triggerOldList)
            {
                Account newAcc = triggerNewMap.get(oldAcc.id);
                //if there is no funding grid record added for this account and KEF Risk role is changed, create a new funding grid
                //record for this account                 
                if(newAcc != null && newAcc.Funding_Grid_Count__c == 0 &&
                    newAcc.KEF_Risk_Role__c != oldAcc.KEF_Risk_Role__c &&  
                    ((newAcc.KEF_Risk_Role__c == System.Label.Vendor_Supplier) || (newAcc.KEF_Risk_Role__c == System.Label.Supplier_Only)))
                {
                    accIdList.add(oldAcc.id);                    
                }
                
                /*start - added for testing -  2 nov 2015 - for Req. 252 - Release 3*/
                if(newAcc.Default_Payment_Method__c != oldAcc.Default_Payment_method__c && oldAcc.Default_Payment_method__c != null)
                {
                    //preparing a map of Funding Grid id as key which are no longer default and their Account Ids as value.
                    oldDPMFGAccList.put(oldAcc.Default_Payment_method__c, oldAcc.id);                   
                }
                /*end - added for testing -  2 nov 2015 - for Req. 252 - Release 3*/
                //prearing list of accounts which are not updated by inserting Vendor/Agreement junction or by updating agreement itself 
                if(newAcc.ActiveAgreement__c == oldAcc.ActiveAgreement__c)
                {
                    accUpdatedToIntegrateAgreement.add(newAcc.id);
                }
            }
            
            /*start - mark relted agreement as Not Picked Up when account is updated - Req. 204, 207, 208 - KRR Release 3 */
            Map<Id, String> errorMapAgrUpdate = AccountHelper_KEF.markAgreementAsNotPickedUp(accUpdatedToIntegrateAgreement);
            AccountHelper_KEF.addErrorOverride(errorMapAgrUpdate, triggerNewMap);
            /*end - mark relted agreement as Not Picked Up when account is updated - Req. 204, 207, 208 - KRR Release 3 */
            
            
            /*start - creating a default Funding Grid with Check Funding method - Req. 245 - Release 3*/
            if(accIdList != null && accIdList.size()>0)
            {
                Map<id,string> errorMapFGCreate = AccountHelper_KEF.createFundingGrids(accIdList);
                //show the error on screen
                AccountHelper_KEF.addErrorOverride(errorMapFGCreate, triggerNewMap);
            }                
            /*end - creating a default Funding Grid with Check Funding method - Req. 245 - Release 3*/
            
            /*start - update child Funding Grid as non default payment method - Req. 252 - Release 3*/
            if(oldDPMFGAccList != null && !oldDPMFGAccList.isEmpty())
            {
                Map<id,string> errorMapDPM = AccountHelper_KEF.uncheckDefaultPaymentMethods(oldDPMFGAccList);
                AccountHelper_KEF.addErrorOverride(errorMapDPM, triggerNewMap);
            }
            /*end - update child Funding Grid as non default payment method - Req. 252 - Release 3*/
        }
    }
    
    
    public void OnAfterDelete(Account[] triggerOldList, Map<ID, Account> triggerOldMap)
    {
        if(triggerOldMap != null && !triggerOldMap.isEmpty() && triggerOldList != null && triggerOldList.size() > 0)
        {
            //check for few conditions before deleting the record.
            Map<id,string> errorMap = AccountHelper_KEF.restrictDeletionOnCondition(triggerOldMap.keyset());
            //show an error message to user for restricting from deleting the record,. 
            if((errorMap != null && !errorMap.isEmpty()) )
            {
                for(Id accId : errorMap.keyset())
                {
                    if(triggerOldMap.containskey(accId))
                    {
                        Account accRec = triggerOldMap.get(accId);
                        String errorMsg = errorMap.get(accId);
                        accRec.addError(errorMsg);
                    }
                }                       
            }    
        }  
    }
    
    
    
     /*
    @future public static void OnAfterUpdateAsync(Set<ID> updatedAgreementIDs)
    {
    
    } 
    
    public void OnBeforeDelete(Account[] triggerOldList, Map<ID, Account> triggerOldMap)
    {
         
    }
    
    @future public static void OnAfterDeleteAsync(Set<ID> deletedAgreementIDs)
    {
        
    }
    
    public void OnUndelete(Account[] triggerNewList)
    {
        
    }
    */
    
    /*
    public boolean IsTriggerContext
    {
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext
    {
        get{ return !IsTriggerContext;}
    }*/
}