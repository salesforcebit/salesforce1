public with sharing class ctlr_OppSelectPricing {

	public Opportunity opp {get;set;}
    public List<DepositWrapper> depList {get{
        depList = new List<DepositWrapper>();
        if(standDepartList != null ){
            for(LLC_BI__Deposit__c dep : (List<LLC_BI__Deposit__c>) standDepartList.getRecords() ){
                depList.add(new DepositWrapper(this, dep) );
            }
        System.debug('depList::'+depList);
        }
        System.debug('depList::'+depList);
        return depList;
    }set;}
    public Set<Id> selectedIds {get;set;}
    public Map<Id, LLC_BI__Deposit__c> id2DepMap {get;set;}
    public Boolean showSpecificPric {get;set;}
    public ApexPages.StandardSetController standDepartList {get;set;}
    public Id exceptionPricingQ {get{
        if(exceptionPricingQ == null){
            exceptionPricingQ = [SELECT id FROM Group WHERE DeveloperName = 'Exception_Pricing' AND Type='Queue' ].id;
            exceptionPricingQ = exceptionPricingQ == null ? UserInfo.getUserId() : exceptionPricingQ;
        }
        return exceptionPricingQ;
    }set;}


    /*public Id rpmOperationsQ {get{
        if(rpmOperationsQ == null){
            rpmOperationsQ = [SELECT id FROM Group WHERE DeveloperName = 'RPM_Operations_Queue' AND Type='Queue' ].id;
            rpmOperationsQ = rpmOperationsQ == null ? UserInfo.getUserId() : rpmOperationsQ;
        }
        return rpmOperationsQ;
    }set;}*/


    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public ctlr_OppSelectPricing(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();
        showSpecificPric =false;
        intializeData();
    }

    public void intializeData(){
        selectedIds = new Set<Id>();
        opp = [SELECT id, Name, AccountId FROM Opportunity WHERE Id =:opp.id];
        System.debug('opp::'+opp);
        id2DepMap = new Map<Id, LLC_BI__Deposit__c>((List<LLC_BI__Deposit__c>)Database.query( 'SELECT id, Name, '+ String.join(getFieldNamesList('LLC_BI__Deposit__c', true), ',') +'  FROM LLC_BI__Deposit__c WHERE LLC_BI__Account__c=\''+opp.AccountId+'\'') );
        System.debug('id2DepMap::'+id2DepMap.values());
        standDepartList = new ApexPages.StandardSetController( id2DepMap.values() );
        //standDepartList.setPageSize(25);
    }


    public PageReference relationStandPric(){
        SavePoint sp = Database.setSavepoint();
        try {
            //create the Setup Task
            TSO_Setup_Task__c sTask = new TSO_Setup_Task__c();

            //set the fields
            sTask.Opportunity_Lookup__c = opp.id;
            sTask.OwnerId = exceptionPricingQ;
            sTask.Name = 'Return Relationship to Standard Pricing';
            sTask.Status__c = 'New';
            sTask.Description__c = 'A request has been submitted to return relationship pricing to standard pricing.';
            sTask.Account__c = opp.AccountId;
            sTask.Requested_By__c = UserInfo.getUserId();
            sTask.RPM_Task__c = true;

            insert sTask;


            PageReference pageRef = new PageReference('/'+opp.id);
            pageRef.setRedirect(true);
            return pageRef;
        }catch (Exception ex){
            Database.rollBack(sp);
            System.debug('error::'+ex.getMessage());
            System.debug('ln::'+ex.getLineNumber());
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() ) );
        }
        return null;
    }

    public PageReference specificStandPric(){
        showSpecificPric = true;
        return null;
    }

    public PageReference createDepTasks (){
        //initialize the variables
        SavePoint sp = Database.setSavepoint();
        List<TSO_Setup_Task__c> insertList = new List<TSO_Setup_Task__c>();
        Id userId = UserInfo.getUserId();
        try{
            for(Id depId : selectedIds){
                insertList.add( new TSO_Setup_Task__c( 
                        Deposit__c = depId, 
                        Opportunity_Lookup__c=opp.id,
                        Name = 'Return DDA to Standard Pricing',
                        Status__c = 'New', 
                        Description__c = 'A request has been submitted to return the following DDA\'s to standard pricing: *list out DDA\'s*',
                        Account__c = opp.AccountId,
                        OwnerId = exceptionPricingQ,
                        Requested_By__c = userId,
                        RPM_Task__c = true ) );
            }
            insert insertList;

            //return the page ref to the opportunity
            PageReference pageRef = new PageReference('/'+opp.id);
            pageRef.setRedirect(true);
            return pageRef;
        }catch (Exception ex){
            Database.rollBack(sp);
            System.debug('error::'+ex.getMessage());
            System.debug('ln::'+ex.getLineNumber());
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() ) );
        }
        return null;
    }

    public class DepositWrapper{
        public LLC_BI__Deposit__c dep {get;set;}
        public ctlr_OppSelectPricing ctlr {get;set;}
        public Boolean isSelected {get{
            return ctlr.selectedIds.contains(dep.id);
        } set{
            if(value){//checkbox is true
                ctlr.selectedIds.add(dep.id);
            } else {
                ctlr.selectedIds.remove(dep.id);
            }
        }}
        public DepositWrapper (ctlr_OppSelectPricing c, LLC_BI__Deposit__c newD){
            dep = newD;
            ctlr = c;
        }
    }

    public  List<string> getFieldNamesList(string objectTypeName, boolean removeSystemFields){
        Set<string> tSet =  Schema.getGlobalDescribe().get(objectTypeName).getDescribe().fields.getMap().keySet();
        if(removeSystemFields){ 
          tSet.removeAll(new List<String>{'name','setupownerid','id','isdeleted','ownerid','createdbyid','createddate','lastmodifiedbyid','lastmodifieddate','systemmodstamp', 'sourceid'}); 
        }
        return new list<string>(tSet);
    }
}