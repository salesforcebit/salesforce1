@isTest
public with sharing class TestECCP_MemoApprovalController {
	
    @isTest
    public static void loadLocked(){
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
    }
    
    @isTest
    public static void loadUnlocked(){
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
    }
    
    @isTest
    static void save() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.memoSave();
        
        Test.stopTest();
    }
    
    @isTest
    public static void cancel() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.cancel();
            
        Test.stopTest();
    }
    
    @isTest
    public static void delegateAuthority() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getdelegatedAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority1() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority2() {
        init();
        Account testAccount = createAccount('10');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void creditAuthority3() {
        init();
        Account testAccount = createAccount('17');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getcreditAuthorityUsers();
            
        Test.stopTest();
    }
    
    @isTest
    static void reviewer1() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getreviewer1();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer2() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getreviewer2();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer3() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getreviewer3();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer4() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getreviewer4();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer5() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getreviewer5();
        
        Test.stopTest();
    }
    
    @isTest
    static void reviewer6() {
        init();
        Account testAccount = createAccount('1');
        LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        LLC_BI__Credit_Memo_Modifcation__c testMemo = createMemo(testLoan.Id, 'Not Approved');
        
        ApexPages.currentPage().getParameters().put('id', testMemo.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_MemoApprovalController MAC = new ECCP_MemoApprovalController(controller);
        
        Test.startTest();
        
        MAC.getreviewer6();
        
        Test.stopTest();
    }
    
    public static void init() {
        LLC_BI__CFG_ConfigValue__c testConfigValue1 = createConfigValue('Change Memo', 'Yes', 'CM_Enabled');
        LLC_BI__CFG_ConfigValue__c testConfigValue2 = createConfigValue('Change Memo', 'Change Memo', 'CM_Name');
        LLC_BI__CFG_ConfigKey__c testConfigKey = createConfigKey('KY_GBL_ChangeMemo');
        LLC_BI__CFG_CKJ__c testJoin1 = createJoin(testConfigValue1.Id, testConfigKey.Id);
        LLC_BI__CFG_CKJ__c testJoin2 = createJoin(testConfigValue2.Id, testConfigKey.Id);
    }
    
    private static LLC_BI__CFG_ConfigValue__c createConfigValue(String category,
                                                                String fieldvalue,
                                                                String key) {
        LLC_BI__CFG_ConfigValue__c newCV = new LLC_BI__CFG_ConfigValue__c(
        	LLC_BI__Category__c = category,
        	LLC_BI__fieldValue__c = fieldValue,
        	LLC_BI__Key__c = key,
        	LLC_BI__lookupKey__c = key);
        
        insert newCV;
		return newCV;        
    }
    
    private static LLC_BI__CFG_ConfigKey__c createConfigKey(String name) {
        LLC_BI__CFG_ConfigKey__c newCK = new LLC_BI__CFG_ConfigKey__c(
        	Name = name,
        	LLC_BI__lookupKey__c = name);
        
        insert newCK;
        return newCK;
    }
    
    private static LLC_BI__CFG_CKJ__c createJoin(Id configValueId, Id configKeyId) {
        LLC_BI__CFG_CKJ__c newJoin = new LLC_BI__CFG_CKJ__c(
        	LLC_BI__CFG_ConfigKeys__c = configKeyId,
        	LLC_BI__CFG_ConfigValue__c = configValueId);
        
        insert newJoin;
        return newJoin;
    }
    
    private static Account createAccount(String grade) {
        Account newA = new Account(
        	Name = 'Test',
        	Proposed_Risk_Rating__c = grade);
        
        insert newA;
        return newA;
    }
    
    private static LLC_BI__Product_Package__c createProductPackage(Id acctId) {
        LLC_BI__Product_Package__c newPP = new LLC_BI__Product_Package__c(
        	Name = 'Test',
        	LLC_BI__Account__c = acctId,
            LLC_BI__New_Money__c = 0,
        	LLC_BI__Primary_Officer__c = UserInfo.getUserId());
        
        insert newPP;
        return newPP;
    }
    
    private static LLC_BI__Loan__c createLoan(Id acctId, Id productPackageId) {
        LLC_BI__Loan__c newL = new LLC_BI__Loan__c(
        	Name = 'Test Loan',
            LLC_BI__Amount__c = 10000,
        	LLC_BI__Account__c = acctId,
        	LLC_BI__Product_Package__c = productPackageId);
        
        insert newL;
        return newL;
    }
    
    private static LLC_BI__Credit_Memo_Modifcation__c createMemo(Id loanId, String status) {
        LLC_BI__Credit_Memo_Modifcation__c newM = new LLC_BI__Credit_Memo_Modifcation__c(
        	LLC_BI__Loan__c = loanId,
        	ECC_New_Money__c = 0,
        	LLC_BI__Approval_Status__c = status,
        	LLC_BI__Memo_Type__c = 'Major Credit Action');
        
        insert newM;
        return newM;
    }
    
    
    
}