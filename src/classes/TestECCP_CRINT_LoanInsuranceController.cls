@isTest

public class TestECCP_CRINT_LoanInsuranceController {

	@isTest
    static void start() {
        Account testAccount = createAccount();
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id);
        ECC_Loan_Insurance_Mgmt__c testInsurance = createLoanInsurance();
        ECC_Loan_Insurance__c  testLoanInsuranceLink = createLoanInsuranceLink(testLoan.id, testInsurance.Id);
        
        ApexPages.currentPage().getParameters().put('id', testLoan.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CRINT_LoanInsuranceController ecasc = new ECCP_CRINT_LoanInsuranceController(controller);
    }
    
    @isTest
    static void viewLoanInsurance() {
        Account testAccount = createAccount();
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id);
        
        ECC_Loan_Insurance_Mgmt__c testInsurance = createLoanInsurance();
        ECC_Loan_Insurance__c  testLoanInsuranceLink = createLoanInsuranceLink(testLoan.id, testInsurance.Id);
        
        ApexPages.currentPage().getParameters().put('id', testLoan.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CRINT_LoanInsuranceController ecasc = new ECCP_CRINT_LoanInsuranceController(controller);
        
        Test.startTest();
        
        ecasc.loanInsuranceId = testInsurance.Id;
        ecasc.viewLoanInsurance();
        
        Test.stopTest();
    }
    
    @isTest
    static void back() {
        Account testAccount = createAccount();
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id);

        ECC_Loan_Insurance_Mgmt__c testInsurance = createLoanInsurance();
        ECC_Loan_Insurance__c  testLoanInsuranceLink = createLoanInsuranceLink(testLoan.id, testInsurance.Id);        
        ApexPages.currentPage().getParameters().put('id', testLoan.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CRINT_LoanInsuranceController ecasc = new ECCP_CRINT_LoanInsuranceController(controller);
        
        Test.startTest();
        
        ecasc.loanInsuranceId = testInsurance.Id;
        ecasc.viewLoanInsurance();
        ecasc.back();
        
        Test.stopTest();
    }
    
    private static ECC_Loan_Insurance_Mgmt__c  createLoanInsurance() {
        ECC_Loan_Insurance_Mgmt__c newLI = new ECC_Loan_Insurance_Mgmt__c(
        	ECC_Credit_Insurance__c  = 'Yes',
        	ECC_Insured_Names_First_Middle_Last__c   = 'George W Bush',
        	ECC_Policy_Holder__c   = 'Policy Holder',
        	ECC_Quote_Amount__c   = 10000.00,
            ECC_Quote_Type__c  = 'Type of Quote'
        
        );
        
        insert newLI;
        return newLI;
    }    
    private static LLC_BI__Loan__c createLoan(Id acctId) {
        LLC_BI__Loan__c newL = new LLC_BI__Loan__c(
        	Name = 'Test',
        	LLC_BI__Account__c = acctId);
        
        insert newL;
        return newL;
    }
 
     private static ECC_Loan_Insurance__c  createLoanInsuranceLink(Id loanId, Id mgmtId) {
        ECC_Loan_Insurance__c newL = new ECC_Loan_Insurance__c(
        	ECC_Loan__c  = loanId,
        	ECC_Loan_Insurance_Mgmt__c  = mgmtId);
        
        insert newL;
        return newL;
    }
    
    private static Account createAccount() {
        Account newA = new Account(
        	Name = 'Test');
        
        insert newA;
        return newA;
    }
    

    

}