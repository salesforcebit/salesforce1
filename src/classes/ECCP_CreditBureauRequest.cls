/*******************************************************************************************************************************************
* @name         :   ECCP_CreditBureauRequest
* @description  :   This class is used to prepare the request for service call
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/

public class ECCP_CreditBureauRequest{
    
    public ECCP_CreditBureauRequest(){} //require for test class
    public string SSN;
    public string Orchestration_Code;
    public string AccountName;
    public string BillingStreet;
    public string BillingCity;
    public string BillingState;
    public string BillingPostalcode;
    public string FirstName;
    public string LastName;
    public String Birthdate;
    public string HomePhone;
    public string MobilePhone;
}