/*******************************************************************************************************************************************
* @name         :   ECCP_MCA_CheckMCI_Request
* @description  :   This class is request wrapper for MCA request
* @author       :   Ashish Agrawal
* @createddate  :   23/03/2016
*******************************************************************************************************************************************/
public class ECCP_MCA_CheckMCI_Response{
    public ECCP_MCA_CheckMCI_Response(){
        
    }
    public String Proceed;
    public String SupplCode;
    public String AddDescInd;
    public String Pass;
    public String Result;
    public String CIURefNb;
    public String CIURefSq;
    public String SLADay;
    public String SLAHour;
    public String SLAMin;
    public String ErrorTp;
    public String ErrorCd;
    public String ErrorDsc;
    public String CustType;
    public String BusName;
    public String FrstName;
    public String MdlName;
    public String LastName;
    public String FraudActiveAlert;
    public String AddressAlert;
    public String OFACResult;
    public String LNRisk1;
    public String LNRisk2;
    public String LNRisk3;
    public String LNRisk4;
    public String LNRisk5;
    public String LNRisk6;
    public String RegOPass;
    public String RegWPass;
    public String PA326Pass;
    public String OFACPass;
    public String HotFilePass;
    public String FAAlertPass;
    public String AddrCodeField;
    public String Untitled;
    public String error;  //to capture salesforce callout error       
}