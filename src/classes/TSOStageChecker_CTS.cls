public class TSOStageChecker_CTS {
    /*public static Boolean Fullfillmentcheck(LLC_BI__Treasury_Service__c treasuryService){
        if(treasuryService != null && treasuryService.LLC_BI__Stage__c =='Fulfillment'){
            if(treasuryService.Is_this_a_KTT_client__c){
                return true;
            }
            else{
               
               return false; 
            }
        }
        else {
             return true;
        }
    }*/
    static Map<Id,string> parentexceptionmap = new Map<Id,string>();
    static set<id> parentids = new set<id>();
    public static Map<Id,string> FullfillmentcheckMap(List<TSO_Setup_Task__c> setuptasklist){
       
        for(TSO_Setup_Task__c tst:setuptasklist){
            if(tst.Status__c =='complete'){
            	parentids.add(tst.Treasury_Service__c);
            }    
        }
        system.debug('parentids'+parentids);
        TreasuaryStages__mdt ts  = [Select Id,ACH__c, Is_KTT_Checked__c from TreasuaryStages__mdt Where DeveloperName = 'New_Fulfillment'];
        for(LLC_BI__Treasury_Service__c lts: [Select Id, LLC_BI__Stage__c,Is_this_a_KTT_client__c from LLC_BI__Treasury_Service__c Where Id in: parentids]){
            system.debug('ssss');
            // system.debug('ssss'+TRSatges__c.getinstance(system.label.Fulfillment_stage).value__c);
            if(lts.Is_this_a_KTT_client__c == ts.Is_KTT_Checked__c){
                system.debug('vvvvv');
                parentexceptionmap.put(lts.id,'Please provide the list  values on Treasuary service :IS  KTT check got cheked');
            }
            if(!lts.Is_this_a_KTT_client__c && lts.LLC_BI__Stage__c =='Fulfillment 2'){
                system.debug('ffff');
                parentexceptionmap.put(lts.id,'Please provide the list  values on Treasuary service :IS  KTT check got cheked');
            }
        }
        system.debug('parentexceptionmap'+parentexceptionmap);
        return parentexceptionmap;
    }
}