@isTest
public class UnitTestFactory {

	public static Account buildTestAccount() {
		Account a = new Account();
		a.Name = 'Test Account';
		a.Delivery_Method__c = 'test';
		return a;
	}

	public static Account buildTestAccount(Id depositId) {
		Account a = buildTestAccount();
		a.Billing_Account_Number__c = depositId;
		a.Bank_Number__c = depositId;
		return a;
	}

	public static Assumptions_and_Clarifications__c buildTestAssumptionsAndClarifications(Id scenarioId){
		Assumptions_and_Clarifications__c aac = new Assumptions_and_Clarifications__c();
		aac.Scenario__c = scenarioId;
		aac.Description__c = 'Test';
		aac.Description2__c = 'Test2';
		aac.Name = 'Test Assumption';

		return aac;
	}

	public static Contact buildTestContact(Id accountId) {
		Contact c = new Contact();
		c.FirstName = 'Test';
		c.LastName = 'Contact';
		c.AccountId = accountId;
		return c;
	}

	public static Group buildTestQueue() {
		Group g = new Group();
		g.Name = 'queue' + uniqueNumber;
		g.DeveloperName = 'queue' + uniqueNumber;
		g.Type = 'Queue';
		return g;
	}

	public static QueueSObject buildTestQueueSObject(Id queueId, String sObjectType) {
		QueueSObject q = new QueueSObject();
		q.QueueId = queueId;
		q.SObjectType = sObjectType;
		return q;
	}

	public static Opportunity buildTestOpportunity() {
		Opportunity o = new Opportunity();
		o.Name = 'Test Opportunity';
		o.StageName = 'Pursue';
		o.CloseDate = date.today().addDays(7);
		return o;
	}

	public static Opportunity buildTestDepositsECPOpportunity() {
		Opportunity o = buildTestOpportunity();
		o.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Deposits & ECP').getRecordTypeId();
		return o;
	}

	public static OpportunityLineItem buildTestOpportunityLineItem(Opportunity o, PricebookEntry pbe) {
		OpportunityLineItem oli = new OpportunityLineItem();
		oli.OpportunityId = o.Id;
		oli.PricebookEntryId = pbe.Id;
		return oli;
	}

	public static PricebookEntry buildTestPricebookEntry(Product2 p) {
		PricebookEntry pbe = new PricebookEntry();
		pbe.isActive = true;
		pbe.UnitPrice = 1;
		pbe.Product2Id = p.Id;
		pbe.Pricebook2Id = testPricebookId;
		return pbe;
	}

	public static final Id testPricebookId = test.getStandardPricebookId();

	public static Product2 buildTestProduct() {
		Product2 p = new Product2();
		p.Name = 'Test Product';
		p.Product_Category__c = 'Test Category';
		return p;
	}

	public static LLC_BI__Analyzed_Account__c buildTestAnalyzedAccount(Id treasuryServiceId, Id depositId) {
		LLC_BI__Analyzed_Account__c a = new LLC_BI__Analyzed_Account__c();
		a.LLC_BI__Treasury_Service__c = treasuryServiceId;
		a.LLC_BI__Deposit_Account__c = depositId;
		return a;
	}

	public static LLC_BI__Authorized_User__c buildTestAuthorizedUser(Id treasuryServiceId, Id contactId) {
		LLC_BI__Authorized_User__c a = new LLC_BI__Authorized_User__c();
		a.LLC_BI__Treasury_Service__c = treasuryServiceId;
		a.LLC_BI__Contact_Reference__c = contactId;
		return a;
	}

	public static LLC_BI__Deposit__c buildTestDeposit(Id accountId) {
		LLC_BI__Deposit__c d = new LLC_BI__Deposit__c();
		d.LLC_BI__Account__c = accountId;
		d.LLC_BI__Account_Number__c = String.valueOf(uniqueNumber);
		d.Bank_Number__c = String.valueOf(uniqueNumber);
		return d;
	}

	public static LLC_BI__Legal_Entities__c buildTestLegalEntities(Id accountId, Id treasuryServiceId){
		LLC_BI__Legal_Entities__c le = new LLC_BI__Legal_Entities__c();
		le.Name = 'Test Entity';
		le.LLC_BI__Account__c = accountId;
		le.LLC_BI__Treasury_Service__c = treasuryServiceId;
		return le;
	}


	public static LLC_BI__Treasury_Service__c buildTestTreasuryService() {
		LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c();
		return ts;
	}

	public static LLC_BI__Treasury_Service__c buildTestTreasuryService(Id accountId) {
		LLC_BI__Treasury_Service__c ts = buildTestTreasuryService();
		ts.LLC_BI__Relationship__c = accountId;
		return ts;
	}


	public static LLC_BI__Treasury_Service__c buildTestTreasuryService(Id productId, Id productPackageId) {
		LLC_BI__Treasury_Service__c ts = buildTestTreasuryService();
		ts.LLC_BI__Product_Reference__c = productId;
		ts.LLC_BI__Product_Package__c = productPackageId;
		return ts;
	}

	public static LLC_BI__Treasury_Service__c buildTestTreasuryService(Id productId, Id productPackageId, Id accountId) {
		LLC_BI__Treasury_Service__c ts = buildTestTreasuryService(productId, productPackageId);
		ts.LLC_BI__Relationship__c = accountId;
		return ts;
	}

	public static LLC_BI__Product__c buildTestNCinoProduct(Product2 p, LLC_BI__Product_Type__c pt) {
		LLC_BI__Product__c product = new LLC_BI__Product__c();
		product.Related_SFDC_Product__c = p.Id;
		product.LLC_BI__Product_Type__c = pt.Id;
		return product;
	}

	public static LLC_BI__Product_Line__c buildTestProductLine() {
		LLC_BI__Product_Line__c pl = new LLC_BI__Product_Line__c();
		pl.Name = 'Deposits & ECP';
		return pl;
	}

	public static LLC_BI__Product_Package__c buildTestProductPackage() {
		LLC_BI__Product_Package__c pp = new LLC_BI__Product_Package__c();
		return pp;
	}

	public static LLC_BI__Product_Type__c buildTestProductType(LLC_BI__Product_Line__c pl) {
		LLC_BI__Product_Type__c pt = new LLC_BI__Product_Type__c();
		pt.LLC_BI__Product_Line__c = pl.Id;
		return pt;
	}

	public static TSO_Stage__c buildTestTSOStage(Id workflowTemplateId) {
		TSO_Stage__c s = new TSO_Stage__c();
		s.Stage_IDL_ID__c = String.valueOf(uniqueNumber);
		s.Workflow_Template__c = workflowTemplateId;
		return s;
	}

	public static TSO_Stage__c buildTestTSOStage(Id workflowTemplateId, String treasuryStage) {
		TSO_Stage__c s = buildTestTSOStage(workflowTemplateId);
		s.Related_Treasury_Stage__c = treasuryStage;
		s.Name = treasuryStage;
		return s;
	}

	public static TSO_Stage__c buildTestTSOStage(Id workflowTemplateId, String treasuryStage, Integer daysToComplete) {
		TSO_Stage__c s = buildTestTSOStage(workflowTemplateId, treasuryStage);
		s.Expected_Days_to_Complete__c = daysToComplete;
		return s;
	}

	public static TSO_Setup_Task__c buildTestTSOSetupTask(Id treasuryServiceId, Id setupTaskTemplateId) {
		TSO_Setup_Task__c t = new TSO_Setup_Task__c();
		t.Treasury_Service__c = treasuryServiceId;
		t.Setup_Task_Template__c = setupTaskTemplateId;

		t.parent_Task__c = true;
		return t;
	}

	public static TSO_Setup_Task_Template__c buildTestTSOSetupTaskTemplate(Id stageId) {
		TSO_Setup_Task_Template__c t = new TSO_Setup_Task_Template__c();
		t.Parent_Setup_Task__c = true;
		t.Parent_Stage__c = stageId;
		return t;
	}

	public static TSO_Workflow_Template__c buildTestTSOWorkflowTemplate(Id productId, String typeValue) {
		TSO_Workflow_Template__c wt = new TSO_Workflow_Template__c();
		wt.SLA__c = 1;
		wt.Product__c = productId;
		wt.Type__c = typeValue;
		return wt;
	}

	public static TSO_Workflow_Template__c buildTestTSOWorkflowTemplate(Id productId, String typeValue, Integer sla) {
		TSO_Workflow_Template__c wt = buildTestTSOWorkflowTemplate(productId, typeValue);
		wt.SLA__c = sla;
		return wt;
	}

	public static Task buildTestTask(){
		Task t = new Task();

		return t;
	}

	public static LLC_BI__Scenario__c buildTestScenario(Opportunity o) {
		LLC_BI__Scenario__c scenario = new LLC_BI__Scenario__c();
		scenario.LLC_BI__Opportunity__c = o.Id;
        scenario.Name = 'test scenario';
        scenario.LLC_BI__Status__c = 'Draft';
        scenario.Renewal_Type__c = 'Pricing Modification Renewal';
        scenario.Exception_Pricing_Expiration_Date2__c = Date.today().addYears(1);
        return scenario;
	}

	public static LLC_BI__Scenario_Item__c buildTestScenarioItem(LLC_BI__Scenario__c scenario) {
		LLC_BI__Scenario_Item__c item = new LLC_BI__Scenario_Item__c();
		item.LLC_BI__Scenario__c = scenario.Id;
		item.Name = 'Service';
		item.LLC_BI__Product__c = 'Product';
		item.LLC_BI__Standard_Price__c = 1.00;
		return item;
	}

	public static Users_and_Accounts__c buildTestUsersAndAccounts(LLC_BI__Authorized_User__c user, LLC_BI__Analyzed_Account__c acct) {
		Users_and_Accounts__c uaa = new Users_and_Accounts__c();
		uaa.Authorized_User__c = user.Id;
		uaa.Analyzed_Account__c = acct.Id;
		uaa.Select_Service_Option_Type__c = acct.Select_Service_Option_Type__c;
		uaa.Service_Options__c = acct.Service_Options__c;
		return uaa;
	}

	public static Integer uniqueNumber {get {
		if(uniqueNumber == null) uniqueNumber = 0;
		else uniqueNumber++;
		return uniqueNumber;
	}set;}
}