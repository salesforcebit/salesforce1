@isTest
private class RenewalPageExtensionTest {

	/*
`	Removed since Return to Standard Pricing is no longer a valid value for Scenario
	@isTest static void testStandardPricing() {
		Account testAcc = UnitTestFactory.buildTestAccount();
		insert testAcc;

		Opportunity testOppty = UnitTestFactory.buildTestOpportunity();
		testOppty.AccountId = testAcc.Id;
		insert testOppty;

		Product2 testProduct = UnitTestFactory.buildTestProduct();
		insert testProduct;

		PricebookEntry testPbe = UnitTestFactory.buildTestPricebookEntry(testProduct);
		insert testPbe;

		OpportunityLineItem testOli = UnitTestFactory.buildTestOpportunityLineItem(testOppty,testPbe);
		insert testOli;

		LLC_BI__Scenario__c testScenario = UnitTestFactory.buildTestScenario(testOppty);
		insert testScenario;

		LLC_BI__Scenario_Item__c testScenarioItem = UnitTestFactory.buildTestScenarioItem(testScenario);
		insert testScenarioItem;

		PageReference pageRef = Page.RenewalPage;

		Test.startTest();
		Test.setCurrentPage(pageRef);
		RenewalPageExtension extension = new RenewalPageExtension(
				new ApexPages.StandardController(testScenario));
		extension.renewalType = 'Return to Standard Pricing';
		extension.processRenewals();
		System.assertEquals(3,extension.renewalTypes.size());
		Test.stopTest();

		List<LLC_BI__Scenario__c> scenarios = [SELECT Name, LLC_BI__Status__c, Renewal__c,
				Renewal_Type__c, Exception_Pricing_Expiration_Date2__c, (SELECT LLC_BI__Exception_Price__c
				FROM LLC_BI__Scenario_Items__r)
				FROM LLC_BI__Scenario__c WHERE Id !=: testScenario.Id];
		System.assertEquals(1,scenarios.size());
		System.assertEquals(1,scenarios[0].LLC_BI__Scenario_Items__r.size());
		System.assertEquals(null,scenarios[0].LLC_BI__Scenario_Items__r[0].LLC_BI__Exception_Price__c);
		System.assert(scenarios[0].Name.contains('Renewal'));
		System.assertEquals('Draft',scenarios[0].LLC_BI__Status__c);
		System.assertEquals(true,scenarios[0].Renewal__c);
		System.assertEquals('Return to Standard Pricing',scenarios[0].Renewal_Type__c);
		System.assertEquals(Date.today().addYears(1),scenarios[0].Exception_Pricing_Expiration_Date2__c);
	}*/

	@isTest static void testExistingPricing() {
		Account testAcc = UnitTestFactory.buildTestAccount();
		insert testAcc;

		Opportunity testOppty = UnitTestFactory.buildTestOpportunity();
		testOppty.AccountId = testAcc.Id;
		insert testOppty;

		Product2 testProduct = UnitTestFactory.buildTestProduct();
		insert testProduct;

		PricebookEntry testPbe = UnitTestFactory.buildTestPricebookEntry(testProduct);
		insert testPbe;

		OpportunityLineItem testOli = UnitTestFactory.buildTestOpportunityLineItem(testOppty,testPbe);
		insert testOli;

		LLC_BI__Scenario__c testScenario = UnitTestFactory.buildTestScenario(testOppty);
		insert testScenario;

		LLC_BI__Scenario_Item__c testScenarioItem = UnitTestFactory.buildTestScenarioItem(testScenario);
		insert testScenarioItem;

		PageReference pageRef = Page.RenewalPage;

		Test.startTest();
		Test.setCurrentPage(pageRef);
		RenewalPageExtension extension = new RenewalPageExtension(
				new ApexPages.StandardController(testScenario));
		extension.renewalType = 'Renew Existing Pricing';
		List<SelectOption> options = extension.renewalTypes;
		extension.showAlert();
		extension.closeAlert();
		extension.processRenewals();
		Test.stopTest();

		List<LLC_BI__Scenario__c> scenarios = [SELECT Name, LLC_BI__Status__c, Renewal__c,
				Renewal_Type__c, Exception_Pricing_Expiration_Date2__c, (SELECT Id FROM LLC_BI__Scenario_Items__r)
				FROM LLC_BI__Scenario__c WHERE Id !=: testScenario.Id];
		System.assertEquals(1,scenarios.size());
		System.assertEquals(1,scenarios[0].LLC_BI__Scenario_Items__r.size());
		System.assert(scenarios[0].Name.contains('Renewal'));
		System.assertEquals('Draft',scenarios[0].LLC_BI__Status__c);
		System.assertEquals(true,scenarios[0].Renewal__c);
		System.assertEquals('Renew Existing Pricing',scenarios[0].Renewal_Type__c);
		System.assertEquals(Date.today().addYears(1),scenarios[0].Exception_Pricing_Expiration_Date2__c);
	}

	@isTest static void testPricingMod() {
		Account testAcc = UnitTestFactory.buildTestAccount();
		insert testAcc;

		Opportunity testOppty = UnitTestFactory.buildTestOpportunity();
		testOppty.AccountId = testAcc.Id;
		insert testOppty;

		Product2 testProduct = UnitTestFactory.buildTestProduct();
		insert testProduct;

		PricebookEntry testPbe = UnitTestFactory.buildTestPricebookEntry(testProduct);
		insert testPbe;

		OpportunityLineItem testOli = UnitTestFactory.buildTestOpportunityLineItem(testOppty,testPbe);
		insert testOli;

		LLC_BI__Scenario__c testScenario = UnitTestFactory.buildTestScenario(testOppty);
		insert testScenario;

		LLC_BI__Scenario_Item__c testScenarioItem = UnitTestFactory.buildTestScenarioItem(testScenario);
		insert testScenarioItem;

		PageReference pageRef = Page.RenewalPage;

		Test.startTest();
		Test.setCurrentPage(pageRef);
		RenewalPageExtension extension = new RenewalPageExtension(
				new ApexPages.StandardController(testScenario));
		extension.renewalType = 'Pricing Modification Renewal';
		extension.processRenewals();
		Test.stopTest();

		List<Opportunity> opportunities = [SELECT Parent_Opportunity_Id__c, (SELECT Id
				FROM OpportunityLineItems) FROM Opportunity WHERE Id !=: testOppty.Id];
		System.assertEquals(1,opportunities.size());
		System.assertEquals(1,opportunities[0].OpportunityLineItems.size());
		System.assertEquals(testOppty.Id,opportunities[0].Parent_Opportunity_Id__c);

		List<LLC_BI__Scenario__c> scenarios = [SELECT Name, LLC_BI__Status__c, Renewal__c,
				Renewal_Type__c, Exception_Pricing_Expiration_Date2__c, (SELECT Id FROM LLC_BI__Scenario_Items__r)
				FROM LLC_BI__Scenario__c WHERE Id !=: testScenario.Id];
		System.assertEquals(1,scenarios.size());
		System.assertEquals(1,scenarios[0].LLC_BI__Scenario_Items__r.size());
		System.assert(scenarios[0].Name.contains('Renewal'));
		System.assertEquals('Draft',scenarios[0].LLC_BI__Status__c);
		System.assertEquals(true,scenarios[0].Renewal__c);
		System.assertEquals('Pricing Modification Renewal',scenarios[0].Renewal_Type__c);
		System.assertEquals(Date.today().addYears(1),scenarios[0].Exception_Pricing_Expiration_Date2__c);
	}

}