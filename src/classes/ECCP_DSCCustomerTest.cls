@isTest
public class ECCP_DSCCustomerTest{
    static LLC_BI__Product_Package__c prod = new LLC_BI__Product_Package__c() ;
    
    public static testMethod void callDSCButton(){
        
        List<LLC_BI__Loan__c>  lonList = ECCP_CreateLoanUtil.CreateLoanData(1,1);
         Profile p = [SELECT Id FROM Profile WHERE Name='Integration/Data Migration']; 
         User u = new User(Alias = 'stdInt', Email='standarduser@testorg.comInt', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comInt',PLOB__c='Support');
        insert u;
        System.runAs(u){
        account acc = new account(Name = 'Test Account',Type ='Business Prospect');
        insert acc;
        LLC_BI__Product_Package__c pp = new LLC_BI__Product_Package__c();
          pp.Name = acc.name;
          PP.Bank_Division__c ='Business Banking';
          pp.LLC_BI__Account__c = acc.id;
          pp.LLC_BI__Stage__c = 'Application Processing';
          pp.LLC_BI__Primary_Officer__c = u.Id;
          pp.Credit_Authority_Name__c =u.Id;
          pp.Delegated_Authority_Name__c  = u.Id;
        insert pp;
        System.currentPageReference().getParameters().put('id', lonList[0].LLC_BI__Product_Package__c );
        system.debug('---lonList[0].LLC_BI__Product_Package__c ---'+lonList[0].LLC_BI__Product_Package__c );
        Apexpages.StandardController stdController = new Apexpages.StandardController(prod);
        test.startTest();
        
        ECCP_DSCCustomerButton buton = new ECCP_DSCCustomerButton(stdController);
        buton.servicCall();
        System.currentPageReference().getParameters().put('id', pp.id);
        ECCP_DSCCustomerButton butonexcp = new ECCP_DSCCustomerButton(stdController);
        butonexcp.servicCall();
        
        }
        ECCP_DSCCustomerWS_Helper cust = new ECCP_DSCCustomerWS_Helper();
        cust.getService();
        test.stopTest();
    
    } 
    
}