/**
 * Provides a strongly-typed intermediate object for converting the JSON representation
 * of a Product Type to its sObject equivalent.
 */
public class ProductType extends nFORCE.AForce {
	public String sid;
	public String name;
	public String lookupKey;
	public String productLine;
	public Boolean isDeleted;
	public List<Product> products;

	public ProductType() {
		this.products = new List<Product>();
	}

	public ProductType(String sid, String name, String lookupKey,
			Boolean isDeleted) {
		this.sid = sid;
		this.name = name;
		this.lookupKey = lookupKey;
		this.isDeleted = isDeleted;
		this.products = new List<Product>();
	}

	public LLC_BI__Product_Type__c getSObject() {
		LLC_BI__Product_Type__c productType = new LLC_BI__Product_Type__c();
		this.mapToDb(productType);
		return productType;
	}

	public static ProductType buildFromDB(sObject obj) {
		ProductType productType = new ProductType();
		productType.mapFromDB(obj);
		return productType;
	}

	public override Type getType() {
		return ProductType.class;
	}

	public override Schema.SObjectType getSObjectType() {
		return LLC_BI__Product_Type__c.sObjectType;
	}

	public override void mapFromDb(sObject obj) {
		LLC_BI__Product_Type__c productType = (LLC_BI__Product_Type__c)obj;
		if (productType != null) {
			this.sid = productType.ID;
			this.name = productType.Name;
			this.lookupKey = productType.LLC_BI__lookupKey__c;
			this.productLine = productType.LLC_BI__Product_Line__c;
		}
	}

	public override void mapToDb(sObject obj) {
		LLC_BI__Product_Type__c productType = (LLC_BI__Product_Type__c)obj;
		if (productType != null) {
			if (this.sid != null && this.sid.length() > 0) {
				productType.Id = Id.valueOf(this.sid);
			}
			productType.Name = this.name;
			productType.LLC_BI__Usage_Type__c = ProductCatalogController.TS_USAGE_TYPE;
			productType.LLC_BI__lookupKey__c = this.lookupKey;
		}
	}
}