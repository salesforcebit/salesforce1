@isTest
public with sharing class ScenarioProductTriggerHandler_UT {
    
    static void setup()
    {
        LLC_BI__Scenario__c scenario = [Select Id from LLC_BI__Scenario__c LIMIT 1];
        LLC_BI__Product__c prod = [Select Id from LLC_BI__Product__c LIMIT 1];
        
        Scenario_Product__c scp = new Scenario_Product__c(Name=' AJ TEST 1', Product__c = prod.Id, Scenario__c = scenario.Id);
        //insert scp;
        
        //Scenario_Product__c scp1 = [select]
    }
    
    /*static testMethod void setIncrementalValueTest()
    {
        Scenario_Product__c scp1 = [select id, Scenario__r.Id, Product__c, Scenario__c, Total_Incremental_Bookable_Revenue__c, fee_equivalent_revenue__c,
          Name, Fee_Equivalent_Revenue__c, Scenario__r.LLC_BI__Opportunity__c from Scenario_Product__c  LIMIT 1];
        ScenarioProductTriggerHandler th = new ScenarioProductTriggerHandler();
        List<Scenario_Product__c> scps = new List<Scenario_Product__c>();
        scps.add(scp1); 
        th.setIncrementalValue(scps, null);
        
    } */
    
    static testMethod void testScenarioProductBeforeInsertTriggerPos()
    {
        Account acc = UnitTestFactory.buildTestAccount();
        acc.KBCM_Customer_Number__c = '98765';
        acc.Name = 'Test Account';
        acc.Average_Deposit_Balance__c = 50;
        acc.Service_Charges__c = 50;
        insert acc;
        Id accId = acc.Id;
        System.debug(acc.Name);
        
        Opportunity o = UnitTestFactory.buildTestOpportunity();
        o.AccountId = acc.Id;
        o.Name = 'Scenario Test';
        insert o;
        
        Id oppId = o.Id;

        Product2 prod = UnitTestFactory.buildTestProduct();
        prod.Name = 'Scenario Product Test';
        prod.Family = 'Deposits & ECP';
        insert prod;
        
        LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
        insert pl;
        LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
        insert pt;

        LLC_BI__Product__c np = UnitTestFactory.buildTestNCinoProduct(prod, pt);
        insert np;
        
        Id scenarioRecordTypeId = Schema.SObjectType.LLC_BI__Scenario__c.getRecordTypeInfosByName().get('DDA Proforma').getRecordTypeId();
        LLC_BI__Scenario__c scenario = new LLC_BI__Scenario__c(recordTypeId=scenarioRecordTypeId, name='ScenarioOppProdStatus1', LLC_BI__Opportunity__c=oppId,
        Deposit_Balance__c=100.00, ServiceCharges_Eligible_4_EarningsCredit__c=100);
        insert scenario;
        
        LLC_BI__Product_Package__c pp = UnitTestFactory.buildTestProductPackage();
        insert pp;
        LLC_BI__Treasury_Service__c ts = UnitTestFactory.buildTestTreasuryService(np.Id, pp.Id, accId);
        //ts.Prior_Month_Product_Revenue__c = 5;
        insert ts;
        
        
        Scenario_Product__c scp = new Scenario_Product__c(Name=' AJ TEST 1', Product__c = np.Id, Scenario__c = scenario.Id, fee_equivalent_revenue__c = 50,
        Total_Bookable_Product_Revenue__c = 50);
        insert scp;
        //System.assertEquals(50, scp.Total_Incremental_Revenue__c);
    }
    
    static testMethod void testScenarioProductBeforeInsertTriggerNeg()
    {
        Account acc = UnitTestFactory.buildTestAccount();
        acc.KBCM_Customer_Number__c = '98765';
        acc.Name = 'Test Account';
        acc.Average_Deposit_Balance__c = 50;
        acc.Service_Charges__c = 50;
        insert acc;
        Id accId = acc.Id;
        System.debug(acc.Name);
        
        Opportunity o = UnitTestFactory.buildTestOpportunity();
        o.AccountId = acc.Id;
        o.Name = 'Scenario Test';
        insert o;
        
        Id oppId = o.Id;

        Product2 prod = UnitTestFactory.buildTestProduct();
        prod.Name = 'Scenario Product Test';
        prod.Family = 'Deposits & ECP';
        insert prod;
        
        Product2 prod1 = UnitTestFactory.buildTestProduct();
        prod1.Name = 'Scenario Product Test';
        prod1.Family = 'Deposits & ECP';
        insert prod1;
        
        LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
        insert pl;
        LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
        insert pt;

        LLC_BI__Product__c np = UnitTestFactory.buildTestNCinoProduct(prod, pt);
        insert np;
        
        LLC_BI__Product__c np1 = UnitTestFactory.buildTestNCinoProduct(prod1, pt);
        insert np1;
        
        Id scenarioRecordTypeId = Schema.SObjectType.LLC_BI__Scenario__c.getRecordTypeInfosByName().get('DDA Proforma').getRecordTypeId();
        LLC_BI__Scenario__c scenario = new LLC_BI__Scenario__c(recordTypeId=scenarioRecordTypeId, name='ScenarioOppProdStatus1', LLC_BI__Opportunity__c=oppId,
        Deposit_Balance__c=100.00, ServiceCharges_Eligible_4_EarningsCredit__c=100);
        insert scenario;
        
        LLC_BI__Product_Package__c pp = UnitTestFactory.buildTestProductPackage();
        insert pp;
        LLC_BI__Treasury_Service__c ts = UnitTestFactory.buildTestTreasuryService(np.Id, pp.Id, accId);
        //ts.Prior_Month_Product_Revenue__c = 5;
        insert ts;
        
        
        Scenario_Product__c scp = new Scenario_Product__c(Name=' AJ TEST 1', Product__c = np1.Id, Scenario__c = scenario.Id, fee_equivalent_revenue__c = 50,
        Total_Bookable_Product_Revenue__c = 50);
        insert scp;
        //System.assertEquals(50, scp.Total_Incremental_Revenue__c);
        //System.assertEquals(50, scp.Total_Incremental_Bookable_Revenue__c);
    }
    
}