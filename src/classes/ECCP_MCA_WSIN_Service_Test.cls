@isTest
public class ECCP_MCA_WSIN_Service_Test{
        
    public static testmethod void MCATest(){
        ECCP_MCA_WSIN_Service_Test.createData();
        ECCP_MCA_WSIB_Request reqobj = new ECCP_MCA_WSIB_Request();
        reqobj.ApplNbr = 'MCA123';
        reqobj.AcctNbr = '61923';
        reqobj.CIURefNbr = 'MCAZ';
        reqobj.CIURefSeqNbr = 'MCA9';
        reqobj.MCIPassFail = 'PASS';
        reqobj.MCAPassPendResponseFlag = True;
        User u = [select id from User where UserName='standarduser@testorg.comInt'];
        System.runAs(u){
        ECCP_MCA_WSIN_Service.loanRequest(reqobj);
        }
        
        }
         
    public static testMethod void MCA_WBIN_NullInput(){
        ECCP_MCA_WSIN_Service_Test.createData();
        ECCP_MCA_WSIB_Request reqobj = new ECCP_MCA_WSIB_Request();
        User u = [select id from User where UserName='standarduser@testorg.comInt'];
        System.runAs(u){
        ECCP_MCA_WSIN_Service.loanRequest(reqobj);
        }
        }
        
    public static testMethod void MCA_WBIN_InvalidInput(){
        ECCP_MCA_WSIN_Service_Test.createData();
        ECCP_MCA_WSIB_Request reqobj = new ECCP_MCA_WSIB_Request();
        reqobj.ApplNbr = 'MCA123';
        reqobj.AcctNbr = '61923';
        reqobj.CIURefNbr = 'MCABC';
        reqobj.CIURefSeqNbr = 'MCA968797900000';
        reqobj.MCIPassFail = 'PASS';
        reqobj.MCAPassPendResponseFlag = True;
        User u = [select id from User where UserName='standarduser@testorg.comInt'];
        System.runAs(u){
        ECCP_MCA_WSIN_Service.loanRequest(reqobj);
        }
        }
    public static testMethod void MCA_WBIN_QueryResponseError(){
        ECCP_MCA_WSIN_Service_Test.createData();
        ECCP_MCA_WSIB_Request reqobj = new ECCP_MCA_WSIB_Request();
        reqobj.ApplNbr = 'MCA123';
        reqobj.AcctNbr = '61923';
        reqobj.CIURefNbr = 'MCABC';
        reqobj.CIURefSeqNbr = 'MCA9';
        reqobj.MCIPassFail = 'PASS';
        reqobj.MCAPassPendResponseFlag = True;
        User u = [select id from User where UserName='standarduser@testorg.comInt'];
        System.runAs(u){
        ECCP_MCA_WSIN_Service.loanRequest(reqobj);
        }
    }
    public static void createData(){
        List<LLC_BI__Loan__c>  lonList = ECCP_CreateLoanUtil.CreateLoanData(1,1);
       /* Profile p = [SELECT Id FROM Profile WHERE Name='Integration/Data Migration']; 
        User u = new User(Alias = 'stdInt', Email='standarduser@testorg.comInt', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comInt',PLOB__c='Support');
        insert u; */
        createUser();
        User u = [select id from User where UserName='standarduser@testorg.comInt'];
        System.runAs(u){
            ECCP_LogSetting__c plog = new ECCP_LogSetting__c();
            plog.Name= 'PerformanceExceptionLogEnabled';
            plog.isEnable__c = true;
            insert plog;
            ECCP_LogSetting__c plogNew = new ECCP_LogSetting__c();
            plogNew.Name = 'SaveRequest_Response';
            plogNew.isEnable__c = true;
            insert plogNew;
            ECCP_Compliance__c comp = new ECCP_Compliance__c();
            comp.MCA_Reference_ID__c = 'MCAZ';
            comp.MCAStatus__c = 'Pend';
            comp.Loan_Name__c = lonList[0].Id;
            insert comp;
        }
    }
    
    public static User createUser(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Integration/Data Migration']; 
        User u = new User(Alias = 'stdInt', Email='standarduser@testorg.comInt', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comInt',PLOB__c='Support');
        insert u;
        return u;
    }

}