/*******************************************************************************************************************************************
* @name         :   ECCP_CreditBureauResponse
* @description  :   This class is used to prepare the response for service call
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/
public class ECCP_CreditBureauResponse{

    public ECCP_CreditBureauResponse(){} // Require for test class
    public string SSN;
   // public Integer Orchestration_Code;
   public String Orchestration_Code;
    //public integer Credit_Bureau_ID;
    public string Credit_Bureau_ID;
   // public integer CreditScore;
   public String CreditScore;
    public string Credit_Bureau_Reason_Code_1;
    //public integer Credit_Bureau_Reason_Code_1;
    public string Credit_Bureau_Reason_Code_Description_1;
    public string Credit_Bureau_Reason_Code_2;
    public string Credit_Bureau_Reason_Code_Description_2;
    public string Credit_Bureau_Reason_Code_3;
    public string Credit_Bureau_Reason_Code_Description_3;
    public string Credit_Bureau_Reason_Code_4;
    public string Credit_Bureau_Reason_Code_Description_4;
    public string Credit_Bureau_Reason_Code_5;
    public string Credit_Bureau_Reason_Code_Description_5;
    public string Credit_Bureau_FA_Request_Type;
    public string Bureau_Fraud;
    public string Patriot_Fraud_Indicator;                          
    public string Credit_Bureau_Addr_Discrepancy;
  //  public Date Credit_Active_Date;
  //  public Date ReportDate;  
    public String Credit_Active_Date;
    public String ReportDate;             
    public String Credit_Report;
    public string ID_Scan_Fraud_Code; 
    public string Hawk_Alert_Fraud_Code;
    public string errorcode;
    public string errordesc;
    //Added by Aditya 2August2016 : Added for Release 1.1 : To Add BureauInstruct1 and BureauInstruct2 fields in response
    public string BureauInstruct1;
    public string BureauInstruct2;
}