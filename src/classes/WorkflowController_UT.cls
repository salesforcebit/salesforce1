@isTest
private class WorkflowController_UT {
	
	@isTest static void test_method_one() {
		// Implement test code
		Test.startTest();
		WorkflowController controller = new WorkflowController();
		List<LLC_BI__Product_Type__c> prodType = WorkflowController.getProducts();
		//List<String> stringList = controller.availableWorkflowObjects;
		Test.stopTest();

		System.assertequals(1, prodType.size() );
	}
	
	@testSetup static void testSetupMethod() {
		// Implement test code
		LLC_BI__Deposit__c da = new LLC_BI__Deposit__c();
        da.Name = 'TestDA2';
        da.Bank_Number__c = '1234';
        da.LLC_BI__Account_Number__c = '999999';
        insert da;
		TSO_Cost_Center__c tcc = new TSO_Cost_Center__c();
		tcc.Charge_Class__c = 'CN';
		insert tcc;
		Account acc = new Account();
		acc.Name = 'TestAccount';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		acc.Cost_Center__c = tcc.Id;
        acc.Billing_Account_Number__c = da.Id;
		acc.Bank_Number__c = da.Id;
		acc.Delivery_Method__c = 'I';
		insert acc;
		LLC_BI__Deposit__c testDA  = [SELECT Id, Name FROM LLC_BI__Deposit__c limit 1];
		// testDA.LLC_BI__Account_Number__c = '5792874523';
		// insert testDA;
		Opportunity oppty = new Opportunity();
		oppty.Name = 'TestOppty';
		oppty.AccountId = acc.Id;
		oppty.CloseDate = System.Today();
		oppty.StageName = 'Pursue';
		oppty.Opportunity_Type__c = 'Addl Product';
		insert oppty;
		Product2 SFprod = UnitTestFactory.buildTestProduct();
		SFprod.Name = 'Scenario Test';
		SFprod.Family = 'Deposits & ECP';
		insert SFprod;
		LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
		insert pl;
		LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
		pt.LLC_BI__Usage_Type__c = 'Treasury Management';
		insert pt;
	}
	
}