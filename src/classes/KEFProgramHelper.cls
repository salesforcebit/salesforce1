/*
This class is written for KEF KRR Project by TCS Team

It is a helper class written for Program object
*/

public with sharing class KEFProgramHelper 
{
    //this set is a list of status values which are used for Program Approval trends report - Req. 148
    public static Set<String> PROGRAM_APPROVAL_TRENDS_STATUS_1 = new Set<String>
    {
        System.Label.Active, System.Label.ActiveHuntingLicense, System.Label.VendorCommitteeDeclined, System.Label.SubmittoVendorCommittee
    }; 
    
    //Method for changing ProgramName which will update the AgreementName - Req. 179    
    public static map<id,string> UpdateAgreementNameBasedonProgramName(Set<id> pgmsets)
    {
        List<Agreement__c> Agglist = new List<Agreement__c>();
        List<Program__c> newPgms = new Program__c[]{};
        Map<id,string> errorstr = new Map<id,string>();
        
        newPgms = [SELECT Id, Name_of_the_Vendor_Program__c,
                        (SELECT id,Agreement_Name_UDF__c,Agreement_Type__c, Default_lessor__c,Program__c,Agreement_Name__c 
                            FROM Agreements__r )
                    FROM Program__c 
                    WHERE Id in :pgmsets];    
        
        for(Program__c pgm:newPgms) 
        { 
            if(pgm.Name_of_the_Vendor_Program__c!=null)
            {
                for(Agreement__c objagg : pgm.Agreements__r) 
                {  
                    if(objagg.Agreement_Name_UDF__c == null)
                    {
                         objagg.Agreement_Name__c = pgm.Name_of_the_Vendor_Program__c+'.'+ objagg.Agreement_Type__c + '.' + 
                                                        objagg.Default_lessor__c+'.'+'..';                       
                    }
                    else if(objagg.Agreement_Name_UDF__c!=null)
                    {
                        objagg.Agreement_Name__c = pgm.Name_of_the_Vendor_Program__c +'.'+ objagg.Agreement_Type__c + '.' + 
                                                        objagg.Default_lessor__c+'.'+objagg.Agreement_Name_UDF__c ;
                        system.debug('value of agg name---'+objagg.Agreement_Name__c);
                    }
                    Agglist.add(objagg);
                }
            }
        } 
     
        /* if(!Agglist.isEmpty())
        {
            update Agglist; 
            system.debug('list of agg----'+Agglist);
        }*/
     
        if(Agglist.size()>0)
        {
            List<Database.SaveResult> updateResults;
            updateResults = Database.update(Agglist, false);
        
            for(Integer i=0;i<updateResults.size();i++)
            {
                if (updateResults.get(i).isSuccess())
                {
                    updateResults.get(i).getId();
                }
                else if (!updateResults.get(i).isSuccess())
                {
                    // DML operation failed
                    Database.Error error = updateResults.get(i).getErrors( ).get(0);
                    String failedDML = error.getMessage();
                    system.debug('failedDML-->   '+failedDML);
                    //Agglist.get(i).adderror('failed');//failed record from the list
                    system.debug('Failed ID'+Agglist.get(i).Id);
                    system.debug('Failed IDProgram__c-->   '+Agglist.get(i).Program__c);
                    String errorMsg = System.Label.AgreementNameUpdateError + ' ' + (failedDML != null ? failedDML : '');
                    errorstr.put( Agglist.get(i).Program__c,errorMsg);
                }
            }
        } 
        return errorstr; 
    }

    //this method will populate a map of Wrapper with fields to update
    public static Map<String, KEFReportFieldPopulator> populateMapForStatusChangeAudit()
    {
        Map<String, KEFReportFieldPopulator> statusChangeAuditMap = new Map<String, KEFReportFieldPopulator>();
        
        //executing the constructor for wrapper 'KEFReportFieldPopulator' to assign field values
        statusChangeAuditMap.put(System.Label.ActiveHuntingLicense, 
            new KEFReportFieldPopulator(System.Label.ProgramStatusAPIName, System.Label.ActiveHuntingLicense,  
                true, KEFSystemConstants.DT_ACTIVEHUNTINGLICENSE_API, true, KEFSystemConstants.UL_ACTIVEHUNTINGLICENSE_API));
                
        statusChangeAuditMap.put(System.Label.SubmittoCredit, 
            new KEFReportFieldPopulator(System.Label.ProgramStatusAPIName, System.Label.SubmittoCredit,  
                true, KEFSystemConstants.DT_SUBMITTOCREDIT_API, true, KEFSystemConstants.UL_SUBMITTOCREDIT_API));
                
        statusChangeAuditMap.put(System.Label.Active, 
            new KEFReportFieldPopulator(System.Label.ProgramStatusAPIName, System.Label.Active,  
                true, KEFSystemConstants.PROGRAM_DT_ACTIVE_API, true, KEFSystemConstants.PROGRAM_UL_ACTIVE_API));
                
        statusChangeAuditMap.put(System.Label.AnyStatusChange, 
            new KEFReportFieldPopulator(System.Label.ProgramStatusAPIName, System.Label.AnyStatusChange,  
                true, KEFSystemConstants.DT_ANYSTATUSCHANGE_API, true, KEFSystemConstants.UL_ANYSTATUSCHANGE_API));
                
        statusChangeAuditMap.put(KEFSystemConstants.PROGRAM_APPROVAL_TREND_1, 
            new KEFReportFieldPopulator(System.Label.ProgramStatusAPIName, KEFSystemConstants.PROGRAM_APPROVAL_TREND_1,  
                true, KEFSystemConstants.DT_PROGAPPROVALTRENDS_API, true, KEFSystemConstants.UL_PROGAPPROVALTRENDS_API));
        
        return statusChangeAuditMap;
    }
    
    //base method for populating Datetime and User id values for Program Record based on Status changes 
    public static void processProgramforDRDataPopulation(Program__c newProg, Map<String, KEFReportFieldPopulator> statusValueMap)
    {
        if(newProg != null)
        {
            //checking if the current Status value exists in the map for which date and user needs to be populated
            if(statusValueMap != null && statusValueMap.containsKey(newProg.Program_Status__c))
            {
                //populate date and User Id values for the status change
                populateStatusDateAndUserId(newProg, statusValueMap.get(newProg.Program_Status__c));
            }                   
            //checking if current Program status equals to any one of the Program Approval Trend statuses. 
            if(KEFPRogramHelper.PROGRAM_APPROVAL_TRENDS_STATUS_1.contains(newProg.Program_Status__c) &&
            statusValueMap.containsKey(KEFSystemConstants.PROGRAM_APPROVAL_TREND_1))
            {
                //populating date and user id for Program Approval Trend status values 
                populateStatusDateAndUserId(newProg, statusValueMap.get(KEFSystemConstants.PROGRAM_APPROVAL_TREND_1));  
            }                   
            //populating the Date and User Id for Any Status change for Program
            populateStatusDateAndUserId(newProg, statusValueMap.get(System.Label.AnyStatusChange));
        }
    } 

    //this method will populate the Program object with Date and User Id
    public static void populateStatusDateAndUserId(Program__c programRecord, KEFReportFieldPopulator wrapperIns)
    {
        if(programRecord != null && wrapperIns != null)
        {
            //checking if date-time data population is required
            if(wrapperIns.populateDateTimeValue && wrapperIns.dateTimeFieldAPIName != null)
            {
                programRecord.put(wrapperIns.dateTimeFieldAPIName, System.Now());
            }
            //checking if user id population is required
            if(wrapperIns.populateUserLookupValue && wrapperIns.userLookupFieldAPIName != null)
            {
                programRecord.put(wrapperIns.userLookupFieldAPIName, UserInfo.getUserId());
            }
        }
    } 
    
    
    //method for creation of 3 entries for Notes and Attachment  as part of july release CR-1410
    public static void  UpdateNotesAndAttachment(Program__c[] pgm)
    {
     list<Private_Note_Attachment__c> noteList = new list<Private_Note_Attachment__c>();
        if(!Test.isRunningTest()){
            for(Program__c objpgm :pgm){
                Private_Note_Attachment__c Att= new Private_Note_Attachment__c();
                Att= new Private_Note_Attachment__c (name='Vendor Committee Info',KEF_Program__c= objpgm.id);
                noteList.add(Att);
                Att= new Private_Note_Attachment__c (name='Program Launch Info',KEF_Program__c= objpgm.id);
                noteList.add(Att);
                Att= new Private_Note_Attachment__c (name='Sales and Report Info',KEF_Program__c= objpgm.id);
                noteList.add(Att);
             }
         }
     insert noteList ; 
        
    } 
}