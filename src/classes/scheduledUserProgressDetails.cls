/*
This class schedules the 'UserProgressDetails' Apex class, which updates the User Progress Details like login count, call count and 
stale opportunities for all users.
*/
global class scheduledUserProgressDetails implements Schedulable {

    global void execute(SchedulableContext SC) {
        UserProgressDetails userDetails = new UserProgressDetails();
    }
}