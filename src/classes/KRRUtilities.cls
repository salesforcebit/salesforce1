public with sharing class KRRUtilities 
{
	public static DateTime fetchRandomDateTimeValue()
	{
		Integer randomNumber = Math.round(Math.random()*1000) + Math.round(Math.random()*100) + Math.round(Math.random());
		return system.now().addHours(randomNumber);
	}
}