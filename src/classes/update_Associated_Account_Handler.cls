//================================================================================
//  KeyBank  
//  Object: Call
//  Author: Offshore
//  Detail: update_Associated_Account_Handler
//  Description : To Update Associated Account Id for Related To fields in Calls.
//                To stamp the Calls Created date in Program Meber record which is a lookup to Calls Associated Account

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================

public class update_Associated_Account_Handler
{
  
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    private static boolean run = true;
    
    public update_Associated_Account_Handler(boolean isExecuting, integer size)
    {    
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    public void onAfterInsert(call__c[] newCalls,call__c[] listtoUpdate)
    {    
     try{
       
        for(call__c c:newCalls)
        {
        
            //For Account call        
            if(c.Record_Type_Name__c == 'Account_Call')
            {
                list<call__c> callList = new list<call__c>();
                System.debug(+listtoUpdate.size());
                for(call__c callrec:listtoUpdate)
                {
                    System.debug('Inside For Loop');
                    call__c newCall = new call__c(id=callrec.id);
                    newCall.Associated_Account__c = callrec.AccountId__c;
                    callList.add(newCall);
                    System.debug(+callList.size());
                }
                update callList;
                } 
                
            // For Contact call            
            if(c.Record_Type_Name__c == 'Contact_Call')
            {
                list<call__c> callcontList = new list<call__c>();
                for(call__c concallList :listtoUpdate )
                {
                    call__c newconCall = new call__c(id=concallList.id);
                    newconCall.Associated_Account__c = concallList.ContactId__r.Accountid;
                    Id contid= concallList.ContactId__r.Accountid;
                    System.debug(+contid);
                    callcontList.add(newconCall );
                    System.debug(+callcontList.size());
                }
                update callcontList;
    
            }    
    
            // For Lead call            
            if(c.Record_Type_Name__c == 'Lead_Call')
            {
                list<call__c> callleadList = new list<call__c>();
                for(call__c leadcallList :listtoUpdate )
                {
                    If(leadcallList.LeadId__r.IsConverted==false)
                   {
                    call__c newleadCall = new call__c(id=leadcallList.id);
                    newleadCall.Associated_Account__c = leadcallList.LeadId__r.Associated_Account__c;                    
                    callleadList.add(newleadCall);
                    System.debug(+callleadList .size());
                    }
                }
                update callleadList;            
            } 
    
            // For Opportunity call            
            if(c.Record_Type_Name__c == 'Opportunity_Call')
            {
                list<call__c> callOpptyList = new list<call__c>();
                for(call__c OpptycallList : listtoUpdate )
                {
                    call__c newOpptyCall = new call__c(id=OpptycallList.id);
                    newOpptyCall.Associated_Account__c = OpptycallList.OpportunityId__r.Accountid;                    
                    callOpptyList.add(newOpptyCall);
                    System.debug(+callOpptyList.size());
                }
                update callOpptyList;   
            }
        }
       }
       catch(DMLException e){
           System.debug('Error: '+ e);
         }
    }
    
    public void onAfterUpdate(call__c[] newCalls,call__c[] listtoUpdate){
    
    try{
       for(call__c c:newCalls)
        {
        
            //For Account call        
            if(c.Record_Type_Name__c == 'Account_Call')
            {
                list<call__c> callList = new list<call__c>();
                System.debug(+listtoUpdate.size());
                for(call__c callrec:listtoUpdate)
                {
                    System.debug('Inside For Loop');
                    call__c newCall = new call__c(id=callrec.id);
                    newCall.Associated_Account__c = callrec.AccountId__c;
                    callList.add(newCall);
                    System.debug(+callList.size());
                }
                update callList;
                } 
                
            // For Contact call            
            if(c.Record_Type_Name__c == 'Contact_Call')
            {
                list<call__c> callcontList = new list<call__c>();
                for(call__c concallList :listtoUpdate )
                {
                    call__c newconCall = new call__c(id=concallList.id);
                    newconCall.Associated_Account__c = concallList.ContactId__r.Accountid;
                    Id contid= concallList.ContactId__r.Accountid;
                    System.debug(+contid);
                    callcontList.add(newconCall );
                    System.debug(+callcontList.size());
                }
                update callcontList;
    
            }    
    
            // For Lead call            
            if(c.Record_Type_Name__c == 'Lead_Call')
            {
                list<call__c> callleadList = new list<call__c>();
                for(call__c leadcallList :listtoUpdate )
                {
                   If(leadcallList.LeadId__r.IsConverted==false)
                   {
                    call__c newleadCall = new call__c(id=leadcallList.id);
                    newleadCall.Associated_Account__c = leadcallList.LeadId__r.Associated_Account__c;                    
                    callleadList.add(newleadCall);
                    System.debug(+callleadList .size());
                    }
                }
                update callleadList;            
            } 
    
            // For Opportunity call            
            if(c.Record_Type_Name__c == 'Opportunity_Call')
            {
                list<call__c> callOpptyList = new list<call__c>();
                for(call__c OpptycallList : listtoUpdate )
                {
                    call__c newOpptyCall = new call__c(id=OpptycallList.id);
                    newOpptyCall.Associated_Account__c = OpptycallList.OpportunityId__r.Accountid;                    
                    callOpptyList.add(newOpptyCall);
                    System.debug(+callOpptyList.size());
                }
                update callOpptyList;   
            }
        }
       }
       catch(DMLException e){
           System.debug('Error: '+ e);
         } 
    
    }

/*public void Pcupdate(map<id,call__c> callmap,List<call__c> calllist)
{
List<pc__c> pc = new List<pc__c>();
List<Account> acc=[SELECT Id,name,(Select id from Program_Member__r) from account where id in(Select AccountId__c from Call__c where id in :callmap.keySet())];
    
    for(Call__c call: calllist)
    {
        for(Account a : acc)
        {
            If(a.Program_Member__r!=null)
            {
            for(program_member__c p: a.Program_Member__r)
            {
                pc__c p1 = new pc__c();
                p1.Account__c=a.Id;
                p1.Call__c=Call.id;
                p1.Program_Member__c=p.id;
                pc.add(p1);
            }
        }

    }
  }  
insert pc;
}*/
    public static void prmupdate(map<Id,call__c> prlist,List<call__c> calllist)
    {
        List<program_member__c> pc = new List<program_member__c>();
        List<Account> acc=[SELECT Id,name,(Select id from Program_Member__r) from account where id in(Select Associated_Account__c from Call__c where id in :prlist.keySet())];
        try 
        {   
            for(Call__c call: calllist)
            {
                for(Account a : acc)
                {
                    If(a.Program_Member__r!=null)
                    {
                        for(program_member__c p: a.Program_Member__r)
                        {
                            program_member__c p1 = new program_member__c();
                            p1.id=p.id;
                            p1.Last_Call_Date__c=call.Call_Date__c;
                            pc.add(p1);
                        } 
                    }
                }
            }
            update pc;
        }
        catch (NullPointerException e)
        {
            system.debug(e.getMessage());
        }
        catch (DMLException e1)
        {
            system.debug(e1.getMessage());
        }
    }
  
    public static boolean runOnce()
    {
        if(run)
        {
            run=false;
            return true;
        }
        else
        {
            return run;
        }
    }
}