@istest
Public class Test_OpportuntunityProductExtension
{
    static Account acc;
    static Program__c objpgm;
    static Agreement__c objagg;
    static opportunity KEFopty;
    static Product2 prd;
    static KEF_Product__c lstproduct;
    static PricebookEntry pre;
    static Opportunitylineitem olirec;
    static Opportunitylineitem olirec1;

Public static testmethod void optyprod()
{

         acc = new account (name='test 13234');
        recordtype rt = [Select Id, name from recordtype where name ='Deposits & ECP'];
        
        // Creating a Test Oppty of type Deposits & ECP
        opportunity opty = new opportunity(
        name ='test',
        AccountId = acc.id,
        closedate=system.today(),
        StageName='Pursue',
        RecWeb_Number__c = '12589',
        recordtypeId=rt.id);        
        insert opty;
        
        PageReference testPage = Page.OpportunityProductCustomPage;
        Test.setCurrentPage(testPage);
        System.currentPageReference().getParameters().put('Id',opty.Id); 
        ApexPAges.StandardController sc = new ApexPages.StandardController(opty);
        OpportuntunityProductExtension opptyprodExten= new OpportuntunityProductExtension(sc); 
        
        Recordtype rtname =[Select Id,Name From RecordType where id =:opty.recordtypeId ]; 
        
        
         prd = new Product2(Name='Test', Family='Deposits & ECP', Product_category__c='Receivables Products',IsActive=true);
        insert prd;
        
        // Passing Oppty record id 
        opptyprodExten.theOpp=opty;
        Id pricebookId = Test.getStandardPricebookId();
         pre = new PricebookEntry(Pricebook2Id=pricebookId,IsActive=true, UnitPrice =7658, Product2Id=prd.id);
        insert pre;
        priceBookEntry availprod=[select Id, Name,Pricebook2Id, IsActive, Product2.Name, Product2.Family,Product2.Product_Category__c,Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where PriceBookEntry.Product2.Family =:rtname.name limit 1];   
        // Step 1: Testing search functionality which finds nothing
        opptyprodExten.searchString = 'Test';
        opptyprodExten.SearchFilter = 'Product2.Name';
        opptyprodExten.updateAvailableList();
        //system.assert(opptyprodExten.AvailableProducts.size()==0);
        
        
        // Step 2: Testing search functionality which finds results
        opptyprodExten.searchString = availprod.Name;
        opptyprodExten.updateAvailableList();
        system.assert(opptyprodExten.AvailableProducts.size()>0); 
        
        // Step 3:  Testing by adding products to Shopping Cart 
        Integer startCount = opptyprodExten.ShoppingCart.size();
        
        opptyprodExten.toSelect = opptyprodExten.AvailableProducts[0].Id;
        opptyprodExten.addToShoppingCart();
        system.assert(opptyprodExten.shoppingCart.size()!=startCount);   
        
        //Step 4: we know that there is at least one line item, so we confirm
        startCount = opptyprodExten.ShoppingCart.size();
        system.assert(startCount>0);
        
        //Step 4: Saving the product to the oppty and confirm no error message
        opptyprodExten.onSave();
        system.assert(ApexPages.getMessages().size()==0);  
        
        //Step 5: Confirming the Oppty Product(OLI) has been created
        test.starttest();
        opportunityLineItem[] olicreated = [select Id from opportunityLineItem where OpportunityId = :opty.id];
      //  system.assert(olicreated.size()==startCount);
        
        //Step 6:  Saving and redirecting to the same page to add new porduct
        opptyprodExten.saveAndNew();
     //   System.assert(opptyprodExten.saveAndNew()!=null); 
        
        //Step 7: Saving and redirecting the user to the Oppty page on clicking cancal button in pop up
        opptyprodExten.newProdCancel();     
        
        //Step 8:  Redirecting the user to the base oppty page on click of cancel
        opptyprodExten.onCancel();
     //   System.assert(opptyprodExten.onCancel()!=null);
        test.stoptest();
}

public static testmethod void opptyproddetailExten_Test()
{

        Account acc1 = new account (name='test 13234');
        recordtype rt = [Select Id, name from recordtype where name ='Deposits & ECP'];
        // Creating a Test Oppty of type Deposits & ECP
        opportunity opty1 = new opportunity(
        name ='test',
        AccountId = acc1.id,
        closedate=system.today(),
        StageName='Pursue',
        RecWeb_Number__c = '12589',
        recordtypeId=rt.id);        
        insert opty1;
        // Querying for standard pricebook ID
        
        //Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1]; 
        
         prd = new Product2(Name='Test', Family='Deposits & ECP', Product_category__c='Receivables Products',IsActive=true);
        insert prd;     
        Id pricebookId1 = Test.getStandardPricebookId();
        PricebookEntry pre1 = new PricebookEntry(Pricebook2Id=pricebookId1,IsActive=true, UnitPrice =7658, Product2Id=prd.id);
        insert pre1;
        
        
        // Creating a Test OLi 
        
         olirec = new Opportunitylineitem (
        Opportunityid=opty1.id,
        PriceBookEntryID=pre1.id,
        quantity=4, totalprice=1);
        
        insert olirec;
        
        Test.startTest();
        
        PageReference testPg = Page.Opportunity_product_detail_custompage;
        Test.setCurrentPage(testPg );
    
        ApexPAges.StandardController stdcon = new ApexPages.StandardController(olirec);
        opportunity_product_detail_Extension opptyprod = new opportunity_product_detail_Extension(stdcon );
        Id oliId= olirec.Opportunity.id  ;
        
        opptyprod.onCancel();
        
        Test.stopTest();
        

}
//Added new method for KEF Opty Jan Release 
Public static testmethod void KEFoptyprod()
{
        List<OpportunityLineItem> toInsert = new List<OpportunityLineItem>();
        List<Picklist_Transformation__c> insertPicklist =new List<Picklist_Transformation__c>();
        Picklist_Transformation__c PF;
        Picklist_Transformation__c PT;
         PF= new Picklist_Transformation__c(Code__c='4',Picklist_Field__c='PaymentFrequency',DefaultDescription__c='Monthly');
         insertPicklist.add(PF);        
         PT= new Picklist_Transformation__c(Code__c='1393',Picklist_Field__c='Payment Type',DefaultDescription__c='Advance');
         insertPicklist.add(PT);      
        insert insertPicklist;
        
        //created testaccount
        acc = new Account();
        acc.name = 'Test Account';
        //acc.recordtypeId = accRTId;
        acc.BillingStreet = 'dajgfrewg';
        acc.BillingCity = 'dajgfrewg';
        acc.BillingState = 'dajgfrewg';
        acc.BillingPostalCode = 'dajgfrewg';
        acc.BillingCountry = 'dajgfrewg';
        insert acc;

        recordtype rt = [Select Id, name from recordtype where name ='KEF Opportunity'];
        //created testprogram
        objpgm=new Program__c();
        objpgm.Program_Status__c=System.Label.Prospect;
        objpgm.Name_of_the_Vendor_Program__c='Testprogram';
        objpgm.Business_Development_Stage__c=System.label.BuisnessStage;
        insert objpgm;
        
        //craeted testagreement
        objagg=new Agreement__c();
        objagg.Agreement_Name_UDF__c='TestUDF';
        objagg.Agreement_Legal_Status__c=System.Label.informal;
        objagg.Agreement_Type__c=System.label.other;
        objagg.Agreement_Status__c=System.label.Prospect;
        objagg.Generate_invoices_to_the_servicer__c='Yes';
        objagg.Servicer_Name__c='Testnme';
        objagg.Third_Party_Name_for_Property_Tax_Servic__c='Test2';
        objagg.Third_Party_Name_for_Sales_Tax_Servicing__c='Test1';
        objagg.Default_lessor__c=System.Label.KEF;
        objagg.Program__c=objpgm.id;
        objagg.Agreement_Term__c = 12;
        objagg.Agreement_Commencement_Date__c = System.Today();
        insert objagg;


        // Creating a Test Oppty of type KEF Opportunity
        KEFopty = new opportunity();
        KEFopty.name ='testKEF';
        KEFopty.AccountId = acc.id;
        KEFopty.closedate=system.today();
        KEFopty.StageName='Pursue';
        KEFopty.Volume__c=23;
        KEFopty.Vendor_Agreement__c=objagg.id;
        KEFopty.recordtypeId=rt.id;        
        insert KEFopty;

        Recordtype rtname =[Select Id,Name From RecordType where id =:KEFopty.recordtypeId ]; 

        //created testproduct
        prd = new Product2();
        prd.Name='TestKEFProduct';
        prd.Family='KEF';
        prd.Product_category__c='Conditional Lease';
        prd.IsActive=true;
        insert prd;
        
        //created kefproduct
         lstproduct=new KEF_Product__c();
        //lstproduct.Product_Category__c='Lease';
        lstproduct.Product_Association__c='testKEF';
        lstproduct.product__c= prd.id;
        lstproduct.Agreement__c=KEFopty.Vendor_Agreement__c;
        insert lstproduct;

        // Passing Oppty record id 

        Id pricebookId = Test.getStandardPricebookId();
        pre = new PricebookEntry();
        pre.Pricebook2Id=pricebookId;
        pre.IsActive=true;
        pre.UnitPrice =7658;
        pre.Product2Id=prd.id;
        insert pre;

        //craeted test oli with primary value true
        olirec = new Opportunitylineitem();
        olirec.Opportunityid=KEFopty.id;
        olirec.PriceBookEntryID=pre.id;
        olirec.Payment_Frequency__c='Monthly';
        olirec.Payment_Type__c='Advance';
        olirec.Primary_Product__c=true;
        olirec.quantity=4;
        olirec.totalprice=1;
        toInsert.add(olirec);
        
        //created testoli with primaryproduct value false
        olirec1 = new Opportunitylineitem(); 
        olirec1.Opportunityid=KEFopty.id;
        olirec1.PriceBookEntryID=pre.id;
        olirec1.Payment_Frequency__c='Monthly';
        olirec1.Payment_Type__c='Advance';
        olirec1.Primary_Product__c=false;
        olirec1.quantity=4;
        olirec1.totalprice=1;
        toInsert.add(olirec1);
        insert toInsert ;
        Test.startTest();
        PageReference testPage = Page.OpportunityProductCustomPage;
        Test.setCurrentPage(testPage);
        System.currentPageReference().getParameters().put('Id',KEFopty.Id);
        ApexPAges.StandardController sc = new ApexPages.StandardController(KEFopty);
        OpportuntunityProductExtension opptyprodExten= new OpportuntunityProductExtension(sc); 

        Id oliId= olirec.Opportunity.id  ;
        opptyprodExten.theOpp=KEFopty;
        opptyprodExten.shoppingCart.add(olirec);
        opptyprodExten.onsave();
        opptyprodExten.newProdCancel(); 
        opptyprodExten.onCancel();
        System.assert(opptyprodExten.onCancel()!=null);
        opptyprodExten.addToShoppingCartKEF();
        Test.stopTest();

}

Public static testmethod void opptyprodsaveandnew(){
         List<OpportunityLineItem> toInsert = new List<OpportunityLineItem>();
        List<Picklist_Transformation__c> insertPicklist =new List<Picklist_Transformation__c>();
        Picklist_Transformation__c PF;
        Picklist_Transformation__c PT;
         PF= new Picklist_Transformation__c(Code__c='4',Picklist_Field__c='PaymentFrequency',DefaultDescription__c='Monthly');
         insertPicklist.add(PF);        
         PT= new Picklist_Transformation__c(Code__c='1393',Picklist_Field__c='Payment Type',DefaultDescription__c='Advance');
         insertPicklist.add(PT);      
        insert insertPicklist;
        
        //created testaccount
        acc = new Account();
        acc.name = 'Test Account';
        //acc.recordtypeId = accRTId;
        acc.BillingStreet = 'dajgfrewg';
        acc.BillingCity = 'dajgfrewg';
        acc.BillingState = 'dajgfrewg';
        acc.BillingPostalCode = 'dajgfrewg';
        acc.BillingCountry = 'dajgfrewg';
        insert acc;

        recordtype rt = [Select Id, name from recordtype where name ='KEF Opportunity'];
        //created testprogram
        objpgm=new Program__c();
        objpgm.Program_Status__c=System.Label.Prospect;
        objpgm.Name_of_the_Vendor_Program__c='Testprogram';
        objpgm.Business_Development_Stage__c=System.label.BuisnessStage;
        insert objpgm;
        
        //craeted testagreement
        objagg=new Agreement__c();
        objagg.Agreement_Name_UDF__c='TestUDF';
        objagg.Agreement_Legal_Status__c=System.Label.informal;
        objagg.Agreement_Type__c=System.label.other;
        objagg.Agreement_Status__c=System.label.Prospect;
        objagg.Generate_invoices_to_the_servicer__c='Yes';
        objagg.Servicer_Name__c='Testnme';
        objagg.Third_Party_Name_for_Property_Tax_Servic__c='Test2';
        objagg.Third_Party_Name_for_Sales_Tax_Servicing__c='Test1';
        objagg.Default_lessor__c=System.Label.KEF;
        objagg.Program__c=objpgm.id;
        objagg.Agreement_Term__c = 12;
        objagg.Agreement_Commencement_Date__c = System.Today();
        insert objagg;


        // Creating a Test Oppty of type KEF Opportunity
        KEFopty = new opportunity();
        KEFopty.name ='testKEF';
        KEFopty.AccountId = acc.id;
        KEFopty.closedate=system.today();
        KEFopty.StageName='Pursue';
        KEFopty.Volume__c=23;
        KEFopty.Vendor_Agreement__c=objagg.id;
        KEFopty.recordtypeId=rt.id;        
        insert KEFopty;

        Recordtype rtname =[Select Id,Name From RecordType where id =:KEFopty.recordtypeId ]; 

        //created testproduct
        prd = new Product2();
        prd.Name='TestKEFProduct';
        prd.Family='KEF';
        prd.Product_category__c='Conditional Lease';
        prd.IsActive=true;
        insert prd;
        
        //created kefproduct
         lstproduct=new KEF_Product__c();
        //lstproduct.Product_Category__c='Lease';
        lstproduct.Product_Association__c='testKEF';
        lstproduct.product__c= prd.id;
        lstproduct.Agreement__c=KEFopty.Vendor_Agreement__c;
        insert lstproduct;

        // Passing Oppty record id 

        Id pricebookId = Test.getStandardPricebookId();
        pre = new PricebookEntry();
        pre.Pricebook2Id=pricebookId;
        pre.IsActive=true;
        pre.UnitPrice =7658;
        pre.Product2Id=prd.id;
        insert pre;

        //craeted test oli with primary value true
        olirec = new Opportunitylineitem();
        olirec.Opportunityid=KEFopty.id;
        olirec.PriceBookEntryID=pre.id;
        olirec.Payment_Frequency__c='Monthly';
        olirec.Payment_Type__c='Advance';
        olirec.Primary_Product__c=true;
        olirec.quantity=4;
        olirec.totalprice=1;
        toInsert.add(olirec);
        
        //created testoli with primaryproduct value false
        olirec1 = new Opportunitylineitem(); 
        olirec1.Opportunityid=KEFopty.id;
        olirec1.PriceBookEntryID=pre.id;
        olirec1.Payment_Frequency__c='Monthly';
        olirec1.Payment_Type__c='Advance';
        olirec1.Primary_Product__c=false;
        olirec1.quantity=4;
        olirec1.totalprice=1;
        toInsert.add(olirec1);
        insert toInsert;
        Test.startTest();
        PageReference testPage = Page.OpportunityProductCustomPage;
        Test.setCurrentPage(testPage);
        System.currentPageReference().getParameters().put('Id',KEFopty.Id); 
        ApexPAges.StandardController sc = new ApexPages.StandardController(KEFopty);
        OpportuntunityProductExtension opptyprodExten= new OpportuntunityProductExtension(sc); 
        
        Id oliId= olirec.Opportunity.id  ;
        opptyprodExten.theOpp=KEFopty;
        opptyprodExten.shoppingCart.add(olirec);
        opptyprodExten.saveAndNew();
        opptyprodExten.newProdCancel(); 
        
        opptyprodExten.onCancel();
        System.assert(opptyprodExten.onCancel()!=null);
        
        opptyprodExten.addToShoppingCartKEF();
        Test.stopTest();
}
public static testmethod void opptyprodEdit()
{
         List<OpportunityLineItem> toInsert = new List<OpportunityLineItem>();
        List<Picklist_Transformation__c> insertPicklist =new List<Picklist_Transformation__c>();
        Picklist_Transformation__c PF;
        Picklist_Transformation__c PT;
         PF= new Picklist_Transformation__c(Code__c='4',Picklist_Field__c='PaymentFrequency',DefaultDescription__c='Monthly');
         insertPicklist.add(PF);        
         PT= new Picklist_Transformation__c(Code__c='1393',Picklist_Field__c='Payment Type',DefaultDescription__c='Advance');
         insertPicklist.add(PT);      
        insert insertPicklist;
        //created testaccount
        acc = new Account();
        acc.name = 'Test Account';
       // acc.recordtypeId = accRTId;
        acc.BillingStreet = 'dajgfrewg';
        acc.BillingCity = 'dajgfrewg';
        acc.BillingState = 'dajgfrewg';
        acc.BillingPostalCode = 'dajgfrewg';
        acc.BillingCountry = 'dajgfrewg';
        insert acc;

        recordtype rt = [Select Id, name from recordtype where name ='KEF Opportunity'];
        //created testprogram
        objpgm=new Program__c();
        objpgm.Program_Status__c=System.Label.Prospect;
        objpgm.Name_of_the_Vendor_Program__c='Testprogram';
        objpgm.Business_Development_Stage__c=System.label.BuisnessStage;
        insert objpgm;
        
        //craeted testagreement
        objagg=new Agreement__c();
        objagg.Agreement_Name_UDF__c='TestUDF';
        objagg.Agreement_Legal_Status__c=System.Label.informal;
        objagg.Agreement_Type__c=System.label.other;
        objagg.Agreement_Status__c=System.label.Prospect;
        objagg.Generate_invoices_to_the_servicer__c='Yes';
        objagg.Servicer_Name__c='Testnme';
        objagg.Third_Party_Name_for_Property_Tax_Servic__c='Test2';
        objagg.Third_Party_Name_for_Sales_Tax_Servicing__c='Test1';
        objagg.Default_lessor__c=System.Label.KEF;
        objagg.Program__c=objpgm.id;
        objagg.Agreement_Term__c = 12;
        objagg.Agreement_Commencement_Date__c = System.Today();
        insert objagg;


        // Creating a Test Oppty of type KEF Opportunity
        KEFopty = new opportunity();
        KEFopty.name ='testKEF';
        KEFopty.AccountId = acc.id;
        KEFopty.closedate=system.today();
        KEFopty.StageName='Pursue';
        KEFopty.Volume__c=23;
        KEFopty.Vendor_Agreement__c=objagg.id;
        KEFopty.recordtypeId=rt.id;        
        insert KEFopty;

        //created testproduct
        prd = new Product2();
        prd.Name='TestKEFProduct';
        prd.Family='KEF';
        prd.Product_category__c='Conditional Lease';
        prd.IsActive=true;
        insert prd;
        
        //created kefproduct
         lstproduct=new KEF_Product__c();
        //lstproduct.Product_Category__c='Lease';
        lstproduct.Product_Association__c='testKEF';
        lstproduct.product__c= prd.id;
        lstproduct.Agreement__c=KEFopty.Vendor_Agreement__c;
        insert lstproduct;

        // Passing Oppty record id 

        Id pricebookId = Test.getStandardPricebookId();
        pre = new PricebookEntry();
        pre.Pricebook2Id=pricebookId;
        pre.IsActive=true;
        pre.UnitPrice =7658;
        pre.Product2Id=prd.id;
        insert pre;
        
        
        // Creating a Test OLi 
        
        olirec = new Opportunitylineitem();
        olirec.Opportunityid=KEFopty.id;
        olirec.PriceBookEntryID=pre.id;
        olirec.Payment_Frequency__c='Monthly';
        olirec.Payment_Type__c='Advance';
        olirec.Primary_Product__c=true;
        olirec.quantity=4;
        olirec.totalprice=1;
        toinsert.add(olirec);
        
        olirec1 = new Opportunitylineitem();
        olirec1.Opportunityid=KEFopty.id;
        olirec1.PriceBookEntryID=pre.id;
        olirec1.Payment_Frequency__c='Monthly';
        olirec1.Payment_Type__c='Advance';
        olirec1.Primary_Product__c=false;
        olirec1.quantity=4; 
        olirec1.totalprice=1;  
        toinsert.add(olirec1);
        insert toinsert;
        Test.startTest();
        
        PageReference testPg = Page.opportunity_Product_Edit_Custompage;
        Test.setCurrentPage(testPg );
        ApexPAges.StandardController stdcon = new ApexPages.StandardController(olirec);
        OpportunityProductEditExtensions opptyprod = new OpportunityProductEditExtensions(stdcon );
        Id oliId= olirec.Opportunity.id  ;
        
        opptyprod.onsave();
        opptyprod.CheckPrimaryProduct[0] = olirec1;
        opptyprod.onsave();
        opptyprod.onCancel();
        opptyprod.primaryPrd();
        
        stdcon = new ApexPages.StandardController(olirec1);
        opptyprod = new OpportunityProductEditExtensions(stdcon);
        opptyprod.onsave();
        opptyprod.CheckPrimaryProduct[0] = olirec;
        opptyprod.onsave();
        olirec.Primary_Product__c=false;
        update olirec;
        opptyprod.CheckPrimaryProduct[0] = olirec1;
        opptyprod.onsave();
        opptyprod.onCancel();
        opptyprod.primaryPrd();
        Test.stopTest();
        
        
        }

}