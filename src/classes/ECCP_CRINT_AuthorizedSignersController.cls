public class ECCP_CRINT_AuthorizedSignersController {
    public LLC_BI__Loan__c loan {get;set;}    
    public List<LLC_BI__Contingent_Liabilty__c> authorizedSignerList {get;set;}
    public List<Schema.FieldSetMember> authorizedSignerFieldListView {get;set;}
    public List<Schema.FieldSetMember> selectedAuthorizedSignerFieldList {get;set;}
    public LLC_BI__Contingent_Liabilty__c selectedAuthorizedSigner {get;set;}
    public String authorizedSignerId {get;set;}
    public Boolean showList {get;set;}
    public nForce.TemplateController template;
    
    public ECCP_CRINT_AuthorizedSignersController(nForce.TemplateController controller) {
        this.template = controller;
        this.loan = getLoan(ApexPages.currentPage().getParameters().get(PARAMETER_NAME));
        this.authorizedSignerFieldListView = this.getAuthorizedSignerFieldListView(FIELDSETLIST);
        this.authorizedSignerList = this.getAuthorizedSignerList(this.loan.Id);
        this.showList = True;
    }
    
    public void viewAuthorizedSigner() {
    	this.showList = False;
        this.selectedAuthorizedSignerFieldList = this.getSelectedAuthorizedSignerFieldList(FIELDSETINDIVIDUAL);
        this.selectedAuthorizedSigner = this.getSelectedAuthorizedSigner(this.authorizedSignerId);
    }
    
    public void back() {
        this.showList = True;
    }
    
    private List<Schema.FieldSetMember> getAuthorizedSignerFieldListView(String fieldSet) {
        return SObjectType.LLC_BI__Contingent_Liabilty__c.fieldSets.getMap().get(fieldSet).getFields();
    }
    
    private List<Schema.FieldSetMember> getSelectedAuthorizedSignerFieldList(String fieldSet) {
        return SObjectType.LLC_BI__Contingent_Liabilty__c.fieldSets.getMap().get(fieldSet).getFields();
    }
    
    
    private LLC_BI__Loan__c getLoan(Id recordId) {
        return [SELECT
               		Id,
               		Name
               	FROM
               		LLC_BI__Loan__c
               	WHERE
               		Id = :recordId LIMIT 1];
    }
    
    private List<LLC_BI__Contingent_Liabilty__c> getAuthorizedSignerList(Id recordId) {
        String query = SELECTSTATEMENT;
        for(Schema.FieldSetMember f: authorizedSignerFieldListView) {
            query += f.getFieldPath() + COMMASPACE;
        }
        
        query += 'Id ';
        query += 'FROM LLC_BI__Contingent_Liabilty__c ';
        query += 'WHERE LLC_BI__Entity__c IN ';
        query += '(SELECT Id FROM LLC_BI__Legal_Entities__c WHERE LLC_BI__Loan__c = \'' + recordId + '\') LIMIT 1000';
        
        return Database.query(query);
    }
    
    private LLC_BI__Contingent_Liabilty__c getSelectedAuthorizedSigner(Id recordId) {
        String query = SELECTSTATEMENT;
        for(Schema.FieldSetMember f: selectedAuthorizedSignerFieldList) {
            query += f.getFieldPath() + COMMASPACE;
        }
        
        query += 'Id ';
        query += 'FROM LLC_BI__Contingent_Liabilty__c ';
        query += 'WHERE Id = \'' + recordId + '\' LIMIT 1';
        
        return Database.query(query);
    }
    
    private static final String PARAMETER_NAME = 'id';
    private static final String FIELDSETLIST = 'CRINT_Borrowing_Structure_Authorized_Sig';
    private static final String FIELDSETINDIVIDUAL = 'CRINT_Authorized_Signer_View';
    private static final String SELECTSTATEMENT = 'SELECT ';
    private static final String COMMASPACE = ', ';
}