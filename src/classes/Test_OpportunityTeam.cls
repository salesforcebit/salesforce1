@istest
Public class Test_OpportunityTeam
{

    public static testMethod void OAT()
    {
        Profile p = [select id from profile where name='System Administrator'];
        
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharanx@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharanx@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
        
        recordtype rt = [Select Id, name from recordtype where name ='Deposits & ECP'];
        Account acc = new account (name='test 13234');
        
        opportunity opty = new opportunity(name ='OAT Test Opty',RecWeb_Number__c = '12589', AccountId = acc.id, closedate=system.today(), StageName='Pursue', 
        recordtypeId=rt.id, ownerid=testUser.id, Actual_Fee_Amount__c=23);        
        insert opty;
                
        OpportunityTeamMember OTM = new OpportunityTeamMember(OpportunityId=opty.id, TeamMemberRole = 'Opportunity Owner', UserId = testuser.id, Actual_Fee__c=opty.Actual_Fee_Amount__c);
        insert OTM;
        
        Opportunity_Audit_Table__c OAT = new Opportunity_Audit_Table__c(Opportunity_Name__c=opty.id, User__c = testuser.id, Action__c= 'Added');
        insert OAT;
        
        //delete OTM;
        
        Opportunity_Audit_Table__c OAT1 = new Opportunity_Audit_Table__c(Opportunity_Name__c=opty.id, User__c = testuser.id, Action__c= 'Deleted or Removed');
        insert OAT1;
    }
    
    public static testMethod void OAT1()
    {    
        Profile p = [select id from profile where name='System Administrator'];
        
        User testUser1 = new User(alias = 'u1', email='dhanesh_subramanian@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='dhanesh_subramanian71@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser1;
     
        recordtype rt1 = [Select Id, name from recordtype where name ='Credit'];
        Account acc1 = new account (name='Credit Test');
        
        opportunity optyc = new opportunity(name ='OAT Test Credit Opty',AccountId = acc1.id, closedate=system.today(), StageName='Pursue', 
        recordtypeId=rt1.id, ownerid=testUser1.id, Actual_Syndication_Non_Amortizing_Fee__c=23);        
        insert optyc;
        
        OpportunityTeamMember OTMC = new OpportunityTeamMember(OpportunityId=optyc.id, TeamMemberRole = 'Banker', UserId = testuser1.id, Actual_Fee__c=optyc.Actual_Syndication_Non_Amortizing_Fee__c);
        insert OTMC;

    }
    
    public static testMethod void OAT2()
    {    
        Profile p = [select id from profile where name='System Administrator'];
        
        User testUser2 = new User(alias = 'u1', email='dhanesh_subramanianx@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='dhanesh_subramanian72@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser2;
        
        
        recordtype rt2 = [Select Id, name from recordtype where name ='Invst Bnkg & Market'];
        Account acc2 = new account (name='Inv Test');
        
        opportunity optyi = new opportunity(name ='OAT Test Inv Opty',AccountId = acc2.id, closedate=system.today(), StageName='Pursue', 
        recordtypeId=rt2.id, ownerid=testUser2.id, Actual_Syndication_Non_Amortizing_Fee__c=23, Product_Category__c='CIS-Fixed', product__c='CIS-Fixed',Private__c=true, Private_Code_Name__c='OAT Test Inv Opty');        
        insert optyi;
               
        OpportunityTeamMember OTMI = new OpportunityTeamMember(OpportunityId=optyi.id, TeamMemberRole = 'Banker', UserId = testuser2.id, Actual_Fee__c=optyi.Actual_Fee_Amount__c);
        insert OTMI;
        
        User testUser3 = new User(alias = 'u1', email='dhanesh_subramaniany@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='dhanesh_subramanian73@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser3;
        
                
        opportunity otmacc1 =[Select Id,name,ownerId from opportunity where Id=:optyi.Id];
        otmacc1.ownerId=testUser3.Id;
        update otmacc1;

    }
    
    public static testMethod void OAT3()
    {
        Profile p = [select id from profile where name='System Administrator'];
        
        
        User testUser2 = new User(alias = 'u1', email='dhanesh_subramanianx@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States', 
        timezonesidkey='America/Los_Angeles', username='dhanesh_subramanian72@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser2;
        
        User testUser3 = new User(alias = 'uTest1', email='dhanesh_subramanianTest@keybank.com',
        emailencodingkey='UTF-8', lastname='TestRec2015', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='dhanesh_subramanian792@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser3;
     
        Map<String, Schema.SObjectType> oppsObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType opps = oppsObjectMap.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaOpp = opps.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> oppRecordTypeInfo = resSchemaOpp.getRecordTypeInfosByName();
        Id oppRTId = oppRecordTypeInfo.get('KEF Opportunity').getRecordTypeId();
        System.Debug('oppRTId:'+oppRTId);
        
        Account acc2 = new account (name='Inv Test');
        
        Opportunity opptyObj = new Opportunity();
        opptyObj.name = 'Test Oppty';
        opptyObj.AccountId = acc2.Id;
        opptyObj.recordtypeId = oppRTId;
        opptyObj.Volume__c = 5;
        opptyObj.StageName = 'Pitch';
        opptyObj.CloseDate = System.Today()+100;
        //opptyObj.Vendor__c = accVendorObj.Id;
        insert opptyObj;
               
        OpportunityTeamMember OTMI = new OpportunityTeamMember(OpportunityId=opptyObj.id, TeamMemberRole= 'Relationship Manager', UserId = testuser2.id, Actual_Fee__c=opptyObj.Actual_Fee_Amount__c);
        insert OTMI;
        OpportunityTeamMember OTM2 = new OpportunityTeamMember(OpportunityId=opptyObj.id, TeamMemberRole= 'Banker', UserId = testUser3.id, Actual_Fee__c=opptyObj.Actual_Fee_Amount__c);
        insert OTM2;
        List<opportunityteamMember> newOtmAddedList = new  List<opportunityteamMember>();
        newOtmAddedList.add(OTMI);
        List<Id> tId = new List<Id>();
        tId.add(OTMI.Id);
        List<Id> tIdp = new List<Id>();
        tIdp.add(OTMI.OpportunityId);
        
        List<opportunityteamMember> newOtmdeleted = new List<opportunityteamMember>();
        newOtmdeleted.add(OTM2);
        Test.StartTest();
        //List<opportunityteamMember> newOtmAdded,list<id> tId, list<id> tIdp
        opportunityteamMemberTracking_Handler ctrlObj = new opportunityteamMemberTracking_Handler(true, 1); 
        opportunityteamMemberTracking_Handler.onAfterInsert(tId,tIdp);
        ctrlObj.onAfterDelete(newOtmdeleted);
        Test.StopTest();
    }
    
    public static testMethod void OAT4()
    {    
        Profile p=[Select id from Profile where name='System Administrator'];
        
        User testUser2 = new User(alias = 'u1', email='dhanesh_subramanianx@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='dhanesh_subramanian72@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser2;
        
        
        recordtype rt2 = [Select Id, name from recordtype where name ='credit'];
        Account acc2 = new account (name='Test Account');
        
        system.runas(testUser2)
        {
        opportunity optyi = new opportunity(name ='Test Opportunity',AccountId = acc2.id, closedate=system.today(), StageName='Pursue', 
        recordtypeId=rt2.id, ownerid=testUser2.id,Private__c=true, Private_Code_Name__c='864786');        
        insert optyi;
        
        Userrole r = [Select Id from UserRole where name like '%Tech IB%' limit 1];
        
        User testUser3 = new User(alias = 'uTest1', email='dhanesh_subramanianTest@keybank.com', userroleid = r.id,
        emailencodingkey='UTF-8', lastname='TestRec2015', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = p.Id, country='United States', department = 'Tech IB',
        timezonesidkey='America/Los_Angeles', username='dhanesh_subramanian792@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser3;
               
                      
        OpportunityTeamMember OTMI = new OpportunityTeamMember(OpportunityId=optyi.id, TeamMemberRole = 'Banker', UserId = testuser3.id);
        insert OTMI;
        
        OpportunityTeamMember otm = [Select userid from OpportunityTeamMember where userid =: testuser3.id];
        delete otm;
        }
        
           
        
    }
    
    
}