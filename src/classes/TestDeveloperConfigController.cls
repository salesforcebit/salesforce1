@isTest
private class TestDeveloperConfigController {

	@isTest
	static void TestCreateFullTMProductCatalog() {

		DeveloperConfigController d = new DeveloperConfigController();
		d.createFullTMProductCatalog();

		Test.startTest();

		System.assert(d.featureMap.size() > 0);
		System.assert(d.productMap.size() > 0);
		System.assert(d.productTypeMap.size() > 0);
		System.assertEquals(d.productLineID, d.getProductLineIdFromLookupKey('TM001'));
		System.assert(d.productLineID != null);
		System.assertEquals(null, d.getProductLineIdFromLookupKey('NOMATCH'));
		System.assertEquals('12345678901234567', d.getLookupKey('12345678901234567890'));

		Test.stopTest();
	}
}