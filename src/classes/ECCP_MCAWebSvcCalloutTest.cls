@isTest
public class ECCP_MCAWebSvcCalloutTest {
public static testmethod ECCP_MCA_CheckMCI_Response testMycalloutreturn(){
 // This causes a fake response to be generated   
  Test.setMock(WebServiceMock.class, new ECCP_MCA_WebServiceMockImpl());
  
  ECCP_MCACheckRequestData req = new ECCP_MCACheckRequestData();
  req.CustNbr = '123456';
  req.SupplCode ='MCA567';
  req.SSN ='BNNAS8KMM8';
  req.EmprNbr = '789';
  
 ECCP_MCA_CheckMCI_Response Myresp = ECCP_MCAWebSvcCallout.Mycalloutreturn(req);
 return Myresp;
 }
 /*
public static testmethod ECCP_MCA_CheckMCI_Response testMycalloutreturnExceptionCase(){
 // This causes a fake response to be generated   
    Test.setMock(WebServiceMock.class, new ECCP_MCA_WebServiceMockImplExceptionCase());
    
    ECCP_MCACheckRequestData req = new ECCP_MCACheckRequestData();
    req.CustNbr = '123456';
    req.SupplCode ='MCA567';
    req.SSN ='BNNAS8KMM8';
    req.EmprNbr = '789';
    
    ECCP_MCA_CheckMCI_Response Myresp = ECCP_MCAWebSvcCallout.Mycalloutreturn(req);
    return Myresp;
    }
    */
}