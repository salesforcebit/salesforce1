@isTest
public with sharing class Utility_UT {
	static testmethod void Utility_UT() {
		Utility controller = new Utility();
		Utility.lastDigit = 10;
		Utility.lastDigit = 9;
		Utility.encrypt('This is a test');
		//Utility.decrypt('AES256');
		Utility.generateDocUnId();
		Utility.generateDocRefID(true);
		controller.changeFromCaps('test');
		Utility.toLowerCase('TEST');
		Utility.getNamespace();
		Utility.getCoreNamespace();
		Utility.alphanumericOnly('test test teset');
		Utility.isProduction();
		Utility.isToolDataInstalled();
		Utility.isConfigurationToolInstalled();
		Utility.createSystemProperty(
			'propertyName',
			'categoryName',
			'key',
			'value',
			true,
			true
		);
		Utility.updateSystemProperty(
			'propertyName',
			'categoryName',
			'key',
			'value',
			true
		);

	}
}