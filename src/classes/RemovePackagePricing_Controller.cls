public class RemovePackagePricing_Controller {

    public Opportunity opp {get;set;}
    List<LLC_BI__Scenario__c> oppScenariosList = new List<LLC_BI__Scenario__c>();
    set<id> scenarioIdSet = new set<id>();
    public Id opportunityId {get;set;}
    Public List<scenarioItemWrapper> scenarioItemList{get;set;}
    public Id exceptionPricingQ {get{
        if(exceptionPricingQ == null){
            exceptionPricingQ = [SELECT id FROM Group WHERE DeveloperName = 'Exception_Pricing' AND Type='Queue' ].id;
            exceptionPricingQ = exceptionPricingQ == null ? UserInfo.getUserId() : exceptionPricingQ;
        }
        return exceptionPricingQ;
    }set;}
     public Id record_typeid{get{
        if(record_typeid== null){
            record_typeid= Schema.SObjectType.TSO_Setup_Task__c.getRecordTypeInfosByName().get('Parent Task').getRecordTypeId();
            
        }
        return record_typeid;
    }set;}
   
    Public class scenarioItemWrapper{
        public boolean isCheck{get;set;}       
        public LLC_BI__Scenario_Item__c scenitem{get;set;}
        public scenarioItemWrapper(LLC_BI__Scenario_Item__c item){
            isCheck = false;
            scenitem = item;     
        }
        
    }
    //The below piece of code will display list of scenario items.
    public RemovePackagePricing_Controller(ApexPages.standardController std) {
        
        scenarioItemList = new List<scenarioItemWrapper>();
        opportunityId = std.getId();  
            system.debug('opportunityId****'+opportunityId); 
            try {
                oppScenariosList = [select id,LLC_BI__Opportunity__c from LLC_BI__Scenario__c where LLC_BI__Opportunity__c=:opportunityId]; 
            }
            Catch(Exception e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,e.getmessage());
                ApexPages.addMessage(myMsg);
            }
            for(LLC_BI__Scenario__c scenario : oppScenariosList){
                scenarioIdSet.add(scenario.id); 
                system.debug('scenarioIdSet****'+scenarioIdSet);
            }   
            for(LLC_BI__Scenario_Item__c scenitem:[select id,name,Scenario_Item_Type__c,LLC_BI__Bill_Point__r.name,Package_Pricing__c,LLC_BI__Scenario__c,Remove_Package__c from 
                                                   LLC_BI__Scenario_Item__c  where LLC_BI__Scenario__c in : scenarioIdSet and Package_Pricing__c=true and Remove_Package__c=false]){
                scenarioItemList.add(new scenarioItemWrapper(scenitem));                                          
                                                     
            }
            system.debug('scenarioItemList****'+scenarioItemList);
            if(scenarioItemList.size()==0){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'there are no items to select');
                ApexPages.addMessage(myMsg);
            } 
            
    } 
        
        public PageReference removeItem(){
        
        List<LLC_BI__Scenario_Item__c> removeItemList = new List<LLC_BI__Scenario_Item__c>(); 
        List<scenarioItemWrapper> scenarioItemListremoved = new List<scenarioItemWrapper>();
        List<TSO_Setup_Task__c> insertList = new List<TSO_Setup_Task__c>();
            for(scenarioItemWrapper wrapper: scenarioItemList){
                if(wrapper.isCheck == true){
                    removeItemList.add(wrapper.scenitem);
                }                
                else {
                   scenarioItemListremoved.add(wrapper);
                }
            }
            if(removeItemList !=null && removeItemList.size()>0){
               
                Try{
                   
                    for(LLC_BI__Scenario_Item__c sc: removeItemList){
                    sc.Remove_Package__c=true;
                       insertList.add(new TSO_Setup_Task__c(
                       Opportunity_Lookup__c=opportunityId,
                       RecordTypeId=record_typeid,
                        Name = 'Remove Package Pricing for '+''+sc.Name,
                        Status__c = 'New',
                        OwnerId = exceptionPricingQ,
                        RPM_Task__c = true,
                        Description__c = 'A request has been submitted to remove package pricing for scenario item with ID'+'  '+sc.Id+'  '+'and Name'+'  '+sc.Name,
                        Scenario__c=sc.LLC_BI__Scenario__c
                    ));
                    }
                    
                   Database.insert (insertList,false);
                   update removeItemList;
                   PageReference pageRef = new PageReference('/'+opportunityId );
                   pageRef.setRedirect(true);
                   return pageRef;
                }
                Catch(Exception e){
                    
                }
                 
            }
            return null;
        }
            
    }