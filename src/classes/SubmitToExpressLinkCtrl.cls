//================================================================================
//  KeyBank  
//  Object: Opportunity
//  Author: offshore
//  Detail: SubmitToExpressLinkCtrl
//  Description: To check few valdiations on opportunity object and on relted object before submitting oppty to expresslink
//================================================================================
//          Date            Purpose
// Changes: 10/6/2015      Initial Version
//================================================================================

public class SubmitToExpressLinkCtrl{
    
   public String errorMsg{get;set;}
    public Id opptyId;
    public boolean assetCodeError{get;set;}
    public boolean assetdescError{get;set;}
    public boolean opptyStageError{get;set;}
    public boolean opptyfeaTypeError{get;set;}
    public boolean estSyncFeeError{get;set;}
    public boolean venstatuserror{get;set;}
    public boolean agrstatuserror{get;set;}
    public boolean asssuperror{get;set;}
    public boolean sentoppError{get;set;}
    public Account accObj{get;set;}
    public String contactId{get;set;} 
    public Id contactLink{get;set;} 
    Id OpptyRTId;
    Id OpptyRTId1;
    public Opportunity opptyObj{get;set;} 
    public List<prodWrap> prodWrapList{get;set;}
    public List<accWrap> accWrapList{get;set;}
    public List<conWrap> conWrapList{get;set;}
    public List<contactRoleWrap> contactRoleWrapList{get;set;}
    Public List<Contact> conAddRecList {get;set;}
    Public Boolean submitOpptyFlag{get;set;}
    public id vendorId{get;set;}
    public String vendorName {get;set;}
    public id agreeId{get;set;}
    public String agreeName{get;set;}
    List<Attachment> AttList;
    Boolean CustomerAccountCheckBool;
    Boolean ContactAddCheckBool;
    Boolean vendorAccountCheckBool;
    Boolean partnerAddCheckBool ;
    Boolean COIPartnerCheckBool ;
    Boolean COIAssetStatusCheckBool;
    Public Id AssetAccountId{get;set;}
    Public String assetAccLink{get;set;}
    Boolean primaryProdCheckBool;
    Boolean contactRoleCheckBool;
    Boolean contactRoleAddCheckBool;
    Boolean primaryProdAddCheckBool;
    OpportunityLineItem opptyLineItemObj;
    //public Boolean contactRoleaddedBool{get;set;} 
    List<OpportunityContactRole> opptyContactRoleList;
    public SubmitToExpressLinkCtrl(ApexPages.StandardController controller){
        
        opptyId = ApexPages.currentPage().getParameters().get('Id');
        System.Debug('opptyId#'+opptyId);
        opptyStageError = false;
        assetCodeError = false;
        assetdescError = false;
        opptyfeaTypeError = false;
        estSyncFeeError = false;
        venstatuserror = false;
        agrstatuserror = false;
        asssuperror = false;
        sentoppError = false;
        submitOpptyFlag = true;
        CustomerAccountCheckBool  = false;
        ContactAddCheckBool = false;
        vendorAccountCheckBool = false;
        partnerAddCheckBool = false ;
        COIPartnerCheckBool = false ;
        COIAssetStatusCheckBool = false;
        primaryProdCheckBool = false;
        contactRoleCheckBool = false;
        contactRoleAddCheckBool = false;
        opptyLineItemObj = new OpportunityLineItem();
        primaryProdAddCheckBool = false;
        //contactRoleaddedBool = false;
        
        //KEF Recordtype for Oppty  : KEF Opportunity
        Map<String, Schema.SObjectType> OpptysObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType Opptys = OpptysObjectMap.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaOppty = Opptys.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> OpptyRecordTypeInfo = resSchemaOppty.getRecordTypeInfosByName();
        OpptyRTId = OpptyRecordTypeInfo.get('KEF Opportunity').getRecordTypeId();
        System.Debug('OpptyRTId:'+OpptyRTId);
        
        //KEF Recordtype for Oppty  : KEF Oppty - Submitted
        Map<String, Schema.SObjectType> OpptysObjectMap1 = Schema.getGlobalDescribe() ;
        Schema.SObjectType Opptys1 = OpptysObjectMap1.get('Opportunity') ;
        Schema.DescribeSObjectResult resSchemaOppty1 = Opptys1.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> OpptyRecordTypeInfo1 = resSchemaOppty1.getRecordTypeInfosByName();
        OpptyRTId1 = OpptyRecordTypeInfo1.get('KEF Oppty - Submitted').getRecordTypeId();
        System.Debug('OpptyRTId1:'+OpptyRTId1);
        
        
        AttList = new List<Attachment>();
        opptyObj = new Opportunity();
        accWrapList = new List<accWrap>();
        conWrapList = new List<conWrap>();
        prodWrapList = new List<prodWrap>();
        contactRoleWrapList = new List<contactRoleWrap>();
        conAddRecList  = new List<Contact>();
        opptyContactRoleList= new List<OpportunityContactRole>();
        try{
            //Query to retrieve oppty record and relevant data to check the blank values before submitting the oppty to expresslink
            opptyObj = [SELECT id,Asset_Code__c,Asset_Description__c,StageName,Vendor__r.name,Vendor__r.Vendor_status__c,Vendor_Agreement__r.name,Vendor_Agreement__r.Agreement_Status__c,Vendor_status__c,Vendor__r.Supplier_status__c,Bank_Underwritten__c,Term__c,Relationship_Manager__c,Send_to_Express_Link__c,Feature_Type__c,Account.Id,RecordTypeId,Estimated_Syndication_Fees__c,Distribution_Strategy__c,Parent_Opportunity_Id__c  FROM Opportunity WHERE id =:opptyId LIMIT 1];
            System.debug('opptyObj:'+opptyObj);
        
            if(opptyObj.RecordTypeId != null && OpptyRTId != null && opptyObj.RecordTypeId == OpptyRTId){
                if(opptyObj.Send_to_Express_Link__c != null && opptyObj.Send_to_Express_Link__c == true){
                    sentoppError = true;
                }
                else{
                    accObj = new Account();
                    accObj = [Select id,BillingAddress,Name,recordtypeId,Supplier_status__c,
                                (SELECT id,Account.Id,firstName,lastName,Birthdate,MailingAddress,OtherAddress,Contact_Email__c,Phone FROM Contacts),
                                (SELECT id,Account_Vendor__c,Agreement__c,AgreementStatus__c FROM Vendor_Agreement__r) 
                               
                                FROM Account WHERE id=: opptyObj.Account.Id ];
                    System.Debug('accObj:'+accObj);
                    System.Debug('Vendor_Agreement__r:'+accObj.Vendor_Agreement__r);
                    System.Debug('Contacts:'+accObj.Contacts);
                    //Check for Oppty stage
                    if(opptyObj.StageName == null || opptyObj.StageName == '' || opptyObj.StageName != 'Engage High'){
                        opptyStageError = true;
                        submitOpptyFlag = false;
                    }
                    //Check for asset Description on oppty
                    if(opptyObj.Asset_Description__c == null || opptyObj.Asset_Description__c == ''){
                        assetdescError = true;
                        submitOpptyFlag = false;
                    }
                    //Check for Asset code on oppty
                    if(opptyObj.Asset_Code__c == null || opptyObj.Asset_Code__c == ''){
                        assetCodeError = true;
                        submitOpptyFlag = false;
                    }
                    //Check for distribution strategy value for oppty
                    if ((opptyObj.Distribution_Strategy__c == 'Hold for Sale' || opptyObj.Distribution_Strategy__c  == 'Syndicate') && (opptyObj.Estimated_Syndication_Fees__c  == null )){
                        estSyncFeeError = true;
                        submitOpptyFlag = false;
                    }
                    //Check the vendor status for oppty
                    if(opptyObj.Vendor__r.Vendor_status__c!='Active'){
                        venstatuserror = true;
                        vendorId = opptyObj.Vendor__c;
                        vendorName = opptyObj.Vendor__r.name;
                        submitOpptyFlag = false;
                    }
                    //Check the vendor agreement status on oppty
                    if(opptyObj.Vendor_Agreement__r.Agreement_Status__c!='Active'){
                        agrstatuserror = true;
                        agreeId = opptyObj.Vendor_Agreement__c;
                        agreeName = opptyObj.Vendor_Agreement__r.name;
                        submitOpptyFlag = false;
                    }
                }
            }
        }
        catch(exception exp){
            System.debug('exception:'+exp.getMessage());
            System.debug('exception:'+exp.getstackTracestring());
        }
    }
    
    //Method to check the Customer account have complete address.
    public Boolean getCustomerAccountCheck(){
        try{
            Account custAcc = new Account();
            custAcc = [Select id,BillingStreet, BillingCity, BillingState, BillingPostalCode,BillingCountry,Name,recordtypeId FROM Account WHERE id=: opptyObj.Account.Id ];
            System.debug('Customer Account:'+custAcc);
            if(custAcc != null){
                System.Debug('Customer Acc exists.');
                if(custAcc.BillingStreet == null || custAcc.BillingStreet == '' || custAcc.BillingCity == null || custAcc.BillingCity == '' || custAcc.BillingState == null || custAcc.BillingState == '' || custAcc.BillingPostalCode == null || custAcc.BillingPostalCode == '' || custAcc.BillingCountry == null || custAcc.BillingCountry == ''){
                    submitOpptyFlag = false;
                    CustomerAccountCheckBool = true;
                    return true;
                }
                else{
                    submitOpptyFlag = true;
                    CustomerAccountCheckBool = false;
                    return false;
                }
            }
        }
        catch(Exception Exp){
            System.Debug('Exception:'+exp.getMessage());
            System.Debug('Exception:'+exp.getStackTraceString());
        }
        return null;
    }
    
    //Method to check the address for the vendor added on the oppty.
    public Boolean getVendorAccountCheck(){
        try{
            Account vendorAcc = new Account();
            vendorAcc = [Select id,BillingStreet, BillingCity, BillingState, BillingPostalCode,BillingCountry,Name,recordtypeId FROM Account WHERE id=: opptyObj.Vendor__c ];
            System.debug('Vendor Account:'+vendorAcc);
            if(vendorAcc != null){
                System.Debug('Vendor Acc exists.');
                if(vendorAcc.BillingStreet == null || vendorAcc.BillingStreet == '' || vendorAcc.BillingCity == null || vendorAcc.BillingCity == '' || vendorAcc.BillingState == null || vendorAcc.BillingState == '' || vendorAcc.BillingPostalCode == null || vendorAcc.BillingPostalCode == '' || vendorAcc.BillingCountry == null || vendorAcc.BillingCountry == ''){
                    submitOpptyFlag = false;
                    vendorAccountCheckBool = true;
                    return true;
                }
                else{
                    submitOpptyFlag = true;
                    vendorAccountCheckBool = false;
                    return false;
                }
            }
        }
        catch(Exception Exp){
            System.Debug('Exception:'+exp.getMessage());
            System.Debug('Exception:'+exp.getStackTraceString());
        }
        return null;
    }
    
    //Method to check if th COI Partner do not have more than 1 KEF Asset supplier added.
    public Boolean getCOIPartnerCheck(){
        List<Partner> coiPartList = new List<Partner>();
        coiPartList = [SELECT id, Role,OpportunityId,AccountToId FROM Partner WHERE OpportunityId = :opptyId AND Role = 'KEF – Asset Supplier'];
        System.Debug('coiPartList:'+coiPartList);
        System.Debug('coiPartList.size():'+coiPartList.size());
        if(coiPartList != null && coiPartList.size() >0){
            if(coiPartList.size() >1 ){
                submitOpptyFlag = false;
                COIPartnerCheckBool =true;
                return true;
            }
            else{
                AssetAccountId = coiPartList[0].AccountToId;
                system.debug('AssetAccountId Found:'+AssetAccountId);
                submitOpptyFlag = true;
                COIPartnerCheckBool = false;
                return false;
            }
        }
         return null;
    }
    
    //Method to check the asset supplier status added in partner for oppty. 
    /*
    public Boolean getCOIAssetStatusCheck(){
        if(AssetAccountId != null){
            Account accObj = [SELECT id,Supplier_status__c,name FROM Account WHERE id= :AssetAccountId LIMIT 1];
            if(accObj != null && accObj.Supplier_status__c != 'Active'){
                COIAssetStatusCheckBool = true;
                assetAccLink = accObj.name;
                submitOpptyFlag = false;
                return true;
            }
            else{
                COIAssetStatusCheckBool = false;
                submitOpptyFlag = true;
                return false;
            }
        }
        return null;
    }
    */
    //Method to check if the partner added on oppty have complete address.
    public Boolean getPartnerAddCheck(){
        List<Partner> coiPartList = new List<Partner>();
        Map<Id,String> partMap = new Map<Id,String>();
        coiPartList = [SELECT id, Role,OpportunityId,AccountToId FROM Partner WHERE OpportunityId = :opptyId];
        System.Debug('coiPartList:'+coiPartList);
        Set<Id> partAccIdSet = new Set<Id>();
        for(Partner temprec : coiPartList){
            if(temprec.OpportunityId != null && temprec.Role != null && temprec.Role != ''&& temprec.Role != '--'){
                partAccIdSet.add(temprec.AccountToId);
                partMap.put(temprec.AccountToId,temprec.Role);
            }
        }
        System.debug('Added to Partner Map'+partMap.size());
        System.debug('Added to Partner Map'+partMap );
            
        System.debug('Partner Account Size:'+partAccIdSet.size());
        List<Account> partnerAccList = new List<Account>();
        partnerAccList = [Select id,BillingStreet, BillingCity, BillingState, BillingPostalCode,BillingCountry,Name,recordtypeId FROM Account WHERE id IN :partAccIdSet];
        System.debug('partnerAccList:'+partnerAccList);
        
        for(Account accRec : partnerAccList){
            if(accRec != null){
                System.Debug('accRec exists.'); 
                if(accRec.BillingStreet == null || accRec.BillingStreet == '' || accRec.BillingCity == null || accRec.BillingCity == '' || accRec.BillingState == null || accRec.BillingState == '' || accRec.BillingPostalCode == null || accRec.BillingPostalCode == '' || accRec.BillingCountry == null || accRec.BillingCountry == ''){
                    System.Debug('accRec second if exists.'); 
                    if(partMap.containsKey(accRec.Id)){
                        accWrap accTempWrap = new accWrap(partMap.get(accRec.Id)+' Partner address cannot be blank! ', accRec.Id ,accRec.Name );
                        accWrapList.add(accTempWrap);
                        System.debug('accWrapList:'+accWrapList.size()+': '+accWrapList);
                    }
                }
            }
        }
        if(accWrapList!= null && accWrapList.size() > 0){
            submitOpptyFlag = false;
            partnerAddCheckBool = true;
            return true;
        }
        else{
            submitOpptyFlag = true;
            partnerAddCheckBool = false;
            return false;
        }
        return null;
    }
    
    //Method to check the contact primary address is complete and not blank.
    public Boolean getContactAddCheck(){
        List<OpportunityContactRole> conRoleList = new List<OpportunityContactRole>();
        Map<Id,String> contactMap = new Map<Id,String>();
        conRoleList = [SELECT id, Role,OpportunityId,ContactId FROM OpportunityContactRole WHERE OpportunityId = :opptyId];
        System.Debug('conRoleList:'+conRoleList);
        Set<Id> contactIdSet = new Set<Id>();
        for(OpportunityContactRole temprec : conRoleList){
            if(temprec.OpportunityId != null && temprec.Role != null && temprec.Role != ''&& temprec.Role != '--' ){
                contactIdSet.add(temprec.ContactId);
                contactMap.put(temprec.ContactId,temprec.Role);
            }
        }
        System.debug('Added to Contact Map'+contactMap.size());
        System.debug('Added to Contact Map'+contactMap);
        
        System.debug('Contact Role Size:'+contactIdSet.size());
        conAddRecList = [select id,MailingCity,MailingStreet,MailingState,MailingCountry,MailingPostalCode,Name from Contact where id IN :contactIdSet];
        System.debug('conAddRecList:'+conAddRecList);
        if(conAddRecList != null && conAddRecList.size()>0){
            for(Contact conRec : conAddRecList){
                System.Debug('conAddRecList exists.');
                if(conRec != null){
                    System.Debug('conRec exists.');
                    if(conRec.MailingStreet == null || conRec.MailingStreet == '' || conRec.MailingCity == null || conRec.MailingCity == '' || conRec.MailingState == null || conRec.MailingState == '' || conRec.MailingPostalCode == null || conRec.MailingPostalCode == '' || conRec.MailingCountry == null || conRec.MailingCountry == ''){
                        if(contactMap.containsKey(conRec.Id)){
                            conWrap conTempWrap = new conWrap(contactMap.get(conRec.Id)+' Contact Primary Address cannot be blank! ', conRec.Id,conRec.Name );
                            conWrapList.add(conTempWrap);
                            System.debug('conWrapList:'+conWrapList.size()+': '+conWrapList);
                            
                        }
                    }
                }
            }
            if(conWrapList != null && conWrapList.size() > 0){
                submitOpptyFlag = false;
                ContactAddCheckBool = true;
                return true;
            }
            else{
                submitOpptyFlag = true;     
                ContactAddCheckBool = false;                
                return false;
            }
        }
        return null;
    } 
    
    //Method to check if the attachment added on oppty has size more than 15mb.
    public Boolean getFileCheck(){
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId ].Name;
        List<Id> attIdList = new List<Id>();
        if(profileName.containsAny('KEF')){
            List<Id> Idlist = new List<Id>();
            List<Private_Note_Attachment__c> privateId=[Select Id from Private_Note_Attachment__c where Opportunity_Name__c=:opptyId]; 
            For(Private_Note_Attachment__c p : privateId){
                Idlist.add(p.id);
            }
            AttList = [SELECT Id,Body,BodyLength,Name,ParentId FROM Attachment where ParentId IN:Idlist];    
            for(Attachment Att: AttList ){
                if(att.BodyLength>15728640){
                    attIdList.add(att.Id);
                }
            }
            if(attIdList != null && attIdList.size() > 0){
                submitOpptyFlag = false;
                return true;
            }
            else{
                submitOpptyFlag = true;
                return false;
            }
        }
        return null;
    }
    
    //Method to check the contact role added on oppty record
    /*
    public Boolean getContactRoleaddedBool(){
        opptyContactRoleList = new List<OpportunityContactRole>();
        try{
            opptyContactRoleList = [SELECT id,ContactId,Contact.Name,Contact.Birthdate,Contact.Contact_Type__c,OpportunityId,Role FROM OpportunityContactRole WHERE OpportunityId = :opptyId];
            System.debug('opptyContactRoleList:'+opptyContactRoleList.size());
            if(opptyContactRoleList == null || opptyContactRoleList.size() == 0){
                contactRoleAddCheckBool = true;
                submitOpptyFlag = false;
                return true;
            }
            else{
                contactRoleAddCheckBool = false;
                submitOpptyFlag = true;
                return false;
            }
        }
        catch(exception exp){
            System.debug('catch:'+exp.getMessage());
            System.debug('catch String:'+exp.getStackTraceString());
        }
        return null;
    }
    */
    
    //Method to check the Date of birth of the Contact Role = Personal Guarantor
    public Boolean getContactRoleCheck(){
        opptyContactRoleList = new List<OpportunityContactRole>();
        opptyContactRoleList = [SELECT id,ContactId,Contact.Name,Contact.Birthdate,Contact.Contact_Type__c,OpportunityId,Role FROM OpportunityContactRole WHERE OpportunityId = :opptyId];
        System.debug('opptyContactRoleList:'+opptyContactRoleList.size());
        contactRoleWrap contactRoleWrapObj = new contactRoleWrap('',null,'');
        try{
            if(opptyContactRoleList != null && opptyContactRoleList.size() >0){
                for(OpportunityContactRole tempObj : opptyContactRoleList){
                    if(tempObj.Contact.Birthdate == null && tempObj.Role == 'KEF – Personal Guarantor' ){
                        contactRoleWrapObj = new contactRoleWrap('Please provide the Date of Birth for the Contact role ',tempObj.ContactId,tempObj.Contact.Name);
                        contactRoleWrapList.add(contactRoleWrapObj);
                    }
                }
            }
            if(contactRoleWrapList != null && contactRoleWrapList.size() > 0){
                contactRoleCheckBool = true; 
                submitOpptyFlag = false;
                return true;    
            }
            else{
                contactRoleCheckBool = false; 
                submitOpptyFlag = true;
                return false;
            }
        }
        catch(exception exp){
            System.debug('catch:'+exp.getMessage());
            System.debug('catch String:'+exp.getStackTraceString());
        }
        return null;
    }
    
    //Method to display submit button on page
    public Boolean getSubmitOpptyCheck(){
        system.debug(+String.valueof(opptyStageError)+String.valueof(assetCodeError)+String.valueof(assetdescError)+String.valueof( opptyfeaTypeError)+String.valueof(estSyncFeeError)+String.valueof(AttList.size())+String.valueof(venstatuserror)+String.valueof(agrstatuserror)+String.valueof(asssuperror)+String.valueof(CustomerAccountCheckBool)+String.valueof(vendorAccountCheckBool)+String.valueof(COIAssetStatusCheckBool)+String.valueof( contactAddCheckBool)+String.valueof(COIPartnerCheckBool)+String.valueof(partnerAddCheckBool)+String.valueof(primaryProdCheckBool));
        //if(opptyStageError == true ||  assetCodeError == true ||   assetdescError == true ||   opptyfeaTypeError == true ||   estSyncFeeError == true ||   venstatuserror == true ||   agrstatuserror == true ||   asssuperror == true ||   CustomerAccountCheckBool == true ||   vendorAccountCheckBool == true ||   contactAddCheckBool ==  true ||   COIPartnerCheckBool == true || COIAssetStatusCheckBool == true ||  partnerAddCheckBool == true || primaryProdCheckBool == true || primaryProdAddCheckBool == true || contactRoleCheckBool == true  || contactRoleAddCheckBool == true) {
        if(opptyStageError == true ||  assetCodeError == true ||   assetdescError == true ||   opptyfeaTypeError == true ||   estSyncFeeError == true ||   venstatuserror == true ||   agrstatuserror == true ||   asssuperror == true ||   CustomerAccountCheckBool == true ||   vendorAccountCheckBool == true ||   contactAddCheckBool ==  true ||   COIPartnerCheckBool == true || partnerAddCheckBool == true || primaryProdCheckBool == true || primaryProdAddCheckBool == true || contactRoleCheckBool == true) {
            return false;
        }
        else {
            return true;
        }
    }
    
    //Method to check promary Product on oppty exist before submitting 
    public Boolean getAddPrimaryProd(){
        opptyLineItemObj = new OpportunityLineItem();
        
        try{
            opptyLineItemObj = [Select Id,Product2.Name,Primary_Product__c,Doc_Origination_Fee__c,Cost_of_Funds__c,Pricing_Yield1__c,Spread__c,Deal_Term_in_Months__c,NPV__c From OpportunityLineItem WHERE Primary_Product__c = true AND Opportunityid = :opptyId];
            System.debug('opptyLineItemObj:'+opptyLineItemObj);
            if(opptyLineItemObj == null){
                primaryProdAddCheckBool = true;
                submitOpptyFlag = false;
                return true;
            }
            else{
                primaryProdAddCheckBool = false;
                submitOpptyFlag = true;
                return false;
            }
        }
        catch(Exception exp){
            System.debug('catch:'+exp.getMessage());
            System.debug('catch String:'+exp.getStackTraceString());
        }
        return null;
    }
    
    //Method to check the primary product field added to Oppty record.
    public Boolean getCheckPrimaryProduct(){
        system.debug('opptyLineItemObj:'+opptyLineItemObj);
        prodWrap prodWrapObj = new prodWrap('',null,'');
        try{
            if(opptyLineItemObj != null && opptyLineItemObj.Id != null){
                if(opptyLineItemObj.Doc_Origination_Fee__c == null ){ // || opptyLineItemObj.Doc_Origination_Fee__c == 0.00){
                    prodWrapObj = new prodWrap('Please fill value for Doc + Origination Fee on the Primary Product of opportunity',opptyLineItemObj.Id,opptyLineItemObj.Product2.Name);
                    prodWrapList.add(prodWrapObj);
                } 
                if(opptyLineItemObj.Cost_of_Funds__c == null ){ // || opptyLineItemObj.Cost_of_Funds__c == 0.00){
                    prodWrapObj = new prodWrap('Please fill value for Cost of Funds on the Primary Product of opportunity',opptyLineItemObj.Id,opptyLineItemObj.Product2.Name);
                    prodWrapList.add(prodWrapObj);
                }
                if(opptyLineItemObj.Pricing_Yield1__c== null ){ // || opptyLineItemObj.Pricing_Yield1__c== 0.00){
                    prodWrapObj = new prodWrap('Please fill value for Pricing Yield on the Primary Product of opportunity',opptyLineItemObj.Id,opptyLineItemObj.Product2.Name);
                    prodWrapList.add(prodWrapObj);
                }
                if(opptyLineItemObj.Spread__c == null ){ // || opptyLineItemObj.Spread__c == 0.00){
                    prodWrapObj = new prodWrap('Please fill value for Spread on the Primary Product of opportunity',opptyLineItemObj.Id,opptyLineItemObj.Product2.Name);
                    prodWrapList.add(prodWrapObj);
                }
                if(opptyLineItemObj.Deal_Term_in_Months__c == null ){ // || opptyLineItemObj.Deal_Term_in_Months__c == 0.00){
                    prodWrapObj = new prodWrap('Please fill value for Deal Term (in Months) on the Primary Product of opportunity',opptyLineItemObj.Id,opptyLineItemObj.Product2.Name);
                    prodWrapList.add(prodWrapObj);
                }
                if(opptyLineItemObj.NPV__c == null ){ // || opptyLineItemObj.NPV__c == 0.00){
                    prodWrapObj = new prodWrap('Please fill value for NPV on the Primary Product of opportunity',opptyLineItemObj.Id,opptyLineItemObj.Product2.Name);
                    prodWrapList.add(prodWrapObj);
                }
            }        
            if(prodWrapList != null && prodWrapList.size() > 0){
                primaryProdCheckBool = true; 
                submitOpptyFlag = false;
                return true;    
            }
            else{
                primaryProdCheckBool = false; 
                submitOpptyFlag = true;
                return false;
            }
        }
        catch(exception exp){
            System.debug('catch:'+exp.getMessage());
            System.debug('catch String:'+exp.getStackTraceString());
        }
        return null;
    }
    
    //Method to update oppty if all validations are checked and passed.
    public PageReference updateoppty(){
        try{
            if(submitOpptyFlag == true){
                opportunity opptyTemp = new opportunity();
                opptyTemp = opptyObj;
                opptyTemp.Send_to_Express_Link__c = true;
                opptyTemp.Expresslink_Stage__c = 'Submitted';
                if(opptyTemp.Wm_Status__c == null || opptyTemp.Wm_Status__c == '')
                    opptyTemp.Wm_Status__c = 'Not Picked Up';
                opptyTemp.recordtypeId = OpptyRTId1;
                update opptyTemp;
                System.debug('Oppty Submitted.');
                PageReference pageRef = new PageReference ('/'+opptyObj.Id );
                return pageRef ;
            }
        }
        catch(exception exp){
            System.Debug('exp.getMessage():'+exp.getMessage());
            system.debug('Stack String:'+exp.getStacktraceString());
        }
        return null;
    }
    public PageReference bckOppty(){
        PageReference pageRef = new PageReference ('/'+opptyObj.Id );
        return pageRef ;
    }
    
    //Wrapper class for creating account record link on page
    public class accWrap{
        public String firstMsg{get;set;}
        public Id linkMsg{get;set;}
        public String linkName{get;set;}
        
        public accWrap(String firstMsg, Id linkMsg, String linkName){
            this.firstMsg = firstMsg;
            this.linkMsg = linkMsg;
            this.linkName = linkName;
        }
    }
    
    //Wrapper class for creating contact record link on page
    public class conWrap{
        public String firstMsg{get;set;}
        public Id linkMsg{get;set;}
        public String linkName{get;set;}
        
        public conWrap(String firstMsg, Id linkMsg, String linkName){
            this.firstMsg = firstMsg;
            this.linkMsg = linkMsg;
            this.linkName = linkName;
        }
    }
    
    //Wrapper class for creating primary product record link on page
    public class prodWrap{
        public String firstMsg{get;set;}
        public Id linkMsg{get;set;}
        public String linkName{get;set;}
        
        public prodWrap(String firstMsg, Id linkMsg, String linkName){
            this.firstMsg = firstMsg;
            this.linkMsg = linkMsg;
            this.linkName = linkName;
        }
    }
    
    //Wrapper class for creating Contact Role record link on page
    public class contactRoleWrap{
        public String firstMsg{get;set;}
        public Id linkMsg{get;set;}
        public String linkName{get;set;}
        
        public contactRoleWrap(String firstMsg, Id linkMsg, String linkName){
            this.firstMsg = firstMsg;
            this.linkMsg = linkMsg;
            this.linkName = linkName;
        }
    }
}