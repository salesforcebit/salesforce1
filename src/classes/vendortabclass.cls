//========================================================================
//  KeyBank
//  Object: Opportunity
//  Author: Offshore
//  Detail: To associate a vendor to opportunity
//========================================================================
public class vendortabclass 
{
public string venIdChosen {get;set;}
public List<Vendor_Agreement__c> vendorlist {get;set;}
Public opportunity agven {get;set;}
public Vendor_Agreement__c selvendor {get;set;}
public Vendor_Agreement__c vendorl {get;set;}
opportunity op = new opportunity();
public vendortabclass(ApexPages.StandardController controller) 
{
   // try{
        agven=[Select id,Vendor_Agreement__c,Vendor_Agreement__r.Agreement_Name__c,Vendor_and_Agreement_Association__c from opportunity where id=: controller.getRecord().id];
        vendorlist =[select id,name,Account_Vendor__c,Account_Vendor__r.Vendor_status__c,Vendor_status__c  from Vendor_Agreement__c where Agreement__c=:agven.Vendor_Agreement__c];
        //system.debug(vendorlist.Vendor_status__c);
        if(agven.Vendor_and_Agreement_Association__c!=null)
        {
            vendorl=[select id,name,Account_Vendor__c,Vendor_status__c from Vendor_Agreement__c where id=:agven.Vendor_and_Agreement_Association__c];
        }
   // }
    //catch(exception e){
        //system.debug('catch:'+e.getMessage());
        //system.debug('catch:'+e.getStackTraceString());
    //}
}
public void selectedvendors()
    {
        try{
        selvendor=[select id,name,Account_Vendor__c,Vendor_status__c from Vendor_Agreement__c where name=:venIdChosen];
        op.id=agven.id;
        op.Vendor_and_Agreement_Association__c=selvendor.id;
        op.Vendor__c=selvendor.Account_Vendor__c;
        update op;
        }
        catch(exception e){
            system.debug('catch:'+e.getMessage());
            system.debug('catch:'+e.getStackTraceString());
        }
    }
public PageReference redirect() 
    {
        PageReference nextPage = new PageReference('/' + agven.Id);
        return nextPage;
    }
}