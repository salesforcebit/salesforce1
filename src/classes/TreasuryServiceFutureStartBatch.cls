global class TreasuryServiceFutureStartBatch implements Database.Batchable<SObject> {
	
	global String query = 'Select Id, LLC_BI__Stage__c From LLC_BI__Treasury_Service__c Where LLC_BI__Stage__c = \'' + Constants.treasuryServiceStagePendingFutureStartDate + '\' And Planned_Implementation_Start_Date__c = today';
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<SObject> scope) {
		List<LLC_BI__Treasury_Service__c> updateTreasuryServices = new List<LLC_BI__Treasury_Service__c>();
		for(LLC_BI__Treasury_Service__c treasuryService : (List<LLC_BI__Treasury_Service__c>)scope) {
			treasuryService.LLC_BI__Stage__c = Constants.treasuryServiceStageFulfillment;
			updateTreasuryServices.add(treasuryService);
		}
		
		if(updateTreasuryServices.size() > 0) {
			update updateTreasuryServices;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}