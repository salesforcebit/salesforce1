public with sharing class AuthorizedUserServiceOptionsVFC {

	public Id treasuryServiceId {get;set;}
    public String title {get;set;}

	private String queryString {get {
		if(queryString == null) {
			Set<String> fieldNames = new Set<String> {'Id', 'Service_Options__c', 'Analyzed_Account__c','Analyzed_Account__r.Deposit_Name__c','Analyzed_Account__r.Bank_Number__c', 'Analyzed_Account__r.Account_Number2__c', 'Analyzed_Account__r.Service_Options__c', 'Authorized_User__c', 'Authorized_User__r.First_Name__c', 'Authorized_User__r.Last_Name__c'};
			for(Mass_Add_Accounts__mdt metadata : serviceOptionMetadata) {
				fieldNames.add(metadata.Field_to_Display__c);
			}
			fieldNames.remove(null);
			fieldNames.remove('');

			queryString = 'Select ';
			for(String fieldName : fieldNames) {
				queryString += fieldName + ', ';
			}

			queryString = queryString.removeEnd(', ');
			queryString += ' from Users_and_Accounts__c Where Analyzed_Account__r.LLC_BI__Treasury_Service__c = \'' + treasuryServiceId + '\'';
		}
		return queryString;
	}set;}

	public List<Users_and_Accounts__c> usersAndAccounts {get {

		if(usersAndAccounts == null) {
			usersAndAccounts = Database.query(queryString);
		}

		return usersAndAccounts;
	}set;}

	public String selectedUser {get {if(selectedUser == null && userOptions.size() > 0) return userOptions[0].getValue();return selectedUser;}set;}

	public List<UserAndAccountServiceOptionsWrapper> usersAndAccountsForUser {get {

		if(usersAndAccountsForUser == null) {
			usersAndAccountsForUser = new List<UserAndAccountServiceOptionsWrapper>();
			for(UserAndAccountServiceOptionsWrapper uaasow : usersAndAccountWrappers) {
				if(uaasow.record.Authorized_User__c == selectedUser) {
					usersAndAccountsForUser.add(uaasow);
				}
			}
		}

		return usersAndAccountsForUser;
	}set;}

	public List<UserAndAccountServiceOptionsWrapper> usersAndAccountWrappers {get {

		if(usersAndAccountWrappers == null) {
			usersAndAccountWrappers = new List<UserAndAccountServiceOptionsWrapper>();
			for(Users_and_Accounts__c uaa : usersAndAccounts) {
				usersAndAccountWrappers.add(new UserAndAccountServiceOptionsWrapper(uaa));
			}
		}

		return usersAndAccountWrappers;
	}set;}

	public List<SelectOption> userOptions {get {
		if(userOptions == null) {
			userOptions = new List<SelectOption>();
			Set<String> userIds = new Set<String>();
			for(Users_and_Accounts__c uaa : usersAndAccounts) {
				if(!userIds.contains(uaa.Authorized_User__c)) {
					userOptions.add(new SelectOption(uaa.Authorized_User__c, uaa.Authorized_User__r.First_Name__c + ' ' + uaa.Authorized_User__r.Last_Name__c));
				}
				userIds.add(uaa.Authorized_User__c);
			}
		}
		return userOptions;
	}set;}

	public static List<String> serviceOptionFields(List<ServiceOptionWrapper> serviceOptions) {
      Set<String> apiFieldNames = new Set<String>();
      if(serviceOptions == null) return new List<String> (apiFieldNames);

      Set<String> selectedServiceOptions = new Set<String>();
      for(ServiceOptionWrapper serviceOption : serviceOptions) {
        if(serviceOption.isChecked == true) {
          selectedServiceOptions.add(serviceOption.optionName);
        }
      }

      for(Mass_Add_Accounts__mdt serviceOptionMeta : serviceOptionMetadata) {
        if(selectedServiceOptions.contains(serviceOptionMeta.Service_Option_Value__c)) {
          apiFieldNames.add(serviceOptionMeta.Field_to_Display__c);
        }
      }

      return new List<String>(apiFieldNames);
    }

	public static List<Mass_Add_Accounts__mdt> serviceOptionMetadata {get {
		if(serviceOptionMetadata == null) {
			serviceOptionMetadata = [Select Service_Option_Value__c, Field_to_Display__c from Mass_Add_Accounts__mdt];
		}
		return serviceOptionMetadata;
	}set;}

	public AuthorizedUserServiceOptionsVFC(ApexPages.StandardController std) {
		treasuryServiceId = std.getId();
	}

	public PageReference selectUser() {
		usersAndAccountsForUser = null;
		return null;
	}

	public void updateServiceOptions() {
		for(UserAndAccountServiceOptionsWrapper uaasow : usersAndAccountsForUser) {
			uaasow.updateServiceOptions();
		}
    }

	public PageReference saveOptions() {
		SavePoint sp = Database.setSavePoint();

		try {

			update usersAndAccounts;

			ApexPages.standardController treasuryServiceController = new ApexPages.standardController(new LLC_BI__Treasury_Service__c(id = treasuryServiceId));
   			return treasuryServiceController.view();

		} catch(Exception e) {
			Database.rollback(sp);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
		}
		return null;
	}

	public class UserAndAccountServiceOptionsWrapper {
		public Users_and_Accounts__c record {get;set;}
       	public List<ServiceOptionWrapper> serviceOptions {get;set;}
		public List<String> apiFieldNames {get {
	        if(apiFieldNames == null) {
				apiFieldNames = serviceOptionFields(serviceOptions);
	        }
	        return apiFieldNames;
	    }set;}

		public UserAndAccountServiceOptionsWrapper(Users_and_Accounts__c uaa) {
			record = uaa;
			serviceOptions = new List<ServiceOptionWrapper>();

			//Need to pull the available service options from the Analyzed Account, but use the selected ones from the Users and Accounts record itself
			Set<String> selectedServiceOptions = new Set<String>();
			if(record.Service_Options__c != null) {
				for(String option : record.Service_Options__c.split(';')) {
					selectedServiceOptions.add(option);
				}
			}

			if(record.Analyzed_Account__r.Service_Options__c != null) {
	   			for(String option : record.Analyzed_Account__r.Service_Options__c.split(';')) {
					serviceOptions.add(new ServiceOptionWrapper(option, selectedServiceOptions.contains(option)));
	   			}
	   		}
		}

		public void updateServiceOptions() {
	        Map<String, Object> fieldValues = new Map<String, Object>();
	        //Save old values and clear all variables when the service options selected change
	        for(String fieldName : apiFieldNames) {
				fieldValues.put(fieldName, record.get(fieldName));
				record.put(fieldName, null);
	        }
	        //Recalculate field names based on new service options
	        apiFieldNames = null;
	        //Replace field values from previously existing values
	        for(String fieldName : apiFieldNames) {
	        	record.put(fieldName, fieldValues.get(fieldName));
	        }

	        //Also update the service option field on the record itself
	        record.Service_Options__c = '';
			for(ServiceOptionWrapper sow : serviceOptions) {
				if(sow.isChecked) {
					record.Service_Options__c += sow.optionName + ';';
				}
			}
			record.Service_Options__c = record.Service_Options__c.removeEnd(';');
	     }
	}

	public class ServiceOptionWrapper {
   		public Boolean isChecked {get;set;}
   		public String optionName {get;set;}

  		public ServiceOptionWrapper(String option, Boolean defaulted) {
  			optionName = option;
  			isChecked = defaulted;
  		}	
   	}
}