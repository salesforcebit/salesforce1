/*******************************************************************************************************************************************
* @name         :   ECCP_McaLoanRecUpdate
* @description  :   This class updates MCA details in loan record.
* @author       :   Nagarjun
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
public class ECCP_McaLoanRecUpdate{
    public static ECCP_MCA_WSIB_Response updateLoan( ECCP_Compliance__c complianceQuerried, ECCP_MCA_WSIB_Request loanReqInfo){
        
        ECCP_MCA_WSIB_Response updateResonse = new ECCP_MCA_WSIB_Response(); 
        //try{
            List<ECCP_Compliance__c> updateCompliance = new List<ECCP_Compliance__c>();
            
            if(complianceQuerried.MCAStatus__c =='PEND' || complianceQuerried.MCAStatus__c =='N' ){
                ECCP_Compliance__c complianceIns = new ECCP_Compliance__c ();
                complianceIns.id = complianceQuerried.id;
                complianceIns.MCAStatus__c= loanReqInfo.MCIPassFail == 'Y' ? 'PASS' : (loanReqInfo.MCIPassFail == 'PASS' ? 'PASS' : 'FAIL' ) ;
                //complianceIns.ECCP_Application_Number__c = loanReqInfo.ApplNbr;
                //Account number will not be updated in loan object
                //complianceIns.ECCP_CIU_Reference_Sequence_Number__c = loanReqInfo.CIURefSeqNbr;
                complianceIns.MCA_Reference_ID__c = loanReqInfo.CIURefNbr;
                complianceIns.RegO__c = loanReqInfo.RegOPassFail;
                complianceIns.RegW__c = loanReqInfo.RegWPassFail;
                complianceIns.PA326__c = loanReqInfo.PA326Pass;
                complianceIns.OFAC__c = loanReqInfo.OFACPassFail;
                complianceIns.HotFile__c = loanReqInfo.HotFIlePassFail;
                complianceIns.FAAlert__c = loanReqInfo.FAPassFail;
                complianceIns.ECCP_MCA_Error_Type__c= loanReqInfo.ErrorTp; 
                complianceIns.MCA_Error_Code__c = loanReqInfo.ErrorCd;
                //complianceIns.MCA_Proceed_Code__c = loanReqInfo.MCAPassPendResponseFlag;
                complianceIns.MCA_Error_Description__c = loanReqInfo.errorDescription ;
                updateCompliance.add(complianceIns);
                //loanReqInfo.AcctNbr;
                //complianceIns.McaResponse__c = loanReqInfo.McaResponse;
                system.debug('---updateCompliance --'+updateCompliance );
                if(updateCompliance != null && updateCompliance .size()>0){
                    update updateCompliance ; 
                    updateResonse = updateLoanResponseCall(updateCompliance );
                }       
                
            }else{
                updateResonse.sfdcResponse = ECCP_IntegrationConstants.pending;
            }
            
        /*}catch(exception e){   
            ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.MCAInService, currentClassName, ECCP_IntegrationConstants.updateLoan , string.valueOf(e.lineNumber()) ,e.getMessage() , ECCP_IntegrationConstants.NullValue,e.getStackTraceString());                      
            updateResonse.sfdcResponse = 'Error -'+ECCP_IntegrationConstants.updateFail +':'+ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.updateFail).get(ECCP_IntegrationConstants.updateFail).ECCP_Error_Message__c;
            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
             
        }               
        */  
        return updateResonse;
    }
    public static ECCP_MCA_WSIB_Response updateLoanResponseCall(List<ECCP_Compliance__c > updateCompliance ){
        ECCP_MCA_WSIB_Response updateResonse = new ECCP_MCA_WSIB_Response();
        for(ECCP_Compliance__c ComplianceData :updateCompliance ){

            updateResonse.custId = ComplianceData.MCA_Reference_ID__c ;
            updateResonse.custSsn = ComplianceData.Loan_ID__c;
            updateResonse.sfdcResponse = ECCP_IntegrationConstants.Success;
        }
       return updateResonse; 
    }
}