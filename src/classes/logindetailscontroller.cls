public with sharing class logindetailscontroller{

        public PageReference display() {
        return null;
    }
    public logindetailscontroller() {

    }
  
    Public integer monthlogin{get;set;}
    Public integer lastMonthLogin{get;set;}
    Public integer thiscallcount{get;set;}
    Public integer lastcallcount{get;set;}
    Public integer pastOptyCount{get;set;}
    Public integer unmodifiedOptyCount{get;set;}
    Public Id ReportOptyWoPursue{get;set;}
    Public Id ReportPipeline{get;set;}
    Public Id ReportOpenOptys{get;set;}
    Public Id ReportAccounts{get;set;}
    
    public List<Report> lstreportOptyWoPursue=new List<Report>();
    public List<Report> lstreportpipeline=new List<Report>();
    public List<Report> lstreportOpenOptys=new List<Report>();
    public List<Report> lstreportAccount=new List<Report>();
         
    User_Progress_Detail__c loginUserProgress{get;set;}
    
    public void updateProgress(){
    
    if(!Test.IsRunningTest())
    {
    lstreportOptyWoPursue=[SELECT DeveloperName,Format,Id,Name FROM Report WHERE DeveloperName= :'Opportunities_w_o_Pursue_Stage'];
        ReportOptyWoPursue= lstreportOptyWoPursue[0].Id;
    
    lstreportpipeline=[SELECT DeveloperName,Format,Id,Name FROM Report WHERE DeveloperName= :'Pipeline_Threshold'];
        ReportPipeline= lstreportpipeline[0].Id;
    
    lstreportOpenOptys=[SELECT DeveloperName,Format,Id,Name FROM Report WHERE DeveloperName= :'Open_Opportunities_this_Month'];
    
    ReportOpenOptys= lstreportOpenOptys[0].Id;
    
    lstreportAccount=[SELECT DeveloperName,Format,Id,Name FROM Report WHERE DeveloperName= :'Accounts_without_Contacts'];
        ReportAccounts= lstreportAccount[0].Id;
    }
        List<User> allUsersList = new List<User>();
        allUsersList = [SELECT id, Name FROM user];
        Map<id, String> userIdVsUserName = new Map<id, String>();
             
        for(user u : allUsersList){
            userIdVsUserName.put(u.id, u.Name);
        }
    
    id userId = UserInfo.getUserId(); 
    loginUserProgress = new User_Progress_Detail__c();
    
    try{         
        system.debug('user id----'+userId);
        loginUserProgress = [SELECT Name, Last_Month_Login_Count__c, This_Month_Login_Count__c, Last_Month_Call_Count__c, 
                            This_Month_Call_Count__c, Past_Due_Opportunities__c, Unmodified_Opportunities__c 
                            FROM User_Progress_Detail__c WHERE User__c = :userId];            
      }catch(exception e){}  
               
            set<id> userIdSet = new set<id>();
            userIdSet.add(userId);         
            
            Map<Id, Integer> userIdVsLastLoginCount = new Map<Id, Integer>();
            Map<Id, Integer> userIdVsThisLoginCount = new Map<Id, Integer>();
            Map<Id, Integer> userIdVsLastCallCount = new Map<Id, Integer>();
            Map<Id, Integer> userIdVsThisCallCount = new Map<Id, Integer>();
            Map<Id, Integer> userIdVsPastDueOppCount = new Map<Id, Integer>();
            Map<Id, Integer> userIdVsUnmodifiedOppCount = new Map<Id, Integer>();
            
            userIdVsLastLoginCount = UserLoginDetails.UserLoginCounts(userIdSet, 'LAST_MONTH'); 
            userIdVsThisLoginCount = UserLoginDetails.UserLoginCounts(userIdSet, 'THIS_MONTH'); 
            userIdVsLastCallCount = callcountprogress.UserCallCounts(userIdSet, 'LAST_MONTH'); 
            userIdVsThisCallCount = callcountprogress.UserCallCounts(userIdSet, 'THIS_MONTH');
            userIdVsPastDueOppCount = UserOptyDetails.UserPastDueOptyCounts(userIdSet);
            userIdVsUnmodifiedOppCount = UserOptyDetails.UserUnmodifiedOptyCounts(userIdSet);
       
            loginUserProgress.User__c = userId;
            loginUserProgress.User_Id__c = userId;
            loginUserProgress.Name = userIdVsUserName.get(userId);
            
            if(userIdVsLastLoginCount.keyset().contains(userId)){   
                    loginUserProgress.Last_Month_Login_Count__c = userIdVsLastLoginCount.get(userId);
                }
                else  
                    loginUserProgress.Last_Month_Login_Count__c = 0;
                if(userIdVsThisLoginCount.keyset().contains(userId)){
                    loginUserProgress.This_Month_Login_Count__c = userIdVsThisLoginCount.get(userId); 
                }
                else
                    loginUserProgress.This_Month_Login_Count__c = 0;    
                if(userIdVsLastCallCount.keyset().contains(userId)){
                    loginUserProgress.Last_Month_Call_Count__c = userIdVsLastCallCount.get(userId); 
                }
                else
                    loginUserProgress.Last_Month_Call_Count__c = 0;
                if(userIdVsThisCallCount.keyset().contains(userId)){
                    loginUserProgress.This_Month_Call_Count__c = userIdVsThisCallCount.get(userId);
                }
                else
                    loginUserProgress.This_Month_Call_Count__c = 0;
                if(userIdVsPastDueOppCount.keyset().contains(userId)){
                    loginUserProgress.Past_Due_Opportunities__c = userIdVsPastDueOppCount.get(userId);
                }
                else
                    loginUserProgress.Past_Due_Opportunities__c = 0;
                if(userIdVsUnmodifiedOppCount.keyset().contains(userId)){
                    loginUserProgress.Unmodified_Opportunities__c = userIdVsUnmodifiedOppCount.get(userId);
                }
                else
                    loginUserProgress.Unmodified_Opportunities__c = 0;  
                     
            upsert loginUserProgress;                                                             
        
            Decimal dec = loginUserProgress.This_Month_Login_Count__c;                                    
            monthlogin = dec.intValue();
            dec = loginUserProgress.Last_Month_Login_Count__c;
            lastMonthLogin = dec.intValue();
            dec = loginUserProgress.This_Month_Call_Count__c;
            thiscallcount = dec.intValue(); 
            dec = loginUserProgress.Last_Month_Call_Count__c;
            lastcallcount = dec.intValue();
            dec =  loginUserProgress.Past_Due_Opportunities__c; 
            pastOptyCount = dec.intValue();
            dec = loginUserProgress.Unmodified_Opportunities__c ;
            unmodifiedOptyCount = dec.intValue();    
          }
}