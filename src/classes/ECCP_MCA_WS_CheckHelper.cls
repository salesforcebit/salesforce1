/*******************************************************************************************************************************************
* @name         :   ECCP_MCA_WS_CheckHelper
* @description  :   This class will be responsible for all the helper methods for ECCP_MCA_CheckHelper webservices
*                   controller for Integration, Parsing, Page redirection etc.
* @author       :   Nagarjun
* @createddate  :   07/04/2016
*******************************************************************************************************************************************/
public class ECCP_MCA_WS_CheckHelper{

    public ECCP_MCA_CheckMCI_Response  resObj;
    public static integer noRequests = 0;
    public ECCP_MCACheckRequestData reqObj;
    private ECCP_PerformanceLog__c plog; 
    private String currentClassName;    
    //Get Service custom setting
    private Map<String,ECCP_WebService_Data__mdt> mapWsSettings;
    public ECCP_MCA_WS_CheckHelper(){
        // Getting current class name
        currentClassName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));            
        mapWsSettings = ECCP_CustomMetadataUtil.queryMetadata(ECCP_IntegrationConstants.MCACheckMCIWS);        
        resObj = new ECCP_MCA_CheckMCI_Response();         
    }
     
/*******************************************************************************************************************************************
* @name         :   ECCP_MCACheckMCIProcess
* @description  :   This method is responsible for communicating with Extenal System 'MCA'.
*                   This method will take the input from UI and fetch the data from MCA.
* @param        :   reqObj  -   ECCP_MCACheckRequestData
*                   A custom wrapper for ECCP_MCACheckRequestData.
* @return       :   resObj  -   ECCP_MCA_CheckMCI_Response
*                   A custom wrapper for MCA response.
* @author       :   Nagarjun
* @createddate  :   07/04/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/  
    public ECCP_MCA_CheckMCI_Response ECCP_MCACheckMCIProcess(ECCP_MCACheckRequestData reqObj){
        ECCP_MCA_CheckWS.RequestDataOutput_Type responseElement = new ECCP_MCA_CheckWS.RequestDataOutput_Type();
        ECCP_MCA_CheckWS.getRequestDataInput reqData = new ECCP_MCA_CheckWS.getRequestDataInput();                
        map<String,ECCP_CheckTheService__mdt> servMap = ECCP_CustomMetadataUtil.queryStubMetadata(ECCP_IntegrationConstants.MCAObServiceStub);        
       // try{
            if(servMap.get(ECCP_IntegrationConstants.MCAObServiceStub).isStubSet__c){
                resObj = ECCP_WebserviceStub.ECCP_MCACheck(reqObj);
            }else{  
            //Get Performance log Object
            //plog = ECCP_UTIL_PerformanceLog.createPerformanceLog(ECCP_IntegrationConstants.MCACheckMCIWS,  mapWsSettings.get(ECCP_IntegrationConstants.MCACheckMCIWS).EndPointUrl__c,ECCP_IntegrationConstants.MCAProcessRequest ,JSON.Serialize(reqObj));
            ECCP_MCA_CheckWS.WebServices_invokeCIUPatriotActWS_Port service = getServiceInstance();   
            system.debug('++++++service ' +service );      
            reqData = getServiceRequestElements(reqObj); 
            system.debug('++++++reqData ' +reqData );              
            // Calling service method
            responseElement = service.getRequestOverHttp(reqData.RequestDataInput); 
            system.debug('+++++responseElement ' +responseElement );
            resObj = populateResponseObject(responseElement);
            // store performance log
            ECCP_UTIL_PerformanceLog.updatePerformanceLog(plog, JSON.serialize(resObj ));
        }
       // }catch(CalloutException coEx){
            //update log
        /*    ECCP_UTIL_PerformanceLog.updatePerformanceLog(plog, JSON.serialize(resObj));
            // log exception and return 
            Map<String,ECCP_Error_Code__mdt>  errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullResponse);
            ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.MCACheckMCIWS, currentClassName, ECCP_IntegrationConstants.MCAObServiceStub, String.valueOf(coEx.getLineNumber()),coEx.getMessage(), 'Record Id',coEx.getStackTraceString());
            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
            resObj.error = errorMessageMap.get(ECCP_IntegrationConstants.nullResponse).ECCP_Error_Message__c;
            return resObj;
        }
         catch(Exception Ex){
            //update log
            ECCP_UTIL_PerformanceLog.updatePerformanceLog(plog, JSON.serialize(resObj));
            // log exception and return 
            Map<String,ECCP_Error_Code__mdt>  errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullResponse);
            ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.MCACheckMCIWS, currentClassName, ECCP_IntegrationConstants.MCAObServiceStub, String.valueOf(Ex.getLineNumber()),Ex.getMessage(), 'Record Id',Ex.getStackTraceString());
            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
            resObj.error = errorMessageMap.get(ECCP_IntegrationConstants.nullResponse).ECCP_Error_Message__c;
            //return resObj;
        } */       
        return resObj;
    }//end of method
    
//*******************************************************************************************************************************************         
/* @name        :   ParseResponse
* @description  :   This method will parse the response.
* @return       :   ECCP_MCA_CheckMCI_Response
* @author       :   Nagarjun
* @createddate  :   07/04/2016
* @lastmodifiedby:  
//***************************AUW credit Response********************************************************************************************/     
    private ECCP_MCA_CheckMCI_Response populateResponseObject(ECCP_MCA_CheckWS.RequestDataOutput_Type responseElement){
    
        if(responseElement!=null){
            resObj.Proceed =  responseElement.Proceed;
            resObj.SupplCode =  responseElement.SupplCode;
            resObj.AddDescInd =  responseElement.AddDescInd;
            resObj.Pass =  responseElement.Pass;
            resObj.Result =  responseElement.Result;
            resObj.CIURefNb =  responseElement.CIURefNb;
            resObj.CIURefSq =  responseElement.CIURefSq;
            resObj.SLADay =  responseElement.SLADay;
            resObj.SLAHour =  responseElement.SLAHour;
            resObj.SLAMin =  responseElement.SLAMin;
            resObj.ErrorTp =  responseElement.ErrorTp;
            resObj.ErrorCd =  responseElement.ErrorCd;
            resObj.ErrorDsc =  responseElement.ErrorDsc;
            resObj.CustType =  responseElement.CustType;
            resObj.BusName =  responseElement.BusName;
            resObj.FrstName =  responseElement.FrstName;
            resObj.MdlName =  responseElement.MdlName;
            resObj.LastName =  responseElement.LastName;
            resObj.FraudActiveAlert =  responseElement.FraudActiveAlert;
            resObj.AddressAlert =  responseElement.AddressAlert;
            resObj.OFACResult =  responseElement.OFACResult;
            resObj.LNRisk1 =  responseElement.LNRisk1;
            resObj.LNRisk2 =  responseElement.LNRisk2;
            resObj.LNRisk3 =  responseElement.LNRisk3;
            resObj.LNRisk4 =  responseElement.LNRisk4;
            resObj.LNRisk5 =  responseElement.LNRisk5;
            resObj.LNRisk6 =  responseElement.LNRisk6;
            resObj.RegOPass =  responseElement.RegOPass;
            resObj.RegWPass =  responseElement.RegWPass;
            resObj.PA326Pass =  responseElement.PA326Pass;
            resObj.OFACPass =  responseElement.OFACPass;
            resObj.HotFilePass =  responseElement.HotFilePass;
            resObj.FAAlertPass =  responseElement.FAAlertPass;
            resObj.AddrCodeField =  responseElement.AddrCodeField;
        }else{
                // log exception
                Map<String,ECCP_Error_Code__mdt>  errorMessage = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.nullResponse);
                ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.MCACheckMCIWS, currentClassName, ECCP_IntegrationConstants.MCAObServiceStub, '',errorMessage.get(ECCP_IntegrationConstants.nullResponse).ECCP_Error_Message__c, 'Record Id','');
                ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
                resObj.error = errorMessage.get(ECCP_IntegrationConstants.nullResponse).ECCP_Error_Message__c;
                return resObj;
            }        
        return resObj;
    }
/*******************************************************************************************************************************************
* @name         :   getServiceRequestElements
* @description  :   This method will set request parameters.
* @return       :   service  -   ECCP_MCA_CheckWS.WebServices_invokeCIUPatriotActWS_Port
*                   A service instance.
* @author       :   Nagarjun
* @createddate  :   11/04/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/      
    private ECCP_MCA_CheckWS.getRequestDataInput getServiceRequestElements(ECCP_MCACheckRequestData reqObj){
        ECCP_MCA_CheckWS.RequestDataInput_Type requestType = new ECCP_MCA_CheckWS.RequestDataInput_Type();
        ECCP_MCA_CheckWS.getRequestDataInput reqData = new ECCP_MCA_CheckWS.getRequestDataInput();
        requestType.AcctNbr =  reqObj.AcctNbr;
        requestType.AddrL1 =  reqObj.AddrL1;
        requestType.AddrL2 =  reqObj.AddrL2;
        requestType.ApplNbr =  reqObj.ApplNbr;
        requestType.BtchOnln =  reqObj.BtchOnln;
        requestType.BureauFA =  reqObj.BureauFA;
        requestType.BureauID =  reqObj.BureauID;
        requestType.BureauInstruct1 =  reqObj.BureauInstruct1;
        requestType.BureauInstruct2 =  reqObj.BureauInstruct2;
        requestType.AddrL3 =  reqObj.AddrL3;
        requestType.AddrL4 =  reqObj.AddrL4;           
        requestType.AddDescInd =  reqObj.AddDescInd; 
        requestType.BusName =  reqObj.BusName;
        requestType.Channel =  reqObj.Channel;
        requestType.Citizen =  reqObj.Citizen;
        requestType.City =  reqObj.City;
        requestType.CntctPh =  reqObj.CntctPh;
        requestType.Country =  reqObj.Country;
        requestType.CustType =  reqObj.CustType;
        requestType.DOB =  reqObj.DOB;
        requestType.DrvIssDt =  reqObj.DrvIssDt;
        requestType.DrvLcNbr =  reqObj.DrvLcNbr;
        requestType.DrvLcSt =  reqObj.DrvLcSt;
        requestType.FrstName =  reqObj.FrstName;
        requestType.LastName =  reqObj.LastName;
        requestType.LOB =  reqObj.LOB;
        requestType.ProdCd =  reqObj.ProdCd;
        requestType.ReteMailAddr =  reqObj.ReteMailAddr;
        requestType.SSN =  reqObj.SSN;
        requestType.State =  reqObj.State;
        requestType.SubProd =  reqObj.SubProd;
        requestType.UserId =  reqObj.UserId;
        requestType.Zip =  reqObj.Zip; 
        //Aditya 2.0 change
        
        requestType.DrvExpDt = reqObj.DrvExpDt ;
        requestType.PsprtIss = reqObj.PsprtIss;
        requestType.PsprtNbr = reqObj.PsprtNbr;
        requestType.PsprtCntr = reqObj.PsprtCntr;
        requestType.PsprtExp = reqObj.PsprtExp;
        requestType.OthIdIssDt = reqObj.OthIdIssDt;
        requestType.OthIdNbr = reqObj.OthIdNbr;
        requestType.OthIdIssSt = reqObj.OthIdIssSt;
        requestType.OthIdExpDt = reqObj.OthIdExpDt;
        requestType.OthIdTyp = reqObj.OthIdTyp;
        requestType.MdlName = reqObj.MdlName;
        
        requestType.PAreaCd = reqObj.PAreaCd;
        requestType.PExchgCd = reqObj.PExchgCd ;
        requestType.PRootNbr = reqObj.PRootNbr ;
        requestType.SAreaCd = reqObj.SAreaCd ;
        requestType.SExchgCd = reqObj.SExchgCd ;
        requestType.SRootNbr = reqObj.SRootNbr ;
        
        reqData.RequestDataInput = requestType; 
        
           
        return reqData;
      }         
/*******************************************************************************************************************************************
* @name         :   getServiceInstance
* @description  :   This method will set the service instance with the setting from Custom setting and return instance.
* @return       :   service  -   ECCP_MCA_CheckWS.WebServices_invokeCIUPatriotActWS_Port
*                   A service instance.
* @author       :   Nagarjun
* @createddate  :   07/04/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/      
    private ECCP_MCA_CheckWS.WebServices_invokeCIUPatriotActWS_Port getServiceInstance(){
        noRequests = noRequests + 1;
        system.debug('++++noRequests' +noRequests );
        ECCP_MCA_CheckWS.WebServices_invokeCIUPatriotActWS_Port service = new ECCP_MCA_CheckWS.WebServices_invokeCIUPatriotActWS_Port ();        
        service.timeOut_x   =   Integer.valueOf(mapWsSettings.get(ECCP_IntegrationConstants.MCACheckMCIWS).TimeOut__c);
        service.endpoint_x  =   mapWsSettings.get(ECCP_IntegrationConstants.MCACheckMCIWS).EndPointUrl__c;
        /*Added to test service un availability in between 
        if(noRequests == 2){
            service.endpoint_x = 'https://tdpaxwebsrvc.key.com:5002/ecc/invokec';
        } */
        service.clientCertName_x =  mapWsSettings.get(ECCP_IntegrationConstants.MCACheckMCIWS).CertificateName__c;     
        return service;
    }
       
} //end of main class