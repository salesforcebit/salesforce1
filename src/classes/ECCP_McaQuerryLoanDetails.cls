/*******************************************************************************************************************************************
* @name         :   ECCP_McaQuerryLoanDetails
* @description  :   This class gets the corresponding loan record from nCino for the request came from MCA.
* @author       :   Nagarjun
* @createddate  :   21/03/2016
*******************************************************************************************************************************************/
public class ECCP_McaQuerryLoanDetails{
   /* public static LLC_BI__Loan__c fetchLoanRec( ECCP_MCA_WSIB_Request loanReqInfo){
        LLC_BI__Loan__c loanIns = [ select id,MCA_Status__c from LLC_BI__Loan__c where MCA_Reference_ID__c = :loanReqInfo.CIURefNbr limit 1];        
        return loanIns ;
    }*/
    public static ECCP_Compliance__c fetchLoanRec( ECCP_MCA_WSIB_Request complianceInfo){
        ECCP_Compliance__c complianceIns= [ select id,MCAStatus__c from ECCP_Compliance__c where MCA_Reference_ID__c = :complianceInfo.CIURefNbr limit 1];        
        return complianceIns;
    }    
    
}