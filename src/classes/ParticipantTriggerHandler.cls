//================================================================================
//  KeyBank  
//  Object: ParticipantTriggerHandler 
//  Author: Offshore
//  Detail: Participant Trigger Handler Class
//================================================================================
//          Date            Purpose
// Created: 03/07/2014      Initial Version
// Changes: 06/29/2015      July release 
//================================================================================


public with sharing class ParticipantTriggerHandler 
{
    
    private static boolean run = true;
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
     
    Set<ID> userNames = new Set<ID>();
   
    public ParticipantTriggerHandler (boolean isExecuting, integer size)
    {
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
       
    public void OnAfterInsert(list<Participant__c> Pparti)
    {
    
    }
    
    @future public static void OnAfterInsertAsync(list<id> tId, list<id> tIdp, list<id> tIdc)
    {
        
        system.debug(tId);
        system.debug(tIdc);
        Map<Id,User> useract =new Map<Id,User>([select Id, isactive from user where Isactive =: true]);
        List<Call__c> Ccall = [select id,Is_Private__c  from Call__c where Id =:tId];
        List<Call__Share> callshare = new List <Call__Share> (); 
        Map<Id,contact> usermap =new Map<Id,contact>([Select Id,Name,User_Id__c,Contact_Type__c from contact where Id =: tIdc ]);
        List <Participant__c> partidata = [SELECT CallId__c,Contact_Id__c,Id,Name,Contact_Type__c FROM Participant__c where  id =: tIdp]; 
        
        for (Call__c c : Ccall)
        {       
            for (participant__c p : partidata)
            {
                contact us = usermap.get(p.Contact_Id__c);
                system.debug(c.id);
                //system.debug(p.CallId__c);
                system.debug(p.Contact_Type__c);
               
                if (c.id == p.CallId__c) 
                {
                    if(p.Contact_Type__c == 'Key Employee')
                    {
                    if(useract.containskey(us.User_Id__c))
                    {
                        //system.debug(us.User_Id__r.Isactive);
                        system.debug(us.id);
                        callshare.add(new Call__Share (AccessLevel = 'Edit',ParentId = c.Id,UserOrGroupId = us.User_Id__c ));
                    }
                    }           
                }
            }
        }
        try 
        {
            insert (callshare);
            system.debug(callshare);
        }
        catch (Exception Ex)
        {
            system.debug(Ex);
        }    
    }
    
    @future public static void changeowner(list<id> pIds)
    {
        List<Participant__c> parties = [SELECT Id, ownerid,Contact_Id__r.User_Id__r.RACF_Number__c, Contact_Id__r.Salesforce_User__c, Contact_Id__r.User_Id__c,Contact_Id__r.RACFID__c,Contact_Id__r.Contact_Type__c FROM Participant__c WHERE Id =: pIds];
        List<Participant__c> partiupdate = new List <Participant__c> ();
        system.debug(parties);
        for(Participant__c p : parties )
        {
        if(p.Contact_Id__r.Contact_Type__c == 'Key Employee' && p.Contact_Id__r.User_Id__r.RACF_Number__c == p.Contact_Id__r.RACFID__c)
            {
                 p.OwnerId = p.Contact_Id__r.User_Id__c;
                 partiupdate.add(p);
            }      
           
        }
         try 
         {
             update partiupdate;
             system.debug(partiupdate);
         }
         catch (Exception Exp)
        {
            system.debug(Exp);
        }
    }
    
    public void updateRelatedRecordshandler(list<id> pIds)
    {
        ParticipantTriggerHelper.updateRelatedRecords(pIds);
    }
    
    public void callgoalsjunctionhandler(list<Id> parrecId)
    {
        ParticipantTriggerHelperCGJ.callgoalsjunction(parrecId);
    }
    
    //Added method to alert participant that the call has been shared with them and providing to the call record.
    //Reqiurement # 1175 March Release 2016
    /*
    @future public static void sendMailPart(list<id> pIds){
        try{
            List<Participant__c> parties = [SELECT Id,CallId__c,CallId__r.Name,LastModifiedBy.Name,Contact_Id__c,Contact_Id__r.name,Contact_Id__r.firstname,Contact_Id__r.lastname,Contact_Id__r.User_Id__c,Contact_Id__r.Contact_Email__c,Contact_Type__c FROM Participant__c WHERE Id =: pIds];
            System.Debug('Participants Added:'+parties);
            Map<Participant__c,String> emailMap = new Map<Participant__c,String>();
            Set<Id> UserIdSet = New Set<Id>();
            for(Participant__c partRec : parties){
                if(partRec.Contact_Type__c == 'Key Employee'){
                    UserIdSet.add(partRec.Contact_Id__r.User_Id__c);
                }
            }
            List<User> userList = new List<User>();
            userList = [SELECT id,isactive,email FROM user WHERE Isactive =: true AND id IN :UserIdSet];
            System.Debug('userList:'+userList.size());
            if(userList != null && userList.size() > 0){
                for(User userRec : userList){
                    for(Participant__c partRec : parties){
                        if(userRec.Id == partRec.Contact_Id__r.User_Id__c)
                        emailMap.put(partRec, String.valueOf(userRec.Email));
                    }
                }
                System.Debug('emailMap:'+emailMap);
            }
            System.Debug('Email List Final:'+emailMap.size());
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            for(Participant__c partRec : parties){
                if(emailMap.containsKey(partRec) && (emailMap.get(partRec) != null || emailMap.get(partRec) != '')){
                    System.Debug('Data Found');
                    List<String> toAddresses= new List<String>();
                    toAddresses.add(String.valueof(emailMap.get(partRec)));
                    String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + partRec.CallId__c;
                    String dateval= System.Today().format();
                    
                    String emailMessage = '<font face="Times New Roman"><br/>'
                    + 'To '+(partRec.Contact_Id__r.firstname == null ? '': partRec.Contact_Id__r.firstname)+' '+partRec.Contact_Id__r.lastname+', </br></br>'
                    +'This email is to inform you that the Call ' +partRec.CallId__r.Name+' have been shared with you by '+partRec.LastModifiedBy.Name+'. </br></br>'
                    + ' You can access the record by clicking here: '+fullRecordURL +'<br/><br/>'  
                    +'Regards <br/><br/>'+partRec.LastModifiedBy.Name;
                    //mail.setReplyTo('Renuka_Singh@keybank.com');
                    mail.setSenderDisplayName(string.valueOf(partRec.LastModifiedBy.Name));
                    mail.setBccSender(false);
                    mail.setToAddresses(toAddresses);
                    mail.setSubject(partRec.CallId__r.Name+' is shared with you.');       
                    mail.setUseSignature(false);
                    mail.saveAsActivity = false;
                    mail.setHTMLBody(emailMessage); 
                    //Send email
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    system.debug('Mail Sent Successfully');
                }
            }
        } 
        Catch(Exception e){
            System.Debug('Mail Sending Error:'+e.getMessage());
            System.Debug('Mail Sending Error:'+e.getStackTraceString());
        }             
    }
    */
    /*public void callgoalsjunction(list<Id> parrecId)
    {
        system.debug('ParticipantName' + parrecId);
        List<Call_Goals_Junction__c> cglL = new List<Call_Goals_Junction__c>();
        List<Call_Goals__c> cgoalL = [Select Id, Username__c, year__c, Goal_Month__c from Call_Goals__c];
        List<Participant__c> ptlist = [Select Id, Name, CallId__c, Call_Date__c, Did_Not_Participate__c from Participant__c where Id =: parrecId];
        system.debug(cgoalL.size());
        {
            for(Call_Goals__c cgoal : cgoalL)
            {        
                for(Participant__c partL : ptlist )
                {
                    List<Call__C> callsL = [Select Id, Name,Call_Date__c from Call__c where Id =: partL.CallId__c];
                    system.debug('CallList' + callsL);
                    for(Call__c callL : callsL)
                    {
                        if(partL.Did_Not_Participate__c == false && cgoal.Goal_Month__c == partL.Call_Date__c)
                        {
                            Call_Goals_Junction__c cgj = new Call_Goals_Junction__c();
                            cgj.Participant__c = partL.Id;
                            cgj.Call_Goals__c = cgoal.Id;
                            cgj.Call__c = callL.Id;
                            cglL.add(cgj);
                        }
                    }
                }
            }
        }
        //insert cglL;
        system.debug(cglL );
    } */
   public static boolean runOnce(){
    if(run){
     run=false;
     return true;
    }else{
        return run;
    } 
  }  
}