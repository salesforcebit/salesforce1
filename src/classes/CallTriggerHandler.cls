//================================================================================
//  KeyBank  
//  Object: CallTriggerHandler 
//  Author: Offshore
//  Detail: Call Trigger Handler Class
//          It handler class will automatically add call owner in participant list
//================================================================================
//          Date            Purpose
//Changes: 03/07/2014      Initial Version
//         04/06/2015      Selection based on RACF-ID
//================================================================================


public with sharing class CallTriggerHandler 
{
    public static boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    List<Participant__c> addparticipant = new List<Participant__c>();
    Map<Id,user> usermap =new Map<Id,user>([Select Id,Name,RACF_Number__c from user where isactive=true]);
    Set<String> userNames = new Set<String>();
    
    public CallTriggerHandler(boolean isExecuting, integer size)
    {
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
          
    public void OnAfterInsert(list<Call__c> Ccall)    
    {
        m_isExecuting = true;        
        for(Call__c call: Ccall)
        {
            if(call.OwnerId != null)
            {
                User us = usermap.get(call.Ownerid);
                userNames.add(us.RACF_Number__c);
            }
        }
        
        if(userNames.size() > 0)
        {
            List<Contact> userNameContacts = [Select Id, Name,RACFID__c from Contact where RACFID__c IN : userNames and Contact_Type__c='Key Employee' and Salesforce_User__c=true ]; 
    
            for(Call__c call : Ccall)
            {
                 User us = usermap.get(call.Ownerid);
                 Contact foundContact;
                 for(Contact contact : userNameContacts)
                 {
                      if(contact.RACFID__c == us.RACF_Number__c && contact.RACFID__c != null && us.RACF_Number__c != null)
                      {
                            foundContact = contact;
                            break;
                      }
                 }
                 if(foundContact != null )
                 {
                      Participant__c  part = new Participant__c();
                      part.CallId__c = call.Id;
                      part.Contact_Id__c = foundContact.Id;
                      addparticipant.add(part);
                 }
                 else
                 {
                      try{}
                      catch(Exception e )
                      {
                          ApexPages.addMessages(e);
                      }
                      //  contact not found
                 }
            }
        }
    
        if(addparticipant .size()>0)
        {
            database.insert(addparticipant);
        }
        
        
    }
    
    @future public static void OnAfterInsertAsync(Set<ID> newCallIDs)
    {

    }
    
    public static void updateCallCount(List<Call__c> calllist)
    {
        integer count;
        List<Account> acclist = new List<Account>();
        Set<Id> accid = new Set<Id>();
        for(Call__c c : calllist)
        {
            if(c.Associated_Account__c !=null)
            accid.add(c.Associated_Account__c );
       
        }
       if(accid.size()>0){
        count = [select count() from Call__c where Associated_Account__c in : accid];
        Account a = [Select Id, Number_of_Calls__c from Account where id in : accid];
        
        if(count > 0)
            a.Number_of_Calls__c= count-1;    
        update a;
        }
    }
    
    
     
    @future public static void updateCallCountOnUpdate(Set<ID> OldCallList, Set<ID> NewCallList)
    {
        if(!Test.isRunningTest())
        {
        system.debug('**Old'+ OldCallList.size());
        system.debug('**New'+ NewCallList.size());
        integer count1, count2;
        Account acc1, acc2;
        /*
        Set<Id> accidold = new Set<ID>();
        Set<Id> accidnew = new Set<ID>();
        for(Call__c c1 : OldCallList)
        {
            accidold.add(c1.Associated_Account__c );
            system.debug('***oldaccount***'+ c1.Associated_Account__c );
        }
        for(Call__c c2 : NewCallList)
        {
            accidnew.add(c2.Associated_Account__c );
            system.debug('***newaccount***'+ c2.Associated_Account__c );
        } */
        
             
        if(OldCallList.size()>1)
        {
            count1 = [select count() from Call__c where Associated_Account__c in : OldCallList];
            system.debug('count1' + count1);
            acc1 = [Select Id, Number_of_Calls__c from Account where id in : OldCallList];
            acc1.Number_of_Calls__c= count1; 
            update acc1;
        }
        
        if(NewCallList.size()>0)
        {
            count2 = [select count() from Call__c where Associated_Account__c in : NewCallList];
            system.debug('count2' + count2);       
            acc2 = [Select Id, Number_of_Calls__c from Account where id in : NewCallList];
            acc2.Number_of_Calls__c= count2;  
            update acc2; 
        
        }
        
        }
        }
    
    
}