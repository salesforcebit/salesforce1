/*******************************************************************************************************************************************
* @name         :   ECCP_MCA_WSIB_Response
* @description  :   This class holds the Response variables from nCino to MCA
* @author       :   Nagarjun
* @createddate  :   18/03/2016
*******************************************************************************************************************************************/
global class ECCP_MCA_WSIB_Response{  
    public  ECCP_MCA_WSIB_Response(){
    }
    webservice String custId;
    webservice String custSsn;
    webservice string sfdcResponse;
}