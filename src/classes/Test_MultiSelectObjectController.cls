@isTest
Public class Test_MultiSelectObjectController
{
    public static testMethod void testMultiSelectProduct()
    {
        Profile p = [select id from profile where name=:System.label.KEF_HUB_Owner];
        
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharan1@keybank.com',
           emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p.Id, country='United States',
           timezonesidkey='America/Los_Angeles', username='Test_User@Domain.com',PLOB__c='support',PLOB_Alias__c='support');             
        insert testUser;
        
        System.RunAs(testUser)
        {
            Agreement__c agr = new Agreement__c();
            agr = TestUtilityClass.CreateTestAgreement();
            agr.Agreement_Name_UDF__c = 'Test UDF Value';
            insert agr;
            
            List<Product2> prodList = TestUtilityClass.createProductsList();
            List<KEF_Product__c> prodAssoList = new List<KEF_Product__c>();
            KEF_Product__c  prodAsso; 
             
            integer i;
            if(prodList!=null){
                for(i=0; i<prodList.size(); i++){
                    prodAsso = TestUtilityClass.createProductAssociations(agr.id,prodList[i].id);
                    prodAssoList.add(prodAsso);
                }
            }
            
            Test.StartTest();
            
            PageReference pageRef = Page.AddMultipleObjectFeature;
            pageRef.getParameters().put('Id', agr.id);
            Test.setCurrentPageReference(pageRef);
            
            ApexPages.StandardController sc = new ApexPages.standardController(agr);
            MultiSelectObjectController ctrl = new  MultiSelectObjectController(sc);

            ctrl.add(); 
            ctrl.listcount = '5';  
            ctrl.add(); 
    
            ctrl.allFileList = prodAssoList; 
            ctrl.save2(); 
            
            ctrl.allFileList = prodAssoList.clone();  
            ctrl.save2(); 
            ctrl.cancel();
            
            system.assertEquals(true, [select id from KEF_Product__c where Agreement__c =: agr.id].size() > 0 );
            
            Test.StopTest();
        }
    }
}