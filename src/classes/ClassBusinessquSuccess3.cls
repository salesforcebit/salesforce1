//Generated by wsdl2apex

public class ClassBusinessquSuccess3 {
    public class getBusinessResponse_element {
        public ClassBusinessquSuccess3.Businesswrapper[] result;
        private String[] result_type_info = new String[]{'result','http://soap.sforce.com/schemas/class/BusinessQuery',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }
    public class LogInfo {
        public String category;
        public String level;
        private String[] category_type_info = new String[]{'category','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] level_type_info = new String[]{'level','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'category','level'};
    }
    public class DebuggingInfo_element {
        public String debugLog;
        private String[] debugLog_type_info = new String[]{'debugLog','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'debugLog'};
    }
    public class address {
        public String city;
        public String country;
        public String countryCode;
        public String geocodeAccuracy;
        public String postalCode;
        public String state;
        public String stateCode;
        public String street;
        private String[] city_type_info = new String[]{'city','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] country_type_info = new String[]{'country','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] countryCode_type_info = new String[]{'countryCode','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] geocodeAccuracy_type_info = new String[]{'geocodeAccuracy','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] postalCode_type_info = new String[]{'postalCode','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] state_type_info = new String[]{'state','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] stateCode_type_info = new String[]{'stateCode','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] street_type_info = new String[]{'street','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'city','country','countryCode','geocodeAccuracy','postalCode','state','stateCode','street'};
    }
    public class SessionHeader_element {
        public String sessionId;
        private String[] sessionId_type_info = new String[]{'sessionId','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'sessionId'};
    }
    public class Businesswrapper {
        public Decimal accountnumber{get;set;}
        public Decimal amount{get;set;}
        public String Id{get;set;}
        public String name{get;set;}
        private String[] accountnumber_type_info = new String[]{'accountnumber','http://soap.sforce.com/schemas/class/BusinessQuery',null,'0','1','true'};
        private String[] amount_type_info = new String[]{'amount','http://soap.sforce.com/schemas/class/BusinessQuery',null,'0','1','true'};
        private String[] Id_type_info = new String[]{'Id','http://soap.sforce.com/schemas/class/BusinessQuery',null,'0','1','true'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/schemas/class/BusinessQuery',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'accountnumber','amount','Id','name'};
    }
    public class getBusiness_element {
        public String passName;
        private String[] passName_type_info = new String[]{'passName','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'passName'};
    }
    public class CallOptions_element {
        public String client;
        private String[] client_type_info = new String[]{'client','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'client'};
    }
    public class DebuggingHeader_element {
        public ClassBusinessquSuccess3.LogInfo[] categories;
        public String debugLevel;
        private String[] categories_type_info = new String[]{'categories','http://soap.sforce.com/schemas/class/BusinessQuery',null,'0','-1','false'};
        private String[] debugLevel_type_info = new String[]{'debugLevel','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'categories','debugLevel'};
    }
    public class location {
        public Double latitude;
        public Double longitude;
        private String[] latitude_type_info = new String[]{'latitude','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] longitude_type_info = new String[]{'longitude','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'latitude','longitude'};
    }
    public class AllowFieldTruncationHeader_element {
        public Boolean allowFieldTruncation;
        private String[] allowFieldTruncation_type_info = new String[]{'allowFieldTruncation','http://soap.sforce.com/schemas/class/BusinessQuery',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery','true','false'};
        private String[] field_order_type_info = new String[]{'allowFieldTruncation'};
    }
    public class BusinessQuery {
        public String endpoint_x = 'https://ap2.salesforce.com/services/Soap/class/BusinessQuery';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public ClassBusinessquSuccess3.AllowFieldTruncationHeader_element AllowFieldTruncationHeader;
        public ClassBusinessquSuccess3.CallOptions_element CallOptions;
        public ClassBusinessquSuccess3.DebuggingHeader_element DebuggingHeader;
        public ClassBusinessquSuccess3.SessionHeader_element SessionHeader;
        public ClassBusinessquSuccess3.DebuggingInfo_element DebuggingInfo;
        private String AllowFieldTruncationHeader_hns = 'AllowFieldTruncationHeader=http://soap.sforce.com/schemas/class/BusinessQuery';
        private String CallOptions_hns = 'CallOptions=http://soap.sforce.com/schemas/class/BusinessQuery';
        private String DebuggingHeader_hns = 'DebuggingHeader=http://soap.sforce.com/schemas/class/BusinessQuery';
        private String SessionHeader_hns = 'SessionHeader=http://soap.sforce.com/schemas/class/BusinessQuery';
        private String DebuggingInfo_hns = 'DebuggingInfo=http://soap.sforce.com/schemas/class/BusinessQuery';
        private String[] ns_map_type_info = new String[]{'http://soap.sforce.com/schemas/class/BusinessQuery', 'ClassBusinessquSuccess3'};
        public ClassBusinessquSuccess3.Businesswrapper[] getBusiness(String passName) {
            ClassBusinessquSuccess3.getBusiness_element request_x = new ClassBusinessquSuccess3.getBusiness_element();
            request_x.passName = passName;
            ClassBusinessquSuccess3.getBusinessResponse_element response_x;
            Map<String, ClassBusinessquSuccess3.getBusinessResponse_element> response_map_x = new Map<String, ClassBusinessquSuccess3.getBusinessResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://soap.sforce.com/schemas/class/BusinessQuery',
              'getBusiness',
              'http://soap.sforce.com/schemas/class/BusinessQuery',
              'getBusinessResponse',
              'ClassBusinessquSuccess3.getBusinessResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
    }
}