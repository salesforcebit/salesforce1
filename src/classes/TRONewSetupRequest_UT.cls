@isTest
private class TRONewSetupRequest_UT {

  static testMethod void addUserRequestTest() {
  	TRONewSetupRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_newSetupRequest_Port testNewSetup = new TRONewSetupRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_newSetupRequest_Port();

  	TRONewSetupRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_newSetup_ExportFields();

  	Test.setMock(WebServiceMock.class, new TRO_NewSetupRequest_Mock());

		Test.startTest();
  		TRONewSetupRequest_v2.KTTResponse response = testNewSetup.processNewSetUpAPI(exportFields);
		Test.stopTest();

		System.assertEquals(response.CSOAutomation.KTTResponse.status, 'Good to go');

  }
		static testMethod void aSyncNewSetupRequestTest() {
        AsyncTRONewSetupRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_newSetupRequest_Port aSyncAddAccount = new AsyncTRONewSetupRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_newSetupRequest_Port();

        TRONewSetupRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_newSetup_ExportFields();
        Continuation con = new Continuation(120);
        con.continuationMethod = 'processResponse';

        Test.setMock(WebServiceMock.class, new TRO_AsyncNewSetupRequest_Mock());

        AsyncTRONewSetupRequest_v2.processNewSetUpAPIResponseFuture response = aSyncAddAccount.beginProcessNewSetUpAPI(con, exportFields);

    }

}