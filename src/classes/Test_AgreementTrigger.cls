@isTest
private class Test_AgreementTrigger 
{
    static testMethod void AgreementTrigger() 
    {
    	Agreement__c agg=TestUtilityClass.CreateTestAgreement();
    	agg.Internal_Notification_Group__c = System.Label.All_statuses;
    	agg.Internal_Notification_Email_Address__c = 'test@email.com;my@email.com';
    	insert agg;
    	
    	test.startTest(); 
    	
    	agg.Agreement_Legal_Status__c= system.label.Contractual;
    	agg.Internal_Notification_Email_Address__c = 'test@email.com';
    	update agg;
    
    	system.assertEquals(true, [select Agreement_End_Date__c from Agreement__c where Id = :agg.id].Agreement_End_Date__c != null);
    
    	agg.Agreement_Renewal_Date__c = system.today().addDays(10);
    	agg.Agreement_Status__c = system.label.Agreement_CreditDeclined;
    	agg.Reason_for_Status_Change__c = system.label.Agreement_Adverse_Risk;
    	update agg;
    
    	System.assertEquals([select DateTime_CreditDeclined__c from Agreement__c where Id = :agg.id].DateTime_CreditDeclined__c.Date(),system.today());
    	
    	test.stopTest();    
    }
    
    static testMethod void ValidateEmailAddressTest() 
    {
    	Agreement__c agg=TestUtilityClass.CreateTestAgreement();
    	agg.Internal_Notification_Group__c = System.Label.All_statuses;
    	agg.Internal_Notification_Email_Address__c = 'test';
    	
    	try
    	{
    		insert agg;
    	}
    	catch(DMLException dmlEx)
        {
        	system.debug('dmlEx.getDMLMessage>>>>      '+dmlEx);
        	System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION,dmlEx.getDmlType(0));
        }
        
        Test.StartTest();
        
        agg.Internal_Notification_Email_Address__c = 'test@email.com;my@email.com';
        insert agg;
        
        System.assertEquals(agg.Id != null, true);
        
        
        /*Inserting an agreement with valid Email ids --> Update Test */
        Agreement__c agrUpdate = TestUtilityClass.CreateTestAgreement();
        agrUpdate.Agreement_Name_UDF__c = 'THE UDF VALE';
    	agrUpdate.Internal_Notification_Group__c = System.Label.All_statuses;
    	agrUpdate.Internal_Notification_Email_Address__c = 'test@email.com;my_email@email.com';
    	insert agrUpdate;
    	
    	try
    	{
    		//updating the same agreement with invalid email id
    		agrUpdate.Internal_Notification_Email_Address__c = 'test';
    		update agrUpdate;
    	}
    	catch(DMLException dmlEx)
        {
        	system.debug('dmlEx.getDMLMessage>>>>      '+dmlEx);
        	System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION,dmlEx.getDmlType(0));
        }
        
        Test.Stoptest();
    	    
    }
    
    static testMethod void AgreementDeleteTest() 
    {
    	Agreement__c agg = TestUtilityClass.CreateTestAgreement();
    	agg.Integrated__c = true;
    	agg.Agreement_Legal_Status__c= system.label.Contractual;
    	agg.Agreement_Renewal_Date__c = system.today().addDays(10); 
        agg.Internal_Notification_Group__c = System.Label.All_statuses;
        agg.Internal_Notification_Email_Address__c = 'testtest@email.com;mymy@email.com';
    	insert agg;
    	
        Test.StartTest();
    	try
        {
        	delete agg;
        }
        catch(DMLException dmlEx)
        {
        	system.debug('dmlEx.getDMLMessage>>>>      '+dmlEx);
        	System.assertEquals(StatusCode.DELETE_FAILED,dmlEx.getDmlType(0));
        }   
        Test.Stoptest();
    }
}