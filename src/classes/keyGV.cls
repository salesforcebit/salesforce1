//=================================================================================================
//  KeyBank
//  Object: KeyBank Global Variables
//  Author: Barney Young Jr
//  Detail: Global Variables to be used in Customization
//=================================================================================================

public with sharing class keyGV {
	
	//VisualForce Pages
	public static final string vfCallNew = 'CallCustomNew';
	
	//RecordType Names
	public static final String callRTAccount = 'Account Call';
	public static final String callRTLead = 'Lead Call';
	public static final String callRTOpportunity = 'Opportunity Call';
	public static final String callRTContact = 'Contact Call';
	
	
}