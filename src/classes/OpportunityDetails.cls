public class OpportunityDetails
{

public OpportunityDetails(ApexPages.StandardController controller) 
{
    Opportunity opty = new Opportunity();
    this.opty = (Opportunity)controller.getRecord();
    
}
public OpportunityDetails() 
{
    listofopportunities = new List<Opportunity>(); 
    Id userid = Userinfo.getUserId();      
    listofopportunities = fetch(userid ); 
    listofopportunities1 = new List<Opportunity>();
    listofopportunities1 = fetch1(userid);       
}

public OpportunityDetails(logindetailscontroller controller) 
{

}    

Public List<Opportunity> listofopportunities {get;set;}
Public List<Opportunity> listofopportunities1 {get;set;}
Public Opportunity opty{get;set;}

public PageReference ondetails() 
{
return Page.OpportunityDetailPage;
}

public List<Opportunity> fetch(Id i)
{
    return [select id, name, closedate, lastmodifieddate, stagename from Opportunity where lastmodifieddate !=  LAST_N_DAYS:60 AND (NOT stagename like 'Closed%') and ownerid =: i];
}

public List<Opportunity> fetch1(Id i)
{
    return [SELECT id, name, closedate, lastmodifieddate, stagename FROM Opportunity WHERE OwnerId =: i AND closedate < today AND (NOT stagename like 'Closed%')];
                             
}
public PageReference back()
{

    PageReference pageRef;
    pageRef = new PageReference('/home/home.jsp');
    pageRef.setRedirect(true);
    return pageRef;  
}

public PageReference detail()
{
    String i= ApexPages.currentPage().getParameters().get('oppid');   
    pagereference detail = new pagereference('/'+i);
    return detail;
}
}