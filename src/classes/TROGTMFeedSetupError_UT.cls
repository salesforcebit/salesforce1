@isTest
public with sharing class TROGTMFeedSetupError_UT {
	static testMethod void TROGTMFeedSetupError_UT_test1() {
		LLC_BI__Deposit__c da = new LLC_BI__Deposit__c();
    da.Name = 'TestDA2';
    da.Bank_Number__c = '1234';
    da.LLC_BI__Account_Number__c = '999999';
    insert da;
        TSO_Cost_Center__c tcc = new TSO_Cost_Center__c();
        tcc.Charge_Class__c = 'CN';
        insert tcc;
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        acc.Cost_Center__c = tcc.Id;
    acc.Billing_Account_Number__c = da.Id;
		acc.Bank_Number__c = da.Id;
		acc.Delivery_Method__c = 'I';
		insert acc;
		Opportunity oppty = new Opportunity();
		oppty.Name = 'TestOppty';
		oppty.AccountId = acc.Id;
		oppty.CloseDate = System.Today();
		oppty.StageName = 'Pursue';
		oppty.Opportunity_Type__c = 'Addl Product';
		insert oppty;
		Product2 SFprod = UnitTestFactory.buildTestProduct();
		SFprod.Name = 'Scenario Test';
		SFprod.Family = 'Deposits & ECP';
		insert SFprod;
		LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
		insert pl;
		LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
		insert pt;
		LLC_BI__Product__c prod = new LLC_BI__Product__c();
		prod.Name = 'TestProd';
		prod.Related_SFDC_Product__c = SFprod.Id;
		prod.LLC_BI__Product_Type__c = pt.Id;
		insert prod;
		LLC_BI__Opportunity_Product__c op = new LLC_BI__Opportunity_Product__c();
		op.LLC_BI__Opportunity__c = oppty.Id;
		op.LLC_BI__Product__c = prod.Id;
		insert op;
		LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c();
		ts.Name = 'TestTS';
		ts.Request_Type__c = 'New';
		ts.LLC_BI__Stage__c = 'Fulfillment';
		ts.LLC_BI__Product_Reference__c = prod.Id;
		ts.LLC_BI__Relationship__c = acc.Id;
		ts.GTM_Request_Type__c = 'NEW Setup';
		insert ts;
		LLC_BI__Bill_Point__c bp = new LLC_BI__Bill_Point__c();
		bp.Name = 'TestBillPoint';
		bp.LLC_BI__Product__c = prod.Id;
    bp.LLC_BI__lookupKey__c = 'DDAPOSP1PCN-0';
		insert bp;
		LLC_BI__Bill_Point__c bp2 = new LLC_BI__Bill_Point__c();
		bp2.Name = 'TestBillPoint';
		bp2.LLC_BI__Product__c = prod.Id;
    bp2.LLC_BI__lookupKey__c = 'DDAPOSP1PCN-0';
		insert bp2;
		TSO_Volumes_and_Prices__c vp = new TSO_Volumes_and_Prices__c();
		vp.Name = 'TestVP';
		vp.Product__c = prod.Id;
		vp.Treasury_Service__c = ts.Id;
		vp.Bill_Point__c = bp2.Id;
		insert vp;
		Contact con = new Contact();
		con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Contact').getRecordTypeId();
		con.LastName = 'Test';
		con.FirstName = 'Test';
		con.AccountId = acc.Id;
		con.Contact_Email__c = 'test@test.com';
		con.Phone = '1111111111';
		con.Phone_Extension__c = 'x123';
		con.Title = 'Blah';
		insert con;
		LLC_BI__Authorized_User__c AU = new LLC_BI__Authorized_User__c();
		AU.LLC_BI__Contact_Reference__c = con.Id;
		AU.LLC_BI__Treasury_Service__c = ts.Id;
		AU.User_has_unlimited_transfer_limit__c = true;
		insert AU;
        
        
        
		LLC_BI__Analyzed_Account__c AA = new LLC_BI__Analyzed_Account__c();
		AA.LLC_BI__Deposit_Reference__c = da.Id;
        AA.LLC_BI__Deposit_Account__c = da.Id;
		AA.Name = 'Test';
		AA.LLC_BI__Treasury_Service__c = ts.Id;
		AA.Select_Service_Option_Type__c = 'KTT Information Reporting';
		AA.Service_Options__c = 'Previous Day;Previous Day Via';
        AA.Previous_Day_Credit__c  = 100.00;
        AA.Account_Number__c = '12345';
        AA.Previous_Day_Via__c = 'KTT';
        AA.Previous_Day_Options__c = 'Summary';
		insert AA;
        
        Users_and_Accounts__c UserAccnt = new Users_and_Accounts__c();
        UserAccnt.Authorized_User__c = AU.id;
        UserAccnt.Analyzed_Account__c = AA.id;
        UserAccnt.Select_Service_Option_Type__c = 'KTT Information Reporting';
        insert UserAccnt;
        list<ServiceOption_Not_in_GTM__c> listServiceOption_Not_in_GTM = new list<ServiceOption_Not_in_GTM__c>();
        list<String> serviceOptions = new list<String>{'ESP Reports','Current Day OLDS Memo Post','Current Day Deposit Items','Current Day Lockbox','Current Day CDA','Current Day Wires','Previous Day','Previous Day Via','Customer Statements','Current Day ACH','Current Day ACH Via'};
        
           for(string optName : serviceOptions)
           {
              ServiceOption_Not_in_GTM__c testServiceOptions = new ServiceOption_Not_in_GTM__c();
                testServiceOptions.name = optName;
                listServiceOption_Not_in_GTM.add(testServiceOptions);
           }       
           insert listServiceOption_Not_in_GTM;
        
		ApexPages.StandardController sc = new ApexPages.StandardController(ts);
		
		//Test.setMock(WebServiceMock.class,new TRO_NewSetupRequest_Mock());
		Test.startTest();
        TROGTMFeedSetupError controller = new TROGTMFeedSetupError(sc);
		//Test.setMock(WebServiceMock.class,new TRO_AsyncNewSetupRequest_Mock());
		//controller.initiateGTMRequest();
       // controller.redirectUrl = '';
        controller.errMsg = '';
		Test.stopTest();
	}
    
    static testMethod void TROGTMFeedSetupError_UT_test2() {
		LLC_BI__Deposit__c da = new LLC_BI__Deposit__c();
    da.Name = 'TestDA2';
    da.Bank_Number__c = '1234';
    da.LLC_BI__Account_Number__c = '999999';
    insert da;
        TSO_Cost_Center__c tcc = new TSO_Cost_Center__c();
        tcc.Charge_Class__c = 'CN';
        insert tcc;
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        acc.Cost_Center__c = tcc.Id;
    acc.Billing_Account_Number__c = da.Id;
		acc.Bank_Number__c = da.Id;
		acc.Delivery_Method__c = 'I';
		insert acc;
		Opportunity oppty = new Opportunity();
		oppty.Name = 'TestOppty';
		oppty.AccountId = acc.Id;
		oppty.CloseDate = System.Today();
		oppty.StageName = 'Pursue';
		oppty.Opportunity_Type__c = 'Addl Product';
		insert oppty;
		Product2 SFprod = UnitTestFactory.buildTestProduct();
		SFprod.Name = 'Scenario Test';
		SFprod.Family = 'Deposits & ECP';
		insert SFprod;
		LLC_BI__Product_Line__c pl =  UnitTestFactory.buildTestProductLine();
		insert pl;
		LLC_BI__Product_Type__c pt = UnitTestFactory.buildTestProductType(pl);
		insert pt;
		LLC_BI__Product__c prod = new LLC_BI__Product__c();
		prod.Name = 'TestProd';
		prod.Related_SFDC_Product__c = SFprod.Id;
		prod.LLC_BI__Product_Type__c = pt.Id;
		insert prod;
		LLC_BI__Opportunity_Product__c op = new LLC_BI__Opportunity_Product__c();
		op.LLC_BI__Opportunity__c = oppty.Id;
		op.LLC_BI__Product__c = prod.Id;
		insert op;
		LLC_BI__Treasury_Service__c ts = new LLC_BI__Treasury_Service__c();
		ts.Name = 'TestTS';
		ts.Request_Type__c = 'New';
		ts.LLC_BI__Stage__c = 'Order Entry';
		ts.LLC_BI__Product_Reference__c = prod.Id;
		ts.LLC_BI__Relationship__c = acc.Id;
		ts.GTM_Request_Type__c = 'NEW Setup';
		insert ts;
		LLC_BI__Bill_Point__c bp = new LLC_BI__Bill_Point__c();
		bp.Name = 'TestBillPoint';
		bp.LLC_BI__Product__c = prod.Id;
    bp.LLC_BI__lookupKey__c = 'DDAPOSP1PCN-0';
		insert bp;
		LLC_BI__Bill_Point__c bp2 = new LLC_BI__Bill_Point__c();
		bp2.Name = 'TestBillPoint';
		bp2.LLC_BI__Product__c = prod.Id;
    bp2.LLC_BI__lookupKey__c = 'DDAPOSP1PCN-0';
		insert bp2;
		TSO_Volumes_and_Prices__c vp = new TSO_Volumes_and_Prices__c();
		vp.Name = 'TestVP';
		vp.Product__c = prod.Id;
		vp.Treasury_Service__c = ts.Id;
		vp.Bill_Point__c = bp2.Id;
		insert vp;
		Contact con = new Contact();
		con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Contact').getRecordTypeId();
		con.LastName = 'Test';
		con.FirstName = 'Test';
		con.AccountId = acc.Id;
		con.Contact_Email__c = 'test@test.com';
		con.Phone = '1111111111';
		con.Phone_Extension__c = 'x123';
		con.Title = 'Blah';
		insert con;
		LLC_BI__Authorized_User__c AU = new LLC_BI__Authorized_User__c();
		AU.LLC_BI__Contact_Reference__c = con.Id;
		AU.LLC_BI__Treasury_Service__c = ts.Id;
		AU.User_has_unlimited_transfer_limit__c = true;
		insert AU;
        
        
        
		LLC_BI__Analyzed_Account__c AA = new LLC_BI__Analyzed_Account__c();
		AA.LLC_BI__Deposit_Reference__c = da.Id;
        AA.LLC_BI__Deposit_Account__c = da.Id;
		AA.Name = 'Test';
		AA.LLC_BI__Treasury_Service__c = ts.Id;
		AA.Select_Service_Option_Type__c = 'KTT Information Reporting';
		AA.Service_Options__c = 'Previous Day;Previous Day Via';
        AA.Previous_Day_Credit__c  = 100.00;
        AA.Account_Number__c = '12345';
        AA.Previous_Day_Via__c = 'KTT';
        AA.Previous_Day_Options__c = 'Summary';
		insert AA;
        
        Users_and_Accounts__c UserAccnt = new Users_and_Accounts__c();
        UserAccnt.Authorized_User__c = AU.id;
        UserAccnt.Analyzed_Account__c = AA.id;
        UserAccnt.Select_Service_Option_Type__c = 'KTT Information Reporting';
        insert UserAccnt;
        list<ServiceOption_Not_in_GTM__c> listServiceOption_Not_in_GTM = new list<ServiceOption_Not_in_GTM__c>();
        list<String> serviceOptions = new list<String>{'ESP Reports','Current Day OLDS Memo Post','Current Day Deposit Items','Current Day Lockbox','Current Day CDA','Current Day Wires','Previous Day','Previous Day Via','Customer Statements','Current Day ACH','Current Day ACH Via'};
        
           for(string optName : serviceOptions)
           {
              ServiceOption_Not_in_GTM__c testServiceOptions = new ServiceOption_Not_in_GTM__c();
                testServiceOptions.name = optName;
                listServiceOption_Not_in_GTM.add(testServiceOptions);
           }       
           insert listServiceOption_Not_in_GTM;
        
		ApexPages.StandardController sc = new ApexPages.StandardController(ts);
		
		//Test.setMock(WebServiceMock.class,new TRO_NewSetupRequest_Mock());
		Test.startTest();
        TROGTMFeedSetupError controller = new TROGTMFeedSetupError(sc);
		//Test.setMock(WebServiceMock.class,new TRO_AsyncNewSetupRequest_Mock());
		//controller.initiateGTMRequest();
       // controller.redirectUrl = '';
        controller.errMsg = '';
		Test.stopTest();
	}
}