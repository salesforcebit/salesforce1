@isTest
private class TaskTrigger_UT {
    /*
    @testSetup
    static void setup(){

        LLC_BI__Treasury_Service__c ts = UnitTestFactory.buildTestTreasuryService();
        ts.Name = 'Test TS';
        insert ts;


    }
    */
    static testMethod void TaskTest() {
            
            //TSO_CAP_Data__c testTSO = new TSO_CAP_Data__c();
            //insert testTSO;
            Profile p = [SELECT Id, Name FROM Profile WHERE Name='Integration/Data Migration'];
      User u = new User(Alias = 'standt', Email='standarduserkeybank@testorg.com',
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = p.Id, PLOB__c = 'Support',
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduserkeybank@testorg.com', Department = 'CB KBCM Derivatives');
        System.debug(p.Name);
        System.debug(u.ProfileId);
System.runAs(u) {


            Account testAccount = UnitTestFactory.buildTestAccount();
            //testAccount.Related_CAP__c = testTSO.Id;
            insert testAccount;
        
            Product2 testProduct2 = UnitTestFactory.buildTestProduct();
            testProduct2.Family = 'Deposits & ECP';
            insert testProduct2;
        
            LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
            testProductLine.Name = 'testProductLine';
            insert testProductLine;
            
            LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
            testProductLine.Name = 'testProductType';
            insert testProductType;
        
            LLC_BI__Product__c prod = new LLC_BI__Product__c();
            prod.Name = 'Test';
            prod.LLC_BI__Product_Type__c = testProductType.Id;
            prod.Related_SFDC_Product__c = testProduct2.Id;
            insert prod;
        
            LLC_BI__Product_Package__c ppTemp = new LLC_BI__Product_Package__c();
            ppTemp.Name = 'Test';
            ppTemp.LLC_BI__Stage__c = 'Order Entry';
            ppTemp.LLC_BI__Status__c = 'New';
            ppTemp.Package_Complexity__c = 'Low';
            insert ppTemp;
            /*
            LLC_BI__Product_Line__c testProductLine = UnitTestFactory.buildTestProductLine();
            testProductLine.Name = 'testProductLine';
            insert testProductLine;
            
            LLC_BI__Product_Type__c testProductType = UnitTestFactory.buildTestProductType(testProductLine);
            testProductLine.Name = 'testProductType';
            insert testProductType;
            
            LLC_BI__Product__c testProduct = UnitTestFactory.buildTestNCinoProduct(testProduct2, testProductType);
            testProduct.Name = 'testProduct';
            System.debug('>>>>>' + testProduct2.id);
            testProduct.Related_SFDC_Product__c = testProduct2.Id;
            insert testProduct;
             */
            LLC_BI__Treasury_Service__c ts = UnitTestFactory.buildTestTreasuryService();
            ts.Name = 'Test TS';
            ts.LLC_BI__Product_Reference__c = prod.Id;
            ts.LLC_BI__Relationship__c = testAccount.Id;
            ts.Documents_Complete__c = true;
            ts.LLC_BI__Product_Package__c = ppTemp.Id;
            LLC_BI__Treasury_Service__c ts2 = UnitTestFactory.buildTestTreasuryService();
            ts2.Name = 'Test TS2';
            ts2.LLC_BI__Product_Reference__c = prod.Id;
            ts2.LLC_BI__Relationship__c = testAccount.Id;
            ts2.Documents_Complete__c = true;
            ts2.LLC_BI__Product_Package__c = ppTemp.Id;

        //Test.startTest();
            insert new List<LLC_BI__Treasury_Service__c>{ts, ts2};
        //Test.stopTest();
            System.debug(ts.CreatedById);
            Task t = UnitTestFactory.buildTestTask();
            t.LLC_BI__Maintenance_Type__c = 'New';
            t.WhatId = ts.id;
            Task t1 = UnitTestFactory.buildTestTask();
            t1.WhatId = ts2.id;

            Test.startTest();
                insert new List<Task>{t, t1};
            Test.stopTest();

            LLC_BI__Treasury_Service__c ts1after = [select id, name, LLC_BI__Stage__c, Type__c from LLC_BI__Treasury_Service__c where id = :ts.id];
            LLC_BI__Treasury_Service__c ts2after = [select id, name, LLC_BI__Stage__c, Type__c from LLC_BI__Treasury_Service__c where id = :ts2.id];

            
            System.assertEquals(ts1after.LLC_BI__Stage__c, 'Order Entry');
            System.assertEquals(ts1after.Type__c, 'Revise - Add/Change/Delete');
            System.assertNotEquals(ts2after.LLC_BI__Stage__c, 'Fulfillment');
            System.assertNotEquals(ts2after.Type__c, 'Revise - Add/Change/Delete');
}
    }
}