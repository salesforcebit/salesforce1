@isTest
global class ECCP_LPL_WebServiceMockImpl implements WebServiceMock {
global void doInvoke(
       Object stub,
       Object request,
       Map<string,object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
       
       ECCP_LPL_Service.CreateLoanResponseType resp = new ECCP_LPL_Service.CreateLoanResponseType();
       resp.Status = 'Pass';
       resp.ErrorCode = 'E0001';
       resp.ErrorDesc = 'The LPL Service is successfully executed';
       resp.LPLpkgID = 'LPL000008';
       
       response.put('response_x', resp);
       
       }
  }