//================================================================================
//  KeyBank  
//  Object: Opportunity & OpportunityLineItem 
//  Author: offshore
//  Detail: PicklistUpdate
//  Description: Opportunity Picklist Code update based on the Asset Code, Asset Description and KEF Owning Group
//               Opportunity line Item Picklist Code update based on the Payment Code and Payment Frequency
//                
//================================================================================
//          Date            Purpose
// Changes: 03/12/2015      Initial Version
//================================================================================

Public class PicklistUpdate{
    
    // Opportunity Picklist Code update based on the Asset Code, Asset Description and KEF Owning Group
    public static void oppupdate(List<opportunity> opplist,List<string> ACatId,List<String> ASCatId, List<String> CDLId)
    {
        //Create a map reference which will store custom setting values
        Map<String,String> ACATMap = new Map<String, String>();
        Map<String,String> ASCATMap = new Map<String, String>();
        Map<String,String> CDLMap = new Map<String, String>();    
    
        //fetch the code and description from the custom setting
        system.debug('Inside Handler...'+ACatId+'...'+ASCatId+'...'+CDLId);
        List<Picklist_Transformation__c> acatList=[Select id,Code__c,DefaultDescription__c,Picklist_Field__c from Picklist_Transformation__c where Picklist_Field__c ='Asset Description' and DefaultDescription__c IN: ACatId ];
        List<Picklist_Transformation__c> ascatList=[Select id,Code__c,DefaultDescription__c,Picklist_Field__c from Picklist_Transformation__c where Picklist_Field__c ='Asset Code' and DefaultDescription__c IN: ASCatId ]; 
        List<Picklist_Transformation__c> cdlList=[Select id,Code__c,DefaultDescription__c,Picklist_Field__c from Picklist_Transformation__c where Picklist_Field__c ='KEF Owning Group' and DefaultDescription__c IN: CDLId ];               
        
        for(Picklist_Transformation__c acat :acatList)
        {
            ACATMap.put(acat.DefaultDescription__c,acat.Code__c);
            system.debug('Category Map..'+ACATMap);
        }
        for(Picklist_Transformation__c ascat :ascatList)
        {
            ASCATMap.put(ascat.DefaultDescription__c,ascat.Code__c);
            system.debug('Sub Category Map..'+ASCATMap);
        }  
        for(Picklist_Transformation__c cdl :cdlList)
        {
            CDLMap.put(cdl.DefaultDescription__c,cdl.Code__c);
            system.debug('KEF Owning Group..'+CDLMap );
        }  
     
        for(Opportunity o :opplist)
        {
            if(ACATMap.containskey(o.Asset_Description__c) )
            o.AssetCategoryID__c = ACATMap.get(o.Asset_Description__c);
        
            if(ASCATMap.containskey(o.Asset_Code__c))
            o.AssetSubCategoryID__c = ASCATMap.get(o.Asset_Code__c);
            
            if(CDLMap.containskey(o.KEF_Owning_Group__c))
            o.CustomDataListItemID__c= CDLMap.get(o.KEF_Owning_Group__c);                     
        
        }
     }
     
     // Opportunity line Item Picklist Code update based on the Payment Code and Payment Frequency
     
     public static void OLUpdate(List<OpportunityLineItem> opplist,List<string> ATypId,List<String> PFqId)
    {
        //Create a map reference which will store custom setting values
        Map<String,String> ATYPMap = new Map<String, String>();
        Map<String,String> PFQMap = new Map<String, String>();    
    
        //fetch the code and description from the custom setting
        system.debug('Inside Handler...'+ATypId+'...'+PFqId);
        
        List<Picklist_Transformation__c> atypList=[Select id,Code__c,DefaultDescription__c,Picklist_Field__c from Picklist_Transformation__c where Picklist_Field__c ='Payment Type' and DefaultDescription__c IN: ATypId]; 
        List<Picklist_Transformation__c> pfqList=[Select id,Code__c,DefaultDescription__c,Picklist_Field__c from Picklist_Transformation__c where Picklist_Field__c ='PaymentFrequency' and DefaultDescription__c IN: PFqId];               
        
        for(Picklist_Transformation__c atyp :atypList)
        {
            ATYPMap.put(atyp.DefaultDescription__c,atyp.Code__c);
            system.debug('Payment Type Map..'+ATYPMap);
        }
        for(Picklist_Transformation__c pfq :pfqList)
        {
            PFQMap.put(pfq.DefaultDescription__c,pfq.Code__c);
            system.debug('Payment Frequency..'+PFQMap);
        }  
     
        for(OpportunityLineItem o :opplist)
        {
            if(ATYPMap.containskey(o.Payment_Type__c) )
            o.AdvanceTypeID__c = ATYPMap.get(o.Payment_Type__c);
        
            if(PFQMap.containskey(o.Payment_Frequency__c))
            o.PaymentFrequencyID__c= PFQMap.get(o.Payment_Frequency__c);                  
        
        }
     }
     
}