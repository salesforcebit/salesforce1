@isTest
public class TestAdvancedValidationTriggerHandler {
	
    @isTest
	public static void initialize() {
        Advanced_Validation_Rule__c testValidationRule = createRule();
        Account testAccount = createAccount();
		LLC_BI__Product_Package__c testProductPackage = createProductPackage(testAccount.Id);
		LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testProductPackage.Id);
        
        try {
            testProductPackage.LLC_BI__Status__c = 'Hold';
            Database.update(testProductPackage);
        } catch (Exception ex) {}
	}
    
    private static Account createAccount() {
        Account newA = new Account(Name = 'Test');
        
        Database.insert(newA);
        return newA;
    }

	private static LLC_BI__Product_Package__c createProductPackage(Id acctId) {
        LLC_BI__Product_Package__c newPP = new LLC_BI__Product_Package__c(
        	Name = 'Test',
        	LLC_BI__Account__c = acctId,
        	LLC_BI__Primary_Officer__c = UserInfo.getUserId(),
        	LLC_BI__Stage__c = 'Qualification / Proposal',
        	LLC_BI__Status__c = 'Open');
        
        Database.insert(newPP);
        return newPP;
    }

	private static LLC_BI__Loan__c createLoan(Id acctId, Id ppId) {
        LLC_BI__Loan__c newL = new LLC_BI__Loan__c(
        	Name = 'Test',
        	LLC_BI__Account__c = acctId,
        	LLC_BI__Product_Package__c = ppId,
        	LLC_BI__Stage__c = 'Qualification / Proposal',
        	LLC_BI__Status__c = 'Open');
        
        Database.insert(newL);
        return newL;
    }

	private static Advanced_Validation_Rule__c createRule() {
		Advanced_Validation_Rule__c validationRule = new Advanced_Validation_Rule__c(
            Exclude_From_Integration__c =  True,
			Field_Set__c = 'LLC_BI__Booked_Information',
			Object_To_Validate__c = 'LLC_BI__Loan__c',
			Rule__c = 'LLC_BI__Status__c=Hold;LLC_BI__Stage__c=Qualification / Proposal;',
			Filter_Criteria__c = 'LLC_BI__Stage__c != \'Booked\'',
			Parent_Object__c = 'LLC_BI__Product_Package__c',
        	isActive__c = True,
        	Evaluation_Type__c = 'AND');

		Database.insert(validationRule);
		return validationRule;
	}
}