/*
This class is written for KEF KRR Project by TCS Team

It is an apex controller for page which allows user to upload multiple attachments at once.
*/
public with sharing class MultiFileUploader 
{

  public string objectname { get; set; }
  public String listcount { get; set; }

  public List<Attachment> allFileList {get; set;}
  public List<SelectOption> Filelist{get; set;}

  public List<wrapperclass> wrapperlist{get;set;}
  public Boolean allBool {get;set;}
  public string recId {get;set;}
  public boolean showResults {get;set;}
 
  public MultiFileUploader ()
  {
       showResults = false;
       allBool = false;
       recId = System.currentPagereference().getParameters().get('id');
       objectname = System.currentPagereference().getParameters().get('name');
       fetchExistingAttachments();
       system.debug('recid---'+recId); 
     
        //intialize the dropdownlist for selection of no of files
      Filelist = new List<SelectOption>() ;
    listcount = '' ;
    //Attachment list intialization
    allFileList = new list<Attachment>();
     
    for(integer i=1;i<11;i++)
    {
        Filelist.add(new SelectOption(String.valueOf(i) , String.valueOf(i))) ; 
    }
    
    addBlankAttachments(5); 
  }
  
  public void addBlankAttachments(Integer attCount)
  {
    if(attCount != null)
    {
      for(integer i = 0; i < attCount; i ++)
      {
          allFileList.add(new Attachment());        
      }
    }
  }

  public void fetchExistingAttachments()
  {
      wrapperlist = new list<wrapperclass>();
      system.debug('list--'+wrapperlist);
      
      for(Attachment att : [select id,Name,description,Parentid,IsPrivate, lastModifiedDate, CreatedById
                  from Attachment 
                  where parentid=:recId
                  order by Name ])
      {
        if(att!=null)
        {
          showResults=true;
        }
        wrapperlist.add(new wrapperclass(att));
        system.debug('att value---'+wrapperlist);
      }
      
      if(wrapperList.size() == 0)
      {
        showResults = false;
      }
  }

  //upload multiple attachments
  public PageReference SaveAttachments() 
  {
      if(recId == null || recId == '')
      {  
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, system.label.No_record_is_associated));
      }  
       
        List<Attachment> listToInsert = new List<Attachment>() ;  
        
        for(Attachment att : allFileList)  
        {  
            if(att.name  != null && att.name != ''  && att.body != null)
            {
                att.ParentId = recId;
                Attachment attNew = att.clone();
                listToInsert.add(attNew);
            }               
        }  
        
        system.debug('listinsert---'+listToInsert);
          
        //Inserting attachments  
        if(listToInsert.size() > 0)  
        { 
          try
          { 
            system.debug('listsize----'+listToInsert.size() );
              insert listToInsert ; 
               listcount='';  
               ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, listToInsert.size() + ' ' + system.label.File_s_uploaded));
               
               Integer listSize = allFileList.size();                
               allFileList = new List<Attachment>();
               addBlankAttachments(listSize);
             }
             catch(Exception e)
             {
              system.debug('e-->    '+e);
              ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, ' ' + system.label.file_s_not_uploaded)) ;
           }
         
           fetchExistingAttachments();          
    }          
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, system.label.select_at_least_one_file));
    } 
    return null;
     }
    
   //click on add to add more file to select   
  public PageReference Add() 
  {
      Attachment objatt= new Attachment();
      //allFileList.add(objatt);
      system.debug('Listcount show---'+listcount );
      if(listcount == null || (listcount != null && listcount.trim() == ''))
      {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, system.label.select_number_of_files));
      }      
      else if(allFileList != null)
      {
          for(Integer i = 1 ; i <= Integer.valueOf(listcount) ; i++)
          {
            allFileList.add(new Attachment()) ;
          }
      }
      return null;    
    
  }
  
  //Delete the required attachments
   public PageReference DeleteAttachments()
   {    
      List<Attachment> listForDel = new List<Attachment>();
      
      //List<WrapperClass> listTempWrapper = new List<WrapperClass>();
      
      for(WrapperClass w : wrapperlist) 
      {
          if(w.checked != null && w.checked == true) 
          {
              listForDel.add(w.att);
          } 
      }
      
      if(listForDel.size() > 0) 
      {
          try
          {
          delete listForDel;
          ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, listForDel.size() + ' ' + system.label.File_s_deleted));          
          fetchExistingAttachments();
          allBool = false;
          }
          catch(Exception e)
          {
              system.debug('e-->    '+e);
              ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, ' ' + system.label.file_s_not_deleted)) ;
           }
      } 
      else if(listForDel.size()==0)
      {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,system.label.Attachment_to_delete));
      }
      return null;
  }

  public void selectAll()
  {
      if(allBool != null && allBool) 
      {
          for(WrapperClass w : wrapperlist) 
          {
              w.checked = true;
          }
      } 
      else 
      {
          for(WrapperClass w : wrapperlist) 
          {
              w.checked = false;
          }
    }
  }
  
  public class WrapperClass 
  {
      public Boolean checked {get;set;}
      //public Boolean IsPrivate{get;set;}
      public Attachment  att {get;set;}
      
      public WrapperClass(Attachment att) 
      {
          this.att= att;
      }
  }
  
  //return back to record detail page
  public PageReference Cancel() 
  {
      PageReference aggrementpage= new PageReference('/'+recId);  
       aggrementpage.setRedirect(true);
       return aggrementpage ;       
    }
}