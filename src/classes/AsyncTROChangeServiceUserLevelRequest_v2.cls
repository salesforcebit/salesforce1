//Generated by wsdl2apex

public class AsyncTROChangeServiceUserLevelRequest_v2 {
    public class processCSULAPIResponseFuture extends System.WebServiceCalloutFuture {
        public TROChangeServiceUserLevelRequest_v2.KTTResponse getValue() {
            if(!Test.isRunningTest()){ 
            	TROChangeServiceUserLevelRequest_v2.processCSULAPIResponse response = (TROChangeServiceUserLevelRequest_v2.processCSULAPIResponse)System.WebServiceCallout.endInvoke(this);
            	return response.processCSULResponse;
            }
            else {
                TROChangeServiceUserLevelRequest_v2.processCSULAPIResponse response = TROUnitTestFactory.buildTestChangeUserResponse();
            	return response.processCSULResponse;
            }
        }
    }
    public class AsyncTROnCinoKTTIntegration_WebServices_Provider_changeServiceUserLevelRequest_Port {
        public String endpoint_x =  Label.TROChangeServices;
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x =Label.TROCredential;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://sdc01dwbmis01.keybank.com/TROnCinoKTTIntegration.WebServices.Provider:changeServiceUserLevelRequest', 'TROChangeServiceUserLevelRequest_v2'};
        public AsyncTROChangeServiceUserLevelRequest_v2.processCSULAPIResponseFuture beginProcessCSULAPI(System.Continuation continuation,TROChangeServiceUserLevelRequest_v2.ExportFields processCSULRequest) {
            TROChangeServiceUserLevelRequest_v2.processCSULAPI request_x = new TROChangeServiceUserLevelRequest_v2.processCSULAPI();
            request_x.processCSULRequest = processCSULRequest;
                return (AsyncTROChangeServiceUserLevelRequest_v2.processCSULAPIResponseFuture) System.WebServiceCallout.beginInvoke(
                  this,
                  request_x,
                  AsyncTROChangeServiceUserLevelRequest_v2.processCSULAPIResponseFuture.class,
                  continuation,
                  new String[]{endpoint_x,
                  'TROnCinoKTTIntegration_WebServices_Provider_changeServiceUserLevelRequest_Binder_processCSULAPI',
                  'http://sdc01dwbmis01.keybank.com/TROnCinoKTTIntegration.WebServices.Provider:changeServiceUserLevelRequest',
                  'processCSULAPI',
                  'http://sdc01dwbmis01.keybank.com/TROnCinoKTTIntegration.WebServices.Provider:changeServiceUserLevelRequest',
                  'processCSULAPIResponse',
                  'TROChangeServiceUserLevelRequest_v2.processCSULAPIResponse'}
                );
        }
    }
}