public with sharing class ScenarioProductTriggerHandler {

    public void handleBeforeInsert(List<Scenario_Product__c> triggerNewList, Map<Id, Scenario_Product__c> triggerNewMap)
    {
        //setIncrementalValue(triggerNewList, triggerNewMap);
    }

    public void handleBeforeUpdate(List<Scenario_Product__c> triggerNewList, Map<Id, Scenario_Product__c> triggerNewMap)
    {
        //do we need this? marleypaige
        //setIncrementalValue(triggerNewList, triggerNewMap);

        setPriorMonthActualProductRevenue(triggerNewList, triggerNewMap);
    }

    public void setPriorMonthActualProductRevenue(List<Scenario_Product__c> triggerNewList, Map<Id, Scenario_Product__c> triggerNewMap){
      Map<Id,Id> AcctToSpMap = new Map<Id,Id>();
      Map<Id,Id> ProductToSpMap = new Map<Id,Id>();
      Map<Id, Scenario_Product__c> scenarioMap = new Map<Id, Scenario_Product__c>();

      for(Scenario_Product__c sp:[SELECT Id,
                                    Scenario__c,
                                    //Scenario__r.LLC_BI__Opportunity__c,
                                    Scenario__r.LLC_BI__Opportunity__r.AccountId,
                                    Product__c
                                  FROM Scenario_Product__c
                                  WHERE Id in :triggerNewMap.keyset()
                                   AND
                                    Scenario__r.LLC_BI__Opportunity__r.AccountId != ''
                                   AND product__c != ''
                                   ])
      {
        AcctToSpMap.put(sp.Scenario__r.LLC_BI__Opportunity__r.AccountId,sp.id);
        ProductToSpMap.put(sp.Product__c,sp.id);
        scenarioMap.put(sp.id, sp);

      }
      System.debug('AcctToSpMap::'+AcctToSpMap.keySet() );
      Map<String,Decimal> AcctProductToPriorMonthMap = new Map<String,Decimal>();
      if(scenarioMap.keyset().size() > 0){
          for(LLC_BI__Treasury_Service__c ts: [SELECT Id,
                                                LLC_BI__Relationship__c,
                                                LLC_BI__Product_Reference__c,
                                                Prior_Month_Product_Revenue__c
                                              FROM LLC_BI__Treasury_Service__c
                                              WHERE
                                                LLC_BI__Relationship__c in :AcctToSpMap.keyset()
                                                and LLC_BI__Product_Reference__c in :ProductToSpMap.keyset()
                                              ])
          {
            AcctProductToPriorMonthMap.put(ts.LLC_BI__Relationship__c+''+ts.LLC_BI__Product_Reference__c,ts.Prior_Month_Product_Revenue__c);
          }
          System.debug('AcctProductToPriorMonthMap::'+AcctProductToPriorMonthMap.values() );
          System.debug('AcctProductToPriorMonthMap::'+AcctProductToPriorMonthMap.keyset() );

          for(scenario_product__c sp: triggerNewList ){
            if(scenarioMap.containsKey(sp.id) ){
                System.debug('sp.Scenario__r.LLC_BI__Opportunity__r.AccountIdsp.Product__c::'+sp.Scenario__r.LLC_BI__Opportunity__r.AccountId+''+sp.Product__c);
                if( AcctProductToPriorMonthMap.containsKey(scenarioMap.get(sp.id).Scenario__r.LLC_BI__Opportunity__r.AccountId+''+scenarioMap.get(sp.id).Product__c)){
                  sp.Prior_Month_Actual_Product_Revenue__c = AcctProductToPriorMonthMap.get(scenarioMap.get(sp.id).Scenario__r.LLC_BI__Opportunity__r.AccountId+''+scenarioMap.get(sp.id).Product__c);
                }
                else sp.Prior_Month_Actual_Product_Revenue__c = 0;
                System.debug('sp.Prior_Month_Actual_Product_Revenue__c::'+sp.Prior_Month_Actual_Product_Revenue__c);
            }
          }
      }

    }

//do we need any of the code below? marleypaige
   /* @TestVisible
    private void setIncrementalValue(List<Scenario_Product__c> triggerNewList, Map<Id, Scenario_Product__c> triggerNewMap)
    {
        Set<Id> accIds = new Set<Id>();
        Set<Id> prodIds = new Set<Id>();
        Set<Id> scenarioIds = new Set<Id>();
        Map<Id, Id> scnAccMap = new Map<Id, Id>();
        Map<Id, Id> scpScnMap = new Map<Id, Id>();
        List<LLC_BI__Scenario__c> accList;

        for (Scenario_Product__c scp: triggerNewList)
        {
            scenarioIds.add(scp.Scenario__c);
            prodIds.add(scp.Product__c);
            scpScnMap.put(scp.Id, scp.Scenario__c);
        }

        accList = new List<LLC_BI__Scenario__c>([Select Id, LLC_BI__Opportunity__r.Account.Id from LLC_BI__Scenario__c where Id in :scenarioIds]);
        for (LLC_BI__Scenario__c scn: accList)
        {
            accIds.add(scn.LLC_BI__Opportunity__r.Account.Id);
            scnAccMap.put(scn.Id, scn.LLC_BI__Opportunity__r.Account.Id);
        }

        List<LLC_BI__Treasury_Service__c> tservices  = new List<LLC_BI__Treasury_Service__c>([Select id, LLC_BI__Relationship__r.Id, LLC_BI__Relationship__c,
            LLC_BI__Product_Reference__c, Prior_Month_Product_Revenue__c from LLC_BI__Treasury_Service__c where LLC_BI__Relationship__c in :accIds]);

        boolean found = false;
        for (Scenario_Product__c scp: triggerNewList)
        {
            found = false;
            for (LLC_BI__Treasury_Service__c ts: tservices)
            {
                Id scenarioId = scpScnMap.get(scp.Id);
                Id accountId = scnAccMap.get(scenarioId);
                if (accountId == ts.LLC_BI__Relationship__r.Id &&
                    scp.Product__c == ts.LLC_BI__Product_Reference__c)
                    {
                        //scp.Total_Incremental_Revenue__c = scp.fee_equivalent_revenue__c - ts.Prior_Month_Product_Revenue__c;
                        // scp.Total_Incremental_Bookable_Revenue__c = scp.Total_Bookable_Product_Revenue__c - ts.Prior_Month_Product_Revenue__c;
                        found = true;
                        break;
                    }
            }

            if (!found)
            {
                // scp.Total_Incremental_Revenue__c = scp.fee_equivalent_revenue__c;
                // scp.Total_Incremental_Bookable_Revenue__c = scp.Total_Bookable_Product_Revenue__c;
            }
        }
    }*/

}