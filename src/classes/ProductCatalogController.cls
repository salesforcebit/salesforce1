/**
 * Controller for server-side functionality related to managing the product catalog for
 * Treasury Services.
 * @author Dan Wuensch
 * @version 2
 */
global with sharing class ProductCatalogController {
	/**
	 * Gets the namespace prefix for classes in this package.
	 * @return String namespace prefix for use in fieldnames on the client side.
	 */
	@RemoteAction
	global static String getNamespacePrefix() {
		return ProductCatalogController.class.getName()
			.substringBefore('ProductCatalogController').substringBefore('.');
	}

	/**
	 * Gets a list of fieldsets for a given sObject type.
	 * @param sObjectName sObject to retrieve fieldsets for
	 * @return JSON array of objects representing the fieldsets available. "name" attribute
	 * contains the field API name and "fields" array contains objects representing the available
	 * fields.
	 */
	@RemoteAction
	global static String retrieveFieldSets(String sObjectName) {
		Map<String, Schema.sObjectType> globalDescribeMap = Schema.getGlobalDescribe();

		Schema.sObjectType sObjectTypeObj = globalDescribeMap.get(sObjectName);
		if (sObjectTypeObj == null) {
			return JSON_EMPTY_ARRAY;
		}

		Schema.DescribeSObjectResult description = sObjectTypeObj.getDescribe();

		List<FieldSetDesc> foundFieldSets = new List<FieldSetDesc>();
		for (Schema.FieldSet fieldset: description.FieldSets.getMap().values()) {
			foundFieldSets.add(new FieldSetDesc(fieldset.getName(), null));
		}

		// Serialize object hierarchy to JSON
		foundFieldSets.sort();
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(foundFieldSets);

		return gen.getAsString();
	}

	/**
	 * Gets a list of options for Screen Section Resource derived from the Screen Section Resource
	 * picklist field.
	 * @return JSON array of section resource options
	 */
	@RemoteAction
	global static String retrieveSectionResources() {
		List<SelectOption> options = new List<SelectOption>();

		Schema.DescribeFieldResult fieldResult =
			nFORCE__Screen_Section__c.nFORCE__Section_Resource__c.getDescribe();
		List<Schema.PicklistEntry> sectionResources = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry sectionResource : sectionResources)
		{
			options.add(new SelectOption(sectionResource.getValue(), sectionResource.getValue()));
		}

		// Serialize options to JSON
		options.sort();
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(options);

		return gen.getAsString();
	}

	/**
	 * Gets a list of VisualForce pages in the system.
	 * @return JSON array of route body options
	 */
	@RemoteAction
	global static String retrieveRouteBodyOptions() {
		List<SelectOption> options = new List<SelectOption>();

		List<ApexPage> apexPages = [
			SELECT
				Id,
				Name,
				NamespacePrefix
			FROM
				ApexPage
		];

		for (ApexPage page : apexPages) {
			String fullName = page.NamespacePrefix + '__' + page.Name;
			options.add(new SelectOption(fullName, fullName));
		}

		// Serialize options to JSON
		options.sort();
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(options);

		return gen.getAsString();
	}

	/**
	 * Gets a list of sObject types in the system.
	 * @return JSON array of sObject options
	 */
	@RemoteAction
	global static String retrieveSObjectTypes() {
		List<SelectOption> options = new List<SelectOption>();

		Map<String, Schema.sObjectType> globalDescribeMap = Schema.getGlobalDescribe();
		List<String> objectNames = new List<String>(globalDescribeMap.keySet());
		for (String objectName : objectNames) {
			options.add(new SelectOption(objectName, objectName));
		}

		// Serialize options to JSON
		options.sort();
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(options);

		return gen.getAsString();
	}

	/**
	 * Given a JSON object representing a UI template, stores it, contained routes, and screen
	 * section configurations.
	 * @param newUITemplate JSON object having a name field, isDefault field, and routes object
	 * array. Routes object array contains route-specific fields and an array of screen section
	 * objects.
	 * @return True if all operations completed successfully.
	 */
	@RemoteAction
	global static Boolean saveUITemplate(String newUITemplate) {
		// Initialize lists for storing objects, parent indices
		List<Product_Catalog_UI_Template__c> uiTemplateUpsertList
			= new List<Product_Catalog_UI_Template__c>();

		List<Product_Catalog_Route_Template__c> routeTemplateUpsertList
			= new List<Product_Catalog_Route_Template__c>();
		List<Integer> routeTemplateInsertParentIndices = new List<Integer>();

		List<Product_Catalog_Section_Configuration__c> sectionConfigUpsertList
			= new List<Product_Catalog_Section_Configuration__c>();
		List<Integer> sectionConfigInsertParentIndices = new List<Integer>();

		// Deserialize JSON
		ProductCatalogUITemplate uiTemplate = (ProductCatalogUITemplate)
			JSON.deserialize(newUITemplate, ProductCatalogUITemplate.class);
		uiTemplateUpsertList.add(uiTemplate.getSObject());

		// Process route templates
		for (Integer routeTemplateIndex = 0; routeTemplateIndex < uiTemplate.routeTemplates.size();
			routeTemplateIndex++) {
			ProductCatalogRouteTemplate routeTemplate = uiTemplate.routeTemplates.get(routeTemplateIndex);

			routeTemplateUpsertList.add(routeTemplate.getSObject());
			routeTemplateInsertParentIndices.add((uiTemplateUpsertList.size() - 1));

			// Process section configurations
			for (Integer sectionConfigIndex = 0; sectionConfigIndex <
				routeTemplate.sectionConfigurations.size(); sectionConfigIndex++) {
				ProductCatalogSectionConfiguration config
					= routeTemplate.sectionConfigurations.get(sectionConfigIndex);

				sectionConfigUpsertList.add(config.getSObject());
				sectionConfigInsertParentIndices.add((routeTemplateUpsertList.size() - 1));
			}
		}

		upsert uiTemplateUpsertList;

		// Map product type to parent UI template (insert only)
		mapChildNodesToParents(routeTemplateUpsertList, uiTemplateUpsertList,
			routeTemplateInsertParentIndices,
			Product_Catalog_Route_Template__c.Product_Catalog_UI_Template__c.getDescribe().getName());

		upsert routeTemplateUpsertList;

		// Map product to parent product type (insert only)
		mapChildNodesToParents(sectionConfigUpsertList, routeTemplateUpsertList,
			sectionConfigInsertParentIndices,
			Product_Catalog_Section_Configuration__c.Product_Catalog_Route_Template__c
				.getDescribe().getName());

		upsert sectionConfigUpsertList;

		return true;
	}

	/**
	 * Retrieves a list of Product Catalog UI Templates without their child collections populated.
	 * @return JSON array of UI Templates without Route Templates populated for brevity.
	 */
	@RemoteAction
	global static String retrieveUITemplates() {
		List<Product_Catalog_UI_Template__c> uiTemplates = [
			SELECT
				Id,
				Name,
				Is_Default__c
			FROM
				Product_Catalog_UI_Template__c
		];

		List<ProductCatalogUITemplate> jUITemplates = new List<ProductCatalogUITemplate>();
		for (Product_Catalog_UI_Template__c uiTemplate : uiTemplates) {
			ProductCatalogUITemplate jUITemplate = ProductCatalogUITemplate.buildFromDB(uiTemplate);
			jUITemplates.add(jUITemplate);
		}

		// Serialize object hierarchy to JSON
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(jUITemplates);

		return gen.getAsString();
	}

	/**
	 * Retrieves a single Product Catalog UI Template by Id.
	 * @param jId String representation of the template Id to look up
	 * @return UI Template object with child object arrays populated.
	 */
	@RemoteAction
	global static String retrieveUITemplateById(String jId) {
		Id templateId = Id.valueOf(jId);
		List<Product_Catalog_UI_Template__c> uiTemplates = [
			SELECT
				Id,
				Name,
				Is_Default__c
			FROM
				Product_Catalog_UI_Template__c
			WHERE
				Id =: templateId
		];

		if (uiTemplates == null || uiTemplates.size() == 0) {
			return JSON_EMPTY_OBJECT;
		}

		// Create a map of route templates to their parent object Ids
		Map<Id, List<Product_Catalog_Route_Template__c>> routeTemplatesAndParents =
			new Map<Id, List<Product_Catalog_Route_Template__c>>();
		List<Product_Catalog_Route_Template__c> routeTemplates = [
			SELECT
				Id,
				Name,
				Screen_Section_Resource__c,
				Route_Body__c,
				Route_Order__c,
				Route_Topbar__c,
				Route_Icon_Class__c,
				Route_Navigation__c,
				Route_Sub_Navigation__c,
				Product_Catalog_UI_Template__c
			FROM
				Product_Catalog_Route_Template__c
		];
		routeTemplatesAndParents = buildParentLookupMap(routeTemplates,
			Product_Catalog_Route_Template__c.Product_Catalog_UI_Template__c.getDescribe().getName());

		// Create a map of section configuration templates to their parent object Ids
		Map<Id, List<Product_Catalog_Section_Configuration__c>> sectionConfigsAndParents =
			new Map<Id, List<Product_Catalog_Section_Configuration__c>>();
		List<Product_Catalog_Section_Configuration__c> sectionConfigs = [
			SELECT
				Id,
				Name,
				SObject_Type__c,
				Field_Set__c,
				Layout__c,
				Display_Order__c,
				Product_Catalog_Route_Template__c
			FROM
				Product_Catalog_Section_Configuration__c
		];
		sectionConfigsAndParents = buildParentLookupMap(sectionConfigs,
			Product_Catalog_Section_Configuration__c.Product_Catalog_Route_Template__c
				.getDescribe().getName());

		// Build hierarchy of objects using smart lookups
		ProductCatalogUITemplate jUITemplate = ProductCatalogUITemplate.buildFromDB(uiTemplates[0]);
		if (routeTemplatesAndParents.get(uiTemplates[0].Id) != null) {
			for (Product_Catalog_Route_Template__c routeTemplate :
				routeTemplatesAndParents.get(uiTemplates[0].Id)) {
				ProductCatalogRouteTemplate jRouteTemplate
					= ProductCatalogRouteTemplate.buildFromDB(routeTemplate);
				if (sectionConfigsAndParents.get(routeTemplate.Id) != null) {
					for (Product_Catalog_Section_Configuration__c sectionConfig :
						sectionConfigsAndParents.get(routeTemplate.Id)) {
						ProductCatalogSectionConfiguration jSectionConfig
							= ProductCatalogSectionConfiguration.buildFromDB(sectionConfig);
						jRouteTemplate.sectionConfigurations.add(jSectionConfig);
					}
				}
				jUITemplate.routeTemplates.add(jRouteTemplate);
			}
		}

		// Serialize object hierarchy to JSON
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(jUITemplate);

		return gen.getAsString();
	}

	/**
	 * Queries product lines, product types, products, and bill points in the Treasury
	 * Management LOB.
	 * @return JSON array of product line objects,
	 * which have a name field and productTypes array of objects, which have name,
	 * lookupKey fields and a products array of objects, which have name and
	 * lookupKey fields and a billPoints array of objects, which have name,
	 * price, unitCost, lookupKey fields.
	 * All fields store their SF id in the "sid" field.
	 */
	@RemoteAction
	global static String retrieveProductCatalog() {
		// Query product catalog objects
		List<LLC_BI__Product_Line__c> productLines = [
			SELECT
				Id,
				Name
			FROM
				LLC_BI__Product_Line__c
			WHERE
				LLC_BI__Product_Object__c =: TS_PRODUCT_OBJECT
		];

		// Create a map of product types to their parent object Ids
		Map<Id, List<LLC_BI__Product_Type__c>> productTypesAndParents =
			new Map<Id, List<LLC_BI__Product_Type__c>>();
		List<LLC_BI__Product_Type__c> productTypes = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Product_Line__c
			FROM
				LLC_BI__Product_Type__c
			WHERE
				LLC_BI__Usage_Type__c =: TS_USAGE_TYPE
		];
		productTypesAndParents = buildParentLookupMap(productTypes,
			LLC_BI__Product_Type__c.LLC_BI__Product_Line__c.getDescribe().getName());

		// Create a map of products to their parent object Ids
		Map<Id, List<LLC_BI__Product__c>> productsAndParents =
			new Map<Id, List<LLC_BI__Product__c>>();
		List<LLC_BI__Product__c> products = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Product_Type__c
			FROM
				LLC_BI__Product__c
		];
		productsAndParents = buildParentLookupMap(products,
				LLC_BI__Product__c.LLC_BI__Product_Type__c.getDescribe().getName());

		// Create a map of bill points to their parent object ids
		Map<Id, List<LLC_BI__Bill_Point__c>> billPointsAndParents =
			new Map<Id, List<LLC_BI__Bill_Point__c>>();
		List<LLC_BI__Bill_Point__c> billPoints = [
			SELECT
				Id,
				Name,
				LLC_BI__lookupKey__c,
				LLC_BI__Price__c,
				LLC_BI__Unit_Cost__c,
				LLC_BI__Product__c
			FROM
				LLC_BI__Bill_Point__c
		];
		billPointsAndParents = buildParentLookupMap(billPoints,
			LLC_BI__Bill_Point__c.LLC_BI__Product__c.getDescribe().getName());

		// Build hierarchy of objects using smart lookups
		List<ProductLine> jLines = new List<ProductLine>();
		for (LLC_BI__Product_Line__c line : productLines) {
			ProductLine jLine = ProductLine.buildFromDB(line);
			if (productTypesAndParents.get(line.Id) != null) {
				for (LLC_BI__Product_Type__c type : productTypesAndParents.get(line.Id)) {
					ProductType jType = ProductType.buildFromDB(type);
					if (productsAndParents.get(type.Id) != null) {
						for (LLC_BI__Product__c prod : productsAndParents.get(type.Id)) {
							Product jProduct = Product.buildFromDB(prod);
							if (billPointsAndParents.get(prod.Id) != null) {
								for (LLC_BI__Bill_Point__c bp : billPointsAndParents.get(prod.Id)) {
									BillPoint jBillPoint = BillPoint.buildFromDB(bp);
									jProduct.billPoints.add(jBillPoint);
								}
							}
							jType.products.add(jProduct);
						}
					}
					jLine.productTypes.add(jType);
				}
			}
			jLines.add(jLine);
		}

		// Serialize object hierarchy to JSON
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeObject(jLines);

		return gen.getAsString();
	}

	/**
	 * Given a JSON data structure representing the product catalog, parses JSON content into
	 * hierarchical strongly-typed objects which can be converted to SObjects for DML operations.
	 * Inserts and updates product catalog records based on data provided.
	 * Uses collections of objects and parent record indices to allow for bulkification of DML
	 * @param jsonLineArray JSON array of product line objects,
	 * which have a name field and productTypes array of objects, which have name,
	 * lookupKey fields and a products array of objects, which have name and
	 * lookupKey fields and a billPoints array of objects, which have name,
	 * price, unitCost, lookupKey fields.
	 * An "sid" field at any object level in the data structure indicates an existing record and
	 * contains that record's sObject Id.
	 * @return True if all operations completed.
	 */
	@RemoteAction
	global static Boolean saveProductCatalog(String jsonLineArray) {
		// Initialize lists for storing objects, parent indices, and objects to delete
		List<LLC_BI__Product_Line__c> productLinesUpsertList = new List<LLC_BI__Product_Line__c>();

		List<LLC_BI__Product_Type__c> productTypesUpsertList = new List<LLC_BI__Product_Type__c>();
		List<Integer> productTypeInsertParentIndices = new List<Integer>();

		List<LLC_BI__Product__c> productsUpsertList = new List<LLC_BI__Product__c>();
		List<Integer> productInsertParentIndices = new List<Integer>();

		List<LLC_BI__Bill_Point__c> billPointsUpsertList = new List<LLC_BI__Bill_Point__c>();
		List<Integer> billPointInsertParentIndices = new List<Integer>();

		// Deserialize JSON
		List<ProductLine> jLines =
			(List<ProductLine>)JSON.deserialize(jsonLineArray, List<ProductLine>.class);

		// Process product lines
		for (Integer productLineIndex = 0; productLineIndex < jLines.size();
			productLineIndex++) {
			ProductLine productLine = jLines.get(productLineIndex);

			productLinesUpsertList.add(productLine.getSObject());

			// Process product types
			for (Integer productTypeIndex = 0; productTypeIndex < productLine.productTypes.size();
				productTypeIndex++) {
				ProductType productType = productLine.productTypes.get(productTypeIndex);

				productTypesUpsertList.add(productType.getSObject());
				productTypeInsertParentIndices.add((productLinesUpsertList.size() - 1));

				// Process products
				for (Integer productIndex = 0; productIndex < productType.products.size();
					productIndex++) {
					Product product = productType.products.get(productIndex);

					productsUpsertList.add(product.getSObject());
					productInsertParentIndices.add((productTypesUpsertList.size() - 1));

					// Process bill points
					for (Integer billPointIndex = 0; billPointIndex < product.billPoints.size();
						billPointIndex++) {
						BillPoint billPoint = product.billPoints.get(billPointIndex);

						billPointsUpsertList.add(billPoint.getSObject());
						billPointInsertParentIndices.add((productsUpsertList.size() - 1));
					}
				}
			}
		}

		upsert productLinesUpsertList;

		// Map product type to parent product line (insert only)
		mapChildNodesToParents(productTypesUpsertList, productLinesUpsertList,
			productTypeInsertParentIndices,
			LLC_BI__Product_Type__c.LLC_BI__Product_Line__c.getDescribe().getName());

		upsert productTypesUpsertList;

		// Map product to parent product type (insert only)
		mapChildNodesToParents(productsUpsertList, productTypesUpsertList,
			productInsertParentIndices,
			LLC_BI__Product__c.LLC_BI__Product_Type__c.getDescribe().getName());

		upsert productsUpsertList;

		// Map bill point to parent product (insert only)
		mapChildNodesToParents(billPointsUpsertList, productsUpsertList,
			billPointInsertParentIndices,
			LLC_BI__Bill_Point__c.LLC_BI__Product__c.getDescribe().getName());

		upsert billPointsUpsertList;

		return true;
	}

	/**
	 * Provides an object representing the JSON schema of a fieldset and its associated fields
	 * Is sortable by name field using List.sort()
	 */
	public class FieldSetDesc implements Comparable {
		public String name;
		public List<FieldSetMemberDesc> fields;

		public FieldSetDesc() {
			this.fields = new List<FieldSetMemberDesc>();
		}

		public FieldSetDesc(String name, List<FieldSetMemberDesc> fields) {
			this.name = name;
			this.fields = fields;
		}

		public Integer compareTo(Object compareTo) {
			FieldSetDesc other = (FieldSetDesc)compareTo;
			if (this.name == other.name) {
				return 0;
			}
			if (this.name > other.name) {
				return 1;
			}
			return -1;
		}
	}

	/**
	 * Provides an object representing the JSON schema of a field contained within a fieldset
	 * Is sortable by field label using List.sort()
	 */
	public class FieldSetMemberDesc implements Comparable {
		public String label;
		public String fieldPath;

		public FieldSetMemberDesc() {}

		public FieldSetMemberDesc(String label, String fieldPath) {
			this.label = label;
			this.fieldPath = fieldPath;
		}

		public Integer compareTo(Object compareTo) {
			FieldSetMemberDesc other = (FieldSetMemberDesc)compareTo;
			if (this.label == other.label) {
				return 0;
			}
			if (this.label > other.label) {
				return 1;
			}
			return -1;
		}
	}

	/**
	 * A simple class for representing a select option in a JSON object array.
	 * Used due to lack of support for System.SelectOption in JSON serialization.
	 * Is sortable by label field using List.sort()
	 */
	public class SelectOption implements Comparable {
		public String value;
		public String label;

		public SelectOption(String value, String label) {
			this.value = value;
			this.label = label;
		}

		public Integer compareTo(Object compareTo) {
			SelectOption other = (SelectOption)compareTo;
			if (this.label == other.label) {
				return 0;
			}
			if (this.label > other.label) {
				return 1;
			}
			return -1;
		}
	}

	private static Map<Id, List<sObject>> buildParentLookupMap(List<sObject> sObjectsWithParents,
		String parentFieldName) {
		Map<Id, List<sObject>> parentLookupMap = new Map<Id, List<sObject>>();
		for (sObject obj : sObjectsWithParents) {
			if (obj.get(parentFieldName) != null) {
				if (parentLookupMap.get((Id)obj.get(parentFieldName)) == null) {
					parentLookupMap.put((Id)obj.get(parentFieldName), new List<sObject> {obj});
				}
				else {
					parentLookupMap.get((Id)obj.get(parentFieldName)).add(obj);
				}
			}
		}
		return parentLookupMap;
	}

	private static void mapChildNodesToParents(List<sObject> sObjectsWithParents,
		List<sObject> parents, List<Integer> parentIndices, String parentFieldName) {
		for (Integer i = 0; i < sObjectsWithParents.size(); i++) {
			sObject child = sObjectsWithParents.get(i);
			if (child.get('Id') == null || String.valueOf(child.get('Id')).length() == 0) {
				Integer parentIndex = parentIndices.get(i);
				child.put(parentFieldName, parents.get(parentIndex).get('Id'));
			}
		}
	}

	/**
	 * The default product object (LLC_BI__Product_Object__c) value for TM product lines.
	 */
	public static final String TS_PRODUCT_OBJECT = 'Treasury_Service__c';

	/**
	 * The default usage type (LLC_BI__Usage_Type__c) value for TM product types.
	 */
	public static final String TS_USAGE_TYPE = 'Treasury Management';

	/**
	 * Provides the literal String representation of an empty JSON object.
	 */
	public static final String JSON_EMPTY_OBJECT = '{}';

	/**
	 * Provides the literal String representation of an empty JSON array.
	 */
	public static final String JSON_EMPTY_ARRAY = '[]';
}