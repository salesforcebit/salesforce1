@isTest
public class ECCP_DscCustomer_WebSvcCalloutTest {
    public static testmethod ECCP_CustomerResponse DCTest(){
    
        Test.setMock(WebServiceMock.class, new ECCP_DSCCustomer_WebServiceMockImpl());
        
        ECCP_CustomerRequest TestReq = new ECCP_CustomerRequest();
        List<String> ssnList = new List<String>();
        ssnList.add('123');
        ssnList.add('567908999');
        TestReq.SSN = ssnList;
        TestReq.appId='123asd';
        TestReq.MDMId = 'extId';
        
        ECCP_CustomerResponse CBres = ECCP_DSCCustomerTest_WebSvcCallout.DSCCustomerCallout(TestReq);
        return CBres;
    }

   
}