/*******************************************************************************************************************************************
* @name         :   ECCP_CreditBureauController
* @description  :   To call the service helper and show the corresponding message in visual force page
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/

public class ECCP_CreditBureauController{

    public String id{get;set;}
    public String ochCode{get;set;}
    public boolean afterComp{get;set;}
    public Map<String,ECCP_WebService_Data__mdt> mapWsSettings;
    public list<ECCP_CreditBureauContHelper.errorWrapper> errLst{get;set;}
    public string vStage{get;set;}
    private final LLC_BI__Loan__c loanRec;
    

    public ECCP_CreditBureauController(ApexPages.StandardController stdController) {
        afterComp = false;
        id = stdController.getRecord().id;            
        mapWsSettings = ECCP_CustomMetadataUtil.queryMetadata(ECCP_IntegrationConstants.CreditBureauMataData);
    
    }
     
     public PageReference ClosePage() {     
         PageReference parentPage = new PageReference('/?'+id);
         parentPage.setRedirect(true);                
         return parentPage;
     }
    
    public PageReference Search() {

     
        errLst =null;
        afterComp = true;
        
        // Mandatory validation to run the service   
             
        String vStage = [select LLC_BI__Stage__c from LLC_BI__Loan__c where id=:id LIMIT 1].LLC_BI__Stage__c;        
        if(vStage != 'Credit Stop and Proceed' && vStage != 'Application Processing' && vStage != 'Credit Underwriting' && vStage != 'Credit Decisioning' && vStage != 'Pre-Closing Due Diligence'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error,'Current loan stage is not applicable to run the credit bureau check'));
            return null;
        }


String orchCode = string.valueof(integer.valueof(mapWsSettings.get(ECCP_IntegrationConstants.CreditBureauMataData).OrchestrationCode__c));

        //String orchCode = string.valueof(mapWsSettings.get(ECCP_IntegrationConstants.CreditBureauMataData).OrchestrationCode__c);        
        //String orchCode = string.valueof(mapWsSettings.get(ECCP_IntegrationConstants.CreditBureauMataData).OrchestrationCode__c);
        //system.debug('--ORC 1--'+orchCode );
        //system.debug('--ORC 2--'+string.valueof(integer.valueof(mapWsSettings.get(ECCP_IntegrationConstants.CreditBureauMataData).OrchestrationCode__c)));
        
        errLst = ECCP_CreditBureauContHelper.ServiceCalloutMain(id, orchCode );
        if (errLst != null && errLst.size() > 0)
        {
            for(ECCP_CreditBureauContHelper.errorWrapper err : errLst){
            
                if(err.errorCode != 'ECCP_INT_0005' && err.errorCode != 'ECCP_INT_0007' ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error,err.errorDesc+':'+(err.acctname).touppercase()+' ['+err.entityId+']'));
                }
                if(err.errorCode == 'ECCP_INT_0005'){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.confirm,err.errorDesc+':'+(err.acctname).touppercase()+' ['+err.entityId+']'));
                }
                if(err.errorCode == 'ECCP_INT_0007'){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.warning,err.errorDesc+':'+(err.acctname).touppercase()+' ['+err.entityId+']'));
                }
            }//end of for loop
            return null;
        }//end of if
        return null;
    }//end of search function
}//end of controller clas