/** This controller controls the behavior of the ProjectPlanning VF page
  * Chintan Adhyapak, cadhyapak@westmonroepartners.com, January 25, 2016 
  */ 
public with sharing class ctlr_ProjectPlanning {
    //Object to hold treasury service and a selected prerequisite service
    public class CustomService {
        public LLC_BI__Treasury_Service__c treasuryService  {get; set;}
        public String prereqService                         {get; set;}
        
        //constructor
        public CustomService(LLC_BI__Treasury_Service__c service, String name) {
            this.treasuryService = service;
            this.prereqService = name;
        }
    } //customService
     
    //local vars
    private Map<Id, LLC_BI__Treasury_Service__c> myTSMap;
    private LLC_BI__Product_Package__c myProductPackage;
    public List<SelectOption> serviceNames      {get; set;}
    public List<CustomService> myServicesList   {get; set;}
    public String statusMsg                     {get; set;}
    public boolean displayGantt                 {get; set;} 
    
    //Constructor
    public ctlr_ProjectPlanning(ApexPages.StandardController stdController) {
        myProductPackage = (LLC_BI__Product_Package__c)stdController.getRecord();
        
        //read all fieldNames for LLC_BI__Treasury_Service__c object
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.LLC_BI__Treasury_Service__c.fields.getMap();
        
        //create a SOQL string with all fields
        String fieldNames = '';
        for(String fieldName : schemaFieldMap.keySet()) {
            fieldNames += ',' + fieldName;
        }
        fieldNames = fieldNames.substring(1);
        fieldNames += ', Prerequisite_Treasury_Service__r.Id, Prerequisite_Treasury_Service__r.Name';
        String soql = 'Select ' + fieldNames + ' From LLC_BI__Treasury_Service__c Where LLC_BI__Product_Package__c = \'' + myProductPackage.Id + '\' Order by Name';
        System.debug('Treasury Service query is: ' + soql);
        
        //read Treasury Services data
        myTSMap = new Map<Id, LLC_BI__Treasury_Service__c> ((List<LLC_BI__Treasury_Service__c>)Database.query(soql));
            
        //Instantiate myServicesList and serviceNames to use in drop down list
        serviceNames = new List<SelectOption> ();
        serviceNames.add(new SelectOption('None', '--- None ---'));
        myServicesList = new List<customService> ();
        for (LLC_BI__Treasury_Service__c ts : myTSMap.values()) {
            serviceNames.add(new SelectOption(ts.Id, ts.Name));
            myServicesList.add(new CustomService(ts, ts.Prerequisite_Treasury_Service__r.Id));
            System.debug('CPA Treasury Services read: ' + ts.Name + 'With prereq id: ' + ts.Prerequisite_Treasury_Service__r.Id);   
        }
        
        //Set flag to display Gantt Chart to false
        this.displayGantt = false;
    } //ctlr_ProjectPlanning
    
    
    
    /** Save the data from the form on to the Treasury Services */
    
    public PageReference saveData() {
        //local vars
        String errMsg = null;
        Map<String, String> dependencyMap = new Map<String, String> ();
        Map<String, String> serviceNameMap = new Map<String, String> ();
        Map<String, String> cyclicErrorMap = new Map<String, String> ();
        boolean found = false;
        this.statusMsg = '<span style=\"color:red">Please Correct Errors Before Saving</span>';
        
        //Check for errors
        for (CustomService service : myServicesList) {
            System.debug('CPA reading service: ' + service.treasuryService.Name + '-' + service.treasuryService.Delay_Implementation__c + '-' + service.treasuryService.Planned_Implementation_Start_Date__c + '-' + service.prereqService);
            //Check that Start Date is not filled in if the Delay Implementation checkbox is not checked
            if (service.treasuryService.Planned_Implementation_Start_Date__c != null && !service.treasuryService.Delay_Implementation__c) {
                System.debug('CPA detected an error on service: ' + service.treasuryService.Name);
                errMsg = 'You cannot have a <b>Planned Implementation Date</b> for treasury service <span style=\"color:#659EC7; font-weight: bold;\">' + service.treasuryService.Name + '</span> unless <b>Delay Implementation</b> is checked.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errMsg));
            }
            //Check that if Delay Implementation is checked then Implementation Date is filled in
            if (service.treasuryService.Planned_Implementation_Start_Date__c == null && service.treasuryService.Delay_Implementation__c) {
                System.debug('CPA detected an error on service: ' + service.treasuryService.Name);
                errMsg = 'You must have a <b>Planned Implementation Date</b> for treasury service <span style=\"color:#659EC7; font-weight: bold;\">' + service.treasuryService.Name + '</span> if <b>Delay Implementation</b> is checked.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errMsg));
            }
            //Check that a service isn't dependent on itself
            if (!service.prereqService.equals('None') && (service.prereqService.equals(service.treasuryService.Id))) {
                System.debug('CPA detected an error on service: ' + service.treasuryService.Name);
                errMsg = 'Treasury service <span style=\"color:#659EC7; font-weight: bold;\">' + service.treasuryService.Name + '</span> cannot be dependent on itself.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errMsg));
            }
            //Save Dependency data in maps to check for cyclic dependencies
            if (!service.prereqService.equals('None')) {
                dependencyMap.put(String.valueOf(service.treasuryService.Id), service.prereqService);
                serviceNameMap.put(service.treasuryService.Id, service.treasuryService.Name);
            }
        }
        //Check for cyclic dependency error
        for (String service : dependencyMap.keySet()) {
            String dependentService = dependencyMap.get(service);
            System.debug('CPA comparing service ' + service + ' with dependent service ' + dependentService);
            if (dependencyMap.get(dependentService) != null && !service.equals(dependentService) && dependencyMap.get(dependentService).equals(service)) {
                //cyclicErrorMap is used only because I don't want to print two error messages for cyclic dependencies, one for each service
                for (String name : cyclicErrorMap.keySet()) {
                    if (service.equals(cyclicErrorMap.get(name)))
                        found = true;
                }
                if (!found)
                    cyclicErrorMap.put(service, dependentService);
            }
        }
        for (String service : cyclicErrorMap.keySet()) {
            errMsg = 'Treasury service <span style=\"color:#659EC7; font-weight: bold;\">' + serviceNameMap.get(service);
            errMsg += '</span> has a cyclic dependency on service <span style=\"color:#659EC7; font-weight: bold;\">';
            errMsg += serviceNameMap.get(cyclicErrorMap.get(service)) + '</span>. They cannot be dependent on each other.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errMsg));
        }
        //Save data if no errors
        if (String.isEmpty(errMsg)) {
            //Reset all Treasury Service Data
            for (CustomService service : myServicesList) {
                service.treasuryService.Prerequisite_Treasury_Service__c = null;
                service.treasuryService.Is_this_Treasury_Service_a_Prerequisite__c = false;
            }

            //Save Prerequisite Service Dependencies
            for (CustomService service : myServicesList) {
                if (!service.prereqService.equals('None')) {
                    System.debug('CPA service.prereqService is: ' + service.prereqService);
                    service.treasuryService.Prerequisite_Treasury_Service__c = service.prereqService;
                    (myTSMap.get(service.prereqService)).Is_this_Treasury_Service_a_Prerequisite__c = true;
                }
            }
            try {
                update myTSMap.values();
            } catch (Exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
                statusMsg = 'An Error Occurred While Saving Your Data';
                return null;
            }
            statusMsg = 'Your Changes Were Successfully Saved';
            this.displayGantt = true;
        }
        return null;
    }//saveData
    
    
    
    /** Return to the Product Package Page */
    public PageReference returnBack() {
        PageReference redirect = new PageReference('/' + myProductPackage.Id);
        redirect.setRedirect(true);
        return redirect;
    } //returnBack
}