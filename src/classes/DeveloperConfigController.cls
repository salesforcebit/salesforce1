public with sharing class DeveloperConfigController extends TMProducts {
	public Boolean isCatalogLoaded;

	public DeveloperConfigController() {
		getProductTypeMap();
		this.isCatalogLoaded = hasProducts();
	}

	public void createFullTMProductCatalog() {

		/* Create Product Features */
		csvFeature = PRODUCT_FEATURE_DEFAULT;
		createProductFeatures();

		/* Create Product Lines */
		csvProductLine = PRODUCT_LINE_DEFAULT;
		createProductLine();

		/* Create Product Types (Product Type - Usage Type picklist, add 'Treasury Management') */
		csvProductType = PRODUCT_TYPE_DEFAULT;
		productLineID = getProductLineIdFromLookupKey(TM_PRODUCT_LINE_LOOKUP);
		usageType = TM_USAGE_TYPE;
		createProductType();

		/* Create Products */
		getFeatureMap();
		getProductTypeMap();
		for (String key : PRODUCT_DEFAULT.keySet()) {
			productTypeID = productTypeMap.get(key);
			csvProduct = PRODUCT_DEFAULT.get(key);
			createProduct();
		}

		/* Create Bill Points */
		getProductMap();
		getProductTypeMap();
		csvBillPoints = BILL_POINTS_DEFAULT;
		createBillPoint();

		csvProductConnection = PRODUCT_CONNECTIONS_DEFAULT;
		createProductConnection();
	}

	private Boolean hasProducts(){
		getProductTypeMap();
		if(productTypeMap.size() == 11 || productTypeMap.size() > 11){
			return true;
		}
		return false;
	}

	/*
DeveloperConfigController controller = new DeveloperConfigController();
controller.createFullTMProductCatalog();

	public void wipeOut() {
		List<LLC_BI__Bill_Point__c> delBillPts = [SELECT Id From LLC_BI__Bill_Point__c];
		delete delBillPts;
		List<LLC_BI__Product__c> delProds = [SELECT Id From LLC_BI__Product__c];
		delete delProds;
		List<LLC_BI__Product_Type__c> delTypes = [SELECT Id From LLC_BI__Product_Type__c];
		delete delTypes;
		List<LLC_BI__Product_Line__c> delLines = [SELECT Id From LLC_BI__Product_Line__c];
		delete delLines;
		List<LLC_BI__Product_Feature__c> delFeats = [SELECT Id From LLC_BI__Product_Feature__c];
		delete delFeats;
		List<LLC_BI__Product_Connection__c> delConns = [SELECT Id From LLC_BI__Product_Connection__c];
		delete delConns;
	}
*/


	// Product Feature: Purpose Code, Lookup Code, true booleans
	private static String PRODUCT_FEATURE_DEFAULT =
		'Bundle, BNDL, Is_Bundle__c\n' +
		'Online Banking Service, OB,Is_Online_Banking_Service__c\n' +
		'Automated Clearing House Service, ACH,Is_Automated_Clearing_House_Service__c\n' +
		'Reconciliation Service, RS,Is_Reconciliation_Service__c\n' +
		'Cash Service, CS,Is_Cash_Service__c\n' +
		'Remote Deposit Capture Service, RDCS,Is_Remote_Deposit_Capture_Service__c\n' +
		'Depository Service, DS,Is_Depository_Service__c\n' +
		'Reporting Service, RPTS,Is_Reporting_Service__c\n' +
		'Disbursement Service, DISS,Is_Disbursement_Service__c\n' +
		'Sweep Service, SS,Is_Sweep_Service__c\n' +
		'Lockbox Service, LBX,Is_Lockbox_Service__c\n' +
		'Wire Service, WS,Is_Wire_Service__c\n' +
		'Miscellaneous Service, MISC,Is_Miscellaneous_Service__c\n' +
		'Zero Balance Account Service, ZBAS,Is_Zero_Balance_Account_Service__c';

	// Product Line: Name, Lookup Key, Object Type
	private static String PRODUCT_LINE_DEFAULT =
		'Treasury Management, TM001, Treasury_Service__c\n' +
		'Loan Product Line, LPL001, Loan__c';

	private static String TM_PRODUCT_LINE_LOOKUP = 'TM001';
	private static String TM_USAGE_TYPE = 'Treasury Management';

	// Product Type: Name, Lookup Key
	private static String PRODUCT_TYPE_DEFAULT =
		'Account Analysis, AA\n' +
		'Account Reconciliation Services, ARS\n' +
		'ACH Services, ACH\n' +
		'Cash Services, CS\n' +
		'Controlled Disbursement, CD\n' +
		'Depository Services, DS\n' +
		'Liquidity Services, LS\n' +
		'Lockbox Services, LBX\n' +
		'Online Banking, OB\n' +
		'Return Deposit Item Processing, RDIP\n' +
		'Wire Services, WS';

	// Product: name, lookup key, feature lookup key, default app (Map Key = Product Type lookupkey)
	private static final Map<String, String> PRODUCT_DEFAULT = new Map<String,String>{

		// Account Analysis							Feature:	Is_Miscellaneous_Service__c (MISC)
		'AA'  => 'Account Analysis Statement, AA001,MISC,treasury-generic.ts-oe-generic',

		// Account Reconciliation Services 		Feature:	Is_Reconciliation_Service__c (RS)
		'ARS' => 'Account Analysis Statement, ARS001, RS,treasury-generic.ts-oe-generic\n' +
					'Account Reconciliation Services, ARS002, RS,treasury-generic.ts-oe-generic\n' +
					'ARP Deposit Recon, ARS003, RS,treasury-generic.ts-oe-generic\n' +
					'ARP Full, ARS004, RS,treasury-generic.ts-oe-generic\n' +
					'ARP Partial, ARS005, RS,treasury-generic.ts-oe-generic\n' +
					'ARP Setup, ARS006, RS,treasury-generic.ts-oe-generic\n' +
					'ARP Transmission, ARS007, RS,treasury-generic.ts-oe-generic\n' +
					'CD-Rom, ARS008, RS,treasury-generic.ts-oe-generic',

		// ACH Services							 	Feature:	Is_Automated_Clearing_House_Service__c (ACH)
		'ACH' => 'Account Receivables Conversion, ACH001, ACH,treasury-generic.ts-oe-generic\n' +
					'ACH Block and Filter, ACH002, ACH,treasury-generic.ts-oe-generic\n' +
					'ACH Credit, ACH003, ACH,treasury-generic.ts-oe-generic\n' +
					'ACH Debit, ACH004, ACH,treasury-generic.ts-oe-generic\n' +
					'ACH Direct Send, ACH005, ACH,treasury-generic.ts-oe-generic\n' +
					'Direct Deposit, ACH006, ACH,treasury-generic.ts-oe-generic\n' +
					'Remote Deposit, ACH007,RDCS,treasury-generic.ts-oe-generic\n' +
					'Serv ACH, ACH008, ACH,treasury-generic.ts-oe-generic',

		// Cash Services								Feature:	Is_Cash_Service__c (CS)
		'CS'  => 'Coin and Currency, CS001, CS,treasury-generic.ts-oe-generic',

		// Controlled Disbursement 				Feature:	Is_Disbursement_Service__c (DISS)
		'CD'  => 'Controlled Disbursement, CD001, DISS,treasury-generic.ts-oe-generic',

		// Depository Services 						Feature:	Is_Depository_Service__c (DS)
		'DS'  => 'Check Printing, DS001, DS,treasury-generic.ts-oe-generic\n' +
					'Checks Deposited, DS002, DS,treasury-generic.ts-oe-generic\n' +
					'Deposit Adjustment, DS003, DS,treasury-generic.ts-oe-generic\n' +
					'FDIC Insurance, DS004, DS,treasury-generic.ts-oe-generic\n' +
					'Inclearing Checks, DS005, DS,treasury-generic.ts-oe-generic\n' +
					'Overdraft, DS006, DS,treasury-generic.ts-oe-generic\n' +
					'Pre-Encoded Checks, DS007, DS,treasury-generic.ts-oe-generic\n' +
					'Pre-Encoded Rejects, DS008, DS,treasury-generic.ts-oe-generic\n' +
					'Research, DS009, DS,treasury-generic.ts-oe-generic\n' +
					'Return Deposited Item, DS010, DS,treasury-generic.ts-oe-generic\n' +
					'Returned Deposited Item, DS011, DS,treasury-generic.ts-oe-generic\n' +
					'Stop Payment Manual, DS012, DS,treasury-generic.ts-oe-generic\n' +
					'Account Services, DS013, DS,treasury-generic.ts-oe-generic',	//required product

		// Liquidity Services 						Feature: Is_Sweep_Service__c (SS), Is_Zero_Balance_Account_Service__c (ZBAS)
		'LS'  => 'Repo Sweep, LS001, SS,treasury-generic.ts-oe-generic\n' +
					'Zero Balance Account, LS003, ZBAS,treasury-generic.ts-oe-generic',

		// Lockbox Services 							Feature:	Is_Lockbox_Service__c (LBX)
		'LBX' => 'Retail Lockbox, LBX001, LBX,treasury-generic.ts-oe-generic\n' +
					'Wholesale Lockbox, LBX002, LBX,treasury-generic.ts-oe-generic',

		// Online Banking 							Feature:	Is_Online_Banking_Service__c (OB), Is_Bundle (BNDL), Is_Reporting_Service (RPTS)
		'OB'  => 'Bill Pay, OB001, OB,treasury-generic.ts-oe-generic\n' +
					'Direct File Transfer, OB002, OB,treasury-generic.ts-oe-generic\n' +
					'OB Account Analysis, OB003, OB,treasury-generic.ts-oe-generic\n' +
					'OB Book Transfer, OB004, OB,treasury-generic.ts-oe-generic\n' +
					'OB Bundle, OB005, BNDL,treasury-generic.ts-oe-generic\n' +
					'OB Information Reporting, OB006,RPTS,treasury-generic.ts-oe-generic\n' +
					'OB Loan Module, OB007, OB,treasury-generic.ts-oe-generic\n' +
					'OB Security Services, OB008, OB,treasury-generic.ts-oe-generic\n' +
					'OB Security Tokens, OB009, OB,treasury-generic.ts-oe-generic\n' +
					'OB Stop Payment, OB010, OB,treasury-generic.ts-oe-generic',

		// Return Deposit Item Processing 		Feature:	Is_Remote_Deposit_Capture_Service(RDCS)
		'RDIP' => 'Return Deposited Item, RDIP001,RDCS,treasury-generic.ts-oe-generic',

		// Wire Services 								Feature:	Is_Wire_Service__c (WS)
		'WS'  => 'Forced Wire Transfer Wd, WS001, WS,treasury-generic.ts-oe-generic\n' +
					'Wire Incoming, WS003, WS,treasury-generic.ts-oe-generic\n' +
					'Wire Notification, WS005, WS,treasury-generic.ts-oe-generic\n' +
					'Wire Outgoing Manual, WS006, WS,treasury-generic.ts-oe-generic'
	};

	// Product Connections: parent product lookupKey, child product lookupKey, isRequired (Y/N), Type
	private static String PRODUCT_CONNECTIONS_DEFAULT =
		'DS013, , Y\n' + 			// Account Services = required product
		'OB005, OB001, N, Bundle\n' +		// OB Bundle
		'OB005, OB002, N, Bundle\n' +
		'OB005, OB003, N, Bundle\n' +
		'OB005, OB004, N, Bundle';

	// Bill Point: name, lookupKey, product lookupKey, price, unitCost
	private static String BILL_POINTS_DEFAULT =
		'Account Analysis Statement, 1014_1, AA001,,10.0000,0\n' +
		'Deposit Recon Item, 1055_2, ARS003,,0.0500,0\n' +
		'Deposit Recon Monthly, 1060_3, ARS003,,40.0000,0\n' +
		'Full Recon Item, 1063_4, ARS004,,0.0500,0\n' +
		'Full Recon Monthly, 1062_5, ARS004,,60.0000,0\n' +
		'Partial Recon Item, 1059_6, ARS005,,0.0300,0\n' +
		'Partial Recon Monthly, 1058_7, ARS005,,50.0000,0\n' +
		'Setup, 1061_8, ARS006,,50.0000,0\n' +
		'Per Transmission, 1057_9, ARS007,,15.0000,0\n' +
		'Transmission Monthly, 1056_10, ARS007,,35.0000,0\n' +
		'CD-ROM Annual, 1508_11, ARS008,,15.0000,0\n' +
		'CD-ROM Check Image, 1511_12, ARS008,,0.0800,0\n' +
		'CD-ROM Duplicate, 1528_13, ARS008,,20.0000,0\n' +
		'CD-ROM Monthly, 1510_14, ARS008,,30.0000,0\n' +
		'CD-ROM Quarterly, 1509_15, ARS008,,10.0000,0\n' +
		'CD-ROM Setup, 1507_16, ARS008,,50.0000,0\n' +
		'ARC Representment, 3544_29, ACH001,,2.5000,0\n' +
		'ARC Return Item, 3543_30, ACH001,,5.0000,0\n' +
		'ARC Transaction, 3542_31, ACH001,,0.2500,0\n' +
		'ACH Block Monthly, 3520_32, ACH002,,10.0000,0\n' +
		'ACH Block Monthly 2nd - 5th Acct, 3520_33, ACH002,,5.0000,0\n' +
		'ACH Block Monthly 6th and <, 3520_34, ACH002,,0.0000,0\n' +
		'ACH Block/Filter Authorization, 3522_35, ACH002,,5.0000,0\n' +
		'ACH Block/Filter Reject, 3521_36, ACH002,,5.0000,0\n' +
		'ACH Block/Filter Setup, 3519_37, ACH002,,25.0000,0\n' +
		'ACH Filter Add or Change, 3524_38, ACH002,,5.0000,0\n' +
		'ACH Filter Monthly, 3523_39, ACH002,,15.0000,0\n' +
		'ACH Filter Monthly 2nd - 5th Acct, 3523_40, ACH002,,10.0000,0\n' +
		'ACH Filter Monthly 6th and <, 3523_41, ACH002,,0.0000,0\n' +
		'ACH Credits, 3506_42, ACH003,,0.3000,0.0027\n' +
		'ACH Debits, 3507_43, ACH004,,0.2000,0.0027\n' +
		'Direct Send ACH Credit, 3563_44, ACH005,,0.1500,0\n' +
		'Direct Send ACH Debit, 3562_45, ACH005,,0.1500,0\n' +
		'Direct Send ACH Return, 3564_46, ACH005,,5.0000,0\n' +
		'Direct Send ACH Reversal, 3565_47, ACH005,,25.0000,0\n' +
		'Direct Send ACH Setup, 3560_48, ACH005,,300.0000,0\n' +
		'Direct Send File Monthly, 3516_49, ACH005,,250.0000,0\n' +
		'Third Party Payroll File, 3514_50, ACH005,,20.0000,0\n' +
		'Direct Deposit File Deletion, 3537_51, ACH006,,10.0000,0\n' +
		'Direct Deposit Item, 3536_52, ACH006,,0.2000,0\n' +
		'Direct Deposit Monthly, 3535_53, ACH006,,15.0000,0\n' +
		'Direct Deposit per file***, NA_54, ACH006,,0.0000,0\n' +
		'Direct Deposit Setup, 3533_55, ACH006,,30.0000,0\n' +
		'Direct Deposit Weekly, 3534_56, ACH006,,25.0000,0\n' +
		'Remote Deposit - Deposit, 3570_57, ACH007,,1.0000,0\n' +
		'Remote Deposit Item, 3541_58, ACH007,,0.0500,0.03\n' +
		'Remote Deposit Monthly per Location, 3539_59, ACH007,,40.0000,0\n' +
		'Remote Deposit Setup, 3538_60, ACH007,,50.0000,0\n' +
		'ACH Return, 3513_61, ACH008,,5.0000,0\n' +
		'International ACH Monthly, 4056_62, ACH008,,20.0000,0\n' +
		'International ACH per Item, 4057_63, ACH008,,5.0000,0\n' +
		'ACH Addenda, 4021_64, ACH008,,0.3000,0\n' +
		'ACH Cash Con w/Addenda, 4014_65, ACH008,,4.0000,0\n' +
		'ACH Cash Concentration, 4013_66, ACH008,,4.0000,0\n' +
		'ACH Monthly, 4010_67, ACH008,,40.0000,0\n' +
		'ACH PPD, 4015_68, ACH008,,5.0000,0\n' +
		'ACH PPD w/Addenda, 4016_69, ACH008,,5.0000,0\n' +
		'ACH Prenotification, 4023_70, ACH008,,3.0000,0\n' +
		'ACH Reversal, 3505_71, ACH008,,25.0000,0\n' +
		'ACH Setup, 4009_72, ACH008,,200.0000,0\n' +
		'ACH Small Business, 4018_73, ACH008,,5.0000,0\n' +
		'ACH Tax Payment, 4017_74, ACH008,,1.0000,0\n' +
		'Originated ACH Credit, 4012_75, ACH008,,0.2000,0\n' +
		'Originated ACH Debit, 4011_76, ACH008,,0.2000,0\n' +
		'Boxes of Rolled Coin, 1105_77, CS001,,4.0000,0\n' +
		'Cash/coin Deposited per $1000, 1010_78, CS001,,1.6000,0\n' +
		'Currency Straps Purchased, 1103_79, CS001,,0.7000,0\n' +
		'Late Coin & Currency Order, 1108_80, CS001,,15.0000,0\n' +
		'Partial Strap per Bill, 1110_81, CS001,,0.2500,0\n' +
		'Rolled Coin Purchased, 1102_82, CS001,,0.1200,0\n' +
		'Controlled Disb Deposit, 1503_83, CD001,,1.0000,0\n' +
		'Controlled Disb Item, 1502_84, CD001,,0.0300,0\n' +
		'Controlled Disb Monthly, 1527_85, CD001,,200.0000,0\n' +
		'Controlled Disb Setup, 1504_86, CD001,,50.0000,0\n' +
		'Cashier\'s checks, NA_87, DS001,,10.0000,0\n' +
		'Certified Checks, NA_88, DS001,,25.0000,0\n' +
		'Check printing, 1038_89, DS001,,0.0000,0\n' +
		'Checks Deposited - Local, 1008_90, DS002,,0.1200,0.038\n' +
		'Checks Deposited - On Us, 1007_91, DS002,,0.1000,0.038\n' +
		'Deposit Adjustment, 1109_92, DS003,,3.0000,0\n' +
		'FDIC Insurance per $1000, 106_93, DS004,,0.1479,0\n' +
		'Inclearing Checks, 1001_94, DS005,,0.1900,0\n' +
		'Negative Balance Interest Charge*, 113_95, DS006,,0.0000,0\n' +
		'Pre-Encoded Checks - Local, 1003_96, DS007,,0.0800,0.038\n' +
		'Pre-Encoded Checks - On Us, 1002_97, DS007,,0.0700,0.038\n' +
		'Pre-Encoded Rejects, 1005_98, DS008,,0.0500,0\n' +
		'Audit Confirmation, 1042_99, DS009,,20.0000,0\n' +
		'Garnishments levies or attachments served on TCF , NA_100, DS009,,125.0000,0\n' +
		'Photocopied requests, NA_101, DS009,,5.0000,0\n' +
		'Postdated/Stale Dated check fee, NA_102, DS009,,35.0000,0\n' +
		'Return Deposited Item, 105_103, DS010,,8.0000,0\n' +
		'Return Item Resubmit, NA_104, DS011,,2.2500,0\n' +
		'Extended Stop Pymt Single, 1017_105, DS012,,40.0000,0\n' +
		'Stop Payment Single, 1016_106, DS012,,35.0000,0\n' +
		'Deposit Services, 1016_107, DS013,,1.0000,0\n' +
		'Repo Sweep Monthly, 4514_109, LS001,,175.0000,0\n' +
		'Repo Sweep Public/Non-Profit, 4512_110, LS001,,100.0000,0\n' +
		'Repo Sweep Setup, 4513_111, LS001,,25.0000,0\n' +
		'Zero Balance Master Account, 4502_116, LS003,,40.0000,0\n' +
		'Zero Balance Sub Account, 4501_117, LS003,,25.0000,0\n' +
		'Account Maintenance, 701_118, LBX001,,155.0000,0\n' +
		'Additional Document, 704_119, LBX001,,0.1300,0\n' +
		'Cash Pmt Processing, 712_120, LBX001,,2.5000,0\n' +
		'Check Only Processing, 709_121, LBX001,,0.4000,0\n' +
		'Correspondence, 705_122, LBX001,,0.0600,0\n' +
		'Creation Per Item, 703_123, LBX001,,0.0600,0\n' +
		'Data Transmission, 710_124, LBX001,,150.0000,0\n' +
		'Incoming Courier Pkg, 708_125, LBX001,,5.0000,0\n' +
		'Keying-Keystrokes, 715_126, LBX001,,0.0140,0\n' +
		'MICR Capture, 706_127, LBX001,,0.0600,0\n' +
		'Minimum Volume Change, 711_128, LBX001,,0.2600,0\n' +
		'Package Prep, 707_129, LBX001,,60.0000,0\n' +
		'Partial Pay Singles, 716_130, LBX001,,0.0500,0\n' +
		'Per Item, 702_131, LBX001,,0.2600,0\n' +
		'Scan Rejects, 714_132, LBX001,,0.1300,0\n' +
		'Account Maintenance, 533_133, LBX002,,160.0000,0\n' +
		'Canadian Check Dep, 578_134, LBX002,,8.0000,0\n' +
		'CD-ROM Maintenance, 553_135, LBX002,,30.0000,0\n' +
		'Deposit, 505_136, LBX002,,1.5000,0\n' +
		'Item Deposited, 501_137, LBX002,,0.1000,0\n' +
		'Item Image, 554_138, LBX002,,0.1000,0\n' +
		'Item Processed, 514_139, LBX002,,0.4000,0\n' +
		'Keystroke, 544_140, LBX002,,0.0080,0\n' +
		'Mailout, 560_141, LBX002,,20.0000,0\n' +
		'Mailout Item, 542_142, LBX002,,0.1500,0\n' +
		'Match, 525_143, LBX002,,0.1500,0\n' +
		'Photocopy, 523_144, LBX002,,0.1500,0\n' +
		'PO Box Rental, 579_145, LBX002,,45.0000,0\n' +
		'Pre-encoded Rejects, 593_146, LBX002,,0.5000,0\n' +
		'Returned Deposited Item, 582_147, LBX002,,6.0000,0\n' +
		'Setup, 512_148,,LBX,100.0000,0\n' +
		'Statement, 526_149, LBX002,,10.0000,0\n' +
		'Unprocessable Item, 536_150, LBX002,,0.4000,0\n' +
		'Walk in Dep, 522_151, LBX002,,5.0000,0\n' +
		'Web Viewer, 567_152, LBX002,,75.0000,0\n' +
		'Bill Payment Monthly, 4060_153, OB001,,10.0000,0\n' +
		'Bill Payment NSF, 4059_154, OB001,,0.0000,0\n' +
		'Bill Payment per Item <11, 4058_155, OB001,,0.0000,0\n' +
		'Bill Payment per Item >10, 4058_156, OB001,,0.7500,0\n' +
		'Bill Payment Photocopy, 4065_157, OB001,,10.0000,0\n' +
		'Bill Payment Research, 4067_158, OB001,,25.0000,0\n' +
		'Bill Payment Stop Payment, 4061_159, OB001,,30.0000,0\n' +
		'CD Orders, 4066_160, OB001,,35.0000,0\n' +
		'E Bills, 4064_161, OB001,,0.0000,0\n' +
		'Scanned Bills, 4062_162, OB001,,1.0000,0\n' +
		'Scraped Bills, 4063_163, OB001,,0.0000,0\n' +
		'Direct File Transfer Monthly, 1051_164, OB002,,75.0000,0\n' +
		'Direct File Transfer Monthly 2nd - 4th Acct, 1051_165, OB002,,75.0000,0\n' +
		'Direct File Transfer Monthly 5th and >, 1051_166, OB002,,75.0000,0\n' +
		'Direct File Transfer Setup, 1049_167, OB002,,100.0000,0\n' +
		'Analysis Stmt Monthly, 1054_168, OB003,,25.0000,0\n' +
		'Analysis Stmt Setup, 1053_169, OB003,,30.0000,0\n' +
		'OB Book Transfer, 4033_170, OB004,,1.0000,0\n' +
		'OB Book Transfer Monthly, 4032_171, OB004,,0.0000,0\n' +
		'OB Book Transfer Setup, 4031_172, OB004,,15.0000,0\n' +
		'Small Business Wire Module, 4025_173, OB005,,10.0000,0\n' +
		'OB Prioir Day Detail, 4047_174, OB005,,0.1000,0\n' +
		'OB Same Day Detail, 4046_175, OB005,,0.0800,0\n' +
		'OB Services - Bundle A, 4070_176, OB005,,15.0000,0\n' +
		'OB Services - Bundle B, 4071_177, OB005,,30.0000,0\n' +
		'OB Services - Bundle C, 4072_178, OB005,,40.0000,0\n' +
		'OB Services - Bundle C; 6 - 10 accounts, 4072_179, OB005,,10.0000,0\n' +
		'OB 90 Day History, 4040_180, OB006,,0.0000,0\n' +
		'OB Info Rpt Prior Day Mo, 4035_181, OB006,,60.0000,0\n' +
		'OB Info Rpt Prior Day Mo 2nd and 3rd Accts, 4035_182, OB006,,45.0000,0\n' +
		'OB Info Rpt Prior Day Mo 4 and <, 4035_183, OB006,,5.0000,0\n' +
		'OB Info Rpt Same Day Mo, 4036_184, OB006,,70.0000,0\n' +
		'OB Info Rpt Same Day Mo 2nd and 3rd Accts, 4036_185, OB006,,50.0000,0\n' +
		'OB Info Rpt Same Day Mo 4th and <, 4036_186, OB006,,10.0000,0\n' +
		'OB Info Rpt Setup, 4034_187, OB006,,50.0000,0\n' +
		'OB Prioir Day Detail, 4047_188, OB006,,0.1000,0\n' +
		'OB Prior Day Summary, 4048_189, OB006,,0.0000,0\n' +
		'OB Same Day Detail, 4046_190, OB006,,0.0800,0\n' +
		'OB Same Day Summary, 4045_191, OB006,,0.0000,0\n' +
		'OB Loan Module, 4069_192, OB007,,25.0000,0\n' +
		'OB Security Services Monthly, 4049_193, OB008,,1.5000,0\n' +
		'OB Security Token, 4042_194, OB009,,50.0000,0\n' +
		'OB Extended Stop Pymt Range, 4007_195, OB010,,40.0000,0\n' +
		'OB Extended Stop Pymt Single, 4006_196, OB010,,16.0000,0\n' +
		'OB Release Stop Payment, 4008_197, OB010,,8.0000,0\n' +
		'OB Stop Payment Monthly, 4002_198, OB010,,0.0000,0\n' +
		'OB Stop Payment Range, 4005_199, OB010,,20.0000,0\n' +
		'OB Stop Payment Setup, 4001_200, OB010,,15.0000,0\n' +
		'OB Stop Payment Single, 4003_201, OB010,,12.0000,0\n' +
		'RDI Alternate Account, 1020_202, RDIP001,,0.0000,0\n' +
		'RDI Alternative Address, 1024_203, RDIP001,,2.0000,0\n' +
		'RDI Calls (Disp), 1028_204, RDIP001,,5.0000,0\n' +
		'RDI Calls (Info), 1027_205, RDIP001,,5.0000,0\n' +
		'RDI Fax Notification, 1029_206, RDIP001,,1.0000,0\n' +
		'RDI Faxed Advise Copies, 1030_207, RDIP001,,1.0000,0\n' +
		'RDI Monthly Fax Service Fee, 1033_208, RDIP001,,20.0000,0\n' +
		'RDI Non Standard Advise, 1026_209, RDIP001,,0.0000,0\n' +
		'RDI Non Standard Redeposit, 1022_210, RDIP001,,0.0000,0\n' +
		'RDI RCK Monthly, 1032_211, RDIP001,,20.0000,0\n' +
		'RDI RCK Redeposit, 1023_212, RDIP001,,1.0000,0\n' +
		'RDI Setup, 1035_213,,RDIP,20.0000,0\n' +
		'RDI Special Instructions, 1034_214, RDIP001,,25.0000,0\n' +
		'RDI Standard Redeposit, 1021_215, RDIP001,,2.0000,0\n' +
		'RDI Web Viewing Monthly, 1031_216, RDIP001,,0.0000,0\n' +
		'Forced Wire Transfer Wd, 3532_217, WS001,,25.0000,0\n' +
		'Wire Incoming, 3508_229, WS003,,15.0000,2.2455\n' +
		'Wire Incoming (11 - 20 ), 3508_230, WS003,,15.0000,0\n' +
		'Wire Incoming (21 and >), 3508_231, WS003,,15.0000,0\n' +
		'Incoming Wire Notification, 3545_233, WS005,,4.0000,0\n' +
		'Wire Fax Notice In/Out, 3547_234, WS005,,20.0000,0\n' +
		'Wire Fax Notification, 3546_235, WS005,,4.0000,0\n' +
		'Wire Notice Online Monthly, 3550_236, WS005,,20.0000,0\n' +
		'Wire Notification E-mail, 3552_237, WS005,,2.0000,0\n' +
		'Wire Manual Outgoing Domestic, 3509_238, WS006,,40.0000,0\n' +
		'Wire Manual Outgoing Int\'l, 3510_239, WS006,,50.0000,0';
}