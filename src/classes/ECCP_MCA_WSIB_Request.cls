/*******************************************************************************************************************************************
* @name         :   ECCP_MCA_WSIB_Request
* @description  :   This class holds the request variables coming from MCA
* @author       :   Nagarjun
* @createddate  :   18/03/2016
*******************************************************************************************************************************************/
global class ECCP_MCA_WSIB_Request{
    
    public ECCP_MCA_WSIB_Request(){
    }
    webservice String ApplNbr;
    webservice String AcctNbr;
    webservice String CIURefNbr;
    webservice String CIURefSeqNbr;
    webservice String MCIPassFail;
    webservice String RegOPassFail;
    webservice String RegWPassFail;
    webservice String PA326Pass;
    webservice String OFACPassFail;
    webservice String HotFIlePassFail;
    webservice String FAPassFail;
    webservice String ErrorTp;
    webservice String ErrorCd; 
    // Added by Aditya
    webservice Boolean MCAPassPendResponseFlag;
    webservice String errorDescription;
   
}