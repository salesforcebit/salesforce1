@isTest
public class TRO_AsyncNewSetupRequest_Mock implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        // Create response element from the autogenerated class.
        // Populate response element.
        // Add response element to the response parameter, as follows:
        AsyncTRONewSetupRequest_v2.processNewSetUpAPIResponseFuture responseElement = TROUnitTestFactory.buildTestprocessNewSetUpAPIResponseFuture();
        //responseElement.processCSULResponse.CSOAutomation.KTTResponse.status = 'Good to go';
        response.put('response_x', responseElement);
   }
}