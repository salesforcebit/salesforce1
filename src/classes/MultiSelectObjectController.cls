public with sharing class MultiSelectObjectController 
{
        public String listcount { get; set; }
        public List<KEF_Product__c > allFileList {get; set;}
        public string valueId {get;set;}   
        public List<SelectOption> Filelist{get; set;}
        public List<KEF_Product__c > showlist{get;set;}
        public boolean showResults{get;set;}
        //public string listtsb{get;set;}
        public string Aggname {get;set;}
        //public string objectname{get;set;}
        public boolean showerror{get;set;}
        public List<KEF_Product__c> InsertProduct = new List<KEF_Product__c>() ;  
        
        public MultiSelectObjectController(ApexPages.StandardController controller)
        {
            showlist= new list<KEF_Product__c >();
            showResults=false;
            showerror=false;
            valueId = System.currentPagereference().getParameters().get('id');
            if(valueId != null  && valueId .trim() != '')
            {
                try
                {
                        Agreement__c objagg= [select Agreement_Name__c from Agreement__c where id=:valueId limit 1];
                        Aggname = string.valueof(objagg.Agreement_Name__c);
                        system.debug('Aggname ----'+Aggname );
                }
                catch(exception e)
                {
                        system.debug('e--->'+e);             
                }
            }
            
            fetchExistingProducts();
            Filelist = new List<SelectOption>() ;
            allFileList = new list<KEF_Product__c >();
            for(integer i=1;i<6;i++)
            {
                Filelist.add(new SelectOption(String.valueOf(i) , String.valueOf(i))) ; 
            }
            addProduct(5);
        }

        public void addProduct(Integer addpro)
        {
            if(addpro!= null)
            {
                for(integer i = 0; i < addpro; i ++)
                {
                allFileList.add(new KEF_Product__c());        
                }
            }
        }
   
        public PageReference Add() 
        {
            //system.debug('show value----'+listtsb);
            KEF_Product__c  objpro= new KEF_Product__c ();
                if(listcount == null || (listcount != null && listcount.trim() == ''))
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,system.label.Add_Product));
                }      
                else if(allFileList != null)
                {
                    for(Integer i = 1 ; i <= Integer.valueOf(listcount) ; i++)
                    {
                    allFileList.add(new KEF_Product__c  ()) ;
                    }
                }
            listcount =null ;
            return null;        
        }
  
        public  Void save()
        {
        
        if(valueId == null || valueId == '')
        {  
             ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,system.label.Record_association));
        }  

        
            
            for(KEF_Product__c objproduct : allFileList)  
            {  
                if(objproduct.product__c!=null)
                {
                    objproduct.Agreement__c= valueId ;
                    KEF_Product__c  prokef= objproduct .clone();
                    InsertProduct.add(prokef);
                }               
            }  
        
        if(InsertProduct.size() > 0)  
        { 
                Database.SaveResult[] srList = Database.insert(InsertProduct , false);
                system.debug('product value--'+InsertProduct );
                listcount=''; 
                Integer listSize = allFileList.size();                
                allFileList = new List<KEF_Product__c >();
                addProduct(listSize); 
                  
                List<String> msgList = new List<String>();
                list<string>sucesslist=new list<string>();
                for (Database.SaveResult sr : srList)
                {
                    if (sr.isSuccess())
                    {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted product. product ID: ' + sr.getId());
                        sucesslist.add(string.ValueOf(sr.getid()));
                        showerror=false;
                    }
                    else
                    {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors())
                        {
                            System.debug('The following error has occurred.'+err);                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account fields that affected this error: ' + err.getFields());
                            msgList.add(string.ValueOf(err.getMessage()));
                    }
                        showerror=true;
                    }
                }
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, sucesslist.size() +' product(s) added sucessfully')); 
        
                    System.Debug('msgList size:'+msgList.size());
                    List<String> idStr = new List<String>();
                for(String str : msglist )
                {
                        System.Debug('str size:'+str);
                    List<String> strSplit = new List<String>();
                    if(str.contains(system.label.duplicate_value_found))
                    {
                        strSplit = str.split(':');
                        System.debug('Split Value:'+strSplit[2]);
                        idStr.add(strSplit[2].trim());
                    }
                    else
                    {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,str ));
                }
                }
                List<KEF_Product__c>  kefNameList = [select id,Product__c,Product__r.name from KEF_Product__c  WHERE id =: idStr];
                System.debug('kefNameList: '+kefNameList);
                String strSobjects = '';
                for(integer i=0;i<kefNameList.size();i++)
                {
                    //if(kefNameList.size()>1)
                    {            
                       strSobjects = strSobjects + kefNameList [i].Product__r.name+ ', ';
                       
                       system.debug('string length-----'+strSobjects.length());                       
                    }
                    /*else
                    {
                       strSobjects = strSobjects + kefNameList [i].Product__r.name;
                    }*/
                system.debug('strSobjects --'+strSobjects );
                }
                String fieldNames = '';
                if(strSobjects !=null && strSobjects!='' && strSobjects.length() >= 2)
                {
                    fieldNames = strSobjects.substring(0,strSobjects.length()-2);
                }
                
                if(fieldNames !=null && fieldNames!='')
                {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Duplicate values found with KEF Product Record with Name(s): ' + fieldNames));
                }
                }
        else
        {
           ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, system.label.select_at_least_one_Product));    
           showerror = true;    
        }
        
        //clearing list
        InsertProduct = new List<KEF_product__c>();
        
        fetchExistingProducts(); 
              
    }             
    
        public void fetchExistingProducts()
        {
        showlist= new list<KEF_Product__c >();
        system.debug('list--'+showlist);

        for(KEF_Product__c  prod: [select id,Name,Product__c,Product_Category__c,Product_Family__c ,CreatedById, CreatedDate
                                        from KEF_Product__c  
                                        where Agreement__c=:valueId 
                                        order by CreatedDate desc ])
        {
                if(prod!=null)
                {
                    showResults=true;
                }
                showlist.add((prod));
                system.debug('showlist--'+showlist); 
        }

        if(showlist.size() == 0)
        {
            showResults = false;
        }
        }
      
        /*public PageReference save()
        {
          SaveProductdetails(); 
          return null;
        }*/
        
    public PageReference cancel() 
    {
        PageReference aggrementpage= new PageReference('/'+valueId );  
        aggrementpage.setRedirect(true);
        return aggrementpage ;
        }
    
    public PageReference save2() 
    {
     //if(InsertProduct .size() > 0)
     {  
        save();
        if(showerror == true)
        {
            return null;
        }        
        else
        { 
            if(showerror == false)
            {
                PageReference aggrementpage= new PageReference('/'+valueId );  
                aggrementpage.setRedirect(true);
                return aggrementpage ;  
            }
          
           
            }
        }
        /*
         else
         {
             ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, system.label.select_at_least_one_Product));
         } */
         return null;
    }
}