@istest
Public class Test_Triggeruser
{
  public static testMethod void DerivativeUsertest() 
    {
        Profile p = [select id from profile where name='System Administrator'];
        
        User testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharanza@keybank.com',
        emailencodingkey='UTF-8', lastname='Sag2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharanxq@keybank.com',PLOB__c='support',
        PLOB_Alias__c='support', RACF_Number__c ='HINDF', Department ='CB KBCM Derivatives');             
        insert testUser;
        
        User testUser1 = new User(alias = 'u1', email='vidhyasagaran_muralidharanzab@keybank.com',
        emailencodingkey='UTF-8', lastname='Sag12015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharanxq1@keybank.com',PLOB__c='support',
        PLOB_Alias__c='support', RACF_Number__c ='HINDF1', Department ='CB KBCM Derivatives');             
        insert testUser1;
        
        Group g=[select Id from Group Where DeveloperName='Derivative_Users'];
        
        if(testUser.Department =='CB KBCM Derivatives')
        {
            GroupMember grpmem = new GroupMember(GroupId = g.id, UserOrGroupId = testuser.id);
            insert grpmem;
            GroupMember grpmem1 = new GroupMember(GroupId = g.id, UserOrGroupId = testuser1.id);
            insert grpmem1;
        }
        
        testuser1.Department = 'KTO ETS EDW/ESA SUPPORT';
        update testUser1;
        
        if(testUser.Department !='CB KBCM Derivatives')
        {
            List<GroupMember> gm = [SELECT GroupId,Id,UserOrGroupId FROM GroupMember WHERE UserOrGroupId =:testuser1.id]; 
            Delete gm;
        }
        
    }
}