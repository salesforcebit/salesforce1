@isTest
private class ctlr_OppSelectPricingTest {
	
	@isTest static void initializeTest() {
		// Implement test code
		Opportunity opp = [SELECT id FROM Opportunity limit 1];

		//startTest
		Test.startTest();

		ctlr_OppSelectPricing ctr = new ctlr_OppSelectPricing( new ApexPages.StandardController(opp) );
		ctr.specificStandPric();


		Test.stopTest();

		//assertions
		System.assertEquals(true, ctr.showSpecificPric);
		System.assertEquals(true, ctr.depList.size() > 0);
	}

	@isTest static void createDepTasksTest() {
		// Implement test code
		Opportunity opp = [SELECT id FROM Opportunity limit 1];
		Map<Id, LLC_BI__Deposit__c> id2DepMap = new Map<Id, LLC_BI__Deposit__c>([SELECT id FROM LLC_BI__Deposit__c limit 2]);

		//startTest
		Test.startTest();

		ctlr_OppSelectPricing ctr = new ctlr_OppSelectPricing( new ApexPages.StandardController(opp) );
		//ctr.selectedIds.addAll( id2DepMap.keyset() );
		ctr.depList[0].isSelected = true;
		ctr.depList[1].isSelected = false;
		ctr.depList[1].isSelected = true;
		Boolean selectedId = ctr.depList[0].isSelected;
		ctr.createDepTasks();

		Test.stopTest();

		//requery
		List<TSO_Setup_Task__c> taskList = [SELECT id FROM TSO_Setup_Task__c];

		//assertions
		System.assertEquals(true, selectedId);
		System.assertEquals(2, taskList.size() );
	}

	@isTest static void relationStandPricTest() {
		// Implement test code
		Opportunity opp = [SELECT id FROM Opportunity limit 1];
		Map<Id, LLC_BI__Deposit__c> id2DepMap = new Map<Id, LLC_BI__Deposit__c>([SELECT id FROM LLC_BI__Deposit__c limit 2]);

		//startTest
		Test.startTest();

		ctlr_OppSelectPricing ctr = new ctlr_OppSelectPricing( new ApexPages.StandardController(opp) );
		//ctr.selectedIds.addAll( id2DepMap.keyset() );
		ctr.relationStandPric();

		Test.stopTest();

		//requery
		List<TSO_Setup_Task__c> taskList = [SELECT id FROM TSO_Setup_Task__c];

		//assertions
		System.assertEquals(1, taskList.size() );
	}
	
	@testSetup static void testSetupMethod() {
		// Implement test code
		Profile systemAdmin = [SELECT id FROM Profile WHERE Name='System Administrator'];
        
        User u = new User();
        u.LastName = 'testLast';
        u.Email = 'testLast@aol.com';
        u.alias = 'testLast';
        u.ProfileId = systemAdmin.id;
        u.Username = 'test123sdfs@aol.com';
        u.CommunityNickname = 'ttompk25';
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'en_US';
        u.PLOB__c = 'jsnisa';
        insert u;

        Group g = new Group (name='RPM Operations Queue', type='Queue');
        Group g2 = new Group (name='Exception Pricing', type='Queue');
        insert new List<Group>{g, g2};

        QueueSObject q = new QueueSObject(SobjectType='TSO_Setup_Task__c', QueueId=g.Id);
		insert q;


        System.runAs(u){
			Account acc = new Account(Name='test Acc');
			insert acc;

			Opportunity opp = new Opportunity(Name='testOpp', AccountId = acc.Id, StageName='New', CloseDate = Date.today().addDays(30) );
			insert opp;

			LLC_BI__Deposit__c dep = UnitTestFactory.buildTestDeposit(acc.Id);
			LLC_BI__Deposit__c dep2 = UnitTestFactory.buildTestDeposit(acc.Id);
			LLC_BI__Deposit__c dep3 = UnitTestFactory.buildTestDeposit(acc.Id);
			LLC_BI__Deposit__c dep4 = UnitTestFactory.buildTestDeposit(acc.Id);
			LLC_BI__Deposit__c dep5 = UnitTestFactory.buildTestDeposit(acc.Id);
			insert new List<LLC_BI__Deposit__c>{dep,dep2,dep3,dep4,dep5};
		}
	}
	
}