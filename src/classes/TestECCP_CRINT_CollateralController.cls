@isTest
public class TestECCP_CRINT_CollateralController {
    
    @isTest
    static void start() {
        Account testAccount = createAccount();
        LLC_BI__Loan_Collateral_Aggregate__c testAggregate = createAggregate();
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testAggregate.Id);
        LLC_BI__Collateral_Type__c testCollateralType = createCollateralType();
        LLC_BI__Collateral__c testCollateral = createCollateral(testCollateralType.Id);
        LLC_BI__Loan_Collateral2__c testCollateralPledged = createCollateralPledged(testAggregate.Id, testCollateral.Id, testLoan.Id);
        
        ApexPages.currentPage().getParameters().put('id', testLoan.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CRINT_CollateralController eccpccc = new ECCP_CRINT_CollateralController(controller);
    }
    
    @isTest
    static void viewCollateral() {
        Account testAccount = createAccount();
        LLC_BI__Loan_Collateral_Aggregate__c testAggregate = createAggregate();
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testAggregate.Id);
        LLC_BI__Collateral_Type__c testCollateralType = createCollateralType();
        LLC_BI__Collateral__c testCollateral = createCollateral(testCollateralType.Id);
        LLC_BI__Loan_Collateral2__c testCollateralPledged = createCollateralPledged(testAggregate.Id, testCollateral.Id, testLoan.Id);
        
        ApexPages.currentPage().getParameters().put('id', testLoan.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CRINT_CollateralController eccpccc = new ECCP_CRINT_CollateralController(controller);
        
        Test.startTest();
        
        eccpccc.collateralTypeId = testcollateralType.Id;
        eccpccc.collateralId = testCollateral.Id;
        eccpccc.viewCollateral();
        
        Test.stopTest();
    }
    
    @isTest
    static void back() {
        Account testAccount = createAccount();
        LLC_BI__Loan_Collateral_Aggregate__c testAggregate = createAggregate();
        LLC_BI__Loan__c testLoan = createLoan(testAccount.Id, testAggregate.Id);
        LLC_BI__Collateral_Type__c testCollateralType = createCollateralType();
        LLC_BI__Collateral__c testCollateral = createCollateral(testCollateralType.Id);
        LLC_BI__Loan_Collateral2__c testCollateralPledged = createCollateralPledged(testAggregate.Id, testCollateral.Id, testLoan.Id);
        
        ApexPages.currentPage().getParameters().put('id', testLoan.Id);
        nForce.TemplateController controller = new nForce.TemplateController();
        ECCP_CRINT_CollateralController eccpccc = new ECCP_CRINT_CollateralController(controller);
        
        Test.startTest();
        
        eccpccc.collateralTypeId = testcollateralType.Id;
        eccpccc.collateralId = testCollateral.Id;
        eccpccc.viewCollateral();
        eccpccc.back();
        
        Test.stopTest();
    }
    
    private static Account createAccount() {
        Account newA = new Account(
            Name = 'Test');
        
        insert newA;
        return newA;
    }
    
    private static LLC_BI__Loan_Collateral_Aggregate__c createAggregate() {
        LLC_BI__Loan_Collateral_Aggregate__c newLCA = new LLC_BI__Loan_Collateral_Aggregate__c(
            Name = 'Test');
        
        insert newLCA;
        return newLCA;
    }
    
    private static LLC_BI__Loan__c createLoan(Id acctId, Id loanCollateralAggregateId) {
        LLC_BI__Loan__c newL = new LLC_BI__Loan__c(
            Name = 'Test',
            LLC_BI__Account__c = acctId,
            LLC_BI__Loan_Collateral_Aggregate__c = loanCollateralAggregateId);
        
        insert newL;
        return newL;
    }
    
    private static LLC_BI__Collateral_Type__c createCollateralType() {
        LLC_BI__Collateral_Type__c newCT = new LLC_BI__Collateral_Type__c(
            Name = 'Test', 
            LLC_BI__Type__c = 'Test', 
            LLC_BI__Subtype__c = 'Test', 
            LLC_BI__Collateral_Code__c = '400',
            CRINT_Field_Set__c = 'CRINT_Collateral_Boat');
        
        insert newCT;
        return newCT;
    }
    
    private static LLC_BI__Collateral__c createCollateral(Id collateralTypeId) {
        LLC_BI__Collateral__c newC = new LLC_BI__Collateral__c(
            LLC_BI__Collateral_Type__c = collateralTypeId);
        
        insert newC;
        return newC;
    }
    
    private static LLC_BI__Loan_Collateral2__c createCollateralPledged(Id loanCollateralAggregateId, 
                                                                       Id collateralId, 
                                                                       Id loanId) {
        LLC_BI__Loan_Collateral2__c newCP = new LLC_BI__Loan_Collateral2__c(
            LLC_BI__Loan_Collateral_Aggregate__c = loanCollateralAggregateId, 
            LLC_BI__Collateral__c = collateralId, 
            LLC_BI__Loan__c = loanId,
            LLC_BI__Lien_Position__c = '1st',
            LLC_BI__Amount_Pledged__c = 10000,
            LLC_BI__Active__c = true
            
            );
        
        insert newCP;
        return newCP;
    }
}