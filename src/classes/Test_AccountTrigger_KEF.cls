@isTest
private class Test_AccountTrigger_KEF 
{
    static testMethod void vendorSupplierRoleTest()
    {
        List<Account> accList = TestUtilityClass.CreateTestAccount(2);
        insert accList;
        
        Account acc = new Account();
        acc = accList[0];       
        acc.KEF_Risk_Role__c = System.Label.Vendor_Supplier;
        acc.Vendor_Status__c = System.Label.Pending_Credit;
        update acc;
        
        Account acc2 = new Account();
        acc2 = accList[1];
        acc2.KEF_Risk_Role__c = System.Label.Supplier_Only;
        acc2.Supplier_Status__c = System.Label.Inactive;
        acc2.Supplier_Inactive_Reason__c = 'A Reason';
        acc2.Final_Date_for_Inflight_deals_Supplier__c = system.today();
        update acc2;
        
        acc2.KEF_Risk_Role__c = System.Label.Vendor_Supplier;
        acc2.Vendor_Status__c = System.Label.Pending_Credit;
        update acc2;
        
       //system.assertEquals(true, [select Is_Vendor__c from Account where Id = :acc2.id].Is_Vendor__c);
    }
    
    static testMethod void nextReviewDateTest()
    {
        User creditUser = TestUtilityClass.createUser(System.label.KEF_Credit);
        insert creditUser;
        
        User sysAdminUser = TestUtilityClass.createUser(System.label.System_Administrator);
        insert sysAdminUser;
        
        Account acc = new Account();
        List<Account> accList = new List<Account>();
        
        system.runas(creditUser)
        {           
            accList = TestUtilityClass.CreateTestAccount(1);
            insert accList;
            
            acc = accList[0];
            
            acc.KEF_Risk_Role__c = System.Label.Vendor_Supplier;
            acc.Vendor_Status__c = System.Label.Pending_Credit;
            update acc;
        }
        
        system.runas(sysAdminUser)
        {   
            Funding_Grid__c fg = TestUtilityClass.createFundingGrid(accList[0].id);
            insert fg;
        }
        
        system.runas(creditUser)
        {   
            acc.Vendor_Status__c = System.Label.Active;
            acc.Credit_Analyst_Approving_Vendor__c = creditUser.id;//UserInfo.getUserId();
            update acc;
            
            //system.assertEquals(System.Today().addYears(1), [select Next_review_date__c from Account where Id = :acc.id].Next_review_date__c);
        }
        
    }
    
    static testMethod void integrationPicklistUpdateTest()
    {
        User creditUser = TestUtilityClass.createUser(System.label.KEF_Credit);
        insert creditUser;
        
        User sysAdminUser = TestUtilityClass.createUser(System.label.System_Administrator);
        insert sysAdminUser;
        
        system.runAs(sysAdminUser)
        {        
            Account acc = new Account();
            List<Account> accList = new List<Account>();
            
            accList = TestUtilityClass.CreateTestAccount(1);
            insert accList;
                
            acc = accList[0];
                
            acc.KEF_Risk_Role__c = System.Label.Vendor_Supplier;
            acc.Vendor_Status__c = System.Label.Pending_Credit;
            update acc;
            
            acc.Vendor_Status__c = System.Label.Active;
            acc.Next_Review_Date__c = system.today() + 7;
            acc.Credit_Analyst_Approving_Vendor__c = creditUser.id;//UserInfo.getUserId();
            update acc;
            
            Agreement__c agr = new Agreement__c();
            agr = TestUtilityClass.CreateTestAgreement();
            agr.Agreement_Name_UDF__c = 'Test UDF Value';        
            agr.Agreement_Term__c = 12;
            agr.Agreement_Commencement_Date__c = System.today().addDays(2);        
            insert agr;
            
            Payoff_Quote__c payoffQuote = new Payoff_Quote__c();
            payoffQuote = TestUtilityClass.createPayoffQuote(agr.id);
            insert payoffQuote;
            
            agr.Agreement_Status__c = System.Label.Active;
            agr.Status_Change_Comments__c = 'These are some test comments';
            update agr;
            
            Vendor_Agreement__c vaRec = new Vendor_Agreement__c();
            vaRec = testUtilityClass.createVAAssociations(acc.id, agr.id);
            insert vaRec;
            
            Test.Starttest();
            
            acc.Name = 'Changing the name';
            update acc;
            
            
            
            try
            {
                delete acc;
            }
            catch(DMLException dmlEx)
            {
                system.debug('dmlEx.getDMLMessage>>>>      '+dmlEx);
                System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION,dmlEx.getDmlType(0));
            }
            
            Test.stopTest();
        }
    } 
    
    static testMethod void integratedAccDeleteTest()
    {
        List<Account> accList = new List<Account>();
            
        accList = TestUtilityClass.CreateTestAccount(2);
        insert accList;
            
        Account acc1 = accList[0];
        acc1.Integrated__c = true;
        update acc1;
        
        /*preparing child data for second inserted account */ 
        Account acc2 = accList[1];
        acc2.KEF_Risk_Role__c = System.Label.Vendor_Supplier;
        acc2.Vendor_status__c = System.Label.Pending_Credit;
        update acc2;      
        
        Agreement__c agr = new Agreement__c();
        agr = TestUtilityClass.CreateTestAgreement();
        agr.Agreement_Name_UDF__c = 'UDF' + Integer.valueOf(math.random() * 100);
        insert agr;  
        //associating the agreement and account records together. 
        insert TestUtilityClass.createVAAssociations(acc2.id, agr.Id);
        
        test.startTest();
        
        try
        {
            delete accList;
        }
        catch(DMLException dmlEx)
        {
            system.debug('dmlEx.getDMLMessage>>>>      '+dmlEx);
            System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION,dmlEx.getDmlType(0));
        }
        
        Test.stopTest();        
    }
    
    static testMethod void childOppAgrAccountDeleteTest()
    {
        Account acc = new Account();
        List<Account> accList = new List<Account>();
            
        accList = TestUtilityClass.CreateTestAccount(2);
        insert accList;
            
        /*preparing child data for First inserted Account */
        Account acc1 = accList[0];
        acc1.KEF_Risk_Role__c = System.Label.Supplier_Only;
        acc1.Supplier_Status__c = System.Label.Pending_Credit;
        update acc1;
        /* inserting opportunity for first account */
        Opportunity oppRec1 = TestUtilityClass.createOpportunity(acc1.id);
        insert oppRec1;
        
        /*preparing child data for second inserted account */ 
        Account acc2 = accList[1];
        acc2.KEF_Risk_Role__c = System.Label.Vendor_Supplier;
        acc2.Vendor_status__c = System.Label.Pending_Credit;
        update acc2;      
        
        Agreement__c agr = new Agreement__c();
        agr = TestUtilityClass.CreateTestAgreement();
        agr.Agreement_Name_UDF__c = 'Test UDF Value';
        insert agr;  
        //associating the agreement and account records together. 
        insert TestUtilityClass.createVAAssociations(acc2.id, agr.Id);
        //inserting opportunity as well for he agreement.
        Opportunity oppRec2 = TestUtilityClass.createOpportunity(acc2.id);
        insert oppRec2;
        
        test.startTest();
        
        try
        {
            delete accList;
        }
        catch(DMLException dmlEx)
        {
            system.debug('dmlEx.getDMLMessage>>>>      '+dmlEx);
            System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION,dmlEx.getDmlType(0));
        }
        
        Test.stopTest();        
    } 
}