public class KEFReportFieldPopulator 
{
	public string objectName;
	//api name for the field which has changed 
	public String fieldAPIName;
	//currently selected changed status value  
	public String changedPicklistValue;
	//api name of the date time field which is to be populated
	public Boolean populateDateTimeValue;
	public String dateTimeFieldAPIName;
	//api name of the User Lookup field which is to be populated
	public boolean populateUserLookupValue; 
	public String userLookupFieldAPIName;
	//denotes if field value is changed
	public boolean isPicklistValueToCompare; 
	/*
	public KEFReportFieldPopulator(String fieldAPI, String dtFieldAPI, String userAPI)
	{ 
		fieldAPIName = fieldAPI;
		dateTimeFieldAPIName = dtFieldAPI;
		populateDateTimeValue = populatedt;
		userLookupFieldAPIName = userAPI;
		populateUserLookupValue = populateUser;
	}*/
	
	public KEFReportFieldPopulator(String fieldAPI, String statusValue, Boolean populatedt, String dtFieldAPI, Boolean populateUser, String userAPI)
	{ 
		fieldAPIName = fieldAPI;
		changedPicklistValue = statusValue;
		populateDateTimeValue = populatedt;
		dateTimeFieldAPIName = dtFieldAPI;
		populateUserLookupValue = populateUser;
		userLookupFieldAPIName = userAPI;
	}
}