@isTest
private class TROAddServiceRequest_UT {

    static testMethod void addServiceRequestTest() {
    	TROAddServiceRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_addServiceRequest_Port testAddService = new TROAddServiceRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_addServiceRequest_Port();

    	TROAddServiceRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_AddService_ExportFields();

    	Test.setMock(WebServiceMock.class, new TRO_AddServiceRequest_Mock());

			Test.startTest();
    		TROAddServiceRequest_v2.KTTResponse response = testAddService.processAddServiceAPI(exportFields);
			Test.stopTest();

			System.assertEquals(response.CSOAutomation.KTTResponse.status, 'Good to go');

    }

    static testMethod void aSyncAddServiceRequestTest() {
        AsyncTROAddServiceRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_addServiceRequest_Port aSyncAddService = new AsyncTROAddServiceRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_addServiceRequest_Port();

        TROAddServiceRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_AddService_ExportFields();
        Continuation con = new Continuation(120);
        con.continuationMethod = 'processResponse';

        Test.setMock(WebServiceMock.class, new TRO_AsyncAddServiceRequest_Mock());

        AsyncTROAddServiceRequest_v2.processAddServiceAPIResponseFuture response = aSyncAddService.beginProcessAddServiceAPI(con, exportFields);

    }

}