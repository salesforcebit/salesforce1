//Generated by wsdl2apex

public class AsyncECCP_DSCCustomer {
    public class getPreLoadDataResponseFuture extends System.WebServiceCalloutFuture {
        public ECCP_DSCCustomer.PreLoadResponseDT getValue() {
            ECCP_DSCCustomer.getPreLoadDataResponse response = (ECCP_DSCCustomer.getPreLoadDataResponse)System.WebServiceCallout.endInvoke(this);
            return response.PreLoadResponseDT;
        }
    }
    public class AsyncECCnCinoPreload_WebServices_Provider_getPreLoadDataWS_Port {
        public String endpoint_x = 'https://sdc01dwbmis01.keybank.com:9055/ws/ECCnCinoPreload.WebServices.Provider.getPreLoadDataWS/ECCnCinoPreload_WebServices_Provider_getPreLoadDataWS_Port';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://soai.keybank.com/getPreLoadDataWS', 'ECCP_DSCCustomer'};
        public AsyncECCP_DSCCustomer.getPreLoadDataResponseFuture beginGetPreLoadData(System.Continuation continuation,String[] SSN_TIN,String nAppID,String MDMId) {
            ECCP_DSCCustomer.getPreLoadData request_x = new ECCP_DSCCustomer.getPreLoadData();
            request_x.SSN_TIN = SSN_TIN;
            request_x.nAppID = nAppID;
            request_x.MDMId = MDMId;
            return (AsyncECCP_DSCCustomer.getPreLoadDataResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncECCP_DSCCustomer.getPreLoadDataResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              'ECCnCinoPreload_WebServices_Provider_getPreLoadDataWS_Binder_getPreLoadData',
              'http://soai.keybank.com/getPreLoadDataWS',
              'getPreLoadData',
              'http://soai.keybank.com/getPreLoadDataWS',
              'getPreLoadDataResponse',
              'ECCP_DSCCustomer.getPreLoadDataResponse'}
            );
        }
    }
}