public without sharing class ScenarioItemTriggerHandler {


    public static void handleBeforeUpdate(Map<Id, LLC_BI__Scenario_Item__c> newMap, Map<Id, LLC_BI__Scenario_Item__c> oldMap) {
        volumesNullCheck(newMap.values());
    }
    public static void handleBeforeInsert(List<LLC_BI__Scenario_Item__c> newList) {
        volumesNullCheck(newList);
    }

    /**
     * 11/21/2016
     * check to see if any volume fields have been changed from 0 to null.  if they have, change it to 0. Need a value for workflows to work.    
     */
    public static void volumesNullCheck(List<LLC_BI__Scenario_Item__c> newList) {
        for(LLC_BI__Scenario_Item__c item : newList){
            if(item.LLC_BI__Volumes__c == null) {
                item.LLC_BI__Volumes__c = 0;
            }
        }
    }


  public void handleAfterUpdate(Map<ID, LLC_BI__Scenario_Item__c> siNewMap, Map<ID, LLC_BI__Scenario_Item__c> siOldMap) {
    Set<Id> ScenarioProductIds = new Set<Id>();
    for(Id si : siNewMap.keyset()){
      if(siNewMap.get(si).Scenario_Product__c != null
        || siNewMap.get(si).LLC_BI__Volumes__c != siOldMap.get(si).LLC_BI__Volumes__c
        || siNewMap.get(si).Actual_Price__c != siOldMap.get(si).Actual_Price__c)
      {
        ScenarioProductIds.add(siNewMap.get(si).Scenario_Product__c);
      }
    }

    Map<Id,Scenario_Product__c> scenarioProductMap = new Map<Id,Scenario_Product__c>();

    AggregateResult[] groupedResults
      = [SELECT
            Scenario_Product__c,
            Sum(Fee_Equivalent_Revenue2__c) Fee_Equivalent_Revenue__c,
            Sum(Annual_Net_Revenue_Variable_Cost__c) Annual_Net_Revenue_Variable_Cost__c,
            Bookable_Opportunity_Revenue__c
        FROM LLC_BI__Scenario_Item__c where Scenario_Product__c in :ScenarioProductIds
        GROUP BY Scenario_Product__c,Bookable_Opportunity_Revenue__c
        ORDER BY Scenario_Product__c,Bookable_Opportunity_Revenue__c];
    for (AggregateResult ar : groupedResults)  {
      Id ScenarioProductId = (Id)ar.get('Scenario_Product__c');
      if(ScenarioProductId == null) continue;
      
      Boolean BookableOpportunityRevenue = (Boolean)ar.get('Bookable_Opportunity_Revenue__c');

      if( scenarioProductMap.get(ScenarioProductId) == null)
        scenarioProductMap.put(ScenarioProductId, new Scenario_Product__c(Id=ScenarioProductId));

      Scenario_Product__c tmpScenarioProduct = scenarioProductMap.get(ScenarioProductId);

//      Fee_Equivalent_Revenue2__c                  Fee_Equivalent_Revenue__c
//      Annual_Net_Revenue_Variable_Cost__c         Annual_Net_Revenue_Variable_Cost__c
//      Fee_Equivalent_Revenue2__c[bookable=true]   Total_Bookable_Product_Revenue__c

      tmpScenarioProduct.Fee_Equivalent_Revenue__c = nullToZero((Decimal)ar.get('Fee_Equivalent_Revenue__c')) + nullToZero(tmpScenarioProduct.Fee_Equivalent_Revenue__c);
      tmpScenarioProduct.Annual_Net_Revenue_Variable_Cost__c = nullToZero((Decimal)ar.get('Annual_Net_Revenue_Variable_Cost__c')) + nullToZero(tmpScenarioProduct.Annual_Net_Revenue_Variable_Cost__c);

      if(BookableOpportunityRevenue){
        tmpScenarioProduct.Total_Bookable_Product_Revenue__c=(Decimal)ar.get('Fee_Equivalent_Revenue__c');
      }

      scenarioProductMap.put(ScenarioProductId,tmpScenarioProduct);
    }
    update scenarioProductMap.values();
  }

  public Decimal nullToZero(Decimal num){
    return (num == null) ? 0 : num;
  }

  public static void updateScenItemsWExceptionPricing(List<LLC_BI__Scenario_Item__c> newList, Map<Id, LLC_BI__Scenario_Item__c> oldMap){
    System.debug('newList::'+newList);
    System.debug('oldMap::'+oldMap);
    //initialize var
    Set<Id> scenarioProductIds = new Set<Id>();
    Map<Id, Scenario_Product__c> updateMap = new Map<Id, Scenario_Product__c>();

    for(LLC_BI__Scenario_Item__c si : newList){
      Boolean meetsCriteria = false;
      //System.debug('sp::'+si.Scenario_Product__c);
      //System.debug('Exception_Priced__c::'+si.Exception_Priced__c);
      //System.debug('ifStatement1::'+si.Scenario_Product__c != null);
      //System.debug('ifStatement2::'+ ( oldMap == null && si.Exception_Priced__c )) ;
      //System.debug('ifStatement3::'+ ( si.Exception_Priced__c != oldMap.get(si.id).Exception_Priced__c || si.Scenario_Product__c != oldMap.get(si.id).Scenario_Product__c) ) ;

      //check to see if it is insert, scenario product is not blank and exeception priced equal true
      //if update, check to see if exception priced is change or ScenarioProduct changes then need to recalculate
      if(oldMap == null){
        if( si.Exception_Priced__c && si.Scenario_Product__c != null)
          meetsCriteria = true; 
      } else {//update call
        if(si.Scenario_Product__c != null &&  ( si.Exception_Priced__c != oldMap.get(si.id).Exception_Priced__c || si.Scenario_Product__c != oldMap.get(si.id).Scenario_Product__c)){
          meetsCriteria = true;
        }
      }

      if(meetsCriteria){
         scenarioProductIds.add(si.Scenario_Product__c);
         if( oldMap != null){
          if(si.Scenario_Product__c != oldMap.get(si.id).Scenario_Product__c && oldMap.get(si.id).Scenario_Product__c != null){
            //recalculate the old scenario produt
            scenarioProductIds.add(oldMap.get(si.id).Scenario_Product__c);
          }
        }
      }
    }
    System.debug('scenarioProductIds::'+scenarioProductIds);

    if(scenarioProductIds.size() >0){
      //query and roup the scenario items by exception priced equal true and the count of true items. grouped by scenario product id
      AggregateResult[] results = [select count(Id) ct, Scenario_Product__c, Exception_Priced__c FROM LLC_BI__Scenario_Item__c WHERE Scenario_Product__c IN: scenarioProductIds Group By Scenario_Product__c, Exception_Priced__c];
      System.debug('results::'+results);

      for(AggregateResult res : results){
        Id ScenarioProductId = (Id)res.get('Scenario_Product__c');
        if(ScenarioProductId == null) continue;

        if((Boolean)res.get('Exception_Priced__c') ){
          Decimal totalCount = 0;
          try{
            totalCount = (Decimal)res.get('ct');
          }catch (Exception ex){}
          System.debug('totalCount::'+totalCount);

          //update the update list to include the scenario product
          updateMap.put(ScenarioProductId, new Scenario_Product__c(Id=ScenarioProductId, Scenario_Items_with_NonStandard_Price__c  = totalCount));
        } else {
          if( !updateMap.containsKey(ScenarioProductId) ){
            updateMap.put(ScenarioProductId, new Scenario_Product__c(Id=ScenarioProductId, Scenario_Items_with_NonStandard_Price__c  = 0));
          }
        }
        
      }

      if(updateMap.keyset().size() > 0)
        update updateMap.values();
    }
  }
}