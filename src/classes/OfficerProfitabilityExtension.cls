//================================================================================
//  KeyBank  
//  Object: Financials
//  Author: Offshore
//  Detail: OfficerProfitabilityExtension 
//  Description : Officers Profitability List View

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================

public class OfficerProfitabilityExtension 
{
    public Financial__c financial;
    public List<Financial__c> ListFinancial;
    public String userId;
    
    public OfficerProfitabilityExtension(ApexPages.StandardController controller) 
    {
        ListFinancial = new List<Financial__c>();
        userId = userInfo.getUserID();
    }
    public List<Financial__c> getListFinancial()
    {
        ListFinancial=[SELECT AccountId__c,Account_Number__c,As_Of_Date_Month__c,As_Of_Date__c,Cash_Management_Total__c,Commercial_Card__c,Contribution_Margin__c,Credit_Revenue__c,Customer_IPI__c,Debt_Capital_Market__c,Deposit_Revenue__c,Equity_Capital_Market__c,Financial_Advisory__c,Foreign_Exchange__c,Id,IndividualId__c,Info_Srvc_Products__c,KMS__c,Legacy_Siebel_Id__c,Legacy_Source_Id__c,Liquidity_Products__c,Mortgage_Banking__c,Name,Net_Revenue__c,Officer_Code__c,Other_Products__c,Other__c,OwnerId,Payables_Products__c,Profitability_Period__c,Profitability_Seq__c,Profitability_Type__c,Receiveable_Products__c,RecordTypeId,Syndication_Fees__c FROM Financial__c where ownerId= :userId and recordtype.DeveloperName='Officer_Profitability' order by Profitability_Period__c Desc]; 
        return ListFinancial;
    }
}