/*
This class is written for KEF KRR Project by TCS Team.
It is a helper class for Origination Document related Requirements.
*/
Public with sharing class OriginationDocHelper_KEF {

    /* Below method is added for Release 3 Integration Req. 208. 
     It updates the Integration status of Agreement on deletion of Origination Document.*/
     
    public static Map<id, string> updateAgrIntSatus(set<id> oriDocIds){
    
        List<Agreement__c> agrList = new List<Agreement__c>();
        List<id> docIds;
        Map<id, List<id>> agrIdVsDocIds = new Map<id, List<id>>();
        Map<id, string> docErrorMap = new Map<id, string>();
    
        for(Origination_Document__c doc : [SELECT KEF_Agreement__c, KEF_Agreement__r.Integration_Status__c, 
                                                    KEF_Agreement__r.Agreement_Status__c, KEF_Agreement__r.Integrated__c 
                                             FROM Origination_Document__c 
                                             WHERE id IN :oriDocIds
                                             AND ( KEF_Agreement__r.Agreement_Status__c = :System.Label.Agreement_Active
                                             OR KEF_Agreement__r.Integrated__c = true)])
        {    
 
            ID agrId = doc.KEF_Agreement__c;
            if(agrIdVsDocIds.keyset().contains(agrId))
            {
                docIds = agrIdVsDocIds.get(agrId);
            }
            else
            {
                Agreement__c agr = new Agreement__c();
                agr.id = doc.KEF_Agreement__c;
                agr.Integration_Status__c = System.Label.Not_Picked_Up;
                agrList.add(agr);
                       
                docIds = new List<id>();
            }     
            docIds.add(doc.id);
            agrIdVsDocIds.put(agrId, docIds); 
        }
        
        if(agrList!=null && agrList.size()>0)
        {                
            Database.SaveResult[] srList = Database.update(agrList, false); 
                   
            for (Integer temp =0; temp < srList.size(); temp ++) 
            {            
                if (!srList[temp].isSuccess()) 
                {
                    String errMsg = srList[temp].getErrors().get(0).getMessage();
                    for(Id docId : agrIdVsDocIds.get(agrList[temp].id))
                    {
                        docErrorMap.put(docId, errMsg);   
                    } 
                }
            }
        } 
        return docErrorMap;
    }
}