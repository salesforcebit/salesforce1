public with sharing class UserProgressDetails{
        
    public Map<Id, Integer> userIdVsLastLoginCount{get;set;} 
    public Map<Id, Integer> userIdVsThisLoginCount{get;set;}
    public Map<Id, Integer> userIdVsLastCallCount{get;set;} 
    public Map<Id, Integer> userIdVsThisCallCount{get;set;}
    public Map<Id, Integer> userIdVsPastDueOppCount{get;set;} 
    public Map<Id, Integer> userIdVsUnmodifiedOppCount{get;set;}
        
    public List<User_Progress_Detail__c> userProgressDetailList{get;set;}
    
    Public UserProgressDetails(){
        List<User> allUsersList = new List<User>();
        allUsersList = [SELECT id, Name FROM user];
        Map<id, String> userIdVsUserName = new Map<id, String>();
        
        Set<id> userIdSet = new Set<id>();      
        for(user u : allUsersList){
            userIdSet.add(u.id);
            userIdVsUserName.put(u.id, u.Name);
        }
        system.debug('main----userIdSet--'+userIdSet);
        Map<Id, Integer> userIdVsLastLoginCount = new Map<Id, Integer>();
        Map<Id, Integer> userIdVsThisLoginCount = new Map<Id, Integer>();
        Map<Id, Integer> userIdVsLastCallCount = new Map<Id, Integer>();
        Map<Id, Integer> userIdVsThisCallCount = new Map<Id, Integer>();
        Map<Id, Integer> userIdVsPastDueOppCount = new Map<Id, Integer>();
        Map<Id, Integer> userIdVsUnmodifiedOppCount = new Map<Id, Integer>();
        userProgressDetailList = new List<User_Progress_Detail__c>();
         
        userIdVsLastLoginCount = UserLoginDetails.UserLoginCounts(userIdSet, 'LAST_MONTH'); 
        system.debug('main----userIdVsLastLoginCount --'+userIdVsLastLoginCount );       
        userIdVsThisLoginCount = UserLoginDetails.UserLoginCounts(userIdSet, 'THIS_MONTH'); 
        system.debug('main----userIdVsThisLoginCount --'+userIdVsThisLoginCount );
         
        userIdVsLastCallCount = callcountprogress.UserCallCounts(userIdSet, 'LAST_MONTH'); 
        system.debug('main----userIdVsLastCallCount --'+userIdVsLastCallCount );
        userIdVsThisCallCount = callcountprogress.UserCallCounts(userIdSet, 'THIS_MONTH');
        system.debug('main----userIdVsThisCallCount --'+userIdVsThisCallCount );
         
        userIdVsPastDueOppCount = UserOptyDetails.UserPastDueOptyCounts(userIdSet);
        system.debug('main----userIdVsPastDueOppCount --'+userIdVsPastDueOppCount );
        userIdVsUnmodifiedOppCount = UserOptyDetails.UserUnmodifiedOptyCounts(userIdSet);
        system.debug('main----userIdVsUnmodifiedOppCount --'+userIdVsUnmodifiedOppCount );  
        
        for(user u : allUsersList){
                
            User_Progress_Detail__c userDetail = new User_Progress_Detail__c();
             
            userDetail.User__c = u.id;
            userDetail.User_Id__c = u.id;
            userDetail.Name = userIdVsUserName.get(u.id);
                       
            if(userIdVsLastLoginCount.keyset().contains(u.id)){   
                userDetail.Last_Month_Login_Count__c = userIdVsLastLoginCount.get(u.id);
            }
            else  
                userDetail.Last_Month_Login_Count__c = 0;
            if(userIdVsThisLoginCount.keyset().contains(u.id)){
                userDetail.This_Month_Login_Count__c = userIdVsThisLoginCount.get(u.id); 
            }
            else
                userDetail.This_Month_Login_Count__c = 0;    
            if(userIdVsLastCallCount.keyset().contains(u.id)){
                userDetail.Last_Month_Call_Count__c = userIdVsLastCallCount.get(u.id); 
            }
            else
                userDetail.Last_Month_Call_Count__c = 0;
            if(userIdVsThisCallCount.keyset().contains(u.id)){
                userDetail.This_Month_Call_Count__c = userIdVsThisCallCount.get(u.id);
            }
            else
                userDetail.This_Month_Call_Count__c = 0;
            if(userIdVsPastDueOppCount.keyset().contains(u.id)){
                userDetail.Past_Due_Opportunities__c = userIdVsPastDueOppCount.get(u.id);
            }
            else
                userDetail.Past_Due_Opportunities__c = 0;
            if(userIdVsUnmodifiedOppCount.keyset().contains(u.id)){
                userDetail.Unmodified_Opportunities__c = userIdVsUnmodifiedOppCount.get(u.id);
            }
            else
                userDetail.Unmodified_Opportunities__c = 0;  
                
            userProgressDetailList.add(userDetail);           
        } 
        
        Schema.SObjectField matchField = User_Progress_Detail__c.Fields.User_Id__c;
        boolean allorNone = false;
        Database.UpsertResult [] upsertResult = Database.upsert(userProgressDetailList,matchField,false);
        
        for (Database.UpsertResult sr : upsertResult) {
            system.debug('----result-----'+sr.isSuccess()+'----error---'+sr.getErrors());
        }
      }        

}