@isTest
public class Test_vendortabclass
{
    Public static testmethod void vendortabclass_test()
    {
        //Account new record
        Account acct = new account(name='vendortest',Is_Vendor__c=true,Vendor_status__c='Pending Credit',KEF_Risk_Role__c='Vendor/Supplier',BillingCity='test1',BillingCountry='tyer',BillingPostalCode='45678',BillingState='uiop',BillingStreet='tertdf',Phone='9876543217');
        insert acct;
                
        Account acc = new account(name='vendortes',Is_Vendor__c=true,Vendor_status__c='Pending Credit',KEF_Risk_Role__c='Vendor/Supplier',BillingCity='test2',BillingCountry='tyer',BillingPostalCode='45678',BillingState='uiop',BillingStreet='tertdf',Phone='9876533217');
        insert acc;

        //Program record
        Program__c pgm = new Program__c(Name_of_the_Vendor_Program__c='pgmtest',Business_Development_Stage__c='Initial Contact Made',Program_Status__c='Prospect');
        insert pgm;
        
        //Agreement new record
        Agreement__c agr = new Agreement__c(Agreement_Name__c='agrname',Default_lessor__c='KEF',Agreement_Type__c='Assignment',Agreement_Legal_Status__c='Informal',Program__c=pgm.id,Agreement_Status__c='Prospect');
        insert agr;
        
        //opportunity record
        opportunity opty = new opportunity(name ='opptest',AccountId = acct.id,closedate=system.today(),Volume__c = 125.9,StageName='Pursue',Vendor_Agreement__c=agr.id);
        insert opty;

        Vendor_Agreement__c venA = new Vendor_Agreement__c(Account_Vendor__c=acct.id,Agreement__c=agr.id);
        insert venA;
        
        Vendor_Agreement__c venAg = new Vendor_Agreement__c(Account_Vendor__c=acc.id,Agreement__c=agr.id);
        insert venAg;
         
        List<Opportunity> op = new List<opportunity>();
        //Passing parameter to call extension class
        ApexPages.StandardController sc = new ApexPages.StandardController(opty);
        vendortabclass vt = new vendortabclass(sc);
        vt.agven=opty;
        vt.venIdChosen=venAg.name;
        vt.selvendor=venAg;
        vt.selectedvendors();
        opty.Vendor_and_Agreement_Association__c=venAg.id;
        opty.Vendor__c = venAg.Account_Vendor__c;
        update opty;
        op.add(opty);
        vt.redirect();
        system.assertEquals(1,op.size());

    }
    Public static testmethod void vendortabclass_test1()
    {

        //Account new record
        Account accven = new account(name='vendor',Is_Vendor__c=true,Vendor_status__c='Pending Credit',KEF_Risk_Role__c='Vendor/Supplier',BillingCity='test3',BillingCountry='tyer',BillingPostalCode='45678',BillingState='uiop',BillingStreet='tertdf',Phone='9876563217');
        insert accven;
        
        /*Account accven1 = new account(name='vendor1',Is_Vendor__c=true,Vendor_status__c='Pending Credit',KEF_Risk_Role__c='Vendor/Supplier',BillingCity='test3',BillingCountry='tyer',BillingPostalCode='45678',BillingState='uiop',BillingStreet='tertdf',Phone='9876563217');
        insert accven1;*/

        //Program record
        Program__c pg = new Program__c(Name_of_the_Vendor_Program__c='venname',Business_Development_Stage__c='Initial Contact Made',Program_Status__c='Prospect');
        insert pg;
        
        //Agreement new record
        Agreement__c ag = new Agreement__c(Agreement_Name__c='agname',Default_lessor__c='KEF',Agreement_Type__c='Assignment',Agreement_Legal_Status__c='Informal',Program__c=pg.id,Agreement_Status__c='Prospect');
        insert ag;
        
        Vendor_Agreement__c venags = new Vendor_Agreement__c(Account_Vendor__c=accven.id,Agreement__c=ag.id);
        insert venags;
        
        //opportunity record
        opportunity oppty = new opportunity(name ='optest',AccountId = accven.id,closedate=system.today(),Volume__c = 127.8,StageName='Pursue',Vendor_Agreement__c=ag.id,Vendor_and_Agreement_Association__c=venags.id,Vendor__c=accven.Id);
        insert oppty;
         
      /*  Vendor_Agreement__c  vtn=[select id,name,Account_Vendor__c,Account_Vendor__r.Vendor_status__c,Vendor_status__c  from Vendor_Agreement__c where Agreement__c=:oppty.Vendor_Agreement__c];
        
        Vendor_Agreement__c vendorl =[select id,name,Account_Vendor__c,Vendor_status__c from Vendor_Agreement__c where id=: oppty.Vendor_and_Agreement_Association__c];
         
        Vendor_Agreement__c venags1 = new Vendor_Agreement__c(Account_Vendor__c=accven1.id,Agreement__c=ag.id);
        insert venags1;*/ 
        //Passing parameter to call extension class
        ApexPages.StandardController sc = new ApexPages.StandardController(oppty);
        vendortabclass vtn = new vendortabclass(sc);
        vtn.agven=oppty;
        vtn.vendorlist=[select id,name,Account_Vendor__c,Account_Vendor__r.Vendor_status__c,Vendor_status__c  from Vendor_Agreement__c where Agreement__c=:oppty.Vendor_Agreement__c];
        vtn.vendorl=venags;
        vtn.selectedvendors();
        vtn.redirect();
               
        /*Vendor_Agreement__c selvendor=[select id,name,Account_Vendor__c,Vendor_status__c from Vendor_Agreement__c where ID =:venags1.ID];
        oppty.Vendor_and_Agreement_Association__c = venags1.id;
        oppty.Vendor__c= venags1.Account_Vendor__c;
        update oppty;*/
    }
    
}