/*
This class is written for KEF KRR Project by TCS Team

It is a helper class for Agreement related requirements
*/
public with sharing class AgreementHelper 
{   
    //Calculation of Vendor Agreement End Date 
    
    /* below sets are used for reports and dashboard requirement 148.   */    
    public static Set<String> AGREEMENT_CREDIT_APPROVAL_TRENDS = new Set<String>
    {
        System.Label.Agreement_Active, System.Label.AgreementPended, System.Label.Agreement_CreditDeclined, System.Label.Agreement_Submit_to_Credit
    }; 
  
    public static Set<String> AGREEMENT_VENDOR_COMMITTEE_TRENDS = new Set<String>
    {
        System.Label.Agreement_Submit_to_Credit, System.Label.Agreement_VendorCommitteeDeclined, System.Label.Agreement_SubmitToVendorCommittee
    };     
  
    public static void calculateEnddate(Agreement__c objagr)          
    {
        if(objagr != null && objagr.Agreement_Term__c != null)
        {
            //if agreement date is not populated, then take the value from Commencement date
            if(objagr.Agreement_Renewal_Date__c == null && objagr.Agreement_Commencement_Date__c != null)
            {   
               objagr.Agreement_End_Date__c = objagr.Agreement_Commencement_Date__c.addmonths(integer.valueof(objagr.Agreement_Term__c));
               system.debug('--test--'+objagr.Agreement_End_Date__c);
            }
            else if(objagr.Agreement_Renewal_Date__c != null)
            {
                objagr.Agreement_End_Date__c = objagr.Agreement_Renewal_Date__c.addmonths(integer.valueof(objagr.Agreement_Term__c));                 
            }
        }
    } 
    
    //method that prepares a map with field api name as key and related wrapper which stores the field mapping as value 
    public static Map<String, KEFReportFieldPopulator> populateMapForFieldChangeAudit()
    {
        Map<String, KEFReportFieldPopulator> fieldChangeAuditMap = new Map<String, KEFReportFieldPopulator>();
        
        fieldChangeAuditMap.put(System.Label.AgreementNameAPIName, 
          new KEFReportFieldPopulator(System.Label.AgreementNameAPIName, '',  
            true, KEFSystemConstants.AGREEMENT_DT_AGREEMENTNAMECHANGE_API, true, KEFSystemConstants.AGREEMENT_UL_AGREEMENTNAMECHANGE_API));
         
     /*   fieldChangeAuditMap.put(System.Label.AgreementStatusAPIName, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, '',  
            true, KEFSystemConstants.AGREEMENT_DT_ANYSTATUSCHANGE_API, true, KEFSystemConstants.AGREEMENT_UL_ANYSTATUSCHANGE_API));
         */          
        return fieldChangeAuditMap;
    }
 
    //method that prepares a map with status picklist value as key and related wrapper which stores the field mapping as value    
    public static Map<String, KEFReportFieldPopulator> populateMapForStatusChangeAudit()
    {
        Map<String, KEFReportFieldPopulator> statusChangeAuditMap = new Map<String, KEFReportFieldPopulator>();   
    
        statusChangeAuditMap.put(System.Label.Agreement_Submit_to_Credit, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, System.Label.Agreement_Submit_to_Credit,  
            true, KEFSystemConstants.AGREEMENT_DT_SUBMITTOCREDIT_API, false, ''));  
        
        statusChangeAuditMap.put(System.Label.Agreement_Active, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, System.Label.Agreement_Active,  
            true, KEFSystemConstants.AGREEMENT_DT_ACTIVE_API, true, KEFSystemConstants.AGREEMENT_UL_ACTIVE_API));  
            
        statusChangeAuditMap.put(System.Label.Agreement_Inactive, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, System.Label.Agreement_Inactive,  
            true, KEFSystemConstants.AGREEMENT_DT_INACTIVE_API, true, KEFSystemConstants.AGREEMENT_UL_INACTIVE_API));   
            
        statusChangeAuditMap.put(System.Label.Agreement_SubmitToVendorCommittee, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, System.Label.Agreement_SubmitToVendorCommittee,  
            true, KEFSystemConstants.AGREEMENT_DT_SUBMITTOVENDORCOMMITTEE_API, false, '')); 
            
        statusChangeAuditMap.put(System.Label.AgreementPended, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, System.Label.AgreementPended,  
            true, KEFSystemConstants.AGREEMENT_DT_AGREEMENTPENDED_API, true, KEFSystemConstants.AGREEMENT_UL_AGREEMENTPENDED_API));   
           
       statusChangeAuditMap.put(System.Label.Agreement_CreditDeclined, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, System.Label.Agreement_CreditDeclined,  
            true, KEFSystemConstants.AGREEMENT_DT_CREDITDECLINED_API, true, KEFSystemConstants.AGREEMENT_UL_CREDITDECLINED_API));   
           
       statusChangeAuditMap.put(System.Label.Agreement_VendorCommitteeDeclined, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, System.Label.Agreement_VendorCommitteeDeclined,  
            true, KEFSystemConstants.AGREEMENT_DT_VENDORCOMMITTEEDECLINED_API, true, KEFSystemConstants.AGREEMENT_UL_VENDORCOMMITTEEDECLINED_API));   
        
       statusChangeAuditMap.put(System.Label.AnyStatusChange, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, System.Label.AnyStatusChange,  
            true, KEFSystemConstants.AGREEMENT_DT_ANYSTATUSCHANGE_API, true, KEFSystemConstants.AGREEMENT_UL_ANYSTATUSCHANGE_API));
        /*
        statusChangeAuditMap.put(System.Label.AgreementNameChange, 
          new KEFReportFieldPopulator(System.Label.AgreementNameAPIName, System.Label.AgreementNameChange,  
            true, KEFSystemConstants.AGREEMENT_DT_AGREEMENTNAMECHANGE_API, true, KEFSystemConstants.AGREEMENT_UL_AGREEMENTNAMECHANGE_API));   
        */
        statusChangeAuditMap.put(KEFSystemConstants.AGREEMENT_CREDITAPPROVAL_TRENDS, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, KEFSystemConstants.AGREEMENT_CREDITAPPROVAL_TRENDS,  
            true, KEFSystemConstants.AGREEMENT_DT_CREDITAPPROVALTRENDS_API, false, ''));
        
        statusChangeAuditMap.put(KEFSystemConstants.AGREEMENT_VANDORCOMMITTEE_TRENDS, 
          new KEFReportFieldPopulator(System.Label.AgreementStatusAPIName, KEFSystemConstants.AGREEMENT_VANDORCOMMITTEE_TRENDS,  
            true, KEFSystemConstants.AGREEMENT_DT_VENDORCOMMITTEETRENDS_API, false, ''));       
        
        return statusChangeAuditMap;
    }  
    
    //base method for populating Datetime and User id values for Agreement Record based on Status changes 
  public static void processAgreementforDRDataPopulation(Agreement__c newAgmnt, Map<String, KEFReportFieldPopulator> statusValueMap)
  {
    if(newAgmnt != null)
    {
       //checking if the current Status value exists in the map for which date and user needs to be populated
      if(statusValueMap != null && statusValueMap.containsKey(newAgmnt.Agreement_Status__c))
      {
        //populate date and User Id values for the status change
        populateStatusDateAndUserId(newAgmnt, statusValueMap.get(newAgmnt.Agreement_Status__c));
      } 
      //Checking if current status value is present in credit approval trends set and in statusValuMap keyset
      if(AgreementHelper.AGREEMENT_CREDIT_APPROVAL_TRENDS.contains(newAgmnt.Agreement_Status__c) &&
      statusValueMap.containsKey(KEFSystemConstants.AGREEMENT_CREDITAPPROVAL_TRENDS)) 
      {
          populateStatusDateAndUserId(newAgmnt, statusValueMap.get(KEFSystemConstants.AGREEMENT_CREDITAPPROVAL_TRENDS));
      }
      //Checking if current status value is present in vendor commitee trends set and in statusValuMap keyset
      if(AgreementHelper.AGREEMENT_VENDOR_COMMITTEE_TRENDS.contains(newAgmnt.Agreement_Status__c) &&
      statusValueMap.containsKey(KEFSystemConstants.AGREEMENT_VANDORCOMMITTEE_TRENDS)) 
      {
          populateStatusDateAndUserId(newAgmnt, statusValueMap.get(KEFSystemConstants.AGREEMENT_VANDORCOMMITTEE_TRENDS));
      }
      //for any status change        
      populateStatusDateAndUserId(newAgmnt, statusValueMap.get(System.Label.AnyStatusChange));
    }
  } 
  /*
  public static void processAgreementforDRDataPopulation2(Agreement__c newAgmnt, Map<String, KEFReportFieldPopulator> statusValueMap)
  {
    if(newAgmnt != null)
    {
      
      //checking if the current Status value exists in the map for which date and user needs to be populated
      if(statusValueMap != null && statusValueMap.containsKey(System.Label.AgreementNameChange))
      {
        //populate date and User Id values for the status change
        populateStatusDateAndUserId(newAgmnt, statusValueMap.get(System.Label.AgreementNameChange));
      } 
     }
  }*/

   //this method will populate the Agreement object with Date and User Id
   public static void populateStatusDateAndUserId(Agreement__c agreementRecord, KEFReportFieldPopulator wrapperIns)
   {
     if(agreementRecord != null && wrapperIns != null)
     {
       //checking if date-time data population is required
       if(wrapperIns.populateDateTimeValue && wrapperIns.dateTimeFieldAPIName != null)
       {         
         agreementRecord.put(wrapperIns.dateTimeFieldAPIName, System.Now());
       }
       //checking if user id population is required
      if(wrapperIns.populateUserLookupValue && wrapperIns.userLookupFieldAPIName != null)
       {
         agreementRecord.put(wrapperIns.userLookupFieldAPIName, UserInfo.getUserId());
       }
     }
   }
   
    //method will check for the valid email address entered in text area field value separated by semi-colon.
    public static Boolean validateEmailAddress(String fieldValue)
    {
        Boolean isError = false;
        
        /*The Regex Pattern is stored in Custom label with single '\' instead of double '\\' as value from custom label is 
        assigned as it is. When assiging value manually in code using string, '\\' is stored as '\' in variable. This is not the case when 
        done with Custom label.*/
        String emailRegex = System.Label.EmailRegexString;              
        Pattern MyPattern = Pattern.compile(emailRegex);        
        if(fieldValue != null && fieldValue != '')
        {
            for(String emailStr : fieldValue.split(';'))
           {
                emailStr = emailStr != null ? emailStr.trim() : '';
                Matcher MyMatcher = MyPattern.matcher(emailStr);
                if(!MyMatcher.matches()) 
                {
                    isError = true;
                    break;          
                }                   
            }           
        }
        return isError;  
    }   
    
     //method for creation of 2 entries for Notes and Attachment july release -- CR-1410 
    public static void  UpdateNotesAndAttachment(Agreement__c[] Agg)
    {
     list<Private_Note_Attachment__c> noteList = new list<Private_Note_Attachment__c>();
        
        for(Agreement__c  objagg :Agg)
        {
            Private_Note_Attachment__c Att= new Private_Note_Attachment__c();
            Att= new Private_Note_Attachment__c (name='Originations Info',KEF_Agreement__c= objagg.id);
            noteList.add(Att);
            Att= new Private_Note_Attachment__c (name='Legal Program Documentation',KEF_Agreement__c= objagg.id);
            noteList.add(Att);
         
         }
     
     insert noteList ; 
        
    } 
}