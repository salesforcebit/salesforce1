/*******************************************************************************************************************************************
* @name         :   ECCP_CreditBureausHelper
* @description  :   This class will be responsible to call the service with all the required parameter and convert a response in desigred structure
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/

public class ECCP_CreditBureausHelper{

    public ECCP_CreditBureauRequest reqObj;
    public ECCP_CreditBureauResponse resObj;
    private String currentclasssname;
    private static Map<String,ECCP_WebService_Data__mdt> mapWsSettings;

    public ECCP_CreditBureausHelper(){
        currentclasssname = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
        ECCP_CreditBureauResponse resObj = new ECCP_CreditBureauResponse();  
        reqObj = new ECCP_CreditBureauRequest();
        mapWsSettings = ECCP_CustomMetadataUtil.queryMetadata(ECCP_IntegrationConstants.CreditBureauMataData); 
    }
/*******************************************************************************************************************************************
* @name         :   ECCP_CreditBureauProcess
* @description  :   This method will be responsible to call the service and form the response
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/
    public ECCP_CreditBureauResponse ECCP_CreditBureauProcess(ECCP_CreditBureauRequest reqObj){
        
        // ECCP_CreditBureauResponse resp = new ECCP_CreditBureauResponse();
        ECCP_CreditbureauWS.Customer_Type wsReq = new ECCP_CreditbureauWS.Customer_Type();  
        
        wsReq.SSN = reqObj.SSN;
        wsReq.Orchestration_Code = reqObj.Orchestration_Code;
        wsReq.AccountName = reqObj.AccountName;
        wsReq.BillingStreet = reqObj.BillingStreet;
        wsReq.BillingCity = reqObj.BillingCity;
        wsReq.BillingState = reqObj.BillingState;
        wsReq.BillingPostalcode = reqObj.BillingPostalcode;
        wsReq.FirstName = reqObj.FirstName;
        wsReq.LastName = reqObj.LastName;
        wsReq.Birthdate = reqObj.Birthdate;
        wsReq.HomePhone = reqObj.HomePhone;
        wsReq.MobilePhone = reqObj.MobilePhone;
        //Added by Alisha to cover Test class

       
            ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port service = new ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port();
            service = getServiceInstance();
            ECCP_CreditbureauWS.ECCP_Credit_Bureau_Type responseElement = new ECCP_CreditbureauWS.ECCP_Credit_Bureau_Type();
            system.debug('--TempReq---'+wsReq);
            responseElement = service.getRequestOverHttp(wsReq);
            resObj = populateResponseObject(responseElement);
            return resObj;
        
        
    }
/*******************************************************************************************************************************************
* @name         :   getServiceInstance
* @description  :   This method sets the required parameter to call the service
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/
    public ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port getServiceInstance(){
        ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port service = new ECCP_CreditbureauWS.WebServices_CreditBureauWS_Port();
        service.timeOut_x   =   Integer.valueOf(mapWsSettings.get(ECCP_IntegrationConstants.CreditBureauMataData).TimeOut__c);
        service.endpoint_x  =   mapWsSettings.get(ECCP_IntegrationConstants.CreditBureauMataData).EndPointUrl__c;
        service.clientCertName_x    =  mapWsSettings.get(ECCP_IntegrationConstants.CreditBureauMataData).CertificateName__c; 
        return service;
        }
/*******************************************************************************************************************************************
* @name         :   populateResponseObject
* @description  :   This method sets the response object
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/    
    public ECCP_CreditBureauResponse populateResponseObject(ECCP_CreditbureauWS.ECCP_Credit_Bureau_Type responseElement){
    
            resObj = new ECCP_CreditBureauResponse();
    
            resObj.SSN=responseElement.SSN;
            resObj.Orchestration_Code=responseElement.Orchestration_Code;
            resObj.Credit_Bureau_ID=responseElement.Credit_Bureau_ID;
            resObj.CreditScore=responseElement.CreditScore;
            resObj.Credit_Bureau_Reason_Code_1= string.valueof(responseElement.Credit_Bureau_Reason_Code_1);
            //resObj.Credit_Bureau_Reason_Code_1= integer.valueof(responseElement.Credit_Bureau_Reason_Code_1);
            resObj.Credit_Bureau_Reason_Code_Description_1=responseElement.Credit_Bureau_Reason_Code_Description_1;
            resObj.Credit_Bureau_Reason_Code_2=responseElement.Credit_Bureau_Reason_Code_2;
            resObj.Credit_Bureau_Reason_Code_Description_2=responseElement.Credit_Bureau_Reason_Code_Description_2;
            resObj.Credit_Bureau_Reason_Code_3=responseElement.Credit_Bureau_Reason_Code_3;
            resObj.Credit_Bureau_Reason_Code_Description_3=responseElement.Credit_Bureau_Reason_Code_Description_3;
            resObj.Credit_Bureau_Reason_Code_4=responseElement.Credit_Bureau_Reason_Code_4;
            resObj.Credit_Bureau_Reason_Code_Description_4=responseElement.Credit_Bureau_Reason_Code_Description_4;
            resObj.Credit_Bureau_Reason_Code_5=responseElement.Credit_Bureau_Reason_Code_5;
            resObj.Credit_Bureau_Reason_Code_Description_5=responseElement.Credit_Bureau_Reason_Code_Description_5;
            resObj.Credit_Bureau_FA_Request_Type=responseElement.Credit_Bureau_FA_Request_Type;
            resObj.Bureau_Fraud=responseElement.Bureau_Fraud;
            resObj.Patriot_Fraud_Indicator=responseElement.Patriot_Fraud_Indicator;
            resObj.Credit_Bureau_Addr_Discrepancy=responseElement.Credit_Bureau_Addr_Discrepancy;
            resObj.Credit_Active_Date =responseElement.Credit_Active_Date;
            resObj.ReportDate   =responseElement.ReportDate;
            resObj.Credit_Report=responseElement.Credit_Report;
            resObj.ID_Scan_Fraud_Code =responseElement.ID_Scan_Fraud_Code;
            resObj.Hawk_Alert_Fraud_Code=responseElement.Hawk_Alert_Fraud_Code; 
            
            resObj.errorcode=responseElement.errorcode; 
            resObj.errordesc=responseElement.errordesc; 
            // Addded by Aditya : 2August2016 : Release 1.1 : To get BureauInstruct1 and BureauInstruct2 from response
            resObj.BureauInstruct1 = responseElement.BureauInstruct1;
            resObj.BureauInstruct2 = responseElement.BureauInstruct2;
            return resObj;
        }
}