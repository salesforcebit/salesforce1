/*******************************************************************************************************************************************
* @name         :   ECCP_CreditBureauHTMLReport
* @description  :   This class will be responsible to pass report data to visual force page.
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/

public class ECCP_CreditBureauHTMLReport {

    public ID id{get;set;}
    public String CBName;
    public String CBAgencyName;
    
    public ECCP_CreditBureauHTMLReport(ApexPages.StandardController stdController) {    
        id = stdController.getRecord().id;
    }


    public String getContent(){

        id = ApexPages.currentPage().getParameters().get('id');        
        List<ECCP_Credit_Bureau__c > lstCreditReport= [select Name,Credit_Bureau_ID__c,Credit_Report__c,Credit_Report_1__c,Credit_Report_2__c from ECCP_Credit_Bureau__c where id=:id];        
        
        CBName = lstCreditReport.get(0).Name;
        CBAgencyName = lstCreditReport.get(0).Credit_Bureau_ID__c;
        
        String rep1 = lstCreditReport.get(0).Credit_Report__c !=null && lstCreditReport.get(0).Credit_Report__c !=''?lstCreditReport.get(0).Credit_Report__c:'';
        String rep2 = lstCreditReport.get(0).Credit_Report_1__c !=null && lstCreditReport.get(0).Credit_Report_1__c !=''?lstCreditReport.get(0).Credit_Report_1__c:'';
        String rep3 = lstCreditReport.get(0).Credit_Report_1__c !=null && lstCreditReport.get(0).Credit_Report_1__c !=''?lstCreditReport.get(0).Credit_Report_1__c:''; 
        String HTMLContentTemp = rep1+rep2+rep3;        
        String HTMLContent = (HTMLContentTemp !=null ||  HTMLContentTemp !='')?((HTMLContentTemp).remove('![CDATA[').remove(']]')):HTMLContentTemp;
        //String HTMLContent =HTMLContentTemp.remove('![CDATA[').remove(']]');        
        return HTMLContent;
    }

public pagereference savePDF() { 

    try{
    pageReference thePage = Page.ECCP_CreditBureauReportPage;
    blob body;
    if(!Test.IsRunningTest()){
    Body = thePage.getContentAsPDF();
    }
    else{
    Body = Blob.valueOf('Unit.Test');   
    }
    attachment pdf = new attachment();
    pdf.body = body;   

    //pdf.name='CreditReport_'+system.today()+'.pdf';
    pdf.name=CBName+'_'+CBAgencyName+'_'+system.today()+'.pdf';
    pdf.ParentId = id;
    pdf.ContentType = 'application/pdf';
    pdf.isPrivate = false;
    insert pdf;
    }catch(exception e){
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error,'Unable to generate PDF file due to error:'+e.getmessage()));
    }
    return null;    
  }

}