public with sharing class KEFSystemConstants 
{
    public static String PROGRAM_OBJ_API_NAME = 'Program__c';
    
    /* added for Req. 148 */
    public static string DT_ACTIVEHUNTINGLICENSE_API = 'DateTime_ActiveHuntingLicense__c';
    public static String UL_ACTIVEHUNTINGLICENSE_API = 'User_ActiveHuntingLicense__c';
    
    public static string DT_SUBMITTOCREDIT_API = 'DateTime_SubmitToCredit__c';
    public static String UL_SUBMITTOCREDIT_API = 'User_SubmitToCredit__c';
    
    public static string DT_ANYSTATUSCHANGE_API = 'DateTime_AnyStatusChange__c';
    public static String UL_ANYSTATUSCHANGE_API = 'User_AnyStatusChange__c';
    
    public static string DT_PROGAPPROVALTRENDS_API = 'DateTime_ProgApprovalTrends__c';
    public static String UL_PROGAPPROVALTRENDS_API = 'User_ProgApprovalTrends__c';
    
    public static String PROGRAM_APPROVAL_TREND_1 = 'pat1'; 
    public static String PROGRAM_DT_ACTIVE_API = 'DateTime_Active__c';
    public static String PROGRAM_UL_ACTIVE_API = 'User_Active__c';
    
    /*----------------Req. 148------KEF Agreement----------------------*/
    
    public static String AGREEMENT_DT_SUBMITTOCREDIT_API = 'DateTime_SubmitToCredit__c';
        
    public static String AGREEMENT_DT_ACTIVE_API = 'DateTime_Active__c';
    public static String AGREEMENT_UL_ACTIVE_API = 'User_Active__c';
    
    public static String AGREEMENT_DT_AGREEMENTPENDED_API = 'DateTime_AgreementPended__c';
    public static String AGREEMENT_UL_AGREEMENTPENDED_API = 'User_AgreementPended__c';
 
    public static String AGREEMENT_DT_CREDITDECLINED_API = 'DateTime_CreditDeclined__c';
    public static String AGREEMENT_UL_CREDITDECLINED_API = 'User_CreditDeclined__c';
    
    public static String AGREEMENT_DT_SUBMITTOVENDORCOMMITTEE_API = 'DateTime_SubmitToVendorCommittee__c';
    
    public static String AGREEMENT_DT_VENDORCOMMITTEEDECLINED_API = 'DateTime_VendorCommitteeDeclined__c';
    public static String AGREEMENT_UL_VENDORCOMMITTEEDECLINED_API = 'User_VendorCommitteeDeclined__c';  
    
    public static String AGREEMENT_DT_INACTIVE_API = 'DateTime_Inactive__c';
    public static String AGREEMENT_UL_INACTIVE_API = 'User_Inactive__c';  
    
    public static String AGREEMENT_DT_ANYSTATUSCHANGE_API = 'DateTime_AnyStatusChange__c';
    public static String AGREEMENT_UL_ANYSTATUSCHANGE_API = 'User_AnyStatusChange__c';   
    
    public static String AGREEMENT_DT_AGREEMENTNAMECHANGE_API = 'DateTime_AgreementName_Change__c';
    public static String AGREEMENT_UL_AGREEMENTNAMECHANGE_API = 'User_AgreementName_Change__c'; 
   
    public static String AGREEMENT_DT_CREDITAPPROVALTRENDS_API = 'DateTime_CreditApprovalTrends__c';
    public static String AGREEMENT_DT_VENDORCOMMITTEETRENDS_API = 'DateTime_VendorCommitteeTrends__c'; 
    
    public static String AGREEMENT_CREDITAPPROVAL_TRENDS = 'key1'; 
    public static String AGREEMENT_VANDORCOMMITTEE_TRENDS = 'key2'; 
}