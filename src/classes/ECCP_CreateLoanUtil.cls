/*******************************************************************************************************************************************
* @name         :   ECCP_CreateLoanUtil
* @description  :   This class will be responsible to create data for test classes.
* @author       :   Aditya/Alisha
* @createddate  :   15/April/2016
*******************************************************************************************************************************************/
@isTest
public class ECCP_CreateLoanUtil{
   public static list<LLC_BI__Loan__c> CreateLoanData(integer numAccts , integer numContactsPerAcct){
   list<LLC_BI__Legal_Entities__c> entList = new list<LLC_BI__Legal_Entities__c>();
   list<LLC_BI__Loan__c> LoanList = new list<LLC_BI__Loan__c>();
   Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
   User u = new User(Alias = 'stdKey', Email='standarduser@testorg.comKey', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comKey',PLOB__c='Support');
   insert u;
   System.runAs(u){
   /************Create Account**************/
     list<account> accList = new list<account>();
     list<LLC_BI__Product_Package__c> ppList = new list<LLC_BI__Product_Package__c>();
     SIC_NAICS__c sicNaics = new SIC_NAICS__c();
     insert sicNaics ;
     Map<string,string> ppMap = new Map<string,string>();
     
     for(integer i=0;i<numAccts;i++){
        account acc = new account(Name = 'Test Account' +i,Type ='Business Prospect',tin__c='12345',phone='1234567890',Mobile_Phone__c='1234567890',Home_Phone__c ='1234567890',
                                 nCino_NAICS__c = sicNaics.Id,BillingCity='city',BillingCountry ='country',BillingState ='Alaska',BillingStreet ='street',BillingPostalCode ='123456',
                                 ECC_Incorporation_State__c ='Ak' ,ECC_Organization_ID__c='orgId');
        
        acclist.add(acc);
     }
     insert acclist;
    
   
       /************Create Contact**************/
     
     list<contact> conlist = new list<contact>();
      for(integer j=0 ;j<numAccts;j++){
          Account acct = acclist[j];  
          //for each account just inserted,add contacts
          for(Integer k=numContactsPerAcct*j;k<numContactsPerAcct*(j+1);k++){
              conlist.add(new contact(firstname = 'Test'+k,lastname = 'Test'+k,AccountId = acct.Id, SSN__c = 'SSN'+k,phone='1234567890'));
              }
            }
            insert conlist;
        list<account>al = new list<account>();
        for(Account a : Acclist){
            account a1 = new account();
            a1.Contact_Name__c = conlist[0].id;
            a1.id = a.id;
            a1.LLC_BI__Tax_Identification_Number__c= '567908999';
            al.add(a1);
            }
        update al;
          
    /**************Create Product Package*******************/  
     for(Account a : Acclist){
       
       LLC_BI__Product_Package__c pp = new LLC_BI__Product_Package__c();
          pp.Name = a.name;
          PP.Bank_Division__c ='Business Banking';
          pp.LLC_BI__Account__c = a.id;
          pp.LLC_BI__Stage__c = 'Application Processing';
          pp.LLC_BI__Primary_Officer__c = u.Id;
          pp.Credit_Authority_Name__c =u.Id;
          pp.Delegated_Authority_Name__c  = u.Id;
          ppList.add(pp);
     }
     insert ppList;
     /****************Create Branch***************/
     List<LLC_BI__Branch__c> branchList = new List<LLC_BI__Branch__c>();
         LLC_BI__Branch__c branch = new LLC_BI__Branch__c();
         branch.Name = 'branch';
         branchList.add(branch);
         insert branchList; 
     
    /*****************Create Loan*****************/
    
     for(LLC_BI__Product_Package__c  pp : ppList){ 
            ppMap.put(pp.LLC_BI__Account__c,pp.Id);
            }
            
         for(String str : ppMap.keySet()){

         LLC_BI__Loan__c newLoan = new LLC_BI__Loan__c(); 
         newLoan.name = 'LoanABCTest';
         newLoan.LLC_BI__Stage__c = 'Application Processing' ;
         newLoan.LLC_BI__Account__c = str;
         newLoan.LLC_BI__Amount__c = 56789000;
         //Aditya : Release 2.0 change : updated ECCP_Application_Number__c to '0977' to avoid exception due to ECC_Loan_Original_Application_Id field update
         newLoan.ECCP_Application_Number__c = '0977';
         newLoan.LLC_BI__Product_Package__c = ppMap.get(str);
         newLoan.ECC_Rate_Description__c = '00170 Akron Stmt Sav';
         newLoan.LLC_BI__Branch__c = branchList[0].Id;
         newLoan.Loan_Purpose_Code__c  = '101 New Coml/cons aircraft - prop';
         newLoan.LLC_BI__Pricing_Basis__c ='Variable';
         LoanList.add(newLoan);  
     }
     insert LoanList;
     system.debug('---------LoanList--------'+LoanList);
      for(integer i=0;i<5;i++){
            LLC_BI__Legal_Entities__c ent = new LLC_BI__Legal_Entities__c();
            ent.name ='Entity'+i;
            ent.LLC_BI__Loan__c = LoanList[0].id;
            ent.LLC_BI__Account__c = LoanList[0].LLC_BI__Account__c;
            ent.LLC_BI__Product_Package__c = LoanList[0].LLC_BI__Product_Package__c;
            ent.LLC_BI__Tax_ID__c ='1234567890';
            //Aditya : Release 2.0 change : Added LLC_BI__Borrower_Type__c type to avoid validation rule ECC_Entity_Involvement_Validation_01
            ent.LLC_BI__Borrower_Type__c = 'Borrower';
            entList.add(ent);
            system.debug('!!!!!!!!!!!'+entlist);
        }
        insert entList;
       List<LLC_BI__Contingent_Liabilty__c> authList = new List<LLC_BI__Contingent_Liabilty__c >();
       for(LLC_BI__Legal_Entities__c  en :entList){  
           LLC_BI__Contingent_Liabilty__c auth = new LLC_BI__Contingent_Liabilty__c();
           auth.LLC_BI__Entity__c = en.Id;
           auth.LLC_BI__Contact__c = conlist[0].Id;
           //Aditya : Release 2.0 change : Added following to avoid exception due to Authorized_Signer_Validation_02
           auth.LLC_BI__Authority__c = 'Authorized';
           auth.LLC_BI__Role__c = 'Sole Proprietorship';
           auth.LLC_BI__Contingent_Percentage__c = 5;
           authList.add(auth);
       }
       insert authList;

     //}
     
    
     List<LLC_BI__Legal_Entities__c > entListNew = new List<LLC_BI__Legal_Entities__c >();
     for(integer i=0;i<2;i++){
            LLC_BI__Legal_Entities__c ent = new LLC_BI__Legal_Entities__c();
            ent.name ='Entity'+i;
            ent.LLC_BI__Loan__c = LoanList[0].id;
            ent.LLC_BI__Account__c = LoanList[0].LLC_BI__Account__c;
            ent.LLC_BI__Product_Package__c = LoanList[0].LLC_BI__Product_Package__c;
            ent.LLC_BI__Tax_ID__c ='1234567890';
            
            ent.LLC_BI__Borrower_Type__c = 'Guarantor';
            entListNew.add(ent);
           
        }
        insert entListNew;
       List<LLC_BI__Contingent_Liabilty__c> authListGuarantor = new List<LLC_BI__Contingent_Liabilty__c >();
       for(LLC_BI__Legal_Entities__c  en :entList){  
           LLC_BI__Contingent_Liabilty__c auth = new LLC_BI__Contingent_Liabilty__c();
           auth.LLC_BI__Entity__c = en.Id;
           auth.LLC_BI__Contact__c = conlist[0].Id;
           auth.LLC_BI__Authority__c = 'Authorized';
           auth.LLC_BI__Role__c = 'Sole Proprietorship';
           auth.LLC_BI__Contingent_Percentage__c = 5;
           authListGuarantor .add(auth);
       }
       insert authListGuarantor;

     }
     return LoanList;
     
     
     }
     
     }