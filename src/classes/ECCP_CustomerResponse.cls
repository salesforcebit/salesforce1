/*******************************************************************************************************************************************
* @name         :   ECCP_DSCCustomerButton
* @description  :   This class will be responsible for capturing response from the DSC Customer Service.
* @author       :   Aditya
* @createddate  :   5/May/2016
*******************************************************************************************************************************************/
public class ECCP_CustomerResponse{
     public ECCP_CustomerResponse(){}
     public String nAppID;
     public String TotalConsumerDepositBalance;
     public String TotalCommercialDepositBalance;
     public String TotalConsumerCreditExposure;
     public String TotalCommercialCreditExposure;
     public String ExposureCalAmount;
     public List<Acct> AcctData;
     
    
     public class Acct{
       // public Acct (){}
        public String AcctID;
        public String AccountType;
        public String AccountNumber;
        public String AccountName; // As per stubb response CustomerName at party level is same as Account Name 
        public String BankNumber;
        public String BalanceAmount;
        public String ChargeOffFlag;
        public String ExposureFlag;
        public String CommitmentAmount;
        public String OpenDate;
        public String ProductCode;
        public String SSN;          
        public String SubProductCode;
        public String AccountStatus;
        public String MaturityDate;
        public String CL3Obligation;
        public String CL3Obligor;
        public String CRACode;
        public String FacilityRR;
        public String LostGivenDefault;
        public String NSFCnt_12_M;
        public String Obligor_RR;
        public String Obligor_POD;
        public String ODCnt_12_M;
        public String PmtAmt;
        public String PmtDueDate;
        public String ReviewDate;
        public String TSYSNumber;
        public String DDA_6M_Ave_bal;
        // From CUAC 
        public String cuacRelationshipCode;
        public String accountIDREF;
        // From Party
        public String partySSN;
     }

}