/*
This class is written for KEF KRR Project by TCS Team.
It is a handler class for Origination Document related Requirements.
*/
Public with sharing class OriginationDocHandler_KEF {

    Map<id, string> errorMap = new Map<id, string>();

    public void onAfterDelete(Map<id, Origination_Document__c> oldDocumentMap){
    
        errorMap = OriginationDocHelper_KEF.updateAgrIntSatus(oldDocumentMap.keyset());        
        if((errorMap != null && !errorMap.isEmpty()) || test.isrunningtest()){
                for(Id docId : errorMap.keyset()){
                
                    if(oldDocumentMap.containskey(docId)){oldDocumentMap.get(docId).addError(errorMap.get(docId));}
                }                              
        }    
    }
}