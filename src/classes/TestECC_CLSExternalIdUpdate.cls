/*******************************************************************************************************************************************
* @name         :   TestECC_CLSExternalIdUpdate
* @description  :   This class will be responsible to create data for test classes.
* @author       :   Lakshmi Myneni
* @createddate  :   10/October/2016
*******************************************************************************************************************************************/
@isTest
public class TestECC_CLSExternalIdUpdate{

    public static testMethod void TestECC_CLSExternalIdUpdateClass(){
       List<LLC_BI__Legal_Entities__c> entityList = new List<LLC_BI__Legal_Entities__c>();
       List<ECC_Loan_Rate_Term__c> lrtList = new List<ECC_Loan_Rate_Term__c>();       
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
       User u = new User(Alias = 'stdKey', Email='standarduser@testorg.comKey', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, No_Validation__c=true,
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comKey',PLOB__c='Support');

       insert u;
           System.runAs(u){
               
           /************Create Account**************/
               account acc = new account(Name = 'Test Account',Type ='Business Prospect',tin__c='12345',LLC_BI__Tax_Identification_Number__c= '567908999',
                                          phone ='1234567890',Mobile_Phone__c='1234567890',Home_Phone__c='1234567890');
              
               insert acc;
                  
            /**************Create Product Package*******************/  
               LLC_BI__Product_Package__c pp = new LLC_BI__Product_Package__c();
                  pp.Name = acc.name;
                  PP.Bank_Division__c ='Business Banking';
                  pp.LLC_BI__Account__c = acc.id;
                  pp.LLC_BI__Stage__c = 'Application Processing';
                  pp.LLC_BI__Primary_Officer__c = u.Id;
                  pp.Credit_Authority_Name__c =u.Id;
                  pp.Delegated_Authority_Name__c  = u.Id;
                  insert pp; 
             
            /*****************Create Loan*****************/   
             
        
                 LLC_BI__Loan__c newLoan = new LLC_BI__Loan__c(); 
                 newLoan.name = 'LoanABCTest';
                 newLoan.LLC_BI__Stage__c = 'Qualification / Proposal';
                 newLoan.LLC_BI__Account__c = acc.Id;
                 newLoan.LLC_BI__Amount__c = 56789000;
                 newLoan.ECCP_Application_Number__c = '0977';
                 newLoan.LLC_BI__Product_Package__c = pp.Id;
                 newLoan.ECC_Rate_Description__c = '00170 Akron Stmt Sav';
                 newLoan.Loan_Purpose_Code__c  = '101 New Coml/cons aircraft - prop';  
                 insert newLoan; 
             
             /*****************Create Entity Involvement*****************/ 
                    LLC_BI__Legal_Entities__c ent = new LLC_BI__Legal_Entities__c();
                    ent.name ='Entity';
                    ent.LLC_BI__Loan__c = newLoan.id;
                    ent.LLC_BI__Account__c = newLoan.LLC_BI__Account__c;
                    ent.LLC_BI__Product_Package__c = newLoan.LLC_BI__Product_Package__c;           
                    ent.LLC_BI__Borrower_Type__c = 'Borrower';
                    ent.CLS_External_ID__c ='aaabbbcc';
                    entityList.add(ent);
                    
                    LLC_BI__Legal_Entities__c ent1 = new LLC_BI__Legal_Entities__c();
                    ent1.name ='Entity';
                    ent1.LLC_BI__Loan__c = newLoan.id;
                    ent1.LLC_BI__Account__c = newLoan.LLC_BI__Account__c;
                    ent1.LLC_BI__Product_Package__c = newLoan.LLC_BI__Product_Package__c;           
                    ent1.LLC_BI__Borrower_Type__c = 'Borrower';
                    ent1.CLS_External_ID__c ='aaabbbcc1';
                    entityList.add(ent1);
                    System.assert(entityList.size() > 0);                    
                      
                   insert entityList; 
                    
             /*****************Create Loan Rate Term *****************/ 
                  ECC_Loan_Rate_Term__c lrt = new ECC_Loan_Rate_Term__c();
                    lrt.Loan__c = newLoan.id;            
                    lrt.ECC_External_CLS_Processing_ID__c ='aaabbbcc';        
                    lrtList.add(lrt);
                     
                    ECC_Loan_Rate_Term__c lrt1 = new ECC_Loan_Rate_Term__c();
                    lrt1.Loan__c = newLoan.id;            
                    lrt1.ECC_External_CLS_Processing_ID__c ='aaabbbcc1'; 
                    lrtList.add(lrt1);     
                    insert lrtList; 
                   
            }  
             
     }
 }