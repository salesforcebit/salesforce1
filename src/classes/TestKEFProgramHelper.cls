/**
 * Created By - ApExlarator
 *
 * This is the test class for KEFProgramHelper
 */
@isTest (seeAllData=false)
private class TestKEFProgramHelper {

	/**
	 * Test method for KEFProgramHelper#UpdateAgreementNameBasedonProgramName().
	 */
	private static testmethod void testUpdateAgreementNameBasedonProgramName() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodUpdateAgreementNameBasedonProgramName();

				*/
					Test.startTest();

					KEFProgramHelper.UpdateAgreementNameBasedonProgramName(null);

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}
	/**
	 * Test method for KEFProgramHelper#populateMapForStatusChangeAudit().
	 */
	private static testmethod void testPopulateMapForStatusChangeAudit() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodpopulateMapForStatusChangeAudit();

				*/
					Test.startTest();

					KEFProgramHelper.populateMapForStatusChangeAudit();

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}
	/**
	 * Test method for KEFProgramHelper#processProgramforDRDataPopulation().
	 */
	private static testmethod void testProcessProgramforDRDataPopulation() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodprocessProgramforDRDataPopulation();

				*/
					Test.startTest();

					KEFProgramHelper.processProgramforDRDataPopulation(null, null);

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}
	/**
	 * Test method for KEFProgramHelper#populateStatusDateAndUserId().
	 */
	private static testmethod void testPopulateStatusDateAndUserId() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodpopulateStatusDateAndUserId();

				*/
					Test.startTest();

					KEFProgramHelper.populateStatusDateAndUserId(null, null);

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}
	/**
	 * Test method for KEFProgramHelper#UpdateNotesAndAttachment().
	 */
	private static testmethod void testUpdateNotesAndAttachment() {
		//Set up user
		 User user1 = [SELECT Id FROM User WHERE Username ='<<User>>'];
		//Run As user1
		 System.RunAs(user1)
			{
				try
				{
				/**
				* TO DO
				* Uncomment the below code or write your own code to call the Test Data Factory method to generate test data

				* SObject  testDataObject = TestDataFactory.testDataFactoryMethodUpdateNotesAndAttachment();

				*/
					Test.startTest();

					KEFProgramHelper.UpdateNotesAndAttachment(null);

					System.assert(null, null);
					Test.stopTest();
				}
				catch(Exception ex){}
			}
		}

}