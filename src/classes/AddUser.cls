//================================================================================
//  KeyBank  
//  Object: User 
//  Author: Offshore
//  Detail: AddUser Class
//  Description: This class is refrenced in the apex trigger Add to Group in order to add users automatically to public groups.
//================================================================================
//          Date            Purpose
// Changes: 12/17/2015      Initial Version
//================================================================================

public class AddUser
{
    @future
    public static void AddToGroups(Set<Id> userIds)
    {
        //Get the groups that the user should be added to
        Group g=[select Id from Group Where DeveloperName='Derivative_Users'];
        
        List<User> users=[Select Id,Name, Department from user Where Id IN :userIds];
                
        List<GroupMember>listGroupMember =new List<GroupMember>();  
        
        // loop the users that have been created
        for (User user : users)
        {
            // Derivative User added to Public Group
            if(user.department == 'CB KBCM Derivatives')
            {
                GroupMember gm= new GroupMember(); 
                gm.GroupId=g.id;
                gm.UserOrGroupId = user.id;
                listGroupMember.add(gm);
            } 
            insert listGroupMember;
            
            // Non-Derivative User removed from Public Group
            if(user.department != 'CB KBCM Derivatives')
            {
                List<GroupMember> gm = [SELECT GroupId,Id,UserOrGroupId FROM GroupMember WHERE UserOrGroupId =:user.id];              
                
                for (GroupMember gm1 : gm)
                {
                    List<GroupMember> listGroupMember1 = [SELECT Id FROM GroupMember WHERE GroupId =: g.id and UserOrGroupId =: user.id];
                    delete listGroupMember1;
                }
            } 
        }           
    }
}