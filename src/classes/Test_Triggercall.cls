@istest
Public class Test_Triggercall 
{
    static Profile p;
    static User testUser;
    static Account acc;
    static Account acc1;
    static contact con;
    static contact con1;
    static opportunity oppty;
    static opportunity oppty1;
    static Lead leadcreate;
    static Lead leadcreate1;
       
    public static testMethod void associatedaccounttest() 
    {
        p = [select id from profile where name='System Administrator'];
        testUser = new User(alias = 'u1', email='vidhyasagaran_muralidharany@keybank.com',
        emailencodingkey='UTF-8', lastname='Testing2015', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='vidhyasagaran_muralidharany@keybank.com',PLOB__c='support',PLOB_Alias__c='support');             
        
        insert testUser;
        List<Account> listAcc = new List<Account>();
        List<Contact> listCon = new List<Contact>();
        List<Opportunity> listOpp = new List<Opportunity>();
        List<Lead> lisLead = new List<Lead>();
        acc  = new account(name='acctest',phone='9962556525');
        listAcc.add(acc);
        
        acc1 = new account(name='acctest1',phone='9962556535');
        listAcc.add(acc1);
        
        insert listAcc;
        con = new contact(LastName='Mr. Dhanesh Subramanian',RACFID__c='HINDF', Officer_Code__c='HINDF',Contact_Type__c='Key Employee',
        AccountId=acc.id, Salesforce_User__c=true, User_Id__c=testUser.Id,phone='9876543210' );
        listCon.add(con);
        
        con1 = new contact(LastName='Mr. Dhanesh Subramanian1',RACFID__c='HINDF1', Officer_Code__c='HINDF1',Contact_Type__c='Key Employee',
        AccountId=acc1.id,phone='9876543210');
        listCon.add(con1);
        insert listCon;
        
        oppty = new opportunity(name ='optyTest',RecWeb_Number__c = '12589', StageName='Pursue', CloseDate=date.today(),AccountId=acc.id);
        listOpp.add(oppty);
        
        oppty1 = new opportunity(name ='optyTest1',RecWeb_Number__c = '12588', StageName='Pursue', CloseDate=date.today(),AccountId=acc1.id);
        listOpp.add(oppty1);
        insert listOpp ;
        
        leadcreate = new Lead(Lastname ='lead test', company = 'test', Associated_Account__c = acc.id);
        lisLead.add(leadcreate);
        
        leadcreate1 = new Lead(Lastname ='lead test1', company = 'test1', Associated_Account__c = acc1.id);
        lisLead.add(leadcreate1);
        insert lisLead;
        Map<String, Schema.SObjectType> accsObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs = accsObjectMap.get('Call__c') ;
        Schema.DescribeSObjectResult resSchemaAcc = accs.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo = resSchemaAcc.getRecordTypeInfosByName();
        Id RT = accRecordTypeInfo.get('Account Call').getRecordTypeId();
        System.Debug('RT:'+RT);
        
        Map<String, Schema.SObjectType> accsObjectMap1 = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs1 = accsObjectMap1.get('Call__c') ;
        Schema.DescribeSObjectResult resSchemaAcc1 = accs1.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo1 = resSchemaAcc1.getRecordTypeInfosByName();
        Id RT1 = accRecordTypeInfo1.get('Contact Call').getRecordTypeId();
        System.Debug('RT1:'+RT1);
        
        Map<String, Schema.SObjectType> accsObjectMap2 = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs2 = accsObjectMap2.get('Call__c') ;
        Schema.DescribeSObjectResult resSchemaAcc2 = accs2.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo2 = resSchemaAcc2.getRecordTypeInfosByName();
        Id RT2 = accRecordTypeInfo2.get('Lead Call').getRecordTypeId();
        System.Debug('RT2:'+RT2);
        
        Map<String, Schema.SObjectType> accsObjectMap3 = Schema.getGlobalDescribe() ;
        Schema.SObjectType accs3 = accsObjectMap3.get('Call__c') ;
        Schema.DescribeSObjectResult resSchemaAcc3 = accs3.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accRecordTypeInfo3 = resSchemaAcc3.getRecordTypeInfosByName();
        Id RT3 = accRecordTypeInfo3.get('Opportunity Call').getRecordTypeId();
        System.Debug('RT3:'+RT3);
        
        /*
        RecordType RT = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Call__c' and name='Account Call'];
        RecordType RT1 = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Call__c' and name='Contact Call'];
        RecordType RT2 = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Call__c' and name='Lead Call'];
        RecordType RT3 = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Call__c' and name='Opportunity Call'];
        */
        Call__c accountcall = new Call__c(Subject__c='test', RecordTypeId=RT, Call_Date__c=system.today(),Call_Type__c='COI Contact',AccountId__c=acc.id,Status__c='done');
        insert accountcall;
        
        Call__c contactcall = new Call__c(Subject__c='test',RecordTypeId=RT1, Call_Date__c=system.today(),Call_Type__c='COI Contact',ContactId__c=con.id,Status__c='done');
        insert contactcall;
        
        Call__c leadcall = new Call__c(Subject__c='test',RecordTypeId=RT2, Call_Date__c=system.today(),Call_Type__c='COI Contact',LeadId__c=leadcreate.id,Status__c='done');
        insert leadcall;
        
        Call__c opptycall = new Call__c(Subject__c='test',RecordTypeId=RT3, Call_Date__c=system.today(),Call_Type__c='COI Contact',OpportunityId__c=oppty.id,Status__c='done');
        insert opptycall;
        
        Call__c actest=[Select name,Associated_Account__c from Call__c where AccountId__c=:acc.Id];
        actest.Associated_Account__c=acc.Id;
        update actest;
        
        Call__c contest=[Select name,Associated_Account__c, ContactId__r.AccountId from Call__c where ContactId__c=:con.id];
        contest.Associated_Account__c=acc.id;      
        update contest;
        
        
        Call__c leadtest=[Select name,Associated_Account__c, LeadId__r.Associated_Account__c from Call__c where LeadId__c=:leadcreate.id];
        leadtest.Associated_Account__c=acc.id;
        update leadtest;
        
        Call__c optytest=[Select name,Associated_Account__c, OpportunityId__r.AccountId from Call__c where OpportunityId__c=:oppty.id];
        optytest.Associated_Account__c=acc.id;
        update optytest;
        
        delete actest;
    }   
    
    public static testMethod void Progtest()
    {
        Account acc3 = new account(name='acctest3',phone='9962556523');
        insert acc3;
        
        RecordType ART = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Call__c' and name='Account Call'];
        
        Call__c paccountcall = new Call__c(Subject__c='test3', RecordTypeId=ART.id, Call_Date__c=system.today(),Call_Type__c='COI Contact',AccountId__c=acc3.id,Status__c='done');
        insert paccountcall;
     
        RecordType RT4 = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Campaign' and name='Programs'];
        
        campaign camp = new campaign(Name = 'TestCampaign', type='Top 5', status='Completed', IsActive=true, recordtypeId= RT4.Id);
        insert camp;
        
        RecordType RT5 = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Program_Member__c' and name='Top 5 Program'];
        
        Program_Member__c pc = new Program_Member__c(CampaignId__c=camp.id, AccountId__c=acc3.id, recordtypeId=RT5.id);
        insert pc;  
        
        /*Program_call__c pgmcall = new Program_call__c(AccountName__c=acc3.id,  Call__c=paccountcall.id, Program_Member__c = pc.id);
        insert pgmcall;*/
    }
    
    public static testMethod void callhandler() 
    {
        Profile pc = [select id from profile where name='System Administrator'];
        User testUserc = new User(alias = 'u1', email='Dhanesh_Subramanianc@keybank.com',
        emailencodingkey='UTF-8', lastname='Dhanesh', languagelocalekey='en_US',
        localesidkey='en_US', profileid = pc.Id, country='United States',
        timezonesidkey='America/Los_Angeles', username='Dhanesh_Subramanianc@keybank.com',PLOB__c='support',
        PLOB_Alias__c='support', RACF_Number__c='HINDFC');             
        insert testUserc;
        
        Account accc = new account(name='acctestc',phone='8962556525');
        insert accc;
        
        RecordType RTc1 = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Contact' and name='Key Employee'];
        
        contact conc = new contact(LastName='Mr. Dhanesh Subramanianc',RACFID__c='HINDFC', Officer_Code__c='HINDFC',Contact_Type__c='Key Employee',
        AccountId=accc.id, Salesforce_User__c=true, User_Id__c=testUserc.Id, Recordtypeid=RTc1.id,Phone='9876543210' );
        insert conc;
        
        RecordType RTc = [SELECT Id, Name,SobjectType FROM RecordType where SobjectType ='Call__c' and name='Account Call'];
        
        Call__c accountcallc = new Call__c(Subject__c='testc', RecordTypeId=RTc.id, Call_Date__c=system.today(),Call_Type__c='COI Contact',AccountId__c=accc.id,Status__c='done', ownerId=testUserc.id);
        insert accountcallc;
        
        List<Participant__c> addparticipant = new List<Participant__c>();
        
        //Contact foundContact;
        
        if(conc.RACFID__c == testUserc.RACF_Number__c && conc.RACFID__c != null && testUserc.RACF_Number__c != null)
        
            
        
        
        
        {
            Participant__c  partc = new Participant__c();
            partc.CallId__c = accountcallc.Id;
            partc.Contact_Id__c = conc.Id;
            insert partc;
            //addparticipant.add(partc);
        }
        
       /* if(addparticipant .size()>0)
        {
            database.insert(addparticipant);
        }*/
        
      
        
     } 
}