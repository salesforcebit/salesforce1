//================================================================================
//  KeyBank  
//  Object: AccountTriggerHandler 
//  Author: Barney Young Jr.
//  Detail: Account Trigger Handler Class
//================================================================================
//          Date            Purpose
// Changes: 03/07/2014      Initial Version
//================================================================================


public with sharing class AccountTriggerHandler 
{
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public AccountTriggerHandler(boolean isExecuting, integer size)
    {
        m_isExecuting = isExecuting;
        BatchSize = size;          
    }   
    
    public void OnAfterDelete(List<Account> deletedAccounts)
    {    
        List<Account_Audit__c> deletedaccts = new List<Account_Audit__c>();
        
        for (Account acct:deletedAccounts)
        {
            If(acct.masterRecordId!=null)
            {
                Account_Audit__c Aaudit = new Account_Audit__c();
                Aaudit.Losing_Account_Name__c= acct.name;
                Aaudit.Losing_Account_Id__c = acct.id;
                Aaudit.Losing_Entity_Id__c = acct.Entity_Id_Backend__c;
                Aaudit.Losing_KEF_Entity_Id__c = acct.Entity_Id__c;
                Aaudit.Losing_Owner__c=acct.OwnerId;
                Aaudit.Merge_date_time__c=system.now();
                Aaudit.Merged_by_user__c=UserInfo.getName() ;
                deletedaccts.add(Aaudit);
            }
        }
        if(deletedaccts.size()>0)
        {
            database.insert(deletedaccts);
        }
    
    }
        @future public static void OnAfterDelete(Set<ID> accountIDs)
    {

    }  
    
    public void updateAccountonMerge(List<Account> acclist)
    {
        List<Id> accid= new List<Id>();
        List<Call__c> calllist = new List<Call__c>();
        
        integer count;
        for (Account a : acclist)
        {
                if (a.MasterRecordId != NULL)
                {
                    accid.add(a.MasterRecordId);
                }
         } 
         
         system.debug('***accid' + accid);
         count = [select count() from Call__c where Associated_Account__c in : accid];
         system.debug('***count***' + count);
         updateCount(count, accid);
            
         
     }
     
      @future public static void updateCount(Integer count, List<Id> accountIDs)
      {
         if(!Test.isRunningTest())
         {
             Account acc;
             acc = [Select id, Number_of_Calls__c from Account where id in : accountIDs];
             acc.Number_of_Calls__c = count;
             update acc;
         }
      }  
    
}