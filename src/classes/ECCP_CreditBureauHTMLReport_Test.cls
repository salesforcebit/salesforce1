@isTest
private class ECCP_CreditBureauHTMLReport_Test{
private static testmethod void CreateData(){
  Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
   User u = new User(Alias = 'stdKey', Email='standarduser@testorg.comKey', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.comKey',PLOB__c='Support');
   insert u;   
 System.runAs(u){
  contact con = new contact();
    con.Lastname = 'Mitra';
    con.Firstname = 'Alisha';
    con.Birthdate = Date.valueOf('1980-01-01');
    con.SSN__c = 'bnnpm333k';
    con.Phone = '1234567890';
    insert con;
    
    RecordType rt = [SELECT id, Name FROM RecordType WHERE SobjectType='Account' AND Name='Individual' limit 1];
    
    
    Account a = new account();
    a.name = 'Test Account';
    a.BillingStreet ='test street';
    a.BillingCity ='test city';
    a.BillingState ='test state';
    a.BillingPostalCode ='302';
    a.Contact_Name__c = con.id;
    a.recordTypeId = rt.id;
    a.type = 'Business Prospect';
    insert a;
    
    LLC_BI__Product_Package__c pp = new LLC_BI__Product_Package__c();
          pp.Name = a.name;
          PP.Bank_Division__c ='Business Banking';
          pp.LLC_BI__Account__c = a.id;
          pp.LLC_BI__Stage__c = 'Application Processing';
          pp.LLC_BI__Primary_Officer__c = u.Id;
          insert pp;
    
       LLC_BI__Loan__c newLoan = new LLC_BI__Loan__c(); 
         newLoan.name = 'Test Bureau';
         newLoan.LLC_BI__Stage__c = 'Application Processing' ;
         newLoan.LLC_BI__Account__c = a.id;
         newLoan.LLC_BI__Amount__c = 56789000;
         newLoan.ECCP_Application_Number__c = '0977';
         newLoan.LLC_BI__Product_Package__c = pp.id;
         newLoan.ECC_Rate_Description__c = '00170 Akron Stmt Sav';
         newLoan.Loan_Purpose_Code__c  = '101 New Coml/cons aircraft - prop';
         newLoan.LLC_BI__Pricing_Basis__c ='Variable';
    insert newLoan;
      
      
      ECCP_Credit_Bureau__c CBObj = new ECCP_Credit_Bureau__c();
      CBObj.Credit_Bureau_ID__c = '567899000';
      CBObj.Loan__c = newLoan.id;
      CBObj.Credit_Report__c = 'ncfjdsnvfjvfdkjvgfdkfdgkdfkvgfdkfdvncfvnkjfdnvjfdnvfnvfdnvdfnvfd';
      CBObj.Credit_Report_1__c = 'sdhfidsjfrgmfrotigotfihoimhogimhjomijohdbvcjndfnvfdsnjsnfjfjdfjdjfdjfdfjdfjjdfjdfjdfjdkgnfvjdnfdgnr';
      CBObj.Credit_Report_2__c = 'cbsdlfhdrnvfrnnreefoireutinrtirtirutihedewbvewbrevrewnweirnrrvuubcrewrreurereirueooreoroeoorooeooe';
      insert CBObj;
      
      Test.startTest();
      
      apexpages.currentpage().getparameters().put('id' , CBObj.Id);
      Apexpages.StandardController stdController = new Apexpages.StandardController(CBObj);
      ECCP_CreditBureauHTMLReport HTML_Rep = new ECCP_CreditBureauHTMLReport(stdcontroller);
      
      HTML_Rep.getContent();
      HTML_Rep.savePDF();
      Test.stopTest();
      
     }
   }  
 }