//================================================================================
//  KeyBank
//  Object: Opportunity
//  Author: West Monroe Partners
//  Detail: PasswordGenerator
//  Description :Generates a password.  Can encrypt or decrypt a password.

//================================================================================
//          Date            Purpose
// Changes: 04/25/2016      Initial Version
//================================================================================

public with sharing class PasswordGenerator {

	//returns an integer value in [min, max]
	private Integer generateRandomLength(Integer min, Integer max){
		return Min + (Integer)(Math.random() * ((Max - Min) + 1));
	}

	//returns a random special character from the special character array
	private String getSpecialCharacter(){
		List<String> specialCharacter = new List<String>{'@', '#', '$'};
		return specialCharacter.get(generateRandomLength(0, specialCharacter.size()-1));
	}

	public PasswordGenerator() {

	}

	//generates a random password with length between 12-24
	//requirements, needs atleast 2 alpha character
	//							needs atleast 2 numbers
	//							No special characters
	public String createPassword(){

		String pwd;
		Integer len = generateRandomLength(8, 12);
		Integer digitCount = len;
		Boolean repeatCharacter = false;
		System.debug('length ' + len);
		Integer count = 0;
		//generate passwords until there are at least 2 letters and 2 numbers
		do {
			Blob blobKey = crypto.generateAesKey(128);
			String key = EncodingUtil.convertToHex(blobKey);
			pwd = key.substring(0,len-1);
			System.debug('************ '+pwd);

			//count the number of numbers in the string.
			Pattern p = Pattern.compile('\\d');
			Matcher pm = p.matcher(pwd);
			System.debug('pwd ' + pwd);
			digitCount = 0;
			while (pm.find())
			   digitCount++;
			System.debug('num of nums ' + digitCount);

			//loop through the password and make sure that there are no repeat sequential characters like mm
			for (integer i = 0; i < pwd.length()-1; i++) {
			    if (pwd.substring(i,i+1) == pwd.substring(i+1, i+2)) {
			        repeatCharacter = true;
			        break;
			    }
			}


		} while(repeatCharacter && (2 > digitCount || digitCount > len-2));

		return pwd+getSpecialCharacter();

	}

	public String encryptPassword(String password){
		return Utility.encrypt(password);
	}

	public String decryptPassword(String password){
		return Utility.decrypt(password);
	}

	public String changeFromCaps(String str){
		return str.toLowerCase();
	}
}