@isTest
private class TROChangeServiceUserLevelRequest_UT {

  static testMethod void changeUserRequestTest() {
  	TROChangeServiceUserLevelRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_changeServiceUserLevelRequest_Port testNewSetup = new TROChangeServiceUserLevelRequest_v2.TROnCinoKTTIntegration_WebServices_Provider_changeServiceUserLevelRequest_Port();

  	TROChangeServiceUserLevelRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_changeUser_ExportFields();

  	Test.setMock(WebServiceMock.class, new TRO_ChangeServiceUserLevelRequest_Mock());

		Test.startTest();
  		TROChangeServiceUserLevelRequest_v2.KTTResponse response = testNewSetup.processCSULAPI(exportFields);
		Test.stopTest();

		System.assertEquals(response.CSOAutomation.KTTResponse.status, 'Good to go');

  }

  static testMethod void aSyncChangeUserRequestTest() {
        AsyncTROChangeServiceUserLevelRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_changeServiceUserLevelRequest_Port aSyncAddAccount = new AsyncTROChangeServiceUserLevelRequest_v2.AsyncTROnCinoKTTIntegration_WebServices_Provider_changeServiceUserLevelRequest_Port();

        TROChangeServiceUserLevelRequest_v2.ExportFields exportFields = TROUnitTestFactory.buildTest_changeUser_ExportFields();
        Continuation con = new Continuation(120);
        con.continuationMethod = 'processResponse';

        Test.setMock(WebServiceMock.class, new TRO_AsyncChangeUserRequest_Mock());

        AsyncTROChangeServiceUserLevelRequest_v2.processCSULAPIResponseFuture response = aSyncAddAccount.beginProcessCSULAPI(con, exportFields);

  }
}