/*******************************************************************************************************************************************
* @name         :   ECCP_CreditBureauContHelper
* @description  :   This class will be responsible for all the helper methods to create request, call the service and update response
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/

public class ECCP_CreditBureauContHelper{

public static list<errorWrapper> errlst = new list<errorWrapper>();
public static String exceptionStep;
public static String exceptionRecord;
private static Map<String,ECCP_WebService_Data__mdt> mapWsSettings;
private static ECCP_PerformanceLog__c plog;
private static map<String,ECCP_Error_Code__mdt> mapErr;

/*******************************************************************************************************************************************
* @method name  :   prepareData
* @description  :   Required to identify list of entities for which credit bureau not triggered; currently it's not using as the requirement is to every time create the CB report on click on clredt check
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/

public static list<LLC_BI__Legal_Entities__c> prepareData(ID loanId, String OrcCode){
    mapErr = ECCP_CustomMetadataUtil.queryErrorMetadata();
    list<LLC_BI__Legal_Entities__c> allEntLst = [select id,name,(select Service_Call_Flag__c from ECCP_Credit_Bureaues__r ) From LLC_BI__Legal_Entities__c where LLC_BI__Loan__c = :loanId];
    list<ID> finalEntLst = new list<ID>(); 
    for(LLC_BI__Legal_Entities__c c: allEntLst){
        /*  
            List<ECCP_Credit_Bureau__c> cB = c.getSObjects('ECCP_Credit_Bureaues__r');
            if ( cB == null || (cB !=null && cB.size() !=0 && cB.get(0).Service_Call_Flag__c == false) ){
                finalEntLst.add(c.Id);        
            } 
        */
        finalEntLst.add(c.Id); //only when required to run for all entities
    }
    // added by aditya LLC_BI__Loan__r.LLC_BI__Stage__c, Organization_Type__c
    list<LLC_BI__Legal_Entities__c> selectedEntDetailLst = [select id,LLC_BI__Account__r.recordType.Name,LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c,LLC_BI__Loan__c,LLC_BI__Loan__r.LLC_BI__Stage__c,
                                                            LLC_BI__Account__r.Contact_Name__r.SSN__c,LLC_BI__Account__c,LLC_BI__Account__r.Name,LLC_BI__Account__r.BillingStreet,LLC_BI__Account__r.BillingCity, LLC_BI__Account__r.BillingState,
                                                            LLC_BI__Account__r.BillingPostalCode,LLC_BI__Account__r.FirstName__c,LLC_BI__Account__r.LastName__c,LLC_BI__Account__r.Birthdate__c,
                                                            LLC_BI__Account__r.Home_Phone__c,LLC_BI__Account__r.Mobile_Phone__c,LLC_BI__Account__r.Organization_Type__c 
                                                            From LLC_BI__Legal_Entities__c 
                                                            where id in :finalEntLst and LLC_BI__Loan__c =:loanId];
    //system.debug('--prepareData--'+selectedEntDetailLst );
    return selectedEntDetailLst;
}//end of method

/*******************************************************************************************************************************************
* @name         :   prepareRequest
* @description  :   To create an request message for applicable individual customer
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/

public static list<requestWrapper> prepareRequest(list<LLC_BI__Legal_Entities__c> entityDetailLst, String orcCode){

    list<requestWrapper> requestObjlst = new list<requestWrapper>(); 
    
    for(LLC_BI__Legal_Entities__c ent : entityDetailLst){    
        requestWrapper reqData = new requestWrapper();        
        ECCP_CreditBureauRequest  req = new ECCP_CreditBureauRequest ();         
        errorWrapper errObj = new errorWrapper();
        if (ent.LLC_BI__Account__r.Organization_Type__c != ECCP_IntegrationConstants.accIndividualRecordType && ent.LLC_BI__Account__r.Organization_Type__c != ECCP_IntegrationConstants.SoleProprietorship )
        {
            errObj.entityId =  ent.Id;
            errObj.errorCode = ECCP_IntegrationConstants.Err07;
            errObj.errorDesc = mapErr.get(ECCP_IntegrationConstants.Err07).ECCP_Error_Message__c;
            errObj.acctname = ent.LLC_BI__Account__r.Name;
            errlst.add(errObj);
            continue;
        }
        // Aditya : 12 July 2016 removed ent.LLC_BI__Account__r.Birthdate__c == null from if condition 
        if(
        ((ent.LLC_BI__Account__r.Organization_Type__c == ECCP_IntegrationConstants.accIndividualRecordType || ent.LLC_BI__Account__r.Organization_Type__c == ECCP_IntegrationConstants.SoleProprietorship) && ( 
        (ent.LLC_BI__Account__r.LastName__c =='' || ent.LLC_BI__Account__r.LastName__c == null) ||
        (ent.LLC_BI__Account__r.FirstName__c =='' || ent.LLC_BI__Account__r.FirstName__c == null) ||
        (ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c =='' || ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c == null)||
        (ent.LLC_BI__Account__r.BillingStreet =='' || ent.LLC_BI__Account__r.BillingStreet == null) ||
        (ent.LLC_BI__Account__r.BillingCity =='' || ent.LLC_BI__Account__r.BillingCity == null) ||
        (ent.LLC_BI__Account__r.BillingState =='' || ent.LLC_BI__Account__r.BillingState == null) ||
        (ent.LLC_BI__Account__r.BillingPostalCode =='' || ent.LLC_BI__Account__r.BillingPostalCode == null)
        
       
        ))
        )    
        
        {   
            
            errObj.entityId =  ent.Id;
            errObj.errorCode = ECCP_IntegrationConstants.Err01;
            errObj.errorDesc = mapErr.get(ECCP_IntegrationConstants.Err01).ECCP_Error_Message__c;
            errObj.acctname = ent.LLC_BI__Account__r.Name;
            errlst.add(errObj);
            continue;
        }else{

            req.SSN = ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c;
            req.Orchestration_Code = orcCode;
            req.AccountName = ent.LLC_BI__Account__r.Name;
            req.BillingStreet = ent.LLC_BI__Account__r.BillingStreet;
            req.BillingCity = ent.LLC_BI__Account__r.BillingCity;
            req.BillingState = ent.LLC_BI__Account__r.BillingState;
            //req.BillingPostalcode = ent.LLC_BI__Account__r.BillingPostalCode;
            req.BillingPostalcode = ent.LLC_BI__Account__r.BillingPostalCode.contains('-') ? ent.LLC_BI__Account__r.BillingPostalCode.remove('-') : ent.LLC_BI__Account__r.BillingPostalCode;  
            req.FirstName = ent.LLC_BI__Account__r.FirstName__c;
            req.LastName= ent.LLC_BI__Account__r.LastName__c ;
            //Aditya 14 july 2016
            req.Birthdate = ent.LLC_BI__Account__r.Birthdate__c !=null ? String.valueOf(ent.LLC_BI__Account__r.Birthdate__c) : '';
            req.HomePhone = ent.LLC_BI__Account__r.Home_Phone__c != null?ent.LLC_BI__Account__r.Home_Phone__c.remove('(').remove(')').remove('-').deleteWhitespace() :'0';
            req.MobilePhone = ent.LLC_BI__Account__r.Mobile_Phone__c !=null?ent.LLC_BI__Account__r.Mobile_Phone__c.remove('(').remove(')').remove('-').deleteWhitespace() :'0';
            reqData.reqObj = req ;
            reqData.loanid = ent.LLC_BI__Loan__c;
            reqData.entityId = ent.Id;
            reqData.acctName = ent.LLC_BI__Account__r.Name;
            requestObjlst.add(reqData);
            }//end of else
    }//end of for loop
    system.debug('--prepareRequest--'+requestObjlst);
    return requestObjlst;
}//end of method

/*******************************************************************************************************************************************
* @name         :   serviceCallout
* @description  :   To call the service for applicable request
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/

public static list<responseWrapper> serviceCallout(list<requestWrapper> reqList){
    ECCP_CreditBureausHelper service = new ECCP_CreditBureausHelper();
    list<responseWrapper> resWrapperlst = new list<responseWrapper>();
    for(requestWrapper wsReq : reqList ){
        errorWrapper  errObj = new errorWrapper();
        ECCP_CreditBureauResponse wsRes = new ECCP_CreditBureauResponse();
        ECCP_CreditBureauResponse wsResTemp = new ECCP_CreditBureauResponse();
        mapWsSettings = ECCP_CustomMetadataUtil.queryMetadata(ECCP_IntegrationConstants.CreditBureauMataData);
        
        //plog = ECCP_UTIL_PerformanceLog.createPerformanceLog(ECCP_IntegrationConstants.serviceCreditBureau,  mapWsSettings.get(ECCP_IntegrationConstants.serviceCreditBureau).EndPointUrl__c,ECCP_IntegrationConstants.serviceCreditBureau,JSON.Serialize(wsReq.reqObj));
        
        try{ responseWrapper resWrapper = new responseWrapper();
             if(!Test.isRunningTest()){
            //system.debug('--wsReq.reqObj--'+wsReq.reqObj); 
            wsRes = service.ECCP_CreditBureauProcess(wsReq.reqObj);
            }
            else{ wsRes = ECCP_CreditBureau_WebSvcCallout.CreditBureauCallout(wsReq.reqObj);
           
            }
            
            resWrapper.resObj = wsRes;
            resWrapper.loanId =wsReq.loanId;
            resWrapper.entityid = wsReq.entityId;
            resWrapper.acctName = wsReq.acctName;
            resWrapperlst.add(resWrapper);
            if(Test.isRunningTest()){
                resWrapper = new responseWrapper();
                wsRes = service.ECCP_CreditBureauProcess(wsReq.reqObj);
                resWrapper.loanId =wsReq.loanId;
                resWrapper.entityid = wsReq.entityId;
                resWrapper.acctName = wsReq.acctName;                
                resWrapper.resObj = wsRes;            
                resWrapperlst.add(resWrapper);            
            }
            
            // store performance log
            //ECCP_UTIL_PerformanceLog.updatePerformanceLog(plog, JSON.serialize(wsResTemp));

         }catch(CalloutException CE){
            errObj.entityId =  wsReq.entityId;
            errObj.errorCode = ECCP_IntegrationConstants.Err04;
            errObj.errorDesc = mapErr.get(ECCP_IntegrationConstants.Err04).ECCP_Error_Message__c;
            errObj.acctname = wsReq.acctName;
            errlst.add(errObj);
            exceptionRecord = wsReq.entityId;
            exceptionStep = 'serviceCallout';
            //create exception log before because in between service callout it does not allow to any DML operation
            Map<String,ECCP_Error_Code__mdt>  errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.dmlException);
            ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.serviceCreditBureau, 'ECCP_CreditBureauContHelper', exceptionStep, String.valueOf(CE.getLineNumber()),CE.getMessage(), exceptionRecord,CE.getStackTraceString());

            continue;
            ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
            // store performance log
            //ECCP_UTIL_PerformanceLog.updatePerformanceLog(plog, JSON.serialize(wsRes));
         }//catch End   
    } //end of for loop
    return resWrapperlst;
}//end of method

/*******************************************************************************************************************************************
* @name         :   processResponse
* @description  :   To call process the response and update in database
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/

public static void processResponse(list<responseWrapper> resLst){
    ECCP_Credit_Bureau__c creditBuObj;
    list<ECCP_Credit_Bureau__c> creditBuLst;
    creditBuLst = new list<ECCP_Credit_Bureau__c>();
    //temp error    
    list<errorWrapper> temperrlst = new list<errorWrapper>();
    for(responseWrapper wsRes : resLst){
        creditBuObj = new ECCP_Credit_Bureau__c();
        errorWrapper   temperrObj = new errorWrapper();
        creditBuObj.CBSSN__c = wsRes.resObj.SSN ;        
        creditBuObj.Orchestration_Code__c = wsRes.resObj.Orchestration_Code !=null && wsRes.resObj.Orchestration_Code !='' ? Integer.valueOf(wsRes.resObj.Orchestration_Code) : null ;
        creditBuObj.Bureau_Fraud__c= wsRes.resObj.Bureau_Fraud;
        creditBuObj.Credit_Bureau_ID__c=wsRes.resObj.Credit_Bureau_ID;        
        creditBuObj.Credit_Bureau_Reason_Code_1__c= wsRes.resObj.Credit_Bureau_Reason_Code_1;
        creditBuObj.Credit_Bureau_Reason_Code_Description_1__c=wsRes.resObj.Credit_Bureau_Reason_Code_Description_1;        
        creditBuObj.Credit_Bureau_Reason_Code_2__c=wsRes.resObj.Credit_Bureau_Reason_Code_2;
        creditBuObj.Credit_Bureau_Reason_Code_Description_2__c=wsRes.resObj.Credit_Bureau_Reason_Code_Description_2;        
        creditBuObj.Credit_Bureau_Reason_code_3__c=wsRes.resObj.Credit_Bureau_Reason_Code_3;
        creditBuObj.Credit_Bureau_Reason_Code_Description_3__c=wsRes.resObj.Credit_Bureau_Reason_Code_Description_3;
        creditBuObj.Credit_Bureau_Reason_code_4__c=wsRes.resObj.Credit_Bureau_Reason_Code_4;
        creditBuObj.Credit_Bureau_Reason_Code_Description_4__c=wsRes.resObj.Credit_Bureau_Reason_Code_Description_4;
        creditBuObj.Credit_Bureau_Reason_code_5__c=wsRes.resObj.Credit_Bureau_Reason_Code_5;
        creditBuObj.Credit_Bureau_Reason_Code_Description_5__c=wsRes.resObj.Credit_Bureau_Reason_Code_Description_5;        
        creditBuObj.Credit_Active_Date__c=wsRes.resObj.Credit_Active_Date !=null && wsRes.resObj.Credit_Active_Date !=''?  Date.valueOf(wsRes.resObj.Credit_Active_Date) :null;
        creditBuObj.Credit_Bureau_Address_Discrepancy__c=wsRes.resObj.Credit_Bureau_Addr_Discrepancy;
        creditBuObj.Credit_Bureau_FA_Request_Type__c=wsRes.resObj.Credit_Bureau_FA_Request_Type;
        
        //report splitting        
        String CreditRep = wsRes.resObj.Credit_Report;
        list<integer> lstInt = splitReport(CreditRep);
        try{
            creditBuObj.Credit_Report__c = CreditRep.substring(lstInt.get(0),lstInt.get(1));
            creditBuObj.Credit_Report_1__c = CreditRep.substring(lstInt.get(1)+1,lstInt.get(2));
            creditBuObj.Credit_Report_2__c = CreditRep.substring(lstInt.get(2)+1,lstInt.get(3));
        }catch(exception e){}
        // Aditya : added default value for credit score
        creditBuObj.CreditScore__c=wsRes.resObj.CreditScore != null && wsRes.resObj.CreditScore != '' ? Integer.valueOf(wsRes.resObj.CreditScore) : Integer.valueOf(ECCP_IntegrationConstants.CreditScore);        
        creditBuObj.Hawk_Alert_Fraud_Code__c=wsRes.resObj.Hawk_Alert_Fraud_Code;
        creditBuObj.Identity_Scan_Fraud_Code__c=wsRes.resObj.ID_Scan_Fraud_Code;        
        creditBuObj.Patriot_Fraud_Indicator__c=wsRes.resObj.Patriot_Fraud_Indicator;
        creditBuObj.Report_Date__c=wsRes.resObj.ReportDate !=null && wsRes.resObj.ReportDate !='' ? Date.valueOf(wsRes.resObj.ReportDate):null;
        creditBuObj.Service_Call_Flag__c=true;
        
        creditBuObj.External_Error_Code__c=wsRes.resObj.errorcode;
        creditBuObj.External_Error_Description__c=wsRes.resObj.errordesc;  
        
        creditBuObj.Loan__c=wsRes.loanid;
        creditBuObj.Entity_Involvement__c=wsRes.entityid;
        // Addded by Aditya : 2August2016 : Release 1.1 : To get BureauInstruct1 and BureauInstruct2 from response
        creditBuObj.Bureau_Instructions_1__c = wsRes.resObj.BureauInstruct1;
        creditBuObj.Bureau_Instructions_2__c = wsRes.resObj.BureauInstruct2;
        creditBuLst.add(creditBuObj);
        //Keeping success in temp and will set in main err list after success insert
        temperrObj.entityId =  wsRes.entityid;
        temperrObj.errorCode = ECCP_IntegrationConstants.Err05;
        temperrObj.errorDesc = mapErr.get(ECCP_IntegrationConstants.Err05).ECCP_Error_Message__c;
        temperrObj.acctName=  wsRes.acctName;        
        temperrlst.add(temperrObj);
    }//end of for loop
    insert creditBuLst;
    //Assing with list of success insert
    errlst.addAll(temperrlst);   
    //system.debug('--ProcessResponse--'+creditBuLst);    
}//end of method

/*******************************************************************************************************************************************
* @name         :   ServiceCalloutMain
* @description  :   Main class which calls from controller
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/
public static list<errorWrapper> ServiceCalloutMain(ID loanId, String OrcCode){
    list<LLC_BI__Legal_Entities__c> entObjlst = new list<LLC_BI__Legal_Entities__c> ();
    list<requestWrapper> reqWlst = new list<requestWrapper>();
    list<responseWrapper> resWlst = new list<responseWrapper>();
    try{
        entObjlst =  prepareData(loanId,OrcCode);
        system.debug('--CB prepareData list--'+entObjlst );
        exceptionStep= '--prepareData--';            
        if(entObjlst == null || entObjlst.size() == 0){
            errorWrapper   exErrObj = new errorWrapper();
            exErrObj.entityId =  loanId; //loan id for failure
            exErrObj.errorCode = ECCP_IntegrationConstants.Err06;
            exErrObj.errorDesc = mapErr.get(ECCP_IntegrationConstants.Err06).ECCP_Error_Message__c;
            exErrObj.acctname = '';
            errlst.add(exErrObj );
        } 
        
        if(entObjlst != null && entObjlst.size()>0) {    
            reqWlst =  prepareRequest(entObjlst , orcCode);
            system.debug('--CB prepareRequest Lst --'+entObjlst );
            exceptionStep= '--prepareRequest--';
        }
        
        if(reqWlst != null && reqWlst.size()>0){
            resWlst  =  serviceCallout(reqWlst);
            system.debug('--CB serviceCallout response List --');
            exceptionStep= '--serviceCallout--';
        }
        
        if(resWlst  != null && resWlst.size()>0){    
            processResponse(resWlst);
            system.debug('--CB processResponse List --');
            exceptionStep= '--processResponse--';
        }   
        system.debug('--CB Error List on success --'+errlst);
        return errlst;
    }catch(Exception ex){
        //clear error list and add generic message about failure
        errlst.clear();        
        errorWrapper   exErrObj = new errorWrapper();
        exErrObj.entityId =  loanId; //loan id for failure
        exErrObj.errorCode = ECCP_IntegrationConstants.Err02;
        exErrObj.errorDesc = mapErr.get(ECCP_IntegrationConstants.Err02).ECCP_Error_Message__c;
        exErrObj.acctname = '';
        errlst.add(exErrObj );        
        exceptionRecord = loanId;
        Map<String,ECCP_Error_Code__mdt>  errorMessageMap = ECCP_CustomMetadataUtil.errorMessage(ECCP_IntegrationConstants.dmlException);
        ECCP_UTIL_ApplicationErrorLog.createExceptionLog(ECCP_IntegrationConstants.serviceCreditBureau, 'ECCP_CreditBureauContHelper', exceptionStep, String.valueOf(ex.getLineNumber()),ex.getMessage(), exceptionRecord,ex.getStackTraceString());
        ECCP_UTIL_ApplicationErrorLog.saveExceptionLog();
        system.debug('--CB Error List on failure--'+errlst);
        return errlst;
    }//end of catch Exception
}

public class requestWrapper{
    ECCP_CreditBureauRequest reqObj;
    ID loanId;
    ID entityId;
    string acctName;
}

public class responseWrapper{
    public ECCP_CreditBureauResponse resObj;
    public ID loanId;
    public ID entityId;
    public string acctName;
}

public class errorWrapper{
    public String errorCode;
    public String errorDesc;
    public string acctName;
    public Id entityId;
}

/*******************************************************************************************************************************************
* @name         :   splitReport
* @description  :   To calculate the length of credit report and split the lenght which can be accomodated in max salesforce length fields
* @author       :   Ashish Agrawal
* @createddate  :   06/05/2016
*******************************************************************************************************************************************/
public static list<integer> splitReport(String report){
    string s = report;
    /* keep salesforce fields level max lenght; making length more than that could cause markup error*/
    integer maxLen = 100000; 
    integer totalLen = s!=null && s !=''? s.length():null;
    system.debug('--CB Total report Length --'+totalLen);
    integer tempLen =totalLen;
    list<integer> splitLen = new list<integer>();
    for(integer i=0;i<=totalLen;i=i+maxLen){
        if( totalLen > i){
            splitLen.add(i);
            tempLen= totallen-maxlen;
        }else{
            splitLen.add(i);
            break;
        }
    }
   
    splitLen.add(totalLen);
    //system.debug('--'+splitLen);
    return splitLen;
}

}