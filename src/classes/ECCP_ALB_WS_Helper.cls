/*******************************************************************************************************************************************
* @name         :   ECCP_ALB_WS_Helper
* @description  :   This is the  helper methods for LPL webservice
*                   controller for Integration, Parsing, Page redirection etc.
* @author       :   Nagarjun
* @createddate  :   19/05/2016
*******************************************************************************************************************************************/
public class ECCP_ALB_WS_Helper{

   // public ECCP_LPL_ResponseData resObj;
    public static integer noRequests = 0;
    public ECCP_ALB_Int_Data reqObj;
    private ECCP_PerformanceLog__c plog; 
    private String currentClassName;   
    private List<ECCP_ALB_RequestData.GuarantorDataType> guarantorDataList;
    private List<ECCP_ALB_RequestData.SignerDataType> signerDataList;
    private List<ECCP_ALB_RequestData.BorrowerDataType> borrowerDataList;
    //Get Service custom setting
    private Map<String,ECCP_WebService_Data__mdt> mapWsSettings;
    public ECCP_ALB_WS_Helper(){
        // Getting current class name
        currentClassName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));            
        mapWsSettings = ECCP_CustomMetadataUtil.queryMetadata(ECCP_IntegrationConstants.LPLService);        
       // resObj = new ECCP_LPL_ResponseData ();         
    }
     
/*******************************************************************************************************************************************
* @name         :   ECCP_LPL_Process
* @description  :   This method is responsible for communicating with Extenal System 'LPL'.
*                   This method will take the input from UI and fetch the data from LPL.
* @param        :   reqObj  -   ECCP_ALB_Int_Data
*                   A custom wrapper for ECCP_ALB_Int_Data.
* @return       :   resObj  -   ECCP_LPL_ResponseData
*                   A custom wrapper for LPL response.
* @author       :   Nagarjun
* @createddate  :   19/05/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/  
    public ECCP_ALB_RequestData ECCP_ALBProcess(ECCP_ALB_Int_Data reqObj){
        
        ECCP_ALB_RequestData responseElement = new ECCP_ALB_RequestData();                                 
        responseElement = getServiceRequestElements(reqObj);   
            system.debug('  +++responseElement  ' + responseElement  );  
            system.debug('---response Data' +JSON.serialize(responseElement));
                         
        return responseElement;
    }//end of method
    
/*******************************************************************************************************************************************
* @name         :   getServiceRequestElements
* @description  :   This method will set request parameters.
* @return       :   service  -   ECCP_ALB_RequestData
*                   A service instance.
* @author       :   Nagarjun
* @createddate  :   19/05/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/      
    private ECCP_ALB_RequestData getServiceRequestElements(ECCP_ALB_Int_Data reqObj){
    
        ECCP_ALB_RequestData requestType = new ECCP_ALB_RequestData ();
        //ECCP_ALB_RequestData.GeneralDataType reqGenData = new ECCP_ALB_RequestData.GeneralDataType();
        guarantorDataList = new List<ECCP_ALB_RequestData.GuarantorDataType>();
        signerDataList = new List<ECCP_ALB_RequestData.SignerDataType>();
        borrowerDataList = new List<ECCP_ALB_RequestData.BorrowerDataType>();        
        List<ECCP_ALB_RequestData.CustomDataType> reqCustomData = getcustomData(reqObj);
        getguarantorData(reqObj);
        List<ECCP_ALB_RequestData.GuarantorDataType> reqGuarantorData = guarantorDataList;
        List<ECCP_ALB_RequestData.SignerDataType> reqSignerData = signerDataList;
        list<ECCP_ALB_RequestData.GeneralDataType> reqGenData = getGenData(reqObj);
        List<ECCP_ALB_RequestData.CollateralDataType> reqCollateralData = getcollateralData(reqObj);
        List<ECCP_ALB_RequestData.DisbursementDataType> reqDisbursementData = getdisbursementData(reqObj);
        
        List<ECCP_ALB_RequestData.FeeInfoType> reqfeeInfo = getFeeInfo(reqObj);
        List<ECCP_ALB_RequestData.BorrowerDataType> reqborrowerData = borrowerDataList;
        requestType.GeneralData  = reqGenData;   
        requestType.CustomData= reqCustomData[0];
        requestType.SignerData=reqSignerData;
        requestType.GuarantorData=reqGuarantorData ;
        requestType.CollateralData=reqCollateralData;
        //requestType.CollateralGuarantorInformation=getCollateralGuarData;
        requestType.DisbursementData=reqDisbursementData;
        requestType.FeeInfo=reqfeeInfo;
        requestType.BorrowerData=reqborrowerData ;            
        return requestType;
      } 
/*******************************************************************************************************************************************
* @name         :   genDataList 
* @description  :   This method will set the general data.
* @return       :   genDataList 
* @author       :   Nagarjun
* @createddate  :   19/05/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    private List<ECCP_ALB_RequestData.GeneralDataType> getGenData(ECCP_ALB_Int_Data reqObj){
        List<ECCP_ALB_RequestData.GeneralDataType> genDataList = new List<ECCP_ALB_RequestData.GeneralDataType>();
        ECCP_ALB_RequestData.GeneralDataType genDataIns = new ECCP_ALB_RequestData.GeneralDataType();
        genDataIns.OriginalAmount = reqObj.OriginalAmount;
        genDataIns.RecordID = reqObj.RecordID;
        genDataIns.AutomaticLoanPayment = reqObj.AutomaticLoanPayment;
        genDataIns.BorrowerState = reqObj.BorrowerState;
        genDataIns.Branch = reqObj.Branch;
        genDataIns.RiskType = reqObj.RiskType;
        genDataIns.AssignmentUnit = reqObj.AssignmentUnit;
        genDataIns.ProposedObligorRiskRating = reqObj.ProposedObligorRiskRating;
        genDataIns.InterestRate = reqObj.InterestRate;
        genDataIns.FirstPaymentDate = reqObj.FirstPaymentDate;
        genDataIns.InterestType = NULL;
        genDataIns.PaymentType = reqObj.PaymentType;
        genDataIns.InitialLoanInterestRate = reqObj.InitialLoanInterestRate;
        genDataIns.NumberofPayments = reqObj.NumberofPayments;
        genDataIns.PaymentSchedule = reqObj.PaymentSchedule;
        genDataIns.Margin = reqObj.Margin;
        genDataIns.PeriodstoAmortize = reqObj.PeriodstoAmortize;
        genDataIns.RateCeiling = reqObj.RateCeiling;
        genDataIns.RateFloor = reqObj.RateFloor;
        genDataIns.RateType = reqObj.RateType;
        genDataIns.LoanPurposeCode = reqObj.LoanPurposeCode;
        genDataIns.ProductKey= reqObj.ProductKey; 
        genDataIns.CRACode= reqObj.CRACode; 
        genDataIns.RateDescription= reqObj.RateDescription; 
        
         //Release 1.1
        genDataIns.PaymentDueDate =  reqObj.PaymentDueDate;
        genDataIns.ClosingDate =  reqObj.ClosingDate;
        //Commented in Release 2 
       // if(reqObj.Covenants != NULL ) if(!reqObj.Covenants.isEmpty()) genDataIns.Covenants.addAll(reqObj.Covenants);
        genDataIns.BankNumber =  reqObj.BankNumber;
        genDataIns.ObligationNumber =    reqObj.ObligationNumber ;   
         //Release 2.0
        genDataIns.RMOfficerCode = reqObj.RMOfficerCode;
        genDataIns.InsiderLoanYN = reqObj.InsiderLoanYN;
        genDataIns.RestrictAccessYN = reqObj.RestrictAccessYN;
        genDataIns.RegOLoan = reqObj.RegOLoan;
        genDataIns.PrePmtPenaltyYN = reqObj.PrePmtPenaltyYN;
        genDataIns.DDAAccountNumber = reqObj.DDAAccountNumber;
        genDataIns.LoanAgreementType = reqObj.LoanAgreementType;
        genDataIns.ObligationRiskRating = reqObj.ObligationRiskRating;
        genDataIns.NewMoney = reqObj.NewMoney;
        genDataIns.CreditRatingDate = reqObj.CreditRatingDate;
        genDataIns.ProbabilityofDefault = reqObj.ProbabilityofDefault;
        genDataIns.DateRated = reqObj.DateRated;
        genDataIns.LossGivenDefault = reqObj.LossGivenDefault;
        //Need Xref
        genDataIns.PrepaymentPenaltyCode = reqObj.PrepaymentPenaltyCode;
        genDataIns.FirstInterestPaymentDate = reqObj.FirstInterestPaymentDate;
        genDataIns.InterestPaymentSchedule =  reqObj.InterestPaymentSchedule;
        genDataList.add(genDataIns);
        return genDataList ;
    }      
/*******************************************************************************************************************************************
* @name         :   customDataList 
* @description  :   This method will set the Customer data.
* @return       :   customDataList 
* @author       :   Nagarjun
* @createddate  :   19/05/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    private List<ECCP_ALB_RequestData.CustomDataType> getcustomData(ECCP_ALB_Int_Data reqObj){
        List<ECCP_ALB_RequestData.CustomDataType> customDataList = new List<ECCP_ALB_RequestData.CustomDataType>();
        //Release 1.1
        
       
        
        ECCP_ALB_RequestData.CustomDataType customDataIns = new ECCP_ALB_RequestData.CustomDataType();
       /* Aditya Commented Release 2.0
        customDataIns.DateofLease= new list<date>();
        customDataIns.StreetAddress = new list<string>();
        customDataIns.City= new list<string>();
        customDataIns.State= new list<string>();
        customDataIns.Zipcode= new list<string>();
        */
        customDataIns.HostSystem = 'nCino';
        customDataIns.CRASalesCode = reqObj.CRASalesCode;
        customDataIns.LoanAgreementEndDate = reqObj.LoanAgreementEndDate;
        // Aditya Commented to save class for Release 1.1
       // customDataIns.CertificateDeliveryGracePeriod = reqObj.CertificateDeliveryGracePeriod;
        customDataIns.RequireAnnualStatements = reqObj.RequireAnnualStatements;
        customDataIns.AnnualStatementPreparedby = reqObj.AnnualStatementPreparedby;
        customDataIns.RequireInterimStatements = reqObj.RequireInterimStatements;
        customDataIns.RequireCopiesofTaxReturns = reqObj.RequireCopiesofTaxReturns;        
        customDataIns.DueDaysforTaxReturns = reqObj.DueDaysforTaxReturns != null && reqObj.DueDaysforTaxReturns !=''? Decimal.valueOf(reqObj.DueDaysforTaxReturns): null;
        // Aditya Commented to save class for Release 1.1
      /*  customDataIns.DateofLease = reqObj.DateofLease;
        customDataIns.StreetAddress = NULL;
        customDataIns.City = NULL;
        customDataIns.State = NULL;
        customDataIns.Zipcode = NULL;
      */
        customDataIns.MaximumLoanAmount = reqObj.MaximumLoanAmount;
        customDataIns.TotalAmountBorrowerOwestoCreditor = reqObj.TotalAmountBorrowerOwestoCreditor;
        customDataIns.InterestOnlyPayments = reqObj.InterestOnlyPayments;
        customDataIns.PeriodicPaymentDetails = string.valueof(reqObj.PeriodicPaymentDetails);
        customDataIns.CreditorPaymentAmount = reqObj.CreditorPaymentAmount;
        customDataIns.PrincipalPaymentFrequency = reqObj.PrincipalPaymentFrequency;  
        //release 1.1
        customDataIns.MaximumAvailability =  reqObj.MaximumAvailability;
        customDataIns.MaximumAgeofEligibleAccounts =  reqObj.MaximumAgeofEligibleAccounts!= NULL && reqObj.MaximumAgeofEligibleAccounts != '' ? Decimal.valueOf(reqObj.MaximumAgeofEligibleAccounts) : null;
        customDataIns.AdditionalLimitEligibleAccounts =  reqObj.AdditionalLimitEligibleAccounts !=NULL && reqObj.AdditionalLimitEligibleAccounts !='' ? Decimal.valueOf(reqObj.AdditionalLimitEligibleAccounts) : null;
        customDataIns.InventoryEligibleExclusions =  reqObj.InventoryEligibleExclusions;
        customDataIns.EligibleInventoryAdditionalLimitations =  reqObj.EligibleInventoryAdditionalLimitations;
        //release 2.0
        if(reqObj.LandlordsConsent!=NULL)
        customDataIns.LandlordsConsent = getLandLordDetails(reqObj.LandlordsConsent);
        system.debug('---getCovenantDetails(reqObj.Covenants)--'+reqObj.Covenants);
        if(reqObj.Covenants != null)
        customDataIns.Covenant = getCovenantDetails(reqObj.Covenants);
        /* Aditya Commented Release 2.0
        if(reqObj.DateofLease != NULL ) if(!reqObj.DateofLease.isEmpty() ) customDataIns.DateOfLease.addAll(reqObj.DateOfLease);
        if(reqObj.StreetAddress != NULL ) if(!reqObj.StreetAddress.isEmpty()) customDataIns.StreetAddress.addAll(reqObj.StreetAddress);
        if(reqObj.City != NULL ) if(!reqObj.City.isEmpty()) customDataIns.City.addAll(reqObj.City);
        if(reqObj.State != NULL ) if(!reqObj.State.isEmpty()) customDataIns.State.addAll(reqObj.State);
        if(reqObj.Zipcode != NULL ) if(!reqObj.Zipcode.isEmpty()) customDataIns.Zipcode.addAll(reqObj.Zipcode);      
        */        
        customDataList.add(customDataIns);
        return customDataList ;
    }
/*******************************************************************************************************************************************
* @name         :   getCovenantDetails
* @description  :   This method will set the Covenant Details.
* @return       :   covDetList
* @author       :   Nagarjun
* @createddate  :   10/09/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    private List<ECCP_ALB_RequestData.CovenantType> getCovenantDetails(list<ECCP_ALB_Int_Data.CovenantType> CovenantsList ){  
        system.debug('---CovenantsList --'+CovenantsList );   
        list<ECCP_ALB_RequestData.CovenantType> covDetList =  new list<ECCP_ALB_RequestData.CovenantType>();
        ECCP_ALB_RequestData.CovenantType covElem;
        if(CovenantsList!= NULL){
        for(ECCP_ALB_Int_Data.CovenantType covDetIns : CovenantsList ){
            covElem = new ECCP_ALB_RequestData.CovenantType();  
            covElem.CovenantId = covDetIns.CovenantId;
            covElem.ComplianceCertificateDeliveryFrequency = covDetIns.ComplianceCertificateDeliveryFrequency;
            covElem.DueXDaysAfterFYEAnnual = covDetIns.covenanttype == 'interim' ? covDetIns.DueXDaysAfterFYEAnnual : NULL;
            covElem.IntermStatementPreparedby = covDetIns.IntermStatementPreparedby;
            covElem.DueXDaysAfterInterm = covDetIns.DueXDaysAfterInterm;
            covElem.IntermStatementDelivered = covDetIns.covenanttype == 'interim' ? covDetIns.IntermStatementDelivered : NULL;
            covElem.WhoPreparesTaxReturns = covDetIns.covenanttype == 'interim' ? covDetIns.WhoPreparesTaxReturns : NULL;
            list<string> covTypeLst = new List<string>();
            covTypeLst.add(covDetIns.CovenantCode);
            covElem.Covenants = covTypeLst;            
            //covElem.Covenants.addAll(covDetIns.Covenants);            
            covDetList .add( covElem );
            }
        }
        return covDetList; 
    }  
    
/*******************************************************************************************************************************************
* @name         :   getGuarantorCovenantType 
* @description  :   This method will set the Covenant Details.
* @return       :   genCovDetList
* @author       :   Nagarjun
* @createddate  :   10/09/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    private List<ECCP_ALB_RequestData.GuarantorCovenantType > getGuarantorCovenantList (list<ECCP_ALB_Int_Data.CovenantType> CovenantsList ){     
        list<ECCP_ALB_RequestData.GuarantorCovenantType > genCovDetList=  new list<ECCP_ALB_RequestData.GuarantorCovenantType >();
        for(ECCP_ALB_Int_Data.CovenantType covDetIns : CovenantsList ){
            ECCP_ALB_RequestData.GuarantorCovenantType covElem = new ECCP_ALB_RequestData.GuarantorCovenantType ();  
            covElem.CovenantId = covDetIns.CovenantId;
            covElem.AddReqAnnualStmtDueWithin = covDetIns.covenanttype == 'Annual' ? covDetIns.AddReqAnnualStmtDueWithin : NULL ;
            covElem.AddReqInterimStmtPrepBy = covDetIns.AddReqInterimStmtPrepBy;
            //If(covDetIns.covenanttype = interim
            covElem.AddReqInterimStmtDueWithin = covDetIns.covenanttype == 'interim' ? covDetIns.AddReqInterimStmtDueWithin : NULL;
            covElem.AddReqTaxReturnPrepBy = covDetIns.covenanttype == 'interim' ? covDetIns.AddReqTaxReturnPrepBy : NULL;
            covElem.AddReqTaxReturnDueWithin = covDetIns.AddReqTaxReturnDueWithin;          
            genCovDetList.add( covElem );
        }
        return genCovDetList; 
    }      
/*******************************************************************************************************************************************
/*******************************************************************************************************************************************
* @name         :   getLandLordDetails
* @description  :   This method will set the LandLord Details.
* @return       :   LandLordDetailsList
* @author       :   Nagarjun
* @createddate  :   10/09/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    private List<ECCP_ALB_RequestData.LandlordsConsentType> getLandLordDetails(list<ECCP_ALB_Int_Data.LandlordsConsentType> LandlordsConsentList ){     
        list<ECCP_ALB_RequestData.LandlordsConsentType> LandlordsDetList =  new list<ECCP_ALB_RequestData.LandlordsConsentType>();
        for(ECCP_ALB_Int_Data.LandlordsConsentType LandlordsConsentIns : LandlordsConsentList){
            ECCP_ALB_RequestData.LandlordsConsentType LandLordElem = new ECCP_ALB_RequestData.LandlordsConsentType();  
            LandLordElem.StreetAddress =   LandlordsConsentIns.StreetAddress;
            LandLordElem.DateofLease =   LandlordsConsentIns.DateofLease;
            LandLordElem.City=   LandlordsConsentIns.City;        
            LandLordElem.State=   LandlordsConsentIns.State;
            LandLordElem.Zipcode=   LandlordsConsentIns.Zipcode;  
            LandlordsDetList.add( LandLordElem);
        }
        return LandlordsDetList; 
    }
/*******************************************************************************************************************************************

/*******************************************************************************************************************************************
* @name         :   collateralGuarantorDataList
* @description  :   This method will set the collateralGuarantor data.
* @return       :   collateralGuarantorDataList
* @author       :   Nagarjun
* @createddate  :   25/05/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    private ECCP_ALB_RequestData.CollateralGrantorInformationType getCollateralGuarantorInformation(ECCP_ALB_Int_Data.CollateralGrantorInformationType collateralGuarDataElemt ){      
            ECCP_ALB_RequestData.CollateralGrantorInformationType collateralGuarantorDataIns = new ECCP_ALB_RequestData.CollateralGrantorInformationType ();
            if(collateralGuarDataElemt != NULL ){                
                collateralGuarantorDataIns.CGOrganizationType  =  collateralGuarDataElemt.CGOrganizationType;
                collateralGuarantorDataIns.CGFirstNameandMiddleNname  =  collateralGuarDataElemt.CGFirstNameandMiddleNname;
                collateralGuarantorDataIns.CGLastname  =  collateralGuarDataElemt.CGLastname;
                collateralGuarantorDataIns.CGBusinessName  =  collateralGuarDataElemt.CGBusinessName;
                collateralGuarantorDataIns.CGTaxIdentificationNumber  =  collateralGuarDataElemt.CGTaxIdentificationNumber;
                collateralGuarantorDataIns.CGFullAddress  =  collateralGuarDataElemt.CGFullAddress;
                collateralGuarantorDataIns.CGCity  =  collateralGuarDataElemt.CGCity;
                collateralGuarantorDataIns.CGCounty  =  collateralGuarDataElemt.CGCounty;
                collateralGuarantorDataIns.CGState  =  collateralGuarDataElemt.CGState;
                collateralGuarantorDataIns.CGPostalCode  =  collateralGuarDataElemt.CGPostalCode;
                collateralGuarantorDataIns.CGPrimaryPhone  =  collateralGuarDataElemt.CGPrimaryPhone;
                collateralGuarantorDataIns.CollateralID =  collateralGuarDataElemt.CollateralID;
                //Release 1.1
                collateralGuarantorDataIns.CollateralKey = collateralGuarDataElemt.CollateralKey;
                collateralGuarantorDataIns.CollateralGrantorKey = collateralGuarDataElemt.CollateralGrantorKey;
                //Release 2.0   
                collateralGuarantorDataIns.GrantorDBA = collateralGuarDataElemt.GrantorDBA ;
            }        
        return collateralGuarantorDataIns;
    }
/*******************************************************************************************************************************************
* @name         :   guarantorDataList 
* @description  :   This method will set the guarantor data.
* @return       :   guarantorDataList 
* @author       :   Nagarjun
* @createddate  :   25/05/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    public void getguarantorData(ECCP_ALB_Int_Data reqObj){
        for( ECCP_ALB_Int_Data.AccountDataClass accountDataElemt : reqObj.loanAccountData){                                    
            if(accountDataElemt.EntityRelationShipType!=NULL){
                string[] EntRel = accountDataElemt.EntityRelationShipType.split('-');
                string EntRelType = EntRel[0];
                string EntRelAuthType = EntRel[1]; 
                if(EntRelType !=NULL){   
                    if(EntRelType == ECCP_IntegrationConstants.Guarantor && EntRelAuthType == ECCP_IntegrationConstants.DirectAccount){ 
                        ECCP_ALB_RequestData.GuarantorDataType GuarantorDataIns = new ECCP_ALB_RequestData.GuarantorDataType();
                        GuarantorDataIns.GuarantorOrganizationType =  accountDataElemt.OrganizationType;                
                        GuarantorDataIns.GuarantorFirstNameandMiddleNname =  accountDataElemt.FirstNameandMiddleNname;
                        GuarantorDataIns.GuarantorLastname =  accountDataElemt.Lastname;
                        GuarantorDataIns.GuarantorBusinessName =  accountDataElemt.BusinessName;
                        GuarantorDataIns.GuarantorTaxIdentificationNumber =  accountDataElemt.TaxIdentificationNumber;
                        GuarantorDataIns.GuarantorAddress =  accountDataElemt.FullBillingAddress;
                        GuarantorDataIns.GuarantorCity =  accountDataElemt.BillingCity;               
                        GuarantorDataIns.GuarantorState =  accountDataElemt.BillingState;
                        GuarantorDataIns.GuarantorPostalCode =  accountDataElemt.BillingPostalCode;                
                        GuarantorDataIns.NonIndividualGuarantorOrganizationType =  accountDataElemt.Title;
                        GuarantorDataIns.GuarantorAuthority =  accountDataElemt.Authority;
                        GuarantorDataIns.GuarantorNAICSCode =  accountDataElemt.NAICSCode;                
                        //GuarantorDataIns.NonProfitCorporation =  accountDataElemt.OrganizationID;                                
                        GuarantorDataIns.NonProfitCorporation =  accountDataElemt.NonProfitCorporation;
                        //release 1.1
                        GuarantorDataIns.GuarantorKey =  accountDataElemt.AccountId; 
                        GuarantorDataIns.GuarantorEntityRoles = accountDataElemt.GuarantorEntityRoles;
                        //Release 2.0   
                        GuarantorDataIns.GuarantorDBA = accountDataElemt.DBA;
                        GuarantorDataIns.GuarantorCustomerNumber = accountDataElemt.CustomerNumber;
                        GuarantorDataIns.GuarSecuredByCollateralYN = accountDataElemt.GuarSecuredByCollateralYN;
                        //GuarantorDataIns.GuarantorCovenant = getGuarantorCovenantList(reqObj.Covenants);
                        GuarantorDataIns.AddReqAnnualStmtsYN = reqObj.AddReqAnnualStmtsYN;
                        GuarantorDataIns.AddReqAnnualStmtPrepBy = reqObj.AddReqAnnualStmtPrepBy;
                        //need Zameer help
                        GuarantorDataIns.AddReqInterimStmtsYN = reqObj.AddReqInterimStmtsYN;
                        GuarantorDataIns.AddReqCopiesOfTaxReturnsYN = reqObj.AddReqCopiesOfTaxReturnsYN;
                        GuarantorDataIns.ObligorNumber = accountDataElemt.CustomerNumber != NULL ? decimal.valueOf(accountDataElemt.CustomerNumber): null;
                        guarantorDataList.add(GuarantorDataIns);     
                                             
                    }
                    
                    if(EntRelType  == ECCP_IntegrationConstants.Borrower && EntRelAuthType == ECCP_IntegrationConstants.DirectAccount){
                        ECCP_ALB_RequestData.BorrowerDataType BorrowerDataIns = new ECCP_ALB_RequestData.BorrowerDataType();
                        BorrowerDataIns.OrganizationType =  accountDataElemt.OrganizationType;
                        BorrowerDataIns.Type_x=  accountDataElemt.Type;
                        BorrowerDataIns.FirstnameMiddlename =  accountDataElemt.FirstNameandMiddleNname;
                        BorrowerDataIns.LastName =  accountDataElemt.Lastname;
                        BorrowerDataIns.BusinessName =  accountDataElemt.BusinessName;
                        BorrowerDataIns.TaxIdentificationNumber =  accountDataElemt.TaxIdentificationNumber;
                        BorrowerDataIns.Address =  accountDataElemt.FullBillingAddress;
                        BorrowerDataIns.City =  accountDataElemt.BillingCity;
                        BorrowerDataIns.County =  accountDataElemt.BillingCounty;
                        BorrowerDataIns.State =  accountDataElemt.BillingState;
                        BorrowerDataIns.postalcode =  accountDataElemt.BillingPostalCode;
                        BorrowerDataIns.primaryPhoneNumber =  accountDataElemt.PrimaryPhoneNumber;                                
                        BorrowerDataIns.NAICSCode =  accountDataElemt.NAICSCode;
                        BorrowerDataIns.IncorporationState =  accountDataElemt.IncorporationState;
                        BorrowerDataIns.OrganizationId =  accountDataElemt.OrganizationID;
                        BorrowerDataIns.CustomerNumber =  accountDataElemt.CustomerNumber;
                        BorrowerDataIns.RiskRating =  accountDataElemt.RiskRating;
                        //release 1.1
                        BorrowerDataIns.BorrowerKey =  accountDataElemt.AccountId; 
                        //Release 2.0                        
                        BorrowerDataIns.BorrowerDBA = accountDataElemt.DBA;
                        BorrowerDataIns.CreditScore = reqObj.CreditScore;                    
                        BorrowerDataList.add(BorrowerDataIns);            
                    }
                }
                if(EntRelAuthType != NULL){
                    if(EntRelAuthType == ECCP_IntegrationConstants.Auth){
                        ECCP_ALB_RequestData.SignerDataType SignerDataIns = new ECCP_ALB_RequestData.SignerDataType();
                        SignerDataIns.OrganizationType =  accountDataElemt.OrganizationType;
                        SignerDataIns.EntityRoles =  accountDataElemt.EntityRoles;
                        SignerDataIns.FirstNameandMiddleNname =  accountDataElemt.FirstNameandMiddleNname;
                        SignerDataIns.Lastname =  accountDataElemt.Lastname;
                        SignerDataIns.BusinessName =  accountDataElemt.BusinessName;
                        SignerDataIns.TaxIdentificationNumber =  accountDataElemt.TaxIdentificationNumber;
                        SignerDataIns.FullBillingAddress =  accountDataElemt.FullBillingAddress;
                        SignerDataIns.BillingCity =  accountDataElemt.BillingCity;
                        SignerDataIns.BillingCounty =  accountDataElemt.BillingCounty;
                        SignerDataIns.BillingState =  accountDataElemt.BillingState;
                        SignerDataIns.BillingPostalCode =  accountDataElemt.BillingPostalCode;
                        SignerDataIns.PrimaryPhoneNumber =  accountDataElemt.PrimaryPhoneNumber;
                        SignerDataIns.Title =  accountDataElemt.Title;           
                        SignerDataIns.Authority =  accountDataElemt.Authority;
                        SignerDataIns.NAICSCode =  accountDataElemt.NAICSCode;
                        SignerDataIns.IncorporationState =  accountDataElemt.IncorporationState;
                        SignerDataIns.OrganizationID =  accountDataElemt.OrganizationID;
                        //release 1.1
                        SignerDataIns.GuarantorKey =  (EntRelType  == ECCP_IntegrationConstants.Guarantor ? accountDataElemt.EntAccId: '');
                        SignerDataIns.BorrowerKey =  (EntRelType  == ECCP_IntegrationConstants.Borrower ? accountDataElemt.EntAccId: '');  
                        SignerDataIns.SignorKey =  accountDataElemt.AccountId;  
                        //Release 2.0
                        SignerDataIns.SignorDBA= accountDataElemt.DBA;
                        SignerDataList.add(SignerDataIns);
                    }
                }
            }              
        }           
    }
/*******************************************************************************************************************************************
* @name         :   collateralDataList 
* @description  :   This method will set the collateral data.
* @return       :   collateralDataList 
* @author       :   Nagarjun
* @createddate  :   25/05/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    private List<ECCP_ALB_RequestData.CollateralDataType> getcollateralData(ECCP_ALB_Int_Data reqObj){

        List<ECCP_ALB_RequestData.CollateralDataType> collateralDataList = new List<ECCP_ALB_RequestData.CollateralDataType>();
        for( ECCP_ALB_Int_Data.collateralDataClass collateralDataElemt : reqObj.loancollateralData){
            ECCP_ALB_RequestData.CollateralDataType collateralDataIns = new ECCP_ALB_RequestData.CollateralDataType();
            collateralDataIns.UCCState  =  collateralDataElemt.UCCState;
            collateralDataIns.GeneralCollateralType  =  collateralDataElemt.GeneralCollateralType;
            collateralDataIns.CollateralSubType  =  collateralDataElemt.CollateralSubType;
            collateralDataIns.AgriculturalUse  =  collateralDataElemt.AgriculturalUse;
            collateralDataIns.CollateralDescription  =  collateralDataElemt.CollateralDescription;
            collateralDataIns.BodyStyle  =  collateralDataElemt.BodyStyle;
            collateralDataIns.CollateralCity  =  collateralDataElemt.CollateralCity;
            collateralDataIns.CollateralCounty  =  collateralDataElemt.CollateralCounty;
            collateralDataIns.CollateralState  =  collateralDataElemt.CollateralState;
            collateralDataIns.Make  =  collateralDataElemt.Make;
            collateralDataIns.Model  =  collateralDataElemt.Model;
            collateralDataIns.PossessoryAccountNumber  =  collateralDataElemt.PossessoryAccountNumber;
            collateralDataIns.PossessoryCurrentBalance  =  collateralDataElemt.PossessoryCurrentBalance;
            collateralDataIns.NumberofUnits  =  collateralDataElemt.NumberofUnits;
            collateralDataIns.CollateralID  =  collateralDataElemt.CollateralID;
            collateralDataIns.StreetAddress  =  collateralDataElemt.StreetAddress;
            collateralDataIns.PropertyTaxID  =  collateralDataElemt.PropertyTaxID;
            collateralDataIns.VehicleIdentificationNumber  =  collateralDataElemt.VehicleIdentificationNumber;
            collateralDataIns.Year  =  collateralDataElemt.Year;
            collateralDataIns.Zipcode  =  collateralDataElemt.Zipcode;
            collateralDataIns.FirstLienIndicator  =  collateralDataElemt.FirstLienIndicator;
            collateralDataIns.FloodSectionDetail  =  collateralDataElemt.FloodSectionDetail;
            collateralDataIns.BookEntry  =  collateralDataElemt.BookEntry;
            collateralDataIns.LifeInsurancePolicyNumber  =  collateralDataElemt.LifeInsurancePolicyNumber;
            collateralDataIns.LifeInsuranceBenefitAmount  =  collateralDataElemt.LifeInsuranceBenefitAmount;
            collateralDataIns.Nameoftheinsured  =  collateralDataElemt.Nameoftheinsured;
            collateralDataIns.LifeInsuranceCompany  =  collateralDataElemt.LifeInsuranceCompany;
            collateralDataIns.LifeInsuranceCompanyStreetAdress  =  collateralDataElemt.LifeInsuranceCompanyStreetAdress;
            collateralDataIns.LifeInsuranceCompanyCity  =  collateralDataElemt.LifeInsuranceCompanyCity;
            collateralDataIns.LifeInsuranceCompanyState  =  collateralDataElemt.LifeInsuranceCompanyState;
            collateralDataIns.LifeInsuranceCompanyZipcode  =  collateralDataElemt.LifeInsuranceCompanyZipcode;
            collateralDataIns.CollateralType  =  collateralDataElemt.CollateralType;
            collateralDataIns.Manufacturer  =  collateralDataElemt.Manufacturer;
            collateralDataIns.FAARegistrationNumber  =  collateralDataElemt.FAARegistrationNumber;
            // Aditya Commented for Release 1.1
           // collateralDataIns.UCCTypes  =  collateralDataElemt.UCCTypes;
            collateralDataIns.VesselName  =  collateralDataElemt.VesselName;
            collateralDataIns.VesselNumber  =  collateralDataElemt.VesselNumber;
            collateralDataIns.StateRegistered  =  collateralDataElemt.StateRegistered;
            ECCP_ALB_RequestData.CollateralGrantorInformationType getCollateralGuarData = getCollateralGuarantorInformation(collateralDataElemt.CollateralGuarantorList);  
            collateralDataIns.CollateralGrantorInformation =   getCollateralGuarData; 
              //Release  1.1
            collateralDataIns.CollateralKey = collateralDataElemt.CollateralKey;      
            //Release 2.0
            collateralDataIns.CollateralInsuranceAmount =  collateralDataElemt.CollateralInsuranceAmount;           
            collateralDataList.add(collateralDataIns);            
        } 
        return collateralDataList ;
    }
/*******************************************************************************************************************************************
* @name         :   disbursementDataList 
* @description  :   This method will set the disbursement data.
* @return       :   disbursementDataList 
* @author       :   Nagarjun
* @createddate  :   25/05/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    private List<ECCP_ALB_RequestData.DisbursementDataType> getdisbursementData(ECCP_ALB_Int_Data reqObj){

        List<ECCP_ALB_RequestData.DisbursementDataType> disbursementDataList = new List<ECCP_ALB_RequestData.DisbursementDataType>();
        ECCP_ALB_RequestData.DisbursementDataType disbursementDataIns = new ECCP_ALB_RequestData.DisbursementDataType();
        disbursementDataIns.DisbursementDescription = reqObj.DisbursementDescription;
        disbursementDataIns.DisbursementPaymentNumber = reqObj.DisbursementPaymentNumber;
        disbursementDataIns.DisbursementAccountType = reqObj.DisbursementAccountType;
        disbursementDataIns.TotalDisbursed = reqObj.TotalDisbursed;
        disbursementDataList .add(disbursementDataIns);        
        return disbursementDataList ;
    }
/*******************************************************************************************************************************************
* @name         :   feeInfoList 
* @description  :   This method will set the fee data.
* @return       :   feeInfoList 
* @author       :   Nagarjun
* @createddate  :   25/05/2016
* @lastmodifiedby:  
//*******************************************************************************************************************************************/ 
    private List<ECCP_ALB_RequestData.FeeInfoType> getfeeInfo(ECCP_ALB_Int_Data reqObj){

        List<ECCP_ALB_RequestData.FeeInfoType> feeInfoList = new List<ECCP_ALB_RequestData.FeeInfoType>();
        for(ECCP_ALB_Int_Data.FeeDataClass feeDataElemt: reqObj.feeData){
            ECCP_ALB_RequestData.FeeInfoType feeInfoIns = new ECCP_ALB_RequestData.FeeInfoType();
            feeInfoIns.Feetype = NULL;
            feeInfoIns.PrepaidFinanceChargeCash = NULL;
            feeInfoIns.PrepaidFinanceChargeDescription = feeDataElemt.PrepaidFinanceChargeDescription;
            feeInfoIns.PrepaidFinanceChargeFinanced = NULL;
            feeInfoList.add(feeInfoIns);
        }
        return feeInfoList ;
    }              

} //end of main class