//This class is created for Preload Exposure POC 
// THis class can be deleted after poc completion


public class Loanclass {

    public String loan{get;set;}
    public String Rm{get;set;}
    public String rmType{get;set;}
    public String prodtype{get;set;}
    public Integer outStandingAmt{get;set;}
    public Integer committedAmt{get;set;}
    public String exposureType{get;set;}
    public List<ChildPreEx> childPreEx{get;set;}

    public class ChildPreEx {
        public String cloan{get;set;}
        public String cRm{get;set;}
        public String crmType{get;set;}
        public String cprodtype{get;set;}
        public Integer coutStandingAmt{get;set;}
        public Integer ccommittedAmt{get;set;}
        public String cexposureType{get;set;}
        public List<GrandChilds> grandChilds{get;set;}
    }
    public class GrandChilds {
        public String gcloan{get;set;}
        public String gcRm{get;set;}
        public String gcrmType{get;set;}
        public String gcprodtype{get;set;}
        public Integer gcoutStandingAmt{get;set;}
        public Integer gccommittedAmt{get;set;}
        public String gcexposureType{get;set;}
    }

    
    public static Loanclass parse(String json) {
        return (Loanclass) System.JSON.deserialize(json, Loanclass.class);
    }
    
    
}