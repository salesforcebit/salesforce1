/*
This trigger is written for KEF KRR Project by TCS Team.
For release 3 Integration requirements.
*/
trigger OriginationDocTrigger on Origination_Document__c (before delete) {
    
    OriginationDocHandler_KEF handler = new OriginationDocHandler_KEF();
    
    if(Trigger.isDelete && Trigger.isBefore){
        handler.onAfterDelete(Trigger.oldMap);
    }    
}