trigger triggerAnalyzedAccount on LLC_BI__Analyzed_Account__c (before update) {
    
    if(trigger.isBefore) {
        if(trigger.isUpdate) {
          AnalyzedAccountTriggerHandler handler = new AnalyzedAccountTriggerHandler();
          handler.handleBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        }
   }

}