//================================================================================
//  KeyBank  
//  Object: SalesGoal
//  Author: Offshore
//  Description : Trigger actions in Sales Goal Object based on events. 
//                (1) Owner field change to user.
//================================================================================
// Created: 06/26/2015      Initial Version
//================================================================================


trigger triggerSalesGoal on Sales_Goal__c (after insert,after update)
{    
    if(Trigger.isInsert && Trigger.isafter)
    {
        SalesGoalHandler SGH = new SalesGoalHandler();
        SGH.ownerchange(Trigger.new);                 
    }
    
    if(Trigger.isUpdate && Trigger.isAfter)
    {
        if(SalesGoalHandler.runOnce())
        {
            SalesGoalHandler SGH = new SalesGoalHandler();
            SGH.ownerchange(Trigger.new);     
        }
                    
    }
    
}