//================================================================================
//  KeyBank  
//  Object: Opportunity
//  Author: offshore
//  Detail: triggerOpportunity 
//  Description: To Update the Actual Fee at the Team Meber level
//               To Insert a Opportunity Team member record based on Opportunity Owner change. 
//               Modified to insert OLI for single product opportunities.
//               Picklist Code update based on the Asset Code, Asset Description and KEF Owning Group 
//================================================================================
//          Date            Purpose
// Changes: 03/12/2015      Initial Version
//================================================================================


trigger triggerOpportunity on Opportunity  (after delete, after insert, after undelete, after update,before insert, before update ) 
{
   
        
        Trigger_Activation_Setting__c check =  Trigger_Activation_Setting__c.getInstance( UserInfo.GetUserID() );
        if( check.Run__c || system.test.isRunningTest())
        {
        if(trigger.isbefore)
        {
            OpportunityTriggerHandler.updatePrivate(trigger.new);
           // To remove the Old vendor details from the Opportunity
            if(trigger.isUpdate){
            
             OpportunityTriggerHandler.updateVendorDetails(trigger.new,trigger.OldMap,trigger.NewMap);
             
            }
        }
    // -------------------------------------------------------------------------------
        // Picklist Code Update
        if(trigger.isbefore  && (trigger.isinsert || trigger.isupdate))
        {       
           list<string> tidList = new list<string>();
           list<string> tidpList = new list<string>();
           list<string> tidcList = new list<string>();
           for(opportunity rec: [Select id,name,Asset_Description__c,Asset_Code__c,KEF_Owning_Group__c,recordtypeId,recordtype.name from opportunity where id in :trigger.new])
            {                                                
                if(rec.recordtype.name =='KEF Opportunity'){
                tidList.add(rec.Asset_Description__c);
                system.debug('**'+rec.Asset_Description__c+'**');
                tidpList.add(rec.Asset_Code__c);
                tidcList.add(rec.KEF_Owning_Group__c);
                }
                
             }
            system.debug(tidList+'..'+tidpList+'***');
            if(tidList.size()>0 ||tidpList.size()>0||tidcList.size()>0)
            
            PicklistUpdate.oppupdate(trigger.new,tidList,tidpList,tidcList);
            
            
        }
    // ------------------------------------------------------------------------------------    
        //CR-1244
      /* if(trigger.isbefore && trigger.isUpdate){
        OpportunityTriggerHandler.updateAutoDebit(trigger.new); 
    }*/             
        if (trigger.isInsert && trigger.isAfter)
        {
            
            OpportunityTeamMemberUpdate OTMinsert = new OpportunityTeamMemberUpdate();
            OTMinsert.InsertOpportunityeammember(trigger.new);
            List<String> productCode = new List<String>();
           
            List<Id> oppList= new list<id>();
            List<ID> oppid = new List<ID>();
            List<id> countId = new List<id>();
            for(Opportunity opp : trigger.new){
                oppList.add(opp.id);            
                productCode.add('%'+opp.Product_Category__c+'>'+opp.Product__c);
                system.debug('*** '+productCode);
            } 
             
             OpportunityTriggerHandler.insertOppLineItem(oppList,productCode);
            
             OpportunityTriggerHandler.addPrimarySyndicator(trigger.new);
             OpportunityTriggerHandler.updatevendor(trigger.new);
             


            for(Opportunity o : trigger.new){
               if(o.Lead_Opportunity_Record_Type__c!= null ){      
                  oppid.add(o.id);
               }
            }
            
            if(oppid.size()>0){
               OpportunityTriggerHandler.updateOppty(oppid);     
            }             
            //updated as part of September 2016 release for CR 1499
            for(Opportunity opp :[Select id,parent_opportunity_id__c,recordtype.name from opportunity where id in :trigger.new]){
                if((opp.recordtype.name=='KEF Opportunity' || opp.recordtype.name=='KEF Oppty - Submitted')&&opp.parent_opportunity_id__c!=null)
                countId.add(opp.parent_opportunity_id__c);
            }
            if(countId.size()>0)
            OpportunityTriggerHandler.countChildOpty(countId);  
        }
    // ----------------------------------------------------------------------------------------------    
        
        if(Trigger.isUpdate && Trigger.isAfter)
        {
            List<Opportunity> ClosedOpp = new List<Opportunity>();
            List<Id> countOpty = new List<Id>();
            List<Id>OldParentList = new List<id>();
            //assestsuppliertask.ASupdate(trigger.new);
            //Opportunity Team Member insert
            
            OpportunityTeamMemberUpdate OTMupdate = new OpportunityTeamMemberUpdate();
            OTMupdate.UpdateOpportunityteammember(trigger.new,trigger.oldmap);
            OpportunityTriggerHandler.addPrimarySyndicator(trigger.new);
            //Added as part of March release to populate junction object when opty is closed.
                if(OpportunityTriggerHandler.runOnce()){  
                  for(Opportunity opp: trigger.new){
                      if(opp.StageName!=Trigger.OldMap.get(opp.Id).StageName && opp.StageName=='Closed'){
                          ClosedOpp.add(opp);
                      }
                  }
                  if(ClosedOpp.size()>0){
                  OpportunityTriggerHandler.insertProgramGoalJunction(closedOpp);
                  OpportunityTriggerHandler.insertProgramBDJunction(closedOpp);
                  OpportunityTriggerHandler.opportunitysplitquery(trigger.new); 
                  OpportunityTriggerHandler.opportunitysegmentgoals(trigger.new);
                  
                  }
                  OpportunityTriggerHandler.updatevendor(trigger.new);
                }
                 //Added as part of CR 1499 to check if the opty is having child records
              for(Opportunity opty:[Select id,parent_opportunity_id__c,recordtype.name from opportunity where id in :trigger.new]){
                  if((opty.recordtype.name=='KEF Opportunity' || opty.recordtype.name=='KEF Oppty - Submitted') && opty.Parent_opportunity_id__c!= null && (opty.parent_opportunity_id__c!= trigger.oldMap.get(opty.id).parent_opportunity_id__c))
                  countOpty.add(opty.parent_opportunity_id__c);
                  OldParentList.add(trigger.oldMap.get(opty.id).parent_opportunity_id__c);
              }
              if(countOpty.size()>0)
              OpportunityTriggerHandler.countChildOptyUpdate(countOpty,OldParentList);
    // ----------------------------------------------------------------------------------------------         
            //Actual Fee Update
            list<id> tidList = new list<id>();
            List<Id>oppupdateList = new List<Id>();
            List<Id> updateFee = new List<Id>();
            List<Id> removeProducts = new List<Id>();
            system.debug(trigger.new);
            List<Opportunity> opptyList = new List<Opportunity>();
            List<OpportunityTeamMember> opptyTeamMemList = new List<OpportunityTeamMember>();
            List<String> prodCode = new List<String>();
            
            for(Opportunity otm :[Select id,name,Actual_Syndication_Non_Amortizing_Fee__c,Estimated_Fee__c,Vendor_Agreement__c,Estimated_Revenue__c,Revenue__c,recordtype.name,Product_category__c,Product__c, Opportunity.RecordTypeId, Actual_Fee_Amount__c, Actual_Fee_Rollup__c,Investor_Type_Product__c,Product_LSAM__c from Opportunity where id in: trigger.new])
            
            {
                system.debug(otm);
                tidList.add(otm.id);   
                opptyList.add(otm);         
                prodCode.add('%'+otm.Product_Category__c+'>'+otm.Product__c);
                
                 if(((otm.recordtype.name=='Invst Bnkg & Market' ||otm.recordtype.name=='REC-On Balance Sheet' )&&(otm.Product_category__c != Trigger.oldMap.get(otm.Id).Product_Category__c||otm.Product__c != Trigger.oldMap.get(otm.Id).Product__c))
                  || (otm.recordtype.name=='Mortgage Banking' && otm.Investor_type_Product__c != Trigger.oldMap.get(otm.Id).Investor_type_Product__c) 
                  || (otm.recordtype.name=='LSAM' && otm.Product_LSAM__c != Trigger.oldMap.get(otm.Id).Product_LSAM__c))
                {
                    oppupdateList.add(otm.id);
                }
                
                if(otm.Estimated_Fee__c!=Trigger.OldMap.get(otm.Id).Estimated_Fee__c||otm.Estimated_Revenue__c!=Trigger.oldMap.get(otm.Id).Estimated_Revenue__c||otm.Revenue__c!=Trigger.oldMap.get(otm.Id).Revenue__c)
                updateFee.add(otm.Id); 
                 
                //to remove the products from Opportunity if Vendor agreement is changed
                if(otm.Vendor_Agreement__c!=Trigger.oldMap.get(otm.Id).Vendor_Agreement__c)
                {
                    removeProducts.add(otm.Id); 
                    system.debug(otm.Vendor_Agreement__c+'***'+Trigger.oldMap.get(otm.Id).Vendor_Agreement__c+'***'+removeProducts); 
                }          
            } 
            
            if(updateFee.size()>0)
            {       
                OpportunityTriggerHandler.updateSalesPrice(updateFee);
            }
            
            if(removeProducts.size()>0)
            {
                OpportunityTriggerHandler.removeProduct(trigger.new,removeProducts);
            }
            
           // OpportunityTriggerHandler.AFupdate(tidList); 
            OpportunityTriggerHandler.AFupdate(opptyList);
            OpportunityTriggerHandler.bUpdate=true;
            if(oppupdateList.size()>0){
            OpportunityTriggerHandler.insertOppLineItem(oppupdateList,prodCode);
            }
            OpportunityTriggerHandler.bUpdate=false;
            
            if(OpportunityTriggerHandler.runOnce())
            {  
               OpportunityTriggerHandler.updatevendor(trigger.new);
               //OpportunityTriggerHandler.insertOppLineItem(oppupdateList,prodCode);
            }
           
        }
            
      }   
   
}