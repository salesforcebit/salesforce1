trigger FundingGridTrigger on Funding_Grid__c (before insert, before update, after insert, after update) 
{
    FundingGridHandler_KEF handler = new FundingGridHandler_KEF();
    
    if(Trigger.isInsert)
    {
	    if(Trigger.isBefore)
	    {
	    	handler.OnBeforeInsert(Trigger.New, Trigger.newMap);
	    }
	    else if(Trigger.isAfter)
	    {
	        handler.OnAfterInsert(Trigger.New, Trigger.newMap);
	    }
    }
    else if(Trigger.isUpdate)
    { 
    	if(Trigger.isBefore)
	    {
	    	handler.OnBeforeUpdate(Trigger.Old, Trigger.New, Trigger.NewMap);
	    }
	    else if(Trigger.isAfter)
	    {
			handler.OnAfterUpdate(Trigger.Old, Trigger.New, Trigger.NewMap);
	    }
    }
}