trigger triggerProgramforBD on Program_for_BD__c (after insert,after update) {

//This is used to share the record for the user.

if(trigger.isinsert ){
shareHandler.shareProgramForBD(trigger.new);
}
else if(trigger.isUpdate){
    List<Program_for_BD__c > updateBD = new List<Program_for_BD__c >();
    for(Program_for_BD__c pBD: trigger.new){
       if(trigger.oldMap.get(pBD.id).User_Name__c!=pBD.User_name__c){
       updateBD.add(pBD);
       }
    }
    if(updateBD.size()>0)
    shareHandler.shareProgramForBD(updateBD);
   }
}