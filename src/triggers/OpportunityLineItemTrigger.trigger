trigger OpportunityLineItemTrigger on OpportunityLineItem (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if(trigger.isBefore) {
        if(trigger.isDelete) {
            OpportunityLineItemTriggerHandler.handleAfterDelete(trigger.oldMap);
        }
    }
}