trigger triggerScenarioProduct on Scenario_Product__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

   if(trigger.isBefore) {
     	if (trigger.isInsert)
     	{
            ScenarioProductTriggerHandler handler = new ScenarioProductTriggerHandler();
            handler.handleBeforeInsert(Trigger.New, Trigger.newMap);
     	}
    	else if(trigger.isUpdate) {
    	   ScenarioProductTriggerHandler handler = new ScenarioProductTriggerHandler();
     	   handler.handleBeforeUpdate(Trigger.New, Trigger.newMap);
    	}
    }

}