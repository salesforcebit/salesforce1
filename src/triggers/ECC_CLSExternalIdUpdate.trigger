/*******************************************************************************************************************************************
* @name         :   ECC_CLSExternalIdUpdate
* @description  :   This class will be responsible to to nullify the CLS External ID on the Entity Involvement during Renewals/Modification.
* @author       :   Lakshmi Myneni
* @createddate  :   10/October/2016
*******************************************************************************************************************************************/
trigger ECC_CLSExternalIdUpdate on LLC_BI__Legal_Entities__c (before insert) {
    for(LLC_BI__Legal_Entities__c  entity:Trigger.New){
     System.debug('Entity List Entry @@@1'+Trigger.new);
        if(entity.ECC_Loan_Stage__c =='Qualification / Proposal' && entity.CLS_External_ID__c !=''){
        System.debug('Entity @@@'+entity);
           entity.CLS_External_ID__c='';  
        }  
       
    }
}