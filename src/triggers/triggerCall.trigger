//================================================================================
//  KeyBank  
//  Object: triggerCall
//  Author: Barney Young Jr.
//  Detail: Call Trigger
//  Description : To populate the Associated Account field based on the Related to field
//================================================================================
//          Date            Purpose
// Changes: 03/12/2015      Initial Version
//================================================================================


trigger triggerCall on Call__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update)
{
    
    Trigger_Activation_Setting__c check = Trigger_Activation_Setting__c.getInstance(userinfo.getUserId());
    
    if(check.Run__c ||system.test.isRunningTest())
    {   
   
    if(Trigger.isInsert && Trigger.isAfter)
    {    
        CallTriggerHandler handler = new CallTriggerHandler(Trigger.isExecuting, Trigger.size);
        handler.OnAfterInsert(Trigger.new);
        list<call__c> listtoUpdate = [Select  AccountId__r.ID, AccountId__r.name,Record_Type_Name__c,ContactId__r.Accountid,LeadId__r.Associated_Account__c,OpportunityId__r.AccountID,LeadId__r.IsConverted from call__c where ID IN :trigger.newMap.keySet()];        
        CallTriggerHandler.OnAfterInsertAsync(Trigger.newMap.keySet());
        update_Associated_Account_Handler update_Acchandler = new update_Associated_Account_Handler(Trigger.isExecuting, Trigger.size);
        update_Acchandler.onAfterInsert(Trigger.New,listtoUpdate);
    }
    
    if(trigger.isupdate && trigger.isAfter)
    {
         
        if(update_Associated_Account_Handler.runOnce())
        {        
            list<call__c> listtoUpdatecall = [Select  AccountId__r.ID, AccountId__r.name,Record_Type_Name__c,ContactId__r.Accountid,LeadId__r.Associated_Account__c,OpportunityId__r.AccountID,LeadId__r.IsConverted from call__c where ID IN :trigger.newMap.keySet()];  
            update_Associated_Account_Handler update_Acchandler = new update_Associated_Account_Handler(Trigger.isExecuting, Trigger.size);
            update_Acchandler.onAfterUpdate(Trigger.New,listtoUpdatecall );         
            update_Associated_Account_Handler.prmupdate(Trigger.newMap,trigger.new);
                        
        }
        
       
                    
    }
    
     if(trigger.isdelete && trigger.isbefore)
    {
            CallTriggerHandler.updateCallCount(Trigger.old);
    }
    
    
      
    if(trigger.isupdate && trigger.isafter)
    {        
           Set<ID> oldset = new Set<ID>();
           Set<ID> newset = new Set<ID>();
           for(Call__c c1: Trigger.old)
           {
               oldset.add(c1.Associated_Account__c);
           }
           system.debug('oldset'+oldset);
           for(Call__c c2 : Trigger.new)
           {
               newset.add(c2.Associated_Account__c);
           }
           system.debug('newset'+newset);
           CallTriggerHandler.updateCallCountOnUpdate(oldset, newset);
    } 
    
    } 
        
}