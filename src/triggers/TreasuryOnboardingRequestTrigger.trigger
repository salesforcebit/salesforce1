//To Create an opportunity team member when a resource is assigned to a payments analyst request

trigger TreasuryOnboardingRequestTrigger on TSO_Treasury_Onboarding_Request__c (after update) {
    
    
    Map<String, TSO_Treasury_Onboarding_Request__c> trreqByOptm = new Map<String, TSO_Treasury_Onboarding_Request__c>();
    
    for(TSO_Treasury_Onboarding_Request__c t : Trigger.New){
    
    TSO_Treasury_Onboarding_Request__c oldt = Trigger.oldMap.get(t.Id);
    
         if( (t.Assigned_Resource__c != oldt.Assigned_Resource__c) &&
         (t.Opportunity__c!=null) && (t.Assigned_Resource__c!=null) ) {
        trreqByOptm.put(t.Opportunity__r.name , t);
        }
    }
        
    
    system.debug('trreqByOptm: '+trreqByOptm);
    
    List<OpportunityTeamMember> optm= 
    [select Id, OpportunityAccessLevel, TeamMemberRole, Opportunity.Opportunity_Number__c, Opportunity.Name, User.username
     from OpportunityTeamMember where Opportunity.Name IN : Trigger.newMap.keySet()];
    
    system.debug('optm: '+optm);
   
    for(OpportunityTeamMember ot : optm){
        
        ot.User.username=trreqByOptm.get(ot.Opportunity.Name).Assigned_Resource__c;
        ot.Opportunity.Name=trreqByOptm.get(ot.Opportunity.Name).Opportunity__r.name;
        ot.OpportunityAccessLevel='Read/Write';
        ot.TeamMemberRole='Analyst';
        }
       
    update optm;
}