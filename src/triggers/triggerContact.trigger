/*
This trigger is written for KEF KRR Project by TCS Team.
For release 3 Integration requirements.
*/

trigger triggerContact on Contact (after insert, after update) {

    ContactHandler_KEF handler = new ContactHandler_KEF();

    if(Trigger.isInsert && Trigger.isAfter){
        handler.onAfterInsert(Trigger.newMap);
    }

    if(Trigger.isUpdate && Trigger.isAfter){
        handler.onAfterUpdate(Trigger.newMap);
        handler.afterUpdate(Trigger.oldMap, Trigger.newMap);
    }
}