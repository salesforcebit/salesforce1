//================================================================================
//  KeyBank  
//  Object: triggerAccount
//  Author: Barney Young Jr.
//  Detail: Account Trigger 
//  Description : To add Account Owner in Account Team. 
//                SIC & Naics Description auto populated based on Code.
//                Capture Account records deleted during Account Merge.

//================================================================================
//          Date            Purpose
// Changes: 03/12/2015      Initial Version
//================================================================================


trigger triggerAccount on Account (after delete,after insert, after update, before delete, before insert, before update) 
{    
    
    AccountTriggerHandler handler = new AccountTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    /*Added on 6 July 2015 - This code is related to KEF functionality */
    AccountHandler_KEF handler_KEF = new AccountHandler_KEF(Trigger.isExecuting, Trigger.size);
    /*Added on 6 July 2015 - This code is related to KEF functionality  - END*/
    
    Trigger_Activation_Setting__c check =  Trigger_Activation_Setting__c.getInstance( UserInfo.GetUserID() );
    if( check.Run__c ||system.test.isRunningTest())
    {
    if(Trigger.isDelete && Trigger.isAfter)
    {
        handler.OnAfterDelete(Trigger.old);
    }
    
    if (trigger.isInsert && trigger.isAfter)
    {
        AccountTeamMemberUpdate ATMinsert = new AccountTeamMemberUpdate();
        ATMinsert.Insertaccounteammember(trigger.new);
    }
    
    if (trigger.isUpdate && trigger.isAfter)
    {
        AccountTeamMemberUpdate ATMupdate = new AccountTeamMemberUpdate();
        ATMUpdate.Updateaccountteammember(trigger.new,trigger.oldmap);
    }
    
    if(trigger.isbefore &&( trigger.isinsert || trigger.isupdate))
    {
       list<string> tidList = new list<string>();
       list<string> tidpList = new list<string>();
       for(Account rec: trigger.new)
        {
            tidList.add(rec.Sic);
            tidpList.add(rec.NaicsCode); 
         }
        SIC_NAICS_DescriptionUpdate.accupdate(trigger.new,tidList,tidpList);
    } 

    /*KEF Block Starts*/
    //added this statement for KRR Project
    if(Trigger.isInsert && Trigger.isBefore)
    {
        handler_KEF.OnBeforeInsert(Trigger.new);
    }
    else if (trigger.isInsert && Trigger.isAfter)
    {
        handler_KEF.OnAfterInsert(trigger.new, trigger.newMap);
    }
    else if(Trigger.isUpdate && Trigger.isBefore)
    {
        handler_KEF.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    }
    else if(Trigger.isUpdate && Trigger.isAfter)
    {
        handler_KEF.OnAfterUpdate(trigger.old, trigger.new, trigger.newMap);
    }
    /*start - changing the event type to After Delete from before delete for Defect 332 and 335 - 28 jan 2016*/
    else if(Trigger.isDelete && Trigger.isAfter)
    {
        handler_KEF.OnAfterDelete(Trigger.old, Trigger.oldMap);
    }   
    /*end - changing the event type to After Delete from before delete for Defect 332 and 335 - 28 jan 2016*/    
    /*KEF Block Ends*/
    if(trigger.isdelete && trigger.isafter)
    {
        handler.updateAccountonMerge(Trigger.Old);
    }
    
    }
    
}