//================================================================================
//  KeyBank  
//  Object: OpportunityLineItem 
//  Author: offshore
//  Detail: OpportunityProductTrigger 
//  Description: Picklist Code update based on the Payment Code and Payment Frequency
//                
//================================================================================
//          Date            Purpose
// Changes: 03/12/2015      Initial Version
//================================================================================

trigger OpportunityProductTrigger on OpportunityLineItem (before delete, after delete,after insert, after undelete, after update,before insert, before Update) 
{
  
    
    Trigger_Activation_Setting__c check =  Trigger_Activation_Setting__c.getInstance( UserInfo.GetUserID() );
    if( check.Run__c ||system.test.isRunningTest())
    {
    if(Trigger.isdelete && Trigger.isbefore)
    {
         OpportunityProductTriggerHandler.DeleteOLI(Trigger.old);               
    }
    
    // Picklist Code update based on Payment Type and Payment Frequency
    if(trigger.isbefore  && (trigger.isinsert || trigger.isupdate))
    {       
       list<string> tidList = new list<string>();
       list<string> tidpList = new list<string>();

       for(OpportunityLineItem rec: trigger.new)
        {
            tidList.add(rec.Payment_Type__c);
            tidpList.add(rec.Payment_Frequency__c);
         }
        system.debug(tidList+'..'+tidpList);
        PicklistUpdate.OLUpdate(trigger.new,tidList,tidpList);
        
    }
    
    }
  
}