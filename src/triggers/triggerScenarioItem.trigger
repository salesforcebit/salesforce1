trigger triggerScenarioItem on LLC_BI__Scenario_Item__c ( after insert, after update, before insert, before update) {
    ScenarioItemTriggerHandler handler = new ScenarioItemTriggerHandler();

    if (trigger.isAfter) {
        if (trigger.isInsert) {
            ScenarioItemTriggerHandler.updateScenItemsWExceptionPricing(trigger.new, null);
        } 
        else if (trigger.isUpdate) {
            handler.handleAfterUpdate(Trigger.oldMap, Trigger.newMap);
            ScenarioItemTriggerHandler.updateScenItemsWExceptionPricing(trigger.new, trigger.oldMap);
        }
    } else {
        if (trigger.isInsert) {
            ScenarioItemTriggerHandler.handleBeforeInsert(trigger.new);
        } else if (trigger.isUpdate) {
            ScenarioItemTriggerHandler.handleBeforeUpdate(trigger.newMap, trigger.oldMap);            
        }
    }
}