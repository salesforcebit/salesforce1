/*******************************************************************************************************************************************
* @name         :   ECC_CLSExternalIdLRTUpdate
* @description  :   This class will be responsible to to nullify the CLS External ID on the Entity Involvement during Renewals/Modification.
* @author       :   Lakshmi Myneni
* @createddate  :   10/October/2016
*******************************************************************************************************************************************/
trigger ECC_CLSExternalIdLRTUpdate on ECC_Loan_Rate_Term__c (before insert) {
   for(ECC_Loan_Rate_Term__c lrt: Trigger.new){
              if(lrt.ECC_Loan_Stage__c =='Qualification / Proposal' && lrt.ECC_External_CLS_Processing_ID__c !=''){
                     lrt.ECC_External_CLS_Processing_ID__c='';      
                }     }      
}