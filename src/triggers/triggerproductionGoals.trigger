trigger triggerproductionGoals on Production_Goals__c (after insert,after update) {
if(trigger.isinsert ){
shareHandler.shareProductionGoals(trigger.new);
}
else if(trigger.isUpdate){
    List<Production_Goals__c > updateGoals = new List<Production_Goals__c >();
    for(Production_Goals__c prod: trigger.new){
       if(trigger.oldMap.get(prod.id).UserName__c!=prod.Username__c){
       updateGoals.add(prod);
       }
    }
    if(updateGoals.size()>0)
    shareHandler.shareProductionGoals(updateGoals);
   }
}