trigger TreasuryServiceTrigger on LLC_BI__Treasury_Service__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if([SELECT Name FROM Profile WHERE Id =: Userinfo.getProfileId() LIMIT 1].Name == 'Integration/Data Migration') return;
    
    if(trigger.isBefore) {
        if(trigger.isInsert) {
            TreasuryServiceTriggerHandler handler = new TreasuryServiceTriggerHandler(trigger.new);
            handler.handleBeforeInsert();
        }
        if(trigger.isUpdate) {
            TreasuryServiceTriggerHandler handler = new TreasuryServiceTriggerHandler(trigger.oldMap, trigger.newMap);
            handler.handleBeforeUpdate();
        }
    }
    else if(trigger.isAfter) {
        if(trigger.isInsert) {
            TreasuryServiceTriggerHandler handler = new TreasuryServiceTriggerHandler(null, trigger.newMap);
            handler.handleAfterInsert();
        }
        if(trigger.isUpdate) {
            TreasuryServiceTriggerHandler handler = new TreasuryServiceTriggerHandler(trigger.oldMap, trigger.newMap);
            handler.handleAfterUpdate();
        }
    }
}