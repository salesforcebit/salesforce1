/*******************************************************************************************************************************************
* @name         :   ECCP_MCACalloutTrigger 
* @description  :   This trigger will call method to call service for MCA check.
* @author       :   Naga
* @createddate  :   12/April/2016
*******************************************************************************************************************************************/
trigger ECCP_MCACalloutTrigger on LLC_BI__Loan__c (after update) {
    List<String> loanId = new List<String>();
    List<String> loanIdLpl = new List<String>();
    boolean isRequiredFieldMissing = false;
    boolean isMcaCheckInitiation = false;
    boolean isLplInitiation = false; 
    

    if(ECCP_MCATriggerHandler.triggerFire){ 
        
      
        for(LLC_BI__Loan__c loan : Trigger.New){
            if(trigger.oldMap.get(loan.Id).ECCP_MCA_Check__c != trigger.newMap.get(loan.Id).ECCP_MCA_Check__c && trigger.newMap.get(loan.Id).ECCP_MCA_Check__c == true){
                loanId.add(loan.Id);
                isMcaCheckInitiation = true;
               
            }
        }
    }
    if(ECCP_LPL_FutureHandler.triggerFire){
    
        for(LLC_BI__Loan__c loan : Trigger.New){
            if(trigger.oldMap.get(loan.Id).ECC_IntegrateToLPL__c != trigger.newMap.get(loan.Id).ECC_IntegrateToLPL__c && trigger.newMap.get(loan.Id).ECC_IntegrateToLPL__c == true){
                loanIdLpl.add(loan.Id);
                isLplInitiation = true;
                system.debug('loanIdLpl' +loanIdLpl);
            }            
        } 
        if(isLplInitiation){ 
            if(loanIdLpl!=null && loanIdLpl.size()>0){
                ECCP_LPL_FutureHandler.preventTriggerRecurson();
                ECCP_LPL_FutureHandler.LPLRequestQueryHandler(loanIdLpl);
            } 
        } 
    }       
    if(isMcaCheckInitiation && ECCP_MCATriggerHandler.triggerFire){ 
         system.debug('---in trigger fire---');              
         List<LLC_BI__Legal_Entities__c> entData = [select id,LLC_BI__Loan__r.LLC_BI__Product_Package__r.Bank_Division__c,LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c,
                                 LLC_BI__Loan__r.ECCP_Application_Number__c, LLC_BI__Account__r.recordType.Name
                                 From LLC_BI__Legal_Entities__c
                                 where LLC_BI__Loan__c In : loanId]; 
    
        for( LLC_BI__Legal_Entities__c ent: entData){
            string currentSSN = ent.LLC_BI__Account__r.LLC_BI__Tax_Identification_Number__c;
            //if((ent.LLC_BI__Loan__r.ECCP_Application_Number__c == NULL || ent.LLC_BI__Loan__r.ECCP_Application_Number__c == '') ||
            if((ent.LLC_BI__Loan__r.LLC_BI__Product_Package__r.Bank_Division__c == NULL || ent.LLC_BI__Loan__r.LLC_BI__Product_Package__r.Bank_Division__c == '' )||
            (currentSSN  == NULL ||currentSSN  == '')){
                isRequiredFieldMissing = true;                                  
            }
        }  
        
           
       if(!isRequiredFieldMissing ){
        
            system.debug('----123ECCP_MCATriggerHandler.triggerFire----'+ECCP_MCATriggerHandler.triggerFire);
            if(loanId !=null && loanId.size()>0){
                system.debug('---just before fire---');
                ECCP_MCATriggerHandler.preventTriggerRecurson();
                ECCP_MCATriggerHandler.serviceCallOut(loanId);
                isMcaCheckInitiation  = false;
            }
        }
        else{
            trigger.newMap.get(loanId[0]).addError('MCA Call can not be completed Without SSN/LineofBusiness--');
        } 
    }                                               
    
}