trigger TSOSetupTaskTrigger on TSO_Setup_Task__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    if([SELECT Name FROM Profile WHERE Id =: Userinfo.getProfileId() LIMIT 1].Name == 'Integration/Data Migration') return;

    if(trigger.isAfter) {
        if(trigger.isUpdate) {
            TSOSetupTaskTriggerHandler handler = new TSOSetupTaskTriggerHandler(trigger.oldMap, trigger.newMap);
            handler.handleAfterUpdate();
         
        }
        else if(trigger.isInsert){
            TSOSetupTaskTriggerHandler.updateProductPackage(trigger.new, null);
            TSOSetupTaskTriggerHandler handler = new TSOSetupTaskTriggerHandler(null, trigger.newMap);
            handler.handleAfterInsert();
        }
        else if(trigger.isDelete) {
            TSOSetupTaskTriggerHandler handler = new TSOSetupTaskTriggerHandler(trigger.oldMap, null);
            handler.handleAfterDelete();
        }
    } else {
        if(trigger.isInsert){
            
        } else if(Trigger.isUpdate){
            TSOSetupTaskTriggerHandler.updateProductPackage(trigger.new, trigger.oldMap);
        }
    }
}