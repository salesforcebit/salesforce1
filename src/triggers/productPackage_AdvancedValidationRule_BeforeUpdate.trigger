trigger productPackage_AdvancedValidationRule_BeforeUpdate on LLC_BI__Product_Package__c (before update) {
    
    //Private Variables
    Private static final String PARENTOBJECT = 'LLC_BI__Product_Package__c';
    private static final String BLANK = '';
    
    //Public Variables
	String validationError = BLANK;
	SObject newData = Trigger.newMap.get(Trigger.new[0].Id);
    List <Advanced_Validation_Rule__c> validationRules = [SELECT
                                                            Id,
                                                            Field_Set__c,
                                                            Filter_Criteria__c,
                                                            Object_To_Validate__c,
                                                            Parent_Object__c,
                                                            Rule__c,
                                                            Evaluation_Type__c,
                                                            Junction_Object__c,
                                                          	Exclude_From_Integration__c
                                                          FROM
                                                          	Advanced_Validation_Rule__c
                                                          WHERE
                                                          	Parent_Object__c = :PARENTOBJECT
                                                          AND
                                                          	isActive__c = True];

    if(validationRules.size() > 0){
        for (Advanced_Validation_Rule__c rule : validationRules) {
            if(!(new AdvancedValidationTriggerHandler().integrationCheck(rule.Exclude_From_Integration__c))) {
                validationError += new AdvancedValidationTriggerHandler().validateRecords(Trigger.new[0].Id, newData, rule);
            }
        }
    }

    if (validationError != BLANK) {
        Trigger.new[0].addError(validationError, FALSE);
    }
}