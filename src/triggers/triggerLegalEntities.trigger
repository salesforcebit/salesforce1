trigger triggerLegalEntities on LLC_BI__Legal_Entities__c (after insert, after update) {

	if(trigger.isAfter) {
        if (trigger.isInsert) {
        	LegalEntitiesTriggerHandler.handleAfterInsert(Trigger.newMap);
        } else if (trigger.isUpdate) {
        	LegalEntitiesTriggerHandler.handleAfterUpdate(Trigger.oldMap, Trigger.newMap);
        }
    }
}