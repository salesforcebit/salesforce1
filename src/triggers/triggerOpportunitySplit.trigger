trigger triggerOpportunitySplit on OpportunitySplit(before insert, after insert,after update, before update,before delete, after Delete )
{
    User usr = [select Id,Profile.Name from User where Id = :userInfo.getUserId()];
    
    if(usr.Profile.Name != 'Integration/Data Migration')
    { 
        if(Trigger.isupdate && Trigger.isafter )
        {
            /*if(OpportunitySplitTriggerHandler.runOnce())  
            {*/
                for(opportunitysplit osp : trigger.new)
                {
                    Integer i= [Select count() from OpportunitySplit where OpportunityId =: Trigger.oldMap.get(osp.Id).OpportunityId and primary__c=true and Opportunity_Record_Type__c = true];
                    
                    if(i<1 && osp.Opportunity_Record_Type__c == true)
                    {
                        osp.adderror(' Primary Flag should be TRUE for atleast one user');
                    }
                    
                    if(osp.Primary__c != Trigger.OldMap.get(osp.Id).Primary__c)
                    {
                        OpportunitySplitTriggerHandler.OSupdate(trigger.new);
                    }   
                }
           // }   
        }
    }
    
    /* if(Trigger.isDelete && Trigger.IsAfter)
    {
        OpportunitySplitTriggerHandler.OSupdate1(trigger.new);
    } */ 
          
}