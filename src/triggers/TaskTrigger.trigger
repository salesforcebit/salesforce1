trigger TaskTrigger on Task (after insert){
  if(Trigger.isAfter){
    if(Trigger.isInsert){
      Set<Id> taskIds = new Set<Id>();
      List<Task> taskList = new List<Task>();
      for(Task taskTemp : Trigger.new){
        System.debug('1');
        System.debug(taskTemp);
        if(taskTemp.Maintenance_Task__c == true){
          System.debug(taskTemp);
          taskList.add(taskTemp);
          taskIds.add(taskTemp.whatid);
        }
      }
      List<LLC_BI__Treasury_Service__c> tsList = [SELECT Id, LLC_BI__Stage__c, Automated_Task__c, Type__c FROM LLC_BI__Treasury_Service__c WHERE Id IN: taskIds];
      System.debug(tsList);
      Map<Id, LLC_BI__Treasury_Service__c> taskTS = new Map<Id, LLC_BI__Treasury_Service__c>();
      for(Task taskTemp : taskList){
        for(LLC_BI__Treasury_Service__c tsTemp : tsList){
          System.debug('2');
          taskTS.put(taskTemp.Id, tsTemp);
        }
      }
      for(Task taskTemp : taskList){
        System.debug('3');
        if(taskTS.get(taskTemp.Id).LLC_BI__Stage__c != 'Order Entry')  
        	taskTS.get(taskTemp.Id).LLC_BI__Stage__c = 'Fulfillment';
        taskTS.get(taskTemp.Id).Type__c = 'Revise - Add/Change/Delete';
        taskTS.get(taskTemp.Id).Automated_Task__c = true;
      }
      if(taskTS.size() != 0){
        System.debug(taskTS);
        update taskTS.values();
      }
      for(Task taskTemp : taskList){
        taskTS.get(taskTemp.Id).Automated_Task__c = false;
      }
      if(taskTS.size() != 0){
        System.debug(taskTS);
        update taskTS.values();
      }
    }
  }
}