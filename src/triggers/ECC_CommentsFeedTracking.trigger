/*******************************************************************************************************************************************
* @name         :   ECC_CommentsFeedTracking
* @description  :   This trigger will create the feed item for tracking.
* @author       :   Lakshmi Myneni
* @createddate  :   19/August/2016
*******************************************************************************************************************************************/

trigger ECC_CommentsFeedTracking on LLC_BI__Product_Package__c (before update) {
list<FeedItem> lstFeedItem = new  list<FeedItem>();
    for(LLC_BI__Product_Package__c pp : trigger.new ){
        if(pp.Hold_Comments__c != null && pp.Hold_Comments__c != trigger.oldMap.get(pp.Id).Hold_Comments__c ){
            FeedItem objFeedItem = new FeedItem();
            objFeedItem.ParentId = pp.Id;
            objFeedItem.Body = 'Changed the Hold Comments from '+ trigger.oldMap.get(pp.Id).Hold_Comments__c +' to '+ pp.Hold_Comments__c;
            objFeedItem.Type = 'TextPost';
            lstFeedItem.add(objFeedItem);
        }
    }
    if(lstFeedItem.size()>0) {   
        insert lstFeedItem;
    }    
}