//================================================================================
//  KeyBank  
//  Object: Lead
//  Author: Offshore
//  Detail: TriggerLead
//  Description : To associate a converted Account Id to the Program member. 
//                Automatic SIC and NAICS description based on Code.
//                Update the Initiator field with Owner automatically.

//================================================================================
//          Date            Purpose
// Changes: 06/04/2015      Initial Version
//================================================================================

trigger TriggerLead on Lead (after update,before insert,before update, after insert)
{
    if(trigger.isInsert && trigger.isafter)
    {
        if(UserInfo.getName() !='Integration User')
        {     
            LeadAccountsprmassociation_handler LDIni = new LeadAccountsprmassociation_handler();
            LDIni.LeadInitiator(Trigger.new);
            LDIni.Leadownerupdate(Trigger.new);
        }
    }
    
    if(trigger.isUpdate && trigger.isAfter)
    {
        LeadAccountsprmassociation_handler LAprm = new LeadAccountsprmassociation_handler();
        LAprm.Leadaccountupdate(Trigger.newMap);
        //LAprm.LeadaccSNcodeupdate(Trigger.newMap);
        LAprm.Leadcallupdate(Trigger.new);
        if(LeadAccountsprmassociation_handler.runOnce())
        {
            LAprm.Leadownerupdate(Trigger.new);
        }
        
        list<string> tidList = new list<string>();
        for(Lead rec: trigger.new)
        {
            tidList.add(rec.ConvertedAccountId);
        }
        LAprm.LeadaccSNcodeupdate(Trigger.newMap, tidList);
    }  
    if(trigger.isBefore)
    {
        LeadAccountsprmassociation_handler LAprmone = new LeadAccountsprmassociation_handler();
        LAprmone.Leadsicnaicupdate(Trigger.new);
    }
    
    
}