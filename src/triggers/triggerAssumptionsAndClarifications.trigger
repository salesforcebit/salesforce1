trigger triggerAssumptionsAndClarifications on Assumptions_and_Clarifications__c (after insert) {

	if (trigger.isAfter) {
		if (trigger.isInsert) {
			AssumptionsClarificationsTriggerHandler.handleAfterInsert(trigger.new);
		}
	}

}