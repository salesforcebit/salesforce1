//================================================================================
//  KeyBank  
//  Object: triggerParticipant
//  Author: offshore
//  Detail: Participant Trigger
//  Description : To add the Particaipant created to the Call share.
//                To populate the Owner Name with Participant Name.
//================================================================================
//          Date            Purpose
// Changes: 03/12/2015      Initial Version
//================================================================================


trigger triggerParticipant on Participant__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) 
{
    
    ParticipantTriggerHandler handler = new ParticipantTriggerHandler(Trigger.isExecuting, Trigger.size);
    ParticipantTriggerHandler handler1 = new ParticipantTriggerHandler(Trigger.isExecuting, Trigger.size);
    ParticipantTriggerHandler handler2 = new ParticipantTriggerHandler(Trigger.isExecuting, Trigger.size);
        if(Trigger.isInsert && Trigger.isAfter)
        {      
            handler.OnAfterInsert(Trigger.new);
            list<id> tidList = new list<id>();
            list<id> tidpList = new list<id>();
            list<id> tidcList = new list<id>();
            system.debug(trigger.new);        
            
            for(Participant__c rec: trigger.new)
            {
                tidList.add(rec.Callid__c);
                tidpList.add(rec.id);
                tidcList.add(rec.Contact_Id__c);
            } 
            
            ParticipantTriggerHandler.OnAfterInsertAsync(tidList,tidpList,tidcList);
            //ParticipantTriggerHandler.sendMailPart(tidpList);  
        }
        
        if(Trigger.isInsert && Trigger.isAfter)
        {
           
            handler1.OnAfterInsert(Trigger.new);
            list<id> pid = new list<id>();
            system.debug(trigger.new);
            
            for(Participant__c recs : trigger.new)
            {
               pid.add(recs.id);
            } 
            
            ParticipantTriggerHandler.changeowner(pid);
                         
        }     
        
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                handler2.OnAfterInsert(Trigger.new);
                list<id> pid = new list<id>();
                list<id> cid = new list<id>();
                list<id> ccid = new list<id>();
                system.Debug('Trigger Size:'+Trigger.new.size());
                for(Participant__c recs : trigger.new){ 
                   pid.add(recs.id);
                   //cid.add(recs.CallId__c);
                   //ccid.add(recs.Contact_Id__c);
                } 
                ParticipantTriggerHandler parthandler = new ParticipantTriggerHandler(Trigger.isExecuting, Trigger.size);
                parthandler.updateRelatedRecordshandler(pid);  
                ParticipantTriggerHandler parthandler1 = new ParticipantTriggerHandler(Trigger.isExecuting, Trigger.size);
                parthandler.updateRelatedRecordshandler(pid);               
                parthandler1.callgoalsjunctionhandler(pid);
            }
        } 
    
}