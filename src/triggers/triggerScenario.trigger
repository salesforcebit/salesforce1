trigger triggerScenario on LLC_BI__Scenario__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    if(trigger.isBefore) {
        if (trigger.isInsert)
        {

        }
        else if(trigger.isUpdate) {
            ScenarioTriggerHandler handler = new ScenarioTriggerHandler();
            handler.handleBeforeUpdate(Trigger.New, Trigger.newMap, trigger.oldMap);
        }
    }
    else{
      if(trigger.isUpdate){
        ScenarioTriggerHandler handler = new ScenarioTriggerHandler();
        handler.handleAfterUpdate(Trigger.New);

      }
    }
}