//================================================================================
//  KeyBank  
//  Object: User
//  Author: Offshore
//  Detail: User Trigger
//  Description : To add Derivative User in to Derivative public group based on User department 
//               
//================================================================================
//          Date            Purpose
// Changes: 03/12/2015      Initial Version
//================================================================================

trigger triggerUser on User (after insert, after update) 
{
   AddUser.AddToGroups(trigger.newMap.keySet());

}