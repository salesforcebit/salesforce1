trigger triggerCallGoals on Call_Goals__c (after insert, after update) {
if(trigger.isinsert ){
shareHandler.shareCallGoals(trigger.new);
}
else if(trigger.isUpdate){
    List<Call_Goals__c > updateGoals = new List<Call_Goals__c >();
    for(Call_Goals__c prod: trigger.new){
       if(trigger.oldMap.get(prod.id).UserName__c!=prod.Username__c){
       updateGoals.add(prod);
       }
    }
    if(updateGoals.size()>0)
    shareHandler.shareCallGoals(updateGoals);
   }
}