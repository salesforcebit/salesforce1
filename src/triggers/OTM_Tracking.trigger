//================================================================================
// Trigger Name : OTM_Tracking
// Object: OpportunityTeamMember 
// Req # : OPPTY02
// Description : Ability to capture when a user is added or delete from the Oppty Team object for private Oppty
// Author: Offshore Team 
//================================================================================
//          Date            Purpose
// Changes: 03/12/2015      Initial Version
//================================================================================


trigger OTM_Tracking on OpportunityTeamMember(before insert, before update, After insert, after delete) 
{    
    opportunityteamMemberTracking_Handler handler = new opportunityteamMemberTracking_Handler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isBefore ){
        if(Trigger.isInsert || Trigger.isUpdate){
            Set<id> oppIds = new Set<id>();
            for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.new){
                oppIds.add(member.OpportunityId);
            }
            //handler.beforeInsertDisStrategy(oppIds);  
           // opportunityteamMemberTracking_Handler.beforeInsertDisStrategy(oppIds);
            handler.onBeforeInsertOTM(oppIds);   
        } 
    }
    
    if(Trigger.isAfter && Trigger.isInsert )
    {    
          
        list<id> ListotmId = new list<id>();
        list<id> ListotmOId = new list<id>();  
        
    for(OpportunityTeamMember otm: trigger.new)
        {
            ListotmId.add(otm.Id);
            ListotmOId.add(otm.OpportunityId);
           
        } 
        opportunityteamMemberTracking_Handler.onAfterInsert(ListotmId, ListotmOId);
    }  
    if(Trigger.isAfter && Trigger.isDelete)
    {
        List<Id> OTIdlist = new List<Id>();
        for(OpportunityTeamMember otm : Trigger.Old)
        {
            OTIdlist.add(otm.id);
        }
        handler.onAfterDelete(Trigger.old);
        //opportunityteamMemberTracking_Handler.onAfterDelete(OTIdlist);
    }
    
     
   if((Trigger.isAfter && Trigger.isUpdate) || (Trigger.isAfter && Trigger.isInsert))
    {
        handler.opportunityshare(Trigger.new);
           
    }
    
    if(Trigger.isAfter && Trigger.isDelete)
    { 
        
        handler.opportunitysharedelete(Trigger.old);
    }
    
}