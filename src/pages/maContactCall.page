<apex:page standardController="Contact" extensions="ext_maContactCall" title="Mobile Action Opportunity Call" docType="html-5.0">

    <!--  Style properties for this page -->
    <apex:stylesheet value="{!$Resource.mobileActionCSS}"/>

    <script type='text/javascript'>

        // This script assists the search bar functionality
        // It will execute a search only after the user has stopped typing for more than 1 second
        // To raise the time between when the user stops typing and the search, edit the following variable:

        var waitTime = 1;
        var countDown = waitTime+1;
        var started = false;

        function resetTimer(){
            countDown=waitTime+0.5;
            
            if(started==false){
                started=true;
                runCountDown();
            }
        }

        function runCountDown(){
            countDown--;
            
            if(countDown<=0){
                fetchResults();
                started=false;
            }
            else{
                window.setTimeout(runCountDown,1000);
            }
        }
    </script>
        
    <apex:form >
        <apex:pageMessages id="pageMessages"/>
        <apex:pageBlock title="" mode="edit">
            <apex:commandButton action="{!save}" value="Save" id="btnSave"/>
                    
            <!-- Information Block -->
            <apex:pageBlockSection title="Information" columns="1">
                <apex:input type="text" value="{!callSubject}" label="*Subject" size="40"/>
                <apex:input type="date" value="{!callDate}" label="*Call Date" />
                <apex:inputCheckbox value="{!cPrivate}" label="Private"/>
                <apex:selectList value="{!cType}" label="*Call Type" required="TRUE" size="1">
                    <apex:selectOptions value="{!cTypeList}"/>
                </apex:selectList>          
                <apex:selectList value="{!cStatus}" label="*Status" required="TRUE" size="1">
                    <apex:selectOptions value="{!cStatusList}"/>
                </apex:selectList>
                <apex:selectList value="{!cCallLocation}" label="Call Location" size="1">
                    <apex:selectOptions value="{!cCallLocationList}"/>
                </apex:selectList>
                <apex:inputTextarea value="{!cComments}" label="Comments" rows="5" cols="40" richText="true"/>
            </apex:pageBlockSection>
            
            <!-- Product Block -->
            <apex:pageBlockSection title="Additional Information" columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Joint Call" for="jc"/>
                    <apex:inputCheckbox value="{!cJointCall}" id="jc"/>      
                </apex:pageBlockSectionItem>
                
                <!-- Product Family -->
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Product Family" for="plevel1"/>
                    <apex:actionRegion immediate="true">
                    <apex:selectList value="{!cProductFamily}" label="Product Family" id="plevel1" size="1">
                        <apex:selectOptions value="{!pFamilyList}"/>
                        <apex:actionSupport event="onchange" rerender="plevel2"/>
                    </apex:selectList>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
                                
                <!-- Product Category -->
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Product Category" for="plevel2"/>
                    <apex:actionRegion immediate="true">
                    <apex:selectList value="{!cProductCategory}" label="Product Category" id="plevel2" size="1">
                        <apex:selectOptions value="{!pCategoryList}"/>
                        <apex:actionSupport event="onchange" rerender="plevel3"/>
                    </apex:selectList>
                    </apex:actionRegion>          
                </apex:pageBlockSectionItem>

                <!-- Product -->
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Product" for="plevel3"/>
                    <apex:selectList value="{!cProduct}" id="plevel3" label="Product" size="1">
                        <apex:selectOptions value="{!productList}"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            <br/>
            <!-- The shopping cart section -->
            <apex:outputPanel id="callParticipants">
            <apex:pageBlock title="Selected {!$ObjectType.Participant__c.LabelPlural}" id="selected">
                <apex:pageblockTable value="{!shoppingCart}" var="s">
                    <apex:column >
                        <apex:actionRegion immediate="true" >
                            <apex:commandButton value="Remove" action="{!removeFromShoppingCart}" reRender="selected,searchResults" immediate="true">
                            
                                <!-- this param is how we send an argument to the controller, so it knows which row we clicked 'remove' on -->
                                <apex:param value="{!s.Contact_Id__c}" assignTo="{!toUnselect}" name="toUnselect"/>
                            </apex:commandButton>
                        </apex:actionRegion>
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.Participant__c.Fields.Contact_Id__c.Label}" value="{!s.Contact_Id__c}"/> 
                 </apex:pageblockTable>
            </apex:pageBlock>
            </apex:outputPanel>
        </apex:pageBlock>
    
    
        <!-- Search -->
        <apex:pageBlock >
            <apex:outputPanel styleClass="search">
                Search by Name or RACF-ID or Officer Code
            </apex:outputPanel>
            <apex:actionRegion renderRegionOnly="false" immediate="true">
                <apex:actionFunction name="fetchResults" action="{!updateAvailableList}" reRender="searchResults" status="searchStatus"/>
        
                <!-- here we invoke the scripting to get out fancy 'no button' search bar to work -->
                <apex:inputText value="{!searchString}" onkeydown="if(event.keyCode==13){this.blur();}else{resetTimer();}" style="width:300px"/>
                &nbsp;&nbsp;
                <i>
                <apex:actionStatus id="searchStatus" startText="searching..." stopText=" "/>
                </i>
            </apex:actionRegion>
            <br/>
            <br/>
            <apex:outputPanel id="searchResults" layout="block" style="overflow:auto;height:200px"> 
                <apex:pageBlockTable value="{!AvailableParticipants}" var="a">
                    <apex:column >
                        <apex:actionRegion immediate="true" >
                            <apex:commandButton value="Select" action="{!addToShoppingCart}" reRender="selected,searchResults,pageMessages" >
                                <apex:param value="{!a.Id}" assignTo="{!tonSelect}" name="tonSelect"/>
                            </apex:commandButton>
                        </apex:actionRegion>
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.Contact.Fields.Name.Label}" value="{!a.Name}" />
                    <apex:column headerValue="{!$ObjectType.Contact.Fields.Title.Label}" value="{!a.Title}"/> 
                    <apex:column headerValue="Account Name" value="{!a.AccountId}"/>
                    <apex:column headerValue="{!$ObjectType.Contact.Fields.MailingCity.Label}" value="{!a.MailingCity}"/> 
                    <apex:column headerValue="{!$ObjectType.Contact.Fields.MailingState.Label}" value="{!a.MailingState}"/> 
                </apex:pageBlockTable>
            </apex:outputPanel> 
        </apex:pageBlock>
    </apex:form>
</apex:page>